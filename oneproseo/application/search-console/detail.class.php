<?php

require_once('sc.class.php');

class detail extends sc
{

  private $out_view = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () {

    if (isset($_POST['action']) && $_POST['action'] == 'add') {
      
      $this->add();

    } else if (isset($_POST['action']) && $_POST['action'] == 'delete') {
      
      $this->delete();

    } else {
      
      $this->show();

    }

  }


  private function delete () {

    $id = $_POST['jobid'];

    echo $sql = "DELETE FROM search_console_jobs WHERE id = $id";
    $result = $this->db->query($sql);

  }


  private function add () {

    $this->out_view = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Der Export wurde angelegt!<br>Du bekommst eine Email wenn er abgeschlossen ist.</strong></div>';

    parse_str($_POST['data'], $data);

    if (strtotime($data['date1']) > strtotime($data['date2'])) {
			echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Der Export wurde nicht angelegt!<br>Das Startdatum muss vor dem Enddatum liegen .</strong></div>';
    }

    $task   = strip_tags($data['task']);
    $date1  = date('Y-m-d', strtotime($data['date1']));
    $date2  = date('Y-m-d', strtotime($data['date2']));
    $staus  = 'new';
    $scurl  = $data['scurl'];
    $name   = $data['name'];
    $email  = $data['email'];
    $result = 'none';

    $sql = "INSERT INTO search_console_jobs (name, date1, date2, status, created_by, email, url, result) 
            VALUES ('".$task."', '".$date1."', '".$date2."','".$staus."','".$name."','".$email."','".$scurl."','".$result."')";
  
    // INSERT JOB
    $this->db->query($sql);
    
    echo $this->out_view;

  }


  private function show () {

    $searchConsole = new sc;

    $sql = "SELECT * FROM search_console_jobs";

    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {

      $this->out_view = '<p style="padding:20px;">Keine Jobs vorhanden.</p>';

    } else {

      $this->out_view = '
        <table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table">
          <thead>
            <tr>
              <td>#</td>
              <td>Jobname</td>
              <td>URL</td>
              <td>Zeitraum</td>
              <td>angelegt von</td>
              <td>Status</td>
              <td>löschen</td>
            </tr>
          </thead>
        <tbody>';

      $i = 1;

      while ($row = $result->fetch_assoc()) {
      
        $this->out_view .= '
      
          <tr>
            <td>' . $i++ . '</td>
            <td><a href=" ' . $row['id'] . '"> ' . $row['name'] . ' </a></td>
            <td> ' . base64_decode($row['url']) . '</td>
            <td> ' . $this->germanTSred($row['date1']) . ' - ' . $this->germanTSred($row['date2']) . '</td>
            <td> ' . $row['created_by'] . ' </td>
            <td> ' . $this->showStatus($row['id'], $row['status'], $row['result']) . ' </a></td>
            <td>
              <div class="btn-group">
                <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                  <i class="icon-cog"></i>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="#" class="ops_delete_job" data-jobid="'.$row['id'].'">Job löschen</a></li>
                </ul>
              </div>
            </td>';
      
      }

      $this->out_view .= '</tbody></table>';
    }

    echo $this->out_view;

  }

  private function showStatus ($id, $status, $result) {

    if ($status == 'working') {
      return '<span class="label label-blue">'  . $status . '</span>';
    }
    if ($status == 'done') {
      return '<a href="https://enterprise.oneproseo.com//temp/searchconsole/'.$result.'.csv" class="label label-green">download CSV</a>';
    }
    if ($status == 'new') {
      return '<span class="label label-red">'  . $status . '</span>';
    }

  }

}

?>