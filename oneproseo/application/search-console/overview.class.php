<?php

require_once('sc.class.php');

class overview extends sc
{

  private $out_view = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () 
  {


    $searchConsole = new sc;
    $sc = $searchConsole->SearchConsoleAuth();
    $auth = $sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'No auth token created!';
    }

    $sc->setAccessToken($accessToken);

    $profiles = $sc->getProfiles();

    $i=1;


    $out = '
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header"><span class="title">Alle Domains mit Search Console Zugriff</span></div>
            <div class="box-content">

              <table class="table table-normal data-table" id="data-table">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Host</th>
                    <th>Domain</th>
                    <th>Export</th>
                  </tr>
                </thead>
                <tbody>';


      $arr = array('http://www.', 'https://www.', 'http://', 'https://');

      foreach ($profiles['siteEntry'] as $item) {
        
        if ($item['permissionLevel'] == 'siteUnverifiedUser') {
          continue;
        }

        if (stripos($item['siteUrl'], 'sc-domain:') !== false) {
          continue; 
        }

        $bare = str_replace($arr, '', $item['siteUrl']);
        $bare = rtrim($bare, '/');

        $out .= '<tr><td>'.$i++.'</td><td>'.$bare.'</td><td>'.$item['siteUrl'].'</td><td><a href="search-console/'.base64_encode($item['siteUrl']).'" class="label label-green">Export anlegen</a></td></tr>';

      }


    $out .= '</tbody></table>';  

    $this->dashboardHint();

    echo $out;
   
  }


  private function dashboardHint() {


    echo '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">×</button><p><strong>Hinweis:</strong></p>
            <p>Es wird ein CSV generiert: Keyword, URL, Impressions, Clicks, CTR und Average Position.</p>
            <p>Wenn der Export abgeschlossen ist bekommst du eine Email.</p>
            <br />
            <p>Sollte eine Domain fehlen muss in der Search Console Property diese Emailadresse mit Leserechten freigeschaltet werden:</p>
            <p><b>352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com</b></p>
          </div>';

  }

}

?>