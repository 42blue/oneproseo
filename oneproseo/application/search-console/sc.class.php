<?php

include('/var/www/oneproapi/searchconsole/api/GoogleSearchConsoleAPI.class.php');

class sc {


  public function setEnv ($env_data)
  {

    $this->env_data = $env_data;

  }


  public function startSession ()
  {

    session_start();
    // allow multiple ajax requests (SESSION)
    session_write_close();

    if ($_SESSION['login'] != true) {
      exit;
    }

  }


// CORE GERMAN TS
  public function germanTSred ($ts)
  {
    if ($ts == '0000-00-00') {
      $ts = '2015-01-01';
    }
    return date('d.m.Y', strtotime($ts));
  }


  // GETHOSTNAME
  public function getHostname ($id)
  {

    $sql = "SELECT * from ruk_project_customers WHERE id = $id";

    $result   = $this->db->query($sql);
    $project  = array();

    while ($row = $result->fetch_assoc()) {
      $project = $row;
    }

   return $project;

  }



  // HOSTNAME
  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  // SUBDOMAIN -> HOSTNAME
  public function pureHostNameNoSubdomain ($host)
  {

    $domain = $host;

    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) && $this->ignore_subd == 1) {
      return $regs['domain'];
    } else {
      return $host;
    }

  }


  public function AnalyticsAuth ()
  {

    $ga = new GoogleAnalyticsAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/analytics_v3/api/key.p12');

    return $ga;

  }


  public function SearchConsoleAuth ()
  {

    $ga = new GoogleSearchConsoleAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }


  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }

  // MYSQL CLOSER
  public function mySqlClose ()
  {

    $this->db->close();

  }


}

?>
