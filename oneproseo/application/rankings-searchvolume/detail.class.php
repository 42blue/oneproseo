<?php

require_once('rs.class.php');

class detail extends rs
{

  private $out_view = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () 
  {

    $this->jobid = $_POST['jobid'];

    $this->getJob();

  }


  private function getJob () {


    $sql    = "SELECT * FROM rankings_searchvolume_jobs WHERE id = $this->jobid";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {

      $out = '
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <span class="title">Ergebnis</span>
              </div>
              <div class="box-content"><p style="padding:20px;">Dieser Job ist nicht vorhanden.</p></div>
            </div>
          </div>
        </div>';

    } else {

      while ($row = $result->fetch_assoc()) {
        $name = $row['name'];
        $date = date('d.m.Y', strtotime($row['date']));
        $kws  = count(unserialize($row['keywords']));
        $type = $row['type'];
      }

      $out = '
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header"><span class="title">Job: '.$name.' vom: '.$date.'</span></div>
              <div class="box-content padded">
                <ul class="content">
                  <li>Anzahl der Keywords: <b>'.$kws.'</b></li>
                  <li>Abgefragt wurden: <b>'.$this->showType($type).'</b></li>
                </ul>
              </div>
            </div>
          </div>
        </div>';


      $sql1    = "SELECT * FROM rankings_searchvolume_keywords WHERE id_job = $this->jobid";
      $result1 = $this->db->query($sql1);
      
      $res = [];
      while ($row = $result1->fetch_assoc()) {
        $res[] = $row;  
      }

      $outc = $this->makeTable($type, $res);

      $out .= '
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header"><span class="title">Ergebnisse:</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProTools | Rankings Searchvolume" data-filepath="' . $this->env_data['csvstore'] . $this->filename . '"></i></li></ul></div>
              <div class="box-content">';      


      $out .= $outc;

      $out .= '
                  </tbody>
                </table>      
              </div>
            </div>
          </div>
        </div>';


    }


    echo $out;

  }

  private function makeTable ($type, $res) {

    if ($type == 'rasv') {

      $outc = '
        <table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Keyword</td><td>Suchvolumen</td><td>CPC</td><td>Wettbewerb</td><td>Rank</td><td>Rankende URL</td></tr></thead><tbody>';
      $csvh   = array();  
      $csvh[] = array('Keyword', 'Suchvolumen', 'CPC', 'Wettbewerb', 'Rank', 'Rankende URL');        
      $csvc   = array();
      $i = 1;
      foreach ($res as $val) {
        $outc .= '<tr><td>'.$i++.'</td><td>'.$val['keyword'].'</td><td>'.$val['searchvolume'].'</td><td>'.$val['cpc'].'</td><td>'.$val['competition'].'</td><td>'.$val['rank'].'</td><td>'.$val['url'].'</td></tr>';
        $csvc[] = array($val['keyword'], $val['searchvolume'], number_format($val['cpc'], 2, ',', ''), number_format($val['competition'], 2, ',', ''), $val['rank'], $val['url']);
      }

    }

    if ($type == 'sv') {

      $outc = '
        <table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Keyword</td><td>Suchvolumen</td><td>CPC</td><td>Wettbewerb</td></tr></thead><tbody>';
      $csvh   = array();          
      $csvh[] = array('Keyword', 'Suchvolumen', 'CPC', 'Wettbewerb');        
      $csvc   = array();
      $i = 1;
      foreach ($res as $val) {
        $outc .= '<tr><td>'.$i++.'</td><td>'.$val['keyword'].'</td><td>'.$val['searchvolume'].'</td><td>'.$val['cpc'].'</td><td>'.$val['competition'].'</td></tr>';
        $csvc[] = array($val['keyword'], $val['searchvolume'], number_format($val['cpc'], 2, ',', ''), number_format($val['competition'], 2, ',', ''));
      }

    }

    if ($type == 'ra') {
      $outc = '
        <table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Keyword</td><td>Rank</td><td>Rankende URL</td></tr></thead><tbody>';
      $csvh   = array();  
      $csvh[] = array('Keyword', 'Rank', 'Rankende URL');        
      $csvc   = array();
      $i = 1;
      foreach ($res as $val) {
        $outc .= '<tr><td>'.$i++.'</td><td>'.$val['keyword'].'</td><td>'.$val['rank'].'</td><td>'.$val['url'].'</td></tr>';
        $csvc[] = array($val['keyword'], $val['rank'], $val['url']);
      }
    }

    $csv = array_merge($csvh, $csvc);

    $this->filename = $this->writeCsv($csv);

    return $outc;

  }


  private function showType ($type) {

    if ($type == 'sv') {
      return 'Suchvolumen';
    }
    if ($type == 'ra') {
      return 'Rankings';
    }
    if ($type == 'rasv') {
      return 'Rankings und Suchvolumen';
    }

  }

  private function writeCsv ($csv) {
    $filename = 'rankings-searchvolume-'.time().'.csv';
    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');

    foreach ($csv as $k => $line) {
      fputcsv($fp, $line);
    }
    fclose($fp);
    return $filename;
  }


}

?>