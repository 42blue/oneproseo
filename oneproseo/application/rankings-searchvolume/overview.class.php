<?php

require_once('rs.class.php');

class overview extends rs
{

  private $out_view = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () 
  {


    if ($_POST['action'] == 'add') {

      parse_str($_POST['data'], $this->data);

      $this->saveToDb();

    }

    if ($_POST['action'] == 'delete') {

      $this->job_id = $_POST['jobid'];

      $this->deleteFromDb();
      
    }

    if ($_POST['action'] == 'show') {
      
      $this->getJobs();

    }

   
  }

  private function getJobs () {


    $sql    = "SELECT * FROM rankings_searchvolume_jobs";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      $out = '<p style="padding:20px;">Keine Jobs vorhanden.</p>';

    } else {

      $out = '<table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr>
        <td>#</td>
        <td>Jobname</td>
        <td>Datum</td>
        <td>angelegt von</td>
        <td>Keywords</td>
        <td>Typ</td>
        <td>Domain</td>
        <td>Sprache</td>
        <td>Status</td>
        <td>löschen</td></tr></thead><tbody>';

      $i = 1;
      while ($row = $result->fetch_assoc()) {

        if ($row['url'] == 'false') {
          $row['url'] = ' - ';
        }

        $out .= '<tr>';
          $out .= '<td>'.$i++.'</td>';
          $out .= '<td>'.$this->showStatusName($row['name'], $row['id'], $row['status']).'</td>';  
          $out .= '<td>'.date('d.m.Y', strtotime($row['date'])).'</td>';
          $out .= '<td>'.$row['created_by'].'</td>';
          $out .= '<td>'.count(unserialize($row['keywords'])).'</td>';
          $out .= '<td>'.$this->showType($row['type']).'</td>';
          $out .= '<td>'.$row['url'].'</td>';
          $out .= '<td>'.$row['lang'].'</td>';
          $out .= '<td>'.$this->showStatus($row['id'], $row['status']).'</td>';        
          $out .= '<td><div class="btn-group"><button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button><ul class="dropdown-menu"><li><a href="#" class="ops_delete_job" data-jobid="'.$row['id'].'">Job löschen</a></li></ul></div></td>';
        $out .= '</tr>';

      }

      $out .= '</tbody></table>';

    }
    
    echo $out;

  }

  private function deleteFromDb () {

    $id = $this->job_id;

    $sql = "DELETE FROM rankings_searchvolume_jobs WHERE id = $id";
    $result = $this->db->query($sql);

    $sql = "DELETE FROM rankings_searchvolume_keywords WHERE id_job = $id";
    $result = $this->db->query($sql);

  }


  private function saveToDb () {

    $name          = $this->data['jobname'];
    $date          = parent::dateYMD();
    $status        = 'new';
    $created_by    = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];    
    $email         = $_SESSION['email'];
    $keywords      = $this->chunkKeywordSet();
    $type          = $this->data['ops_type'];
    $url           = $this->data['url'];
    $rankings_type = $this->data['type'];
    $lang          = $this->data['lang'];
    $customer_id   = $this->env_data['customer_id'];
    $customer_name = $this->env_data['customer_name'];

    if (empty($url)) {
      $url = 'false';
    }

    if (empty($customer_id)) {
      $customer_id = 'false';
    }

    if (empty($customer_name)) {
      $customer_name = 'false';
    }    

    $skeywords     = serialize($keywords);

    $sql = "INSERT INTO 
        rankings_searchvolume_jobs (name, date, status, created_by, email, keywords, type, url, rankings_type, lang, customer_id, customer_name) 
      VALUES 
        ('".$name."', '".$date."', '".$status."', '".$created_by."', '".$email."', '".$skeywords."', '".$type."', '".$url."', '".$rankings_type."', '".$lang."', '".$customer_id."', '".$customer_name."')";

    $this->db->query($sql);

    echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Job für '.count($keywords).' Keywords hinzugefügt!</strong><br />Du bekommst eine Email an '.$email.' sobald die Daten vorliegen.</div>';

  }


  private function chunkKeywordSet () {

    $kw   = strip_tags($this->data['keywords']);
    $kw   = strip_tags($kw);
    $kw   = explode("\n", $kw);
    $kw   = array_map('trim', $kw);
    $umla = array('Ä', 'Ü', 'Ö', "'", '"');
    $umlo = array('ä', 'ü', 'ö', ' ', ' ');
    $kws  = array();

    foreach ($kw as $value) {
      if (empty($value)) {
        continue;
      }
      $value = str_replace($umla, $umlo, $value);
      $value = strtolower($value);
      $value = preg_replace('/\s+/', ' ',$value);
      $kws[$value] = $value;
    }

    if (count($kws) > 5000) {
      $kws = array_slice($kws, 0, 5000);
    }

    $kws = array_values($kws);

    return $kws;

  }

  private function showStatus ($id, $status) {

    if ($status == 'working') {
      return '<span class="label label-blue">'  . $status . '</span>';
    }
    if ($status == 'done') {
      return '<a href="../tools/rankings-searchvolume/'.$id.'" class="label label-green">'  . $status . '</a>';
    }
    if ($status == 'new') {
      return '<span class="label label-red">'  . $status . '</span>';
    }

  }

  private function showStatusName ($name, $id, $status) {

    if ($status == 'done') {
      return '<a href="../tools/rankings-searchvolume/'.$id.'"><b>'  . $name . '</b></a>';
    }

    return $name;

  }

  private function showType ($type) {

    if ($type == 'sv') {
      return 'Suchvolumen';
    }
    if ($type == 'ra') {
      return 'Rankings';
    }
    if ($type == 'rasv') {
      return 'Rankings und Suchvolumen';
    }

  }

}

?>