<?php

class rs {


  public function setEnv ($env_data)
  {

    $this->env_data = $env_data;

  }


  public function startSession ()
  {

    session_start();
    // allow multiple ajax requests (SESSION)
    session_write_close();

    if ($_SESSION['login'] != true) {
      exit;
    }

  }

  public function dateYMD ()
  {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);
  }

  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }

  // MYSQL CLOSER
  public function mySqlClose ()
  {

    $this->db->close();

  }


}

?>
