<?php

require_once('searchconsole/GoogleSearchConsoleAPI.class.php');

class pdc {


  public function setEnv ($env_data)
  {

    $this->env_data = $env_data;

  }


  public function dateYMD ()
  {

    return date("Y-m-d", strtotime('today'));

  }


  // PROPER GERMAN TS
  public function germanTS ($ts)
  {

    $day[0] = 'Sonntag';
    $day[1] = 'Montag';
    $day[2] = 'Dienstag';
    $day[3] = 'Mittwoch';
    $day[4] = 'Donnerstag';
    $day[5] = 'Freitag';
    $day[6] = 'Samstag';

    $dayn = date('w', strtotime($ts));

    return $day[$dayn] . ', ' . date('d.m.Y', strtotime($ts));

  }


  public function readCsv ($file) {

    $csvFile = @file($file);

    if ($csvFile == FALSE) {
      return array();
    }
  
    $data = [];
    foreach ($csvFile as $line) {
      $arr = str_getcsv($line);
      $data[$arr[0]] = $arr;
    }

    return $data;
    
  }


  public function writeCsv ($csv) {

    $filename = 'siteclinic-data-'.time().'.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');

    foreach ($csv as $k => $line) {
      $line = $this->convertToWindowsCharset($line);
      fputcsv($fp, $line);

    }

    fclose($fp);
    return $filename;

  }

  // WINDOWS EXCEL FIX
  private function convertToWindowsCharset($arr) {

    foreach ($arr as $key => $value) {

      $charset =  mb_detect_encoding($value, "UTF-8, ISO-8859-1, ISO-8859-15", true);
      $newarr[] =  mb_convert_encoding($value, "Windows-1252", $charset);

    }

    return $newarr;

  }


  public function readDirectory($dir) {
    
    $fileinfo = array();
    
    $mDirectory = new RecursiveDirectoryIterator(realpath($dir));

    foreach ($mDirectory as $file) {

      if ($file->getFilename() != '.' && $file->getFilename() != '..' && $file->getFilename() != 'index.html') {

        $filetime = filemtime($file);

        $fileinfo[] = array(
           'filename' => $file->getFilename(),
           'filetime' => $filetime,
           'timediff' => time() - $filetime,
           'filesize' => filesize($file)
        );

      }

    }

    return $fileinfo;

  }

  public function auth ()
  {

    $ga = new GoogleSearchConsoleAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }

  // MYSQL CLOSER
  public function mySqlClose ()
  {

    $this->db->close();

  }

}

?>
