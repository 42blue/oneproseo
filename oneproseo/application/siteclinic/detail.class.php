<?php

require_once('pdc.class.php');

class detail extends pdc {

  private $out = '';
  private $scProfiles = array();

  public function __construct ($env_data)
  {

    $this->env_data = $env_data;

    if (!isset($_POST['host'])) {
      echo 'No HOSTNAME!';
      exit;
    }

    if (!isset($_POST['date'])) {
      echo 'No DATE!';
      exit;
    }

    $this->host   = $_POST['host'];
    $this->date   = $_POST['date'];
    $this->action = $_POST['action'];

    // PURGE VIEW    
    $this->filedesc  = 'Export Ergebnis';
    $this->title     = 'Ergebnis:';

    if ($this->action == 'purge') {
      $this->purge     = true;
      $this->filedesc  = 'Export Seiten optimieren / noindex / entfernen';
      $this->title     = 'Seiten optimieren / noindex / entfernen:';
    }

    $this->store = $this->env_data['tempcache'] . '../siteclinic/' . $this->host . '/' . $this->date;
    $this->projectExists();

  }


  private function projectExists () {

    if (!is_dir($this->store)) {

      $this->out = '
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="title">Siteclinc: ' . $this->host . ' / Datum: ' . $this->date . '</span>
                </div>
                <div class="box-content padded">
                  Hier gibt es noch nichts zu sehen. Schaue doch in ein paar Minuten noch einmal.
                </div>
              </div>
            </div>
          </div>
      ';

    } else {

      $sc_data_past30 = parent::readCsv($this->store . '/searchconsole_past30.csv');
      $sc_data_past90 = parent::readCsv($this->store . '/searchconsole_past90.csv');
      $ga_data        = parent::readCsv($this->store . '/analytcis_past30.csv');
      $crawl  			  = parent::readCsv($this->store . '/crawl.csv');

      $timespan1 = 'Zeitraum vom ' . $this->startDate30() . ' bis ' . $this->endDate30();
      $timespan2 = 'Zeitraum vom ' . $this->startDate90() . ' bis ' . $this->endDate90();

      $csv   = array();
      $csv[] = array(
      		'URL', 
      		'Trend', 
      		'URL im Crawl', 
      		'URL in Searchconsole (aktuell)', 
      		'URL in Searchconsole (vor 3 Monaten)', 
          'Pageviews',
      		'Veränderung',
      		'Average Position (aktuell)',
      		'Average Position (vor 3 Monaten)',
      		'Clicks (aktuell)', 
      		'Clicks (vor 3 Monaten)', 
      		'CTR (aktuell)', 
      		'CTR (vor 3 Monaten)', 
      		'Impressions (aktuell)',
      		'Impressions (vor 3 Monaten)', 
      );


      $out_table = '
        <table class="table table-normal data-table dtoverflow" id="data-table">
          <thead>
            <tr>
              <td>#</td>
              <td>URL</td>
              <td>Trend</td>
              <td>URL im Crawl</td>
              
              <td>URL in Searchconsole (aktuell)<br /><br />'.$timespan1.'</td>
              <td>URL in Searchconsole (vor 3 Monaten)<br /><br />'.$timespan2.'</td>

              <td>Pageviews</td>

              <td>
                Veränderung<br />
                <input class="rangefilter colsearch" type="text" id="min6" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max6" name="max" placeholder="max.">
              </td>

              <td>
                &Oslash; Position (aktuell)<br />
                <input class="rangefilter colsearch" type="text" id="min7" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max7" name="max" placeholder="max.">
              </td>
              <td>
                &Oslash; Position (vor 3 Monaten)<br />
                <input class="rangefilter colsearch" type="text" id="min8" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max8" name="max" placeholder="max.">
              </td>
              
              <td>
                Clicks (aktuell)<br />
                <input class="rangefilter colsearch" type="text" id="min9" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max9" name="max" placeholder="max.">
              </td>
              <td>
                Clicks (vor 3 Monaten)<br />
                <input class="rangefilter colsearch" type="text" id="min10" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max10" name="max" placeholder="max.">
              </td>
              
              <td>
                Impressions (aktuell)<br />
                <input class="rangefilter colsearch" type="text" id="min11" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max11" name="max" placeholder="max.">
              </td>
              <td>
                Impressions (vor 3 Monaten)<br />
                <input class="rangefilter colsearch" type="text" id="min12" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max12" name="max" placeholder="max.">
              </td>
              
              <td>
                CTR (%) (aktuell)<br />
                <input class="rangefilter colsearch" type="text" id="min13" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max13" name="max" placeholder="max.">
              </td>
              <td>
                CTR (%) (vor 3 Monaten)<br />
                <input class="rangefilter colsearch" type="text" id="min14" name="min" placeholder="min."><br />
                <input class="rangefilter colsearch" type="text" id="max14" name="max" placeholder="max.">
              </td>
              
            </tr>
          </thead>
        <tbody>';


      if (empty($crawl)) {
        $crawl = $sc_data_past30;
      }

      $all_urls = array_merge($sc_data_past30, $sc_data_past90, $crawl);

      // merge index page pattern
      $entry_pattern = array();
      $entry_pattern[] = 'http://' . $this->host . '/';
      $entry_pattern[] = 'https://' . $this->host . '/';
      $entry_pattern[] = 'http://www.' . $this->host . '/';
      $entry_pattern[] = 'https://www.' . $this->host. '/';

      $entry_pattern[] = 'http://.' . $this->host;
      $entry_pattern[] = 'https://' . $this->host;
      $entry_pattern[] = 'http://www.' . $this->host;
      $entry_pattern[] = 'https://www.' . $this->host;

      foreach ($entry_pattern as $entry) {
        if (!isset($sc_data_past30[$entry])) {
          unset($all_urls[$entry]);
        }
      }

      $purgecount = 0;
      foreach ($all_urls as $url => $data) {

				$icon_crawl = '<td class="center" data-order="0"></td>';
				$csv_crawl  = 'Nein';
        
        $pageviews   = '-';
        $d30_sc_url  = '-';
        $d30_clicks  = '-';
        $d30_impres  = '-';
        $d30_ctr     = '-';
        $d30_pos     = '-';
        $d30_icon_sc = '<td class="center" data-order="0"></td>';
        $d30_csv_sc  = 'Nein';

        $d90_sc_url  = '-';
        $d90_clicks  = '-';
        $d90_impres  = '-';
        $d90_ctr     = '-';
        $d90_pos     = '-';
        $d90_icon_sc = '<td class="center" data-order="0"></td>';
        $d90_csv_sc  = 'Nein';

				$change 		 = '';
        $pos_change  = '<td class="center" data-order="0"></td>';
       	$trend 			 = '<td class="center" data-order="0"></td>';       	
        $trend_csv   = '';

        if (isset($crawl[$url])) {
          $icon_crawl = '<td class="center" data-order="1"><i class="icon-ok"></td>';
          $csv_crawl  = 'Ja';
        }

        if (isset($ga_data[$url])) {
          $pageviews = $ga_data[$url][1];
        }

        if (isset($sc_data_past30[$url])) {
          $d30_sc_url  = $sc_data_past30[$url][0];
          $d30_clicks  = $sc_data_past30[$url][1];
          $d30_impres  = $sc_data_past30[$url][2];
          $d30_ctr     = $sc_data_past30[$url][3];
          $d30_pos     = $sc_data_past30[$url][4];          
          $d30_icon_sc = '<td class="center" data-order="1"><i class="icon-ok"></td>';
          $d30_csv_sc  = 'Ja';
        	$trend 			 = '<td class="center" data-order="1"><i class="status-success icon-circle-arrow-up"></i></td>';
        	$trend_csv   = '+';
        }

        if (isset($sc_data_past90[$url])) {
          $d90_sc_url  = $sc_data_past90[$url][0];
          $d90_clicks  = $sc_data_past90[$url][1];
          $d90_impres  = $sc_data_past90[$url][2];
          $d90_ctr     = $sc_data_past90[$url][3];
          $d90_pos     = $sc_data_past90[$url][4];          
          $d90_icon_sc = '<td class="center" data-order="1"><i class="icon-ok"></td>';
          $d90_csv_sc  = 'Ja';
        	$trend       = '<td class="center" data-order="-1"><i class="status-error icon-circle-arrow-down"></i></td>';
        	$trend_csv   = '-';        	
        }

        if (isset($sc_data_past30[$url]) && isset($sc_data_past90[$url])) {
        	if ($d90_pos > $d30_pos) {
        		$trend      = '<td class="center" data-order="1"><i class="status-success icon-circle-arrow-up"></i></td>';
        		$trend_csv  = '+';
        		$change     = round($d90_pos - $d30_pos, 1);
        		$pos_change = '<td class="center" data-order="'.$change.'">+'.$change.'</td>';
        	}
        	if ($d90_pos < $d30_pos) {
        		$trend      = '<td class="center" data-order="-1"><i class="status-error icon-circle-arrow-down"></i></td>';
        		$trend_csv  = '-';
        		$change     = round($d90_pos - $d30_pos, 1);
        		$pos_change = '<td class="center" data-order="'.$change.'">'.$change.'</td>';
        	}
        	if ($d90_pos == $d30_pos) {
        	  $trend      = '<td class="center" data-order="0"><i class="status-error icon-circle-arrow-right"></i></td>';
        	  $trend_csv  = '';
        		$change     = round($d90_pos - $d30_pos, 1);
        		$pos_change = '<td class="center" data-order="'.$change.'">'.$change.'</td>';
        	}
        } else if (!isset($sc_data_past30[$url]) && isset($sc_data_past90[$url])) {
      		$trend      = '<td class="center" data-order="1"><i class="status-error icon-circle-arrow-down"></i></td>';
      		$trend_csv  = '-';
      		$change     = round(100 - $d90_pos, 1);
      		if ($change < 0) {
      			$change = $change * (-1);
      		}
      		$pos_change = '<td class="center" data-order="-'.$change.'">-'.$change.'</td>';

        } else if (isset($sc_data_past30[$url]) && !isset($sc_data_past90[$url])) {

      		$trend      = '<td class="center" data-order="1"><i class="status-success icon-circle-arrow-up"></i></td>';
      		$trend_csv  = '+';
      		$change     = round($d30_pos, 1);
      		if ($change > 100) {
      			$change = 0;
      		}
      		$pos_change = '<td class="center" data-order="'.$change.'">+'.$change.'</td>';

	      }

        if ($pageviews > 0 OR $d30_pos != '-' OR $d30_impres > 0) {            
          $purgecount++;
        }


        // PURGE VIEW INTERCEPTOR
        if ($this->purge == true) {
          if ($pageviews > 0 OR $d30_pos != '-' OR $d30_impres > 0) {            
            continue;
          }
        }

        $csv[] = array (
          $url, 
          $trend_csv, 
          $csv_crawl,           
          $d30_csv_sc,
          $d90_csv_sc, 
          $pageviews,
          $change,
          $d30_pos,
          $d90_pos,
          $d30_clicks, 
          $d90_clicks,
          $d30_impres, 
          $d90_impres, 
          $d30_ctr, 
          $d90_ctr
        );

        $out_table .= '
          <tr>
            <td></td>
            <td><span style="display:inline-block; width:400px;"><a href="'.$url.'" target="_blank">'.$url.'</span></td>
            '.$trend.'
            '.$icon_crawl.'            
            '.$d30_icon_sc.'
            '.$d90_icon_sc.'
            <td data-order="'.$pageviews.'">'.$pageviews.'</td>            
            '.$pos_change.'
            <td data-order="'.$d30_pos.'">'.$d30_pos.'</td>
            <td data-order="'.$d90_pos.'">'.$d90_pos.'</td>
            <td data-order="'.$d30_clicks.'">'.$d30_clicks.'</td>
						<td data-order="'.$d90_clicks.'">'.$d90_clicks.'</td>
            <td data-order="'.$d30_impres.'">'.$d30_impres.'</td>
            <td data-order="'.$d90_impres.'">'.$d90_impres.'</td>
            <td data-order="'.$d30_ctr.'">'.$d30_ctr.'</td>
            <td data-order="'.$d90_ctr.'">'.$d90_ctr.'</td>
          </tr>';        

      }

      $out_table .= '</tbody></table>';

      
      $this->filename = parent::writeCsv($csv);

			$start = $this->startDate90();
			$end   = $this->endDate30();

      $purgecount = count($all_urls) - $purgecount;

      $this->out = '

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="title">Siteclinc: ' . $this->host . ' / Datum: ' . date('d.m.Y', (strtotime ($this->date))) . '</span>
                </div>
                <div class="box-content padded">
                  Die Searchconsole Daten der Siteclinic stammen aus dem Zeitraum vom <b>'.$start.'</b> bis zum <b>'.$end.'</b>.<br />
                  Der Crawl erfolgte am <b>'. date('d.m.Y', (strtotime ($this->date))) .'</b>.
                  
                  <ul style="margin:20px;">
                    <li>insgesamt wurden <b> ' . count($all_urls) . '</b> Seiten berücksichtigt</li>
                    <li>'.count($crawl).' stammen aus dem OneProCrawler</li>
                    <li>'.count($sc_data_past90).' stammen aus den Search Console Daten ('.$timespan2.')</li>  
                    <li>'.count($sc_data_past30).' stammen aus den Search Console Daten ('.$timespan1.')</li>    
                    <li>davon haben <b>'.$purgecount.'</b> Seiten keine Rankings / Impressions / PageViews</li>
                    <li>Seiten aus dem OneProCrawler überschneiden sich in der Regel mit den Search Console URLs</li>
                  </ul>';
                  
                  if ($this->purge == true) {
                    $this->out .= '
                      <div style="margin-top:20px;" class="alert alert-error">
                        <strong>'. $purgecount .' Seiten entfernen:</strong><br /> Hier werden alle Seiten angezeigt die in den letzten 4 Wochen:
                        <ul style="margin:10px 20px;">
                          <li>keine PageViews hatten</li>
                          <li>keine Rankings hatten</li>
                          <li>keine Impressions hatten</li>
                        </ul>
                        Diese können im Zuge einer Siteclinic von der Domain entfernt werden.
                      </div>';
                  }


        $this->out .= '</div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="titleleft">'.$this->title.' ' . $this->host . '</span>
                  <ul class="box-toolbar">
                    <li><a target="_blank" href="result"><span class="label label-dark-blue">Ergebnis</span></a></li>
                    <li><a target="_blank" href="purge"><span class="label label-green">' . $purgecount .' Seiten optimieren / noindex / entfernen</span></a></li>
                    <li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename . '" target="_blank"></a></li>
                    <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | Siteclinic | '.$this->filedesc.'" data-filepath="' . $this->env_data['csvstore'] . $this->filename . '"></i></li>
                    </ul>
                </div>
                <div class="box-content">
                  '.$out_table.'
                </div>
              </div>
            </div>
          </div>
      ';

    }

    echo $this->out;

  }

  public function startDate90 ()
  {
    return date("d.m.Y", strtotime('- 90 days', strtotime ($this->date)));    
  }

  public function endDate90 ()
  {
    return date("d.m.Y", strtotime('- 60 days', strtotime ($this->date)));    
  }

  public function startDate30 ()
  {
    return date("d.m.Y", strtotime('- 33 days', strtotime ($this->date)));
  }

  public function endDate30 ()
  {
    return date("d.m.Y", strtotime('- 3 days', strtotime ($this->date)));
  }


}

?>