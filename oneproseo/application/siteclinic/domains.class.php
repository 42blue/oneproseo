<?php

require_once('pdc.class.php');

class domains extends pdc {

  private $out = '';
  private $scProfiles = array();


  public function __construct ($env_data)
  {

    $this->env_data = $env_data;

    parent::mySqlConnect();

    $this->store = $this->env_data['tempcache'] . '../siteclinic/';

    $this->getAccountsDB();
    $this->getAccountsSC();
    $this->renderView();

    echo $this->out;

  }

  public function getAccountsDB () {

    $sql = "SELECT name, url, manager, gwt_account, ga_account from ruk_project_customers WHERE gwt_account != 0";

    $result   = $this->db->query($sql);
    $this->dbProfiles  = array();

    while ($row = $result->fetch_assoc()) {
      $host = parent::pureHostName($row['url']);
      $this->dbProfiles[$host] = array($host, $row['url'], $row['manager'], $row['ga_account']);
    }

  }

  public function getAccountsSC () {

    $sc   = parent::auth();
    $auth = $sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'No auth token created!';
      exit;
    }

    $sc->setAccessToken($accessToken);
    $profiles = $sc->getProfiles();
    $sites = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }
    	$host = parent::pureHostName($item['siteUrl']);
      $sites[$item['siteUrl']] = array($host, $item['siteUrl']);
    }


    // SELECT ONLY FULL DOMAINS NO SUBS
    foreach ($sites as $url => $data) {

    	$host = parent::pureHostName($url);
			$path = parse_url($url, PHP_URL_PATH);

			// PREFER HTTPS // NO SUBFOLDERS
    	if (stripos($url, 'https') !== FALSE && strlen($path) <= 1 ) {
				$this->scProfiles[$host] = $data;
    	}

    	// HTTPS ?
    	if (isset($this->scProfiles[$host])) {
				continue;
    	}

			// NO SUBFOLDERS
    	if (strlen($path) > 1) {
				continue;
    	}

    	// COLLECT REST
			$this->scProfiles[$host] = $data;

    }

  }


  public function renderView () {

    $this->out .= '<table class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td style="width:400px;">Domain</td><td style="width:400px;">URL</td><td>Analytics</td><td>Siteclinics</td><td style="width:200px;">anzeigen</td><td style="width:200px;">Siteclinic starten</td>';
    $this->out .= '</tr></thead><tbody>';

    $i = 1;
  
    foreach ($this->dbProfiles as $key => $set) {

      // SC ACCESS??
      if (!isset($this->scProfiles[$key])) {
        continue;
      }
      
      $this->createMeta($set[0], $set[1], $set[3]);

    	$store = $this->env_data['tempcache'] . '../siteclinic/' . $set[0];
    	$dir   = parent::readDirectory($store);

    	$count = count($dir);
    	$count = $count - 1;

      $create_sc = '<a data-hostname="' . $set[0] . '" class="label label-green ops_siteclinic_start">Siteclinic starten</a>';

      if (file_exists($this->store .$set[0].'/'. parent::dateYMD())) {
        $create_sc = 'Siteclinic wurde heute schon erstellt';
      } 

      $analytics = 'Nein';
      if (!empty($set[3])) {
        $analytics = '<span title="'.$set[3].'">Ja</span>';
      }

      $this->out .= '
        <tr>
          <td>'.$i++.'</td>
          <td>'.$set[0].'</td>
          <td>'.$set[1].'</td>
          <td>'.$analytics.'</td>          
          <td>'.$count.'</td>          
          <td><a href="siteclinic/'.$set[0].'" target="_blank" class="label label-green">anzeigen</a></td>
          <td>'.$create_sc.'</td>
        </tr>';
    }

    $this->out .= '</table>';

  }


  private function createMeta ($host, $url, $gaid) {

    $dir = $this->store . $host;

    if (is_dir($dir) == false) {

			mkdir($dir);

      $meta         = array();
      $meta['url']  = $url;
      $meta['gaid'] = $gaid;      
      $data         = serialize($meta);

      file_put_contents($dir . '/url.lock', $data);
    }

  }

}

?>