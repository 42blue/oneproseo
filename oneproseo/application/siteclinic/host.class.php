<?php

require_once('pdc.class.php');

class host extends pdc {

  private $out = '';
  private $scProfiles = array();

  public function __construct ($env_data)
  {

    $this->env_data = $env_data;

    if (!isset($_POST['host'])) {
      echo 'No HOSTNAME!';
      exit;
    }

    $this->host  = $_POST['host'];
    $this->store = $this->env_data['tempcache'] . '../siteclinic/' . $this->host;

    if (isset($_POST['action'])) {
      
      if ($_POST['action'] == 'start') {
        $this->startSiteclinic();
      } else if ($_POST['action'] == 'delete') {
        $this->date  = $_POST['date'];
        $this->deleteSiteclinic();
      }

    } else {

      $this->projectExists();      

    }

  }


  private function projectExists () {

      $dir = parent::readDirectory($this->store);

      $out = 'Die Siteclinic crawlt die Website. Bei großen Websites kann dies länger dauern. Ein Crawl ist derzeit auf 50.000 Seiten beschränkt.<br /><br />';
      $out .= '<button type="submit" class="btn btn-primary btn-lg" data-hostname="' . $this->host . '" id="ops_siteclinic_start">Siteclinic starten</button>';

      foreach ($dir as $attribute) {
        if ($attribute['filename'] == parent::dateYMD()) {
          $out = 'Eine Siteclinic wird oder wurde heute schon durchgeführt. Derzeit ist nur eine Analyse pro Tag möglich.';
          break;
        }
      }

      $out_table = '
        <table class="table table-normal data-table dtoverflow" id="data-table">
          <thead>
            <tr>
              <td style="width:400px;">Datum</td>
              <td style="width:100px;">Beauftragt</td>
              <td style="width:100px;">Letztes Update</td>
              <td>Status SC / GA</td>
              <td>Status Crawl</td>
              <td style="width:200px;">angelegt von</td>
              <td style="width:100px;">anzeigen</td>    
              <td style="width:100px;">löschen</td>
            </tr>
          </thead>
        <tbody>';

      $i = 1;

      foreach ($dir as $attribute) {
  
        if ($attribute['filename'] == 'url.lock') {
          continue;
        }

	  		$dir  = $this->store . '/' . $attribute['filename'] . '/meta.lock';

  			$file = file_get_contents($dir);
				$meta = unserialize($file);

        if ($meta['crawl'] == 'Fehler') {
          $meta['crawl'] = 'Fehler anzeigen';
        }

        $out_table .= '
	        <tr>
	        	<td data-sort="'.$attribute['filename'].'">Siteclinic vom ' . parent::germanTS($attribute['filename']) . '</td>
	        	<td>' . date('H:i', $meta['start']) . ' Uhr</td>
	        	<td>' . date('H:i', $meta['lastupdate']) . ' Uhr</td>	        	
	        	<td>' . $meta['searchconsole'] . '</td>
	        	<td>
              <a target="_blank" href="https://enterprise.oneproseo.com/temp/siteclinic/' . $this->host . '/'. $attribute['filename'] . '/crawl.log">' . $meta['crawl'] . '</a></td>
	        	<td>' . $meta['user'] . '</td>        	        	        	
	        	<td><a href="'.$this->host.'/'.$attribute['filename'].'/result" class="label label-green">Ergebnis</a></td>
	        	<td>
	        		<div class="btn-group open">
	        			<div class="btn-group">
	        				<button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
	        					<i class="icon-cog"></i>
	        				</button>
	        				<ul class="dropdown-menu">
	        					<li>
	        						<a href="#" class="ops_siteclinic_delete" data-hostname="' . $this->host . '" data-date="' . $attribute['filename'] . '">Siteclinic löschen</a>
	        					</li>
	        				</ul>
	        			</div>
	        		</div>
	      		</td>
	      	</tr>';
      }

      $out_table .= '</tbody></table>';


      if (count($dir) < 1) {
        $out_table = '
          <div class="box-content padded">
            Keine SiteClinics vorhanden.
          </div>';
      }


      $this->out = '
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="title">Übersicht</span>
                </div>
                <div class="box-content padded">
                  '.$out.'
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                  <span class="title">Vorhandene Siteclinics</span>
                </div>
                <div class="box-content">
                  '.$out_table.'
                </div>
              </div>
            </div>
          </div>
      ';


    echo $this->out;

  }


  private function startSiteclinic () {

    $this->date = parent::dateYMD();

    $dir = $this->store . '/' . $this->date;

    mkdir($dir);

    $meta = array();

		$meta['status']        = 'ausstehend';
		$meta['crawl']         = 'ausstehend';
		$meta['searchconsole'] = 'ausstehend';
		$meta['user']          = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];
		$meta['email']         = $_SESSION['email'];
		$meta['start']         = time();
	  $meta['lastupdate']    = time(); 

		$data = serialize($meta);

    file_put_contents($dir . '/meta.lock', $data);

  }


  private function deleteSiteclinic () {

    $dir = $this->store . '/' . $this->date;

    $files = array_diff(scandir($dir), array('.','..'));
    
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? recurseRmdir("$dir/$file") : unlink("$dir/$file");
    }
  
    rmdir($dir);

  }

}

?>