<?php

namespace OneAdvertising;

use PDO;

class ScreamingfrogUtilities
{
    private static $dbPdo;

    private static function initDatabase()
    {
        try {
            self::$dbPdo = new \PDO('mysql:host:127.0.0.1;dbname=oneproseo', 'root', 'root');
            self::$dbPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $ex) {
            echo $ex->getTraceAsString();
            die();
        }
    }

    /**
     * Saves request data to database
     *
     * @param $processid
     * @param $request
     * @param $response
     * @param $type
     * @return mixed
     */
    public static function saveRequestToDatabase($processid, $request, $response, $type, $user)
    {
        self::initDatabase();
        $timestamp = time();
        $crdate = $timestamp;

        $insertStatement = self::$dbPdo->prepare(
            'INSERT into oneproseo.oneproseo_screamingfrog (processid, request, response, type, tstamp, crdate, user) VALUES (?, ?, ?, ?, ?, ?, ? )'
        );

        return $insertStatement->execute(
            [
                $processid,
                $request,
                $response,
                $type,
                $timestamp,
                $crdate,
                $user
            ]
        );
    }

    /**
     * Gets all saved request by user
     *
     * @param string $user
     * @param string $usergroup
     * @return mixed
     */
    public static function getAllUsersRequestsFromDatabase($user, $type)
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT processid FROM oneproseo.oneproseo_screamingfrog WHERE (user = ?) AND (hidden = 0 AND deleted = 0) AND type = ?'
        );
        $selectStatement->execute(
            [
                $user,
                $type
            ]
        );
        $processHashArray = [];
        while ($row = $selectStatement->fetch()) {
            $processHashArray[] = $row['processid'];
        }
        return $processHashArray;
    }

    /**
     * @param $processid
     * @param $user
     * @param $type
     * @return mixed
     */
    public static function getProcessDataByProcessid($processid, $user, $type)
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT
                *
            FROM
                oneproseo.oneproseo_screamingfrog
            WHERE
                (processid = ? AND type = ?)
            AND
                (user = ?)
            AND
                (hidden = 0 AND deleted = 0)'
        );
        $selectStatement->execute(
            [
                $processid,
                $type,
                $user,
            ]
        );
        $result = $selectStatement->fetch();
        return [
            'request' => unserialize($result['request']),
            'crdate' => $result['crdate']
        ];
    }

}
