<?php

namespace OneAdvertising;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ScreamingfrogCrawler
 *
 * @category Project
 * @package OneAdvertising
 */
class ScreamingfrogCrawler
{

//    protected $apiUrl = 'http://api.oneproseo';
    protected $apiUrl = 'http://api.oneproseo';
    /**
     * @var string
     */
    protected $crawleurl;
    /**
     * @var string
     */
    protected $urlFile;
    /**
     * @var string
     */
    protected $apikey;
    /**
     * @var string
     */
    protected $parameters;
    /**
     * @var mixed
     */
    protected $configfile;
    /**
     * @var array
     */
    public $apiResponse;
    /**
     * @var mixed
     */
    protected $schedule;

    /**
     * ScreamingfrogCrawler constructor.
     *
     * @param $formPostData
     */
    public function __construct($formPostData)
    {
        if (!empty($formPostData['crawlerurl'])) {
            $this->crawleurl = $formPostData['crawlerurl'];
            unset($formPostData['urlfile']);
        }
        // if scheduled crawler
        if (!empty($formPostData['schedule'])) {
            unset($formPostData['urlfile']);
            unset($formPostData['--file']);
            $this->schedule = $formPostData['schedule'];
        }
        // urlfile for crawler
        if (!empty($formPostData['urlfile'])) {
            $this->urlFile = $formPostData['urlfile'];
            unset($formPostData['crawlerurl']);
        }
        // checking for screamingfrog config file - required
        if (empty($formPostData['configfile'])) {
            $this->apiResponse['error'] = json_encode('No configfile found');
            exit();
        }
        if (empty($formPostData['apikey'])) {
            $this->apiResponse['error'] = json_encode('No APIKey found');
            exit();
        }
        $this->configfile = $formPostData['configfile'];
        $this->apikey = $formPostData['apikey'];
        $this->parameters = $formPostData['parameters'];
        // sending request to api
        $response = $this->sendApiRequest();
        $this->apiResponse = json_decode($response);
        if (!empty($this->apiResponse->requestId)) {
            $requestId = $this->apiResponse->requestId;
            $response = $response;
            $request = serialize($formPostData);
            // save data
            ScreamingfrogUtilities::saveRequestToDatabase(
                $requestId,
                $request,
                $response,
                $type = 'screamingfrog',
                $user = $_SESSION['userid']
            );
        }
    }

    /**
     * Sending data to API
     *
     * @throws GuzzleException
     */
    public function sendApiRequest()
    {
        // default endpoint
        $apiEndpoint = '/crawler';
        $postDataArray = [
            'url' => $this->crawleurl,
            '--file' => $this->urlFile,
            'configfile' => $this->configfile,
            'apikey' => $this->apikey,
            'parameters' => $this->parameters
        ];

        // checks final endpoint
        if (isset($this->schedule)) {
            $apiEndpoint = '/crawler/scheduler';
            $postDataArray['form_params']['schedule'] = $this->schedule;
        }

        // starts request
        return $this->postDataToApi($this->apiUrl . $apiEndpoint, $postDataArray);
    }

    /**
     * Post Data Request function
     *
     * @param $url
     * @param $data
     * @return bool|string
     */
    public function postDataToApi($url, $data)
    {
        $curl = curl_init();
        $sUrlForAPI = $url;
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data,
            )
        );

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    /**
     * Gets all crawler data from user or usergroup
     *
     * @param $user
     * @param $usergroup
     * @return array
     */
    public static function getStatus()
    {
        $processHashArray = ScreamingfrogUtilities::getAllUsersRequestsFromDatabase(
            $_SESSION['userid'],
            $type = 'screamingfrog'
        );
        if (is_array($processHashArray)) {
            $crawlerStatus = [];
            foreach ($processHashArray as $processHash) {
                if (empty($processHash)) {
                    continue;
                }
                $crawlerStatus[] = self::getCrawlerStatusFromAPI($processHash);
            }
            return $crawlerStatus;
        }
//         single string request
        return json_encode('No Data found');
    }

    /**
     * @param $processHash
     * @return array
     * @throws GuzzleException
     */
    private static function getCrawlerStatusFromAPI($processHash)
    {
        $responseBody = file_get_contents('http://api.oneproseo' . '/crawler/status/' . $processHash);

        // scheduled crawler
        if (strpos($responseBody, 'Scheduled Crawler')) {
            return [
                'processid' => $processHash,
                'status' => 'scheduled',
                'log' => explode('","', $responseBody)
            ];
        }
        // finished crawlers
        if (strpos($responseBody, 'SpiderCrawlIdleState')) {
            $files = self::getCrawlerFilesByProcessHash($processHash);
            return [
                'processid' => $processHash,
                'status' => 'finish',
                'files' => self::buildDownloadLinks($files, $processHash)
            ];
        }
        // still running
        if (strpos($responseBody, 'Starting to spider UrlListItem') && !strpos($responseBody, 'SpiderCrawlIdleState')) {
            $files = self::getCrawlerFilesByProcessHash($processHash);
            return [
                'processid' => $processHash,
                'status' => 'running',
                'log' => explode('","', $responseBody)
            ];
        }
        // error crawler
        if (!strpos($responseBody, 'SpiderProgress')) {
            return [
                'processid' => $processHash,
                'status' => 'error',
                'log' => explode('","', $responseBody)
            ];
        }
    }

    /**
     * @param array $files
     * @param string $processHash
     * @return array
     */
    private static function buildDownloadLinks($files, $processHash)
    {
        $linkArray = [];
        foreach ($files as $file) {
            if (empty($file)) {
                continue;
            }
            $filedetails = explode('.', basename($file));
            $linkArray[] = [

                'link' => 'http://api.oneproseo' . '/crawler/file/' . $processHash . '/' . $file,
                'filename' => $filedetails[0],
                'fileext' => $filedetails[1],
                'file' => $file
            ];
        }
        return $linkArray;
    }


    /**f
     *
     * @param $processHash
     * @return mixed
     * @throws GuzzleException
     */
    private static function getCrawlerFilesByProcessHash($processHash)
    {
        $responseBody = file_get_contents('http://api.oneproseo'. '/crawler/files/' . $processHash);
        return json_decode($responseBody, true);
    }
}
