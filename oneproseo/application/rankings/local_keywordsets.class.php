<?php

require_once('ruk.class.php');

class local_keywordsets extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['projectid'])) {
      $this->project_id = $_POST['projectid'];
      $this->getProjectData();
    }

  }


  private function getProjectData()
  {

    $sql = "SELECT * FROM ruk_local_keyword_sets WHERE id_customer = '".$this->project_id."'";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      echo '<div class="span6"><div class="box"><div class="box-content padded"><a href="keywords"><span class="label label-green">Local Campaign anlegen</span></a> <a href="keywords-csv"><span class="label label-green">Local Campaign anlegen (CSV)</span></a></div></div></div>';
      exit;
    }

    while ($row = $result->fetch_assoc()) {

      if ($row['customer_view'] == 1) {
        $show_customer = 'Ja';
      } else {
        $show_customer = 'Nein';
      }

      $show_type = $this->rsTypeLocal($row['rs_type']);

      $data[$row['id']] = array ('name' => $row['name'], 'show' => $show_customer, 'type' => $show_type, 'created_by' => $row['created_by'], 'created_on' => $row['created_on']);

    }


    $this->out_view = '<div class="box"><div class="box-header"><span class="title">Vorhandene Local Campaigns</span></div><div class="box-content"><table class="table table-normal" id="data-table"><thead><tr><td>Name</td><td>Type</td><td>Anzeige Kundenversion</td><td>Anzahl Keywords</td><td>Anzahl Orte</td><td>Anzahl Überwachungen</td><td>Typ</td><td>Erstellt von</td><td>Datum</td><td>bearbeiten</td><td>löschen</td></thead><tbody>';

    foreach ($data as $setid => $row) {

      $sql = "SELECT id FROM ruk_local_sets2rs WHERE id_kw_set = '".$setid."'";
      $result = $this->db->query($sql);
      $count_locations = $result->num_rows;

      $sql = "SELECT DISTINCT keyword FROM ruk_local_keywords WHERE id_kw_set = '".$setid."'";
      $result = $this->db->query($sql);
      $count_keywords = $result->num_rows;

      $count_combine = $count_keywords * $count_locations;

      $this->out_view .= '<tr><td>' . $row['name'] . '</td><td>Keyword Campaign</td><td> ' . $row['show'] . '</td><td> ' . $count_keywords . '</td><td> ' . $count_locations . '</td><td> ' . $count_combine . '</td><td> ' . $row['type'] .'</td><td> ' . $row['created_by'] . '</td><td> ' . $this->germanTSred($row['created_on']) . '</td><td><a href="keywords/'.$setid.'"><span class="label label-red-super">Keywords bearbeiten</span></a></td><td><div class="btn-group" data-setid="'.$setid.'"><button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button><ul class="dropdown-menu"><li><a href="#" class="ops_delete_keywordset" data-kwsetid="'.$setid.'">Campaign löschen</a></li></ul></div></td></tr>';

    }

    $this->out_view .= '</tbody></table></div></div>';
    $this->out_view .= '<div class="span6"><div class="box"><div class="box-content padded"><a href="keywords"><span class="label label-green">Local Campaign anlegen</span></a> <a href="keywords-csv"><span class="label label-green">Local Campaign anlegen (CSV)</span></a></div></div></div>';

    echo $this->out_view;

  }


}

?>
