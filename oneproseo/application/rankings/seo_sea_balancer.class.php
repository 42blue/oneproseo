<?php

require_once('ruk.class.php');
require_once('/var/www/oneproapi/analytics_v3/api/GoogleAnalyticsAPI.class.php');
require_once('/var/www/oneproapi/searchconsole/api/GoogleSearchConsoleAPIBalancer.class.php');

/*

Balancer

DONE -> Spalte: Quelle -> SEA / SEO / Beide
DONE -> SEA -> Filter alle die mind. 1 Click
DONE -> SC -> Filter alle die mind. 1 Click
DONE -> Toggle alle an
DONE -> Keywords markieren und in Zwischenablage (alle / einzeln)
DONE -> Keyowords filtern -> dann in Zwischenablage
DONE -> erst ab 3 Zeichen suchen
DONE -> Datenexport in Google Docs
DONE -> alle LL Keywords einmal rankings (morgen früh)
DONE -> Kannst Du Checkbox in eigen Spalte ganz nach links stellen?
DONE -> Zwischenablage -> Zeile für Zeile
DONE -> Ansichten -> Brand / Non Brand
DONE -> Brand Keywords -> anlegen / anzeigen ausblenden
DONE -> Umsatzquelle
DONE -> SEA Matchtype
DONE -> Pageview
DONE -> SC = GSC
DONE -> alle als Toggle: underline klein kein Button
DONE -> Range Filter SEA -> Impressions Clicks, Kosten Umsatz
DONE -> Range Filter SEO -> Clicks, Umsatz, PageValue, Ranking
DONE -> Range Filter SC -> Alle
DONE -> SEA vs. SEO
DONE -> auf clicks
DONE -> text zur erklärung

https://developers.google.com/analytics/devguides/reporting/core/dimsmets

*/


class seo_sea_balancer extends ruk
{

  public $searchkeywords = array();

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->cache = new Cache();
    $this->cache->setEnv($env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {


    if (isset($_POST['id']) && $_POST['timestart'] == 0 && $_POST['timeend'] == 0) {
      
      $this->id = $_POST['id'];
    	$data = parent::getHostname($this->id);
			$this->customername = $data['name'];
			$this->showInput();

		} else if (isset($_POST['id']) && $_POST['timestart'] != 0 && $_POST['timeend'] != 0) {

      $this->id        = $_POST['id'];
      $this->view      = $_POST['view'];

      if (strtotime($_POST['timestart']) < strtotime('-92 days') || strtotime($_POST['timestart']) > strtotime('today')) {
         echo 'Kein gültiger Zeitraum!';
         exit;           
       }
      if (strtotime($_POST['timeend']) < strtotime('-92 days') || strtotime($_POST['timeend']) > strtotime('today')) {
         echo 'Kein gültiger Zeitraum!';
         exit;
       }

      $this->timestart_readable = strip_tags($_POST['timestart']);
      $this->timeend_readable   = strip_tags($_POST['timeend']);

      $this->timestart = date('Y-m-d', strtotime($_POST['timestart'])); 
      $this->timeend   = date('Y-m-d', strtotime($_POST['timeend']));

      $this->filename = 'balancer-'.$this->id.'-'.$this->timestart.'-'.$this->timeend.'-';

      $data = parent::getHostname($this->id);

      $this->brandKeywords = array ();
      $brands = unserialize ($data['brandkeywords']);

      if (!empty($brands)) {
        foreach ($brands as $key) {
          $this->brandKeywords[$key] = $key;
        }
      }

      $this->url        = $data['url'];
      $this->ga_account = $data['ga_account'];
      $this->lang       = $data['country'];
      $this->hostname   = parent::pureHostName($this->url);

      $visitsAdwords = $this->gaQueryAdwords();
      $visitsOrganic = $this->gaQueryOrganic();
      $visitsSearchC = $this->scQuery();

      $this->getRankingData($visitsAdwords, $visitsSearchC);

      if ($this->view == 'overview') {
        $this->considerBrand = false;
        $this->processResult($visitsAdwords, $visitsOrganic, $visitsSearchC);
      } else if ($this->view == 'brand') {
        $this->considerBrand = true;
        $this->processResult($visitsAdwords, $visitsOrganic, $visitsSearchC);
      } else if ($this->view == 'seaseo') {
        $this->considerBrand = false;
        $this->processResult($visitsAdwords, $visitsOrganic, $visitsSearchC);
      } else if ($this->view == 'seosea') {
        $this->considerBrand = false;
        $this->processResult($visitsAdwords, $visitsOrganic, $visitsSearchC);
      }

    }

  }

  private function processResult ($visitsAdwords, $visitsOrganic, $visitsSearchC)
  {

    $this->csvarr['head'] = array(
      'Keyword','Zeichen','Quelle','Umsatzquelle','Match Type','SEA Impressions','SEA Clicks','SEA Kosten','SEA Umsatz','SEA CPC',
      'SEA URL','SEO PV','SEO Clicks','SEO Umsatz','SEO PageValue','Rank (heute)','SEO URL','GSC Impressions','GSC Clicks','GSC CTR (%)',
      'GSC Rank','Short Keyword Adwords','URL Adwords'
    );

    $tablebody = '';
    $sc_divider_ctr = 1;
    $sc_divider_pos = 1;
    $sum_imp        = 0;
    $sum_clicks     = 0;
    $sum_cost       = 0;
    $sum_rev        = 0;
    $sum_cpc        = 0;
    $sum_seo_pv     = 0;
    $sum_seo_cl     = 0;
    $sum_seo_rev    = 0;
    $sum_seo_pava   = 0;
    $reminderurl    = array();
    $i              = 0;


    foreach ($this->searchkeywords as $keyword) {

      $hint = 'ohne Brand Keywords';

      if ($this->considerBrand == false) {
        if (isset($this->brandKeywords[$keyword])) {continue;}
      } else {
        $hint = 'inkl. Brand Keywords';
      }

      $i++;
      $matchUrl = parent::removeHttps($this->rankings[$keyword][0]['u']);

      // SEA->SEO INTERCEPTOR
      if ($this->view == 'seaseo') {
        $hint = 'SEA Umsatz aber keine SEO Clicks';
        if ($visitsSearchC[$keyword]['clicks'] > 0) {
          continue;
        }
        if ($visitsAdwords[$keyword]['rev'] <= 0) {
          continue;
        }
      }
      // SEA->SEO INTERCEPTOR


      // SEO->SEA INTERCEPTOR
      if ($this->view == 'seosea') {
        $hint = 'Mehr als 3 SEO Clicks, kein SEA Umsatz';
        if ($visitsSearchC[$keyword]['clicks'] < 3) {
          continue;
        }

        if ($visitsAdwords[$keyword]['rev'] > 0) {
          continue;
        }
      }
      // SEO->SEA INTERCEPTOR


    // SearchConsole SUM
      $sum_sc_imp = $sum_sc_imp + $visitsSearchC[$keyword]['imp'];
      $sum_sc_clicks = $sum_sc_clicks + $visitsSearchC[$keyword]['clicks'];
      if (!empty($visitsSearchC[$keyword]['ctr'])) {
        $sc_divider_ctr++;
        $sum_sc_ctr = $sum_sc_ctr + $visitsSearchC[$keyword]['ctr'];
      }
      if (!empty($visitsSearchC[$keyword]['pos'])) {
        $sc_divider_pos++;
        $sum_sc_pos = $sum_sc_pos + $visitsSearchC[$keyword]['pos'];
      }
    // SearchConsole SUM

    // ADWords SUM
      $sum_imp = $sum_imp + $visitsAdwords[$keyword]['imp'];
      $sum_clicks = $sum_clicks + $visitsAdwords[$keyword]['clicks'];
      $sum_cost = $sum_cost + $visitsAdwords[$keyword]['cost'];
      $sum_rev = $sum_rev + $visitsAdwords[$keyword]['rev'];
      $sum_cpc = $sum_cpc + $visitsAdwords[$keyword]['cpc'];
    // ADWords SUM

    // GA SUM
    if (!isset($reminderurl[$matchUrl])) {
      $sum_seo_pv  = $sum_seo_pv + $visitsOrganic[$matchUrl]['ga:pageviews'];
      $sum_seo_cl  = $sum_seo_cl + $visitsOrganic[$matchUrl]['ga:organicSearches'];
      $sum_seo_rev = $sum_seo_rev + $visitsOrganic[$matchUrl]['ga:transactionRevenue'];
      $sum_seo_pava  = $sum_seo_pava + $visitsOrganic[$matchUrl]['ga:pageValue'];
      $reminderurl[$matchUrl] = $matchUrl;
    }
    // GA SUM

      $p = '';
      $p_csv = '';
      $u = '';
      $u_csv = '';

      // MULTI RANKINGS
      if (count($this->rankings[$keyword]) > 1) {
        foreach ($this->rankings[$keyword] as $key => $value) {
          if (empty($value['p'])) {continue;}
          $p .= $value['p'] . '<br />';
          $u .= $value['u'] . '<br />';
          $p_csv .= $value['p'] . ' / ';
          $u_csv .= $value['u'] . ' / ';
        }
      } else {
        $p = $this->rankings[$keyword][0]['p'];
        $u = $this->rankings[$keyword][0]['u'];
        $p_csv = $p;
        $u_csv = $u;
      }
      $p_csv = rtrim($p_csv, ' / ');
      $u_csv = rtrim($u_csv, ' / ');
      $u_adwords = $this->rankings[$keyword][0]['u'];

      $marker = '';
      $rev = 'N/V';
      if ($visitsAdwords[$keyword]['rev'] > 0 && $visitsOrganic[$matchUrl]['ga:transactionRevenue'] <= 0) {
        $rev = 'SEA';
      } else if ( $visitsAdwords[$keyword]['rev'] <= 0 && $visitsOrganic[$matchUrl]['ga:transactionRevenue'] > 0 ) {
        $rev = 'SEO';
      } else if ( $visitsAdwords[$keyword]['rev'] > 0 && $visitsOrganic[$matchUrl]['ga:transactionRevenue'] > 0) {
        $rev = 'Beide';
        $marker = 'background:#f6e1e1;';
      }

      $source = '';
      if (isset($visitsSearchC[$keyword]) && isset($visitsAdwords[$keyword])) {
        $source = 'Beide';
      } else if (isset($visitsAdwords[$keyword])) {
        $source = 'SEA';
      } else if (isset($visitsSearchC[$keyword])) {
        $source = 'SEO';
      } else {
        $source = 'SEO';
      }

      $clipboard = $keyword;
      if (isset($this->rankings[$keyword][0]['u'])) {
        $clipboard .= '; ' .$this->rankings[$keyword][0]['u'];
      }

      $tablebody .= '<tr style="'.$marker.'" >';
      $tablebody .= '<td><input id="label_'.$i.'" class="selectkw" type="checkbox" name="selectkw" value="' . $clipboard . '"></td>';
      $tablebody .= '<td><label class="labelplain" for="label_'.$i.'">' . $keyword . '</label></td>';
      $tablebody .= '<td class="nowrap">' . strlen($keyword) . '</td>';
      $tablebody .= '<td class="nowrap">'.$source.'</td>';
      $tablebody .= '<td class="nowrap">'.$rev.'</td>';
      $tablebody .= '<td class="gray"></td>';
      $tablebody .= '<td class="nowrap">' . $visitsAdwords[$keyword]['matchtype'] . '</td>';
      $tablebody .= '<td>' . $visitsAdwords[$keyword]['imp'] . '</td>';
      $tablebody .= '<td>' . $visitsAdwords[$keyword]['clicks'] . '</td>';
      $tablebody .= '<td class="nowrap" data-order="'.$visitsAdwords[$keyword]['cost'].'">' . number_format($visitsAdwords[$keyword]['cost'], 2, ',', '.') . ' €</td>';
      $tablebody .= '<td class="nowrap" data-order="'.$visitsAdwords[$keyword]['rev'].'">' . number_format($visitsAdwords[$keyword]['rev'], 2, ',', '.') . ' €</td>';
      $tablebody .= '<td class="nowrap" data-order="'.$visitsAdwords[$keyword]['cpc'].'">' . number_format($visitsAdwords[$keyword]['cpc'], 2, ',', '.') . ' €</td>';

      $tablebody .= '<td>' . $visitsAdwords[$keyword]['url'] . '</td>';
      $tablebody .= '<td class="gray"></td>';
      $tablebody .= '<td>' . $visitsOrganic[$matchUrl]['ga:pageviews'] . '</td>';
      $tablebody .= '<td>' . $visitsOrganic[$matchUrl]['ga:organicSearches'] . '</td>';
      $tablebody .= '<td class="nowrap" data-order="'.$visitsOrganic[$matchUrl]['ga:transactionRevenue'].'">' . number_format($visitsOrganic[$matchUrl]['ga:transactionRevenue'], 2, ',', '.') . ' €</td>';
      $tablebody .= '<td class="nowrap data-order="'.$visitsOrganic[$matchUrl]['ga:pageValue'].'">' . number_format($visitsOrganic[$matchUrl]['ga:pageValue'], 2, ',', '.') . ' €</td>';
      $tablebody .= '<td>' . $p . '</td>';
      $tablebody .= '<td>' . $u . '</td>';
      $tablebody .= '<td class="gray"></td>';
      $tablebody .= '<td>' . $visitsSearchC[$keyword]['imp'] . '</td>';
      $tablebody .= '<td>' . $visitsSearchC[$keyword]['clicks'] . '</td>';
      $tablebody .= '<td class="nowrap" data-order="'.$visitsSearchC[$keyword]['ctr'].'">' . $visitsSearchC[$keyword]['ctr'] . '</td>';
      $tablebody .= '<td>' . $visitsSearchC[$keyword]['pos'] . '</td>';
      $tablebody .= '</tr>';

      $this->csvarr[$i] = array(
          $keyword,
          strlen($keyword),
          $source,
          $rev,
          $visitsAdwords[$keyword]['matchtype'],
          $visitsAdwords[$keyword]['imp'],
          $visitsAdwords[$keyword]['clicks'],
          number_format($visitsAdwords[$keyword]['cost'], 2, ',', '.'),
          number_format($visitsAdwords[$keyword]['rev'], 2, ',', '.'),
          number_format($visitsAdwords[$keyword]['cpc'], 2, ',', '.'),
          $visitsAdwords[$keyword]['url'],
          $visitsOrganic[$matchUrl]['ga:pageviews'],
          $visitsOrganic[$matchUrl]['ga:organicSearches'],
          number_format($visitsOrganic[$matchUrl]['ga:transactionRevenue'], 2, ',', '.'),
          number_format($visitsOrganic[$matchUrl]['ga:pageValue'], 2, ',', '.'),
          $p_csv,
          $u_csv,
          $visitsSearchC[$keyword]['imp'],
          $visitsSearchC[$keyword]['clicks'],
          $visitsSearchC[$keyword]['ctr'],
          $visitsSearchC[$keyword]['pos'],
          substr($keyword, 0,29),
          $u_adwords
        );

    }

    $tablebody .= '</tbody>';
    $tablebody .= '</table></div></div>';

    $tableintercept = '<tr>
        <td class="nowrap cumm" colspan="5"> SUMME </td>
        <td class="gray"></td>
        <td class="nowrap cumm"></td>
        <td class="nowrap cumm">'.number_format($sum_imp, 0, ',', '.').'</td>
        <td class="nowrap cumm">'.number_format($sum_clicks, 0, ',', '.').'</td>
        <td class="nowrap cumm">'.number_format($sum_cost, 2, ',', '.').' €</td>
        <td class="nowrap cumm">'.number_format($sum_rev, 2, ',', '.').' €</td>
        <td class="nowrap cumm">'.number_format($sum_cpc / count($visitsAdwords), 2, ',', '.').' € (&#216;)</td>
        <td class="nowrap cumm"> </td>
        <td class="gray"></td>
        <td class="nowrap cumm">'.number_format($sum_seo_pv, 0, ',', '.').'</td>
        <td class="nowrap cumm">'.number_format($sum_seo_cl, 0, ',', '.').'</td>
        <td class="nowrap cumm">'.number_format($sum_seo_rev, 2, ',', '.').' €</td>
        <td class="nowrap cumm">'.number_format($sum_seo_pava, 2, ',', '.').' € (ALL)</td>
        <td class="nowrap cumm">  </td>
        <td class="nowrap cumm">  </td>
        <td class="gray"></td>
        <td class="nowrap cumm">'.number_format($sum_sc_imp, 0, ',', '.').'</td>
        <td class="nowrap cumm">'.number_format($sum_sc_clicks, 0, ',', '.').'</td>
        <td class="nowrap cumm">'.number_format($sum_sc_ctr / $sc_divider_ctr, 0, ',', '.').'%</td>
        <td class="nowrap cumm">'.number_format($sum_sc_pos / $sc_divider_pos, 0, ',', '.').' (&#216;)</td>
      </tr></thead><tbody>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'balancer');

    $tablehead = '
    <div class="box">
      <div class="box-header">
        <span class="titleleft">Domain: '.$this->url.' | Zeitraum: '.$this->timestart_readable.' bis '.$this->timeend_readable.' </span>
        <ul class="box-toolbar">
          <li><a target="_blank" href="../../../overview/'.$this->id.'/'.$this->timestart_readable.'/'.$this->timeend_readable.'"><span class="label label-dark-blue">Alle Keywords</span></a></li>
          <li><a target="_blank" href="../../../sea-vs-seo/'.$this->id.'/'.$this->timestart_readable.'/'.$this->timeend_readable.'"><span class="label label-blue">SEO Team</span></a></li>
          <li><a target="_blank" href="../../../seo-vs-sea/'.$this->id.'/'.$this->timestart_readable.'/'.$this->timeend_readable.'"><span class="label label-green">SEA Team</span></a></li>
          <li><a target="_blank" href="../../../brand/'.$this->id.'/'.$this->timestart_readable.'/'.$this->timeend_readable.'"><span class="label label-red">Inkl. Brand Keywords</span></a></li>
          <li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSEA | SEA/SEO Balancer | Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li>
        </ul>
      </div>
      <div class="box-content padded">
      <p>
        <input style="display:none;" id="selectall" type="checkbox">
        <button class="btn btn-mini btn-default"><label style="margin:0; font-size:11px;" for="selectall">Alle Keywords auswählen</label></button>
        <button id="copytoclipboard" class="btn btn-mini btn-default">Markierte Keywords in die Zwischenablage kopieren</button>
        <br /><br />
        Toggle:
        <a data-column="2" class="toggle-vis" data-toggle="modal">Zeichen</a> |<a data-column="3" class="toggle-vis" data-toggle="modal">Quelle</a> | <a data-column="4" class="toggle-vis" data-toggle="modal">Umsatzquelle</a> | <a data-column="6" class="toggle-vis" data-toggle="modal">SEA Match Type</a> | <a data-column="7" class="toggle-vis" data-toggle="modal">SEA Impressions</a> | <a data-column="8" class="toggle-vis" data-toggle="modal">SEA Clicks</a> | <a data-column="11" class="toggle-vis" data-toggle="modal">SEA CPC</a> | <a data-column="12" class="toggle-vis" data-toggle="modal">SEA URL</a> | <a data-column="14" class="toggle-vis" data-toggle="modal">SEO PageViews</a> | <a data-column="15" class="toggle-vis" data-toggle="modal">SEO Clicks</a> | <a data-column="16" class="toggle-vis" data-toggle="modal">SEO Umsatz</a> | <a data-column="17" class="toggle-vis" data-toggle="modal">SEO PageValue</a> | <a data-column="19" class="toggle-vis" data-toggle="modal">SEO URL</a> | <a data-column="21" class="toggle-vis" data-toggle="modal">GSC Impressions</a> | <a data-column="22" class="toggle-vis" data-toggle="modal">GSC Clicks</a> | <a data-column="23" class="toggle-vis" data-toggle="modal">GSC CTR</a>
        <br /><br />
        Hinweis: <b>'.$hint.'.</b>
        </p>
      </div>
      <div class="box-content" style="overflow:scroll;">
        <table class="table table-normal data-table">
          <thead>
            <tr>
              <td>#</td>
              <td>Keyword</td>
              <td>Zeichen</td>
              <td>Quelle</td>
              <td>Umsatzquelle</td>
              <td class="gray"></td>
              <td>SEA Match Type</td>
              <td>SEA Impressions<br />
                <input class="rangefilter colsearch" type="text" id="min7" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max7" name="max" placeholder="max.">
              </td>
              <td>SEA Clicks<br />
                <input class="rangefilter colsearch" type="text" id="min8" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max8" name="max" placeholder="max.">
              </td>
              <td>SEA Kosten<br />
                <input class="rangefilter colsearch" type="text" id="min9" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max9" name="max" placeholder="max.">
              </td>
              <td>SEA Umsatz<br />
                <input class="rangefilter colsearch" type="text" id="min10" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max10" name="max" placeholder="max.">
              </td>
              <td>SEA CPC</td>
              <td>SEA URL</td>
              <td class="gray"></td>
              <td>SEO PageViews<br />
                <input class="rangefilter colsearch" type="text" id="min14" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max14" name="max" placeholder="max.">
              </td>
              <td>SEO Clicks<br />
                <input class="rangefilter colsearch" type="text" id="min15" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max15" name="max" placeholder="max.">
              </td>
              <td>SEO Umsatz<br />
                <input class="rangefilter colsearch" type="text" id="min16" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max16" name="max" placeholder="max.">
              </td>
              <td>SEO PageValue<br />
                <input class="rangefilter colsearch" type="text" id="min17" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max17" name="max" placeholder="max.">
              </td>
              <td>Rank (heute)<br />
                <input class="rangefilter colsearch" type="text" id="min18" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max18" name="max" placeholder="max.">
              </td>
              <td>SEO URL</td>
              <td class="gray"></td>
              <td>GSC Impressions<br />
                <input class="rangefilter colsearch" type="text" id="min21" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max21" name="max" placeholder="max.">
              </td>
              <td>GSC Clicks<br />
                <input class="rangefilter colsearch" type="text" id="min22" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max22" name="max" placeholder="max.">
              </td>
              <td>GSC CTR (%)<br />
                <input class="rangefilter colsearch" type="text" id="min23" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max23" name="max" placeholder="max.">
              </td>
              <td>GSC Rank<br />
                <input class="rangefilter colsearch" type="text" id="min24" name="min" placeholder="min.">
                <input class="rangefilter colsearch" type="text" id="max24" name="max" placeholder="max.">
              </td>
            </tr>
          ';

    echo $tablehead . $tableintercept . $tablebody;

  }



  private function gaQueryAdwords ()
  {

    $filename = $this->filename . 'ga-sea.tmp';
    $arr = $this->cache->readFile ($filename, 24);

    if (!empty($arr)) {
      //return $arr;
    }

    $this->gaauth();

    $visits    = array();

    $startdate = $this->timestart;
    $enddate   = $this->timeend;

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate
    );

    $this->ga->setDefaultQueryParams($this->selected_date);

    $index = array (1, 10000);

    foreach ($index as $startindex) {

	    $params_all = array(
	      'dimensions'  => 'ga:adMatchedQuery,ga:adKeywordMatchType,ga:adDestinationUrl',
	      'metrics'     => 'ga:impressions,ga:adClicks,ga:adCost,ga:transactionRevenue,ga:CPC',
	      //'filters'     => 'ga:adClicks>0',
	      'sort'        => '-ga:adClicks',
	      'start-index' => $startindex,
	      'max-results' => 10000
	    );

      $dataset[]  = $this->ga->query($params_all);

    }

    if (!isset($dataset[0]['rows'])) {
      echo '<div class="box"><div class="box-header"><span class="title">Fehler</span></div><div class="box-content padded">Es besteht kein Zugriff mehr auf das Google Analytics Konto: <b>'.$this->ga_account.'</b></div></div>';
      exit;
    }


    $gaad = array();

    $i=0;

$cl  = 0;
$co = 0;
$rev = 0;

    foreach ($dataset as $key => $data) {

    	if (!isset($data['rows'])) {continue;}

	    foreach ($data['rows'] as $row) {


$cl = $cl + $row[4];
$co = $co + $row[5];
$rev = $rev + $row[6];

	    	echo $i++ . ' - ' . $row[0] . ' - Clicks: ' . $row[4] . ' - COST: ' . $row[5]. ' - REV: ' . $row[6];
	    	echo '<br />';

	      $gaad[$row[0]] = array(
	        'matchtype' => $row[1],
	        'url' 			=> $row[2],
	        'imp' 			=> $row[3],
	        'clicks' 		=> $row[4],
	        'cost'		  => $row[5],
	        'rev' 			=> $row[6],
	        'cpc' 			=> $row[7]
	      );

	    }

	  }


echo '<br /> KLICKS: ';
echo ($cl);
echo '<br /> KOSTEN: ';
var_dump($co);
echo '<br /> UMSATZ: ';
var_dump($rev);



exit;

    $this->cache->writeFile ($filename, $gaad);

    return $gaad;

  }


  private function gaQueryOrganic ()
  {
    $filename = $this->filename . 'ga-seo.tmp';
    $arr = $this->cache->readFile ($filename, 24);
    if (!empty($arr)) {
      return $arr;
    }

    $this->gaauth();

    $visits    = array();
    $startdate = $this->timestart;
		$enddate   = $this->timeend;

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate,
    );

    $this->ga->setDefaultQueryParams($this->selected_date);


    $index = array (1, 10000, 20000, 30000, 40000);

    foreach ($index as $startindex) {

	    $params_seofilter = array(
	      'dimensions'  => 'ga:hostname,ga:landingPagePath',
	      'metrics'     => 'ga:pageValue,ga:transactions,ga:transactionRevenue,ga:pageviews,ga:organicSearches,ga:entrances',
	      'filters'     => 'ga:pageviews!=0;ga:landingPagePath!@?',
	      // non paid search traffic
	      'segment'     => 'gaid::-5',
	      'sort'        => 'ga:landingPagePath',
	      'start-index' => $startindex,
	      'max-results' => 10000
	    );

      $dataset[]  = $this->ga->query($params_seofilter);

    }

		if (!isset($dataset[0]['rows'])) {
      echo 'GOOGLEANALYTICS SEO ERROR';
      exit;
    }

    $result_array = array();

    foreach ($dataset as $key => $data) {

    	if (!isset($data['rows'])) {continue;}

    	foreach ($data['rows'] as $row) {

      	foreach ($row as $cell => $val) {

	        if ($cell == 0) {

	          $host = $val;

	        } elseif ($cell == 1) {

	          if (stripos($val, 'www.') === 0 ) {
	            $url = $val;
	          } else {
	            $url = $host . $val;
	          }

	          $url = str_replace('index.php', '', $url);

	          $result_array[$url] = array('url' => $url);

	          // create proper path
	          if (stripos($val, 'not set') !== false) {
	            $path = $host;
	          } elseif (stripos($val, $host) === 0) {
	            $path = $val;
	          } else {
	            $path = $host . $val;
	          }

	          $result_array[$url]['path'] = $path;

	        } elseif ($cell == 2) {

	          if (isset($result_array[$url]['ga:pageValue'])) {
	            $result_array[$url]['ga:pageValue'] = $result_array[$url]['ga:pageValue'] + $val;
	          } else {
	            $result_array[$url]['ga:pageValue'] = $val;
	          }

	        } elseif ($cell == 3) {

	          if (isset($result_array[$url]['ga:transactions'])) {
	            $result_array[$url]['ga:transactions'] = $result_array[$url]['ga:transactions'] + $val;
	          } else {
	            $result_array[$url]['ga:transactions'] = $val;
	          }

	        } elseif ($cell == 4) {

	          if (isset($result_array[$url]['ga:transactionRevenue'])) {
	            $result_array[$url]['ga:transactionRevenue'] = $result_array[$url]['ga:transactionRevenue'] + $val;
	          } else {
	            $result_array[$url]['ga:transactionRevenue'] = $val;
	          }

	        } elseif ($cell == 5) {

	          if (isset($result_array[$url]['ga:pageviews'])) {
	            $result_array[$url]['ga:pageviews'] = $result_array[$url]['ga:pageviews'] + $val;
	          } else {
	            $result_array[$url]['ga:pageviews'] = $val;
	          }

	        } elseif ($cell == 6) {

	          if (isset($result_array[$url]['ga:organicSearches'])) {
	            $result_array[$url]['ga:organicSearches'] = $result_array[$url]['ga:organicSearches'] + $val;
	          } else {
	            $result_array[$url]['ga:organicSearches'] = $val;
	          }

	        }

	      }

	    }

    }

    $this->cache->writeFile ($filename, $result_array);

    return $result_array;

  }


  private function scQuery ()
  {

    $filename = $this->filename . 'sc-seo.tmp';
    $arr = $this->cache->readFile ($filename, 24);

    if (!empty($arr)) {
      return $arr;
    }

    $this->scauth();

    $profiles = $this->sc->getProfiles();

    $urls = array();
    foreach ($profiles['siteEntry'] as $item) {
      if (stripos($item['siteUrl'], $this->hostname) !== FALSE) {
        $parsed = parse_url($item['siteUrl']);
        $urls[] = $parsed['scheme'] .'://'.$parsed['host'].'/';
      }
    }

    $visits = array();
    if (empty($urls)) {
      return $visits;
    }

    // HTTPS first
    usort($urls, function($a, $b) {
      return strlen($b) - strlen($a);
    });

    $visits    = array();

    $startdate = $this->timestart;
		$enddate   = $this->timeend;

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate,
    );

    $rows = array (0, 5001, 10002);

    foreach ($rows as $startRow) {
      $visits[]  = $this->sc->getData($urls[0], $startdate, $enddate, 5000, $startRow);
    }

    $sc = array();
    foreach ($visits as $key => $set) {
      if (count($set['rows']) < 1) {continue;}
      foreach ($set['rows'] as $key => $set) {
        $kw          = $set['keys'][0];
        $clicks      = $set['clicks'];
        $impressions = $set['impressions'];
        if ($clicks <= 0) {continue;}
        $ctr         = round($set['ctr']*100, 2);
        $position    = round($set['position'], 2);

        $sc[$kw] = array('imp' => $impressions, 'clicks' => $clicks, 'ctr' => $ctr, 'pos' => $position);
      }
    }

    $this->cache->writeFile ($filename, $sc);

    return $sc;

  }


  private function getRankingData($visitsAdwords, $visitsSearchC)
  {

    foreach ($visitsSearchC as $keyx => $value) {
      $key = str_replace('"', '', $keyx);
      $this->searchkeywords[$key] = $key;
    }

    foreach ($visitsAdwords as $keyx => $value) {
      $key = str_replace('"', '', $keyx);
      $this->searchkeywords[$key] = $key;
    }

    $this->searchkeywords = array_values($this->searchkeywords);

    $filename = 'balancer-'.$this->id.'-rankings.tmp';
    $this->rankings = $this->cache->readFile ($filename, 12);
    if (!empty($this->rankings)) {
      return;
    }

    $this->rankings  = array();

    $comma_separated_kws = implode('", "', $this->searchkeywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$this->lang'
            AND
              DATE (timestamp) = CURDATE()";

    $result = $this->db->query($sql);

    $rows_kw = array();

    if (!$result) {
      return;
    }

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
      $this->kw_in_db[$row['keyword']] = $row['keyword'];
      $timestamp = strtotime($row['timestamp']);
      $this->rankings[$row['keyword']] = array('p' => false, 'u' => false, 'ts' => $timestamp);
    }

    if (count($rows_kw) == 0) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    while ($row = $result2->fetch_assoc()) {

      if ($row['hostname'] == $this->hostname) {
        $timestamp = strtotime($rows_kw[$row['id_kw']]['timestamp']);
        $this->rankings[$rows_kw[$row['id_kw']]['keyword']][] = array('p' => $row['position'], 'u' => $row['url'], 'ts' => $timestamp);
      }

    }

    $this->cache->writeFile ($filename, $this->rankings);

  }


  private function gaauth ()
  {

    $this->ga  = parent::AnalyticsAuth();
    $auth      = $this->ga->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'GOOGLEANALYTICS: NO AUTH TOKEN CREATED';
      exit;
    }

    $this->ga->setAccessToken($accessToken);
    $this->ga->setAccountId($this->ga_account);

  }


  private function scauth ()
  {

    $this->sc  = parent::SearchConsoleAuth();
    $auth      = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'GOOGLESC: NO AUTH TOKEN CREATED';
      exit;
    }

    $this->sc->setAccessToken($accessToken);

  }


  private function showInput () {

    $earlydate     = date('d.m.Y', strtotime('-91 days'));
    $yesterday     = date('d.m.Y', strtotime('-1 days'));
    $startdate_out = $earlydate;

    $datepicker = 'true';

    echo '
    <div class="box">
      <div class="box-header">
        <span class="title">Maximaler Zeitraum: letze 90 Tage | '.$this->customername.'</span>
      </div>
      <div class="box-content">
	      <br />
	      <form class="form-horizontal fill-up" role="form" id="ops_date_compare">
	        <div class="form-group">
	          <label for="name" class="col-sm-2 label-1 control-label">Datum 1:</label>
	          <div class="col-sm-8">
	            <input class="datepicker_ruk fill-up" required="required" type="text" name="date1" placeholder="Frühestes Datum: '.$earlydate.'" value="' . $startdate_out .'">
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="name" class="col-sm-2 label-1 control-label">Datum 2:</label>
	          <div class="col-sm-8">
	            <input class="datepicker_ruk fill-up" required="required" type="text" name="date2" placeholder="Spätestes Datum: ' .  $yesterday .'" value="' .  $yesterday .'">
	          </div>
	        </div>
	        <div class="form-group">
	          <div class="col-sm-2 control-label"></div>
	          <div class="col-sm-8">
	            <button type="submit" class="btn btn-primary btn-lg" id="ops_submit">auswählen</button>
	          </div>
	        </div>
	      </form>
	      <script>
	        $(".datepicker_ruk").datepicker({
	          format: "dd.mm.yyyy",
	          autoclose: true,
	          startDate: "'.$earlydate.'",
	          endDate: "+0d",
	          beforeShowDay: function(dx) {
	            var day = dx.getDay();
	            if (day != 1) {
	              return '.$datepicker.';
	            } else {
	              return true;
	            }
	          }
	        });
	      </script>
     	</div>
		</div>';
  }


}

?>
