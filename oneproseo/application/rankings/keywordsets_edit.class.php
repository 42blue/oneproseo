<?php

require_once('ruk.class.php');

class keywordsets_edit extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->rankingspy = new rankingspy();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['action']) && isset($_POST['id'])) {

      // KEYWORD CAMPAIGN OR URL CAMPAIGN
      if (!isset($_POST['type'])) {
        $this->type = 'key';
      } else {
        $this->type = $_POST['type'];
      }

      $action              = $_POST['action'];
      $this->id            = $_POST['id'];
      $this->customer_view = $_POST['customer_view'];

      if ($action == 'show') {
        $this->getProjectData();
        $this->renderView();
      }

      if ($action == 'add') {
        $this->mo_type     = $_POST['mo_type'];
        $this->kw_set_name = $_POST['name'];
        $this->interval    = $_POST['interval'];
        $this->addKeywordSet();
        echo json_encode($this->out);
      }

      if ($action == 'delete') {
        $this->removeKeywordSet();
        echo json_encode($this->out);
      }

    }


  }


  private function renderView ()
  {

    echo '

          <div class="box">
            <div class="box-header">
              <span class="title">Vorhandene Campaigns</span>
            </div>
            <div class="box-content">
              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Type</td>';
              if ($this->env_data['customer_version'] == 0) {
                echo '<td>Anzeige Kundenversion</td>';
              }
              echo '<td>Anzahl Keywords / URLs</td>
                    <td>Typ</td>
                    <td>Interval</td>
                    <td>Erstellt von</td>
                    <td>Datum</td>
                    <td>bearbeiten</td>
                    <td>Tags</td>';

              if ($this->env_data['customer_version'] == 0) {
                echo '<td>kopieren</td>';
              }

    echo '<td>löschen</td></thead><tbody>';

    echo $this->out_view;

    echo '</tbody></table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="title">Neue Campaign hinzufügen</span>
                </div>
                <div class="box-content padded">
                <form class="form-horizontal fill-up" role="form" id="ops_add_keywordset">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Type:</label>
                    <div class="col-sm-8">
                      <div class="crawl-input-radio">
                        <input type="radio" name="type" value="key" class="icheck OPS_type" checked="checked" id="iradio1">
                        <label for="iradio1">Keyword Campaign</label>
                      </div>
                      <div class="crawl-input-radio">
                        <input type="radio" name="type" value="url" class="icheck OPS_type" id="iradio2">
                        <label for="iradio2">URL Campaign</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label">Überwachungsinterval:</label>
                    <div class="col-sm-8">
                      <div class="crawl-input-radio">
                        <input type="radio" name="measuring" value="daily" class="icheck OPS_interval" checked="checked" id="iradio5">
                        <label for="iradio5">täglich</label>
                      </div>
                      <div class="crawl-input-radio">
                        <input type="radio" name="measuring" value="weekly" class="icheck OPS_interval" id="iradio6">
                        <label for="iradio6">wöchentlich (Anzeige Montags)</label>
                      </div>
                    </div>
                  </div>';

              if ($this->env_data['customer_version'] == 0) {
                  echo '<div class="form-group">
                    <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                    <div class="col-sm-8">
                      <div class="crawl-input-radio">
                        <input type="radio" name="customer_view" value="0" class="icheck OPS_show" checked="checked" id="iradio3">
                        <label for="iradio1">Nein</label>
                      </div>
                      <div class="crawl-input-radio">
                        <input type="radio" name="customer_view" value="1" class="icheck OPS_show" id="iradio4">
                        <label for="iradio2">Ja</label>
                      </div>
                    </div>
                  </div>';
                }

                echo '<div class="form-group">
                    <label for="type" class="col-sm-2 label-1 control-label">Google Version:</label>
                    <div class="col-sm-8">
                      <select class="custom" name="type" id="ops_type">
                        <option value="desktop">Google Desktop</option>
                        <option value="mobile">Google Mobile</option>
                        <option value="maps">Google Maps</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="ops_add_keywordset_name" class="col-sm-2 label-1 control-label">Name:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="ops_add_keywordset_name" name="name" required="required" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2 control-label"></div>
                    <div class="col-sm-8">
                      <button type="submit" class="btn btn-primary btn-lg" id="ops_add_keywordset_submit">Campaign hinzufügen</button>
                    </div>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
         ';

  }



  private function getProjectData()
  {

    $this->out_view = '';

    // get all projects
    // get all project keyword set
    // get all project keyword set keywords
    // count keywords

    $sql = "SELECT
              a.id             AS aid,
              a.name           AS project,
              b.id             AS bid,
              b.name           AS setname,
              b.type           AS type,
              b.rs_type        AS mo_type,
              b.customer_view  AS customer_view,
              b.rankingspy_id  AS rankingspy_id,
              b.measuring      AS measuring,
              b.created_by     AS created_by,
              b.created_on	   AS created_on,
              count(DISTINCT c.keyword) AS kwcount,
              count(DISTINCT d.url)     AS urlcount
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON c.id_kw_set = b.id
              LEFT JOIN ruk_project_urls d
                ON d.id_kw_set = b.id
            WHERE a.id = '".$this->id."'
            GROUP BY bid";

    $result = $this->db->query($sql);

    if (!$result) {
      die ('Etwas stimmte mit dem Query nicht: ' . $db->error);
    }


    while ($row = $result->fetch_assoc()) {

      if ($row['customer_view'] == 1) {
        $show_customer = 'Ja';
      } else {
        $show_customer = 'Nein';
      }

      if (stripos($row['measuring'], 'daily') !== FALSE) {
        $show_interval = 'täglich';
      } else {
        $show_interval = 'wöchentlich';
      }

      if (stripos($row['mo_type'], 'maps') !== FALSE) {
        $show_measuring = 'Google Maps';
      } else if (stripos($row['mo_type'], 'mobile') !== FALSE) {
        $show_measuring = 'Google Mobile';
      } else {
        $show_measuring = 'Google Desktop';
      }

			$sort_date = str_replace('-', '', $row['created_on']);

      if (!is_null($row['setname'])) {
        // KEYWORD CAMPAIGN OR URL CAMPAIGN
        if ($row['type'] == 'key') {
          $this->out_view .= '<tr><td data-order="' . trim($row['setname']) . '"><a href="detail-keyword/'.$row['bid'].'">' . $row['setname'] . '</a></td><td>Keyword Campaign</td>';

          if ($this->env_data['customer_version'] == 0) {
            $this->out_view .= '<td> ' . $show_customer . '</td>';
          }

          $this->out_view .= '<td>' . $row['kwcount'] . '</td><td>' . $show_measuring .'</td><td>' . $show_interval . '</td><td> ' . $row['created_by'] . '</td><td data-order="'.$sort_date.'"> ' . $this->germanTSred($row['created_on']) . '</td><td><a target="_blank" href="keywords/'.$row['bid'].'"><span class="label label-red-super">Keywords bearbeiten</span></a></td><td><a target="_blank" href="tags/'.$row['bid'].'"><span class="label label-blue">Tags</span></a></td>';

          if ($this->env_data['customer_version'] == 0) {
            $this->out_view .= '<td><a target="_blank" href="keywords-copy/'.$row['bid'].'"><span class="label label-green">kopieren</span></a></td>';
          }

          $this->out_view .= '<td><div data-setid="'.$row['bid'].'" class="btn-group"><button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button><ul class="dropdown-menu"><li><a href="#" class="ops_delete_keywordset" data-kwsetid="'.$row['bid'].'">Campaign löschen</a></li></ul></div></td></tr>';

        } else {

          $this->out_view .= '<tr><td data-order="' . trim($row['setname']) . '">' . $row['setname'] . '</td><td>URL Campaign</td>';

          if ($this->env_data['customer_version'] == 0) {
            $this->out_view .= '<td> ' . $show_customer . '</td>';
          }

          $this->out_view .= '<td> ' . $row['urlcount'] . '</td><td> ' . $show_measuring . '</td><td> ' . $show_interval . '</td><td> ' . $row['created_by'] . '</td><td data-order="'.$sort_date.'"> ' . $this->germanTSred($row['created_on']) . '</td><td><a target="_blank" href="keywords/'.$row['bid'].'"><span class="label label-red-super">URLs bearbeiten</span></a><td></td></td>';
          if ($this->env_data['customer_version'] == 0) {
            $this->out_view .= '<td><a target="_blank" href="keywords-copy/'.$row['bid'].'"><span class="label label-green">kopieren</span></a></td>';
          }
          $this->out_view .= '<td><div data-setid="'.$row['bid'].'" class="btn-group"><button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button><ul class="dropdown-menu"><li><a href="#" class="ops_delete_keywordset" data-kwsetid="'.$row['bid'].'">Campaign löschen</a></li></ul></div></td></tr>';
        }

      }
    }

  }


  private function addKeywordSet ()
  {

    $project_id  = $this->id;
    $keyword_set = strip_tags($this->kw_set_name);

    $sql = "SELECT country FROM ruk_project_customers WHERE id = '".$project_id."'";
    $result = $this->db->query($sql);
    while ($row = $result->fetch_assoc()) {
      $country = $row['country'];
    }

    $rs_type = $this->mo_type;

    // max 55 keywordsets per customer
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE id_customer='".$project_id."'";
    $exists = $this->db->query($sql);

    if ($exists->num_rows >= $this->env_data['ruk_max_keywordsets']) {
      $this->out = array ('error' => 'Maximal ' . $this->env_data['ruk_max_keywordsets'] . ' Campaigns pro Projekt!');
      return;
    }

    // check if keywordset name exists
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE name='".$keyword_set."' AND id_customer='".$project_id."' ";
    $exists = $this->db->query($sql);

    if ($exists->num_rows > 0) {
      $this->out = array ('error' => 'Campaign existiert bereits!');
      return;
    }

    $created_by = $_SESSION['firstname'] .' '. $_SESSION['lastname'];

    // add new keyword set to db
    $query = "INSERT INTO
                ruk_project_keyword_sets (id_customer, name, type, rs_type, customer_view, measuring, created_by, created_on)
              VALUES
                ('" . $project_id . "', '" . $keyword_set . "', '" . $this->type . "', '" . $rs_type . "', '" . $this->customer_view . "', '" . $this->interval . "', '" . $created_by . "', '" . $this->dateYMD() . "')";

    $this->db->query($query);

    if (isset($this->db->insert_id)) {
      $this->out = array ('success' => 'Campaign hinzugefügt');
    }

  }


  private function removeKeywordSet ()
  {

    $kw_set_id = $this->id;

    $sql = "SELECT rankingspy_id FROM ruk_project_keyword_sets WHERE id='".$this->id."'";
    $result = $this->db->query($sql);
    $ret = $result->fetch_array();

    // RANKING SPY
    if ($ret['rankingspy_id'] != 0) {
      $ret = $this->rankingspy->removeKeywordSet($ret['rankingspy_id']);
    }

    // remove project keyword set
    // remove project keyword set keywords

    $query = "DELETE a.*, b.*
              FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
              ON b.id_kw_set = a.id
              WHERE a.id ='".$kw_set_id."'";

    $this->db->query($query);

    if ($this->db->affected_rows > 0) {
      $this->out = array ('success' => 'Campaign gelöscht!');
    } else {
      $this->out = array ('error' => 'Campaign konnte nicht gelöscht werden.');
    }


  }

}

?>
