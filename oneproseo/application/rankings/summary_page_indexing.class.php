<?php

require_once('ruk.class.php');

class summary_page_indexing extends ruk
{

  private $out_view = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () 
  {

      $this->fetchCustomerData();
      $this->fetchSitesInIndex();

      $this->getProjectDataDashboardRankingsShort();

      echo $this->dashboardHint();

      echo $this->out_view;

  }


  private function fetchCustomerData()
  {

    // get all projects
    // get all project keyword set
    // get all project keyword set keywords
    // count kw sets
    // count keywords

    $sql = "SELECT a.id              AS mid,
                   a.name            AS project,
                   a.url             AS url,
                   count(c.keyword)  AS kwcount
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON c.id_kw_set = b.id
            GROUP BY project";

    $this->result = $this->db->query($sql);

    while ($row = $this->result->fetch_assoc()) {
      $this->customers[$row['url']] = $row;
    }

  }



  private function fetchSitesInIndex()
  {

    $sql = "SELECT 
              id,
              id_customer,
              gen_results_host,
              gen_results_url, 
              api_results_host, 
              api_results_url,
              timestamp
            FROM 
              ruk_project_sites_index
            WHERE
              DATE(timestamp) > CURDATE() - INTERVAL 7 DAY
            AND 
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            ORDER BY timestamp DESC";

    $this->result = $this->db->query($sql);

    while ($row = $this->result->fetch_assoc()) {
      $this->timestamps[$row['timestamp']] = $row['timestamp'];
      $this->sitesinindex[$row['id_customer']][$row['timestamp']] = array($row['gen_results_host'], $row['gen_results_url'], $row['api_results_host'], $row['api_results_url']);
    }

  }


  private function dashboardHint() 
  {


    echo '<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">×</button><p><strong>Hinweis:</strong></p>
            <p>Die angezeigten Seiten im Google Index können von Tag zu Tag leicht abweichen.</p>
            <p>Google liefert hier in verschiedenen Datencentern teilweise unterschiedliche Werte aus.</p>
          </div>';

  }


  private function getProjectDataDashboardRankingsShort()
  {

    // TABLE HEADER
    $this->out_view = '<div class="row-fluid"><div class="span6"><div class="box"><div class="box-header"><span class="title">Seiten im Google Index</span></div><div class="box-content">';
    $this->out_view .= '<table class="table table-normal data-table" id="data-table"><thead>';
    $this->out_view .= '<tr><td>#</td><td>URL <small>[SCRAPE]</small></td>';

    $i = 1;
    foreach ($this->timestamps as $ts) {
      $this->out_view .= '<td>Seiten im Google Index <br />' . parent::germanTSred($ts) . '</td>';
      if ($i++ < count($this->timestamps)) {
        $this->out_view .= '<td>Change %</td><td>Change abs.</td>';
      }
    }

    $this->out_view .= '</tr></thead><tbody>';
    // TABLE HEADER

    $difference = array ('SCRAPE URL');

    // TABLE CONTENT
    $c = 1;
    foreach ($this->customers as $url => $set) {

      $k = 0;
      foreach ($difference as $diff) {

        if (stripos($diff, 'hostname') !== FALSE) {
          $u_out = parent::pureHostName($set['url']);
        } else {
          $u_out = parse_url($set['url'], PHP_URL_HOST);;
        }

        $this->out_view .= '<tr><td> ' . $c++ . ' </td><td><a href="../rankings/overview/' . $set['mid'] . '">' . $u_out . '</a></td>';

        $h = 1;
        foreach ($this->timestamps as $ts) {

          $yesterday = key($this->timestamps);
          next($this->timestamps);

          $host = $u_out;

          $this->out_view .= '<td data-order="'.$this->sitesinindex[$set['mid']][$ts][1].'"><a href="'.parent::googleSearchUrl().'site:'.$host.'" target="_blank">' . number_format($this->sitesinindex[$set['mid']][$ts][1], 0, '', '.') . '</a></td>';
          if ($h++ < count($this->timestamps)) {
            $absolute_change = parent::checkDifferenceBetweenRankingDays($this->sitesinindex[$set['mid']][$ts][1], $this->sitesinindex[$set['mid']][$yesterday][1], false);
            $this->out_view .= '<td class="nowrap">' . parent::calcPercent($this->sitesinindex[$set['mid']][$ts][1], $this->sitesinindex[$set['mid']][$yesterday][1]) . '</td>';
            $this->out_view .= '<td class="nowrap">' . $absolute_change  . '</td>';
          }

        }

        $this->out_view .= '</tr>';

        $k++;

      }

    }
    // TABLE CONTENT

    $this->out_view .= '</tbody></table>';
    $this->out_view .= '</div></div></div></div>';

  }

}

?>