<?php

date_default_timezone_set('CET');

require_once('ruk.class.php');

class summary_dashboard_custom extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['sort'])) {
      $this->newsort = json_decode ($_POST['sort'], TRUE);
      $this->saveSort();
    }

    if (isset($_POST['add'])) {
      $this->newadd[] = json_decode ($_POST['add'], TRUE);
      $this->loadSort();
      $this->newsort = array_merge($this->sort, $this->newadd);
      $this->saveSort();
    }

    if (isset($_POST['show'])) {
      $this->loadSort();
      $this->loadSets();
      echo $this->out;
    }

  }


  private function saveSort ()
  {

    $_SESSION['ruk_personal_dash'] = $this->newsort;

    //$this->newsort = array();
    $data   = serialize($this->newsort);
    $userid = $_SESSION['userid'];

    $sql = "UPDATE onepro_users SET ruk_personal_dash = '$data' WHERE id = '$userid'";
    $result = $this->db->query($sql);

  }


  private function loadSort ()
  {

    $userid = $_SESSION['userid'];

    $sql = "SELECT ruk_personal_dash FROM onepro_users WHERE id = '$userid'";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $sort = $row['ruk_personal_dash'];
    }

    $this->sort = unserialize($sort);

    $_SESSION['ruk_personal_dash'] = $this->sort;

  }


  private function loadSets ()
  {

    if (!is_array($this->sort)) {
      echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Es ist ein Fehler aufgetreten. Bitte wende dich an den Administrator.</strong></div>';
      exit;
    }

    if (empty($this->sort)) {
      echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>In diesem Personal Dashboard sind keine Daten vorhanden!</strong></div>';
      exit;
    }

    $this->out = '<div class="row-fluid"><div class="box"><div class="box-header padded"><span class="btn btn-default f12" id="ops_load_seven_graph12m"><i class="icon-bar-chart"></i> 12 Monate OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph12w"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_table"><i class="icon-table"></i> 7 Tage Rankingverteilung laden (alle)</span><span class="btn btn-default f12" id="ops_clear_views"><i class="icon-refresh"></i> Refresh </span></div></div><div id="sortable">';

    $this->orderSet = array();

    foreach ($this->sort as $key => $value) {

      if (empty($value['setid'])) {
        $value['setid'] = 'all';
      }

      // ALL SET / CUSTOMER
      if ($value['setid'] == 'all') {
        $this->project_id = $value['projectid'];
        $this->set_id     = 'all';
        $this->getProjectData();
      // SPECIFIC SET
      } else {
        $this->project_id = $value['projectid'];
        $this->set_id     = $value['setid'];
        $this->getSetData();
      }

    }

    ksort ($this->orderSet);

    foreach ($this->orderSet as $key => $value) {
      $this->set_id      = $value['setid'];
      $this->project_id  = $value['projectid'];
      $this->rs_type     = $value['rs_type'];
      $this->settype     = $value['settype'];
      $this->projectname = $value['projectname'];
      $this->setname     = $value['setname'];
      $this->ukcount     = $value['ukcount'];
      $this->ga_account  = $value['ga_account'];
      $this->measuring   = $value['measuring'];
      $this->created_by  = $value['created_by'];
      $this->created_on  = $value['created_on'];
      $this->render();
    }


    $this->out .= '</div></div>';

  }


  private function render ()
  {

    $settype = array ('all' => 'Keyword / URL Campaign / Google Desktop','url' => 'URL Campaign / Google ' . ucfirst($this->rs_type), 'key' => 'Keyword Campaign / Google ' . ucfirst($this->rs_type));
    $setcolor = array ('all' => 'big-green', 'url' => 'big-blue', 'key' => 'big-green');
    $setident = array ('all' => 'Keywords', 'url' => 'URLs', 'key' => 'Keywords');
    $setcolor2 = array ('all' => 'green', 'url' => 'blue', 'key' => 'green');

    $this->out .= '
      <div class="span6 sortable">
        <div class="box">
          <div class="box-header">
            <span class="'.$setcolor[$this->settype].'">' . $this->projectname . ': ' . $this->setname . '</span> <span class="small"> ' . $settype[$this->settype]  . $this->setInfo() .' </span>
            <ul class="box-toolbar">
              <li><i title="Set vom Personal Dashboard löschen" class="delete-sortable icon-remove-sign"></i></li>
              <li><span class="label label-'.$setcolor2[$this->settype].' f12"><span>'.$this->ukcount.'</span> '.$setident[$this->settype].'</span></li>
            </ul>
          </div>
          <div class="box-content ops_setdata ops_setdata_bg" data-setid="' . $this->set_id . '" data-projectid="' . $this->project_id . '">
          '.$this->chartload().'
          </div>
          <div class="box-footer">
            <ul class="box-toolbar">
              <li><a href="overview/'.$this->project_id.'/detail-keyword/'.$this->set_id.'" target="_blank"><span class="label label-dark-blue f12">Keywords / Rankings</span></a></li>
              <li><a href="overview/'.$this->project_id.'/detail-url/'.$this->set_id.'" target="_blank"><span class="label label-red f12">URL / Keywords (Campaign)</span></a></li>
              <li><a href="overview/'.$this->project_id.'/detail-url-complete/'.$this->set_id.'" target="_blank"><span class="label label-red f12">URL / Keywords (DB)</span></a></li>
              <li><a href="overview/'.$this->project_id.'/detail-url-onedex/'.$this->set_id.'" target="_blank"><span class="label label-blue f12">URL / OneDex (Campaign)</span></a></li>
              <li><a href="overview/'.$this->project_id.'/detail-url-onedex-complete/'.$this->set_id.'" target="_blank"><span class="label label-blue f12">URL / OneDex (DB)</span></a></li>
              <li><a href="overview/'.$this->project_id.'/detail-keyword-url/'.$this->set_id.'" target="_blank"><span class="label label-green f12">Keywords / URL</span></a></li>
              <li><a href="overview/'.$this->project_id.'/detail-compare/'.$this->set_id.'/'.$this->germanTSred($this->created_on).'" target="_blank"><span class="label label-cyan f12">Datumsvergleich</span></a></li>';

    if ($this->settype != 'url') {
      $this->out .= '<li><a href="overview/'.$this->project_id.'/detail-competition/'.$this->set_id.'" target="_blank"><span class="label label-purple f12">Wettbewerbsvergleich</span></a></li>';
    }

    if (!empty($this->ga_account)) {
      $this->out .= '<li><a href="overview/'.$this->project_id.'/detail-url-onedex-analytics-complete/all" target="_blank"><span class="label label-gold f12">Analytics / OneDex</span></a></li>';
    }

    $this->out .='
            </ul>
          </div>
        </div>
      </div>';
  }



  private function getProjectData()
  {

     $sql = "SELECT
              a.id                      AS id,
              a.name                    AS project,
              a.ga_account              AS ga_account,
              count(c.keyword)          AS kwcount,
              count(DISTINCT c.keyword) AS kwcountunique
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON c.id_kw_set = b.id
            WHERE a.id = '$this->project_id'";

    $result = $this->db->query($sql);

    if ($result->num_rows === 0) {
      return false;
    }

    while ($row = $result->fetch_assoc()) {

      $ukcount       = $row['kwcount'];
      $setname       = 'Alle Keywords';
      $settype       = 'all';
      $projectname   = $row['project'];
      $ga_account    = $row['ga_account'];

      $setid = $row['project'] . $row['setname'];

      $this->orderSet[$setid] = array(
        'projectid'   => $this->project_id,
        'setid'       => 'all',
        'setname'     => $setname,
        'settype'     => $settype,
        'projectname' => $projectname,
        'ga_account'  => $ga_account,
        'ukcount'     => $ukcount,
        'rs_type'     => '',
        'measuring'   => '',
        'created_by'  => '',
        'created_on'  => ''
      );

    }

  }


  private function getSetData()
  {

    $sql = "SELECT
              a.id                      AS aid,
              a.name                    AS project,
              a.ga_account              AS ga_account,
              b.id                      AS bid,
              b.name                    AS setname,
              b.rs_type                 AS rs_type,
              b.type                    AS type,
              b.measuring               AS measuring,
              b.created_by              AS created_by,
              b.created_on              AS created_on,
              count(DISTINCT c.keyword) AS kwcount,
              count(DISTINCT d.url)     AS urlcount
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON c.id_kw_set = b.id
              LEFT JOIN ruk_project_urls d
                ON d.id_kw_set = b.id
            WHERE b.id = '$this->set_id'";

    $result = $this->db->query($sql);

    if ($result->num_rows === 0) {
      return false;
    }

    while ($row = $result->fetch_assoc()) {

      if ($row['kwcount'] == 0) {
        $ukcount = $row['urlcount'];
      } else {
        $ukcount = $row['kwcount'];
      }

      $rs_type       = $row['rs_type'];
      $setname       = $row['setname'];
      $settype       = $row['type'];
      $projectname   = $row['project'];
      $projecturl    = $row['url'];
      $ga_account    = $row['ga_account'];
      $measuring     = $row['measuring'];
      $created_by    = $row['created_by'];
      $created_on    = $row['created_on'];

      $setid = $row['project'] . $row['setname'];

      $this->orderSet[$setid] = array(
        'projectid'   => $this->project_id,
        'setid'       => $this->set_id,
        'setname'     => $setname,
        'settype'     => $settype,
        'projectname' => $projectname,
        'ga_account'  => $ga_account,
        'ukcount'     => $ukcount,
        'rs_type'     => $rs_type,
        'measuring'   => $measuring,
        'created_by'  => $created_by,
        'created_on'  => $created_on
      );

    }

  }

  private function setInfo () {

    if (empty($this->measuring)) {
      return '</span>';
    }

    if ($this->measuring == 'daily') {
      $measuring = ' / täglich';
    } else {
      $measuring = ' / wöchentlich';
    }

    if (empty($this->created_by)) {
      $created_by = '';
    } else {
      $created_by = ' / ' . $this->created_by;
    }

    return $measuring . $created_by . ' / ' . $this->germanTSred($this->created_on) . '</span>';

  }

  private function chartload () {

    if ($this->measuring == 'daily') {
      $data = '<div class="ops_select_view"><span class="btn btn-default ops_setload_dex12m"><i class="icon-bar-chart"></i> 12 Monate OneDex laden</span><span class="btn btn-default ops_setload_dex12w"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden</span><span class="btn btn-default ops_setload_dex31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden</span><span class="btn btn-default ops_setload_dex7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden</span><span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Tage Rankingverteilung laden</span></div><div class="ops_show_view" style="display:none;"></div>';
    } else {
      $data = '<div class="ops_select_view"><span class="btn btn-default ops_setload_dex31"><i class="icon-bar-chart"></i> 12 Monate OneDex laden</span><span class="btn btn-default ops_setload_dex7"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden</span><span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Wochen Rankingverteilung laden</span></div><div class="ops_show_view" style="display:none;"></div>';
    }

    return $data;

  }

}

?>
