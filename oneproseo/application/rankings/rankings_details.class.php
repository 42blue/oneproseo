<?php

require_once('ruk.class.php');

/*

Erzeugt die Detailansicht der Keywords und rankenden URLs
https://datatables.net/examples/plug-ins/dom_sort

*/


class rankings_details extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['setid'])) {

      $this->days          = '';
      $this->type          = $_POST['type'];
      $this->keyword_setid = $_POST['setid'];
      $this->days          = $_POST['days'];
      $this->project_id    = $_POST['projectid'];

      // ENVIRONMENT
      $this->measuring = 'daily';
      if (!empty($this->days)) {
        $this->days    = '_31';
      }

      // 1.
      // CAMPAIGN TYPE? URL or KEYWORD
      $this->campaign_type = $this->checkCampainTypeComp($this->keyword_setid);

      // 2.
      // CHOOSE SINGLE KEYWORD SET OR ALL
      if ($this->keyword_setid == 'all' || $this->campaign_type[0] == 'url') {
        $this->selectAllKeywordSets();
      } else {
        $this->selectKeywordSet();
      }

      // INTERCEPT
      // GOOGLE VERSION & COUNTRY
      if ($this->mo_type == 'desktop') {
        $this->mo_type = $this->country;
      } else {
        $this->mo_type = $this->mo_type . '_' . $this->country;
      }

      // 3.
      // IF URL SET FETCH URLS
      if ($this->campaign_type[0] == 'url') {
        $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
      }
      // 4.
      // FETCH RANKING DATA AND OUTPUT

      // output for keyword view
      if ($this->type == 'keyword') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewKeyword();
      }

      // output for url view
      if ($this->type == 'url') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewUrl();
      }

      // output for url view complete database
      if ($this->type == 'complete') {
        $this->getRankingDataComplete(); // <- NO intercept keyword / url -> LOOSE QUERY
        $this->renderRankingViewUrl();
      }

      // output for dex
      if ($this->type == 'dex') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewUrlDex();
      }

      // output for dex
      if ($this->type == 'dexcomplete') {
        $this->getRankingDataComplete(); // <- NO intercept keyword / url -> LOOSE QUERY
        $this->renderRankingViewUrlDex();
      }

      // output for dex
      if ($this->type == 'dexcomplete-analytics') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewUrlDexAnalytics();
      }

      // output for url view
      if ($this->type == 'keyword-url') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewKeywordUrl();
      }

      // output for url view
      if ($this->type == 'keyword-url-duplicates') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewKeywordUrl();
      }

      // output for personalindex
      if ($this->type == 'personalindex') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewPersonalIndex();
      }

      $this->checkForRankingsHint();

      echo $this->out;

    }

  }


  private function selectKeywordSet ()
  {

    $this->keywords = array();

    $selected_keyword_set = $this->keyword_setid;

    $sql = "SELECT a.id               AS setid,
                   a.name             AS setname,
                   a.measuring        AS measuring,
                   a.rs_type          AS rs_type,
                   a.created_on       AS created_on,
                   b.id               AS kwid,
                   b.keyword          AS keyword,
                   b.tags             AS tags,
                   c.url              AS url,
                   c.id               AS customerid,
                   c.ga_account       AS ga_account,
                   c.gwt_account      AS gwt_account,
                   c.name             AS customername,
                   c.country          AS country,
                   c.ignore_subdomain AS ignore_subdomain
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[]   = $row['keyword'];
      $url                = $row['url'];
      $this->setname      = $row['setname'];
      $this->customername = $row['customername'];
      $this->customerid   = $row['customerid'];
      $this->ga_account   = $row['ga_account'];
      $this->gwt_account  = $row['gwt_account']; 
      $this->country      = $row['country'];
      $this->mo_type      = $row['rs_type'];
      $this->ignore_subd  = $row['ignore_subdomain'];
      $this->created_on   = $row['created_on'];
      $this->tags[$row['keyword']] = array($row['kwid'], $row['tags']);
    }

    $this->keyword_amount = count($this->keywords);
    $this->hostname       = parent::pureHostName($url);
    $this->domain         = rtrim($url, '/');

    $loc = $this->getAdwordsLoc($this->country);
    $this->adwords_country  = $loc[0];
    $this->adwords_language = $loc[1];

  }


  private function selectAllKeywordSets ()
  {

    $selected_keyword_set = $this->keyword_setid;
    $selected_project     = $this->project_id;

    $this->keywords = array();

    $sql = "SELECT a.id               AS setid,
                   a.measuring        AS measuring,
                   b.id               AS kwid,
                   b.keyword          AS keyword,
                   b.tags             AS tags,                   
                   a.rs_type          AS rs_type,
                   a.created_on       AS created_on,
                   c.url              AS url,
                   c.id               AS customerid,
                   c.ga_account       AS ga_account,
                   c.gwt_account      AS gwt_account,                   
                   c.name             AS customername,
                   c.country          AS country,
                   c.ignore_subdomain AS ignore_subdomain
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {

      $url = $row['url'];
      $this->customername = $row['customername'];
      $this->customerid   = $row['customerid'];
      $this->ga_account   = $row['ga_account'];
      $this->gwt_account  = $row['gwt_account'];      
      $this->country      = $row['country'];
      $this->mo_type      = 'desktop';
      $this->ignore_subd  = $row['ignore_subdomain'];
      $this->created_on   = $row['created_on'];

      if (!empty($row['keyword'])) {
        $this->keywords[] = $row['keyword'];
        $this->tags[$row['keyword']] = array($row['kwid'], $row['tags']);
      }

    }

    if ($this->campaign_type[0] == 'url') {
      $this->setname = $this->getCampaignName($selected_keyword_set);
    } else {
      $this->setname = 'Alle Keywords';
    }

    $this->keyword_amount = count(array_unique($this->keywords));
    $this->hostname       = parent::pureHostName($url);
    $this->domain         = rtrim($url, '/');

    $loc = $this->getAdwordsLoc($this->country);
    $this->adwords_country  = $loc[0];
    $this->adwords_language = $loc[1];


  }


  private function getRankingData()
  {

    $hostname = $this->hostname;

    $this->measuring  = $this->campaign_type[1];

    if ($this->measuring == 'weekly') {
      $rows = parent::loadCacheFileWeekly($this->customerid);
      $this->is_weekly = true;
    } else {
      $rows = parent::loadCacheFile($this->customerid, $this->days);
      $this->is_weekly = false;
      if (empty($rows)) {
        $rows = parent::loadCacheFileWeekly($this->customerid);
        $this->is_weekly = true;
      }
    }


    // COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (isset($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }
    // COMPARE OPTIONS

    foreach ($rows as $row) {

      if ($this->mo_type != $row['l']) {
        continue;
      }

      // get all measured days
      $this->rankset['_ts'][$row['t']] = $row['t'];

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

      // CLICKS
        $cl = '';
        if (isset($row['cl'])) {
          $cl = $row['cl'];
        }

        if ($this->type == 'keyword' || $this->type == 'keyword-url' || $this->type == 'keyword-url-duplicates' || $this->type == 'personalindex') {
          // collect keyword rankings
          // collect keyword / url rankings
          $this->rankset['_da'][$row['k']][$row['t']][] = array ('rank' => $row['p'], 'ts' => $row['t'], 'url' => $row['u'], 'cl' => $cl);
        } else {
          // collect url rankings
          if (!empty($row['u'])) {
            $this->rankset['_da'][$row['u']][$row['t']][] = array ('keyword' => $row['k'], 'rank' => $row['p'], 'cl' => $cl);
          }
        }

      }

    }

    // ADWORDS, ANALYTICS, GWT DATA
    $this->googleadwords = parent::getAdwordsData($this->keywords, $this->adwords_country, $this->adwords_language);
    $this->googlewebmastertools = parent::getWebmasterToolsData($this->keywords);

    if ($this->type == 'dex' || $this->type == 'dexcomplete' || $this->type == 'dexcomplete-analytics') {
      $this->googleanalyticsAll = parent::getAnalyticsData($hostname);
    }

    $this->checkForRankingsHint();

  }


  // SEARCH THE COMPLETE DATABASE
  private function getRankingDataComplete()
  {

    $hostname = $this->hostname;

    // COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (isset($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }
    // COMPARE OPTIONS

    // get all keyword rankings
    // where hostname

     $sql = "SELECT
              id,
              keyword,
              language
            FROM
              ruk_scrape_keywords
            WHERE
              DATE(timestamp) = CURDATE()";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if ($this->mo_type == $row['language']) {
        $rows[] = $row;
      }
    }

    $nrows = array();

    foreach ($rows as $k => $v) {

      $idkw = $v['id'];

      $sql = "SELECT
                position,
                url,
                hostname
              FROM
                ruk_scrape_rankings
              WHERE
                id_kw = '$idkw'";

      $result2 = $this->db->query($sql);

      while ($row = $result2->fetch_assoc()) {
        if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
          $nrows[] = array('k' => $v['keyword'], 't' => parent::dateYMD(), 'p' => $row['position'], 'u' => $row['url']);
        }
      }

    }

    // CREATE RESULT ARRAY
    foreach ($nrows as $row) {

      // get all measured days
      $this->rankset['_ts'][$row['t']] = $row['t'];

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        if ($this->type == 'keyword' || $this->type == 'keyword-url' || $this->type == 'keyword-url-duplicates' || $this->type == 'personalindex') {
          // collect keyword rankings
          // collect keyword / url rankings
          $this->rankset['_da'][$row['k']][$row['t']][] = array ('rank' => $row['p'], 'ts' => $row['t'], 'url' => $row['u']);
        } else {
          // collect url rankings
          if (!empty($row['u'])) {
            $this->rankset['_da'][$row['u']][$row['t']][] = array ('keyword' => $row['k'], 'rank' => $row['p']);
          }
        }

      }

    }


    // ADWORDS, ANALYTICS, GWT DATA
    $this->googleadwords = parent::getAdwordsData($this->keywords, $this->adwords_country, $this->adwords_language);
    $this->googlewebmastertools = parent::getWebmasterToolsData($this->keywords);

    if ($this->type == 'dex' || $this->type == 'dexcomplete' || $this->type == 'dexcomplete-analytics') {
      $this->googleanalyticsAll = parent::getAnalyticsData($hostname);
    }

    $this->checkForRankingsHint();

  }


  /*
  KEYWORD -> DAILY RANKINGS -> CHANGE
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/23/detail-keyword/93
  */

  private function renderRankingViewKeyword ()
  {

    $sc_data_host = array();
    if ($this->gwt_account == 1) {
      $sc_data_host = parent::getWebmasterToolsDataHost($this->hostname);
    }

    $best_rankings = parent::loadCacheFileBest($this->project_id);

    $outhead = '';
    $out     = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'OneDex', 'OneDex Max','OneDex Diff', 'OneDex Potential', 'Boost Potential', 'OPI', 'Search Volume', 'CPC', 'Competition', 'Best Rank');

    // merge ranking result with keywordset for keywords that do not rank
    foreach ($this->keywords as $keyword) {
      if (!isset($this->rankset['_da'][$keyword])) {
         $this->rankset['_da'][$keyword] = '';
      }
    }

    // sort by key
    ksort($this->rankset['_da']);

    $ii = 0;
    // TABLE HEADER - DATES
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $ii++;
      $out .= '<td class="nosearch">Rank - '. parent::germanTS($ts) .'</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td style="width:50px;" class="nosearch">Change</td>';
      }
      $out .= '<td class="nosearch">SEO Clicks</td>';
      array_push($this->csvarr['head'], 'Rank ' . $ts);
      array_push($this->csvarr['head'], 'Change');
      array_push($this->csvarr['head'], 'URL');
      array_push($this->csvarr['head'], 'SEO Clicks');
    }

    array_push($this->csvarr['head'], 'Tags');

    $out .= '</tr></thead><tbody>';

    // TABLE BODY - CONTENT
    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      $i++;

      if (isset($best_rankings[$keyword])) {
        $best_rank = $best_rankings[$keyword][0];
        $best_date = $best_rankings[$keyword][1];
      } else {
        $best_rank = '';
        $best_date = '';
      }

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank"> <a title="Dieses Keyword abfragen" target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->customerid.'/keywords/details/'.urlencode($keyword).'/'.$this->mo_type.'"><i class="icon-comment right"></i></a> <a href="'.parent::googleSearchDomain($this->country).'&q=' . $keyword . '" target="_blank">' . $keyword . '</a><small class="rankings"><a target="_blank" href="https://www.google.com/webmasters/tools/search-analytics?hl=de&siteUrl='.$this->domain.'#state=[null,[[null,null,null,28]],null,[[null,2,[%22'.urlencode($keyword).'%22]],[null,6,[%22WEB%22]]],null,[1,2,3,4],1,0,null,[2]]">GSC</a> | 
        <a target="_blank" href="https://de.sistrix.com/seo/serps/keyword/' . $keyword . '">SISTRIX</a> | <a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">TRENDS</a></small></td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {

        $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

      } else {

      // ONE DEX FETCH DATA FOR CALC

        $latest_ts = array_keys($this->rankset['_ts']);
        $latest_ts = $latest_ts[0];

        if (isset($timestamp[$latest_ts][0])) {
          $rank = $timestamp[$latest_ts][0]['rank'];
        } else {
          $rank = 101;
        }

        $opi      = $this->googleadwords[$keyword][3];
        $dex      = $this->calcOneDex($rank, $opi, true);
        $dex_max  = $this->calcOneDexMax($opi);
        $dex_diff = $dex_max - $dex;

        $dex_format      = $this->nufo($dex);
        $dex_max_format  = $this->nufo($dex_max);
        $dex_diff_format = $this->nufo($dex_diff);

        $potential = $dex_diff * $this->googleadwords[$keyword][2] * $this->googleadwords[$keyword][1] * $this->googleadwords[$keyword][1];

        $potential_format = $this->nufo($potential);

        $potential_indicate = 0;
        $potential_indicate_format = '';
        $potential_indicate_format_csv = '';

        if (isset($this->googlewebmastertools[$keyword])) {
          $potential_indicate_format  = '<span class="data-tooltip" data-tooltip="Grün: Großes Potential - Gutes Ranking vorhanden (ToDo: Keine)"><i class="status-success icon-certificate"></i></span>';
          $potential_indicate_format_csv  = 'high';
          $potential_indicate = 1;
          // rank today > 10
          if (!isset($this->rankset['_da'][$keyword][$latest_ts][0]['rank']) || $this->rankset['_da'][$keyword][$latest_ts][0]['rank'] > 7) {
            $potential_indicate_format  = '<span class="data-tooltip" data-tooltip="Rot: Großes Potential - Kein gutes Ranking vorhanden (ToDo: Mit &#34;Campaign Driven&#34; optimieren)"><i class="status-error icon-certificate"></i></span>';
            $potential_indicate_format_csv  = 'very high';
            $potential_indicate = 2;
          }
        }


      // ONE DEX FETCH DATA FOR CALC

        $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';
        $out .= '<td data-order="'.$dex.'" class="gadw tdright"> ' . $dex_format . ' </td>';
        $out .= '<td data-order="'.$dex_max.'" class="gadw tdright"> ' . $dex_max_format . ' </td>';
        $out .= '<td data-order="'.$dex_diff.'" class="gadw tdright"> ' . $dex_diff_format . ' </td>';
        $out .= '<td data-order="'.$potential.'" class="gadw tdright"> ' . $potential_format . ' </td>';
        $out .= '<td data-order="'.$potential_indicate.'" class="gadw tdcenter"> ' . $potential_indicate_format . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][3])  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][0].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][0],0)  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][1].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][1])  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][2].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][2])  . ' </td>';

      }

      $this->csvarr[$i] = array(
          $i,
          $keyword,
          $dex_format,
          $dex_max_format,
          $dex_diff_format,
          $potential_format,
          $potential_indicate_format_csv,
          $this->nufo($this->googleadwords[$keyword][3]),
          $this->nufo($this->googleadwords[$keyword][0],0),
          $this->nufo($this->googleadwords[$keyword][1]),
          $this->nufo($this->googleadwords[$keyword][2]),
          $best_rank
        );

      $out .= '<td data-order="' . $best_rank . '" class="gadw tdright"><span class="best" title="Datum: ' . $best_date . '">' . $best_rank . '</span></td>';
  
      // RANKING SETS
      $ii = 0;

      $ts_mover = $this->rankset['_ts'];

      foreach ($this->rankset['_ts'] as $ts) {

        $yesterday = next($ts_mover);

        $ii++;

        // NO RANKING TODAY
        if (!isset($timestamp[$ts])) {

          $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';

          if (isset( $this->rankset['_da'][$keyword][$yesterday][0]['rank'] )) {
            $change = $this->checkDifferenceBetweenRankings('101', $this->rankset['_da'][$keyword][$yesterday][0]['rank']);
            $change_csv = $this->checkDifferenceBetweenRankings('101', $this->rankset['_da'][$keyword][$yesterday][0]['rank'], false);
          } else {
            $change = '';
            $change_csv = '';
          }

          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change .'</td>';
          }

          $out .= '<td class="rank"> </td>';

          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], $change_csv);
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '');

        // RANKINGS TODAY
        } else {

          //multiple rankings
          if (count($timestamp[$ts]) > 1) {

            // sort multiple rankings, lowest first
            krsort($timestamp[$ts]);

            $out .= '<td class="rank"';
            $k = 0;

            $matched_url = array();
            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_url[$value['url']])) {
                continue;
              }
              $matched_url[$value['url']] = $value['url'];

              if ($k == 0) {
                $compare_index = count($this->rankset['_da'][$keyword][$yesterday]) - 1;
                $csv = $value['rank'];
                $csv_url = $value['url'];
                $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank']);
                $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank'], false);
              } else {
                $csv .= ' | ' . $value['rank'];
                $out .= ' <span class="multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              }
              $k++;

            }

            $out .= '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']). '/' .$this->mo_type.'">TOP100</a></small>';
            $out .= '</td>';

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change.'</td>';
            }

            array_push($this->csvarr[$i], $csv);
            array_push($this->csvarr[$i], $change_csv);
            array_push($this->csvarr[$i], $csv_url);

          } else {

          //single rankings
            foreach ($timestamp[$ts] as $value) {

              if (empty($value['rank'])) {
                $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';
              } else {
                $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] . '</a><small target="_blank" class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small></td>';
              }
              $compare_index = count($this->rankset['_da'][$keyword][$yesterday]) - 1;
              $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank']);
              $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank'], false);
              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'.$change .'</td>';
              }

              array_push($this->csvarr[$i], $value['rank']);
              array_push($this->csvarr[$i], $change_csv);
              array_push($this->csvarr[$i], $value['url']);

            }

          }

          $out .= '<td class="gadw">'.$value['cl'].'</td>';
          array_push($this->csvarr[$i], $value['cl']);

        }


      }

      // add tags 
      if (!empty($this->tags[$keyword][1])) {

        $tags = explode(',', $this->tags[$keyword][1]);

        foreach ($tags as $key => $value) {
          array_push($this->csvarr[$i], $value);
        }

      }

      $out .= '</tr>';

    }




    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords');

    $outhead .= '<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div><span class="titleleft">Ansicht: Keywords / Rankings</span><ul class="box-toolbar">';
    $outhead .= parent::submenu();
    $outhead .= $this->sevenDays();
    $outhead .= $this->renderEditSet();
    $outhead .= $this->personalDashboard();
    $outhead .='</ul></div><div class="box-content"><div class="row"><div class="col-md-12"><div id="OPSC_opi_highchart"><i class="oneproseo-preloader"></i></div></div></div></div><div class="box-content"><div class="row"><div class="col-md-12"><hr /></div></div></div><div class="box-content padded"><div class="row"><div class="col-md-12"><div class="row"><div class="col-md-1"></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-bolt icon-2x"></i></li>';

    if (isset($this->url_campaign)) {
      $outhead .=' <li class="count">'. count($this->url_campaign) .'</li></ul><div class="stats-label">Überwachte URLs</div>';
    } else {
      $outhead .=' <li class="count">'. $this->keyword_amount .'</li></ul><div class="stats-label">überwachte Keywords</div>';
    }

    $outhead .='</div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-signal"></i></li><li class="count" id="ops_top10"></li></ul><div class="stats-label">Top 10 Rankings</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-signal"></i></li><li class="count" id="ops_top20"></li></ul><div class="stats-label">Top 20 Rankings</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-signal"></i></li><li class="count" id="ops_top30"></li></ul><div class="stats-label">Top 30 Rankings</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-search"></i></li><li class="count" id="ops_topQ"></li></ul><div class="stats-label">∅ Ranking</div></div></div></div></div></div></div></div></div></div>';
    $outhead .='<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><span class="title">Detail Rankings für '.$this->setname.'</span><ul class="box-toolbar">';
    
    $outhead .='<li><span class="label label-default" id="ops_generictop10">Generische TOP 10 Rankings exportieren</span></li>';
  
    $outhead .='<li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li></ul></div><div class="box-content"><table class="table table-normal data-table dtoverflow"><thead><tr><td style="width:50px;">#</td><td>Keyword</td><td>Tags</td><td class="nosearch">OneDex</td><td class="nosearch">OneDex (MAX.)</td><td class="nosearch">OneDex (DIFF.)</td><td class="nosearch">OneDex Potential</td class="nosearch"><td class="nosearch">Boost Potential</td><td class="nosearch">OPI</td><td class="nosearch">SV</td><td class="nosearch">CPC</td><td class="nosearch">Wettb.</td><td class="nosearch">Best</td>';

    $this->out = $outhead . $out;

  }


  /*
  URL -> DEX -> ALL KEYWORDS FOR URL
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/23/detail-url/93
  */

  private function renderRankingViewUrl ()
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'URL', 'OneDex');

    // merge ranking result with keywordset for keywords that do not rank
    if (isset($this->url_campaign)) {
      foreach ($this->url_campaign as $url) {
        if (stripos($url, '*') !== false) {continue;}
        if (!isset($this->rankset['_da'][$url])) {
           $this->rankset['_da'][$url] = '';
        }
      }
    }

    // DATES
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $out .= '<td>'.parent::germanTS($ts).'</td>';
      array_push($this->csvarr['head'], $ts);
    }

    $out .= '</tr></thead><tbody>';

    $serp_url = parent::googleSearchDomain($this->country);

    foreach ($this->rankset['_da'] as $url => $timestamp) {

      $i++;

      if (!isset($timestamp[$ts])) {
        $dex_cumm = 0;
      } else {
        // ONE DEX FETCH DATA FOR CALC
        $today = key($timestamp);
        $dex_cumm = $this->oneDexAccumulated($timestamp[$today]);
        $dex_cumm      = $this->nufo($dex_cumm);
      }

      $out .= '<tr>';
      $out .= '<td>' . $i . '</td>';
      $out .= '<td><a href="'.$url.'" target="_blank">' . $url . '</a><small class="rankings"><a target="_blank" href="https://www.google.com/webmasters/tools/search-analytics?hl=de&siteUrl='.$this->domain.'#state=[null,[[null,null,null,28]],null,[[null,3,[%22'.urlencode($url).'%22],1,0],[null,6,[%22WEB%22]]],null,[1,2,3,4],2,0,null,[3]]">GSC</a></small></td>';

      $out .= '<td>' . $dex_cumm . '</td>';

      $this->csvarr[$i] = array($i, $url, $dex_cumm);

      // RANKING SETS
      foreach ($this->rankset['_ts'] as $k => $ts) {

        if (!isset($timestamp[$ts])) {

          $out .= '<td> - </td>';
          array_push($this->csvarr[$i], '-');

        } else {

           // check for double rankings
          if (count($timestamp[$ts]) > 1) {

            $csv = '';
            $out .= '<td>';
            $out .= '<table class="plain" data-ts="' . $ts . '">';

            $matched_keywords = array();
            $s_out = '';
            $kws = '';
            $x = 0;

            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_keywords[$value['keyword']])) {
                continue;
              }
              $opi_val = (!isset($this->googleadwords[$value['keyword']][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][3]);
              $matched_keywords[$value['keyword']] = $opi_val;
              $matched_keywords_rank[$value['keyword']] = $value['rank'];
              $kws++;

            }

            // SORT OPI DESC
            arsort($matched_keywords, SORT_NUMERIC);

            foreach ($matched_keywords as $keyword => $opi) {

              $s_out .= '<tr>
              <td style="width:70%"><a href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $keyword . '</span></a></td>
              <td><a href="'.$serp_url.'&q='.urlencode($keyword).'" target="_blank">' . $matched_keywords_rank[$keyword] . '</a></td><td> ' . $opi . '</td></tr>';
              $csv .=  $keyword . ' | ';

            }

            if ($kws < 2) {$out .= '<tr><td colspan="3"><b>Rankt für 1 Keyword</b></td></tr>';} else {$out .= '<tr><td colspan="3"><b>Rankt für ' . $kws . ' Keywords</b></td></tr>';}

            $out .= $s_out;
            $out .= '</table>';
            $out .= '</td>';

            array_push($this->csvarr[$i], $csv);

          } else {

            $csv = '';
            foreach ($timestamp[$ts] as $value) {
              $out .= '<td>';
              $out .= '<table class="plain" data-ts="' . $ts . '">';
              $out .= '<tr><td colspan="3"><b>Rankt für 1 Keyword</b></td></tr>';
              $out .= '<tr><td style="width:70%"><a href="../keywords/results/'.urlencode($value['keyword']).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $value['keyword'] . '</span></a></td><td>' . $value['rank'] . '</td><td>'.(!isset($this->googleadwords[$value['keyword']][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][3]).'</td></tr>';
              $out .= '</table>';
              $csv .=  $value['keyword'] . ' | ';
            }

            array_push($this->csvarr[$i], $csv);

          }

        }

      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';


    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_urls');

    $outhead .='<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div><span class="titleleft">Ansicht: URL / Keywords</span><ul class="box-toolbar">';

    $outhead .=  parent::submenu();

    if ($this->type != 'complete') {
      $outhead .= $this->sevenDays();
    }

    $outhead .= $this->renderEditSet();
    $outhead .= $this->personalDashboard();

    $css = '';
    if ($this->type != 'complete') {
      $css = 'dtoverflow';
    }

    $outhead .='</ul></div><div class="box-content padded"><p><p>Diese Ansicht zeigt die rankenden URLs eines Keywordsets und die Keywords für die diese URL rankt. <br />Rankende URLs für '.$this->hostname.': <b>' . count ($this->rankset['_da']) . '</b></p></div></div><div class="box"><div class="box-header"><span class="title">Rankings</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li></ul></div><div class="box-content"><table class="table table-normal data-table '.$css.'"><thead><tr><td>#</td><td>URL</td><td>OneDex</td>';

    $this->out = $outhead . $out;

  }


  /*

  URL -> RANKING KEYWORDS -> DEX -> CHANGE
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/23/detail-url-onedex/93
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/35/detail-url-onedex-complete/all
  */

  private function renderRankingViewUrlDex ()
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'URL');

    // merge ranking result with keywordset for keywords that do not rank
    if (isset($this->url_campaign)) {
      foreach ($this->url_campaign as $url) {
        if (stripos($url, '*') !== false) {continue;}
        if (!isset($this->rankset['_da'][$url])) {
           $this->rankset['_da'][$url] = '';
        }
      }
    }

    // GOOGLE ANALYTICS DATA
    if (count($this->googleanalyticsAll) > 0) {
      $out .= '<td>Pageviews</td><td>Page Value (€)</td><td>Umsatz Zielseite (€)</td>';
      array_push($this->csvarr['head'], 'Pageviews');
      array_push($this->csvarr['head'], 'Page Value (€)');
      array_push($this->csvarr['head'], 'Umsatz Zielseite (€)');
    }

    // DATES
    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $ii++;

      $out .= '<td>OneDex</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td>Change</td>';
      }
      $out .= '<td>' . parent::germanTS($ts) . '</td>';

      array_push($this->csvarr['head'], 'Rankende Keywords');
      array_push($this->csvarr['head'], 'OneDex');

    }

    $out .= '</tr></thead><tbody>';


    // DATA
    foreach ($this->rankset['_da'] as $url => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td>' . $i . '</td>';
      $out .= '<td><a href="'.$url.'" target="_blank">' . $url . '</a><small class="rankings"><a target="_blank" href="https://www.google.com/webmasters/tools/search-analytics?hl=de&siteUrl='.$this->domain.'#state=[null,[[null,null,null,28]],null,[[null,3,[%22'.urlencode($url).'%22],1,0],[null,6,[%22WEB%22]]],null,[1,2,3,4],2,0,null,[3]]">GSC</a></small></td>';

      $this->csvarr[$i] = array($i, $url);

      // GOOGLE ANALYTICS DATA
      if (count($this->googleanalyticsAll) > 0) {
        $url = parent::removeHttps($url);
        $url = strtolower($url);
        if (isset ($this->googleanalyticsAll[$url])) {

          $p_v = $this->nufo($this->googleanalyticsAll[$url]['ga_pageValue_filter']);
          $t_r = $this->nufo($this->googleanalyticsAll[$url]['ga_transactionRevenue_filter']);
          $sum_piv = $this->googleanalyticsAll[$url]['ga_pageviews_filter'];

          $out .= '<td class="nowrap" data-order="'.$sum_piv.'"> ' . $sum_piv . ' </td>';
          $out .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_pagevalue_filter']).'"> ' . $p_v . ' </td>';
          $out .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_transactionRevenue_filter']).'"> ' . $t_r . ' </td>';

          array_push($this->csvarr[$i], $sum_piv);
          array_push($this->csvarr[$i], $p_v);
          array_push($this->csvarr[$i], $t_r);

        } else {
          $out .= '<td class="nowrap" data-order="0"> - </td><td data-order="0" class="nowrap"> - </td><td data-order="0" class="nowrap"> - </td>';
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
        }
      }

      // RANKING SETS
      $ii = 0;
      $ts_mover = $this->rankset['_ts'];

      foreach ($this->rankset['_ts'] as $ts) {

        $yesterday = next($ts_mover);

        $ii++;
        if (!isset($timestamp[$ts])) {

          $out .= '<td> - </td>';
          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td> - </td>';
          }
          $out .= '<td> - </td>';

          array_push($this->csvarr[$i], '-');

        } else {

          // ONE DEX FETCH DATA FOR CALC

          $dex_cumm = $this->oneDexAccumulated($timestamp[$ts]);
          $dex_cumm_yesterday = $this->oneDexAccumulated($timestamp[$yesterday]);
          $change_dex = $this->calcPercent($dex_cumm, $dex_cumm_yesterday);

          $dex_cumm = $this->nufo($dex_cumm);

           // check for double rankings
          if (count($timestamp[$ts]) > 1) {

            $csv = '';
            $out .= '<td class="nowrap"> ' . $dex_cumm . ' </td>';
            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td class="nowrap"> ' . $change_dex . ' </td>';
            }
            $out .= '<td class="nowrap">';

            $matched_keywords = array();
            $kws = 0;
            $out_kw = '';
            foreach ($timestamp[$ts] as $value) {
              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_keywords[$value['keyword']])) {
                continue;
              }

              $opi_val = (!isset($this->googleadwords[$value['keyword']][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][3]);
              $matched_keywords[$value['keyword']] = $opi_val;
              $matched_keywords_rank[$value['keyword']] = $value['rank'];

              $out_kw .=  $value['keyword'] . '<br />';
              $kws++;
            }

            // SORT OPI DESC
            arsort($matched_keywords, SORT_NUMERIC);

            $s_out = '<table class="plain" data-ts="' . $ts . '">';
            foreach ($matched_keywords as $keyword => $opi) {
              $s_out .= '<tr><td style="width:70%"><a href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $keyword . '</span></a></td><td>' . $matched_keywords_rank[$keyword] . '</td><td> ' . $opi . '</td></tr>';
            }
            $s_out .= '</table>';

            if ($kws < 2) {
              $out .= '<span class="tt"><strong class="tooltip-with-markup"><b>1</b> Keyword';
              $csv = '1 Keyword';
            } else {
              $out .= '<span class="tt"><strong class="tooltip-with-markup"><b>' . $kws . '</b> Keywords';
              $csv = $kws . ' Keywords';
            }

            $out .= '<span class="tooltip-content"><span class="arrow"></span>'.$s_out.'</span></strong></span>';

            $out .= '</td>';

            array_push($this->csvarr[$i], $csv);
            array_push($this->csvarr[$i], $dex_cumm);

          } else {

            foreach ($timestamp[$ts] as $value) {
              $out .= '<td class="nowrap"> ' .$dex_cumm. ' </td>';
              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td class="nowrap"> ' . $change_dex . ' </td>';
              }
              $s_out = '<table class="plain" data-ts="' . $ts . '">';
              $s_out .= '<tr><td style="width:70%"><a href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $value['keyword'] . '</span></a></td><td>' . $value['rank'] . '</td><td>'.(!isset($this->googleadwords[$value['keyword']][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][3]).'</td></tr>';
              $s_out .= '</table>';
              $out .= '<td class="nowrap"><span class="tt"><strong class="tooltip-with-markup"><b>1</b> Keyword <span class="tooltip-content"><span class="arrow"></span>'.$s_out.'</span></strong></span></td>';
            }

            $csv = '1 Keyword';

            array_push($this->csvarr[$i], $csv);
            array_push($this->csvarr[$i], $dex_cumm);

          }

        }

      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_urls_dex');

    $outhead .='<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div><span class="titleleft">Ansicht: URL / OneDex</span><ul class="box-toolbar">';

    $outhead .=  parent::submenu();

    if ($this->type != 'dexcomplete') {
      $outhead .= $this->sevenDays();
    }

    $outhead .= $this->renderEditSet();
    $outhead .= $this->personalDashboard();

    $css = '';
    if ($this->type != 'dexcomplete') {
      $css = 'dtoverflow';
    }

    $outhead .='</ul></div><div class="box-content padded"><p>Diese Ansicht zeigt die rankenden URLs eines Keywordsets und die Anzahl der Keywords für die diese URL rankt. <br />Rankende URLs für '.$this->hostname.': <b>' . count ($this->rankset['_da']) . '</b></p></div></div><div class="box"><div class="box-header"><span class="title">Rankings</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li></ul></div><div class="box-content"><table class="table table-normal data-table '.$css.'"><thead><tr><td>#</td><td>URL</td>';

    $this->out = $outhead . $out;

  }


  /*
  URL -> RANKING KEYWORDS -> DEX -> ANALYTICS
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/35/detail-url-onedex-analytics-complete/all
  */

  private function renderRankingViewUrlDexAnalytics ()
  {

    $outhead = '';
    $out = '';
    $out_table_head = '';
    $out_table_body = '';
    $out_table_foot = '';
    $sum_p_v = 0;
    $sum_t_r = 0;
    $sum_p_v_all = 0;
    $sum_t_r_all = 0;
    $sum_trans = 0;
    $sum_trans_all = 0;
    $sum_qty = 0;
    $sum_qty_all = 0;
    $sum_piv = 0;
    $sum_piv_all = 0;
    $sum_orgs = 0;
    $sum_orgs_all = 0;
    $sum_top = 0;
    $sum_top_all = 0;
    $sum_plt = 0;
    $sum_plt_all = 0;
    $sum_entr = 0;
    $sum_entr_all = 0;
    $sum_exit = 0;
    $sum_exit_all = 0;

    $this->csvarr['head'] = array('#', 'URL');

    $out_table_head = '<table class="table table-normal data-table dtoverflow"><thead><tr><td>#</td><td>URL</td>';

    $ts = current($this->rankset['_ts']);
    // DATES
    $out_table_head .= '<td>OneDex</td>';
    $out_table_head .= '<td>' . parent::germanTS($ts) . '</td>';
    array_push($this->csvarr['head'], 'OneDex');
    array_push($this->csvarr['head'], $ts);

    // GOOGLE ANALYTICS DATA

    $out_table_head .= '<td>Pageviews<br/>[organic]</td><td>Pageviews<br/>[all]</td><td>Page Value (€)<br/>[organic]</td><td>Page Value (€)<br/>[all]</td><td>Umsatz Zielseite (€)<br/>[organic]</td><td>Umsatz Zielseite (€)<br/>[all]</td><td>Transactions<br/>[organic]</td><td>Transactions<br/>[all]</td><td>Sales QTY<br/>[organic]</td><td>Sales QTY<br/>[all]</td><td>Organic Searches<br/>[organic]</td><td>Organic Searches<br/>[all]</td><td>AVG Time on Page<br/>[organic]</td><td>AVG Time on Page<br/>[all]</td><td>AVG Load Time<br/>[organic]</td><td>AVG Load Time<br/>[all]</td><td>Entrances<br/>[organic]</td><td>Entrances<br/>[all]</td><td>Exits<br/>[organic]</td><td>Exits<br/>[all]</td>';

    array_push($this->csvarr['head'], 'Pageviews [organic]');
    array_push($this->csvarr['head'], 'Pageviews [all]');
    array_push($this->csvarr['head'], 'Page Value (€) [organic]');
    array_push($this->csvarr['head'], 'Page Value (€) [all]');
    array_push($this->csvarr['head'], 'Umsatz Zielseite (€) [organic]');
    array_push($this->csvarr['head'], 'Umsatz Zielseite (€) [all]');
    array_push($this->csvarr['head'], 'Transactions [organic]');
    array_push($this->csvarr['head'], 'Transactions [all]');
    array_push($this->csvarr['head'], 'Sales QTY [organic]');
    array_push($this->csvarr['head'], 'Sales QTY [all]');
    array_push($this->csvarr['head'], 'Organic Searches [organic]');
    array_push($this->csvarr['head'], 'Organic Searches [all]');
    array_push($this->csvarr['head'], 'AVG Time on Page [organic]');
    array_push($this->csvarr['head'], 'AVG Time on Page [all]');
    array_push($this->csvarr['head'], 'AVG Load Time [organic]');
    array_push($this->csvarr['head'], 'AVG Load Time [all]');
    array_push($this->csvarr['head'], 'Entrances [organic]');
    array_push($this->csvarr['head'], 'Entrances [all]');
    array_push($this->csvarr['head'], 'Exits [organic]');
    array_push($this->csvarr['head'], 'Exits [all]');

    $out_table_head .= '</tr>';

    // DATA

    foreach ($this->rankset['_da'] as $url => $timestamp) {

      $i++;

      $out_table_body .= '<tr>';
      $out_table_body .= '<td>' . $i . '</td>';
      $out_table_body .= '<td><a href="'.$url.'" target="_blank">' . $url . '</a></td>';

      $this->csvarr[$i] = array($i, $url);

      // RANKING SETS


			$ts = $this->dateYMD();


      if (!isset($timestamp[$ts])) {

        $out_table_body .= '<td> - </td>';
        $out_table_body .= '<td> - </td>';

        array_push($this->csvarr[$i], '-');

      } else {

        // ONE DEX FETCH DATA FOR CALC
        $dex_cumm = $this->oneDexAccumulated($timestamp[$ts]);
        $dex_cumm_yesterday = $this->oneDexAccumulated($timestamp[$yesterday]);
        $dex_cumm = $this->nufo($dex_cumm);
        $dex_cumm_yesterday = $this->nufo($dex_cumm_yesterday);

        $change_dex = $this->calcPercent($dex_cumm, $dex_cumm_yesterday);

         // check for double rankings
        if (count($timestamp[$ts]) > 1) {

          $csv = '';
          $out_table_body .= '<td class="nowrap"> ' . $dex_cumm . ' </td>';
          $out_table_body .= '<td class="nowrap">';

          $matched_keywords = array();
          $kws = 0;
          $out_kw = '';

          foreach ($timestamp[$ts] as $value) {
            // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
            if (isset($matched_keywords[$value['keyword']])) {
              continue;
            }

            $opi_val = (!isset($this->googleadwords[$value['keyword']][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][3]);
            $matched_keywords[$value['keyword']] = $opi_val;
            $matched_keywords_rank[$value['keyword']] = $value['rank'];

            $out_kw .=  $value['keyword'] . '<br />';
            $kws++;
          }

          // SORT OPI DESC
          arsort($matched_keywords, SORT_NUMERIC);

          $s_out = '<table class="plain" data-ts="' . $ts . '">';
          foreach ($matched_keywords as $keyword => $opi) {
            $s_out .= '<tr><td style="width:70%"><a href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $keyword . '</span></a></td><td>' . $matched_keywords_rank[$keyword] . '</td><td> ' . $opi . '</td></tr>';
          }
          $s_out .= '</table>';

          if ($kws < 2) {
            $out_table_body .= '<span class="tt"><strong class="tooltip-with-markup"><b>1</b> Keyword';
            $csv = '1 Keyword';
          } else {
            $out_table_body .= '<span class="tt"><strong class="tooltip-with-markup"><b>' . $kws . '</b> Keywords';
            $csv = $kws . ' Keywords';
          }

          $out_table_body .= '<span class="tooltip-content"><span class="arrow"></span>'.$s_out.'</span></strong></span>';

          $out_table_body .= '</td>';


          array_push($this->csvarr[$i], $dex_cumm);
          array_push($this->csvarr[$i], $csv);

        } else {

          foreach ($timestamp[$ts] as $value) {
            $out_table_body .= '<td class="nowrap"> ' .$dex_cumm. ' </td>';
            $s_out = '<table class="plain" data-ts="' . $ts . '">';
            $s_out .= '<tr><td style="width:70%"><a href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $value['keyword'] . '</span></a></td><td>' . $value['rank'] . '</td><td>'.(!isset($this->googleadwords[$value['keyword']][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][3]).'</td></tr>';
            $s_out .= '</table>';
            $out_table_body .= '<td class="nowrap"><span class="tt"><strong class="tooltip-with-markup"><b>1</b> Keyword <span class="tooltip-content"><span class="arrow"></span>'.$s_out.'</span></strong></span></td>';
          }

          $csv = '1 Keyword';

          array_push($this->csvarr[$i], $dex_cumm);
          array_push($this->csvarr[$i], $csv);

        }

      }


      // GOOGLE ANALYTICS DATA

      // remove http(s) .. some sites deliver http data via GA but use https
        $url = parent::removeHttps($url);
        $url = strtolower($url);

        if (isset ($this->googleanalyticsAll[$url])) {

          $sum_piv = $sum_piv + $this->googleanalyticsAll[$url]['ga_pageviews_filter'];
          $sum_piv_all = $sum_piv_all + $this->googleanalyticsAll[$url]['ga_pageviews'];
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_pageviews_filter']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_pageviews_filter'],0) . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_pageviews']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_pageviews'],0)  . ' </td>';
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_pageviews_filter']);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_pageviews']);

          $sum_p_v = $sum_p_v + $this->googleanalyticsAll[$url]['ga_pageValue_filter'];
          $sum_t_r = $sum_t_r + $this->googleanalyticsAll[$url]['ga_transactionRevenue_filter'];
          $sum_p_v_all = $sum_p_v_all + $this->googleanalyticsAll[$url]['ga_pageValue'];
          $sum_t_r_all = $sum_t_r_all + $this->googleanalyticsAll[$url]['ga_transactionRevenue'];
          $p_v = $this->nufo($this->googleanalyticsAll[$url]['ga_pageValue_filter']);
          $t_r = $this->nufo($this->googleanalyticsAll[$url]['ga_transactionRevenue_filter']);
          $p_v_all = $this->nufo($this->googleanalyticsAll[$url]['ga_pageValue']);
          $t_r_all = $this->nufo($this->googleanalyticsAll[$url]['ga_transactionRevenue']);
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_pageValue_filter']).'"> ' . $p_v . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_pageValue']).'"> ' . $p_v_all . ' </td>';
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_transactionRevenue_filter']).'"> ' . $t_r . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_transactionRevenue']).'"> ' . $t_r_all . ' </td>';
          array_push($this->csvarr[$i], $p_v);
          array_push($this->csvarr[$i], $t_r);
          array_push($this->csvarr[$i], $p_v_all);
          array_push($this->csvarr[$i], $t_r_all);


          $sum_trans = $sum_trans + $this->googleanalyticsAll[$url]['ga_transactions_filter'];
          $sum_trans_all = $sum_trans_all + $this->googleanalyticsAll[$url]['ga_transactions'];
          $sum_qty = $sum_qty + $this->googleanalyticsAll[$url]['ga_itemQuantity_filter'];
          $sum_qty_all = $sum_qty_all + $this->googleanalyticsAll[$url]['ga_itemQuantity'];
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_transactions_filter']).'"> ' . $this->googleanalyticsAll[$url]['ga_transactions_filter'] . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_transactions']).'"> ' . $this->googleanalyticsAll[$url]['ga_transactions'] . ' </td>';
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_itemQuantity_filter']).'"> ' . $this->googleanalyticsAll[$url]['ga_itemQuantity_filter'] . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_itemQuantity']).'"> ' . $this->googleanalyticsAll[$url]['ga_itemQuantity'] . ' </td>';
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_transactions_filter']);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_transactions']);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_itemQuantity_filter']);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_itemQuantity']);


          $sum_orgs = $sum_orgs + $this->googleanalyticsAll[$url]['ga_organicSearches_filter'];
          $sum_orgs_all = $sum_orgs_all + $this->googleanalyticsAll[$url]['ga_organicSearches'];
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_organicSearches_filter']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_organicSearches_filter'],0)  . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_organicSearches']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_organicSearches'],0)  . ' </td>';
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_organicSearches_filter']);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_organicSearches']);


          $sum_top = $sum_top + $this->googleanalyticsAll[$url]['ga_avgTimeOnPage_filter'];
          $sum_top_all = $sum_top_all + $this->googleanalyticsAll[$url]['ga_avgTimeOnPage'];
          $sum_plt = $sum_plt + $this->googleanalyticsAll[$url]['ga_avgPageLoadTime_filter'];
          $sum_plt_all = $sum_plt_all + $this->googleanalyticsAll[$url]['ga_avgPageLoadTime'];
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_avgTimeOnPage_filter']).'"> ' . round($this->googleanalyticsAll[$url]['ga_avgTimeOnPage_filter'], 2) . ' sec. </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_avgTimeOnPage']).'"> ' . round($this->googleanalyticsAll[$url]['ga_avgTimeOnPage'], 2) . ' sec. </td>';
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_avgPageLoadTime_filter']).'"> ' . round($this->googleanalyticsAll[$url]['ga_avgPageLoadTime_filter'], 2) . ' sec. </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_avgPageLoadTime']).'"> ' . round($this->googleanalyticsAll[$url]['ga_avgPageLoadTime'], 2) . ' sec. </td>';
          array_push($this->csvarr[$i], $this->nufo($this->googleanalyticsAll[$url]['ga_avgTimeOnPage_filter'],0));
          array_push($this->csvarr[$i], $this->nufo($this->googleanalyticsAll[$url]['ga_avgTimeOnPage'],0));
          array_push($this->csvarr[$i], $this->nufo($this->googleanalyticsAll[$url]['ga_avgPageLoadTime_filter'],0));
          array_push($this->csvarr[$i], $this->nufo($this->googleanalyticsAll[$url]['ga_avgPageLoadTime'],0));


          $sum_entr = $sum_entr + $this->googleanalyticsAll[$url]['ga_entrances_filter'];
          $sum_entr_all = $sum_entr_all + $this->googleanalyticsAll[$url]['ga_entrances'];
          $sum_exit = $sum_exit + $this->googleanalyticsAll[$url]['ga_exits_filter'];
          $sum_exit_all = $sum_exit_all + $this->googleanalyticsAll[$url]['ga_exits'];
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_entrances_filter']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_entrances_filter']) . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_entrances']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_entrances']) . ' </td>';
          $out_table_body .= '<td class="nowrap hl" data-order="'.round($this->googleanalyticsAll[$url]['ga_exits_filter']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_exits_filter']) . ' </td>';
          $out_table_body .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_exits']).'"> ' . $this->nufo($this->googleanalyticsAll[$url]['ga_exits']) . ' </td>';
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_entrances_filter'],0);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_entrances'],0);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_exits_filter'],0);
          array_push($this->csvarr[$i], $this->googleanalyticsAll[$url]['ga_exits'],0);

        } else {
          $out_table_body .= '
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                   <td class="nowrap hl" data-order="0"> - </td>
                   <td class="nowrap" data-order="0"> - </td>
                  ';
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '-');

        }

      $out_table_body .= '</tr>';

    }

    $out_table_body .= '</tbody>';

    $out_table_foot .= '<tfoot>
               <tr>
                 <td class="small" colspan="4"> </td>
                 <td class="small">Pageviews<br/>[organic]</td>
                 <td class="small">Pageviews<br/>[all]</td>
                 <td class="small">Page Value<br/>[organic]</td>
                 <td class="small">Page Value<br/>[all]</td>
                 <td class="small">Umsatz Zielseite<br/>[organic]</td>
                 <td class="small">Umsatz Zielseite<br/>[all]</td>
                 <td class="small">Transactions<br/>[organic]</td>
                 <td class="small">Transactions<br/>[all]</td>
                 <td class="small">Sales QTY<br/>[organic]</td>
                 <td class="small">Sales QTY<br/>[all]</td>
                 <td class="small">Organic Searches<br/>[organic]</td>
                 <td class="small">Organic Searches<br/>[all]</td>
                 <td class="small">AVG Time on Page<br/>[organic]</td>
                 <td class="small">AVG Time on Page<br/>[all]</td>
                 <td class="small">AVG Load Time<br/>[organic]</td>
                 <td class="small">AVG Load Time<br/>[all]</td>
                 <td class="small">Entrances<br/>[organic]</td>
                 <td class="small">Entrances<br/>[all]</td>
                 <td class="small">Exits<br/>[organic]</td>
                 <td class="small">Exits<br/>[all]</td>
               </tr>';

        $out_table_intercept = '
               <tr>
                 <td class="nowrap cumm" colspan="4"> SUM (Gesamt aktuelles Jahr)</td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_piv, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_piv_all, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_p_v, 2).' € </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_p_v_all, 2).' € </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_t_r, 2).' € </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_t_r_all, 2).' € </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_trans, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_trans_all, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_qty, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_qty_all, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_orgs, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_orgs_all, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_top, 2).' sec. </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_top_all, 2).' sec. </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_plt, 2).' sec. </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_plt_all, 2).' sec. </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_entr, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_entr_all, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_exit, 0).' </td>
                 <td class="nowrap cumm"> '.$this->nufo($sum_exit_all, 0).' </td>
               </tr>';


    $csv_filename = parent::csvBuilder($this->csvarr, 'export_analytics');

    $outhead .='<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div><span class="titleleft">Ansicht: Analytics / OneDex</span><ul class="box-toolbar">';

    $outhead .= parent::submenu();

    $outhead .= $this->renderEditSet();
    $outhead .= $this->personalDashboard();

    $outhead .='</ul></div><div class="box-content padded"><p>Diese Ansicht zeigt die rankenden URLs eines Keywordsets mit den Google Analytics Daten. <br />Rankende URLs für '.$this->hostname.': <b>' . count ($this->rankset['_da']) . '</b></p></div></div><div class="box"><div class="box-header"><span class="title">Rankings</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Analytics Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li></ul></div><div class="box-content">';
    $this->out = $outhead . $out_table_head . $out_table_intercept . '</thead><tbody>' . $out_table_body . $out_table_foot . $out_table_intercept . '</tfoot></table></div></div></div></div>';

  }


  /*
  KEYWORD -> RANK -> URL
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/23/detail-keyword-url/93
  */


  private function renderRankingViewKeywordUrl() {

    $best_rankings = parent::loadCacheFileBest($this->project_id);

    $ts = current($this->rankset['_ts']);


    if ($this->type == 'keyword-url-duplicates') {

      $rankset['_dc'] = array();
      foreach ($this->rankset['_da'] as $key => $value) {
        if (count($value[$ts]) > 1) {
          $rankset[$key] = $value;
        }
      }
      $this->rankset['_da'] = $rankset;

    } else {

      // merge ranking result with keywordset for keywords that do not rank
      if (!isset($this->url_campaign)) {
        foreach ($this->keywords as $keyword) {
          if (!isset($this->rankset['_da'][$keyword])) {
             $this->rankset['_da'][$keyword] = '';
          }
        }
      }

    }

    $outhead = '';
    $out = '';



    $this->csvarr['head'] = array('#', 'Keyword', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition', 'Best Rank');
    $this->csvarr_single['head'] = array('#', 'Keyword', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition', 'Best Rank');

    if (empty($this->rankset['_da'])) {
      $this->hintEmpty();
      exit;
    }

    // sort by key
    ksort($this->rankset['_da']);
    // DATES
    $out .= '<td class="nosearch">Rank - '. parent::germanTS($ts) .'</td>';
    array_push($this->csvarr['head'], $ts);
    array_push($this->csvarr_single['head'], $ts);

    $out .= '<td class="nosearch">Rankende URL</td></tr></thead><tbody>';
    array_push($this->csvarr['head'], 'Rankende URL');
    array_push($this->csvarr_single['head'], 'Rankende URL');
    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      if (isset($best_rankings[$keyword])) {
        $best_rank = $best_rankings[$keyword][0];
        $best_date = $best_rankings[$keyword][1];
      } else {
        $best_rank = '';
        $best_date = '';
      }

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank"><a href="'.parent::googleSearchDomain($this->country).'&q=' . $keyword . '" target="_blank">' . $keyword . '</a><small class="rankings"><a target="_blank" href="https://www.google.com/webmasters/tools/search-analytics?hl=de&siteUrl='.$this->domain.'#state=[null,[[null,null,null,28]],null,[[null,2,[%22'.urlencode($keyword).'%22]],[null,6,[%22WEB%22]]],null,[1,2,3,4],1,0,null,[2]]">GSC</a> | <a target="_blank" href="https://de.sistrix.com/seo/serps/keyword/' . $keyword . '">SISTRIX</a> | <a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">TRENDS</a></small></td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {

        $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

      } else {

      // ONE DEX FETCH DATA FOR CALC
        $latest_ts = array_keys($this->rankset['_ts']);
        $latest_ts = $latest_ts[0];
        if (isset($timestamp[$latest_ts][0])) {
          $rank = $timestamp[$latest_ts][0]['rank'];
        } else {
          $rank = 101;
        }
        $opi = $this->googleadwords[$keyword][3];
        $dex_format = $this->calcOneDex($rank, $opi);
        $dex_format = $this->nufo($dex_format);

      // ONE DEX FETCH DATA FOR CALC

        $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';
        $out .= '<td data-order="'.$dex_format.'" class="gadw tdright"> ' . $dex_format . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][3])  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][0].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][0], 0)  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][1].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][1])  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][2].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][2])  . ' </td>';

      }

      $this->csvarr[$i] = array($i, $keyword, $dex_format, $this->nufo($this->googleadwords[$keyword][3]), $this->nufo($this->googleadwords[$keyword][0], 0), $this->nufo($this->googleadwords[$keyword][1]), $this->nufo($this->googleadwords[$keyword][2]), $best_rank);
      $this->csvarr_single[$i] = array($i, $keyword, $dex_format, $this->nufo($this->googleadwords[$keyword][3]), $this->nufo($this->googleadwords[$keyword][0], 0), $this->nufo($this->googleadwords[$keyword][1]), $this->nufo($this->googleadwords[$keyword][2]), $best_rank);

      $out .= '<td data-order="' . $best_rank . '" class="gadw tdright"><span class="best" title="Datum: ' . $best_date . '">' . $best_rank . '</span></td>';

      // RANKING SETS
      if (!isset($timestamp[$ts])) {

        $out .= '<td data-order="999" class="rank ops_single"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';
        $rank_url = '-';
        array_push($this->csvarr[$i], '-',  '-');
        array_push($this->csvarr_single[$i], '-',  '-');

      } else {

         // check for multiple rankings
        if (count($timestamp[$ts]) > 1) {

          // sort multiple rankings, lowest first
          krsort($timestamp[$ts]);

          $fv = reset($timestamp[$ts]);

          if ($fv['rank'] == 1) {
            $out .= '<td class="rank ops_single"';
          } else {
            $out .= '<td class="rank"';
          }

          $k = 0;

          $matched_url = array();

          foreach ($timestamp[$ts] as $value) {

            // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
            if (isset($matched_url[$value['url']])) {
              continue;
            }

            $matched_url[$value['url']] = $value['url'];

            if ($k == 0) {

              $csv    = $value['rank'];
              $csvu   = $value['url'];
              $csv_s  = $value['rank'];
              $csvu_s = $value['url'];

		          array_push($this->csvarr[$i], $csv, $csvu);
		          array_push($this->csvarr_single[$i], $csv_s, $csvu_s);

              $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              $rank_url = '<a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';

            } else {

							$i++;

							$this->csvarr[$i] = array($i, $keyword, $dex_format, $this->nufo($this->googleadwords[$keyword][3]), $this->nufo($this->googleadwords[$keyword][0], 0), $this->nufo($this->googleadwords[$keyword][1]), $this->nufo($this->googleadwords[$keyword][2]), $best_rank, $value['rank'], $value['url']);

              $out .= ' <span class="multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              $rank_url .= '<br /><a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
            }
            $k++;
          }




          $out .= '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small>';
          $out .= '</td>';

        } else {

          foreach ($timestamp[$ts] as $value) {
            array_push($this->csvarr[$i], $value['rank'], $value['url']);
            array_push($this->csvarr_single[$i], $value['rank'], $value['url']);
            if (empty($value['rank'])) {
              $out .= '<td data-order="999" class="rank ops_single"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';
            } else {
              $out .= '<td data-order="'.$value['rank'].'" class="rank ops_single"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small></td>';
            }
            $rank_url = '<a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
          }

        }

      }

      $out .= '<td class="rank-url">' . $rank_url . '</td>';
      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords_url');
    $csv_filename_single = parent::csvBuilder($this->csvarr_single, 'export_ruk_keywords_url_single');

    $outhead .='<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div><span class="titleleft">Ansicht: Keywords / URL</span><ul class="box-toolbar">';

    $outhead .=  parent::submenu();

    $outhead .= $this->renderEditSet();
    $outhead .= $this->personalDashboard();

    $outhead .='</ul></div><div class="box-content padded"><p>Diese Ansicht zeigt die Keywords, den Rang heute und welche URL für dieses Keyword rankt.</p></div></div><div class="box"><div class="box-header"><span class="title">Detail Keyword / URL Rankings für '.$this->setname.'</span><ul class="box-toolbar"><li><a title="CSV download ohne Doppelrankings" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename_single . '" target="_blank"></a></li><li><i title="in Google Drive öffnen ohne Doppelrankings" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename_single . '"></i></li><li id="ops_multiple_show" class="info-sortable">Nur mehrfache Rankings anzeigen</li><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li></ul></div><div class="box-content"><table class="table table-normal data-table"><thead><tr><td style="width:50px;">#</td><td>Keyword</td><td>Tags</td><td class="nosearch">OneDex</td><td class="nosearch">OPI</td><td class="nosearch">SV</td><td class="nosearch">CPC</td><td class="nosearch">Wettb.</td><td  class="nosearch">Best</td>';

    $this->out = $outhead . $out;

  }


  /*
  SEARCH CONSOLE VS. SCRAPE
  https://oneproseo.advertising.de/dev-oneproseo/rankings/overview/2/detail-personal-index/1072
  */

  private function renderRankingViewPersonalIndex ()
  {

    $sc_data_host = array();
    if ($this->gwt_account == 1) {
      $sc_data_host = parent::getWebmasterToolsDataHost($this->hostname);
    }

    $best_rankings = parent::loadCacheFileBest($this->project_id);

    $ts = current($this->rankset['_ts']);

    if (!isset($this->url_campaign)) {
      foreach ($this->keywords as $keyword) {
        if (!isset($this->rankset['_da'][$keyword])) {
           $this->rankset['_da'][$keyword] = '';
        }
      }
    }


    $outhead = '';
    $out     = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition', 'Best Rank', 'Rank Search Console', '+/-');

    if (empty($this->rankset['_da'])) {
      $this->hintEmpty();
      exit;
    }


    // sort by key
    ksort($this->rankset['_da']);
    // DATES
    $out .= '<td class="nosearch">Rank neutral - '. parent::germanTS($ts) .'</td>';
    array_push($this->csvarr['head'], $ts);

		$out .= '<td>+/-</td>';
    $out .= '<td class="nosearch">Rankende URL</td></tr></thead><tbody>';
    array_push($this->csvarr['head'], 'Rankende URL');

    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      if (isset($best_rankings[$keyword])) {
        $best_rank = $best_rankings[$keyword][0];
        $best_date = $best_rankings[$keyword][1];
      } else {
        $best_rank = '';
        $best_date = '';
      }

      if (isset($sc_data_host[$keyword][0])) {
        $sc_rank = $sc_data_host[$keyword][0];
      } else {
        $sc_rank = '-';
      }

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank"><a href="'.parent::googleSearchDomain($this->country).'&q=' . $keyword . '" target="_blank">' . $keyword . '</a><small class="rankings"><a target="_blank" href="https://www.google.com/webmasters/tools/search-analytics?hl=de&siteUrl='.$this->domain.'#state=[null,[[null,null,null,28]],null,[[null,2,[%22'.urlencode($keyword).'%22]],[null,6,[%22WEB%22]]],null,[1,2,3,4],1,0,null,[2]]">GSC</a> | <a target="_blank" href="https://de.sistrix.com/seo/serps/keyword/' . $keyword . '">SISTRIX</a> | <a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">TRENDS</a></small></td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {

        $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

      } else {

      // ONE DEX FETCH DATA FOR CALC
        $latest_ts = array_keys($this->rankset['_ts']);
        $latest_ts = $latest_ts[0];
        if (isset($timestamp[$latest_ts][0])) {
          $rank = $timestamp[$latest_ts][0]['rank'];
        } else {
          $rank = 101;
        }
        $opi = $this->googleadwords[$keyword][3];
        $dex_format = $this->calcOneDex($rank, $opi);
        $dex_format = $this->nufo($dex_format);

      // ONE DEX FETCH DATA FOR CALC

        $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';
        $out .= '<td data-order="'.$dex_format.'" class="gadw tdright"> ' . $dex_format . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][3])  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][0].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][0], 0)  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][1].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][1])  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][2].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][2])  . ' </td>';

      }

      $this->csvarr[$i] = array(
          $i, 
          $keyword, 
          $dex_format, 
          $this->nufo($this->googleadwords[$keyword][3]), 
          $this->nufo($this->googleadwords[$keyword][0], 0), 
          $this->nufo($this->googleadwords[$keyword][1]), 
          $this->nufo($this->googleadwords[$keyword][2]), 
          $best_rank,
          $sc_rank
        );

      $out .= '<td data-order="' . $best_rank . '" class="gadw tdright"><span class="best" title="Datum: ' . $best_date . '">' . $best_rank . '</span></td>';
      $out .= '<td data-order="' . $sc_rank . '" class="rank"><span>' . $sc_rank . '</span></td>';      

      // RANKING SETS
      if (!isset($timestamp[$ts])) {

				$out .= '<td></td>';
        $out .= '<td data-order="999" class="rank ops_single"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';
        $rank_url = '-';
        array_push($this->csvarr[$i], '-', '-', '-');

      } else {

         // check for multiple rankings
        if (count($timestamp[$ts]) > 1) {

          // sort multiple rankings, lowest first
          krsort($timestamp[$ts]);

          $fv = reset($timestamp[$ts]);

          if ($fv['rank'] == 1) {
            $out .= '<td class="rank ops_single"';
          } else {
            $out .= '<td class="rank"';
          }

          $k = 0;

          $matched_url = array();

          foreach ($timestamp[$ts] as $value) {

            // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
            if (isset($matched_url[$value['url']])) {
              continue;
            }

            $matched_url[$value['url']] = $value['url'];

            if ($k == 0) {							
              $csv = $value['rank'];
              $csvu = $value['url'];
              $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              $rank_url = '<a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
            } else {
              $csv .= ' | ' . $value['rank'];
              $csvu .= ' | ' . $value['url'];
              $out .= ' <span class="multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              $rank_url .= '<br /><a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
            }
            $k++;
          }



          $out .= '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small>';
          $out .= '</td>';

          $change = $this->checkDifferenceBetweenRankings($sc_rank, $timestamp[$ts][0]['rank']);
          $change_csv = $this->checkDifferenceBetweenRankings($sc_rank, $timestamp[$ts][0]['rank'], false);
					$out .= '<td>'.$change.'</td>';

          array_push($this->csvarr[$i], $change_csv, $csv, $csvu);

        } else {

          foreach ($timestamp[$ts] as $value) {

            if (empty($value['rank'])) {
              $out .= '<td data-order="999" class="rank ops_single"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';
            } else {
              $out .= '<td data-order="'.$value['rank'].'" class="rank ops_single"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small></td>';
            }

            $change = $this->checkDifferenceBetweenRankings($sc_rank, $value['rank']);
            $change_csv = $this->checkDifferenceBetweenRankings($sc_rank, $value['rank'], false);
            $out .= '<td>'.$change.'</td>';

            array_push($this->csvarr[$i], $change_csv, $value['rank'], $value['url']);

            $rank_url = '<a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
          }

        }

      }

      $out .= '<td class="rank-url">' . $rank_url . '</td>';
      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_personal_index');

    $outhead .='<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div><span class="titleleft">Ansicht: Personal Index</span><ul class="box-toolbar">';

    $outhead .= parent::submenu();
    $outhead .= $this->renderEditSet();
    $outhead .= $this->personalDashboard();

    $outhead .='</ul></div>
      <div class="box-content padded">
        <p>Diese Ansicht vergleicht die Rankings der Searchconsole mit den neutralen Rankings.</p>
      </div></div>
      <div class="box"><div class="box-header"><span class="title">Personal Index für '.$this->setname.'</span><ul class="box-toolbar">
      <li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li></ul></div><div class="box-content"><table class="table table-normal data-table"><thead><tr><td style="width:50px;">#</td><td>Keyword</td><td>Tags</td><td class="nosearch">OneDex</td><td class="nosearch">OPI</td><td class="nosearch">SV</td><td class="nosearch">CPC</td><td class="nosearch">Wettb.</td><td  class="nosearch">Best</td><td class="nosearch">Rank Search Console</td>';

    $this->out = $outhead . $out;
  }


  private function oneDexAccumulated ($rankset) {

    if (!is_array($rankset)) {
      return 0;
    }

    $dex_cumm = 0;

    foreach ($rankset as $key => $data) {

      if (isset($data['rank'])) {
        $rank = $data['rank'];
      } else {
        $rank = 101;
      }
      $opi = $this->googleadwords[$data['keyword']][3];
      $dex = $this->calcOneDex($rank, $opi);
      $dex_cumm = $dex_cumm + $dex;

    }

    return $dex_cumm;

  }


  private function renderEditSet () {

    if ($_SESSION['role2'] == 'admin' && $this->keyword_setid != 'all') {
      return '<li><a target="_blank" href="../keywords/'.$this->keyword_setid.'"> <i title="Edit set" class="icon-edit setedit"></i> </a></li>';
    }

  }


  private function personalDashboard()
  {

    if (isset($_SESSION['ruk_personal_dash'])) {
      foreach ($_SESSION['ruk_personal_dash'] as $key => $value) {
        if ($value['setid'] == $this->keyword_setid && $value['projectid'] == $this->customerid) {
          return '<li><i title="Already in personal dashboard" class="icon-ok"></i></li>';
        }
      }
    }

    if (isset($_SESSION['userid'])) {
      return '<li><i data-setid="'.$this->keyword_setid.'" data-projectid="'.$this->customerid.'" title="Add to personal dashboard" class="add-sortable icon-plus-sign"></i></li>';
    }

  }

  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {
      echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br/>Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</strong></div></div></div>';
      exit;
    }

  }

  private function hintEmpty () {
    echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br/>Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</strong></div></div></div>';
    exit;
  }


  private function sevenDays ()
  {

    if ($this->is_weekly === true) {return;}

    if (!empty($this->days)) {
      return '<li><a target="_blank" href="../'.$this->keyword_setid.'"><span class="label label-gray">7 Tage Ansicht</span></a></li>';
    } else {
      return '<li><a target="_blank" href="'.$this->keyword_setid.'/31"><span class="label label-gray">31 Tage Ansicht</span></a></li>';
    }

  }

}

?>
