<?php

require_once('ruk.class.php');

class api extends ruk
{

  private $keyword_set_all = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['projectid'])) {

      $this->project_id = $_POST['projectid'];

      $this->getProjectData();
      
      $this->renderView();

    }

  }

  private function getProjectData()
  {

  	$this->campaigns = array();

    $sql = "SELECT
              a.id             AS aid,
              a.name           AS name,
              a.api_key        AS api_key,
              b.id             AS setid,
              b.name           AS setname,
              b.type           AS type,
              b.customer_view  AS customer_view
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
            WHERE a.id = '".$this->project_id."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->campaigns[]  = $row;
      $this->project_name = $row['name'];
      $this->api_key      = $row['api_key'];
    }

  }


  private function renderView ()
  {

    $out = '
      <div class="row">
       <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <span class="title">APIs Projekt: '.$this->project_name.'</span>
          </div>
          <div class="box-content">
            <table class="table table-normal">
							<thead>
								<tr>
								  <td>Daten</td>
								  <td>URL</td>
								  <td>Link</td>
								</tr>
							</thead>
              <tbody>
                <tr>
                  <td>OneDex - 7 Tage:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
                <tr>
                  <td>OneDex - 31 Tage:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_31.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_31.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_31.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
                <tr>
                  <td>OneDex - 12 Wochen:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_weekly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_weekly.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_weekly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>                
                <tr>
                  <td>OneDex - 12 Monate:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_monthly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_monthly.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_all_monthly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>                
                <tr>
                  <td>Keyword Rankings, 1 Jahr (Monatsbasis):</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_all_monthly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_all_monthly.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_all_monthly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
                <tr>
                  <td>Keyword Rankings inkl. Wettbewerber (aktuell):</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/competition_'.$this->project_id.'_all.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/competition_'.$this->project_id.'_all.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/competition_'.$this->project_id.'_all.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
                <tr>
                  <td>Rankings URL, Keyword, Suchvolumen (aktuell):</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_all.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_all.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_all.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>                
                <tr>
                  <td>Gewonnene Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_all.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_all.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_all.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
                <tr>
                  <td>Unveränderte Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_all.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_all.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_all.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
                <tr>
                  <td>Verlorene Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_all.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_all.json</a></td>
                  <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_all.json" target="_blank"><i class="icon-external-link"></i></a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>


      <div class="row">
       <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <span class="title">APIs Kampagnen: '.$this->project_name.'</span>
          </div>
          <div class="box-content">
            <table class="table table-normal">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Name</td>
                  <td>API</td>
                </tr>
              </thead>            
              <tbody>';



        foreach ($this->campaigns as $arr) {

          if (empty($arr['setname'])) {
            continue;
          }

          if ($this->env_data['customer_version'] > 0) {

            if ($arr['customer_view'] == 0) {
              continue;
            }

          }

          if ($arr['type'] == 'url') {

            $out .= '
              <tr>
                <td><a href="keywords/'.$arr['setid'].'" title="bearbeiten">'.$arr['setid'].'</td>
                <td><a href="detail-keyword/'.$arr['setid'].'" title="Rankings ansehen">'.$arr['setname'].'<br />(URL Kampagne)</a></td>
                <td>
                  <table class="table table-normal">
                    <tr>
                      <td>Daten</td>
                      <td>URL</td>
                      <td>Link</td>
                    </tr>               
                    <tr>
                      <td>OneDex - 7 Tage:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr> 
                      <td>OneDex - 31 Tage:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_31.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_31.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_31.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>OneDex - 12 Wochen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_weekly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_weekly.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_weekly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>OneDex - 12 Monate:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_monthly.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_monthly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Keyword Rankings, 1 Jahr, (Monatsbasis):</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Rankings URL, Keyword, Suchvolumen (aktuell):</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>  
                    <tr>
                      <td>Gewonnene Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Unveränderte Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Verlorene Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>

                  </table>
                </td>
              </tr>
            ';

          } else {


            $out .= '
              <tr>
                <td><a href="keywords/'.$arr['setid'].'" title="bearbeiten">'.$arr['setid'].'</td>
                <td><a href="detail-keyword/'.$arr['setid'].'" title="Rankings ansehen">'.$arr['setname'].'<br />(Keyword Kampagne)</a></td>
                <td>
                  <table class="table table-normal">
                    <tr>
                      <td>Daten</td>
                      <td>URL</td>
                      <td>Link</td>
                    </tr>               
                    <tr>
                      <td>OneDex - 7 Tage:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr> 
                      <td>OneDex - 31 Tage:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_31.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_31.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_31.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>OneDex - 12 Wochen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_weekly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_weekly.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_weekly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>OneDex - 12 Monate:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_monthly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_monthly.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/chart_'.$this->project_id.'_' . $arr['setid'] . '_monthly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Keyword Rankings, 1 Jahr (Monatsbasis):</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/rankings_'.$this->project_id.'_' .  $arr['setid'] . '_monthly.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>                      
                    <tr>
                      <td>Rankings URL, Keyword, Suchvolumen (aktuell):</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/url_rankings_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Rankings Keyword, Wettbewerber (aktuell):</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/competition_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/competition_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/competition_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Gewonnene Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_winner_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Unveränderte Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_same_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>
                    <tr>
                      <td>Verlorene Rankings -> Keyword, Rank, URL, Change, Best, Dex, Suchvolumen:</td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank">https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_' . $arr['setid'] . '.json</a></td>
                      <td><a href="https://enterprise.oneproseo.com/api/v1/'.$this->api_key.'/wl_loser_'.$this->project_id.'_' . $arr['setid'] . '.json" target="_blank"><i class="icon-external-link"></i></a></td>
                    </tr>                    
                  </table>
                </td>
              </tr>
            ';

          }

        }

      $out .='</tbody>
            </table>
          </div>
        </div>
      </div>';


    echo $out;

  }

}

?>
