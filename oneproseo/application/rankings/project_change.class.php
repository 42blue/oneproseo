<?php

require_once('ruk.class.php');

class project_change extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['projectid']) && isset($_POST['change'])) {

       $this->projectid        = $_POST['projectid'];
       $this->url              = $_POST['url'];
       $this->manager          = strip_tags($_POST['manager']);
       $this->name             = strip_tags($_POST['name']);
       $this->teammail         = strip_tags($_POST['team']);
       $this->category         = strip_tags($_POST['category']);
       $this->type             = strip_tags($_POST['type']);
       $this->country          = strip_tags($_POST['country']);
       $this->local            = strip_tags($_POST['local_rankings']);
       $this->ignore_subdomain = strip_tags($_POST['ignore_subdomain']);
       $this->competition      = strip_tags($_POST['competition']);
       $this->brandkeywords    = strip_tags($_POST['brandkeywords']);

       $this->updateData();

    }

    if (isset($_POST['projectid'])) {

       $this->projectid = $_POST['projectid'];

       $this->fetchData();
       $this->renderView();

    }


  }


  private  function renderView() {

    $comp = '';
    if (!empty($this->competition)) {
      $comp =  implode ("\n", unserialize($this->competition));
    }

    $brands = '';
    if (!empty($this->brandkeywords)) {
      $brands =  implode ("\n", unserialize($this->brandkeywords));
    }

    if ($_SESSION['role2'] != 'admin') {
      $disabled = 'readonly="readonly"';
    }

    $available = array();
    $available['de'] = 'Germany';
    $available['al'] = 'Albania';
    $available['dz'] = 'Algeria';
    $available['ar'] = 'Argentina';
    $available['am'] = 'Armenia';
    $available['au'] = 'Australia';
    $available['at'] = 'Austria';
    $available['az'] = 'Azerbaijan';
    $available['bs'] = 'Bahamas';
    $available['bh'] = 'Bahrain';
    $available['bd'] = 'Bangladesh';
    $available['by'] = 'Belarus';
    $available['be'] = 'Belgium (FR)';
    $available['be-nl'] = 'Belgium (NL)';
    $available['bo'] = 'Bolivia';
    $available['ba'] = 'Bosnia and Herzegovina';
    $available['br'] = 'Brazil';
    $available['bn'] = 'Brunei Darussalam';
    $available['bg'] = 'Bulgaria';
    $available['kh'] = 'Cambodia';
    $available['ca'] = 'Canada (EN)';
    $available['ca-fr'] = 'Canada (FR)';
    $available['cl'] = 'Chile';
    $available['hk'] = 'China (HK)';
    $available['co'] = 'Colombia';
    $available['cr'] = 'Costa Rica';
    $available['hr'] = 'Croatia';
    $available['cy'] = 'Cyprus';
    $available['cz'] = 'Czechia';
    $available['dk'] = 'Denmark';
    $available['do'] = 'Dominican Republic';
    $available['ec'] = 'Ecuador';
    $available['eg'] = 'Egypt';
    $available['sv'] = 'El Salvador';
    $available['ee'] = 'Estonia';
    $available['fi'] = 'Finland';
    $available['fr'] = 'France';
    $available['fr'] = 'French Antilles';
    $available['ge'] = 'Georgia';
    $available['gr'] = 'Greece';
    $available['gt'] = 'Guatemala';
    $available['ht'] = 'Haiti';
    $available['hn'] = 'Honduras';
    $available['hk'] = 'Hong Kong';
    $available['hu'] = 'Hungary';
    $available['is'] = 'Iceland';
    $available['in'] = 'India';
    $available['id'] = 'Indonesia';
    $available['iq'] = 'Iraq';
    $available['ie'] = 'Ireland';
    $available['il'] = 'Israel';
    $available['it'] = 'Italy';
    $available['jm'] = 'Jamaica';
    $available['jp'] = 'Japan';
    $available['jo'] = 'Jordan';
    $available['kz'] = 'Kazakhstan';
    $available['kw'] = 'Kuwait';
    $available['la'] = 'Lao';
    $available['lv'] = 'Latvia';
    $available['lb'] = 'Lebanon';
    $available['ly'] = 'Libya';
    $available['lt'] = 'Lithuania';
    $available['lu'] = 'Luxemburg (FR)';
    $available['lu-de'] = 'Luxemburg (DE)';
    $available['mk'] = 'Macedonia';
    $available['mw'] = 'Malawi';
    $available['my'] = 'Malaysia';
    $available['mt'] = 'Malta';
    $available['mu'] = 'Mauritius';
    $available['mx'] = 'Mexico';
    $available['md'] = 'Moldova';
    $available['ma'] = 'Morocco';
    $available['nl'] = 'Netherlands';
    $available['nz'] = 'New Zealand';
    $available['ni'] = 'Nicaragua';
    $available['no'] = 'Norway';
    $available['om'] = 'Oman';
    $available['pk'] = 'Pakistan';
    $available['ps'] = 'Palestinian Territory';
    $available['pa'] = 'Panama';
    $available['py'] = 'Paraguay';
    $available['pe'] = 'Peru';
    $available['ph'] = 'Philippines';
    $available['pl'] = 'Poland';
    $available['pt'] = 'Portugal';
    $available['qa'] = 'Qatar';
    $available['re'] = 'Reunion';
    $available['ro'] = 'Romania';
    $available['ru'] = 'Russia';
    $available['sa'] = 'Saudi Arabia';
    $available['rs'] = 'Serbia';
    $available['sg'] = 'Singapore';
    $available['si'] = 'Slovenia';
    $available['sk'] = 'Slowakia';
    $available['za'] = 'South Africa';
    $available['kr'] = 'South Korea';
    $available['es'] = 'Spain';
    $available['lk'] = 'Sri Lanka';
    $available['se'] = 'Sweden';
    $available['ch'] = 'Switzerland (DE)';
    $available['ch-fr'] = 'Switzerland (FR)';
    $available['ch-it'] = 'Switzerland (IT)';
    $available['pf'] = 'Tahiti';
    $available['tw'] = 'Taiwan';
    $available['th'] = 'Thailand';
    $available['tt'] = 'Trinidad and Tobago';
    $available['tn'] = 'Tunisia';
    $available['tr'] = 'Turkey';
    $available['ua'] = 'Ukraine';
    $available['ae'] = 'United Arab Emirates';
    $available['uk'] = 'United Kingdom';
    $available['uy'] = 'Uruguay';
    $available['us'] = 'USA';
    $available['vn'] = 'Vietnam';

    echo '<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="title">Projektdaten bearbeiten</span>
                </div>
                <div class="box-content padded">
                  <form class="form-horizontal fill-up" role="form" id="ops_project_change">
                    <div class="form-group">
                      <label for="name" class="col-sm-2 label-1 control-label">Projektname:</label>
                      <div class="col-sm-8">
                        <input '.$disabled.' type="text" class="form-control" id="name" name="name" required="required" value="'.$this->name.'">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="url" class="col-sm-2 label-1 control-label">URL:</label>
                      <div class="col-sm-8">
                        <input '.$disabled.' type="url" class="form-control" id="url" name="url" required="required" value="'.$this->url.'">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Subdomains überwachen:</label>
                      <div class="col-sm-8">
                        <div class="crawl-input-radio">
                          <input '.$disabled.' type="radio" name="ignore_subdomain" value="0" class="icheck" id="iradio3"';
                          if ($this->ignore_subdomain == 0) { echo  'checked="checked"'; }
                          echo '>
                          <label for="iradio1">Nein</label>
                        </div>
                        <div class="crawl-input-radio">
                          <input '.$disabled.' type="radio" name="ignore_subdomain" value="1" class="icheck" id="iradio4"';
                          if ($this->ignore_subdomain == 1) { echo  'checked="checked"'; }
                          echo '>
                          <label for="iradio2">Ja</label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="url" class="col-sm-2 label-1 control-label">Google Version:</label>
                      <div class="col-sm-8">
                        <select '.$disabled.' class="custom" name="country" disabled="disabled">
                          <option value="'.$this->country.'">'.$available[$this->country].'</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="manager" class="col-sm-2 label-1 control-label">SEO Project Manager:</label>
                      <div class="col-sm-8">
                        <input '.$disabled.' type="text" class="form-control" id="manager" name="manager" required="required" value="'.$this->manager.'">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="team" class="col-sm-2 label-1 control-label">E-Mail Empfänger der Reports: </label>
                      <div class="col-sm-8">
                        <textarea class="crawler" id="team" name="team" placeholder="Eine Emailadresse pro Zeile!">' . str_replace(',', "\n", $this->teammail) . '</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="team" class="col-sm-2 label-1 control-label">Wettbewerb überwachen:</label>
                      <div class="col-sm-8">
                        <textarea class="crawler" id="competition" name="competition" placeholder="Eine URL pro Zeile! (max. 8)">' . $comp . '</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="team" class="col-sm-2 label-1 control-label">Brand Keywords:</label>
                      <div class="col-sm-8">
                        <textarea class="crawler" id="brandkeywords" name="brandkeywords" placeholder="Ein Keyword pro Zeile!">' . $brands . '</textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="category" class="col-sm-2 label-1 control-label">Kategorie:</label>
                      <div class="col-sm-8">
                        <select '.$disabled.' class="custom" name="category">';

                          if ($this->category == 'brand') {
                            echo '<option value="brand" selected>Brand</option>';
                          } else {
                            echo '<option value="brand">Brand</option>';
                          }

                          if ($this->category == 'electronic-retail') {
                            echo '<option value="electronic-retail" selected>Electronic Retail</option>';
                          } else {
                            echo '<option value="electronic-retail">Electronic Retail</option>';
                          }

                          if ($this->category == 'fashion') {
                            echo '<option value="fashion" selected>Fashion</option>';
                          } else {
                            echo '<option value="fashion">Fashion</option>';
                          }

                          if ($this->category == 'retail') {
                            echo '<option value="retail" selected>Retail</option>';
                          } else {
                            echo '<option value="retail">Retail</option>';
                          }

                          if ($this->category == 'travel') {
                            echo '<option value="travel" selected>Travel</option>';
                          } else {
                            echo '<option value="travel">Travel</option>';
                          }

          echo          '</select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="type" class="col-sm-2 label-1 control-label">Typ:</label>
                      <div class="col-sm-8">
                        <select '.$disabled.' class="custom" name="type">';
                          if ($this->type == 'competitor') {
                            echo '<option value="competitor" selected>Competitor</option>';
                          } else {
                            echo '<option value="competitor">Competitor</option>';
                          }
                          if ($this->type == 'customer') {
                            echo '<option value="customer" selected>Customer</option>';
                          } else {
                            echo '<option value="customer">Customer</option>';
                          }
                          if ($this->type == 'one_internal') {
                            echo '<option value="one_internal" selected>One Intern</option>';
                          } else {
                            echo '<option value="one_internal">One Intern</option>';
                          }
                          if ($this->type == 'new_business') {
                            echo '<option value="new_business" selected>New Business</option>';
                          } else {
                            echo '<option value="new_business">New Business</option>';
                          }
                        echo'</select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Local Rankings aktivieren<br/>(nur für DE/AT/CH):</label>
                      <div class="col-sm-8">
                        <div class="crawl-input-radio">
                          <input '.$disabled.' type="radio" name="local_rankings" value="0" class="icheck" id="iradio3"';
                          if ($this->local == 0) { echo  'checked="checked"'; }
                          echo'>
                          <label for="iradio1">Nein</label>
                        </div>
                        <div class="crawl-input-radio">
                          <input '.$disabled.' type="radio" name="local_rankings" value="1" class="icheck" id="iradio4"';
                          if ($this->local == 1) { echo  'checked="checked"'; }
                          echo '>
                          <label for="iradio2">Ja</label>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="projectid" value="'.$this->projectid.'">
                    <input type="hidden" name="change" value="true">
                    <div class="form-group">
                      <div class="col-sm-2 control-label"></div>
                      <div class="col-sm-8">
                        <button type="submit" class="btn btn-primary btn-lg" id="ops_project_change_submit">Projektdaten speichern</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>';

  }


  private function fetchData()
  {

    // get project data

    $sql = "SELECT id, url, country, manager, name, team_email, category, type, local, ignore_subdomain, competition, brandkeywords
            FROM ruk_project_customers
            WHERE id = '".$this->projectid."'";

    $this->result = $this->db->query($sql);

    if (!$this->result) {
      $this->mySqlQueryError();
    }

    while ($row = $this->result->fetch_assoc()) {

      $this->name             = $row['name'];
      $this->url              = $row['url'];
      $this->country          = $row['country'];
      $this->manager          = $row['manager'];
      $this->teammail         = $row['team_email'];
      $this->category         = $row['category'];
      $this->type             = $row['type'];
      $this->local            = $row['local'];
      $this->ignore_subdomain = $row['ignore_subdomain'];
      $this->competition      = $row['competition'];
      $this->brandkeywords    = $row['brandkeywords'];

    }

  }


  private function parseEmail() {

    $unparsed_teammail  = strip_tags($this->teammail);
    $unparsed_teammail  = explode("\n", $unparsed_teammail);

    $parsed_teammail = array();
    foreach ($unparsed_teammail as $mailadress) {
      $mailadress = strtolower(trim($mailadress));
      if (filter_var($mailadress, FILTER_VALIDATE_EMAIL)) {
        $parsed_teammail[] = $mailadress;
      }
    }

    $parsed_teammail = implode(',', $parsed_teammail);

    $this->teammail = $parsed_teammail;

  }



  private function parseUrl() {

    $unparsed_competition  = strip_tags($this->competition);
    $unparsed_competition  = explode("\n", $unparsed_competition);

    $parsed_competition = array();
    foreach ($unparsed_competition as $url) {
      $url = strtolower(trim($url));
      if (filter_var($url, FILTER_VALIDATE_URL)) {
        $parsed_competition[] = $url;
      }
    }

    // 8 max
    if (count($parsed_competition) > 8) {
      $parsed_competition = array_slice($parsed_competition,0,8);
    }

    $parsed_competition = serialize($parsed_competition);

    $this->competition = $parsed_competition;

  }


  private function updateData()
  {

    $this->parseEmail();
    $this->competition = $this->parseCompetitionUrl($this->competition);
    $this->brandkeywords = $this->parseBrandKeywords($this->brandkeywords);

    $sql = "UPDATE
              ruk_project_customers
            SET
              url              = '$this->url',
              name             = '$this->name',
              manager          = '$this->manager',
              team_email       = '$this->teammail',
              category         = '$this->category',
              type             = '$this->type',
              local            = '$this->local',
              ignore_subdomain = '$this->ignore_subdomain',
              competition      = '$this->competition',
              brandkeywords    = '$this->brandkeywords'
            WHERE
              id = '$this->projectid'";

    $result = $this->db->query($sql);

    if (!$result) {
      die ('DB ERROR: '.$this->db->error);
    }

  }

}

?>
