<?php

require_once('ruk.class.php');

/*

Erzeugt die Detailansicht der Keywords und rankenden URLs
https://datatables.net/examples/plug-ins/dom_sort

*/


class local_rankings_details extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['setid'])) {

      $this->type          = $_POST['type'];
      $this->keyword_setid = $_POST['setid'];
      $this->days          = $_POST['days'];
      $this->project_id    = $_POST['projectid'];
      if (!empty($this->days)) {
        $this->days = '_31';
      } else {
        $this->days = '';
      }

      // 1.
      // CAMPAIGN TYPE? URL or KEYWORD
      $this->campaign_type = $this->checkCampainTypeLocal($this->keyword_setid);

      // GET POSSIBLE RS LOCATIONS
      $this->rs_locations  = $this->getLocationsArray();

      // 2.
      // CHOOSE SINGLE KEYWORD SET OR ALL
      if ($this->keyword_setid == 'all' || $this->campaign_type == 'url') {
        $this->selectAllKeywordSets();
      } else {
        $this->selectKeywordSet();
      }

      // 3.
      // IF URL SET FETCH URLS
      if ($this->campaign_type == 'url') {
        $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
      }

      // 4.
      // FETCH RANKING DATA AND OUTPUT

      // output for keyword view
      if ($this->type == 'keyword') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewKeyword();
      }

      // output for url view
      if ($this->type == 'url') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewUrl();
      }

      // output for url view complete database
      if ($this->type == 'complete') {
        $this->getRankingDataComplete(); // <- NO intercept keyword / url -> LOOSE QUERY
        $this->renderRankingViewUrl();
      }

      // output for dex
      if ($this->type == 'dex') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewUrlDex();
      }

      // output for dex
      if ($this->type == 'dexcomplete') {
        $this->getRankingDataComplete(); // <- NO intercept keyword / url -> LOOSE QUERY
        $this->renderRankingViewUrlDex();
      }

      // output for url view
      if ($this->type == 'keyword-url') {
        $this->getRankingData(); // <- intercept keyword / url
        $this->renderRankingViewKeywordUrl();
      }

      $this->checkForRankingsHint();

      echo $this->out;

    }

  }


  private function selectKeywordSet ()
  {

    $this->keywords    = array();
    $this->keyword_loc = array();
    $this->locations   = array();

    $selected_keyword_set = $this->keyword_setid;

    $sql = "SELECT a.id               AS setid,
                   a.name             AS setname,
                   a.rs_type          AS rstype,
                   b.keyword          AS keyword,
                   b.location         AS location,
                   c.url              AS url,
                   c.id               AS customerid,
                   c.ga_account       AS ga_account,
                   c.name             AS customername,
                   c.country          AS country,
                   c.ignore_subdomain AS ignore_subdomain
            FROM ruk_local_keyword_sets a
              LEFT JOIN ruk_local_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {

      $this->keywords[]                  = $row['keyword'];
      $this->keyword_loc[]               = array ($row['keyword'], $row['location']);
      $this->locations[$row['location']] = $row['location'];
      $url                               = $row['url'];
      $this->setname                     = $row['setname'];
      $this->customername                = $row['customername'];
      $this->customerid                  = $row['customerid'];
      $this->country                     = $row['country'];
      $this->rs_type                     = $row['rstype'];
      $this->setid                       = $row['setid'];
      $this->ignore_subd                 = $row['ignore_subdomain'];

    }

    $this->keyword_amount  = count($this->keywords);
    $this->hostname        = parent::pureHostName($url);
    $this->domain          = rtrim($url, '/');

  }


  private function selectAllKeywordSets ()
  {

    $selected_keyword_set = $this->keyword_setid;
    $selected_project     = $this->project_id;

    $this->keywords    = array();
    $this->keyword_loc = array();
    $this->locations   = array();

    $sql = "SELECT a.id               AS setid,
                   b.keyword          AS keyword,
                   b.location         AS location,
                   c.url              AS url,
                   c.id               AS customerid,
                   c.ga_account       AS ga_account,
                   c.name             AS customername,
                   c.country          AS country,
                   c.ignore_subdomain AS ignore_subdomain
            FROM ruk_local_keyword_sets a
              LEFT JOIN ruk_local_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {

      $url = $row['url'];
      $this->customername                = $row['customername'];
      $this->customerid                  = $row['customerid'];
      $this->country                     = $row['country'];
      $this->locations[$row['location']] = $row['location'];
      $this->setid                       = $row['setid'];
      $this->ignore_subd                 = $row['ignore_subdomain'];

      if (!empty($row['keyword'])) {
        $this->keywords[]   = $row['keyword'];
        $this->keyword_loc[] = array ($row['keyword'], $row['location']);
      }

    }

    if ($this->campaign_type == 'key') {
      $this->setname = 'Alle Keywords';
    } else {
      $this->setname = $this->getCampaignNameLocal($selected_keyword_set);
    }

    $this->keyword_amount = count($this->keywords);
    $this->hostname       = parent::pureHostName($url);
    $this->domain         = rtrim($url, '/');

  }




  private function getRankingData()
  {

    $hostname = $this->hostname;

    $rows = parent::loadCacheFileLocal($this->customerid, $this->days);

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (isset($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }
// COMPARE OPTIONS

    foreach ($rows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        if ($this->type == 'dexcomplete-analytics') {
          $row['t'] = parent::dateYMD();
        }
        // CLICKS
        $cl = '';
        if (isset($row['cl'])) {
          $cl = $row['cl'];
        }
        // SHOW ONLY LOCATIONS SPECIFIED IN THE SET
        if (!$this->locations[$row['l']]) {
          continue;
        }
        // SHOW ONLY SEARCH TYPE SPECIFIED IN THE SET
        if ($this->rs_type != $row['rs']) {
          continue;
        }

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

        if ($this->type == 'keyword' || $this->type == 'keyword-url') {
          $this->rankset['_da'][$row['k']][$row['l']][$row['t']][$row['rs']][] = array ('rank' => $row['p'], 'ts' => $row['t'], 'url' => $row['u'], 'cl' => $cl);
        } else if ($this->type == 'dexcomplete-analytics') {
          if (!empty($row['u'])) {
            $this->rankset['_da'][$row['u']][$row['t']][] = array ('keyword' => $row['k'], 'rank' => $row['p'], 'cl' => $cl);
          }
        } else {
          if (!empty($row['u'])) {
            $this->rankset['_da'][$row['u']][$row['l']][$row['t']][$row['rs']][] = array ('keyword' => $row['k'], 'rank' => $row['p'], 'cl' => $cl);
          }
        }

      }

    }

    // ADWORDS, ANALYTICS, GWT DATA
    $this->googleadwords = parent::getAdwordsDataLocal($this->keyword_loc);
    $this->googlewebmastertools = parent::getWebmasterToolsData($this->keywords);

    if ($this->type == 'dex' || $this->type == 'dexcomplete' || $this->type == 'dexcomplete-analytics') {
      $this->googleanalyticsAll = parent::getAnalyticsData($hostname);
    }

    $this->checkForRankingsHint();

  }


  // SEARCH THE COMPLETE DATABASE

  private function getRankingDataComplete()
  {

    $hostname = $this->hostname;

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (isset($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }
// COMPARE OPTIONS


    // get all keyword rankings
    // where hostname

    $sql = "SELECT
              id,
              keyword,
              location,
              rs_type,
              timestamp,
              language
            FROM
              ruk_scrape_keywords_local
            WHERE
              language = '$this->country'
            AND
              rs_type = '$this->rs_type'
            AND
              DATE(timestamp) = CURDATE()";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $rows[] = $row;
    }

    $nrows = array();

    foreach ($rows as $k => $v) {

      $idkw = $v['id'];

      $sql = "SELECT
                position,
                url,
                id_kw,
                hostname
              FROM
                ruk_scrape_rankings_local
              WHERE
                id_kw = '$idkw'";

      $result2 = $this->db->query($sql);

      while ($row = $result2->fetch_assoc()) {

        if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
          $nrows[] = array('k' => $v['keyword'], 't' => parent::dateYMD(), 'p' => $row['position'], 'u' => $row['url'], 'l' => $v['location'], 'rs' => $v['rs_type']);
        }

      }

    }


    // CREATE RESULT ARRAY
    foreach ($nrows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        // SHOW ONLY LOCATIONS SPECIFIED IN THE SET
        if (!$this->locations[$row['l']]) {
          continue;
        }
        // SHOW ONLY SEARCH TYPE SPECIFIED IN THE SET
        if ($this->rs_type != $row['rs']) {
          continue;
        }

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

        if ($this->type == 'keyword' || $this->type == 'keyword-url') {
          $this->rankset['_da'][$row['k']][$row['l']][$row['t']][$row['rs']][] = array ('rank' => $row['p'], 'ts' => $row['t'], 'url' => $row['u']);
        } else if ($this->type == 'dexcomplete-analytics') {
          if (!empty($row['u'])) {
            $this->rankset['_da'][$row['u']][$row['t']][] = array ('keyword' => $row['k'], 'rank' => $row['p'], 'cl' => $cl);
          }
        } else {
          if (!empty($row['u'])) {
            $this->rankset['_da'][$row['u']][$row['l']][$row['t']][$row['rs']][] = array ('keyword' => $row['k'], 'rank' => $row['p']);
          }
        }

      }

    }

    // ADWORDS, ANALYTICS, GWT DATA
    $this->googleadwords = parent::getAdwordsDataLocal($this->keyword_loc);
    $this->googlewebmastertools = parent::getWebmasterToolsData($this->keywords);

    if ($this->type == 'dex' || $this->type == 'dexcomplete' || $this->type == 'dexcomplete-analytics') {
      $this->googleanalyticsAll = parent::getAnalyticsData($hostname);
    }

    $this->checkForRankingsHint();

  }


  /*
  KEYWORD -> DAILY RANKINGS -> CHANGE
  https://oneproseo.advertising.de/dev-oneproseo/rankings/local/23/detail-keyword/93
  */

  private function renderRankingViewKeyword ()
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'Ort', 'OneDex', 'OneDex Max','OneDex Diff', 'OneDex Potential', 'Boost Potential', 'OPI', 'Search Volume', 'CPC', 'Competition');

    // merge ranking result with keywordset for keywords that do not rank
    if (!isset($this->url_campaign)) {
      foreach ($this->keyword_loc as $id => $kw_loc) {
        if (!isset($this->rankset['_da'][$kw_loc[0]][$kw_loc[1]])) {
           $this->rankset['_da'][$kw_loc[0]][$kw_loc[1]] = '';
        }
      }
    }

    // sort by key
    ksort($this->rankset['_da']);

    // TABLE HEADER - DATES
    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $ii++;
      $out .= '<td>Rank - '. parent::germanTS($ts) .'</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td style="width:50px;">Change</td>';
      }
      array_push($this->csvarr['head'], 'Rank ' . $ts);
      array_push($this->csvarr['head'], 'Change');
      array_push($this->csvarr['head'], 'URL');
    }
    $out .= '</tr></thead><tbody>';


    // TABLE BODY - CONTENT
    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $set) {

      foreach ($set as $loc => $timestamp) {

        $i++;

        $out .= '<tr>';
        $out .= '<td class="rank">' . $i . '</td>';
        $out .= '<td class="rank">
                   <a title="Dieses Keyword für den Ort '.$this->rs_locations[$loc].' abfragen" target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->customerid.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">' . $keyword . '</a>
                   <a title="Das Keyword: '.$keyword.' für alle Orte in diesem Set abfragen" target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->customerid.'/set/locations/'.$this->setid.'/'. urlencode($keyword) .'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><i class="icon-comment right"></i></a>
                 </td>';
        $out .= '<td class="rank">' . $this->rs_locations[$loc] . '
                    <a title="Das komplette Keywordset für den Ort '.$this->rs_locations[$loc].' abfragen" target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->customerid.'/set/keywords/'.$this->setid.'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><i class="icon-comment-alt right"></i></a>
                 </td>';

        // NO ADWORDS DATA
        if (!isset($this->googleadwords[$keyword][$loc])) {

          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

        // ADWORDS DATA
        } else {

        // ONE DEX FETCH DATA FOR CALC

          $latest_ts = array_keys($this->rankset['_ts']);
          $latest_ts = $latest_ts[0];

          if (isset($timestamp[$latest_ts][$this->rs_type][0])) {
            $rank = $timestamp[$latest_ts][$this->rs_type][0]['rank'];
          } else {
            $rank = 101;
          }

          $opi      = $this->googleadwords[$keyword][$loc][3];
          $dex      = $this->calcOneDex($rank, $opi, true);
          $dex_max  = $this->calcOneDexMax($opi);
          $dex_diff = $dex_max - $dex;

          $dex_format      = number_format($dex, 3, ',', '.');
          $dex_max_format  = number_format($dex_max, 3, ',', '.');
          $dex_diff_format = number_format($dex_diff, 3, ',', '.');

          $potential = $dex_diff * $this->googleadwords[$keyword][$loc][2] * $this->googleadwords[$keyword][$loc][1] * $this->googleadwords[$keyword][$loc][1];

          $potential_format = number_format($potential, 3, ',', '.');

          $potential_indicate = 0;
          $potential_indicate_format = '';
          $potential_indicate_format_csv = '';

          if (isset($this->googlewebmastertools[$keyword])) {
            $potential_indicate_format  = '<span class="data-tooltip" data-tooltip="Grün: Großes Potential - Gutes Ranking vorhanden (ToDo: Keine)"><i class="status-success icon-certificate"></i></span>';
            $potential_indicate_format_csv  = 'high';
            $potential_indicate = 1;
            // rank today > 10
            if (!isset($this->rankset['_da'][$keyword][$loc][$latest_ts][$this->rs_type][0]['rank']) || $this->rankset['_da'][$keyword][$loc][$latest_ts][$this->rs_type][0]['rank'] > 7) {
              $potential_indicate_format  = '<span class="data-tooltip" data-tooltip="Rot: Großes Potential - Kein gutes Ranking vorhanden (ToDo: Mit &#34;Campaign Driven&#34; optimieren)"><i class="status-error icon-certificate"></i></span>';
              $potential_indicate_format_csv  = 'very high';
              $potential_indicate = 2;
            }
          }


        // ONE DEX FETCH DATA FOR CALC
          $out .= '<td data-order="'.$dex.'" class="gadw tdright"> ' . $dex_format . ' </td>';
          $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][3].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][3], 0, ',', '.')  . ' </td>';
          $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][0].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][0], 0, ',', '.')  . ' </td>';

        }

        $this->csvarr[$i] = array(
            $i,
            $keyword,
            $this->rs_locations[$loc],
            $dex_format,
            $dex_max_format,
            $dex_diff_format,
            $potential_format,
            $potential_indicate_format_csv,
            number_format($this->googleadwords[$keyword][$loc][3], 0, ',', '.'),
            number_format($this->googleadwords[$keyword][$loc][0], 0, ',', '.'),
            number_format($this->googleadwords[$keyword][$loc][1], 2, ',', '.'),
            number_format($this->googleadwords[$keyword][$loc][2], 2, ',', '.')
          );


        // RANKING SETS
        $ii = 0;
        foreach ($this->rankset['_ts'] as $k => $ts) {

          $ii++;

          // NO RANKING TODAY
          if (!isset($timestamp[$ts][$this->rs_type])) {

            $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';

            $yesterday = parent::getYesterdayYMD($ts);
            if (isset( $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][0]['rank'] )) {
              $change = $this->checkDifferenceBetweenRankings('101', $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][0]['rank']);
              $change_csv = $this->checkDifferenceBetweenRankings('101', $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][0]['rank'], false);
            } else {
              $change = '';
              $change_csv = '';
            }

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change .'</td>';
            }

            array_push($this->csvarr[$i], '-');
            array_push($this->csvarr[$i], $change_csv);
            array_push($this->csvarr[$i], '-');
            array_push($this->csvarr[$i], '');

          // RANKINGS TODAY
          } else {

            //multiple rankings
            if (count($timestamp[$ts][$this->rs_type]) > 1) {

              // sort multiple rankings, lowest first
              krsort($timestamp[$ts][$this->rs_type]);

              $out .= '<td class="rank"';
              $k = 0;

              $matched_url = array();
              foreach ($timestamp[$ts][$this->rs_type] as $value) {

                // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
                if (isset($matched_url[$value['url']])) {
                  continue;
                }
                $matched_url[$value['url']] = $value['url'];

                $yesterday = parent::getYesterdayYMD($ts);

                if ($k == 0) {
                  $compare_index = count($this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type]) - 1;
                  $csv = $value['rank'];
                  $csv_url = $value['url'];
                  $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a>';
                  $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank']);
                  $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank'], false);
                } else {
                  $csv .= ' / ' . $value['rank'];
                  $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                }
                $k++;

              }

              $out .= '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']). '/' .$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small>';
              $out .= '</td>';

              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'.$change.'</td>';
              }

              array_push($this->csvarr[$i], $csv);
              array_push($this->csvarr[$i], $change_csv);
              array_push($this->csvarr[$i], $csv_url);

            //single rankings
            } else {

              foreach ($timestamp[$ts][$this->rs_type] as $value) {

                $yesterday = parent::getYesterdayYMD($ts);

                if (empty($value['rank'])) {
                  $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
                } else {
                  $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small target="_blank" class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
                }

                $compare_index = count($this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type]) - 1;
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank']);
                $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank'], false);
                if ($ii < count($this->rankset['_ts'])) {
                  $out .= '<td>'.$change .'</td>';
                }

                array_push($this->csvarr[$i], $value['rank']);
                array_push($this->csvarr[$i], $change_csv);
                array_push($this->csvarr[$i], $value['url']);

              }

            }

          }

        }

        $out .= '</tr>';

      }

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    // WRITE CSV
    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords_local');

    // OUTPUT
    $outhead .= '<div class="row">

        <div class="col-md-12">

          <div class="box">
            <div class="box-header">
              <div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div>
              <span class="titleleft">Ansicht: Keywords / Rankings</span>
                <ul class="box-toolbar">';

    $outhead .=  parent::submenuLocal();

      if (!empty($this->days)) {
        $outhead .='<li><a target="_blank" href="../'.$this->keyword_setid.'"><span class="label label-gray">7 Tage Ansicht</span></a></li>';
      } else {
        $outhead .='<li><a target="_blank" href="'.$this->keyword_setid.'/31"><span class="label label-gray">31 Tage Ansicht</span></a></li>';
      }

    $outhead .= $this->renderEditSet();
    //$outhead .= $this->personalDashboard();

    $outhead .='</ul>
            </div>

            <div class="box-content">
              <div class="row">
                <div class="col-md-12">
                  <div id="OPSC_opi_highchart"><i class="oneproseo-preloader"></i></div>
                </div>
              </div>
            </div>

            <div class="box-content">
              <div class="row">
                <div class="col-md-12"><hr /></div>
              </div>
            </div>

            <div class="box-content padded">
              <div class="row">
                <div class="col-md-12">

                  <div class="row">
                    <div class="col-md-2">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-bolt icon-2x"></i></li>';

                        if (isset($this->url_campaign)) {
                          $outhead .=' <li class="count">'. count($this->url_campaign) .'</li></ul><div class="stats-label">Überwachte URLs</div>';
                        } else {
                          $outhead .=' <li class="count">'. $this->keyword_amount .'</li></ul><div class="stats-label">überwachte Keywords</div>';
                        }

                    $outhead .='</div>
                    </div>

                    <div class="col-md-2">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-building"></i></li>
                          <li class="count">'.count($this->locations).'</li>
                        </ul>
                        <div class="stats-label">Anzahl Orte</div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-signal"></i></li>
                          <li class="count" id="ops_top10"></li>
                        </ul>
                        <div class="stats-label">Top 10 Rankings</div>
                      </div>
                    </div>

                    <div class="col-md-2">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-signal"></i></li>
                          <li class="count" id="ops_top20"></li>
                        </ul>
                        <div class="stats-label">Top 20 Rankings</div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-signal"></i></li>
                          <li class="count" id="ops_top30"></li>
                        </ul>
                        <div class="stats-label">Top 30 Rankings</div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-search"></i></li>
                          <li class="count" id="ops_topQ"></li>
                        </ul>
                        <div class="stats-label">∅ Ranking</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>';

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Detail Rankings für '.$this->setname.' / Google Version: '.$this->rsTypeLocal($this->rstype).'</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td>
                      <td>Ort</td><td>OneDex <i class="icon-remove hidecolumn right"></i></td>
                      <td>OPI <i class="icon-remove hidecolumn right"></i></td>
                      <td>SV <i class="icon-remove hidecolumn right"></i></td>';

    $this->out = $outhead . $out;

  }



  /*
  URL -> DEX -> ALL KEYWORDS FOR URL
  https://oneproseo.advertising.de/dev-oneproseo/rankings/local/23/detail-url/93
  */

  private function renderRankingViewUrl ()
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'URL', 'Ort', 'OneDex');

    // merge ranking result with keywordset for keywords that do not rank
    if (isset($this->url_campaign)) {
      foreach ($this->url_campaign as $url) {
        if (stripos($url, '*') !== false) {continue;}
        if (!isset($this->rankset['_da'][$url])) {
           $this->rankset['_da'][$url] = '';
        }
      }
    }

    // TABLE HEADER - DATES
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $out .= '<td>'.parent::germanTS($ts).'</td>';
      array_push($this->csvarr['head'], $ts);
    }
    $out .= '</tr></thead><tbody>';

    // TABLE BODY - CONTENT
    foreach ($this->rankset['_da'] as $url => $set) {
      foreach ($set as $loc => $timestamp) {

        $i++;

        if (!isset($timestamp)) {
          $dex_cumm = 0;
        } else {
          // ONE DEX FETCH DATA FOR CALC
          $today    = key($timestamp);
          $dex_cumm = $this->oneDexAccumulated($timestamp[$today][$this->rs_type], $loc);
          $dex_cumm = number_format($dex_cumm, 3, ',', '.');
        }

        $out .= '<tr>';
        $out .= '<td>' . $i . '</td>';
        $out .= '<td><a href="'.$url.'" target="_blank">' . $url . '</a></td>';
        $out .= '<td>' . $this->rs_locations[$loc] . '<a title="Das komplette Keywordset für den Ort '.$this->rs_locations[$loc].' abfragen" target="_blank" href="../../../../rankings/local/'.$this->customerid.'/set/keywords/'.$this->setid.'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><i class="icon-comment-alt right"></i></a></td>';
        $out .= '<td>' . $dex_cumm . '</td>';
        $this->csvarr[$i] = array($i, $url, $this->rs_locations[$loc], $dex_cumm);

        // RANKING SETS
        foreach ($this->rankset['_ts'] as $k => $ts) {

          if (!isset($set[$loc][$ts][$this->rs_type])) {

            $out .= '<td> - </td>';
            array_push($this->csvarr[$i], '-');

          } else {

            //MULTI URL RANKINGS
            if (count($set[$loc][$ts][$this->rs_type]) > 1) {

              $csv = '';
              $out .= '<td>';
              $out .= '<table class="plain" data-ts="' . $ts . '">';

              $matched_keywords = array();
              $s_out = '';
              $kws = '';
              $x = 0;

              foreach ($set[$loc][$ts][$this->rs_type] as $value) {
                $opi_val = (!isset($this->googleadwords[$value['keyword']][$loc][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][$loc][3]);
                $matched_keywords[$value['keyword']] = $opi_val;
                $matched_keywords_rank[$value['keyword']] = $value['rank'];
                $kws++;
              }

              // SORT OPI DESC
              arsort($matched_keywords, SORT_NUMERIC);

              foreach ($matched_keywords as $keyword => $opi) {
                $s_out .= '<tr><td style="width:70%"><a target="_blank" href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><span class="nowrap">' . $keyword . '</span></a></td><td>' . $matched_keywords_rank[$keyword] . '</td><td> ' . $opi . '</td></tr>';
                $csv .=  $keyword . ' / ';
              }

              if ($kws < 2) {$out .= '<tr><td colspan="3"><b>Rankt für 1 Keyword</b></td></tr>';} else {$out .= '<tr><td colspan="3"><b>Rankt für ' . $kws . ' Keywords</b></td></tr>';}

              $out .= $s_out;
              $out .= '</table>';
              $out .= '</td>';

              array_push($this->csvarr[$i], $csv);

            //ONE URL RANKING
            } else {

              $csv = '';
              foreach ($set[$loc][$ts][$this->rs_type] as $value) {
                $out .= '<td>';
                $out .= '<table class="plain" data-ts="' . $ts . '">';
                $out .= '<tr><td colspan="3"><b>Rankt für 1 Keyword</b></td></tr>';
                $out .= '<tr><td style="width:70%"><a target="_blank" href="../keywords/results/'.urlencode($value['keyword']).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><span class="nowrap">' . $value['keyword'] . '</span></a></td><td>' . $value['rank'] . '</td><td>'.(!isset($this->googleadwords[$value['keyword']][$loc][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][$loc][3]).'</td></tr>';
                $out .= '</table>';
                $csv .=  $value['keyword'] . ' / ';
              }

              array_push($this->csvarr[$i], $csv);

            }

          }

        }

        $out .= '</tr>';

      }

    }

    $out .= '</tr></tbody></table></div></div></div></div>';


    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_urls');

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div>
                  <span class="titleleft">Ansicht: URL / Keywords</span>
                <ul class="box-toolbar">';

    $outhead .=  parent::submenuLocal();

    if ($this->type != 'complete') {
      if (!empty($this->days)) {
        $outhead .='<li><a target="_blank" href="../'.$this->keyword_setid.'"><span class="label label-gray">7 Tage Ansicht</span></a></li>';
      } else {
        $outhead .='<li><a target="_blank" href="'.$this->keyword_setid.'/31"><span class="label label-gray">31 Tage Ansicht</span></a></li>';
      }
    }

    $outhead .= $this->renderEditSet();
    // $outhead .= $this->personalDashboard();

    $css = '';
    if ($this->type != 'complete') {
      $css = 'dtoverflow';
    }

    $outhead .='</ul>
                </div>
                <div class="box-content padded">
                  <p><p>Diese Ansicht zeigt die rankenden URLs eines Keywordsets und die Keywords für die diese URL rankt. <br />Rankende URLs für '.$this->hostname.': <b>' . count ($this->rankset['_da']) . '</b></p>
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                 <span class="title">Rankings</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table '.$css.'">
                  <thead>
                    <tr>
                      <td>#</td><td>URL</td><td>Ort</td><td>OneDex</td>';

    $this->out = $outhead . $out;

  }


  /*
  URL -> RANKING KEYWORDS -> DEX -> CHANGE
  https://oneproseo.advertising.de/dev-oneproseo/rankings/local/23/detail-url-onedex/93
  https://oneproseo.advertising.de/dev-oneproseo/rankings/local/35/detail-url-onedex-complete/all
  */

  private function renderRankingViewUrlDex ()
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'URL', 'Ort');

    // merge ranking result with keywordset for keywords that do not rank
    if (isset($this->url_campaign)) {
      foreach ($this->url_campaign as $url) {
        if (stripos($url, '*') !== false) {continue;}
        if (!isset($this->rankset['_da'][$url])) {
           $this->rankset['_da'][$url] = '';
        }
      }
    }

    // GOOGLE ANALYTICS DATA
    if (count($this->googleanalyticsAll) > 0) {
      $out .= '<td>Pageviews</td><td>Page Value (€)</td><td>Umsatz Zielseite (€)</td>';
      array_push($this->csvarr['head'], 'Pageviews');
      array_push($this->csvarr['head'], 'Page Value (€)');
      array_push($this->csvarr['head'], 'Umsatz Zielseite (€)');
    }

    // DATES
    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $ii++;

      $out .= '<td>OneDex</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td>Change</td>';
      }
      $out .= '<td>' . parent::germanTS($ts) . '</td>';

      array_push($this->csvarr['head'], 'Rankende Keywords');
      array_push($this->csvarr['head'], 'OneDex');

    }

    $out .= '</tr></thead><tbody>';

    // TABLE BODY - CONTENT
    foreach ($this->rankset['_da'] as $url => $set) {
      foreach ($set as $loc => $timestamp) {

        $i++;

        $out .= '<tr>';
        $out .= '<td>' . $i . '</td>';
        $out .= '<td><a href="'.$url.'" target="_blank">' . $url . '</a></td>';
        $out .= '<td>' . $this->rs_locations[$loc] . '<a title="Das komplette Keywordset für den Ort '.$this->rs_locations[$loc].' abfragen" target="_blank" href="../../../../rankings/local/'.$this->customerid.'/set/keywords/'.$this->setid.'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><i class="icon-comment-alt right"></i></a></td>';

        $this->csvarr[$i] = array($i, $url, $this->rs_locations[$loc], $dex_cumm);

        // GOOGLE ANALYTICS DATA
        if (count($this->googleanalyticsAll) > 0) {
          $url = parent::removeHttps($url);
          $url = strtolower($url);
          if (isset ($this->googleanalyticsAll[$url])) {

            $p_v = number_format($this->googleanalyticsAll[$url]['ga_pageValue_filter'], 2, ',', '.');
            $t_r = number_format($this->googleanalyticsAll[$url]['ga_transactionRevenue_filter'], 2, ',', '.');
            $sum_piv = $this->googleanalyticsAll[$url]['ga_pageviews_filter'];

            $out .= '<td class="nowrap" data-order="'.$sum_piv.'"> ' . $sum_piv . ' </td>';
            $out .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_pagevalue_filter']).'"> ' . $p_v . ' </td>';
            $out .= '<td class="nowrap" data-order="'.round($this->googleanalyticsAll[$url]['ga_transactionRevenue_filter']).'"> ' . $t_r . ' </td>';

            array_push($this->csvarr[$i], $sum_piv);
            array_push($this->csvarr[$i], $p_v);
            array_push($this->csvarr[$i], $t_r);

          } else {
            $out .= '<td class="nowrap" data-order="0"> - </td><td data-order="0" class="nowrap"> - </td><td data-order="0" class="nowrap"> - </td>';
            array_push($this->csvarr[$i], '-');
            array_push($this->csvarr[$i], '-');
            array_push($this->csvarr[$i], '-');
          }
        }

        // RANKING SETS
        $ii = 0;
        foreach ($this->rankset['_ts'] as $k => $ts) {

          $ii++;
          if (!isset($set[$loc][$ts][$this->rs_type])) {

            $out .= '<td> - </td>';
            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td> - </td>';
            }
            $out .= '<td> - </td>';

            array_push($this->csvarr[$i], '-');

          } else {

            // ONE DEX FETCH DATA FOR CALC

            $yesterday = parent::getYesterdayYMD($ts);

            $dex_cumm           = $this->oneDexAccumulated($timestamp[$ts][$this->rs_type], $loc);
            $dex_cumm_yesterday = $this->oneDexAccumulated($timestamp[$yesterday][$this->rs_type], $loc);
            $change_dex         = $this->calcPercent($dex_cumm, $dex_cumm_yesterday);

            $dex_cumm = number_format($dex_cumm, 3, ',', '.');

            //MULTI URL RANKINGS
            if (count($set[$loc][$ts][$this->rs_type]) > 1) {

              $csv = '';
              $out .= '<td class="nowrap"> ' . $dex_cumm . ' </td>';
              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td class="nowrap"> ' . $change_dex . ' </td>';
              }
              $out .= '<td class="nowrap">';

              $matched_keywords = array();
              $kws = 0;
              $out_kw = '';

              foreach ($set[$loc][$ts][$this->rs_type] as $value) {

                $opi_val = (!isset($this->googleadwords[$value['keyword']][$loc][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][$loc][3]);
                $matched_keywords[$value['keyword']] = $opi_val;
                $matched_keywords_rank[$value['keyword']] = $value['rank'];

                $out_kw .=  $value['keyword'] . '<br />';
                $kws++;
              }

              // SORT OPI DESC
              arsort($matched_keywords, SORT_NUMERIC);

              $s_out = '<table class="plain" data-ts="' . $ts . '">';

              foreach ($matched_keywords as $keyword => $opi) {
                $s_out .= '<tr><td style="width:70%"><a target="_blank" href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><span class="nowrap">' . $keyword . '</span></a></td><td>' . $matched_keywords_rank[$keyword] . '</td><td> ' . $opi . '</td></tr>';
              }
              $s_out .= '</table>';

              if ($kws < 2) {
                $out .= '<span class="tt"><strong class="tooltip-with-markup"><b>1</b> Keyword';
                $csv = '1 Keyword';
              } else {
                $out .= '<span class="tt"><strong class="tooltip-with-markup"><b>' . $kws . '</b> Keywords';
                $csv = $kws . ' Keywords';
              }

              $out .= '<span class="tooltip-content"><span class="arrow"></span>'.$s_out.'</span></strong></span>';

              $out .= '</td>';

              array_push($this->csvarr[$i], $csv);
              array_push($this->csvarr[$i], $dex_cumm);

            } else {

              foreach ($set[$loc][$ts][$this->rs_type] as $value) {
                $out .= '<td class="nowrap"> ' .$dex_cumm. ' </td>';
                if ($ii < count($this->rankset['_ts'])) {
                  $out .= '<td class="nowrap"> ' . $change_dex . ' </td>';
                }
                $s_out = '<table class="plain" data-ts="' . $ts . '">';
                $s_out .= '<tr><td style="width:70%"><a href="../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'"><span class="nowrap">' . $value['keyword'] . '</span></a></td><td>' . $value['rank'] . '</td><td>'.(!isset($this->googleadwords[$value['keyword']][$loc][3]) ? 'N/A' : $this->googleadwords[$value['keyword']][$loc][3]).'</td></tr>';
                $s_out .= '</table>';
                $out .= '<td class="nowrap"><span class="tt"><strong class="tooltip-with-markup"><b>1</b> Keyword <span class="tooltip-content"><span class="arrow"></span>'.$s_out.'</span></strong></span></td>';
              }

              $csv = '1 Keyword';

              array_push($this->csvarr[$i], $csv);
              array_push($this->csvarr[$i], $dex_cumm);

            }

          }

        }

        $out .= '</tr>';

      }
    }

    $out .= '</tr></tbody></table></div></div></div></div>';


    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_urls_dex');


    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div>
                  <span class="titleleft">Ansicht: URL / OneDex</span>
                <ul class="box-toolbar">';

    $outhead .=  parent::submenuLocal();

    if ($this->type != 'dexcomplete') {
      if (!empty($this->days)) {
        $outhead .='<li><a target="_blank" href="../'.$this->keyword_setid.'"><span class="label label-gray">7 Tage Ansicht</span></a></li>';
      } else {
        $outhead .='<li><a target="_blank" href="'.$this->keyword_setid.'/31"><span class="label label-gray">31 Tage Ansicht</span></a></li>';
      }
    }

    $outhead .= $this->renderEditSet();
    //$outhead .= $this->personalDashboard();

    $css = '';
    if ($this->type != 'dexcomplete') {
      $css = 'dtoverflow';
    }

    $outhead .='</ul>
                </div>
                <div class="box-content padded">
                  <p>Diese Ansicht zeigt die rankenden URLs eines Keywordsets und die Anzahl der Keywords für die diese URL rankt. <br />Rankende URLs für '.$this->hostname.': <b>' . count ($this->rankset['_da']) . '</b></p>
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                 <span class="title">Rankings</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table '.$css.'">
                  <thead>
                    <tr>
                      <td>#</td><td>URL</td><td>Ort</td>';

    $this->out = $outhead . $out;

  }



  /*
  KEYWORD -> RANK -> URL
  https://oneproseo.advertising.de/dev-oneproseo/rankings/local/23/detail-keyword-url/93
  */


  private function renderRankingViewKeywordUrl() {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'Ort', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition');

    // merge ranking result with keywordset for keywords that do not rank
    if (!isset($this->url_campaign)) {
      foreach ($this->keywords as $keyword) {
        if (!isset($this->rankset['_da'][$keyword])) {
           $this->rankset['_da'][$keyword][] = '';
        }
      }
    }

    // sort by key
    ksort($this->rankset['_da']);
    // DATES
    $out .= '<td>Rank - '. parent::germanTS(parent::dateYMD()) .'</td>';
    array_push($this->csvarr['head'], parent::dateYMD());

    $out .= '<td>Rankende URL</td></tr></thead><tbody>';
    array_push($this->csvarr['head'], 'Rankende URL');

    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $set) {

      foreach ($set as $loc => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank">
                 <a title="Dieses Keyword für den Ort '.$this->rs_locations[$loc].' abfragen" target="_blank" href="../../../../rankings/local/'.$this->customerid.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">' . $keyword . '</a>
                 <a title="Das Keyword: '.$keyword.' für alle Orte in diesem Set abfragen" target="_blank" href="../../../../rankings/local/'.$this->customerid.'/set/locations/'.$this->setid.'/'. urlencode($keyword) .'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><i class="icon-comment right"></i></a>
               </td>';
      $out .= '<td class="rank">' . $this->rs_locations[$loc] . '
                  <a title="Das komplette Keywordset für den Ort '.$this->rs_locations[$loc].' abfragen" target="_blank" href="../../../../rankings/local/'.$this->customerid.'/set/keywords/'.$this->setid.'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'"><i class="icon-comment-alt right"></i></a>
               </td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword][$loc])) {

        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

      } else {

      // ONE DEX FETCH DATA FOR CALC
        $latest_ts = array_keys($this->rankset['_ts']);
        $latest_ts = $latest_ts[0];
        if (isset($timestamp[$latest_ts][$this->rs_type][0])) {
          $rank = $timestamp[$latest_ts][$this->rs_type][0]['rank'];
        } else {
          $rank = 101;
        }
        $opi = $this->googleadwords[$keyword][$loc][3];
        $dex_format = $this->calcOneDex($rank, $opi);
        $dex_format = number_format($dex_format, 3, ',', '.');

      // ONE DEX FETCH DATA FOR CALC

        $out .= '<td data-order="'.$dex_format.'" class="gadw tdright"> ' . $dex_format . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][3].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][3], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][0].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][0], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][1].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][1], 2, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][2].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][2], 2, ',', '.')  . ' </td>';

      }

      $this->csvarr[$i] = array($i, $keyword, $this->rs_locations[$loc], $dex_format, number_format($this->googleadwords[$keyword][$loc][3], 0, ',', '.'), number_format($this->googleadwords[$keyword][$loc][0], 0, ',', '.'), number_format($this->googleadwords[$keyword][$loc][1], 2, ',', '.'), number_format($this->googleadwords[$keyword][$loc][2], 2, ',', '.'));

      // RANKING SETS
      $ts = parent::dateYMD();

      // NO RANKING TODAY
      if (!isset($timestamp[$ts][$this->rs_type])) {

        $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'">TOP100</a></small></td>';
        $rank_url = '-';
        array_push($this->csvarr[$i], '-',  '-');

      } else {

         // check for multiple rankings
        if (count($timestamp[$ts][$this->rs_type]) > 1) {

          // sort multiple rankings, lowest first
          krsort($timestamp[$ts][$this->rs_type]);

          $out .= '<td class="rank"';
          $k = 0;

          $matched_url = array();
          foreach ($timestamp[$ts][$this->rs_type] as $value) {

            $matched_url[$value['url']] = $value['url'];

            if ($k == 0) {
              $csv = $value['rank'];
              $csvu = $value['url'];
              $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              $rank_url = '<a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
            } else {
              $csv .= ' / ' . $value['rank'];
              $csvu .= ' / ' . $value['url'];
              $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              $rank_url .= '<br /><a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
            }
            $k++;
          }

          array_push($this->csvarr[$i], $csv, $csvu);

          $out .= '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']). '/' .$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small>';
          $out .= '</td>';

        } else {

          foreach ($timestamp[$ts][$this->rs_type] as $value) {
            array_push($this->csvarr[$i], $value['rank'], $value['url']);

            if (empty($value['rank'])) {
              $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
            } else {
              $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small target="_blank" class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'/rankings/local/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
            }

            $rank_url = '<a class="rankings-url" target="_blank" href="'.$value['url'].'">'. $value['url'] .'</a>';
          }

        }

      }

      $out .= '<td class="rank-url">' . $rank_url . '</td>';
      $out .= '</tr>';

    }

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords_url');


    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div>
                  <span class="titleleft">Ansicht: Keywords / URL</span>
                <ul class="box-toolbar">';

    $outhead .=  parent::submenuLocal();

    $outhead .= $this->renderEditSet();
    // $outhead .= $this->personalDashboard();

    $outhead .='</ul>
                </div>
                <div class="box-content padded">
                  <p>Diese Ansicht zeigt die Keywords, den Rang heute und welche URL für dieses Keyword rankt.</p>
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                 <span class="title">Detail Keyword / URL Rankings für '.$this->setname.'</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | URL Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td><td>Ort</td><td>OneDex</td><td>OPI</td><td>SV</td><td>CPC</td><td>Wettb.</td>';


    $this->out = $outhead . $out;

  }


  private function oneDexAccumulated ($rankset, $loc) {

    if (!is_array($rankset)) {
      return 0;
    }

    $dex_cumm = 0;

    foreach ($rankset as $key => $data) {

      if (isset($data['rank'])) {
        $rank = $data['rank'];
      } else {
        $rank = 101;
      }
      $opi = $this->googleadwords[$data['keyword']][$loc][3];
      $dex = $this->calcOneDex($rank, $opi);
      $dex_cumm = $dex_cumm + $dex;

    }

    return $dex_cumm;

  }


  private function renderEditSet () {

    if ($_SESSION['role2'] == 'admin' && $this->keyword_setid != 'all') {
      return '<li><a target="_blank" href="../keywords/'.$this->keyword_setid.'"> <i title="Edit set" class="icon-edit setedit"></i> </a></li>';
    }

  }


  private function personalDashboard()
  {

    if (isset($_SESSION['ruk_personal_dash'])) {
      foreach ($_SESSION['ruk_personal_dash'] as $key => $value) {
        if ($value['setid'] == $this->keyword_setid && $value['projectid'] == $this->customerid) {
          return '<li><i title="Already in personal dashboard" class="icon-ok"></i></li>';
        }
      }
    }

    if ($this->data['customer_version'] == 0 || !isset($_SESSION['userid'])) {
      return '<li><i data-setid="'.$this->keyword_setid.'" data-projectid="'.$this->customerid.'" title="Add to personal dashboard" class="add-sortable icon-plus-sign"></i></li>';
    }

  }


  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {
      echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br/>Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</strong></div></div></div>';
      exit;
    }

  }

}

?>
