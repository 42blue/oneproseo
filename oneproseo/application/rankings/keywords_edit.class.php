<?php

require_once('ruk.class.php');

class keywords_edit extends ruk
{

  private $copy_count = 1;

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->rankingspy = new rankingspy();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () {

    // SHOW
    if (isset($_POST['setid']) && $_POST['action'] == 'show') {

      $this->keyword_set = $_POST['setid'];

      $this->campaign_type = $this->checkCampainType($this->keyword_set);
      $this->selectKeywordSet();
      $this->renderView();

      echo $this->out;

    }

    // CHANGE SET
    if (isset($_POST['setid']) && isset($_POST['keywords']) && $_POST['action'] == 'change') {

      $this->project_id       = $_POST['projectid'];
      $this->keyword_set_name = strip_tags($_POST['setname']);
      $this->keyword_set      = $_POST['setid'];
      $this->keywords         = $_POST['keywords'];
      $this->tags             = $_POST['tags'];      
      $this->customer_view    = $_POST['customer_view'];
      $this->mo_type          = $_POST['mo_type'];
      $this->measuring        = $_POST['measuring'];
      $this->competition      = strip_tags($_POST['competition']);
      $this->teammail         = strip_tags($_POST['team']);

      $this->campaign_type = $this->checkCampainType($this->keyword_set);

      if ($this->campaign_type == 'url') {
        $this->changeUrlSet();
      } else {
        //$this->getTags();
        $this->changeKeywordSet();
      }

      echo $this->out;

    }

    // COPY SET
    if (isset($_POST['copy_setid']) && isset($_POST['copy_projectid']) && isset($_POST['copy_type']) && $_POST['action'] == 'copy') {

      $this->keyword_set     = $_POST['copy_setid'];
      $this->copy_to_project = $_POST['copy_projectid'];
      $this->campaign_type   = $this->checkCampainType($this->keyword_set);

      $this->selectKeywordSet();

      $set_id = $this->addNewKeywordSet();

      if ($set_id == false) {
        echo 'Error - No Set ID / Too many Campaigns?';
        exit;
      }

      $this->keyword_set = $set_id;

      if ($this->campaign_type == 'url') {
        $this->changeUrlSet();
      } else {
        $this->changeKeywordSet();
      }

      echo $this->out_copy;

    }


  }


  private function getTags () {

		$this->tags = array();
		$idkwset = $this->keyword_set;

    $sql = "SELECT id, id_kw_set, tags, keyword FROM ruk_project_keywords WHERE id_kw_set = '$idkwset'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->tags[$row['keyword']] = $row['tags'];
    }

	}


  private function selectKeywordSet () {

		$this->tagsout = '';

    $selected_keyword_set = $this->keyword_set;

    if ($this->campaign_type == 'url') {

      $sql = "SELECT a.id            AS setid,
                     a.name          AS setname,
                     a.customer_view AS customer_view,
                     a.rs_type       AS type,
                     a.measuring     AS measuring,
                     a.team_email    AS team_email,
                     b.id            AS kwid,
                     b.id_kw_set     AS kwsetid,
                     b.url           AS keyurl,
                     c.name          AS customer,
                     c.country       AS country
              FROM ruk_project_keyword_sets a
                LEFT JOIN ruk_project_urls b
                  ON b.id_kw_set = a.id
                LEFT JOIN ruk_project_customers c
                  ON c.id = a.id_customer
              WHERE a.id = '$selected_keyword_set'
              ORDER BY keyurl";

    } else {

      $sql = "SELECT a.id            AS setid,
                     a.name          AS setname,
                     a.customer_view AS customer_view,
                     a.measuring     AS measuring,
                     a.rs_type       AS type,
                     a.measuring     AS measuring,
                     a.competition   AS competition,
                     a.team_email    AS team_email,                     
                     b.id            AS kwid,
                     b.id_kw_set     AS kwsetid,
                     b.keyword       AS keyurl,
                     b.tags          AS tags,
                     c.name          AS customer,
                     c.country       AS country
              FROM ruk_project_keyword_sets a
                LEFT JOIN ruk_project_keywords b
                  ON b.id_kw_set = a.id
                LEFT JOIN ruk_project_customers c
                  ON c.id = a.id_customer
              WHERE a.id = '$selected_keyword_set'
              ORDER BY keyurl";

    }

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->set_name      = $row['setname'];
      $this->customer      = $row['customer'];
      $this->customer_view = $row['customer_view'];
      $this->measuring     = $row['measuring'];
      $this->country       = $row['country'];
      $this->mo_type       = $row['type'];
      $this->competition   = $row['competition'];

      $fielddata           = nl2br(stripslashes($row['keyurl']));
      $this->keywords     .= $fielddata . "\n";
      $fielddata           = nl2br(stripslashes($row['tags']));
      $this->tagsout     	.= $fielddata . "\n";
      $this->teammail      = $row['team_email'];
    }

    if(!preg_match("/[a-z\d]/i", $this->tagsout)){
      $this->tagsout = '';
    }

  }


  private function renderView () {

    $show_customer_true  = '';
    $show_customer_false = '';
    if ($this->customer_view == 1) {
      $show_customer_true = 'checked="checked"';
    } else {
      $show_customer_false = 'checked="checked"';
    }

    $show_measuring_daily  = '';
    $show_measuring_weekly = '';
    if ($this->measuring == 'daily') {
      $show_measuring_daily = 'checked="checked"';
    } else {
      $show_measuring_weekly = 'checked="checked"';
    }

    if ($this->campaign_type == 'url') {

      $this->out = '
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><p><strong>Hinweis:</strong></p>
              <p>Es wird eine <strong>URL Campaign</strong> bearbeitet, bitte keine Keywords eingeben, <strong>fremde Domains</strong> die nicht der Projekturl entsprechen werden nicht gefunden.<br/></p>
              <p>Wildcards sind möglich: <strong>*/beispielurl/test*</strong> <br/></p>
              <p>Eine URL pro Zeile, max. ' . $this->env_data['ruk_max_keywords_set'] . ' URLs pro Set.<br/></p>
              <p>Alles was über ' . $this->env_data['ruk_max_keywords_set'] . ' pro Set hinausgeht wird ignoriert und nicht gespeichert.</p>
              <p>Um eine hohe Performance zu garantieren sollten maximal 1000 URLs pro Set verwendet werden.</p>
              </div>
            </div>
          </div>

          <div class="box">
            <div class="box-header">
              <span class="title">URLs bearbeiten für Projekt: '.$this->customer.' / Campaign: '.$this->set_name.'</span>
            </div>
            <div class="box-content padded">
              <form class="form-horizontal fill-up" role="form" id="ops_rankings_save_keywords">
                <div class="form-group">
                  <label for="ops_add_keywordset_name" class="col-sm-2 label-1 control-label">Campaign:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_keywordset_name" name="name" required="required" value="'.$this->set_name.'">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Überwachungsinterval:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="OPS_interval" value="daily" class="icheck OPS_interval" ' . $show_measuring_daily . ' id="iradio5">
                      <label for="iradio5">täglich</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="OPS_interval" value="weekly" class="icheck OPS_interval" ' . $show_measuring_weekly . '  id="iradio6">
                      <label for="iradio6">wöchentlich (Montag)</label>
                    </div>
                  </div>
                </div>';

              if ($this->env_data['customer_version'] == 0) {
                $this->out .= '<div class="form-group">
                  <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="0" class="icheck OPS_show" ' . $show_customer_false . ' id="iradio3">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="1" class="icheck OPS_show" ' . $show_customer_true . '  id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                 </div>';
              } else {
                $this->out .= '<div class="form-group">
                  <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="0" class="icheck OPS_show" id="iradio3">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="1" class="icheck OPS_show" checked="checked" id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                 </div>';                
              }

              $this->out .= '<div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Google Version:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="type" id="ops_type">
                      <option value="desktop" ' . ($this->mo_type == 'desktop' ? 'selected' : '') .'>Google Desktop</option>
                      <option value="mobile" ' . ($this->mo_type == 'mobile' ? 'selected' : '') .'>Google Mobile</option>
                      <option value="maps"' . ($this->mo_type == 'maps' ? 'selected' : '') .'>Google Maps</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="team" class="col-sm-2 label-1 control-label">Empfänger der E-Mail Reports: </label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="team" name="team" placeholder="Eine Emailadresse pro Zeile!">' . str_replace(',', "\n", $this->teammail) . '</textarea>
                  </div>
                </div>                
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">URLs:</label>
                  <div class="col-sm-8">
                    <textarea class="rankings" id="ops_rankings_keywords" name="keywords" placeholder="Keine URLs vorhanden" required="required">'.$this->keywords.'</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8" id="ops_spin">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_rankings_save_keywords_submit">Campaign speichern</button>
                  </div>
                </div>
              </form>
            </div>
          </div>';

    } else {

      $comp = '';
      if (!empty($this->competition)) {
        $comp =  implode ("\n", unserialize($this->competition));
      }

      $this->out = '
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><p><strong>Hinweis:</strong></p>
              <p>Es wird eine <strong>Keyword Campaign</strong> bearbeitet, bitte keine URLs eingeben.<br/></p>
              <p>Ein Keyword pro Zeile, max. ' . $this->env_data['ruk_max_keywords_set'] . ' Keywords pro Set.<br/></p>
              <p>Alles was über ' . $this->env_data['ruk_max_keywords_set'] . ' pro Set hinausgeht wird ignoriert und nicht gespeichert.</p>
              <p>Nach dem Hinzufügen von neuen Keywords kann es bis zu 24 Stunden dauern bis Ranking Daten vorliegen.</p>
              <p>Um eine hohe Performance zu garantieren sollten maximal 1000 Keywords pro Set verwendet werden.</p>
              </div>
            </div>
          </div>

          <div class="box">
            <div class="box-header">
              <span class="title">Keywords bearbeiten für Projekt: '.$this->customer.' / Campaign: '.$this->set_name.'</span>
            </div>
            <div class="box-content padded">
              <form class="form-horizontal fill-up" role="form" id="ops_rankings_save_keywords">
                <div class="form-group">
                  <label for="ops_add_keywordset_name" class="col-sm-2 label-1 control-label">Campaign:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_keywordset_name" name="name" required="required" value="'.$this->set_name.'">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Überwachungsinterval:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="OPS_interval" value="daily" class="icheck OPS_interval" ' . $show_measuring_daily . ' id="iradio5">
                      <label for="iradio5">täglich</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="OPS_interval" value="weekly" class="icheck OPS_interval" ' . $show_measuring_weekly . '  id="iradio6">
                      <label for="iradio6">wöchentlich (Montag)</label>
                    </div>
                  </div>
                </div>'; 

              if ($this->env_data['customer_version'] == 0) {

                $this->out .= '<div class="form-group">
                  <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="0" class="icheck OPS_show" ' . $show_customer_false . ' id="iradio3">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="1" class="icheck OPS_show" ' . $show_customer_true . '  id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                </div>';
                
              } else {

                $this->out .= '<div class="form-group">
                  <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="0" class="icheck OPS_show" id="iradio3">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="1" class="icheck OPS_show" checked="checked"  id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                </div>';


              }

              $this->out .= '<div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Google Version:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="type" id="ops_type">
                      <option value="desktop" ' . ($this->mo_type == 'desktop' ? 'selected' : '') .'>Google Desktop</option>
                      <option value="mobile" ' . ($this->mo_type == 'mobile' ? 'selected' : '') .'>Google Mobile</option>
                      <option value="maps"' . ($this->mo_type == 'maps' ? 'selected' : '') .'>Google Maps</option>
                    </select>
                  </div>
                </div>
                <!--<div class="form-group">
                  <label class="col-sm-2 control-label">Messzeitpunkte:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="measuring" value="daily" class="icheck OPS_measuring" ' . $show_measuring_daily . ' id="iradio5">
                      <label for="iradio5">täglich</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="measuring" value="weekly" class="icheck OPS_measuring" ' . $show_measuring_weekly . '  id="iradio6">
                      <label for="iradio6">wöchentlich</label>
                    </div>
                  </div>
                </div>-->
                <div class="form-group">
                  <label for="competition" class="col-sm-2 label-1 control-label">Wettbewerb überwachen:</label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="OPS_competition" name="competition" placeholder="Eine URL pro Zeile! (max. 8)">' . $comp . '</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="team" class="col-sm-2 label-1 control-label">Empfänger der E-Mail Reports: </label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="team" name="team" placeholder="Eine Emailadresse pro Zeile!">' . str_replace(',', "\n", $this->teammail) . '</textarea>
                  </div>
                </div>                
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">Keywords:</label>
                  <div class="col-sm-4">
                    <textarea class="rankings" id="ops_rankings_keywords" name="keywords" placeholder="Keine Keywords vorhanden" required="required">'.$this->keywords.'</textarea>
                  </div>
                  <div class="col-sm-4">
                    <textarea class="rankings" id="ops_rankings_tags" name="tags" placeholder="Tags: Keyword <-> Tag (mehrere Tags durch Komma getrennt)">'.$this->tagsout.'</textarea>
                  </div>                  
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8" id="ops_spin">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_rankings_save_keywords_submit">Campaign speichern</button>
                  </div>
                </div>
              </form>
            </div>
          </div>';

    }

  }


  private function changeUrlSet () {

    $id_kw_set = $this->keyword_set;

    $keywords  = strip_tags($this->keywords);
    $keywords  = explode("\n", $keywords);

    // MAX KEYWORDS
    $keyword_amount = $this->db->query("SELECT DISTINCT keyword FROM ruk_project_keywords");
    $keyword_amount = $keyword_amount->num_rows;

    // DELETE ALL KEYWORDS
    $sql = "DELETE FROM ruk_project_urls WHERE id_kw_set = " . (int) $id_kw_set . "";
    $result = $this->db->query($sql);

    // INSERT NEW
    $sql = "INSERT INTO ruk_project_urls (id_kw_set, url) values ";

    $this->out_cut='';
    if (count($keywords) > $this->env_data['ruk_max_keywords_set']) {
      $keywords = array_slice($keywords, 0, $this->env_data['ruk_max_keywords_set']);
      $this->out_cut = '<br/>Sie haben mehr als '.$this->env_data['ruk_max_keywords_set'].' Keywords hinzugefügt.<br/>Alles was über '.$this->env_data['ruk_max_keywords_set'].' Keywords hinausgeht wird nicht gespeichert.';
    }

    $used_keywords = array();
    $values = array();
    foreach($keywords as $keyword) {

      $keyword = preg_replace('/\s/', ' ', $keyword);
      $keyword = trim($keyword);
      $keyword = strtolower($keyword);

      if (empty($keyword)) {
        continue;
      }

      // no duplicate keywords
      if (in_array ($keyword, $used_keywords)) {
        continue;
      }

      $used_keywords[] = $keyword;

      $keyword = $this->db->real_escape_string($keyword);

      $values[] = "('" . $id_kw_set ."', '".$keyword."')";

    }

    $sql .= implode(',', $values);

    $result = $this->db->query($sql);

    $rs_type = $this->mo_type;

    $sql = "UPDATE
              ruk_project_keyword_sets
            SET
              name = '$this->keyword_set_name', rs_type = '$rs_type', team_email = '$this->teammail', lastupdate = CURDATE() - INTERVAL 1 DAY, customer_view = $this->customer_view, measuring = '$this->measuring'
            WHERE
              id = '$id_kw_set'";

    $this->db->query($sql);

    if (!$result) {
      $this->out = ('DB ERROR: '.$this->db->error);
    }

    // DELETE WEEKLY CACHE
    if ($this->measuring == 'weekly') {
      parent::deleteCacheFile($this->project_id, $id_kw_set);
    }

    $this->out = 'Änderungen gespeichert';
    $this->out_copy = 'Set wurde kopiert';

    $this->out = $this->out . $this->out_cut;

  }


  private function changeKeywordSet () {

    $id_kw_set = $this->keyword_set;

    $keywords  = strip_tags($this->keywords);
    $keywords  = stripslashes($keywords);
    $forbidden = array(',', ';', '|', '"', "'", '+');
    $keywords  = str_replace($forbidden, ' ', $keywords);
    $keywords  = explode("\n", $keywords);
    $tags      = explode("\n", $this->tags);

    // merge KEYWORDS and TAGS
		$i=0;
		$kw2tags = array();
    foreach ($keywords as $keyword) {
    	if (empty($keyword)) {continue;}
    	$kw2tags[$keyword] = $tags[$i];
    	$i++;
    }

    $keywords = array_filter($keywords);

    // MAX KEYWORDS
    $keyword_amount = $this->getAllKeywordsDaily();

    // MUTI CUSTOMERS
    if ($this->env_data['customer_version'] == 1) {
      $keyword_amount = count(parent::getAllKeywordsForCustomerVersions($this->env_data['customer_available']));
    }

  // check how much of the added keywords are in the db
    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $sql = "SELECT DISTINCT keyword, tags FROM ruk_project_keywords WHERE keyword IN ($comma_separated_kws) ";
    $result = $this->db->query($sql);
    while ($row = $result->fetch_assoc()) {
      $keywords_already_in_db[$row['keyword']] = $row['keyword'];
      $keywords_already_in_db_tags[$row['keyword']] = $row['tags'];
    }
  // check how much of the added keywords are in the db

    $new_keywords = count($keywords);

// check for customer specific keyword amount_old
    $sql    = "SELECT keyword_amount FROM ruk_project_customers WHERE id = '$this->project_id'";
    $result = $this->db->query($sql);
    $row    = $result->fetch_array(MYSQLI_ASSOC);
    $max_project_keywords = $row['keyword_amount'];

    if ($max_project_keywords != 0) {

      $this->env_data['ruk_max_keywords'] = $max_project_keywords;

      // get all keywords by project
      $sql3 = "SELECT DISTINCT a.id_customer AS id_customer, b.keyword AS keyword FROM ruk_project_keyword_sets a LEFT JOIN ruk_project_keywords b ON b.id_kw_set = a.id WHERE a.id_customer = '$this->project_id'";
      $res3 = $this->db->query($sql3);
      $keyword_amount = $res3->num_rows;

    }

    // save only keywords that are already in the db if we exceed the max keywords amount
    if ($new_keywords > count($keywords_already_in_db) && $keyword_amount >= $this->env_data['ruk_max_keywords']) {

      $save = $this->saveKeywords($keywords_already_in_db, $keywords_already_in_db_tags);

      if ($save == false) {
        $this->out = 'Es sind über '.$this->env_data['ruk_max_keywords'].' Keywords in der DB. ('.$keyword_amount.')<br />Die Keywords konnten nicht gespeichert werden.';
        $this->out_copy = 'Fehler - Bitte wenden Sie sich an den Administrator.';
      } else {
        $this->out = 'Daten <strong>teilweise</strong> gespeichert!<br />';
        $this->out .= 'Von den ' .$new_keywords.' Keywords die hinzugefügt wurden, befinden sich ' . count($keywords_already_in_db) . ' bereits in der Datenbank.';
        $this->out .= '<br />Da die maximale Anzahl von ' .$this->env_data['ruk_max_keywords'].' Keywords erreicht ist, wurden nur die Keywords gespeichert, die sich bereits in der Datenbank befinden.';
        $this->out_copy = 'Set wurde kopiert';
      }

    } else {

      $save = $this->saveKeywords($keywords, $kw2tags);

      $diff = $new_keywords - count($keywords_already_in_db);

      if ($save == false) {
        $this->out = 'Es ist ein Fehler aufgetreten! Bitte wenden Sie sich an den Administrator.';
        $this->out_copy = 'Fehler - Bitte wenden Sie sich an den Administrator.';
      } else {
        $this->out = 'Daten wurden erfolgreich gespeichert!<br />' . $new_keywords . ' Keywords gesamt<br />' . $diff . ' Keywords neu.<br />' . count($keywords_already_in_db) . ' Keywords vorhanden.';
        $this->out_copy = 'Set wurde kopiert';
      }

    }

  }


  private function saveKeywords ($keywords, $kws2tags) {

    if (empty($keywords)) {
      return false;
    }

    $this->parseEmail();

    $id_kw_set = $this->keyword_set;

    // RANKING SPY DELETE SET
    $sql = "SELECT rankingspy_id FROM ruk_project_keyword_sets WHERE id='".$id_kw_set."'";
    $result = $this->db->query($sql);
    $ret = $result->fetch_array();
    if ($ret['rankingspy_id'] != 0) {
      $ret = $this->rankingspy->removeKeywordSet($ret['rankingspy_id']);
    }

    // ONEPROSEO DELETE ALL KEYWORDS
    $sql = "DELETE FROM ruk_project_keywords WHERE id_kw_set = " . (int) $id_kw_set . "";
    $result = $this->db->query($sql);

    // INSERT NEW
    $sql = "INSERT INTO ruk_project_keywords (id_kw_set, keyword, tags) values ";

    $used_keywords = array();
    $values = array();

    $rounds = 0;
    foreach ($kws2tags as $keyword => $tag) {

      if ($rounds >= 5000) {continue;}

      $keyword = trim($keyword);
      $keyword = preg_replace('/\s{2}/', ' ', $keyword);
      $keyword = strtolower($keyword);
      // &NBSP; in STRING
      $keyword = htmlentities($keyword, null, 'utf-8');
      $keyword = str_replace('&nbsp;', ' ', $keyword);
      $keyword = html_entity_decode($keyword);

      if (empty($keyword)) {
        continue;
      }

      // no duplicate keywords
      if (in_array ($keyword, $used_keywords)) {
        continue;
      }

  
      $used_keywords[] = $keyword;
  
      $keyword  = $this->db->real_escape_string($keyword);
      $values[] = "('" . $id_kw_set ."', '".$keyword."', '".$tag."')";

      $rounds++;

    }

    $this->reconMySql();

    $geturl = "SELECT a.id      AS setid,
                      a.name    AS setname,
                      b.url     AS url,
                      b.country AS country
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_customers b
                ON b.id = a.id_customer
            WHERE a.id = '$this->keyword_set'";
    $result = $this->db->query($geturl);
    $res = $result->fetch_array();

    // RANKING SPY CREATE SET, ADD KEYWORDS
    $name = $this->keyword_set_name . ' - ' . $id_kw_set;
    $ret  = $this->rankingspy->addKeywordSet($res['url'], $used_keywords, $name, $this->measuring, $res['country'], $this->mo_type);
    $rankingspy_id = $ret;

    // ONEPROSEO
    $sql .= implode(',', $values);

    $result = $this->db->query($sql);

    $rs_type = $this->mo_type;

    $competition = $this->parseCompetitionUrl($this->competition);

    $sql = "UPDATE
              ruk_project_keyword_sets
            SET
              name = '$this->keyword_set_name', lastupdate = CURDATE() - INTERVAL 1 DAY, customer_view = $this->customer_view, rankingspy_id = $rankingspy_id, measuring = '$this->measuring', rs_type = '$rs_type', competition = '$competition', team_email = '$this->teammail'
            WHERE
              id = '$id_kw_set'";

    // DELETE WEEKLY CACHE
    if ($this->measuring == 'weekly') {
      parent::deleteCacheFile($this->project_id, $id_kw_set);
    }

    $res = $this->db->query($sql);


    if (!$result) {
      return false;
    } else {
      return true;
    }

  }


  private function addNewKeywordSet ()
  {

    $this->parseEmail();

    $project_id  = $this->copy_to_project;
    $keyword_set = strip_tags($this->set_name);

    // max 10 keywordsets per customer
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE id_customer='".$project_id."'";
    $exists = $this->db->query($sql);

    if ($exists->num_rows >= $this->env_data['ruk_max_keywordsets']) {
      $this->out = array ('error' => 'Maximal ' . $this->env_data['ruk_max_keywordsets'] . ' Campaigns pro Projekt!');
      return;
    }

    // no duplicate set names
    $keyword_set = $keyword_set . ' KOPIE';
    $this->keyword_set_name = $this->checkSetnameExists($keyword_set, $project_id);

    $created_by = $_SESSION['firstname'] .' '. $_SESSION['lastname'];

    // add new keyword set to db
    $query = "INSERT INTO
                ruk_project_keyword_sets (id_customer, name, type, customer_view, measuring, created_by, created_on, team_email)
              VALUES
                ('" . $project_id . "', '" . $this->keyword_set_name . "', '" . $this->campaign_type . "', '" . $this->customer_view . "', '" . $this->measuring . "', '" . $created_by . "', '" . $this->dateYMD() . "', '" . $this->teammail . "')";

    $this->db->query($query);

    if (isset($this->db->insert_id)) {
      return $this->db->insert_id;
    } else {
      return false;
    }

  }


  private function checkSetnameExists ($keyword_set, $project_id) {

    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE name='".$keyword_set."' AND id_customer='".$project_id."' ";
    $exists = $this->db->query($sql);

    if ($exists->num_rows > 0) {
      $keyword_set = $keyword_set . ' / ' . $this->copy_count++;
      $keyword_set = $this->checkSetnameExists($keyword_set, $project_id);
    }

    return $keyword_set;

  }


	private function parseEmail() {

    $unparsed_teammail  = strip_tags($this->teammail);
    $unparsed_teammail  = explode("\n", $unparsed_teammail);

    $parsed_teammail = array();
    foreach ($unparsed_teammail as $mailadress) {
      $mailadress = strtolower(trim($mailadress));
      if (filter_var($mailadress, FILTER_VALIDATE_EMAIL)) {
        $parsed_teammail[] = $mailadress;
      }
    }

    $parsed_teammail = implode(',', $parsed_teammail);

    $this->teammail = $parsed_teammail;

  }

}

?>
