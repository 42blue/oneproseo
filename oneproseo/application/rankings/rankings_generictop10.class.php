<?php

require_once('ruk.class.php');

class rankings_generictop10 extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if(!isset($_POST['setid'])) {
      echo 'Keywordset not found';
      exit;
    }

    $this->keyword_setid = $_POST['setid'];
    $this->campaign_type = $_POST['type'];

    if ($this->keyword_setid == 'all' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets($_POST['projectid']);
    } else {
      $this->selectKeywordSet($_POST['setid']);
    }

    $result = $this->getRankingData();

    $this->createCSV($result);

  }



  private function selectAllKeywordSets ($selected_project)
  {

    $this->keywords = array();

    $sql = "SELECT a.id               AS setid,
                   a.measuring        AS measuring,
                   b.id               AS kwid,
                   b.keyword          AS keyword,
                   a.rs_type          AS rs_type,
                   c.url              AS url,
                   c.id               AS customerid,
                   c.country          AS country
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {

      $this->country      = $row['country'];
      $this->mo_type      = 'desktop';
      $this->measuring    = $row['measuring'];      
      if (!empty($row['keyword'])) {
        $this->keywords[] = $row['keyword'];
      }

    }

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

  }


  private function selectKeywordSet ($selected_keyword_set)
  {

    $sql = "SELECT a.id               AS setid,
                   a.name             AS setname,
                   a.measuring        AS measuring,                   
                   a.rs_type          AS rs_type,
                   b.keyword          AS keyword,
                   b.id               AS kwid,
                   b.tags             AS tags,                   
                   c.url              AS url,
                   c.name             AS customername,
                   c.country          AS country
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[]            = $row['keyword'];
      $this->country               = $row['country'];
      $this->setname               = $row['setname'];
      $this->customername          = $row['customername'];
      $this->mo_type               = $row['rs_type'];
      $this->measuring             = $row['measuring'];      
    }

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

  }



  private function getRankingData()
  {

    $date = parent::dateYMD();

    if ($this->measuring == 'weekly') {
      $date = date("Y-m-d", strtotime('Monday this week'));
    }
 
    // compare KEYWORDS TO CRAWL vs. SCRAPED KEYWORDS
    $comma_separated_kws = implode('", "', $this->keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $rank_add = 0;

    $sql = "SELECT
              id,
              keyword,
              timestamp
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$this->mo_type'
            AND
              timestamp = '$date'";

    $result = $this->db->query($sql);

    $this->resut_rows = $result->num_rows;

    while ($row = $result->fetch_assoc()) {
      $rows[] = $row;
    }

    if (empty($rows)) {
    return;
    }

    $nrows = array();

    foreach ($rows as $k => $v) {

      $idkw = $v['id'];

      $sql = "SELECT
                position,
                url,
                id_kw,
                hostname
              FROM
                ruk_scrape_rankings
              WHERE
                id_kw = '$idkw'
              AND
                position < 11
              ";

      $result2 = $this->db->query($sql);

      while ($row = $result2->fetch_assoc()) {
        $nrows[] = array('k' => $v['keyword'], 'p' => $row['position'], 'u' => $row['url'], 'h' => $row['hostname']);
      }

    }

    return $nrows;

  }


  private function createCSV ($result) {


    $csv_filename = parent::csvBuilder($result, 'export_generic_top_rankings');

    echo '<a href="'.$this->env_data['csvurl'] . $csv_filename.'">CSV download</a>';

  }  

}

?>
