<?php

header("Content-Type: text/html; charset=utf-8");

require_once('ruk.class.php');

class liverankings extends ruk {

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->restrictQuery();

    require_once ($this->env_data['path']. 'oneproseo/includes/logtofile.class.php');
    $oLogToFile = new logtofile($env_data);

    $this->country   = $_POST['lang'];
    $this->lang      = $_POST['lang'];
    $this->subdomain = $_POST['subdomain'];
    $this->type      = $_POST['type'];

    $this->hostname  = $this->createProperHostname($_POST['url']);
    $this->keywords  = $this->chunkKeywordSet($_POST['keywords']);

    if (empty($this->hostname) or empty($this->keywords)) {
      echo '<div class="row padded"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Bitte eine URL und Keywords angeben.<br/></strong></div></div></div>';
      exit;
    }

    if (isset($_SESSION['lastname'])) {
      $user = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];
    } else {
      $user = 'UNKOWN USER (NO SESSION DATA)';
    }

    $oLogToFile->write('LIVERANKING', 'Domain: '.$this->hostname.' - Keywords (Anzahl): '.count($this->keywords), 'temp/logs/log_liveranking.txt');

    $this->mySqlConnect();

	  $this->searchvolume = $this->fetchKeywordSearchVolumes($this->keywords);
    $this->rankings     = $this->fetchLiveRankings();

    parent::mySqlClose();

    $this->assemble();

    $this->output();

  }


  // restrict querys in customer version to 10
  private function restrictQuery () {

    require_once ($this->env_data['path']. 'oneproseo/includes/limitquerys.class.php');
    $limit  = new limitquerys($this->db, $this->env_data);
    $limit->checkLimit();

  }

  private function assemble () {

    require_once ($this->env_data['path']. 'oneproseo/includes/onedex.class.php');
    $oOnedex = new onedex;

    $res_searchvolume = array ();
    if (isset($this->searchvolume)) {
      foreach ($this->searchvolume as $k => $v) {
        $res_searchvolume[$v[0]] = array ($v[1], $v[2], $v[3]);
      }
    }

    $csv_h = array();
    $this->csv_s = array();
    $this->csv_m = array();
    $this->view  = array();

    foreach ($this->keywords as $keyword) {

    	$rank     = '';
    	$url      = '';
    	$featured = 0;
    	$dex      = 0;
      $opi  		= 0;
      $sv   		= 0;
      $ts   		= date('d.m.Y  (H:i) ', strtotime('today'));

      $cpc  		= 0;
      $comp 		= 0;

      // ADWORDS DATA?
      if (isset($res_searchvolume[$keyword])) {
        $sv   = $res_searchvolume[$keyword][0];
        $cpc  = $res_searchvolume[$keyword][1];
        $comp = $res_searchvolume[$keyword][2];
        $opi  = $sv * $cpc * $comp;
      }

      // NO RANKINGS? 
      if (empty($this->rankings[$keyword])) {
	    	$this->csv_s[] = array ($keyword, $rank, $url, $featured, $dex, $opi, $sv, $ts);
	    	$this->csv_m[] = array ($keyword, $rank, $url, $featured, $dex, $opi, $sv, $ts);
        $this->view[$keyword][]  = array ($keyword, $rank, $url, $featured, $dex, $opi, $sv, $ts);
      }

      // SINGLE RANKINGS? 
      if (count($this->rankings[$keyword]) == 1) {

        $url_f = $this->checkRSURL($this->rankings[$keyword][0][1], $this->rankings[$keyword][0][3]);

        $dset = array (
          $keyword, 
          $this->rankings[$keyword][0][0], 
          $url_f[0], 
          $url_f[1], 
          $oOnedex->calcOneDex($rank, $opi), 
          $opi, 
          $sv, 
          date('d.m.Y (H:i) ', $this->rankings[$keyword][0][2])
        );

        $this->csv_s[] = $dset;
        $this->csv_m[] = $dset;
        $this->view[$keyword][] = $dset;

      // DOPPELRANKINGS
      } else if (count($this->rankings[$keyword]) > 1) {

        $url_f = $this->checkRSURL($this->rankings[$keyword][0][1], $this->rankings[$keyword][0][3]);

		    $this->csv_s[] = array (
		    	$keyword, 
		    	$this->rankings[$keyword][0][0], 
		    	$url_f[0], 
		    	$url_f[1], 
		    	$oOnedex->calcOneDex($rank, $opi), 
		    	$opi, 
		    	$sv, 
          date('d.m.Y (H:i) ', $this->rankings[$keyword][0][2])
		    );

      	foreach ($this->rankings[$keyword] as $result) {

      		if (empty($result[0])) {continue;}

          $url_f = $this->checkRSURL($result[1], $result[3]);

      		$rank     = $result[0];
      		$url  	  = $url_f[0];
      		$featured = $url_f[1];
					$dex      = $oOnedex->calcOneDex($rank, $opi);
          $ts       = date('d.m.Y (H:i) ', $result[2]);

		    	$this->csv_m[] = array ($keyword, $rank, $url, $featured, $dex, $opi, $sv, $ts);
          $this->view[$keyword][] = array ($keyword, $rank, $url, $featured, $dex, $opi, $sv, $ts);
      	}

      } 

    }

    $csv_h[] = array('Keyword', 'Rank', 'URL', 'Featured', 'OneDex', 'OPI', 'Suchvolumen / Monat', 'Ranking ermittelt');

    $csv_s   = array_merge($csv_h, $this->csv_s);
    $csv_m   = array_merge($csv_h, $this->csv_m);

		$this->filename_single = $this->writeCsv($csv_s);    
    $this->filename_multi  = $this->writeCsv($csv_m);

  }


  private function output () {

  	$out_table = '';

    $lang = $this->lang;
    if ($this->type == 'mobile') {
      $lang = 'mobile_' . $this->lang;
    }

    $collector = array();
    foreach ($this->view as $keyw => $data) {

      $keyword   = $data[0][0];
      $rank      = $data[0][1];
      $url       = $data[0][2];
      $featured  = $data[0][3];
      $dex       = $data[0][4];
      $opi       = $data[0][5];
      $sv        = $data[0][6];
      $ts        = $data[0][7];  
      $rank_sort = $rank;

      if (empty($rank)) {
        $rank_sort = 101;
      }

      // DOPPELRANKINGS
      if (count($data) > 1) {

        $url_out_m = '';
        $rank_out_m = '';
        foreach ($data as $rankset) {

          $rank     = $rankset[1];
          $url      = $rankset[2];
          $featured = $rankset[3];

          $featured_out = '';
          if ($featured == 1) {
            $featured_out = ' <span style="color:red;">(featured)</span>';
          }

          $rank_out_m .= $rank . '<br />';
          $url_out_m  .= '<a href="' . $url . '" target="_blank">' . $url . '</a> ' . $featured_out . '<br/>';

        }

        $out_table .= '<tr>';

        $out_table .= '
          <td><a href="../rankings/overview/live/keywords/results/'.urlencode($keyword).'/'.$this->dateYMD().'/'.$lang.'" target="_blank">'.$keyword.'</a></td>
          <td data-sort="'. $rank_sort .'"><small class="rankings"><a href="../rankings/overview/live/keywords/results/'.urlencode($keyword).'/'.$this->dateYMD().'/'.$lang.'" target="_blank">TOP100</a></small>'.$rank_out_m.'</td>
          <td>'.$url_out_m.'</td>
          <td data-sort="'. $dex .'">'. $this->formatnumber($dex) .'</td>
          <td data-sort="'. $opi .'">'. $this->formatnumber($opi) .'</td>
          <td data-sort="'. $sv .'">'. $sv .'</td>
          <td>'. $ts .'</td>
        ';

        $out_table .= '</tr>';

            

      } else {

        $featured_out = '';
        if ($featured == 1) {
          $featured_out = ' <span style="color:red;">(featured)</span>';
        }

        $url_out   = '<a href="' . $url . '" target="_blank">' . $url . '</a> ' . $featured_out;

        $out_table .= '

          <tr>
            <td><a href="../rankings/overview/live/keywords/results/'.urlencode($keyword).'/'.$this->dateYMD().'/'.$lang.'" target="_blank">'.$keyword.'</a></td>
            <td data-sort="'. $rank_sort .'"><small class="rankings"><a href="../rankings/overview/live/keywords/results/'.urlencode($keyword).'/'.$this->dateYMD().'/'.$lang.'" target="_blank">TOP100</a></small>'.$rank.' </td>
            <td>'.$url_out.'</td>
            <td data-sort="'. $dex .'">'. $this->formatnumber($dex) .'</td>
            <td data-sort="'. $opi .'">'. $this->formatnumber($opi) .'</td>
            <td data-sort="'. $sv .'">'. $sv .'</td>
            <td>'. $ts .'</td>
          </tr>';

      }

    }

    $out = '<div class="col-md-12">
    				<div class="box">
    					<div class="box-header">
    						<span class="title">Ergebnisse den Hostname: '.$this->hostname.'</span>
    						<ul class="box-toolbar">
    							<li><a title="CSV - ohne Doppelrankings" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename_single . '" target="_blank"></a></li>
    							<li><i title="Google Drive Export - ohne Doppelrankings" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Live Rankings Export" data-filepath="' . $this->env_data['csvstore'] . $this->filename_single . '"></i></li>    						
    							<li><a title="CSV  - mit Doppelrankings" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename_multi . '" target="_blank"></a></li>
    							<li><i title="Google Drive Export - mit Doppelrankings" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Live Rankings Export" data-filepath="' . $this->env_data['csvstore'] . $this->filename_multi . '"></i></li>
    						</ul>
    					</div>
    				<div class="box-content">
    					<table class="table table-normal data-table">
		            <thead>
		              <tr>
		                <td>Keyword</td>
		                <td>Rank</td>
		                <td>URL</td>
		                <td>OneDex</td>
		                <td>OPI</td>
		                <td>Suchvolumen / Monat</td>
		                <td>Ranking ermittelt</td>
		              </tr>
		            </thead>';

    $out .= $out_table;

    $out .= '</table></div></div></div>';

    echo $out;

  }


  private function checkRSURL ($url, $feature) {

    $data = array($url, 0);

    if (stripos($url, '(featured)') > 0) {
      $url = trim(str_replace('(featured)', '', $url));
      $data = array($url, 1);
    }

    if ($feature == 1) {
      $url = trim(str_replace('(featured)', '', $url));
      $data = array($url, 1);
    }

    return $data;

  }


  private function chunkKeywordSet () {

    $kw   = strip_tags($_POST['keywords']);
    $kw   = strip_tags($kw);
    $kw   = explode("\n", $kw);
    $kw   = array_map('trim', $kw);
    $umla = array('Ä', 'Ü', 'Ö');
    $umlo = array('ä', 'ü', 'ö');
    $kws  = array();

    foreach ($kw as $value) {
      if (empty($value)) {
        continue;
      }
      $value = str_replace($umla, $umlo, $value);
      $value = strtolower($value);
      $value = preg_replace('/\s+/', ' ',$value);
      $kws[$value] = $value;
    }

    // slice @ 100 keywords
    if (count($kws) > 200) {
      $kws = array_slice($kws, 0, 200);
    }

    $kws = array_values($kws);

    return $kws;

  }


  private function fetchLiveRankings () {

    $fetch_live_ranking = array();
    $rankings = array();

    foreach ($this->keywords as $keyword) {
      $fetch_live_ranking[] = array($keyword, $this->lang);
      $rankings[$keyword] = array();
    }

    require_once ($this->env_data['path']. 'oneproseo/includes/liveranking.class.php');
    $rltRankings       = new liveranking($fetch_live_ranking, $this->hostname, $this->db, $this->lang, $this->subdomain, $this->type);
    $realtime_rankings = $rltRankings->getResultMulti();

    if (!empty($realtime_rankings)) {
      foreach ($realtime_rankings as $keyword => $set) {
          // WHY???
          if (!isset($rankings[$keyword])) {
            continue;
          }
	       foreach ($set as $data) {
	        $rankings[$keyword][] = array($data['p'], $data['u'], $data['ts'], $data['fe']);
	       }
      }
    }

    return $rankings;

  }


  private function fetchKeywordSearchVolumes ($payload) {

    $kws = array();
    foreach ($payload as $key => $value) {
      $kws[] = array ($value, $this->lang, $this->country);
    }

    require_once ($env_data['path']. 'oneproseo/includes/svolume.class.php');
    $oSvolume  = new svolume($this->db, $this->env_data);
    $result    = $oSvolume->querySV($kws);

    return $result;

  }

  public function createProperHostname ($url)
  {

    $url = strtolower(trim($url));

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));

  }

  public function formatnumber($num) {
    return number_format($num, 2, ',', '.');
  }

  private function writeCsv ($csv) {

    $filename = 'liverankings-data-'.mt_rand().'.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');

    foreach ($csv as $k => $line) {
      fputcsv($fp, $line);
    }

    fclose($fp);

    return $filename;

  }

  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      echo ('Connect failed:' .  mysqli_connect_error());
    }

  }


}

?>
