<?php

require_once('ruk.class.php');

class summary_overview extends ruk
{

  private $keyword_set_all = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['projectid'])) {

      $this->project_id = $_POST['projectid'];

      $this->getProjectData();

      $this->renderView();

    }

  }

  private function getProjectData()
  {

    $this->amount_keyword_sets = 0;
    $this->amount_keywords     = 0;
    $this->keyword_set_out     = '';
    $this->mo_show             = false;
    $this->hidechart           = false;

    // COUNT KEYWORDS
    $sql = "SELECT
              a.id             AS aid,
              b.id             AS bid,
              c.keyword        AS kw
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON c.id_kw_set = b.id
            WHERE a.id = '".$this->project_id."'";

    $result = $this->db->query($sql);
    //$row    = $result->fetch_array();
    while ($row = $result->fetch_assoc()) {
      $amount_keywords[] = $row['kw'];
      $amount_keywords_u[$row['kw']] = $row['kw'];
    }

    $this->amount_keywords = count ($amount_keywords);
    $this->amount_keywords_u = count ($amount_keywords_u);

    $s_query = '';
    if ($this->env_data['customer_version'] == 1) {
      $s_query = 'AND b.customer_view = 1';
    }

    $sql = "SELECT
              a.id                      AS aid,
              a.name                    AS project,
              a.url                     AS url,
              a.category                AS category,
              a.manager                 AS manager,
              a.ga_account              AS ga_account,
              a.gwt_account             AS gwt_account,
              a.local                   AS local,
              a.value                   AS value_all,
              a.value_now               AS value_now_all,
              b.id                      AS bid,
              b.customer_view           AS customer_view,
              b.name                    AS setname,
              b.type                    AS type,
              b.rs_type                 AS rs_type,
              b.measuring               AS measuring,
              b.created_by              AS created_by,
              b.created_on              AS created_on,
              b.value                   AS value,
              b.value_now               AS value_now,
              count(DISTINCT c.keyword) AS kwcount,
              count(DISTINCT d.url)     AS urlcount
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON c.id_kw_set = b.id
              LEFT JOIN ruk_project_urls d
                ON d.id_kw_set = b.id
            WHERE a.id = '".$this->project_id."'
            ".$s_query."
            GROUP BY bid ORDER BY setname";

    $result = $this->db->query($sql);

    $all_val          = 0;
    $all_val_now      = 0;
    $this->only_weely = true;
    $this->has_weekly = false;

    while ($row = $result->fetch_assoc()) {

      $this->projectname   = $row['project'];
      $this->projecturl    = $row['url'];
      $this->pjm           = $row['manager'];
      $this->category      = $row['category'];
      $this->ga_account    = $row['ga_account'];
      $this->local         = $row['local'];
      $this->mo_type       = $row['rs_type'];
      $this->gwt_account   = $row['gwt_account'];
      $this->measuring     = $row['measuring'];
      $this->created_by    = $row['created_by'];
      $this->created_on    = $row['created_on'];
      $this->settype       = $row['type'];

      if ($row['measuring'] == 'daily') {
        $this->only_weely = false;
      }
      if ($row['measuring'] == 'weekly') {
        $this->has_weekly = true;
      }

      if (is_null($row['setname'])) {

        $this->hidechart = true;
        $this->keyword_set_out .= '<div class="span6"><div class="box"><div class="box-content padded"><a href="'.$this->project_id.'/keywordsets"><span class="label label-green">Campaigns anlegen</span></a></div></div></div>';

      } else {

        $camp_val     = $row['value'];
        $camp_val_now = $row['value_now'];
        $all_val      = $row['value_all'];
        $all_val_now  = $row['value_now_all'];

        $this->all_val = $all_val;
        $this->amount_keyword_sets++;

        if ($this->mo_type == 'mobile') {
          $out_mo          = 'Google Mobile';
        } elseif ($this->mo_type == 'maps') {
          $out_mo          = 'Google Maps';
        } else {
          $this->mo_show   = true;
          $this->hidechart = false;
          $out_mo          = 'Google Desktop';
        }

        // KEYWORDSET OR URL SET?
        if ($row['type'] == 'url') {

            if ($row['urlcount'] == 0) {
              $this->keyword_set_out .= $this->emptySet($row['setname'], $row['bid'], $row['urlcount'], $row['type']);
            } else {
              $this->keyword_set_out .= '<div class="span6"><div class="box"><div class="box-header"><span class="big-blue">'.$row['setname'].'</span> <span class="small">URL Campaign / ' . $out_mo ;
              $this->keyword_set_out .= $this->setInfo();
              $this->keyword_set_out .= '<ul class="box-toolbar">';
              $this->keyword_set_out .= $this->personalDashboard($row['bid'], $row['aid']);
              $this->keyword_set_out .= $this->editKeywordset($row['bid'], $row['urlcount'], $row['type']);

              $this->keyword_set_out .= '</ul></div><div class="box-content ops_setdata ops_setdata_bg" data-setid="'.$row['bid'].'" data-projectid="'.$row['aid'].'">';

              $this->keyword_set_out .= $this->chartload();

              $this->keyword_set_out .= '</div><div class="box-footer"><ul class="box-toolbar"><li><a href="'.$this->project_id.'/detail-keyword/'.$row['bid'].'" target="_blank"><span class="label label-dark-blue f12">Keywords / Rankings</span></a></li><li><a href="'.$this->project_id.'/detail-url/'.$row['bid'].'" target="_blank"><span class="label label-red f12">URL / Keywords (Campaign)</span></a></li><li><a href="'.$this->project_id.'/detail-url-complete/'.$row['bid'].'" target="_blank"><span class="label label-red f12">URL / Keywords (DB)</span></a></li><li><a href="'.$this->project_id.'/detail-url-onedex/'.$row['bid'].'" target="_blank"><span class="label label-blue f12">URL / OneDex (Campaign)</span></a></li><li><a href="'.$this->project_id.'/detail-url-onedex-complete/'.$row['bid'].'" target="_blank"><span class="label label-blue f12">URL / OneDex (DB)</span></a></li><li><a href="'.$this->project_id.'/detail-keyword-url/'.$row['bid'].'" target="_blank"><span class="label label-green f12">Keywords / URL</span></a></li><li><a href="'.$this->project_id.'/detail-keyword-url-duplicate/'.$row['bid'].'" target="_blank"><span class="label label-pink f12">Doppelrankings</span></a></li><li><a href="'.$this->project_id.'/detail-compare/'.$row['bid'].'/'.$this->germanTSred($this->created_on).'" target="_blank"><span class="label label-cyan f12">Ranking Datumsvergleich</span></a></li>';

              $this->keyword_set_out .= $this->renderCompetitionLink($row['bid']);
              $this->keyword_set_out .= $this->renderGaAccount();
              $this->keyword_set_out .= $this->renderScAccount($row['bid']);
              $this->keyword_set_out .= '</ul></div></div></div>';
            }

        } else {

            if ($row['kwcount'] == 0) {
              $this->keyword_set_out .= $this->emptySet($row['setname'], $row['bid'], $row['kwcount'], $row['type']);
            } else {

              $this->keyword_set_out .= '<div class="span6"><div class="box"><div class="box-header"><span class="big-green">'.$row['setname'].'</span> <span class="small">Keyword Campaign / ' . $out_mo;
              $this->keyword_set_out .= $this->setInfo();

              $this->keyword_set_out .= $this->showMediaValue($camp_val, $camp_val_now);
              $this->keyword_set_out .= '<ul class="box-toolbar">';
              $this->keyword_set_out .= $this->personalDashboard($row['bid'], $row['aid']);
              $this->keyword_set_out .= $this->editKeywordset($row['bid'], $row['kwcount'], $row['type']);

              $this->keyword_set_out .= '</ul></div><div class="box-content ops_setdata ops_setdata_bg" data-setid="'.$row['bid'].'" data-projectid="'.$row['aid'].'">';

              $this->keyword_set_out .= $this->chartload();

              $this->keyword_set_out .= '</div><div class="box-footer"><ul class="box-toolbar"><li><a href="'.$this->project_id.'/detail-keyword/'.$row['bid'].'" target="_blank"><span class="label label-dark-blue f12">Keywords / Rankings</span></a></li><li><a href="'.$this->project_id.'/detail-url/'.$row['bid'].'" target="_blank"><span class="label label-red f12">URL / Keywords (Campaign)</span></a></li><li><a href="'.$this->project_id.'/detail-url-complete/'.$row['bid'].'" target="_blank"><span class="label label-red f12">URL / Keywords (DB)</span></a></li><li><a href="'.$this->project_id.'/detail-url-onedex/'.$row['bid'].'" target="_blank"><span class="label label-blue f12">URL / OneDex (Campaign)</span></a></li><li><a href="'.$this->project_id.'/detail-url-onedex-complete/'.$row['bid'].'" target="_blank"><span class="label label-blue f12">URL / OneDex (DB)</span></a></li><li><a href="'.$this->project_id.'/detail-keyword-url/'.$row['bid'].'" target="_blank"><span class="label label-green f12">Keywords / URL</span></a></li><li><a href="'.$this->project_id.'/detail-keyword-url-duplicate/'.$row['bid'].'" target="_blank"><span class="label label-pink f12">Doppelrankings</span></a></li><li><a href="'.$this->project_id.'/detail-compare/'.$row['bid'].'/'.$this->germanTSred($this->created_on).'" target="_blank"><span class="label label-cyan f12">Ranking Datumsvergleich</span></a></li>';

              $this->keyword_set_out .= $this->renderCompetitionLink($row['bid']);
              $this->keyword_set_out .= $this->renderGaAccount();
              $this->keyword_set_out .= $this->renderScAccount($row['bid']);              
              $this->keyword_set_out .= '</ul></div></div></div>';

            }

        }

      }

    }

    // ALL KEYWORDSETS

    if ($result->num_rows > 1 && $this->amount_keywords > 0 && $this->mo_show == true) {

      $this->settype = 'key';

      $this->keyword_set_all = '<div class="span6"><div class="box"><div class="box-header"><span class="big-green">Alle Keywords</span> <span class="small">Keyword Campaign / Google Desktop</span>';
      $this->keyword_set_all .= $this->showMediaValue($all_val, $all_val_now);
      $this->keyword_set_all .= '<ul class="box-toolbar">';
      $this->keyword_set_all .= $this->personalDashboard('all', $this->project_id);
      $this->keyword_set_all .= '</ul></div><div class="box-content ops_setdata ops_setdata_bg" data-setid="all" data-projectid="'.$this->project_id.'"">';

      $this->keyword_set_all .= '<div class="ops_select_view"><span class="btn btn-default ops_setload_dex12m"><i class="icon-bar-chart"></i> 12 Monate OneDex laden</span><span class="btn btn-default ops_setload_dex12w"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden</span>';
      if ($this->only_weely == true) {
        $this->keyword_set_all .= '<span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Wochen Rankingverteilung laden</span></div><div class="ops_show_view" style="display:none;"></div>';
      } else {
        $this->keyword_set_all .= '<span class="btn btn-default ops_setload_dex31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden</span><span class="btn btn-default ops_setload_dex7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden</span><span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Tage Rankingverteilung laden</span></div><div class="ops_show_view" style="display:none;"></div>';
      }

      $this->keyword_set_all .= '</div><div class="box-footer"><ul class="box-toolbar"><li><a href="'.$this->project_id.'/detail-keyword/all" target="_blank"><span class="label label-dark-blue f12">Keywords / Rankings</span></a></li><li><a href="'.$this->project_id.'/detail-url/all" target="_blank"><span class="label label-red f12">URL / Keywords (Campaign)</span></a></li><li><a href="'.$this->project_id.'/detail-url-complete/all" target="_blank"><span class="label label-red f12">URL / Keywords (DB)</span></a></li><li><a href="'.$this->project_id.'/detail-url-onedex/all" target="_blank"><span class="label label-blue f12">URL / OneDex (Campaign)</span></a></li><li><a href="'.$this->project_id.'/detail-url-onedex-complete/all" target="_blank"><span class="label label-blue f12">URL / OneDex (DB)</span></a></li><li><a href="'.$this->project_id.'/detail-keyword-url/all" target="_blank"><span class="label label-green f12">Keywords / URL</span></a></li><li><a href="'.$this->project_id.'/detail-keyword-url-duplicate/all" target="_blank"><span class="label label-pink f12">Doppelrankings</span></a></li><li><a href="'.$this->project_id.'/detail-compare/all/'.$this->germanTSred($this->created_on).'" target="_blank"><span class="label label-cyan f12">Ranking Datumsvergleich</span></a></li>';
      $this->keyword_set_all .= $this->renderCompetitionLink('all');
      $this->keyword_set_all .= $this->renderGaAccount();
      $this->keyword_set_all .= $this->renderScAccount('all');      
      $this->keyword_set_all .= '</ul></div></div></div>';

    }

  }


  private function renderView ()
  {

    echo '<div class="row"><div class="col-md-6"><div class="box"><div class="box-header"><span class="title">Domain Daten</span></div><div class="box-content"><table class="table table-normal"><tbody><tr class="status-pending"><td class="icon"><i class="icon-bookmark"></i></td><td>Domain:</td><td><span id="ops_url" data-url="'.$this->projecturl.'">'.$this->projecturl.'</span></td></tr><tr class="status-pending"><td class="icon"><i class="icon-eye-open"></i></td><td>Campaigns</td><td>'.$this->amount_keyword_sets.'</td></tr></tbody></table></div></div></div><div class="col-md-6"><div class="box"><div class="box-header"><span class="title">Projekt Daten</span>';

    if ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'pjm') {
      echo '<ul class="box-toolbar"><li><a href="'.$this->project_id.'/keywordsets"><span class="label label-red-super">Campaigns bearbeiten</span></a></li><li><a href="../projects-change/'.$this->project_id.'"><span class="label label-red-super">Projekt bearbeiten</span></a></li><li><a href="'.$this->project_id.'/api"><span class="label label-blue">JSON API </span></a></li></ul>';
    } else {
      echo '<ul class="box-toolbar"><li><a href="'.$this->project_id.'/api"><span class="label label-blue">JSON API </span></a></li></ul>';
    }

    echo '</div><div class="box-content"><table class="table table-normal"><tbody><tr class="status-pending"><td class="icon"><i class="icon-lightbulb"></i></td><td>Projektname:</td><td>'.$this->projectname.'</td></tr><tr class="status-pending"><td class="icon"><i class="icon-user"></i></td><td>SEO Project Manager:</td><td>'.$this->pjm.'</td></tr></tbody></table></div></div></div></div>';

    if ($this->local == 1) {
      echo '<div class="row"><div class="col-md-12"><div class="box"><div class="box-header padded"><a href="../local/'.$this->project_id.'"><span class="btn btn-default f12"><span>zu den lokalen Rankings</span></span></a></div></div></div></div>';
    }

    if ($this->hidechart == false) {

      echo '<div class="row"><div class="col-md-12"><div class="box"><div class="box-header"><div class="title">OneDex / ∅ Rankings</div></div><div class="box-content"><div class="row"><div class="col-md-12"><div id="OPSC_opi_highchart"><i class="oneproseo-preloader"></i></div></div></div></div><div class="box-content"><div class="row"><div class="col-md-12"><hr /></div></div></div><div class="box-content padded"><div class="row"><div class="col-md-12"><div class="row"><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-bolt"></i></li><li class="count"><span class="hint" title="Alle Keywords inkl. Duplikate">'. $this->amount_keywords .'</span> / <span class="hint" title="Alle Keyword OHNE Duplikate">'.$this->amount_keywords_u.'</span></li></ul><div class="stats-label">überwachte Keywords</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-signal"></i></li><li class="count" id="ops_top10"></li></ul><div class="stats-label">Top 10 Rankings</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-signal"></i></li><li class="count" id="ops_top20"></li></ul><div class="stats-label">Top 20 Rankings</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-signal"></i></li><li class="count" id="ops_top30"></li></ul><div class="stats-label">Top 30 Rankings</div></div></div><div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-search"></i></li><li class="count" id="ops_topQ"></li></ul><div class="stats-label">∅ Ranking</div></div></div>';

      if ($_SESSION['role'] == 'admin' && $this->gwt_account == 1) {
        echo '<div class="col-md-2"><div class="dashboard-stats small"><ul class="list-inline"><li class="glyph"><i class="icon-money"></i></li><li class="count">€ '.number_format($this->all_val, 2, ',', '.').'</li></ul><div class="stats-label">Media Equivalent Value</div></div></div>';
      }

      echo '</div></div></div></div></div></div><div class="col-md-12"><div class="box"><div class="box-header">

      <div class="title">Gewinner / Unverändert / Verlierer</div>
      <ul class="box-toolbar"><li><span id="ops_open" class="label label-gray">aufklappen</span></a></li>';

      if ($this->only_weely == true) {
        echo '<li><span id="ops_load_wl_1" class="label label-red">Sonntag vs. Vorwoche</span></a></li><li><span id="ops_load_wl_6" class="label label-blue">Sonntag vs. zwei Wochen</span></a></li>';
        echo '<li><a title="CSV download" class="icon-cloud-download csv-request" href="http://enterprise.oneproseo.com/temp/export/winner_looser_weekly'.$this->project_id.'.csv" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Winner Looser Export" data-filepath="/var/www/enterprise.oneproseo.com/temp/export/winner_looser_weekly'.$this->project_id.'.csv"></i></li>';
      } else {
        echo '<li><span id="ops_load_wl_1" class="label label-red">Heute vs. Gestern</span></a></li><li><span id="ops_load_wl_6" class="label label-blue">Heute vs. Vorwoche</span></a></li>';
        echo '<li><a title="CSV download" class="icon-cloud-download csv-request" href="http://enterprise.oneproseo.com/temp/export/winner_looser_'.$this->project_id.'.csv" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Winner Looser Export" data-filepath="/var/www/enterprise.oneproseo.com/temp/export/winner_looser_'.$this->project_id.'.csv"></i></li>';
      }

      echo '</ul></div><div class="box-content"><div class="row"><div class="col-md-12"><div class="cf-winner-looser" id="ops_winner_looser"><div><i class="oneproseo-preloader"></i></div></div></div></div></div></div></div><div class="col-md-12"><div class="box"><div class="box-header padded">';

      if ($this->only_weely == true) {
        echo '<span class="btn btn-default f12" id="ops_load_seven_graph12m"><i class="icon-bar-chart"></i> 12 Monate OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph12w"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_table"><i class="icon-table"></i> 7 Wochen Rankingverteilung laden (alle)</span>';
      } else {
        echo '<span class="btn btn-default f12" id="ops_load_seven_graph12m"><i class="icon-bar-chart"></i> 12 Monate OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph12w"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden (alle)</span>';
        echo '<span class="btn btn-default f12" id="ops_load_seven_graph31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_table"><i class="icon-table"></i> 7 Tage Rankingverteilung laden (alle)</span>';
      }

      echo '<span class="btn btn-default f12" id="ops_clear_views"><i class="icon-refresh"></i> Refresh </span></div></div></div></div>';
    }

    echo'<div class="row-fluid">';

    echo $this->keyword_set_all;
    echo $this->keyword_set_out;

    echo '</div></div>';

  }


  private function emptySet($setname, $setid, $count, $type) {

    $settype  = array ('url' => '(URL Campaign)', 'key' => '(Keyword Campaign)');
    $setident = array ('url' => 'URLs', 'key' => 'Keywords');
    $setcolor = array ('url' => 'blue', 'key' => 'green');

    return '<div class="span6"><div class="box"><div class="box-header"><span class="big-'.$setcolor[$type].'">'.$setname.'</span> <span class="small">'.$settype[$type].'</span><ul class="box-toolbar"><li><a href="'.$this->project_id.'/keywords/'.$setid.'"><span class="label label-'.$setcolor[$type].'"><span f12>'.$count.'</span> '.$setident[$type].'</span></a></li></ul></div><div class="box-content padded"><a href="'.$this->project_id.'/keywords/'.$setid.'"><span class="label label-'.$setcolor[$type].'">'.$setident[$type].' hinzufügen</span></a></div></div></div>';

  }


  private function showMediaValue($camp_val, $camp_val_now) {

    if ($_SESSION['role'] == 'admin' && $this->gwt_account == 1) {
      return '<ul class="box-toolbar-price"><li><span class="btn btn-default mediavalue f12">Media Equivalent Value: € ' . number_format($camp_val, 2, ',', '.') . ' / € ' . number_format($camp_val_now, 2, ',', '.') . '</span></li></ul>';
    } else {
      return '';
    }

  }

  private function setInfo () {

    if ($this->measuring == 'daily') {
      $measuring = ' / täglich';
    } else {
      $measuring = ' / wöchentlich';
    }

    if (empty($this->created_by)) {
      $created_by = '';
    } else {
      $created_by = ' / ' . $this->created_by;
    }

    return $measuring . $created_by . ' / ' . $this->germanTSred($this->created_on) . '</span>';

  }


  private function chartload () {

    $data = '<div class="ops_select_view"><span class="btn btn-default ops_setload_dex12m"><i class="icon-bar-chart"></i> 12 Monate OneDex laden</span><span class="btn btn-default ops_setload_dex12w"><i class="icon-bar-chart"></i> 12 Wochen OneDex laden</span>';

    if ($this->measuring == 'daily') {
      $data .= '<span class="btn btn-default ops_setload_dex31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden</span><span class="btn btn-default ops_setload_dex7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden</span><span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Tage Rankingverteilung laden</span></div><div class="ops_show_view" style="display:none;"></div>';
    } else {
      $data .= '<span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Wochen Rankingverteilung laden</span></div><div class="ops_show_view" style="display:none;"></div>';
    }

    return $data;

  }


  private function editKeywordset($setid, $count, $type) {

    $settype = array ('url' => 'URL(s)', 'key' => 'Keyword(s)');

    if ($_SESSION['role'] == 'admin') {
      return '<li><a href="'.$this->project_id.'/keywords/'.$setid.'"><span class="label label-red-super f12"><span>'.$count.'</span> '.$settype[$type].'</span></a></li>';
    } else {
      return '<li><span class="label label-blue f12"><span>'.$count.'</span> '.$settype[$type].'</span></li>';
    }

  }


  private function renderGaAccount()
  {

    if (!empty($this->ga_account)) {
      return '<li><a href="'.$this->project_id.'/detail-url-onedex-analytics-complete/all" target="_blank"><span class="label label-gold f12">Analytics / OneDex</span></a></li>';
    }

  }


  private function renderScAccount($set)
  {

    if ($this->gwt_account == 1) {
      return '<li><a href="'.$this->project_id.'/detail-personal-index/'.$set.'" target="_blank"><span class="label label-violet f12">Personal Index</span></a></li>';
    }

  }


  private function renderCompetitionLink($set)
  {

    if ($this->settype != 'url') {
      return '<li><a href="'.$this->project_id.'/detail-competition/'.$set.'" target="_blank"><span class="label label-purple f12">Wettbewerbsvergleich</span></a></li>';
    }

  }

  private function personalDashboard($setid, $projectid)
  {

    if (isset($_SESSION['ruk_personal_dash'])) {
      foreach ($_SESSION['ruk_personal_dash'] as $key => $value) {
        if ($value['setid'] == $setid && $value['projectid'] == $projectid) {
          return '<li><i title="Already in personal dashboard" class="info-sortable icon-ok"></i></li>';
        }
      }
    }

    if (isset($_SESSION['userid'])) {
      return '<li><i data-setid="'.$setid.'" data-projectid="'.$projectid.'" title="Add to personal dashboard" class="add-sortable icon-plus-sign"></i></li>';
    }

  }

}

?>
