<?php

require_once('ruk.class.php');

class keywords_edit_tags extends ruk
{

  private $copy_count = 1;

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler () {

    // SHOW
    if (isset($_POST['setid']) && $_POST['action'] == 'show') {

      $this->keyword_set = $_POST['setid'];

      $this->selectKeywordSet();
      $this->renderView();

      echo $this->out;

    }

    // CHANGE SET
    if (isset($_POST['kwid']) && $_POST['action'] == 'change') {
      $this->changeTags($_POST['kwid'], strip_tags($_POST['tags']));
    }

  }


  private function selectKeywordSet () {

    $this->keywords = array();
    
    $selected_keyword_set = $this->keyword_set;

    $sql = "SELECT a.id            AS setid,
                   a.name          AS setname,
                   b.id            AS kwid,
                   b.id_kw_set     AS kwsetid,
                   b.keyword       AS keyurl,
                   b.tags          AS tags,
                   c.name          AS customer,
                   c.country       AS country
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '$selected_keyword_set'
            ORDER BY keyurl";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->customer               = $row['customer'];
      $this->set_name               = $row['setname'];
      $this->keywordsetid           = $row['setid'];
      if (isset($row['kwid'])) {
        $this->keywords[$row['kwid']] = array($row['keyurl'], $row['tags']);        
      }
    }

  }


  private function renderView () {

    if ($this->campaign_type == 'url') {

      $this->out = '

          <div class="box">
            <div class="box-header">
              <span class="title">URLs bearbeiten für Projekt: '.$this->customer.' / Campaign: <a href="../detail-keyword/'.$this->keywordsetid.'">'.$this->set_name.'</a></span>
            </div>
            <div class="box-content padded">
              WONT HAPPEN
            </div>
          </div>';

    } else {

      $render = '<table class="tagedit">';
      foreach ($this->keywords as $id => $keyword) {
        $render .= '<tr><td style="width:300px;"><b>' . $keyword[0] . '</b></td><td><textarea data-kwid="'.$id.'" class="tags" placeholder="Tags bearbeiten">' . $keyword[1] . '</textarea></td></tr>';
      }
      $render .= '</table>';

      if (empty($this->keywords)) {
        $render = '<p>Keine Keywords vorhanden.</p>';
      }

      $this->out = '

          <div class="box">
            <div class="box-header">
              <span class="title">Tags bearbeiten für Projekt: '.$this->customer.' / Campaign: <a href="../detail-keyword/'.$this->keywordsetid.'">'.$this->set_name.'</a></span>
            </div>
            <div class="box-content padded">
              <form class="form-horizontal fill-up" role="form" id="ops_rankings_save_keywords">
                <div class="form-group">
                  <div class="col-sm-12">
                    '.$render.'
                  </div>
                </div>
              </form>
            </div>
          </div>';

    }

  }


  private function changeTags ($id, $tags) {

    $tags = preg_replace('/\s\s+/', ',', $tags);

    $sql = "UPDATE ruk_project_keywords SET tags = '$tags' WHERE id = '$id'";

    $this->db->query($sql);
  
  }


}

?>
