<?php

header('Content-Type: text/html; charset=utf-8');

date_default_timezone_set('CET');

require_once('rankingspy/rankingspy.class.php');

class ruk {


  public function setEnv ($env_data)
  {

    $this->env_data = $env_data;

  }


  public function startSession ()
  {

    session_start();
    // allow multiple ajax requests (SESSION)
    session_write_close();

    if ($_SESSION['login'] != true) {
      exit;
    }

  }


  public function submenu ()
  {

    if (!empty($this->days)) {

      $out = '  <li><a target="_blank" href="../../detail-keyword/'.$this->keyword_setid.'"><span class="label label-dark-blue">Keywords / Rankings</span></a></li>
                <li><a target="_blank" href="../../detail-url/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (Campaign)</span></a></li>
                <li><a target="_blank" href="../../detail-url-complete/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (DB)</span></a></li>
                <li><a target="_blank" href="../../detail-url-onedex/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (Campaign)</span></a></li>
                <li><a target="_blank" href="../../detail-url-onedex-complete/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (DB)</span></a></li>
                <li><a target="_blank" href="../../detail-keyword-url/'.$this->keyword_setid.'"><span class="label label-green">Keywords / URL</span></a></li>
                <li><a target="_blank" href="../../detail-keyword-url-duplicate/'.$this->keyword_setid.'"><span class="label label-pink">Doppelrankings</span></a></li>
                <li><a target="_blank" href="../../detail-compare/'.$this->keyword_setid.'/'.$this->germanTSred($this->created_on).'"><span class="label label-cyan">Datumsvergleich</span></a></li>
             ';

      if ($this->campaign_type[0] != 'url') {
        $out .= '<li><a target="_blank" href="../../detail-competition/'.$this->keyword_setid.'"><span class="label label-purple">Wettbewerbsvergleich</span></a></li>';
      }


    } else {

      $out = '  <li><a target="_blank" href="../detail-keyword/'.$this->keyword_setid.'"><span class="label label-dark-blue">Keywords / Rankings</span></a></li>
                <li><a target="_blank" href="../detail-url/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (Campaign)</span></a></li>
                <li><a target="_blank" href="../detail-url-complete/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (DB)</span></a></li>
                <li><a target="_blank" href="../detail-url-onedex/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (Campaign)</span></a></li>
                <li><a target="_blank" href="../detail-url-onedex-complete/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (DB)</span></a></li>
                <li><a target="_blank" href="../detail-keyword-url/'.$this->keyword_setid.'"><span class="label label-green">Keywords / URL</span></a></li>
                <li><a target="_blank" href="../detail-keyword-url-duplicate/'.$this->keyword_setid.'"><span class="label label-pink">Doppelrankings</span></a></li>
                <li><a target="_blank" href="../detail-compare/'.$this->keyword_setid.'/'.$this->germanTSred($this->created_on).'"><span class="label label-cyan">Datumsvergleich</span></a></li>
             ';

       if ($this->campaign_type[0] != 'url') {
         $out .= '<li><a target="_blank" href="../detail-competition/'.$this->keyword_setid.'"><span class="label label-purple">Wettbewerbsvergleich</span></a></li>';
       }

    }


    if (!empty($this->ga_account)) {
      $out .= '<li><a target="_blank" href="../detail-url-onedex-analytics-complete/'.$this->keyword_setid.'"><span class="label label-gold">Analytics / OneDex (DB)</span></a></li>';
    }

    if ($this->gwt_account == 1) {
      $out .= '<li><a href="../detail-personal-index/'.$this->keyword_setid.'" target="_blank"><span class="label label-violet">Personal Index</span></a></li>';
    }

    return $out;

  }


  public function submenuLocal ()
  {

    if (!empty($this->days)) {

      $out = '  <li><a target="_blank" href="../../detail-keyword/'.$this->keyword_setid.'"><span class="label label-dark-blue">Keywords / Rankings</span></a></li>
                <li><a target="_blank" href="../../detail-url/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (Campaign)</span></a></li>
                <li><a target="_blank" href="../../detail-url-complete/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (DB)</span></a></li>
                <li><a target="_blank" href="../../detail-url-onedex/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (Campaign)</span></a></li>
                <li><a target="_blank" href="../../detail-url-onedex-complete/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (DB)</span></a></li>
                <li><a target="_blank" href="../../detail-keyword-url/'.$this->keyword_setid.'"><span class="label label-green">Keywords / URL</span></a></li>
                <li><a target="_blank" href="../../detail-compare/'.$this->keyword_setid.'/'.$this->germanTSred($this->created_on).'"><span class="label label-cyan">Datumsvergleich</span></a></li>
             ';

    } else {

      $out = '  <li><a target="_blank" href="../detail-keyword/'.$this->keyword_setid.'"><span class="label label-dark-blue">Keywords / Rankings</span></a></li>
                <li><a target="_blank" href="../detail-url/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (Campaign)</span></a></li>
                <li><a target="_blank" href="../detail-url-complete/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (DB)</span></a></li>
                <li><a target="_blank" href="../detail-url-onedex/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (Campaign)</span></a></li>
                <li><a target="_blank" href="../detail-url-onedex-complete/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (DB)</span></a></li>
                <li><a target="_blank" href="../detail-keyword-url/'.$this->keyword_setid.'"><span class="label label-green">Keywords / URL</span></a></li>
                <li><a target="_blank" href="../detail-compare/'.$this->keyword_setid.'/'.$this->germanTSred($this->created_on).'"><span class="label label-cyan">Datumsvergleich</span></a></li>
             ';

    }


    if (!empty($this->ga_account)) {
      $out .= '<li><a target="_blank" href="../detail-url-onedex-analytics-complete/'.$this->keyword_setid.'"><span class="label label-gold">Analytics / OneDex (DB)</span></a></li>';
    }

    return $out;

  }

  public function checkDifferenceBetweenRankings ($today, $yesterday, $markup = true)
  {

    if ($yesterday === NULL && $today != NULL) {
      $change = 101 - intval($today);
      if ($markup === false) {
        return '+ ' .  $change;
      } else {
        return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+' .  $change . '</small>';
      }
    } else if ($today === NULL && $yesterday != NULL) {
      $change = intval($today) + 1;
      if ($markup === false) {
        return '- ' . $change;
      } else {
        return ' <i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-' .  $change . '</small>';
      }
    }

    if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      if ($markup === false) {
        return '+ ' . $change;
      } else {
        return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+' .  $change . '</small>';
      }
    } else if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      if ($markup === false) {
        return '- ' . $change;
      } else {
        return ' <i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-' .  $change . '</small>';
      }
    }

  }


  public function checkDifferenceBetweenMetrics ($today, $yesterday, $align = 'right')
  {

    if ($yesterday === NULL && $today != NULL) {
      return ' <i class="'.$align.' status-success icon-circle-arrow-up"></i>';
    } else if ($today === NULL && $yesterday != NULL) {
      return ' <i class="'.$align.' status-error icon-circle-arrow-down"></i>';
    }

    if ($today < $yesterday) {
      return ' <i class="'.$align.' status-success icon-circle-arrow-up"></i>';
    } else if ($today > $yesterday) {
      return ' <i class="'.$align.' status-error icon-circle-arrow-down"></i>';
    }

  }


  public function checkDifferenceBetweenRankingDays ($today, $yesterday, $symbol = true)
  {

    if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      if ($symbol === true) {$out = '<i class="status-success icon-circle-arrow-up"></i> ';} else {$perc = '';}
      $out .= '<small class="status-success">+' . number_format($change, 0, ',', '.') . '</small>';
    } else if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      if ($symbol === true) {$out = '<i class="status-error icon-circle-arrow-down"></i> ';} else {$perc = '';}
      $out .= '<small class="status-error">-' .  number_format($change, 0, ',', '.') . '</small>';
    }

    return $out;

  }


  public function checkDifferenceBetweenRankingDaysReverse ($today, $yesterday)
  {

    if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+' .  $change . '</small>';
    } else if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      return ' <i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-' .  $change . '</small>';
    }

  }


  public function calcPercent ($val1, $val2, $symbol = true, $precision = 2)
  {

    if ($val1 != 0 && $val2 != 0) {

      $perc = $val1 * 100 / $val2;
      $perc = 100 - $perc;
      $perc = round ($perc, $precision);

      if ($perc > 0) {
        if ($symbol === true) {$out = '<span class="nowrap"><i class="status-error icon-circle-arrow-down"></i> ';} else {$perc = '';}
        $out .= '<small class="status-error">-' . $perc . '%</small></span>';
      } elseif ($perc < 0) {
        if ($symbol === true) {$out = '<span class="nowrap"><i class="status-success icon-circle-arrow-up"></i> ';} else {$perc = '';}
        $out .= '<small class="status-success">' . $perc * -1 . '%</small></span>';
      } else {
        $out .= '<span class="nowrap"><small class="status-success">0 %</small></span>';
      }

    } else {

      $out = '';

    }

    return $out;

  }

 public function calcPercentDex ($val1, $val2, $precision = 2, $markup = true)
  {

    if ($val1 == 0 && $val2 == 0) {
      if ($markup === false) {
        return '0%';
      } else {
        return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">0%</small>';
      }
    }

    if ($val1 > $val2) {

      if ($val2 == 0) {
        if ($markup === false) {
          return '+100%';
        } else {
          return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+100%</small>';
        }
      }

      $diff = $val1 - $val2;
      $perc = $diff * 100 / $val2;
      $perc = round ($perc, $precision);
      $out   = $perc;

      if ($markup === false) {
        return '+'.$perc.'%';
      } else {
        return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+'.$perc.'%</small>';
      }

    } elseif ($val1 < $val2) {

      $diff = $val2 - $val1;
      $perc = $diff * 100 / $val2;
      $perc = round ($perc, $precision);

      if ($markup === false) {
        return '-' . $perc . '%';
      } else {
        return '<i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-' .  $perc . '%</small>';
      }

    } else {
      if ($markup === false) {
        return '0%';
      } else {
        return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">0%</small>';
      }
    }

    return $out;

  }


  // FETCH URLS in KEYWORD CAMPAIGN
  public function fetchURLSet($kw_set_id)
  {

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // CAMPAIGN INFO
  public function getKeywordSetData ($kw_set_id) {
    $sql = "SELECT * FROM ruk_project_keyword_sets WHERE id = '$kw_set_id'";
    $res = $this->db->query($sql);
    while ($row = $res->fetch_assoc()) {
      $data = $row;
    }
    return $data;
  }

  public function checkCampainTypeComp ($kw_set_id) {
    $sql = "SELECT id, type, measuring FROM ruk_project_keyword_sets WHERE id = '$kw_set_id'";
    $res = $this->db->query($sql);
    while ($row = $res->fetch_assoc()) {
      $data = array($row['type'], $row['measuring']);
    }
    return $data;
  }

  // URL OR KEYWORD CAMPAIGN
  public function checkCampainType ($kw_set_id) {
    $sql = "SELECT id, type FROM ruk_project_keyword_sets WHERE id = '$kw_set_id'";
    $res = $this->db->query($sql);
    $type = 'key';
    while ($row = $res->fetch_assoc()) {
      $type = $row['type'];
    }
    return $type;
  }

  // URL OR KEYWORD CAMPAIGN
  public function checkCampainTypeLocalComp ($kw_set_id) {
    $sql = "SELECT * FROM ruk_local_keyword_sets WHERE id = '$kw_set_id'";
    $res = $this->db->query($sql);
    while ($row = $res->fetch_assoc()) {
      $data = array($row['type'], $row['rs_type']);
    }
    return $data;
  }

  // URL OR KEYWORD CAMPAIGN
  public function checkCampainTypeLocal ($kw_set_id) {

    $sql = "SELECT id AS setid, type AS type FROM ruk_local_keyword_sets WHERE id = '$kw_set_id'";
    $res = $this->db->query($sql);
    $type = 'key';

    while ($row = $res->fetch_assoc()) {
      $type = $row['type'];
    }

    return $type;

  }

  // URL OR KEYWORD CAMPAIGN
  public function getCampaignName ($kw_set_id) {

    $sql = "SELECT
             id   AS setid,
             name AS name
            FROM
              ruk_project_keyword_sets
            WHERE
              id = '$kw_set_id'";

    $res = $this->db->query($sql);

    $name = '';

    while ($row = $res->fetch_assoc()) {
      $name = $row['name'];
    }

    return $name;

  }


  // URL OR KEYWORD CAMPAIGN
  public function getCampaignNameLocal ($kw_set_id) {

    $sql = "SELECT
             id   AS setid,
             name AS name
            FROM
              ruk_local_keyword_sets
            WHERE
              id = '$kw_set_id'";

    $res = $this->db->query($sql);

    $name = '';

    while ($row = $res->fetch_assoc()) {
      $name = $row['name'];
    }

    return $name;

  }

  public function getAllKeywords () {
    $sql = "SELECT DISTINCT keyword FROM ruk_project_keywords";
    $res = $this->db->query($sql);
    while ($row = $res->fetch_assoc()) {
      $data[] = $row['keyword'];
    }
    return $data;
  }

  // GET AMOUNT OF KEYWORDS USED BY N CUSTOMERS

  public function getAllKeywordsDaily () {

    $sql = "SELECT b.keyword AS keyword , a.measuring AS measuring FROM ruk_project_keyword_sets a LEFT JOIN ruk_project_keywords b ON b.id_kw_set = a.id WHERE a.type = 'key'";
    $result = $this->db->query($sql);
    $keywords1_weekly = array();
    $keywords1_daily  = array();

    while ($row = $result->fetch_assoc()) {

      if ($row['measuring'] == 'weekly') {
        $keywords1_weekly[$row['keyword']] = $row['keyword'];
      } else {
        $keywords1_daily[$row['keyword']] = $row['keyword'];
      }

    }

    // ANZAHL LOCAL KEYWORDS RUK MIT DUPL
    $sql = "SELECT id FROM ruk_local_keywords";
    $res1 = $this->db->query($sql);
    $local_kw_amount = $res1->num_rows;

    $amount =  count($keywords1_daily) + $local_kw_amount + count($keywords1_weekly) / 6;

    return $amount;

  }

  public function getAllKeywordsForCustomerVersions ($customers) {

    $sql = "SELECT
                   a.id             AS id,
                   a.country        AS country,
                   b.id_customer    AS id_customer,
                   b.rs_type        AS rs_type,
                   c.keyword        AS keyword
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
                LEFT JOIN ruk_project_keywords c
                  ON c.id_kw_set = b.id
            WHERE a.id IN ($customers)";

    $res = $this->db->query($sql);

    $project_keywords = array();
    while ($row = $res->fetch_assoc()) {
      if (!empty($row['keyword'])) {
        $project_keywords[$row['keyword'].$row['country'].$row['rs_type']] = $row['keyword'];
      }

    }

    return $project_keywords;

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  public function getWebmasterToolsData ($keywords)
  {

    // compare KEYWORDS TO CRAWL vs. SCRAPED KEYWORDS
    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = stripslashes($comma_separated_kws);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              query,
              impressions AS sum_imp,
              clicks AS sum_clicks,
              ctr,
              position_1
            FROM
              gwt_aggregation_data
            WHERE
              query IN ($comma_separated_kws)";

    $res = $this->db->query($sql);

    $data = array();
    while ($row = $res->fetch_assoc()) {
      $data[$row['query']] = array ($row['sum_imp'], $row['sum_clicks'], $row['ctr'], $row['position_1']);
    }

    return $data;

  }

  public function getWebmasterToolsDataHost ($hostname)
  {

    $sql = "SELECT id FROM gwt_hostnames WHERE hostname = '$hostname'";
    $res = $this->db->query($sql);

    $hostname_id = 0;
    while ($row = $res->fetch_assoc()) {
      $hostname_id = $row['id'];
    }

    $date = date('Y-m-d', strtotime('-7 days'));  

    $sql = "SELECT
              query,
              position_1,
              position_2
            FROM
              gwt_data
            WHERE
              hostname_id = $hostname_id
            AND 
             timestamp = '$date'";

    $res = $this->db->query($sql);

    $data = array();
    while ($row = $res->fetch_assoc()) {
      $data[$row['query']] = array ($row['position_1'], $row['position_2']);
    }

    return $data;

  }


  // SHITTY
  public function getAdwordsLoc ($country)
  {

    $language = $country;

    if (stripos($country, '-') !== FALSE) {
      $arr = explode ('-', $country);
      $country  = $arr[0];
      $language = $arr[1];
    }
    if ($country == 'be') {
      $language = 'fr';
    }
    if ($country == 'uk') {
      $language = 'en';
    }
    if ($country == 'at') {
      $language = 'de';
    }
    if ($country == 'ch') {
      $language = 'de';
    }

    return array ($country, $language);

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE I18N
  public function getAdwordsData ($keywords, $country = 'de', $lang = 'de')
  {

    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = stripslashes($comma_separated_kws);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $sql = "SELECT
              keyword,
              searchvolume,
              cpc,
              competition,
              opi
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              country = '$country'
            AND
              language = '$lang'
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $googleadwords = array();

    while ($row = $res->fetch_assoc()) {
     $googleadwords[$row['keyword']] = array ($row['searchvolume'], $row['cpc'], $row['competition'], $row['opi']);
    }

    return $googleadwords;

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  public function getAdwordsDataLocal ($keywords)
  {

    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $googleadwords = array();

    foreach ($keywords as $key => $set) {

      $keyword = $set[0];
      $location = $set[1];

      $sql = "SELECT
                keyword,
                rs_loc_id,
                searchvolume,
                cpc,
                competition,
                opi
              FROM
                gen_keywords_local
              WHERE
                keyword = '$keyword'
              AND
                rs_loc_id = '$location'";

      $res = $this->db->query($sql);

      while ($row = $res->fetch_assoc()) {
       $googleadwords[$row['keyword']][$row['rs_loc_id']] = array ($row['searchvolume'], $row['cpc'], $row['competition'], $row['opi']);
      }

    }

    return $googleadwords;

  }

  // FETCH GOOGLE ANALYTICS DATA
  public function getAnalyticsData ($hostname)
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

     $sql = "SELECT
              *
            FROM
              analytics_aggregation_hostnames a
            LEFT JOIN analytics_aggregation_data b
              ON b.id_hostname = a.id
            WHERE
              a.hostname = '$hostname'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      // remove http(s) .. some sites deliver http data via GA but use https
      $url = strtolower($this->removeHttps($row['url']));
      $ga_rows[$url] = $row;
    }

    return $ga_rows;

  }


  // GET ALL RS LOCATIONS
  public function getLocationsArray()
  {

    $sql = "SELECT * FROM ruk_local_locations";
    $result = $this->db->query($sql);
    $locations = array();

    while ($row = $result->fetch_assoc()) {

      if (preg_match('#[0-9]#',$row['town'])) {
        //continue;
      }

      $locations[$row['rs_id']] = $row['town'];

    }

    return $locations;

  }


  // LOAD CACHE FILE DAILY
  public function loadCacheFile ($customer_id, $days = '', $rsort = true) {

    $rows = $this->sql_cache->readFile('metrics_rankings'.$days.'_data_'.$customer_id.'_x.tmp');

    if (empty($rows)) {
      $rows = $this->sql_cache->readFile('metrics_rankings_data_'.$customer_id.'_weekly.tmp');
    }

    if ($rsort === true) {
      // SORT DATES -> NEWEST FIRST
      $rows = array_reverse($rows);
    }

    return $rows;

  }


  // LOAD CACHE FILE WEEKLY
  public function loadCacheFileWeekly ($customer_id, $rsort = true) {

    $rows = $this->sql_cache->readFile('metrics_rankings_data_'.$customer_id.'_weekly.tmp');

    if ($rsort === true) {
      // SORT DATES -> NEWEST FIRST
      $rows = array_reverse($rows);
    }

    return $rows;

  }


  // LOAD CACHE FILE LOCAL
  public function loadCacheFileLocal ($customer_id, $days = '', $rsort = true) {

    $rows = $this->sql_cache->readFile('local/metrics_local'.$days.'_data_'.$customer_id.'.tmp');

    if ($rsort === true) {
      // SORT DATES -> NEWEST FIRST
      $rows = array_reverse($rows);
    }

    return $rows;

  }

  // LOAD DEX DATA FROM CACHE / 7 DAYS
  public function fetchTopRankingsOverall () {

    $rows = $this->sql_cache->readFile('metrics_rankings_data_best_overall.tmp');
    return $rows;

  }

  // LOAD DEX DATA FROM CACHE / 7 DAYS
  public function fetchOneDexData () {

    $rows = $this->sql_cache->readFile('metrics_one_dex_all.tmp');
    return $rows;

  }

  // LOAD DEX DATA FROM CACHE / 31 DAYS
  public function fetchOneDexData31 () {

    $rows = $this->sql_cache->readFile('metrics_one_dex_all_31.tmp');
    return $rows;

  }

// LOAD CACHE FILE
  public function loadCacheFileIndexwatch ($days, $type) {

    $rows = $this->sql_cache->readFile('indexwatch_'.$type.'_'.$days.'_x.tmp');
    return $rows;

  }

  // LOAD CACHE FILE BEST RANKINGS
  public function loadCacheFileBest ($customer_id, $rsort = true) {

    $rows = $this->sql_cache->readFile('metrics_rankings_data_'.$customer_id.'_best.tmp');

    if ($rsort === true) {
      // SORT DATES -> NEWEST FIRST
      $rows = array_reverse($rows);
    }

    return $rows;

  }

  // switch daily to weekly, delete daily cache files
  public function deleteCacheFile ($project_id, $set_id) {

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->sql_cache->deleteFile ('metrics_rankings_data_'.$project_id.'_x.tmp');
    $this->sql_cache->deleteFile ('metrics_rankings_31_data_'.$project_id.'_x.tmp');
    $this->sql_cache->deleteFile ('metrics_rankings_data_competition_'.$project_id.'_x.tmp');
    $this->sql_cache->deleteFile ('metrics_rankings_31_data_competition_'.$project_id.'_x.tmp');
    $this->sql_cache->deleteFile ('wl/wl_'.$project_id.'_1.tmp');
    $this->sql_cache->deleteFile ('wl/wl_'.$project_id.'_6.tmp');
    $this->sql_cache->deleteFile ('charts/chart_'.$project_id.'_all.tmp');
    $this->sql_cache->deleteFile ('charts/chart_'.$project_id.'_all_31.tmp');
    $this->sql_cache->deleteFile ('charts/setdata_'.$project_id.'_all.tmp');
    $this->sql_cache->deleteFile ('charts/setdata_'.$project_id.'_all.tmp');

    if (!empty($set_id)) {
      $this->sql_cache->deleteFile ('charts/chart_'.$project_id.'_'.$set_id.'.tmp');
      $this->sql_cache->deleteFile ('charts/chart_'.$project_id.'_'.$set_id.'_31.tmp');
    }

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  public function nufo ($val, $decimals = 2) {

    $num = 0;

    $num = number_format($val, $decimals, ',', '.');

    return $num;

  }


  // DEXMAX
  public function calcOneDexMax ($opi) {

    if ($opi == 0) {
      $opi = 1;
    }

    $dex_max = $opi * 100 / 100000;

    return round($dex_max, 3, 0);

  }

  // DEX
  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 3, 0);

  }


  // SAVE CSV FILE
  public function csvBuilder ($array, $name = 'ruk')
  {

    $date = date_create();
    $filename = $name . '_' . date_timestamp_get($date) . '.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'a');

    foreach ($array as $value) {
      fputcsv($fp, $value);
    }

    fclose($fp);

    return $filename;

  }

  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }

  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }

  // YMD YESTERDAY
  public function getYesterdayYMD ($ts)
  {
    return date('Y-m-d', strtotime('' . $ts . ' -1 day'));
  }

  // YMD YESTERDAY
  public function getOneWeekAgoYMD ($ts)
  {
    return date('Y-m-d', strtotime('' . $ts . ' -7 day'));
  }

  // YMD ONE MONTH AGO FIRST
  public function getFirstDayPrevMonth ()
  {
    return date('Y-m-d', strtotime('first day of previous month'));
  }

  // CORE GERMAN TS
  public function germanTSred ($ts)
  {
    if ($ts == '0000-00-00') {
      $ts = '2015-01-01';
    }
    return date('d.m.Y', strtotime($ts));
  }

  // PROPER GERMAN TS
  public function germanTS ($ts)
  {

    $day[0] = 'Sonntag';
    $day[1] = 'Montag';
    $day[2] = 'Dienstag';
    $day[3] = 'Mittwoch';
    $day[4] = 'Donnerstag';
    $day[5] = 'Freitag';
    $day[6] = 'Samstag';

    $dayn = date('w', strtotime($ts));

    return $day[$dayn] . ', ' . date('d.m.Y', strtotime($ts));

  }

  // PROPER GERMAN TS
  public function germanWD ($ts)
  {

    $day[0] = 'Sonntag';
    $day[1] = 'Montag';
    $day[2] = 'Dienstag';
    $day[3] = 'Mittwoch';
    $day[4] = 'Donnerstag';
    $day[5] = 'Freitag';
    $day[6] = 'Samstag';

    $dayn = date('w', $ts);

    return $day[$dayn];

  }

  // PROPER GERMAN TS
  public function germanWDshort ($ts)
  {

    $day[0] = 'So';
    $day[1] = 'Mo';
    $day[2] = 'Di';
    $day[3] = 'Mi';
    $day[4] = 'Do';
    $day[5] = 'Fr';
    $day[6] = 'Sa';

    $dayn = date('w', $ts);

    return $day[$dayn];

  }

  public function isWeekend($date) {
    if (date('N', strtotime($date)) >= 6) {
      return true;
    }
    return false;
  }

  public function end ($date) {
    $weekDay = date('w', strtotime($date));
    if ($weekDay == 0 || $weekDay == 6) {
      return true;
    }
    return false;
  }

  // GETHOSTNAME
  public function getHostname ($id)
  {

    $sql = "SELECT * from ruk_project_customers WHERE id = $id";

    $result   = $this->db->query($sql);
    $project  = array();

    while ($row = $result->fetch_assoc()) {
      $project = $row;
    }

   return $project;

  }

  // IGNORE SUBDOMAIN
  public function getIgnoreSubdomain ($id)
  {

    $sql = "SELECT ignore_subdomain from ruk_project_customers WHERE id = $id";

    $result   = $this->db->query($sql);
    $project  = array();

    while ($row = $result->fetch_assoc()) {
      $ignore_subdomain = $row['ignore_subdomain'];
    }

   return $ignore_subdomain;

  }


  // HOSTNAME
  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  // SUBDOMAIN -> HOSTNAME
  public function pureHostNameNoSubdomain ($host)
  {

    $domain = $host;

    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) && $this->ignore_subd == 1) {
      return $regs['domain'];
    } else {
      return $host;
    }

  }

  // GOOGLE SEARCH LINK
  public function googleSearchUrl ()
  {
    return 'https://www.google.de/search?oe=utf-8&pws=0&complete=0&hl=de&num=100&q=';
  }

  public function googleSearchDomain ($country)
  {

    if ($country == 'be-fr') {
      return 'https://www.google.be/search?oe=utf-8&pws=0&complete=0&hl=fr&num=100';
    } else if ($country == 'be-nl') {
      return 'https://www.google.be/search?oe=utf-8&pws=0&complete=0&hl=nl&num=100';
    } else if ($country == 'ch-fr') {
      return 'https://www.google.ch/search?oe=utf-8&pws=0&complete=0&hl=fr&num=100';
    } else if ($country == 'ch-it') {
      return 'https://www.google.ch/search?oe=utf-8&pws=0&complete=0&hl=it&num=100';
    } else if ($country == 'ca-fr') {
      return 'https://www.google.ca/search?oe=utf-8&pws=0&complete=0&hl=fr&num=100';
    } else {
      return 'https://www.google.'.$country.'/search?oe=utf-8&pws=0&complete=0&num=100';
    }

  }


  public function removeHttps ($str) {
    $str = preg_replace('#^https?://#', '', $str);
    return $str;
  }

  public function randomColor () {

    return 'rgb('.rand(50,220).', '.rand(50,220).', '.rand(50,220).')';

  }

  public function rsTypeLocal ($type) {

    if ($type == 'desktop_de') {
      $name = 'Google Desktop Deutschland';
    } else if ($type == 'mobile_de') {
      $name = 'Google Mobile Deutschland';
    } else if  ($type == 'maps_de') {
      $name = 'Google Maps Deutschland';

    } else if  ($type == 'desktop_at') {
      $name = 'Google Desktop Österreich';
    } else if ($type == 'mobile_at') {
      $name = 'Google Mobile Österreich';
    } else if  ($type == 'maps_at') {
      $name = 'Google Maps Österreich';

    } else if  ($type == 'desktop_ch') {
      $name = 'Google Desktop Schweiz';
    } else if ($type == 'mobile_ch') {
      $name = 'Google Mobile Schweiz';
    } else if ($type == 'maps_ch') {
      $name = 'Google Maps Schweiz';
    }

    return $name;

  }

  public function parseCompetitionUrl($competition) {

    $unparsed_competition  = strip_tags($competition);
    $unparsed_competition  = explode("\n", $unparsed_competition);

    $parsed_competition = array();
    foreach ($unparsed_competition as $url) {
      $url = strtolower(trim($url));
      if (filter_var($url, FILTER_VALIDATE_URL)) {
        $parsed_competition[] = $url;
      }
    }

    // 8 max
    if (count($parsed_competition) > 8) {
      $parsed_competition = array_slice($parsed_competition,0,8);
    }

    $parsed_competition = serialize($parsed_competition);

    return $parsed_competition;

  }


  public function parseBrandKeywords($keywords) {

    $unparsed  = strip_tags($keywords);
    $unparsed  = explode("\n", $unparsed);

    $parsed = array();
    foreach ($unparsed as $kw) {
      if (empty($kw)) {continue;}
      $kw = strtolower(trim($kw));
      $parsed[] = $kw;
    }

    $parsed = serialize($parsed);

    return $parsed;

}

  public function AnalyticsAuth ()
  {

    $ga = new GoogleAnalyticsAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/analytics_v3/api/key.p12');

    return $ga;

  }


  public function SearchConsoleAuth ()
  {

    $ga = new GoogleSearchConsoleAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }


  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }

  // MYSQL CLOSER
  public function mySqlClose ()
  {

    $this->db->close();

  }


}

?>
