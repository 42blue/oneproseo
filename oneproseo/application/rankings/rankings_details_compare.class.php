<?php

require_once('ruk.class.php');

/*

Erzeugt die Datum Vergleichsansicht der Keywords und rankenden URLs

*/


class rankings_details_compare extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['setid'])) {
      $this->showResult();
    } else {
      $this->showInput();
    }

   echo $this->out;

  }

  private function showInput () {

    $earlydate     = date('d.m.Y', strtotime('-364 days'));
    $yesterday     = date('d.m.Y', strtotime('-1 days'));
    $startdate_out = $earlydate;

    $datepicker = 'true';
    if (isset($_GET['setid'])) {
      $setdata   = parent::getKeywordSetData($_GET['setid']);
      if (!empty($setdata['created_on'])) {
        $earlydate = date('d.m.Y', strtotime($setdata['created_on']));
      }
      if ($setdata['measuring'] == 'weekly') {
        $datepicker = 'false';
      }
    }

    if (isset($_GET['startdate'])) {
      $startdate_out = $_GET['startdate'];
    }

    $this->out = '<p><b>Bitte beachten:</b><br />Handelt es sich bei der Kampagne um eine wöchentliche Kampagne, kann nur ein Montag zum Vergleich ausgewählt werden.<br /><br /></p>

      <form class="form-horizontal fill-up" role="form" id="ops_date_compare">
        <div class="form-group">
          <label for="name" class="col-sm-2 label-1 control-label">Datum 1:</label>
          <div class="col-sm-8">
            <input class="datepicker_ruk fill-up" required="required" type="text" name="date1" placeholder="Frühestes Datum: '.$earlydate.'" value="' . $startdate_out .'">
          </div>
        </div>
        <div class="form-group">
          <label for="name" class="col-sm-2 label-1 control-label">Datum 2:</label>
          <div class="col-sm-8">
            <input class="datepicker_ruk fill-up" required="required" type="text" name="date2" placeholder="Spätestes Datum: ' .  $yesterday .'" value="' .  $yesterday .'">
          </div>
        </div>

        <div class="form-group">
          <label for="name" class="col-sm-2 label-1 control-label">Art:</label>
          <div class="col-sm-8">
            <div class="crawl-input-radio">
              <input type="radio" name="distribution" value="0" class="icheck" id="iradio3" checked="checked">
              <label for="iradio3">Detailrankings anzeigen</label>
            </div>
            <div class="crawl-input-radio">
              <input type="radio" name="distribution" value="1" class="icheck" id="iradio4">
              <label for="iradio4">Rankingverteilung anzeigen</label>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-2 control-label"></div>
          <div class="col-sm-8">
            <button type="submit" class="btn btn-primary btn-lg" id="ops_new_project_submit">vergleichen</button>
          </div>
        </div>
      </form>
      <script>
        $(".datepicker_ruk").datepicker({
          format: "dd.mm.yyyy",
          autoclose: true,
          startDate: "'.$earlydate.'",
          endDate: "+0d",
          beforeShowDay: function(dx) {
            var day = dx.getDay();
            if (day != 1) {
              return '.$datepicker.';
            } else {
              return true;
            }
          }
        });
      </script>';

  }


  private function showResult () {

    $this->keyword_setid = $_POST['setid'];
    $this->project_id    = $_POST['projectid'];

    $this->date_1 = $this->formatDate($_POST['date1']);
    $this->date_2 = $this->formatDate($_POST['date2']);

    $this->distribution = $_POST['distribution'];

    $this->campaign_type = $this->checkCampainType($this->keyword_setid);

    // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }
    if ($this->keyword_setid == 'all' || $this->campaign_type == 'url') {
      // all keyword sets of a project
      $this->project_id = $_POST['projectid'];
      $this->selectAllKeywordSets();
    } else {
      // only one specific keyword set
      $this->selectKeywordSet();
    }

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

    $this->getRankingData();

    if ($this->resut_rows == 0) {
      $this->checkForRankingsHint();
    } else if ($this->distribution == 1){
      $this->renderRankingViewDistribution();
    } else {
      $this->renderRankingViewKeyword();
    }

  }

  private function selectKeywordSet ()
  {

    $selected_keyword_set = $this->keyword_setid;

    $sql = "SELECT a.id               AS setid,
                   a.name             AS setname,
                   a.rs_type          AS rs_type,
                   b.keyword          AS keyword,
                   b.id               AS kwid,
                   b.tags             AS tags,                   
                   c.url              AS url,
                   c.name             AS customername,
                   c.country          AS country,
                   c.ignore_subdomain AS ignore_subdomain
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[]            = $row['keyword'];
      $url                         = $row['url'];
      $this->country               = $row['country'];
      $this->setname               = $row['setname'];
      $this->customername          = $row['customername'];
      $this->mo_type               = $row['rs_type'];
      $this->ignore_subd           = $row['ignore_subdomain'];
      $this->tags[$row['keyword']] = array($row['kwid'], $row['tags']);
    }

    $loc = $this->getAdwordsLoc($this->country);
    $this->adwords_country  = $loc[0];
    $this->adwords_language = $loc[1];

    $this->keyword_amount = count($this->keywords);
    $this->hostname = parent::pureHostName($url);

  }


  private function selectAllKeywordSets ()
  {

    $selected_project  = $this->project_id;

    $sql = "SELECT a.id               AS setid,
                   a.name             AS setname,
                   a.rs_type          AS rs_type,
                   b.keyword          AS keyword,
                   b.id               AS kwid,
                   b.tags             AS tags,
                   c.url              AS url,
                   c.name             AS customername,
                   c.country          AS country,
                   c.ignore_subdomain AS ignore_subdomain
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (!empty($row['keyword'])) {
        $this->keywords[]            = $row['keyword'];
        $this->tags[$row['keyword']] = array($row['kwid'], $row['tags']);
        $url                         = $row['url'];
      }
      $this->country      = $row['country'];
      $this->customername = $row['customername'];
      $setname            = $row['setname'];
      $this->mo_type      = 'desktop';
      $this->ignore_subd  = $row['ignore_subdomain'];
    }

    //$this->keywords = $this->getAllKeywords();

    if ($this->campaign_type == 'url') {

      $selected_keyword_set = $this->keyword_setid;

      $sql = "SELECT name FROM ruk_project_keyword_sets WHERE id = '".$selected_keyword_set."'";

      $result = $this->db->query($sql);

      while ($row = $result->fetch_assoc()) {
        $this->setname = $row['name'];
      }

    } else {
      $this->setname = 'Alle Keywords';
    }

    $loc = $this->getAdwordsLoc($this->country);
    $this->adwords_country  = $loc[0];
    $this->adwords_language = $loc[1];

    $this->keyword_amount = count($this->keywords);
    $this->hostname = parent::pureHostName($url);

  }


  private function getRankingData()
  {

    $hostname = $this->hostname;

    // compare KEYWORDS TO CRAWL vs. SCRAPED KEYWORDS
    $comma_separated_kws = implode('", "', $this->keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $rank_add = 0;

    $this->googleadwords = parent::getAdwordsData($this->keywords, $this->adwords_country, $this->adwords_language);

    $sql = "SELECT
              id,
              keyword,
              timestamp
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$this->mo_type'
            AND
              (timestamp = '$this->date_1' OR timestamp = '$this->date_2')
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $this->resut_rows = $result->num_rows;

    while ($row = $result->fetch_assoc()) {
      $rows[] = $row;
    }

		if (empty($rows)) {
    	return;
    }

    $nrows = array();

    foreach ($rows as $k => $v) {

      $idkw = $v['id'];

      $sql = "SELECT
                position,
                url,
                id_kw,
                hostname
              FROM
                ruk_scrape_rankings
              WHERE
                id_kw = '$idkw'
              ";

      $result2 = $this->db->query($sql);

      while ($row = $result2->fetch_assoc()) {

        if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
          $nrows[] = array('k' => $v['keyword'], 't' => $v['timestamp'], 'p' => $row['position'], 'u' => $row['url']);
        }

      }

    }

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (isset($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
          unset($this->url_campaign[$key]);
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }
// COMPARE OPTIONS

    foreach ($nrows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {
        $this->rankset['_ts'][$row['t']] = $row['t'];
        $this->rankset['_da'][$row['k']][$row['t']][] = array ('rank' => $row['p'], 'ts' => $row['t'], 'url' => $row['u']);
      }

    }

    $this->checkForRankingsHint();

  }


  private function renderRankingViewDistribution () {


		$distribution = array();

    foreach ($this->rankset['_ts'] as $k => $ts) {

			$distribution[$ts]['rank1'] = 0;
			$distribution[$ts]['rank2'] = 0;
			$distribution[$ts]['rank3'] = 0;
			$distribution[$ts]['rank4'] = 0;
			$distribution[$ts]['rank5'] = 0;
			$distribution[$ts]['rank6'] = 0;
			$distribution[$ts]['top10'] = 0;
			$distribution[$ts]['count'] = 0;
			$distribution[$ts]['sum']   = 0;

      foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      	if (!isset($timestamp[$ts])) {
      		continue;
      	}

				if ($timestamp[$ts][0]['rank'] == 1) {
				  $distribution[$ts]['rank1']++;
				}
				if ($timestamp[$ts][0]['rank'] == 2) {
				  $distribution[$ts]['rank2']++;
				}
				if ($timestamp[$ts][0]['rank'] == 3) {
				  $distribution[$ts]['rank3']++;
				}
				if ($timestamp[$ts][0]['rank'] == 4) {
				  $distribution[$ts]['rank4']++;
				}
				if ($timestamp[$ts][0]['rank'] == 5) {
				  $distribution[$ts]['rank5']++;
				}
				if ($timestamp[$ts][0]['rank'] > 5 && $timestamp[$ts][0]['rank'] <= 10) {
				  $distribution[$ts]['rank6']++;
				}
				if ($timestamp[$ts][0]['rank'] <= 10) {
				  $distribution[$ts]['top10']++;
				}
				$distribution[$ts]['count']++;
				$distribution[$ts]['sum'] = $distribution[$ts]['sum'] + $timestamp[$ts][0]['rank'];


      }

    }


    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div>
                  <span class="titleleft">Ansicht: Keywords / Ranking</span>
                <ul class="box-toolbar">
                  <li><a target="_blank" href="../../detail-keyword/'.$this->keyword_setid.'"><span class="label label-dark-blue">Keywords / Rankings</span></a></li>
                  <li><a target="_blank" href="../../detail-url/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (Campaign)</span></a></li>
                  <li><a target="_blank" href="../../detail-url-complete/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (DB)</span></a></li>
                  <li><a target="_blank" href="../../detail-url-onedex/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (Campaign)</span></a></li>
                  <li><a target="_blank" href="../../detail-url-onedex-complete/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (DB)</span></a></li>
                  <li><a target="_blank" href="../../detail-keyword-url/'.$this->keyword_setid.'"><span class="label label-green">Keywords / URL</span></a></li>
                  <li><a target="_blank" href="../../detail-compare/'.$this->keyword_setid.'/'.$_POST['date1'].'"><span class="label label-cyan">Datumsvergleich</span></a></li>
                  <li><a target="_blank" href="../../detail-competition/'.$this->keyword_setid.'"><span class="label label-purple">Wettbewerbsvergleich</span></a></li>
                </ul>
                </div>
                <div class="box-content padded">
                  <p>Diese Ansicht zeigt die Rankingverteilung zweier frei definierbarer Tage.</p>
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                 <span class="title">Rankingverteilung für '.$this->setname.'</span>
                   <ul class="box-toolbar">
                     <li><a href=""><span class="label label-black">Neuer Keyword Datumsvergleich</span></a></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal">';

    $outheadtable = '<thead><tr><td>Rankings am </td>';
		$tsi = array();
		$i = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {
    	$i++;
		  $outheadtable .= '<td>'.parent::germanTS($ts).'</td>';
		  if ($i <= 1) {
			  $outheadtable .= '<td>Change</td>';
		  }
		  $tsi[] = $ts;
    }

    $outheadtable .= '</tr></thead>';

		$f = round($distribution[$tsi[0]]['sum'] / $distribution[$tsi[0]]['count']);	
		$s = round($distribution[$tsi[1]]['sum'] / $distribution[$tsi[1]]['count']);

		$outtable = '<tbody>';
		$outtable .= '
									<tr>
										<td>Platz 1</td><td>' . $distribution[$tsi[0]]['rank1'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['rank1'], $distribution[$tsi[1]]['rank1']).'</td>
										<td>' . $distribution[$tsi[1]]['rank1'] . '</td>
									</tr>
									<tr>
										<td>Platz 2</td><td>' . $distribution[$tsi[0]]['rank2'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['rank2'], $distribution[$tsi[1]]['rank2']).'</td>
										<td>' . $distribution[$tsi[1]]['rank2'] . '</td>
									</tr>
									<tr>
										<td>Platz 3</td><td>' . $distribution[$tsi[0]]['rank3'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['rank3'], $distribution[$tsi[1]]['rank3']).'</td>
										<td>' . $distribution[$tsi[1]]['rank3'] . '</td>
									</tr>
									<tr>
										<td>Platz 4</td><td>' . $distribution[$tsi[0]]['rank4'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['rank4'], $distribution[$tsi[1]]['rank4']).'</td>
										<td>' . $distribution[$tsi[1]]['rank4'] . '</td>
									</tr>
									<tr>
										<td>Platz 5</td><td>' . $distribution[$tsi[0]]['rank5'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['rank5'], $distribution[$tsi[1]]['rank5']).'</td>
										<td>' . $distribution[$tsi[1]]['rank5'] . '</td>
									</tr>
									<tr>
										<td>Platz 6-10</td><td>' . $distribution[$tsi[0]]['rank6'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['rank6'], $distribution[$tsi[1]]['rank6']).'</td>
										<td>' . $distribution[$tsi[1]]['rank6'] . '</td>
									</tr>
									<tr>
										<td>Summe - Top 10</td><td>' . $distribution[$tsi[0]]['top10'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['top10'], $distribution[$tsi[1]]['top10']).'</td>
										<td>' . $distribution[$tsi[1]]['top10'] . '</td>
									</tr>
									<tr>
										<td>Summe - Top 100</td><td>' . $distribution[$tsi[0]]['count'] . '</td>
										<td>'.parent::checkDifferenceBetweenRankingDays($distribution[$tsi[0]]['count'], $distribution[$tsi[1]]['count']).'</td>
										<td>' . $distribution[$tsi[1]]['count'] . '</td>
									</tr>
									<tr>
										<td>Ø - Rankings</td><td>' . $f . '</td>
										<td>' . parent::checkDifferenceBetweenRankingDaysReverse($f, $s).'</td>
										<td>' . $s . '</td>
									</tr>
									';

    echo $outhead . $outheadtable . $outtable;

  }


  private function renderRankingViewKeyword ()
  {

    $outhead      = '';
    $outheadtable = '';
    $out          = '';
    $dex_sum      = array();

    $this->csvarr['head'] = array('#', 'Keyword', 'OPI');

    // merge ranking result with keywordset for keywords that do not rank
    if (!isset($this->url_campaign)) {
      foreach ($this->keywords as $keyword) {
        if (!isset($this->rankset['_da'][$keyword])) {
           $this->rankset['_da'][$keyword] = '';
        }
      }
    }

    ksort($this->rankset['_da']);

    $ii = 0;

    // TABLE HEADER
    $show_all = false;
    $outheadtable .= '<thead><tr><td style="width:50px;">#</td><td>Keyword</td><td>Tags</td><td>OPI</td>';
    foreach ($this->rankset['_ts'] as $k => $ts) {

      $ii++;

      $outheadtable .= '<td class="nosearch">OneDex - '. parent::germanTS($ts) .'</td>';
      $outheadtable .= '<td class="nosearch">Rank - '. parent::germanTS($ts) .'</td>';
      $outheadtable .= '<td class="nosearch">URL - '. parent::germanTS($ts) .'</td>';

      array_push($this->csvarr['head'], 'OneDex');
      array_push($this->csvarr['head'], $ts);
      array_push($this->csvarr['head'], 'URL');

      if ($ii < count($this->rankset['_ts'])) {
        $show_all = true;
        $outheadtable .= '<td style="width:50px;">Change Rank</td><td style="width:50px;">Change OneDex</td>';
        array_push($this->csvarr['head'], 'Change Rank', 'Change OneDex');
      }
    }

    // TABLE CONTENT
    $i = 0;
    $first_date = array_values($this->rankset['_ts']);
    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank"><a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">' . $keyword . '</a><small target="_blank" class="rankings"><a target="_blank" href="https://next.sistrix.de/seo/ranking_history/keyword/' . $keyword . '/domain/'.$this->hostname.'">SISTRIX</a></small></td>';

      $out .= '<td><textarea data-kwid="'.$this->tags[$keyword][0].'" class="tags nostyle" placeholder="...">' . rtrim($this->tags[$keyword][1], ',') . '</textarea></td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
      } else {
        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw tdright"> ' . $this->nufo($this->googleadwords[$keyword][3])  . ' </td>';
      }

      $this->csvarr[$i] = array($i, $keyword, $this->nufo($this->googleadwords[$keyword][3]));

      // RANKING SETS
      $ii = 0;

      foreach ($this->rankset['_ts'] as $k => $ts) {

        $ii++;

        // NO RANKING FIRST DATE
        if (!isset($timestamp[$ts])) {

          $out .= '<td data-order="0"></td>';
          $out .= '<td data-order="0"></td>';
          $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->mo_type.'">TOP100</a></small></td>';

          // CHANGE
          if ($ii < count($this->rankset['_ts'])) {

            $yesterday = date('Y-m-d',(strtotime($first_date[k+1])));
            if (isset($this->rankset['_da'][$keyword][$yesterday][0]['rank'])) {
              $compare = $this->rankset['_da'][$keyword][$yesterday][0]['rank'];
              $change_dex = '<i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-100%</small>';
              $change_dex_csv = '+100%';
            } else {
              $compare = NULL;
              $change_dex = '<i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+0%</small>';
              $change_dex_csv = '0%';
            }
            $change = $this->checkDifferenceBetweenRankings(NULL, $compare);
            $change_csv = $this->checkDifferenceBetweenRankings(NULL, $compare, false);

            $out .= '<td>'.$change.'</td>';
            $out .= '<td>'.$change_dex.'</td>';

            array_push($this->csvarr[$i], '-', '-', '-', $change_csv, $change_dex_csv);
          } else {
            array_push($this->csvarr[$i], '-', '-', '-');
          }

        } else {

          // ONE DEX FETCH DATA FOR CALC
            if (isset($timestamp[$ts][0]['rank'])) {
              $rank = $timestamp[$ts][0]['rank'];
            } else {
              $rank = 101;
            }
            $opi = $this->googleadwords[$keyword][3];
            $dex = $this->calcOneDex($rank, $opi);

            if (isset($dex_sum[$ts])) {
              $dex_sum[$ts] = $dex_sum[$ts] + $dex;
            } else {
              $dex_sum[$ts] = $dex;
            }

            $dex_format = $this->nufo($dex);
          // ONE DEX FETCH DATA FOR CALC

            $out .= '<td data-order="'.$dex.'" class="gadw tdright"> ' . $dex_format . ' </td>';
            array_push($this->csvarr[$i], $dex_format);

           // check for multiple rankings
          if (count($timestamp[$ts]) > 1) {

            $out .= '<td class="rank"';
            $k = 0;
            $matched_url = array();

            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_url[$value['url']])) {
                continue;
              }
              $matched_url[$value['url']] = $value['url'];

              $yesterday = date('Y-m-d',(strtotime($first_date[k+1])));

              if ($k == 0) {
                $csv = $value['rank'] . "\t";
                $csv_url = $value['url'];
                $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                $out_url = '<a class="rankings" target="_blank" href="'.$value['url'].'">'.$value['url'].'</a>';
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][0]['rank']);
                $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][0]['rank'], false);

              if (isset ($this->rankset['_da'][$keyword][$yesterday][0]['rank'])) {
                $rank_2 = $this->rankset['_da'][$keyword][$yesterday][0]['rank'];
              } else {
                $rank_2 = 101;
              }

              $change_dex = $this->calcPercentDex($this->calcOneDex($value['rank'], $opi), $this->calcOneDex($rank_2, $opi));
              $change_dex_csv = $this->calcPercentDex($this->calcOneDex($value['rank'], $opi), $this->calcOneDex($rank_2, $opi), 2, false);

              } else {
                $csv .= ' / ' . $value['rank'];
                $csv_url .= ' / ' . $value['url'];
                $out .= ' / <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                $out_url .= ' <br /> <a class="rankings" target="_blank" href="'.$value['url'].'">'.$value['url'].'</a>';
              }
              $k++;
            }

            array_push($this->csvarr[$i], $csv);
            array_push($this->csvarr[$i], $csv_url);


            $out .= '<small class="rankings"><a target="_blank" href="../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small>';
            $out .= '</td>';
            $out .= '<td>'.$out_url.'</td>';

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change.'</td>';
              $out .= '<td class="nowrap">'.$change_dex.'</td>';
              array_push($this->csvarr[$i], $change_csv);
              array_push($this->csvarr[$i], $change_dex_csv);
            }

          } else {

            $k = 0;
            foreach ($timestamp[$ts] as $value) {

              $yesterday = date('Y-m-d',(strtotime($first_date[k+1])));

              $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small target="_blank" class="rankings"><a href="../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->mo_type.'">TOP100</a></small></td>';
              $out .= '<td><a class="rankings" target="_blank" href="'.$value['url'].'">'.$value['url'].'</a></td>';
              $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][0]['rank']);
              $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][0]['rank'], false);

              if (isset ($this->rankset['_da'][$keyword][$yesterday][0]['rank'])) {
                $rank_2 = $this->rankset['_da'][$keyword][$yesterday][0]['rank'];
              } else {
                $rank_2 = 101;
              }

              $change_dex = $this->calcPercentDex($this->calcOneDex($value['rank'], $opi), $this->calcOneDex($rank_2, $opi));
              $change_dex_csv = $this->calcPercentDex($this->calcOneDex($value['rank'], $opi), $this->calcOneDex($rank_2, $opi), 2, false);

              array_push($this->csvarr[$i], $value['rank'] . "\t");
              array_push($this->csvarr[$i], $value['url']);

              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'.$change .'</td>';
                $out .= '<td class="nowrap">'.$change_dex.'</td>';
                array_push($this->csvarr[$i], $change_csv);
                array_push($this->csvarr[$i], $change_dex_csv);
              }

              $k++;

            }

          }

        }

      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    // OUTPUT
    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords');

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <div class="titleright">Projekt: '.$this->customername.' / Campaign: '.$this->setname.'</div>
                  <span class="titleleft">Ansicht: Keywords / Ranking</span>
                <ul class="box-toolbar">
                  <li><a target="_blank" href="../../detail-keyword/'.$this->keyword_setid.'"><span class="label label-dark-blue">Keywords / Rankings</span></a></li>
                  <li><a target="_blank" href="../../detail-url/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (Campaign)</span></a></li>
                  <li><a target="_blank" href="../../detail-url-complete/'.$this->keyword_setid.'"><span class="label label-red">URL / Keywords (DB)</span></a></li>
                  <li><a target="_blank" href="../../detail-url-onedex/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (Campaign)</span></a></li>
                  <li><a target="_blank" href="../../detail-url-onedex-complete/'.$this->keyword_setid.'"><span class="label label-blue">URL / OneDex (DB)</span></a></li>
                  <li><a target="_blank" href="../../detail-keyword-url/'.$this->keyword_setid.'"><span class="label label-green">Keywords / URL</span></a></li>
                  <li><a target="_blank" href="../../detail-compare/'.$this->keyword_setid.'/'.$_POST['date1'].'"><span class="label label-cyan">Datumsvergleich</span></a></li>
                  <li><a target="_blank" href="../../detail-competition/'.$this->keyword_setid.'"><span class="label label-purple">Wettbewerbsvergleich</span></a></li>
                </ul>
                </div>
                <div class="box-content padded">
                  <p>Diese Ansicht vergleicht die Rankings zweier frei definierbarer Tage.</p>
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                 <span class="title">Datumsvergleich der Rankings für '.$this->setname.'</span>
                   <ul class="box-toolbar">
                     <li><a href=""><span class="label label-black">Neuer Keyword Datumsvergleich</span></a></li>
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="' . $this->env_data['csvstore'] . $csv_filename . '"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">';


    // SUMME ONE DEX
    $out_table_intercept = '<tr>
     <td class="nowrap cumm" colspan="4"> SUM (OneDex)</td>
     <td class="nowrap cumm tdright">'.$this->nufo($dex_sum[$first_date[0]]).'</td>
     <td class="nowrap cumm" colspan="2"></td>';

    if ($show_all == true) {

      if ($dex_sum[$first_date[0]] > $dex_sum[$yesterday]) {
        $diff = $dex_sum[$first_date[0]] - $dex_sum[$yesterday];
        $sumdex = $diff * 100 / $dex_sum[$yesterday];
        $prefix = '+';
      } else if ($dex_sum[$first_date[0]] < $dex_sum[$yesterday]) {
        $diff = $dex_sum[$yesterday] - $dex_sum[$first_date[0]];
        $sumdex = $diff * 100 / $dex_sum[$yesterday];
        $prefix = '-';
      } else {
        $sumdex = 0;
        $prefix = '+';
      }

      $out_table_intercept .= '<td class="nowrap cumm"></td>
      <td class="nowrap cumm">' . $prefix . $this->nufo($sumdex) . '%</td>
      <td class="nowrap cumm tdright">' . $this->nufo($dex_sum[$yesterday]) . '</td>
      <td class="nowrap cumm" colspan="3"></td>';

    }

    $out_table_intercept .= '</tr>';

    $outheadtable .= $out_table_intercept;
    $outheadtable .= '</tr></thead><tbody>';

    $this->out = $outhead . $outheadtable . $out;

  }



  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {
      echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><strong>Für diesen Zeitraum liegen keine Rankings vor!<br/>
      Wenn Sie die Rankings auf wöchentlicher Basis überwachen wählen Sie bitte einen Montag.</strong><br /><br /><a class="white" href="javascript:window.location.reload(true)">zurück</a></div></div></div>';
      exit;
    }

  }


  private function formatDate($date) {

    $date = new DateTime($date);
    return $date->format('Y-m-d');

  }

}

?>
