<?php

ini_set('auto_detect_line_endings', true);

require_once('ruk.class.php');

class local_keywords extends ruk
{

  private $mo_name = '';
  private $mo_loca = array();
  private $mo_kewo = array();
  private $limit   = FALSE;

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    $this->project_id = $_POST['projectid'];
    $this->getOverallKeywordAmount();

    $this->desktop_rs_type = 'desktop_' . $this->country;
    $this->maps_rs_type    = 'maps_' . $this->country;
    $this->mobile_rs_type  = 'mobile_' . $this->country;

    // MODIFY
    if (isset($_POST['projectid']) && isset($_POST['setid']) && $_POST['setid'] > 0) {
      $this->set_id     = $_POST['setid'];
      $this->getSetData();
    }

    // SHOW TEXT INPUT
    if (isset($_POST['projectid']) && $_POST['kind'] == 'text') {
      if (!isset($this->set_id )) {
        $this->checkForLimit();
      }
      $this->loc = '';
      $this->getLocations();
      $this->renderView();
    }

    // SHOW CSV INPUT
    if (isset($_POST['projectid']) && $_POST['kind'] == 'csv') {
      $this->checkForLimit();
      $this->renderViewCsv();
    }

    //ADD TEXT
    if (isset($_POST['projectid']) && $_POST['action'] == 'add') {
      $this->set_id     = $_POST['setid'];
      $this->rankingspy = new rankingspy();

      //ADD CSV
      if (!empty($_FILES)) {
        $this->parseUpload();
      }

      if ($this->limit === TRUE) {

        $c_kw   = count (array_filter(explode("\n", $_POST['keywords'])));
        $c_town = count ($_POST['town']);
        $amount_new = $c_kw * $c_town;
        $amount_old = count($this->mo_kewo) * count($this->mo_loca);

        if ($amount_new >= $amount_old) {
          echo json_encode(array('error' => '<strong>Sie können keine neuen Keywords anlegen.<br />Die maximale Anzahl von '.$this->max_used_keywords.' Keywordüberwachungen ist erreicht.<br/>Sie können erst speichern wenn Sie die Anzahl der Überwachungen reduzieren. <br/> Entfernen Sie hierzu Keywords oder Orte.'));
          exit;
        }

      }

      $this->removeKeywordSet();
      $this->addNewKeywordSet();
      echo json_encode(array('msg' => 'Daten gespeichert.'));
    }

    // DELETE
    if (isset($_POST['id']) && $_POST['action'] == 'delete') {
      $this->set_id     = $_POST['id'];
      $this->rankingspy = new rankingspy();
      $this->removeKeywordSet();
    }

  }


  private function parseUpload() {

    $tmpName  = $_FILES['csv']['tmp_name'];
    $uploaded = file_get_contents($tmpName);

    if (mb_detect_encoding($uploaded,'UTF-8',true) == 'UTF-8') {
      $uploaded = $uploaded;
    } else {
      $uploaded = iconv("windows-1256","utf-8", $uploaded);
    }

    foreach( explode( "\r", $uploaded ) as $line ) {
      if (!empty($line)) {
        $rows[] = explode(';', $line);
      }
    }

   foreach ($rows as $key => $data) {

     if (empty($data[1]) || empty($data[0])) {
      continue;
     }

     // LOOSE STRING SEARCH
     if (!is_numeric($data[1])) {
       $csv_town = $data[1];
       $data[1] = $this->searchLocation($csv_town);
     }

     if (isset($town[$data[1]])) {
       array_push($town[$data[1]], $data[0]);
     } else {
       $town[$data[1]] = array ($data[0]);
     }

   }

   if (empty($town) || count($town) < 1) {
     echo json_encode(array('error' => 'Dies ist kein CSV File.'));
     exit;
   }

   $this->csv_data = $town;
 }


  private function getSetData() {

    $sql = "SELECT
              a.id            AS aid,
              a.name          AS name,
              a.rs_type       AS type,
              a.customer_view AS customer_view,
              b.id_kw_set     AS bid,
              b.location      AS location,
              c.id_kw_set     AS cid,
              c.keyword       AS keyword
            FROM ruk_local_keyword_sets a
              LEFT JOIN ruk_local_sets2rs b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_local_keywords c
                ON c.id_kw_set = a.id
            WHERE a.id = '".$this->set_id."'";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><p><strong>Hinweis:</strong></p><p>Dieses Keyword Set ist nicht vorhanden!</p></div></div></div>';
      exit;
    }

    while ($row = $result->fetch_assoc()) {
      $this->mo_name                   = $row['name'];
      $this->mo_type                   = $row['type'];
      $this->mo_cuvi                   = $row['customer_view'];
      $this->mo_loca[$row['location']] = $row['location'];
      $this->mo_kewo[$row['keyword']]  = $row['keyword'];
    }

  }


  private function renderView() {
    $out = '
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Neue Local Campaign hinzufügen</span>
            </div>
            <div class="box-content padded">

              <form class="form-horizontal fill-up" role="form" id="ops_add_keywordset">
                <input type="hidden" name="action" value="add">
                <input type="hidden" name="projectid" value="'.$this->project_id.'">
                ' . (!empty($this->mo_name) ? '<input type="hidden" name="setid" value="'.$this->set_id.'">' : '') .'
                <div class="form-group">
                  <label for="name" class="col-sm-2 label-1 control-label">Name:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_add_keywordset_name" name="name" required="required" value="'.$this->mo_name.'">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="0" class="icheck OPS_show" ' . ($this->mo_cuvi == 0 ? 'checked="checked"' : '') .' id="iradio3">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="1" class="icheck OPS_show" ' . ($this->mo_cuvi == 1 ? 'checked="checked"' : '') .' id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Google Version:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="type">
                      <option value="'.$this->desktop_rs_type.'" ' . ($this->mo_type == $this->desktop_rs_type ? 'selected' : '') .'>Google Desktop</option>
                      <option value="'.$this->mobile_rs_type.'" ' . ($this->mo_type == $this->mobile_rs_type ? 'selected' : '') .'>Google Mobile</option>
                      <option value="'.$this->maps_rs_type.'"' . ($this->mo_type == $this->maps_rs_type ? 'selected' : '') .'>Google Desktop Maps</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Ort:</label>
                  <div class="col-sm-8">
                    <select class="chzn-select" multiple name="town[]" required="required">
                      '.$this->loc.'
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="team" class="col-sm-2 label-1 control-label">Keywords:</label>
                  <div class="col-sm-8">
                    <textarea class="rankings" id="team" name="keywords" placeholder="Ein Keyword pro Zeile!" required="required">';
          foreach ($this->mo_kewo as $keyword) {
            $out .= $keyword . "\n";
          }
          $out .='</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8" id="ops_spin">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_add_keywordset_submit">Campaign speichern</button>
                  </div>
                </div>
              </form>
            </div>
          ';

    echo $out;

  }


  private function renderViewCsv() {
    $out = '
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Neue Local Campaign hinzufügen</span>
            </div>
            <div class="box-content padded">

              <form class="form-horizontal fill-up" role="form" id="ops_add_keywordset" enctype="multipart/form-data">
                <input type="hidden" name="action" value="add">
                <input type="hidden" name="projectid" value="'.$this->project_id.'">
                <div class="form-group">
                  <label for="name" class="col-sm-2 label-1 control-label">Name:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_add_keywordset_name" name="name" required="required" value="'.$this->mo_name.'">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Anzeige Kundenversion:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="0" class="icheck OPS_show" ' . ($this->mo_cuvi == 0 ? 'checked="checked"' : '') .' id="iradio3">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="customer_view" value="1" class="icheck OPS_show" ' . ($this->mo_cuvi == 1 ? 'checked="checked"' : '') .' id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Google Version:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="type">
                      <option value="'.$this->desktop_rs_type.'" ' . ($this->mo_type == $this->desktop_rs_type ? 'selected' : '') .'>Google Desktop</option>
                      <option value="'.$this->mobile_rs_type.'" ' . ($this->mo_type == $this->mobile_rs_type ? 'selected' : '') .'>Google Mobile</option>
                      <option value="'.$this->maps_rs_type.'"' . ($this->mo_type == $this->maps_rs_type ? 'selected' : '') .'>Google Desktop Maps</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                  <div class="alert alert-info">
                    <strong>Bitte beachten!</strong><br />
                    Sie können nur CSV Files hochladen, beim Speichern aus Excel bitte das Format "Windows-kommagetrennt (.csv)" wählen.<br />
                    Die erste Spalte enhält das Keyword, die zweite Spalte die ID des Ortes, der Trenner im CSV ist muss ein ";" sein.<br />
                    Sie können auch den Namen des Ortes in der zweiten Spalte verwenden. Passt dieser aber nicht zu unseren Datenbankeinträgen wird die entsprechende Zeile ignoriert.<br />
                    Hier finden Sie ein <b><a href="'.$this->env_data['domain'].'temp/csv/local_csv_example.csv">Beispiel CSV</a></b>, außerdem eine <b><a href="'.$this->env_data['domain'].'temp/csv/locations.xlsx">Liste mit allen IDs / Orten</a></b>.
                    <br />
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="csv" class="col-sm-2 label-1 control-label">CSV Datei:</label>
                  <div class="col-sm-8">
                    <input type="file" class="form-control" name="csv" required="required">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8" id="ops_spin">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_add_keywordset_submit">Campaign speichern</button>
                  </div>
                </div>
              </form>
            </div>
          ';

    echo $out;

  }


  private function addNewKeywordSet ()
  {

    $parsed_keywords  = array();
    $project_id       = $this->project_id;

    $type           = $_POST['type'];
    $name           = $_POST['name'];
    $customer_view  = $_POST['customer_view'];

    // NO CSV
    if (empty($_FILES)) {

      $keywords       = $_POST['keywords'];
      $keywords       = explode("\n", $keywords);
      $town           = $_POST['town'];

      foreach ($town as $location) {
        $keyword_data[$location] = $keywords;
      }

    } else {

      $keyword_data = $this->csv_data;

    }

    foreach($keyword_data as $town => $keywords) {

      $parsed_keywords = array();
      foreach ($keywords as $keyword) {
        $keyword = trim($keyword);
        $keyword = strtolower($keyword);
        $keyword = preg_replace('/\s{2}/', ' ', $keyword);
        $keyword = htmlentities($keyword, null, 'utf-8');
        $keyword = str_replace('&nbsp;', ' ', $keyword);
        $keyword = html_entity_decode($keyword);
        if (empty($keyword)) {continue;}
        $parsed_keywords[$keyword] = $keyword;
      }

      $parsed_keywords = array_values($parsed_keywords);
      if (empty($parsed_keywords)) {continue;}
      $keyword_data[$town] = $parsed_keywords;

    }

    $campaign_type    = 'key';
    $keyword_set_name = strip_tags($name);

    // max 10 keywordsets per customer
    $sql = "SELECT id FROM ruk_local_keyword_sets WHERE id_customer='".$project_id."'";
    $exists = $this->db->query($sql);

    if ($exists->num_rows >= $this->env_data['ruk_max_keywordsets']) {
      echo json_encode(array('error' => 'Maximal ' . $this->env_data['ruk_max_keywordsets'] . ' Campaigns pro Projekt!'));
      exit;
    }

    $created_by = $_SESSION['firstname'] .' '. $_SESSION['lastname'];

// SAVE LOCAL KEYWORDSET
    $query = "INSERT INTO
                ruk_local_keyword_sets (id_customer, name, type, rs_type, customer_view, created_by, created_on)
              VALUES
                ('" . $project_id . "','" . $keyword_set_name . "','" . $campaign_type . "','" . $type . "','" . $customer_view . "','" . $created_by . "','" . $this->dateYMD() . "')";

    $this->db->query($query);

    $local_set_id = $this->db->insert_id;

// SAVE TO RS
    $geturl = "SELECT url FROM ruk_project_customers WHERE id = '$project_id'";
    $result = $this->db->query($geturl);
    $res    = $result->fetch_array();

    foreach ($keyword_data as $location => $keywords) {

      $keyword_set_name = 'RUKID: ' . $local_set_id . ' / LOC:' . $location;

      $ret  = $this->rankingspy->addKeywordSetLocal($res['url'], $keywords, $keyword_set_name, $location, $type);
      $rankingspy_id = $ret;

      // mapping table
      $query = "INSERT INTO ruk_local_sets2rs (id_kw_set, id_rs, location) VALUES ('" . $local_set_id . "','" . $rankingspy_id . "', '" . $location . "')";
      $this->db->query($query);

    }


// SAVE LOCAL KEYWORDS
    $sql = "INSERT INTO ruk_local_keywords (id_kw_set, keyword, language, location) values ";

    $used_keywords = array();
    $values = array();

    foreach ($keyword_data as $location => $keywords) {
      foreach ($keywords as $keyword) {
        $keyword  = $this->db->real_escape_string($keyword);
        $values[] = "('" . $local_set_id ."', '" . $keyword . "', '" . $this->country . "', '" . $location . "')";
      }
    }

    // ONEPROSEO
    $sql .= implode(',', $values);
    $result = $this->db->query($sql);


  }


  private function removeKeywordSet ()
  {

    $kw_set_id = $this->set_id;

    // GET RS IDs and remove on RS SERVER
    $sql = "SELECT id_rs FROM ruk_local_sets2rs WHERE id_kw_set = '" . $kw_set_id . "'";
    $result = $this->db->query($sql);
    $rankingspy_id = array();
    while ($row = $result->fetch_assoc()) {
      $rankingspy_id[] = $row['id_rs'];
    }
    foreach ($rankingspy_id as $rs_id) {
      $ret = $this->rankingspy->removeKeywordSet($rs_id);
    }

    // REMOVE FROM RUK DB
    $query = "DELETE a.*, b.*, c.*
              FROM ruk_local_keyword_sets a
                LEFT JOIN ruk_local_keywords b
                  ON b.id_kw_set = a.id
                LEFT JOIN ruk_local_sets2rs c
                  ON c.id_kw_set = a.id
              WHERE a.id ='".$kw_set_id."'";

    $this->db->query($query);

  }


  private function getOverallKeywordAmount ()
  {

    $sql    = "SELECT local_keyword_amount, country FROM ruk_project_customers WHERE id = '$this->project_id'";
    $result = $this->db->query($sql);
    $row    = $result->fetch_array(MYSQLI_ASSOC);

    $this->max_used_keywords = $row['local_keyword_amount'];
    $this->country           = $row['country'];

    $sql = "SELECT
              a.id as id
            FROM ruk_local_keyword_sets a LEFT JOIN ruk_local_keywords b ON b.id_kw_set = a.id
            WHERE id_customer = '$this->project_id'";

    $result2 = $this->db->query($sql);
    $used_keywords = $result2->num_rows;

    if ($used_keywords >= $this->max_used_keywords) {
      $this->limit = TRUE;
    }

  }


  private function checkForLimit () {

    if ($this->limit === TRUE) {
      echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><strong>Sie können keine neuen Keywords anlegen.<br />Die maximale Anzahl von '.$this->max_used_keywords.' Keywordüberwachungen ist erreicht.<br/>Bitte löschen Sie zuerst Keywords oder Sets.</strong></div></div></div>';
      exit;
    }

  }


  // SEARCH ALL RS LOCATIONS
  private function searchLocation($csv_town) {

    $sql = "SELECT * FROM ruk_local_locations WHERE town LIKE '%".$csv_town."%' AND town NOT REGEXP '[0-9]'";
    $result = $this->db->query($sql);
    $location = array();

    $rs_id = false;
    while ($row = $result->fetch_assoc()) {
      $rs_id = $row['rs_id'];
    }

    return $rs_id;

  }


  // GET ALL RS LOCATIONS
  public function getLocations() {

    $country = $this->country;

    $sql = "SELECT * FROM ruk_local_locations WHERE country = '$country'";
    $result = $this->db->query($sql);
    $location = array();

    while ($row = $result->fetch_assoc()) {

      if (preg_match('#[0-9]#',$row['town'])) {
        continue;
      }

      $location[$row['town']] = $row['town'];

      if (isset($this->mo_loca[$row['rs_id']])) {
        $this->loc .= '<option selected value="'.$row['rs_id'].'">'.$row['town'].'</option>';
      } else {
        $this->loc .= '<option value="'.$row['rs_id'].'">'.$row['town'].'</option>';
      }

    }

  }

}

?>
