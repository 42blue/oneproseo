<?php

require_once('ruk.class.php');

class project_add extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['name']) && isset($_POST['url'])) {
      $this->addProject();
      echo json_encode($this->out);
    }

  }


  private function addProject ()
  {

    $project_name          = strip_tags($_POST['name']);
    $project_manager       = strip_tags($_POST['manager']);
    $project_team_mail     = strip_tags($_POST['team']);
    $project_competition   = strip_tags($_POST['competition']);
    $project_brandkeywords = strip_tags($_POST['brandkeywords']);
    $project_category      = strip_tags($_POST['category']);
    $project_type          = strip_tags($_POST['type']);
    $project_country       = strip_tags($_POST['country']);
    $project_local         = strip_tags($_POST['local_rankings']);
    $ignore_subdomain      = strip_tags($_POST['ignore_subdomain']);
    $project_url           = $_POST['url'];

    if (empty($project_name)) {
      $this->out = array ('error' => 'Kein Projektname angegeben!');
      return;
    }

   $project_team_mail     = $this->parseEmail($project_team_mail);
   $project_competition   = $this->parseUrl($project_competition);
   $project_brandkeywords = $this->parseBrandKeywords($project_brandkeywords);

    // check if project name exists
    $sql = "SELECT id FROM ruk_project_customers WHERE name='".$project_name."'";
    $project_exists = $this->db->query($sql);

    if ($project_exists->num_rows > 0) {
      $this->out = array ('error' => 'Projekt existiert bereits!');
      return;
    }

    $created_by = $_SESSION['firstname'] .' '. $_SESSION['lastname'];

    $token = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));

    // add new project
    $sql = "INSERT INTO
              ruk_project_customers (name, country, url, manager, team_email, category, type, local, ignore_subdomain, created_by, created_on, competition, brandkeywords, api_key)
            VALUES ('" . $project_name . "', '" . $project_country . "', '" . $project_url . "', '" . $project_manager . "', '" . $project_team_mail . "', '" . $project_category . "', '" . $project_type . "', '" . $project_local . "', '" . $ignore_subdomain . "', '" . $created_by . "', '" . $this->dateYMD() . "', '" . $project_competition . "', '" . $project_brandkeywords . "', '" . $token . "')";

    $this->db->query($sql);

    if (isset($this->db->insert_id)) {
      $this->out = array ('success' => 'Das Projekt '.$_POST['name'].' wurde hinzugefügt!');
    } else {
      $this->out = array ('error' => 'Ein DB Fehler ist aufgetreten. Bitte informieren Sie den Administrator!');
    }

  }



  private function parseEmail($project_team_mail) {

    $unparsed_teammail  = strip_tags($project_team_mail);
    $unparsed_teammail  = explode("\n", $unparsed_teammail);

    $parsed_teammail = array();
    foreach ($unparsed_teammail as $mailadress) {
      $mailadress = strtolower(trim($mailadress));
      if (filter_var($mailadress, FILTER_VALIDATE_EMAIL)) {
        $parsed_teammail[] = $mailadress;
      }
    }

    $parsed_teammail = implode(',', $parsed_teammail);

    return $parsed_teammail;

  }


  private function parseUrl($project_competition) {

    $unparsed_competition  = strip_tags($project_competition);
    $unparsed_competition  = explode("\n", $unparsed_competition);

    $parsed_competition = array();
    foreach ($unparsed_competition as $url) {
      $url = strtolower(trim($url));
      if (filter_var($url, FILTER_VALIDATE_URL)) {
        $parsed_competition[] = $url;
      }
    }

    // 8 max
    if (count($parsed_competition) > 7) {
      $parsed_competition = array_slice($parsed_competition,0,7);
    }

    $parsed_competition = serialize($parsed_competition);

    return $parsed_competition;

  }


}

?>
