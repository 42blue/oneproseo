<?php

require_once('ruk.class.php');

class local_overview extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['projectid'])) {
      $this->project_id = $_POST['projectid'];
      $this->getProjectData();
      $this->renderView();
    }

  }


  private function getProjectData()
  {

    $s_query = '';
    if ($this->env_data['customer_version'] == 1) {
      $s_query = 'AND b.customer_view = 1';
    }

    $sql = "SELECT
              a.id             AS aid,
              a.name           AS project,
              a.url            AS url,
              a.category       AS category,
              a.manager        AS manager,
              a.ga_account     AS ga_account,
              a.gwt_account    AS gwt_account,
              a.local          AS local,
              b.id             AS bid,
              b.customer_view  AS customer_view,
              b.name           AS setname,
              b.type           AS type,
              b.rs_type        AS rs_type,
              b.created_by     AS created_by,
              b.created_on     AS created_on,
              count(DISTINCT c.keyword) AS kwcount,
              c.keyword        AS keyword,
              c.location       AS location
            FROM ruk_project_customers a
              LEFT JOIN ruk_local_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_local_keywords c
                ON c.id_kw_set = b.id
            WHERE a.id = '".$this->project_id."'
            ".$s_query."
            GROUP BY bid";

    $result = $this->db->query($sql);

    $kwcount = 0;

    while ($row = $result->fetch_assoc()) {
      $this->projectname   = $row['project'];
      $this->projecturl    = $row['url'];
      $this->pjm           = $row['manager'];
      $this->category      = $row['category'];
      $this->ga_account    = $row['ga_account'];
      $this->local         = $row['local'];
      $this->created_by    = $row['created_by'];
      $this->created_on    = $row['created_on'];

      $kwcount = $kwcount + $row['kwcount'];

      // LOCATIONS
      $sql = "SELECT id FROM ruk_local_sets2rs WHERE id_kw_set = '".$row['bid']."'";
      $result2 = $this->db->query($sql);
      $count_locations = $result2->num_rows;

      // KEYWORDS
      $sql = "SELECT DISTINCT keyword FROM ruk_local_keywords WHERE id_kw_set = '".$row['bid']."' ORDER BY keyword";
      $result3 = $this->db->query($sql);
      $kws = '';
      while ($row3 = $result3->fetch_assoc()) {
        $kws .= '<option value="'.$row3['keyword'].'">'.$row3['keyword'].'</option>';
      }

      $this->kwsets[$row['bid']] = array ($row['setname'], $row['kwcount'], $row['type'], $row['rs_type'], $count_locations, $kws);

    }

    $this->amount_keywords = $kwcount;

  }


  private function renderView () {

    if ($this->local == 0) {
      echo '<div class="row"><div class="col-md-12"><div class="alert alert-error"><strong>Local Rankings sind nicht aktiviert.</strong></div></div></div>';
      exit;
    }

    $this->out = '<div class="row"><div class="col-md-6"><div class="box"><div class="box-header"><span class="title">Domain Daten</span></div><div class="box-content"><table class="table table-normal"><tbody><tr class="status-pending"><td class="icon"><i class="icon-bookmark"></i></td><td>Domain:</td><td><span id="ops_url" data-url="'.$this->projecturl.'">'.$this->projecturl.'</span></td></tr><tr class="status-pending"><td class="icon"><i class="icon-eye-open"></i></td><td>Keywords:</td><td>'.$this->amount_keywords.'</td></tr></tbody></table></div></div></div><div class="col-md-6"><div class="box"><div class="box-header"><span class="title">Projekt Daten</span><ul class="box-toolbar"><li><a href="'.$this->project_id.'/keywordsets"><span class="label label-red-super">Campaigns bearbeiten</span></a></li></ul></div><div class="box-content"><table class="table table-normal"><tbody><tr class="status-pending"><td class="icon"><i class="icon-lightbulb"></i></td><td>Projektname:</td><td>'.$this->projectname.'</td></tr><tr class="status-pending"><td class="icon"><i class="icon-user"></i></td><td>SEO Project Manager:</td><td>'.$this->pjm.'</td></tr></tbody></table></div></div></div></div><div class="row"><div class="col-md-12"><div class="box"><div class="box-header padded"><a href="../overview/'.$this->project_id.'"><span class="btn btn-default f12 right"><span>zu den landesweiten Rankings</span></span></a><span class="btn btn-default f12" id="ops_load_seven_graph31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_graph7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden (alle)</span><span class="btn btn-default f12" id="ops_load_seven_table"><i class="icon-table"></i> 7 Tage Rankingverteilung laden (alle)</span><span class="btn btn-default f12" id="ops_clear_views"><i class="icon-refresh"></i> Refresh </span></div></div></div></div>';

    $this->keyword_set_out = '';

    foreach ($this->kwsets as $setid => $data) {

      if (empty($setid)) {

        $this->hidechart = true;
        $this->keyword_set_out .= '<div class="span6"><div class="box"><div class="box-content padded"><a href="'.$this->project_id.'/keywordsets"><span class="label label-green">Campaigns anlegen</span></a></div></div></div>';

      } else {


        if ($data[1] == 0) {

          $this->keyword_set_out .= $this->emptySet($data[0], $setid, $data[1], $data[2]);

        } else {

          $this->keyword_set_out .= '
          <div class="span6">
            <div class="box">

              <div class="box-header">
                <span class="big-green">'.$data[0].'</span>
                <span class="small">Keyword Campaign / '. $this->rsTypeLocal($data[3]);
          $this->keyword_set_out .= $this->setInfo();
          $this->keyword_set_out .= '<ul class="box-toolbar">';
          $this->keyword_set_out .= $this->editKeywordset($setid, $data[1], $data[2], $data[4]);
          $this->keyword_set_out .= '
                </ul>
              </div>

              <div class="box-content ops_setdata ops_setdata_bg" data-setid="'.$setid.'" data-projectid="'.$this->project_id.'">
                <div class="ops_select_view">
                  <span class="btn btn-default ops_setload_dex31"><i class="icon-bar-chart"></i> 31 Tage OneDex laden</span><span class="btn btn-default ops_setload_dex7"><i class="icon-bar-chart"></i> 7 Tage OneDex laden</span><span class="btn btn-default ops_setload"><i class="icon-table"></i> 7 Tage Rankingverteilung laden</span><span class="btn btn-default ops_set"><i class="icon-table"></i> Rankingverteilung: <select class="uniform ops_setload_keyword"><option value="0">auswählen</option>'.$data[5].'</select></span><span class="btn btn-default ops_set"><i class="icon-bar-chart"></i> Rankingverteilung Graph: <select class="uniform ops_setload_graph"><option value="false">auswählen</option><option value="7">7 Tage</option><option value="31">31 Tage</option></select></span>
                </div>
                <div class="ops_show_view" style="display:none;"></div>
                <div class="box-footer">
                  <ul class="box-toolbar">
                    <li><a href="'.$this->project_id.'/detail-keyword/'.$setid.'" target="_blank"><span class="label label-dark-blue f12">Keywords / Rankings</span></a></li>
                    <li><a href="'.$this->project_id.'/detail-url/'.$setid.'" target="_blank"><span class="label label-red f12">URL / Keywords (Campaign)</span></a></li>
                    <li><a href="'.$this->project_id.'/detail-url-complete/'.$setid.'" target="_blank"><span class="label label-red f12">URL / Keywords (DB)</span></a></li>
                    <li><a href="'.$this->project_id.'/detail-url-onedex/'.$setid.'" target="_blank"><span class="label label-blue f12">URL / OneDex (Campaign)</span></a></li>
                    <li><a href="'.$this->project_id.'/detail-url-onedex-complete/'.$setid.'" target="_blank"><span class="label label-blue f12">URL / OneDex (DB)</span></a></li>
                    <li><a href="'.$this->project_id.'/detail-keyword-url/'.$setid.'" target="_blank"><span class="label label-green f12">Keywords / URL</span></a></li>
                    <li><a href="'.$this->project_id.'/detail-compare/'.$setid.'/'.$this->germanTSred($this->created_on).'" target="_blank"><span class="label label-cyan f12">Ranking Datumsvergleich</span></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>';
        }
      }

    }

    echo $this->out . $this->keyword_set_out;

  }



  private function emptySet($setname, $setid, $count, $type) {

    $settype  = array ('url' => 'URL Campaign', 'key' => 'Keyword Campaign');
    $setident = array ('url' => 'URLs', 'key' => 'Keywords');
    $setcolor = array ('url' => 'blue', 'key' => 'green');

    return '
              <div class="span6">
                <div class="box">
                  <div class="box-header">
                    <span class="big-'.$setcolor[$type].'">'.$setname.'</span> <span class="small">'.$settype[$type].'</span>
                    <ul class="box-toolbar">
                      <li><a href="'.$this->project_id.'/keywordsets/'.$setid.'"><span class="label label-'.$setcolor[$type].'"><span f12>'.$count.'</span> '.$setident[$type].'</span></a></li>
                    </ul>
                  </div>
                  <div class="box-content padded">
                    <a href="'.$this->project_id.'/keywordsets/'.$setid.'">
                    <span class="label label-'.$setcolor[$type].'">'.$setident[$type].' hinzufügen</span></a>
                  </div>
                </div>
              </div>';

  }


  private function editKeywordset($setid, $count, $type, $count_loc) {

    $settype = array ('url' => 'URL(s)', 'key' => 'Keyword(s)');
    return '<li><a href="'.$this->project_id.'/keywords/'.$setid.'"><span class="label label-red-super f12"><span>'.$count.'</span> '.$settype[$type].'  an '.$count_loc.' Ort(en)</span></a></li>';

  }


  private function setInfo () {

    if (empty($this->created_by)) {
      $created_by = '';
    } else {
      $created_by = ' / ' . $this->created_by;
    }

    return $measuring . $created_by . ' / ' . $this->germanTSred($this->created_on) . '</span>';

  }


}

?>
