<?php

require_once('ruk.class.php');

class keywords_copy extends ruk 
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    $this->set_to_copy = $_POST['setid'];
    $this->getAllCustomers();
    echo $this->out_view;

  }


  private function getAllCustomers()
  {

    $view_table = '';

    $sql = "SELECT 
              a.id             AS aid,
              a.name           AS setname,
              a.type           AS type,
              a.customer_view  AS customer_view,
              a.rankingspy_id  AS rankingspy_id,
              a.measuring      AS measuring,
              count(b.keyword) AS kwcount,
              count(c.url)     AS urlcount
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_urls c
                ON c.id_kw_set = a.id
            WHERE a.id = '".$this->set_to_copy."'
            GROUP BY setname";

    $result = $this->db->query($sql);

    while ($row_set = $result->fetch_assoc()) {
      $setname = $row_set['setname'];
      $set_id  = $row_set['aid'];
      $type    = $row_set['type'];
      if ($row_set['type'] == 'key') {
        $type_verbose = 'Keyword Set';
        $count        = $row_set['kwcount'];
      } else {
        $type_verbose = 'URL Set';
        $count = $row_set['urlcount'];
      }
    }

    $sql = "SELECT 
              id,
              name,
              url,
              country
            FROM 
              ruk_project_customers
            ORDER BY 
              name";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $view_table .= '<tr>
                        <td>' . $row['name'] . '</td>
                        <td>' . $row['url'] . '</td>
                        <td><img src="'.$this->env_data['domain'].'oneproseo/src/images/flags/'.$row['country'].'.png" width="25"></td>
                        <td><span class="label label-green ops_copy_to" data-setid="' . $set_id . '" data-type="' .$type. '" data-projectid="' . $row['id'] . '">zu diesem Kunden kopieren</span></td>
                      </tr>';
    }


    $this->out_view = '

      <div class="box">
        <div class="box-header">
          <span class="title">Folgendes Set kopieren: </span>
        </div>
        <div class="box-content padded">
          Name: <strong>' . $setname . '</strong><br />Typ: <strong>' . $type_verbose . '</strong><br /> Anzahl Keywords/URLs: <strong>' . $count . '</strong>
        </div>
      </div>
      <div class="box">
        <div class="box-header">
          <span class="title">Vorhandene Projekte</span>
        </div>
        <div class="box-content">
          <table class="table table-normal" id="data-table"><thead><tr><td>Projekt</td><td>URL</td><td>Land</td><td>kopieren</td></thead><tbody>';

    $this->out_view .= $view_table;

    $this->out_view .= '</tbody></table>
        </div>
      </div>';

  }

}

?>