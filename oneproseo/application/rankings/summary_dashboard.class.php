<?php

require_once('ruk.class.php');

class summary_dashboard extends ruk
{

  private $out_view = '';

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['type'])) {

      $type = $_POST['type'];
      $this->type = $type;

      $this->fetchData();

      if ($type == 'admin') {
        $this->getProjectDataAdmin();
      }

      if ($type == 'dashboard' || $type == 'seoprojects') {
        $this->getProjectDataDashboard();
      }

      if ($type == 'seaseobalancer') {
        $this->getProjectDataSeaSeoBalancer();
      }

      // DAILY
      // https://oneproseo.advertising.de/dev-oneproseo/rankings/dashboard-rankings
      if ($type == 'rankings') {
        $this->getProjectDataDashboardRankings();
      }

      // WEEKLY
      // https://oneproseo.advertising.de/dev-oneproseo/rankings/dashboard-visibility
      if ($type == 'visibility') {
        $this->getProjectDataDashboardVisibility();
      }

      echo $this->out_view;

    }

  }


  private function fetchData()
  {

    if (isset($this->env_data['customer_available'])) {

      /// XXXLUTZ advanced user management
      if ($_SESSION['restrict_customer_versions'] != 0) {
        $show_only = $_SESSION['restrict_customer_versions'];
      } else {
        $show_only = $this->env_data['customer_available'];
      }

      $sql = "SELECT a.id                      AS mid,
                     a.name                    AS project,
                     a.customer_suffix         AS csuffix,
                     a.url                     AS url,
                     a.country                 AS country,
                     a.manager                 AS manager,
                     a.team_email              AS team_email,
                     a.category                AS category,
                     a.ga_account              AS analytics,
                     a.gwt_account             AS gwt_account,
                     a.mediavalue              AS mediavalue,
                     a.value                   AS value,
                     a.value_now               AS value_now,                     
                     a.type                    AS type,
                     a.local                   AS local,
                     a.ignore_subdomain        AS ignore_subdomain,
                     a.created_by              AS created_by,
                     a.created_on              AS created_on,
                     count(DISTINCT b.name)    AS setcount,
                     count(c.keyword)          AS kwcount,
                     count(DISTINCT c.keyword) AS kwcountunique,
                     c.keyword                 AS keyword
              FROM ruk_project_customers a
                LEFT JOIN ruk_project_keyword_sets b
                  ON b.id_customer = a.id
                LEFT JOIN ruk_project_keywords c
                  ON c.id_kw_set = b.id
              WHERE a.id IN ($show_only)
              GROUP BY a.name";

    } else {

      $sql = "SELECT a.id                      AS mid,
                     a.name                    AS project,
                     a.customer_suffix         AS csuffix,
                     a.url                     AS url,
                     a.country                 AS country,
                     a.manager                 AS manager,
                     a.team_email              AS team_email,
                     a.category                AS category,
                     a.ga_account              AS analytics,
                     a.gwt_account             AS gwt_account,
                     a.mediavalue              AS mediavalue,
                     a.value                   AS value,
                     a.value_now               AS value_now,
                     a.type                    AS type,
                     a.local                   AS local,
                     a.ignore_subdomain        AS ignore_subdomain,
                     a.created_by              AS created_by,
                     a.created_on              AS created_on,
                     count(DISTINCT b.name)    AS setcount,
                     count(c.keyword)          AS kwcount,
                     count(DISTINCT c.keyword) AS kwcountunique,
                     c.keyword                 AS keyword
              FROM ruk_project_customers a
                LEFT JOIN ruk_project_keyword_sets b
                  ON b.id_customer = a.id
                LEFT JOIN ruk_project_keywords c
                  ON c.id_kw_set = b.id
              GROUP BY a.name";
    }

    $this->result = $this->db->query($sql);

    while ($row = $this->result->fetch_assoc()) {
      $this->rows[$row['mid']] = $row;
    }


    // get all keywords by project
    $sql3 = "SELECT
                   a.id_customer    AS id_customer,
                   b.keyword        AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id";

    $res3 = $this->db->query($sql3);

    $this->project_keywords = array();
    while ($row = $res3->fetch_assoc()) {
      $this->project_keywords[$row['id_customer']][$row['keyword']] = $row['keyword'];
    }

    // get all keywords that are with more than one customer
    $sql2 = "SELECT
              keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            GROUP BY keyword HAVING COUNT(*) > 1";

    $res  = $this->db->query($sql2);

    $this->multi_keywords = array();
    while ($row = $res->fetch_assoc()) {
      $this->multi_keywords[$row['keyword']] = $row['keyword'];
    }

  }


  private function diffKeywords($mid) {

    $unique = 0;

    if (empty($this->project_keywords[$mid])) {
      return 0;
    }

    $errors = array_filter($this->project_keywords[$mid]);

    if (empty($errors)) {
      return $unique;
    }

    foreach ($this->project_keywords[$mid] as $keyword => $value) {

      if (!isset( $this->multi_keywords[$keyword] )) {
        $unique++;
      }

    }

    return $unique;

  }


  private function getProjectDataSeaSeoBalancer()
  {

    $this->out_view = '<table class="table table-normal dtoverflow" id="data-table"><thead><tr><td>Projektname</td><td>Zeitraum<br />90 Tage</td><td>Zeitraum auswählen</td><td>URL</td><td>Campaigns</td><td>Keywords</td><td>SEO Project Manager</td><td>Search Console</td><td>Google Analytics</td><td>Media Value<br/>(letzter Monat)</td><td>Media Value<br/>(aktueller Monat)</td><td>Kunde angelegt</td>';
    $this->out_view .= '</tr></thead><tbody>';

    $timestart = date('d.m.Y', strtotime('- 91 day')); 
    $timeend   = date('d.m.Y', strtotime('- 1 day'));


    if ($this->result->num_rows === 0) {

        $this->out_view = '<tr><td>Keine Projekte gefunden.</td></tr>';

    } else {

      foreach ($this->rows as $key => $row) {

        if (empty($row['analytics'])) {
          continue;
        }

        if ($row['gwt_account'] == 0) {
          continue;
        }

        $this->out_view .= '<tr>
          <td><b>' . $row['project'] . '</b></td>
          <td><a href="overview/'.$row['mid'].'/'.$timestart.'/'.$timeend.'" class="label label-green">90 Tage</a></td>
          <td><a href="overview/'.$row['mid'].'" class="label label-blue">frei definieren</a></td>
          <td> ' .$row['url']. '</td>
          <td> ' .$row['setcount']. '</td>
          <td> ' .$row['kwcount']. '</td>
          <td> ' .$row['manager']. ' </td>
          <td> ' .$row['analytics']. '</td>
          <td>Ja</td>
          <td> € ' . number_format($row['mediavalue'], 0, ',', '.') . '</td>
          <td> € ' .number_format($row['value_now'], 0, ',', '.'). '</td>
          <td> ' . $this->germanTSred($row['created_on']). '</td>';

        $this->out_view .= '</tr>';
      }

    }

    $this->out_view .= '</tbody></table>';

  }


  private function getProjectDataAdmin()
  {

    $this->out_view = '<table class="table table-normal dtoverflow" id="data-table"><thead><tr><td>Projektname</td><td>URL</td><td>Campaigns</td><td>Keywords</td><td>SEO Project Manager</td><td>Team</td><td>Kategorie</td><td>Typ</td><td>Local</td><td>Subdomain</td><td>Erstellt von</td><td>Datum</td><td>Land</td><td>bearbeiten</td>';
    $this->out_view .= '<td>löschen</td>';
    $this->out_view .= '</tr></thead><tbody>';

    if ($this->result->num_rows === 0) {

        $this->out_view = '<tr><td>Keine Projekte gefunden.</td></tr>';

    } else {

      foreach ($this->rows as $key => $row) {

        if ($row['type'] == 'new_business') {
          $row['type'] = 'New Business';
        } else if ($row['type'] == 'customer') {
          $row['type'] = 'Customer';
        } else if ($row['type'] == 'competitor') {
          $row['type'] = 'Competitor';
        } else if ($row['type'] == 'one_internal') {
          $row['type'] = 'One Intern';
        }

        if ($row['local'] == 1) {
          $row['local'] = 'Ja';
        } else {
          $row['local'] = 'Nein';
        }

        if ($row['ignore_subdomain'] == 1) {
          $row['ignore_subdomain'] = 'Ja';
        } else {
          $row['ignore_subdomain'] = 'Nein';
        }

        $this->out_view .= '<tr><td><a href="overview/'.$row['mid'].'"><b>' . $row['project'] . '</b></a></td><td> <a href="overview/'.$row['mid'].'">'  . $row['url'] . '</a></td><td> ' .$row['setcount']. '</td><td> ' .$row['kwcount']. '</td><td> ' .$row['manager']. '</td><td>' . str_replace(',', '<br/>', $row['team_email']) . '</td><td><a href="overview/detail-category/'.$row['category'].'">'. ucwords($row['category']).'</a></td><td> ' .$row['type']. '</td><td> ' .$row['local']. '</td><td> ' .$row['ignore_subdomain']. '</td><td> ' .$row['created_by']. '</td><td> ' . $this->germanTSred($row['created_on']). '</td><td><img src="'.$this->env_data['domain'].'oneproseo/src/images/flags/'.$row['country'].'.png" width="25"></td><td><a class="label label-green" href="projects-change/'.$row['mid'].'">bearbeiten</a></td>';
        $this->out_view .= '<td><div class="btn-group"><button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button><ul class="dropdown-menu"><li><a href="#" class="ops_delete_project" data-projectid="'.$row['mid'].'">Projekt löschen</a></li></ul></div></td>';
        $this->out_view .= '</tr>';
      }

    }

    $this->out_view .= '</tbody></table>';

  }


  private function getProjectDataDashboard()
  {

    $this->out_view = '<table class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Projektname</td><td>URL</td><td>Keywords</td><td>Unique Keywords</td><td>Mediavalue All (Last Month)</td><td>Mediavalue Campaigns (Last Month)</td><td>SEO Project Manager</td><td>Team</td><td>Kategorie</td><td>Typ</td><td>Analytics</td><td>SC</td><td>Land</td><td>anzeigen</td></tr></thead><tbody>';

    $i = 0;

    if ($this->result->num_rows === 0) {

        $this->out_view = '<tr><td>Keine Projekte gefunden.</td></tr>';

    } else {

      $this->customer_urls = array ();

      foreach ($this->rows as $key => $row) {

        if ($row['type'] == 'new_business') {
          $row['type'] = 'New Business';
        } else if ($row['type'] == 'customer') {
          $row['type'] = 'Customer';
        } else if ($row['type'] == 'competitor') {
          $row['type'] = 'Competitor';
        } else if ($row['type'] == 'one_internal') {
          $row['type'] = 'One Intern';
        }

        $gwt = 'Nein';
        if ($row['gwt_account'] == 1) {
          $gwt = 'Ja';
        }

        $analytics = 'Nein';
        if (!empty($row['analytics'])) {
          $analytics = 'Ja';
        }

        $i++;

        $row['url'] = trim($row['url'],'/');

        $this->customer_urls[$row['url']] = $row['url'];

        // check for unique keywords
        $unique = $this->diffKeywords($row['mid']);
        $this->out_view .= '<tr><td>'.$i.'</td><td>';

        if ($_SESSION['role2'] !== 'nodash') {
          if (!empty($row['csuffix'])) {
            $this->out_view .= '<a target="_blank" href="mis/'.$row['mid'].'"><i class="icon-dashboard"></i></a> ';
          }
        }
        $this->out_view .= '<a href="overview/'.$row['mid'].'"><b>' . $row['project'] . '</b></a></td><td> <a href="overview/'.$row['mid'].'">'  . $row['url'] . '</a></td><td>' . $row['kwcount'] . '</td><td>' . $unique . '</td>';
        $this->out_view .= '<td data-sort="' . (int) $row['mediavalue'] . '" class="nowrap">€ ' . number_format($row['mediavalue'], 0, ',', '.') . '</td>';
        $this->out_view .= '<td data-sort="' . (int) $row['value'] . '" class="nowrap">€ ' . number_format($row['value'], 0, ',', '.') . '</td>';
        $this->out_view .= '<td>' .$row['manager']. '</td><td>' . str_replace(',', '<br/>', $row['team_email']) . '</td><td><a href="overview/detail-category/'.$row['category'].'">'. ucwords($row['category']).'</a></td><td> ' .$row['type']. '</td><td> ' . $analytics . '</td><td> ' . $gwt . '</td><td><img src="'.$this->env_data['domain'].'oneproseo/src/images/flags/'.$row['country'].'.png" width="25"></td><td><a href="overview/'.$row['mid'].'" class="label label-green">anzeigen</a></td></tr>';

      }

    }

    $this->out_view .= '</tbody></table>';

  }


  private function getProjectDataDashboardRankings()
  {

    while ($row = $this->result->fetch_assoc()) {
      $rows[$row['mid']] = $row;
    }

    $this->rankset = parent::fetchOneDexData();

    // CUSTOMER INTERCEPTOR
    if (isset($this->env_data['customer_available'])) {
      $show_only = explode (',', $this->env_data['customer_available']);
      foreach ($this->rankset as $key => $value) {
        if (!in_array($key, $show_only)) {
          unset($this->rankset[$key]);
        }
      }
    }

    $this->rankset_31 = $this->rankset;
    // CALC AVG DEX FOR 31 DAYS
    $dex31 = array();
    foreach ($this->rankset_31 as $c_id => $dataset) {
      $dex_avg = 0;
      $rounds = 0;
      foreach ($dataset as $ts => $value) {
        $rounds++;
        $dex_avg = $dex_avg + $value['dex'];
      }
      $dex31[$c_id] = $dex_avg / $rounds;
    }

    $this->out_view = '<table class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Projektname</td><td>Typ</td><td>Trend</td>';

    // DATES
    $d = key ($this->rankset);
    $ii = 0;
    foreach ($this->rankset[$d] as $ts => $v) {
      $ii++;
      $this->out_view .= '<td>OneDex ' . parent::germanTS($ts) . '</td>';
      if ($ii < count($this->rankset[$d])) {
        $this->out_view .= '<td>Change</td>';
      }
    }

    $this->out_view .= '<td>anzeigen</td></tr></thead><tbody>';

      $i = 1;
      foreach ($this->rankset as $c_id => $row) {

        if ($this->rows[$c_id]['type'] == 'new_business') {
          $this->rows[$c_id]['type'] = 'New Business';
        } else if ($this->rows[$c_id]['type'] == 'customer') {
          $this->rows[$c_id]['type'] = 'Customer';
        } else if ($this->rows[$c_id]['type'] == 'competitor') {
          $this->rows[$c_id]['type'] = 'Competitor';
        } else if ($this->rows[$c_id]['type'] == 'one_internal') {
          $this->rows[$c_id]['type'] = 'One Intern';
        }

        $today = $this->dateYMD();

        if (!isset($this->rankset[$c_id][$today]['dex'])) {
          $today = $this->dateYMDYesterday();
        }

        $this->out_view .= '<tr><td>'.$i++.'</td><td><a href="overview/'.$c_id.'">' . $this->rows[$c_id]['project'] . '</a></td><td>' . $this->rows[$c_id]['type'] . '</td>';
        $this->out_view .= '<td class="darker"><span class="data-tooltip" data-tooltip="Der heutige OneDex im Vergleich zum Durchschnitt OneDex der letzten 31 Tage">' . parent::calcPercent($this->rankset[$c_id][$today]['dex'], $dex31[$c_id], true, 0) . '</span></td>';

        $date = $this->rankset[$c_id];

        $ii = 0;
        foreach ($this->rankset[$c_id] as $ts => $value) {

          $ii++;
          next($date);
          $yesterday = key($date);

          $perc = parent::calcPercent($value['dex'], $this->rankset[$c_id][$yesterday]['dex'], true, 0);

          $this->out_view .= '<td class="tdright" data-order="'.$value['dex'].'">' . number_format($value['dex'], 2, ',', '.') .'</td>';
          if ($ii < count($this->rankset[$c_id])) {
            $this->out_view .= '<td class="tdleft nowrap">' . $perc .' </td>';
          }

        }

        $this->out_view .= '<td><a href="overview/'.$c_id.'" class="label label-green">anzeigen</a></td></tr>';

      }

    $this->out_view .= '</tbody></table>';

  }



  private function getProjectDataDashboardVisibility()
  {

    while ($row = $this->result->fetch_assoc()) {
      $rows[$row['mid']] = $row;
    }

    $this->fetchSistrixData();
    $this->fetchSearchmetricsData();
    $this->rankset = parent::fetchOneDexData();

    $today =  $this->dateYMD();
    $lastweek = '-7 days';

    $i = 1;

    // CUSTOMER INTERCEPTOR
    if (isset($this->env_data['customer_available'])) {
      $show_only = explode (',', $this->env_data['customer_available']);
      foreach ($this->rankset as $key => $value) {
        if (!in_array($key, $show_only)) {
          unset($this->rankset[$key]);
        }
      }
    }

    $out_view_table = '';

    foreach ($this->rankset as $c_id => $row) {

      $shown[$this->rows[$c_id]['url']] = $this->rows[$c_id]['url'];

      $out_view_table .= '<tr>
      	<td>'.$i++.'</td><td><a href="overview/'.$c_id.'">' . $this->rows[$c_id]['project'] . '</a></td>
      	<td><a href="'.$this->rows[$c_id]['url'].'" target="_blank">' . $this->rows[$c_id]['url'] . '</a></td>
      	<td class="gray"></td>';

      $firstkey = parent::dateYMD();
      if (!isset($this->rankset[$c_id][$firstkey]['dex'])) {
        $firstkey = $this->dateYMDYesterday();
      }

      $lastkey  = date('Y-m-d', strtotime('-6 days'));
      //weekly
      if (empty($this->rankset[$c_id][$lastkey]['dex'])) {
        $lastkey  = date('Y-m-d', strtotime('-8 days'));
      }

      $perc = parent::calcPercent($this->rankset[$c_id][$firstkey]['dex'], $this->rankset[$c_id][$lastkey]['dex']);

      $this->countUpDown($this->rankset[$c_id][$firstkey]['dex'], $this->rankset[$c_id][$lastkey]['dex'] , 'onedex');

      $out_view_table .= '<td>'.number_format($this->rankset[$c_id][$firstkey]['dex'], 2, ',', '.').'</td><td>'.number_format($this->rankset[$c_id][$lastkey]['dex'], 2, ',', '.').'</td><td>'.$perc.'</td><td class="gray"></td>';

      $out_view_table .= $this->renderVendorDataSistrix($this->sistrix, $c_id, $this->rows[$c_id]['url'], $this->rows[$c_id]['country']);

      $out_view_table .= $this->renderVendorData($this->searchmetrics, $c_id, 'searchmetrics', $this->rows[$c_id]['url']);

      $out_view_table .= '</td>';

    }


    $this->out_view = '
      <table class="table table-normal data-table dtoverflow" id="data-table">
        <thead>
          <tr>
            <td rowspan="3">#</td>
            <td rowspan="3">Projektname</td>
            <td rowspan="3">URL</td>
            <td rowspan="3" class="gray"></td>
            <td colspan="3">OneDex</td>
            <td rowspan="3" class="gray"></td>
            <td colspan="3">Sistrix</td>
            <td rowspan="3" class="gray"></td>
            <td colspan="3">Searchmetrics</td>
          </tr>
          <tr>
            <td>' . parent::germanTSred($today) . '</td><td>' . parent::germanTSred($lastweek) . '</td><td>Change</td>
            <td>' . parent::germanTSred(key($this->sistrix_ts)) . '</td><td>' . parent::germanTSred(next($this->sistrix_ts)) . '</td><td>Change</td>
            <td>' . parent::germanTSred(key($this->searchmetrics_ts)) . '</td><td>' . parent::germanTSred(next($this->searchmetrics_ts)) . '</td><td>Change</td>
          </tr>
          <tr>
            <td colspan="3">Gestiegen: ' . $this->up_onedex . ' / Gefallen: ' . $this->down_onedex . '</td>
            <td colspan="3">Gestiegen: ' . $this->up_sistrix . ' / Gefallen: ' . $this->down_sistrix . '</td>
            <td colspan="3">Gestiegen: ' . $this->up_searchmetrics . ' / Gefallen: ' . $this->down_searchmetrics . '</td>
          </tr>';

    $this->out_view .= '</thead><tbody>';
    $this->out_view .= $out_view_table;
    $this->out_view .= '</tbody></table>';

  }


  private function renderVendorData ($vendor, $c_id, $source, $url) {

    if (!isset($vendor[$c_id][0])) {
      $vendor[$c_id][0] = 0;
    }

    if (!isset($vendor[$c_id][1])) {
      $vendor[$c_id][1] = 0;
    }

    $k = array_keys ($vendor[$c_id]);

    $perc = parent::calcPercent($vendor[$c_id][$k[0]], $vendor[$c_id][$k[1]]);

		if ($source == 'searchmetrics') {

      $this->countUpDown($vendor[$c_id][$k[0]] , $vendor[$c_id][$k[1]] , $source);
      $out = '<td class="tdright" data-order="'.$vendor[$c_id][$k[0]] .'"><a href="http://suite.searchmetrics.com/de/research/domains/visibility?acc=0&url='.$url.'&cc=DE" target="_blank">'. $vendor[$c_id][$k[0]] .'</a></td>';
      $out .= '<td class="tdright" data-order="'.$vendor[$c_id][$k[1]] .'"><a href="http://suite.searchmetrics.com/de/research/domains/visibility?acc=0&url='.$url.'&cc=DE target="_blank">'. $vendor[$c_id][$k[1]].'</a></td>';
      $out .= '<td class="nowrap">'.$perc.'</td>';

    } else {

      $this->countUpDown($vendor[$c_id][$k[0]] , $vendor[$c_id][$k[1]] , $source);
      $out = '<td class="tdright" data-order="'.$vendor[$c_id][$k[0]] .'"> '. $vendor[$c_id][$k[0]] .' </td>';
      $out .= '<td class="tdright" data-order="'.$vendor[$c_id][$k[1]] .'"> '. $vendor[$c_id][$k[1]].' </td>';
      $out .= '<td class="nowrap">'.$perc.'</td>';
      $out .= '<td class="gray"></td>';
    }

    return $out;

  }


  private function renderVendorDataSistrix ($vendor, $c_id, $url, $country) {

  	$c_id = $this->pureHostName($url) . '_' . $country;

    if (!isset($vendor[$c_id][0])) {
      $vendor[$c_id][0] = 0;
    }

    if (!isset($vendor[$c_id][1])) {
      $vendor[$c_id][1] = 0;
    }

    $k = array_keys ($vendor[$c_id]);

    $perc = parent::calcPercent($vendor[$c_id][$k[0]], $vendor[$c_id][$k[1]]);

    $this->countUpDown($vendor[$c_id][$k[0]] , $vendor[$c_id][$k[1]] , 'sistrix');

    $out = '<td class="tdright" data-order="'.$vendor[$c_id][$k[0]] .'"><a href="https://de.sistrix.com/'.$this->pureHostName($url).'" target="_blank">'. $vendor[$c_id][$k[0]] .'</a></td>';
    $out .= '<td class="tdright" data-order="'.$vendor[$c_id][$k[1]] .'"><a href="https://de.sistrix.com/'.$this->pureHostName($url).'" target="_blank">'. $vendor[$c_id][$k[1]].'</a></td>';
    $out .= '<td class="nowrap">'.$perc.'</td>';
    $out .= '<td class="gray"></td>';


    return $out;

  }


  private function countUpDown($c1, $c2, $source) {

    if (!isset($this->{'up_'.$source})) {$this->{'up_'.$source} = 0;}
    if (!isset($this->{'down_'.$source})) {$this->{'down_'.$source} = 0;}

    if ($c1 > $c2) {
      $this->{'up_'.$source}++;
    }

    if ($c1 < $c2) {
      $this->{'down_'.$source}++;
    }

  }


  private function fetchSistrixData () {

    $sql = "SELECT
              host,
              country,
              score,
              timestamp
            FROM
              sistrix
            WHERE 
            	type = 'desktop'
            ORDER BY
              timestamp DESC";

    $sistrix = $this->db->query($sql);

    while ($row = $sistrix->fetch_assoc()) {

    	$hash = $row['host'] . '_' . $row['country'];

      $this->sistrix[$hash][$row['timestamp']] = $row['score'];
      $this->sistrix_ts[$row['timestamp']] = $row['timestamp'];
    }

  }


  private function fetchSearchmetricsData () {

    $sunday2weeksago = date('Y-m-d',(strtotime('last Sunday -1 week')));

    $sql = "SELECT
              id_customer,
              score,
              timestamp
            FROM
              ruk_project_searchmetrics
            ORDER BY
              timestamp DESC";

    $searchmetrics = $this->db->query($sql);

    while ($row = $searchmetrics->fetch_assoc()) {
      $this->searchmetrics[$row['id_customer']][$row['timestamp']] = $row['score'];
      $this->searchmetrics_ts[$row['timestamp']] = $row['timestamp'];
    }

  }

}

?>
