<?php

require_once('ruk.class.php');

/*

Erzeugt die Tabellen der Keywordsets für die Projektübersicht

*/


class local_summary_setdata extends ruk
{


  public function __construct ($env_data)
  {


    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    $this->keyword_setid = $_POST['setid'];
    $this->project_id    = $_POST['projectid'];
    $this->type          = $_POST['type'];
    $this->keyword       = $_POST['val'];

    if($_POST['val'] == 31) {
      $this->cache_filename = 'local/metrics_local_31_data_'.$this->project_id.'.tmp';
    } else {
      $this->cache_filename = 'local/metrics_local_data_'.$this->project_id.'.tmp';
    }

    $res = $this->sql_cache->readFile($this->cache_filename);

    if (empty($res) || $this->keyword == '0') {
      $this->noDataMessage();
    }

    // TABLE VIEW
    if ($this->type == 'keyword') {
      $this->getRankingData($res);
      $this->renderView();
    } else {
    // GRAPH VIEW
      $this->renderChart($res);
    }

  }


  private function renderChart($res)
  {

    $rs_type = $this->checkCampainTypeLocalComp($this->keyword_setid);

    foreach ($res as $row) {
      $this->rankset['_ts'][$row['t']] = $row['t'];
      // match type
      if ($rs_type[1] != $row['rs']) {
        continue;
      }

      if ($row['t'] === $this->rankset['_ts'][$row['t']]) {
        if ($row['p'] >= 1 && $row['p'] <= 3) {
          $this->summarySetdataMerge($row['t'], 'rank1', 1);
        }
        if ($row['p'] >= 4 && $row['p'] <= 6) {
          $this->summarySetdataMerge($row['t'], 'rank2', 1);
        }
        if ($row['p'] >= 6 && $row['p'] <= 10) {
          $this->summarySetdataMerge($row['t'], 'rank3', 1);
        }
        if ($row['p'] >= 11 && $row['p'] <= 20) {
          $this->summarySetdataMerge($row['t'], 'rank4', 1);
        }
        if ($row['p'] >= 21 && $row['p'] <= 30) {
          $this->summarySetdataMerge($row['t'], 'rank5', 1);
        }
      }

    }


    if (!isset($this->rankset['_ts']) || !isset($this->rankset['_ov'])) {
      $this->noDataMessage();
    }
    $this->rankset['_ts'] = array_reverse($this->rankset['_ts']);

    foreach ($this->rankset['_ts'] as $ts) {
      $timestamp = strtotime($ts);
      if ($this->isWeekend($ts) == true) {
        $this->chartdates[] = '<b>' . $this->germanWDshort($timestamp) . '., ' . date('d.m.', $timestamp) . '</b>';
      } else {
        $this->chartdates[] = $this->germanWDshort($timestamp) . '., ' . date('d.m.', $timestamp);
      }
    }

    foreach ($this->rankset['_ov'] as $ts => $rankings) {
      if (!isset($rankings['rank1'])) {$rank1[] = 0;} else {$rank1[] = $rankings['rank1'];}
      if (!isset($rankings['rank2'])) {$rank2[] = 0;} else {$rank2[] = $rankings['rank2'];}
      if (!isset($rankings['rank3'])) {$rank3[] = 0;} else {$rank3[] = $rankings['rank3'];}
      if (!isset($rankings['rank4'])) {$rank4[] = 0;} else {$rank4[] = $rankings['rank4'];}
      if (!isset($rankings['rank5'])) {$rank5[] = 0;} else {$rank5[] = $rankings['rank5'];}
    }

    $chart = array();
    $chart['ctitle'] =  'Rankingverteilung';
    $chart['categories'] = $this->chartdates;
    $chart['charts'][] = array('name' => 'Position 1-3', 'series' => array_reverse($rank1), 'color' => 'rgb(34,139,34)');
    $chart['charts'][] = array('name' => 'Position 4-6', 'series' => array_reverse($rank2), 'color' => 'rgb(154,205,50)');
    $chart['charts'][] = array('name' => 'Position 7-10', 'series' => array_reverse($rank3), 'color' => 'rgb(238,154,0)');
    $chart['charts'][] = array('name' => 'Position 11-20 ', 'series' => array_reverse($rank4), 'color' => 'rgb(201,105,170)');
    $chart['charts'][] = array('name' => 'Position 21-30', 'series' => array_reverse($rank5), 'color' => 'rgb(205,0,0)');

    echo json_encode($chart);

  }

  private function getRankingData ($res)
  {

    $rs_type = $this->checkCampainTypeLocalComp($this->keyword_setid);

    foreach ($res as $row) {

      // match type
      if ($rs_type[1] != $row['rs']) {
        continue;
      }

      $this->rankset['_ts'][$row['t']] = $row['t'];

      if ($row['k'] == $this->keyword) {

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {

          $this->summarySetdataMerge($row['t'], 'avg', $row['p']);
          $this->summarySetdataMerge($row['t'], 'divider', 1);

          if ($row['p'] >= 1 && $row['p'] <= 10) {
            $this->summarySetdataMerge($row['t'], 'sum', 1);
          }
          if ($row['p'] == 1) {
            $this->summarySetdataMerge($row['t'], 'rank1', 1);
          }
          if ($row['p'] == 2) {
            $this->summarySetdataMerge($row['t'], 'rank2', 1);
          }
          if ($row['p'] == 3) {
            $this->summarySetdataMerge($row['t'], 'rank3', 1);
          }
          if ($row['p'] == 4) {
            $this->summarySetdataMerge($row['t'], 'rank4', 1);
          }
          if ($row['p'] == 5) {
            $this->summarySetdataMerge($row['t'], 'rank5', 1);
          }
          if ($row['p'] >= 6 && $row['p'] <= 10) {
            $this->summarySetdataMerge($row['t'], 'rank10', 1);
          }

        }

      }

    }

    if (!isset($this->rankset['_ov']) || !isset($this->rankset['_ts'])) {
      $this->noDataMessage();
    }

    $this->rankset['_ts'] = array_reverse($this->rankset['_ts']);
    $this->rankset['_ov'] = array_reverse($this->rankset['_ov']);

  }

  private function renderView() {

    if (!isset($this->rankset['_ts'])) {
      $this->noDataMessage();
    }

    $out = '<table class="table table-normal" id="data-table-'.$this->keyword_setid.'"><thead><tr><td>Rankingverteilung für ' . $this->keyword . '</td>';

    $ii = 0;
    // DATES
    foreach ($this->rankset['_ts'] as $ts) {
      $ii++;
      $out .= '<td>'. $this->germanTS($ts) .'</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td>Change</td>';
      }
    }

    $out .= '</tr></thead><tbody>';
    $out .= '<tr><td>Platz 1</td>' . $this->generateCells('rank1') .'</tr>';
    $out .= '<tr><td>Platz 2</td>' . $this->generateCells('rank2') .'</tr>';
    $out .= '<tr><td>Platz 3</td>' . $this->generateCells('rank3') .'</tr>';
    $out .= '<tr><td>Platz 4</td>' . $this->generateCells('rank4') .'</tr>';
    $out .= '<tr><td>Platz 5</td>' . $this->generateCells('rank5') .'</tr>';
    $out .= '<tr><td>Platz 6-10</td>' . $this->generateCells('rank10') . '</tr>';
    $out .= '<tr><td>Summe - Top 10</td>' . $this->generateCellsSUM() . '</tr>';
    $out .= '<tr><td>&#216; - Rankings</td>' . $this->generateCellsAVG() . '</tr>';
    $out .= '</tbody></table>';

    echo $out;

  }

  public function summarySetdataMerge($ts, $type, $add)
  {

    if (isset($this->rankset['_ov'][$ts][$type])) {
      $this->rankset['_ov'][$ts][$type] = $this->rankset['_ov'][$ts][$type] + $add;
    } else {
      $this->rankset['_ov'][$ts][$type] = $add;
    }

  }



  private function generateCellsSUM ()
  {

    $out = '';

    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {

      $ii++;
      if (!isset($this->rankset['_ov'][$ts]['sum'])) {
        $out .= '<td>0</td>';
        if ($ii < count($this->rankset['_ts'])) {
          $out .= '<td></td>';
        }
      }

      if (!isset($this->rankset['_ov'][$ts])) {
        continue;
      }

      foreach ($this->rankset['_ov'][$ts] as $key => $ranks) {

        if ($key == 'sum') {

          $yesterday = key($this->rankset['_ts']);
          next($this->rankset['_ts']);
          $ranktoday = $ranks;

          $change = '';

          if (isset($this->rankset['_ov'][$yesterday]['sum'])) {
            $rankyesterday = $this->rankset['_ov'][$yesterday]['sum'];
            $change =  $this->checkDifferenceBetweenRankingDays($ranktoday, $rankyesterday);
          }

          $out .= '<td>'. $ranktoday . '</td>';
          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change.'</td>';
          }

        }

      }

    }

    return $out;

  }


  private function generateCellsAVG ()
  {

    $out = '';

    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $ii++;
      if (!isset($this->rankset['_ov'][$ts]['avg'])) {
        $out .= '<td>0</td>';
        if ($ii < count($this->rankset['_ts'])) {
          $out .= '<td></td>';
        }
      }

      if (!isset($this->rankset['_ov'][$ts])) {
        continue;
      }

      foreach ($this->rankset['_ov'][$ts] as $key => $ranks) {

        if ($key == 'avg') {

          $yesterday = key($this->rankset['_ts']);
          next($this->rankset['_ts']);
          $ranktoday = round($ranks/$this->rankset['_ov'][$ts]['divider'],0);

          $change = '';
          if (isset($this->rankset['_ov'][$yesterday]['avg']) && isset($this->rankset['_ov'][$yesterday]['divider'])) {
            $rankyesterday = round($this->rankset['_ov'][$yesterday]['avg'] / $this->rankset['_ov'][$yesterday]['divider'] ,0);
            $change =  $this->checkDifferenceBetweenRankingDaysReverse($ranktoday, $rankyesterday);
          }

          $out .= '<td>Rang '. $ranktoday . '</td>';
          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change.'</td>';
          }

        }

      }
    }

    return $out;

  }


  private function generateCells ($rank)
  {

    $out = '';
    $i   = 0;
    $ii  = 0;

    foreach ($this->rankset['_ts'] as $k => $ts) {

      $ii++;

      $yesterday = key($this->rankset['_ts']);
      next($this->rankset['_ts']);

      if ($i < 6) {
        if (!isset($this->rankset['_ov'][$ts][$rank])) {$this->rankset['_ov'][$ts][$rank] = 0;}
        if (!isset($this->rankset['_ov'][$yesterday][$rank])) {$this->rankset['_ov'][$yesterday][$rank] = 0;}
        $change = $this->checkDifferenceBetweenRankingDays($this->rankset['_ov'][$ts][$rank], $this->rankset['_ov'][$yesterday][$rank]);
      } else {
        $change = '';
      }
      $out .= '<td>' . $this->rankset['_ov'][$ts][$rank] . '</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td>'.$change.'</td>';
      }

      $i++;
    }

    return $out;

  }


  private function noDataMessage ()
  {
    echo '<div class="padded">Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br> Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</div>';
    exit;
  }

}

?>
