<?php

require_once('ruk.class.php');

/*

Erzeugt die Detailansicht der Keywords und rankenden URLs
https://datatables.net/examples/plug-ins/dom_sort
*/


class indexwatch_seo2 extends ruk
{


  public function __construct ($env_data)
  {

    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['type'])) {

      $set_id = 1097;

      $this->selectKeywordSet($set_id);
      $this->selectSeoCompanies();

      $this->getRankingData();
      $this->renderRankingViewKeyword();

      $out_arr = array();
      $out_arr['html'] = $this->out;

      echo json_encode($out_arr);

    }

  }


  private function selectKeywordSet ($set_id) {

    $sql = "SELECT
              keyword
            FROM ruk_project_keywords
            WHERE id_kw_set = $set_id";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    $this->keywords = $rows;

  }


  private function selectSeoCompanies () {

    $sql = "SELECT * FROM ruk_indexwatch_seo_companies";


    $sql = "SELECT
              a.url      AS url,
              a.name     AS name,
              a.ort      AS ort,
              a.location AS location,
              b.score    AS score
            FROM ruk_indexwatch_seo_companies a
              LEFT JOIN ruk_indexwatch_seo_companies_sistrix b
                ON b.id_customer = a.id";


    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $hostname = $this->pureHostName($row['url']);
      $rows[$hostname] = array ( $row['url'], $row['name'], $row['score'], $row['ort'], $row['location'] );
    }

    $this->seo_companies         = $rows;
    $this->seo_companies_sistrix = $rows;

  }


  private function getRankingData()
  {

    $rows = parent::loadCacheFileIndexwatch (1, 'seocompany');

    //ADWORDS DATA
    $this->googleadwords = parent::getAdwordsData($this->keywords);

    foreach ($rows as $row) {

      $hostname = $this->pureHostName($row['url']);

      if (isset($this->seo_companies[$hostname])) {

        $dex = $this->calcOneDex($row['position'], $this->googleadwords[$row['keyword']][3]);

        if (isset($this->rankset['_dex'][$hostname])) {
          $this->rankset['_dex'][$hostname] = $this->rankset['_dex'][$hostname] + $dex;
        } else {
          $this->rankset['_dex'][$hostname] = $dex;
        }

      }

    }

  }


  private function renderRankingViewKeyword ()
  {

    foreach ($this->seo_companies as $hostname => $dex) {
      if (isset($this->rankset['_dex'][$hostname])) {
        array_push($this->seo_companies[$hostname], $this->rankset['_dex'][$hostname]);
      } else {
        array_push($this->seo_companies[$hostname], 0);
      }
    }
    // SORT BY DEX
    uasort($this->seo_companies, function($a, $b) {
      return $b[5] > $a[5];
    });
    // SORT BY SISTRIX
    uasort($this->seo_companies_sistrix, function($a, $b) {
      return $b[2] > $a[2];
    });

    $out = '';
    $i = 1;

    $rows_local = parent::loadCacheFileIndexwatch (1, 'seocompany_local');

    foreach ($this->seo_companies as $host => $set) {

      if (isset($rows_local[$set[4]])) {
        $rank = $this->checkForRanking($rows_local[$set[4]], $set[0]);
      }

      $sistrix_rank = array_search($host, array_keys($this->seo_companies_sistrix)) + 1;

      $out .= '<tr>
                 <td class="rank">'.$i++.'</td>
                 <td class="rank">' . $sistrix_rank . '</td>
                 <td class="rank"><a href="'.$set[0].'" target="_blank">'.$set[1].'</a></td>
                 <td class="rank"><a href="'.$set[0].'" target="_blank">'.$set[0].'</a></td>
                 <td class="rank">'.$set[3].'</a></td>
                 '.$rank.'
                 <td class="rank"><a target="_blank" href="/'.$host.'">' . number_format($set[5], 3, ',', '.') . '</a></td>
                 <td class="rank"><a target="_blank" href="https://de.sistrix.com/'.$host.'">' . number_format($set[2], 3, ',', '.') . '</a></td>
                 <td class="rank"><small><a target="_blank" href="/'.$host.'">Rankings anzeigen</a></small></td>
               </tr>';
    }

    $out .= '</tbody></table></div></div></div></div>';

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <div class="right"><span class="sistrix"><a href="http://www.sistrix.com" target="_blank">SISTRIX Index powered by SISTRIX.com</a></span></div>
                 <span class="title cps_green">Indexwatch: SEO Agenturen, Rankings am '. parent::germanTS(parent::dateYMD()).'</span>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table">
                  <thead>
                    <tr>
                      <td style="width:100px;">Rang (DEX)</td><td style="width:100px;">Rang (SISTRIX)</td><td>Agentur</td><td>URL</td><td>Ort</td><td>Rank</td><td>OneDex</td><td>Sistrix</td><td>Rankings</td></thead><tbody>';

    $this->out = $outhead . $out;

  }

  private function checkForRanking ($rankset, $url) {

    foreach ($rankset as $local_id => $rankings) {
      if ($this->pureHostName($rankings['u']) === $this->pureHostName($url)){
        return $rank = '<td class="rank" data-sort="'.$rankings['p'].'"><a href="' . $rankings['u'] .'" target="_blank">' . $rankings['p'] . '</a></td>';
      }
    }

   return '<td class="rank" data-sort="101">-</td>';

  }

}

?>
