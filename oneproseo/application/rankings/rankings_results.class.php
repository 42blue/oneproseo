<?php

require_once('ruk.class.php');

class rankings_results extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if(!isset($_POST['kw']))
    {
      echo 'NO KEYWORD SET';
      exit;
    }

    $this->keyword  = urldecode($_POST['kw']);
    $this->date     = urldecode($_POST['ts']);
    $this->country  = urldecode($_POST['co']);
    $this->location = urldecode($_POST['loc']);
    $this->rs_type  = urldecode($_POST['type']);

    if(!isset($_POST['ts']))
    {
      $this->date = parent::dateYMD();
    }

    if(!isset($_POST['co'])) {
      $this->country = 'de';
    }

    if(!empty($_POST['loc'])) {
      $this->location = $_POST['loc'];
      $this->rs_locations  = $this->getLocationsArray();
      $this->getRankingDataLocal();
    } else {
      $this->getRankingData();
    }

      $this->renderView();
  }


  private function renderView ()
  {

    echo '
          <div class="box">
            <div class="box-header">
              <span class="titleleft">Rankings für das Keyword <span class="red">'.$this->keyword.'</span> am <span class="red">'.parent::germanTS($this->date).'</span>';


    if(!empty($_POST['loc'])) {
      echo ' am Ort <span class="red">' . $this->rs_locations[$this->location] . ' </span> in <span class="red">' . $this->rsTypeLocal($this->rs_type) . '</span>';
    } else {
      if (stripos($this->country, 'mobile') !== false) {
        $mo_type = 'Mobile';
      } elseif (stripos($this->country, 'maps') !== false) {
        $mo_type = 'Maps';
      } else {
        $mo_type = 'Desktop';
      }
      echo ' in <span class="red">Google ' . $mo_type . '</span>';
    }

    echo '</span>
            </div>
            <div class="box-content">
              <table class="table table-normal data-table"><thead><tr><td>Rang</td><td>URL</td></thead><tbody>';

    echo $this->out_view;

    echo '</tbody></table>
            </div>
          </div>
         ';

  }



  private function getRankingData()
  {

    $sql = "SELECT
              DISTINCT keyword,
              position,
              timestamp,
              url,
              description
            FROM
              ruk_scrape_keywords a
            LEFT JOIN ruk_scrape_rankings b
              ON b.id_kw = a.id
            WHERE BINARY
              keyword = '$this->keyword'
            AND
              language ='$this->country'
            AND
              timestamp = '$this->date'";

    $result = $this->db->query($sql);

    if ($result->num_rows < 1 ) {

      $sql = "SELECT
                DISTINCT keyword,
                position,
                timestamp,
                url,
                description                
              FROM
                structure_scrape_keywords a
              LEFT JOIN structure_scrape_rankings b
                ON b.id_kw = a.id
              WHERE BINARY
                keyword = '$this->keyword'
              AND
                language ='$this->country'";

      $result = $this->db->query($sql);

    }

    if (!$result) {
      $this->mySqlQueryError();
    } else {
      while ($row = $result->fetch_assoc()) {
        $featured = '';
        if ($row['description'] == '1') {
          $featured = ' <span style="color:red;">(featured)</span> ';
        }        
       $this->out_view .= '<tr><td>' . $row['position'] . ' </td><td><a href="' . $row['url'] . '" target="_blank"> ' . $row['url'] . '</a>'.$featured.'</td>';
      }
    }

  }

  private function getRankingDataLocal()
  {

    $sql = "SELECT
              DISTINCT keyword,
              position,
              timestamp,
              url
            FROM
              ruk_scrape_keywords_local a
            LEFT JOIN ruk_scrape_rankings_local b
              ON b.id_kw = a.id
            WHERE
              keyword = '$this->keyword'
            AND
              language ='$this->country'
            AND
              location ='$this->location'
            AND
              rs_type = '$this->rs_type'
            AND
              timestamp = '$this->date'";

    $result = $this->db->query($sql);

    if (!$result) {
      $this->mySqlQueryError();
    } else {
      while ($row = $result->fetch_assoc()) {
       $this->out_view .= '<tr><td>' . $row['position'] . ' </td><td><a href="' . $row['url'] . '" target="_blank"> ' . $row['url'] . '</a></td>';
      }
    }

  }

}

?>
