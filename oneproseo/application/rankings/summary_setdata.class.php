<?php

require_once('ruk.class.php');

/*

Erzeugt die Tabellen der Keywordsets für die Projektübersicht

*/


class summary_setdata extends ruk
{


  public function __construct ($env_data)
  {


    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['setid'])) {

      $this->keyword_setid = $_POST['setid'];
      $this->project_id    = $_POST['projectid'];
      $this->type          = $_POST['type'];

      if ($this->type == 'local') {
        $this->cache_filename = 'local/charts/setdata_'  . $this->project_id . '_' . $this->keyword_setid . '.tmp';
        $res = $this->sql_cache->readFileRaw($this->cache_filename);
      } else {
        $this->cache_filename = 'charts/setdata_'  . $this->project_id . '_' . $this->keyword_setid . '.tmp';
        $res = $this->sql_cache->readFileRaw($this->cache_filename);
      }

      // WEEKLY CHART HACK
      if (empty($res) || stripos($res, 'keine Rankings') !== false) {
        $this->cache_filename = 'charts/setdata_'  . $this->project_id . '_' . $this->keyword_setid . '_weekly.tmp';
        $res = $this->sql_cache->readFileRaw($this->cache_filename);
      }

      if (!empty($res)) {
        echo $res;
      } else {
        echo '<div class="padded">Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br> Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</div>';
      }

    }

  }

}

?>
