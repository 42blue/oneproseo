<?php

require_once('ruk.class.php');

class rankings_winner_looser extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    $this->project_id    = $_GET['projectid'];

    if (!isset($_GET['days'])) {
      $this->days = 6;
    } else {
      $this->days = $_GET['days'];
    }

    $this->cache_filename = 'wl/wl_'.$this->project_id.'_'.$this->days.'.tmp';
    $res = $this->sql_cache->readFileRaw($this->cache_filename);

    // WEEKLY FALLBACK
    if (empty($res)) {
      $this->cache_filename = 'wl/wl_'.$this->project_id.'_'.$this->days.'_weekly.tmp';
      $res = $this->sql_cache->readFileRaw($this->cache_filename);
    }

    if (!empty($res)) {
      echo $res;
    } else {
      echo '<div class="box-content padded"> Für die Keywords wurde heute noch keine Gewinner / Verlierer Liste erstellt.<br />Es kann bis zu 24 Stunden dauern bis die Liste erstellt ist.</div>';
    }

  }

}

?>
