<?php

require_once('ruk.class.php');

/*

Erzeugt die Detailansicht der Keywords und rankenden URLs
https://datatables.net/examples/plug-ins/dom_sort

*/


class rankings_top_rankings extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    $this->data = parent::fetchTopRankingsOverall();

    $this->renderView();

    echo $this->out;


  }



  private function renderView ()
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'Rank', 'OneDex', 'OPI', 'Rank vor 6 Monaten', 'Projekt', 'Suchvolumen', 'URL', 'Datum');

    $i=0;
    foreach ($this->data as $id => $set) {

      $set_id   = $id;
      $set_name = $set[1];
      $set_lang = $set[2];

      foreach ($set[3] as $key => $value) {

        $keyword  = $value[0];
        $sv       = $value[1];
        $opi      = $value[2];
        $rank_now = $value[3];
        $url_now  = $value[4];
        $ts_now   = $value[5];
        $rank_old = $value[6];
        $url_old  = $value[7];
        $ts_old   = $value[8];
        $onedex   = parent::calcOneDex($rank_now, $opi);

        $this->csvarr[] = array($i++, $keyword, $rank_now, parent::nufo($onedex), parent::nufo($opi), $rank_old, $set_name, $sv, $url_now, $ts_now);

        $out .= '<tr>';
        $out .= '<td></td>';
        $out .= '<td><a title="Dieses Keyword abfragen" target="_blank" href="../rankings/overview/'.$set_id.'/keywords/details/'.urlencode($keyword).'/'.$set_lang.'"><i class="icon-comment right"></i></a>' .  $keyword . '</a></td>';
        $out .= '<td><small class="rankings"><a target="_blank" href="'.parent::googleSearchDomain($set_lang).'&q='.$keyword.'">SERP</a> | <a target="_blank" href="https://trends.google.de/trends/explore?q='.$keyword.'&geo='.strtoupper($set_lang).'">TRENDS</a></small>' .  $rank_now . '</td>';
        $out .= '<td data-order="'.$onedex.'">' . parent::nufo($onedex) . '</td>';
        $out .= '<td data-order="'.$opi.'">' . parent::nufo($opi) . '</td>';
        $out .= '<td>' .  $rank_old . '</td>';        
        $out .= '<td><a target="_blank" href="../rankings/overview/'.$set_id.'">' .  $set_name . '</a></td>';
        $out .= '<td data-order="'.$sv.'">' .  number_format($sv, 0, ',', '.') . '</td>';
        $out .= '<td><a href="'.$url_now.' target="_blank">' . $url_now . '</a></td>';
        $out .= '<td>' .  parent::germanTS($ts_now) . '</td>';        
        $out .= '</tr>';

      }

    }


    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_top_rankings');

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Top 10 Rankings der letzten 7 Tage (Suchvolumen > 1000)</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td>
                      <td>Keyword</td>
                      <td>Rank</td>
                      <td>OneDex</td>
                      <td>OPI</td>
                      <td>Rank (vor 6 Monaten)</td>
                      <td>Projekt</td>
                      <td>Suchvolumen</td>
                      <td>URL</td>
                      <td>Datum</td>
                    </tr>
                  </thead>
                  <tbody>';

    $this->out = $outhead . $out;

  }

}

?>
