<?php

require_once(dirname(__FILE__) .' /../ruk.class.php');

include('/var/www/oneproapi/searchconsole/api/GoogleSearchConsoleAPI.class.php');

class cf_dash_searchconsole extends ruk {

  public function __construct ($env_data, $hostname)
  {

    $this->env_data = $env_data;

    $this->host = $hostname;

    $this->googleAuth();

  }


  private function googleAuth () {

    $this->sc = $this->auth();
    $auth     = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $this->accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'No auth token created!';
    }

    $this->sc->setAccessToken($this->accessToken);
    $profiles = $this->sc->getProfiles();

    $this->account = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }
      if (stripos($item['siteUrl'], $this->host) !== FALSE) {
        $this->accounturl = $item['siteUrl'];
      }

    }

  }

  public function getDataAPISearchAnalytics () {

		if (!$this->accessToken) {
			throw new Exception('You must provide the accessToken');
		}

		$url       = $this->accounturl;
		$startDate = date('Y-m-d', strtotime("-3 months"));
		$endDate 	 = date('Y-m-d', strtotime("-1 day"));

		$params = array(
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array ('query'),
			'rowLimit'   => 5000
		);

		$_params = array('access_token' => $this->accessToken);
		$url     = 'https://www.googleapis.com/webmasters/v3/sites/'.urlencode($url).'/searchAnalytics/query';
		$data    = $this->curlPost($url, $_params, $params);

		$this->dataset = json_decode($data, true);

	}


	public function getDataAPIError() {

		if (!$this->accessToken) {
			throw new Exception('You must provide the accessToken');
		}

		$url       = $this->accounturl;
		$startDate = date('Y-m-d', strtotime("-3 months"));
		$endDate 	 = date('Y-m-d', strtotime("-1 day"));

		$params = array(
			'category'  			 => 'notFound',
			'platform'   			 => 'web',
			'latestCountsOnly' => 'false',
		);

		$_params = array('access_token' => $this->accessToken);
		$url     = 'https://www.googleapis.com/webmasters/v3/sites/'.urlencode($url).'/urlCrawlErrorsCounts/query';
		$data    = $this->curlGet($url, $_params, $params);

		return json_decode($data, true);

	}

	public function getDataAPISoft404() {

		if (!$this->accessToken) {
			throw new Exception('You must provide the accessToken');
		}

		$url       = $this->accounturl;
		$startDate = date('Y-m-d', strtotime("-3 months"));
		$endDate 	 = date('Y-m-d', strtotime("-1 day"));

		$params = array(
			'category'  			 => 'soft404',
			'platform'   			 => 'web',
			'latestCountsOnly' => 'false',
		);

		$_params = array('access_token' => $this->accessToken);
		$url     = 'https://www.googleapis.com/webmasters/v3/sites/'.urlencode($url).'/urlCrawlErrorsCounts/query';
		$data    = $this->curlGet($url, $_params, $params);

		return json_decode($data, true);

	}


  public function calcCtr () {

    $this->ctr = array ();

    if (isset($this->dataset['rows'])) {

      foreach ($this->dataset['rows'] as $key => $value) {

        $pos  = (int) $value['position'];
        $ctr  = round ($value['ctr'] * 100, 0);

        if ($ctr == 0) {
          continue;
        }

        if (isset($this->ctr[$pos])) {
          $this->ctr[$pos]         = $this->ctr[$pos] + $ctr;
          $this->ctr_divider[$pos] = $this->ctr_divider[$pos] + 1;
        } else {
          $this->ctr[$pos]         = $ctr;
          $this->ctr_divider[$pos] = 1;
        }

      }


    foreach ($this->ctr as $key => $value) {
      $this->ctr_out[$key] = $this->ctr[$key] / $this->ctr_divider[$key];
    }

     ksort($this->ctr_out);

    }

  }


  public function getData () {

    return $this->ctr_out;

  }


	private function curlPost ($url, $get = array(), $post = array()) {

		if (empty($url)) return false;

	  //$url = $url . "?" . http_build_query($get);

		$requestHeaders = array ();
		$requestHeaders['Content-Type']              = 'application/json';
		$requestHeaders['Content-Transfer-Encoding'] = 'binary';
		$requestHeaders['MIME-Version']              = '1.0';
		$requestHeaders['Authorization']             = 'Bearer ' . $get['access_token'];

    foreach ($requestHeaders as $k => $v) {
      $curlHeaders[] = "$k: $v";
    }

		$curl = curl_init($url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $curlHeaders);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($post));
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		$data = curl_exec($curl);

		$http_code = (int) curl_getinfo($curl, CURLINFO_HTTP_CODE);

		// Add the status code to the json data, useful for error-checking
		$data = preg_replace('/^{/', '{"http_code":'.$http_code.',', $data);
		curl_close($curl);

		return $data;

	}


	private function curlGet ($url, $params2, $params = array()) {

		if (empty($url)) return false;

		if (!empty($params)) {
			$url = $url . "?" . http_build_query($params);
		}

		$curl = curl_init($url);

		$requestHeaders = array ();
		$requestHeaders['Content-Type']              = 'application/json';
		$requestHeaders['Content-Transfer-Encoding'] = 'binary';
		$requestHeaders['MIME-Version']              = '1.0';
		$requestHeaders['Authorization']             = 'Bearer ' . $params2['access_token'];

		foreach ($requestHeaders as $k => $v) {
			$curlHeaders[] = "$k: $v";
		}


		curl_setopt($curl, CURLOPT_HTTPHEADER, $curlHeaders);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$data = curl_exec($curl);

		$http_code = (int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
		// Add the status code to the json data, useful for error-checking
		$data = preg_replace('/^{/', '{"http_code":'.$http_code.',', $data);
		curl_close($curl);


		return $data;

	}



  public function auth () {

    $ga = new GoogleSearchConsoleAPI('service');
    $this->ga = $ga;
    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }

}

?>
