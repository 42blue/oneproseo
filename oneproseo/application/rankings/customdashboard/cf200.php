<?php

require_once('cf_dash_googlesearchconsole.class.php');

class cf_custom extends ruk {

  public function __construct ($env_data, $id) {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->env_data['customer_id'] = $id;

    $this->fetchCustomer();

    // SET CUSTOM CUSTOMER AND KEYWORDSET
    $this->url           = $this->customer['url'];
    $this->hostname      = parent::pureHostName($this->url);
    $this->customer_id   = $this->env_data['customer_id'];
    $this->keywordset_id = $this->customer['customer_keywordset_id'];

    $this->env_data = $env_data;

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    // CACHE
    require_once($env_data['path'] . 'oneproseo/includes/cache.class.php');
    $this->cache = new Cache();
    $this->cache->setEnv($env_data);
    $data = $this->cache->readFile('cache_cv'.$this->env_data['customer_id'].'.tmp', 23);
    if (!empty ($data)) {
      echo json_encode($data);
      exit;
    }
    // CACHE

    $this->google_searchconsole_data = new cf_dash_searchconsole($env_data, $this->hostname);

    // CUSTOMER DATA AND DEX
    $this->calcDex();

    // KEYWORD RANKINGS
    $this->fetchKeywordData();
    $this->getRankingData();

    // SISTRIX
    $this->fetchSistrixData();
    $this->fetchSistrixDataMobile();

    // OVERALL RANKINGS
    $this->getOverallRankingData();

    // SEARCH CONSOLE
    $this->getFigures();

    // JSON OUTPUT
    $this->appHandler();

    parent::mySqlClose();

  }


  private function fetchCustomer()
  {

    $sql = "SELECT * FROM ruk_project_customers WHERE id = '".$this->env_data['customer_id']."'";

    $result = $this->db->query($sql);

    $this->keywords = array();
    while ($row = $result->fetch_assoc()) {
      $this->customer = $row;
    }

    $loc = $this->getAdwordsLoc($this->customer['country']);
    $this->adwords_country  = $loc[0];
    $this->adwords_language = $loc[1];

  }


  private function calcDex()
  {

    $rankset = parent::fetchOneDexData();

    foreach ($rankset[$this->customer_id] as $ts => $value) {
      if(empty($value['dex'])) {
        continue;
      }
      $this->dex_chart[] = $value['dex'];
    }

    krsort($this->dex_chart);

    $this->dex_chart = array_values($this->dex_chart);

    $dex_today = end($this->dex_chart);
    $dex_yesterday = prev ($this->dex_chart);
    $perc = 0;

    if ($dex_today != 0 && $dex_yesterday != 0) {
      $perc = $dex_today * 100 / $dex_yesterday;
      $perc = $perc - 100;
      $perc = round ($perc, 2);
    }

    if ($dex_today < $dex_yesterday) {
      $this->dex_view = '<div class="metric"><small>OneDex:</small> ' . number_format($dex_today, 0, ',', '.') . '</div><div class="change m-red metric-small"><div class="arrow-down"></div><span class="large">' . number_format($perc, 2, ',', '.') . '%</span></div>';
    } else {
      $this->dex_view = '<div class="metric"><small>OneDex:</small> ' . number_format($dex_today, 0, ',', '.') . '</div><div class="change m-green metric-small"><div class="arrow-up"></div><span class="large">' . number_format($perc, 2, ',', '.') . '%</span></div>';
    }

  }


  private function fetchKeywordData ()
  {

    // get project_keyword_sets with keywordset_id ID
    // get all keywords

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id = '".$this->keywordset_id."'";

    $result = $this->db->query($sql);

    $this->keywords = array();
    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function getRankingData()
  {

    $this->adwordsData = $this->getAdwordsData($this->keywords, $this->adwords_country, $this->adwords_language);

    // compare KEYWORDS TO CRAWL vs. SCRAPED KEYWORDS
    $comma_separated_kws = implode('", "', $this->keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    // get all keyword rankings
    $d   = strtotime('Monday this week');
    $w0  = date('Y-m-d', $d);
    $w1  = date('Y-m-d', strtotime('-1 week', $d));

    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND (timestamp = '$w0' OR timestamp = '$w1')
            ORDER BY timestamp DESC";
            
    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[] = $row;
    }

    $rankset = array();
    $keywords_flip = array_flip($this->keywords);

    foreach ($rows as $k => $v) {

      $idkw = $v['id'];

      $sql = "SELECT
                position,
                url,
                id_kw,
                hostname
              FROM
                ruk_scrape_rankings
              WHERE
                id_kw = '$idkw'";

      $result2 = $this->db->query($sql);

      while ($row = $result2->fetch_assoc()) {

        if ($row['hostname'] == $this->hostname) {

         if (isset($keywords_flip[$v['keyword']])) {

            // get all measured days
            $rankset['_ts'][$v['timestamp']] = $v['timestamp'];
            $rankset['_da'][$v['keyword']][$v['timestamp']][] = array ('rank' => $row['position'], 'ts' => $v['timestamp'], 'url' => $row['url']);

          }

        }

      }

    }

    // merge ranking result with keywordset for keywords that do not rank
    foreach ($this->keywords as $keyword) {
      if (!isset($rankset['_da'][$keyword])) {
         $rankset['_da'][$keyword] = '';
      }
    }

    $this->keyword_results = $this->renderKeywords($rankset['_da'], $rankset['_ts']);

  }


  private function renderKeywords($set, $dates)
  {

      /*
          array(9) {
            ["urlaub buchen"]=>
            array(2) {
              ["2014-11-17"]=>
              array(1) {
                [0]=>
                array(3) {
                  ["rank"]=>
                  string(1) "7"
                  ["ts"]=>
                  string(10) "2014-11-17"
                  ["url"]=>
                  string(18) "http://www.weg.de/"
                }
              }
              ["2014-11-16"]=>
              array(1) {
                [0]=>
                array(3) {
                  ["rank"]=>
                  string(1) "6"
                  ["ts"]=>
                  string(10) "2014-11-16"
                  ["url"]=>
                  string(18) "http://www.weg.de/"
                }
              }
            }
      */

    $out_g = '';
    $out_r = '';
    $out_n = '';
    $out_x = '';

    $dates = array_keys($dates);

    foreach ($set as $keyword => $values) {

      if (empty($values)) {
        $out_x .= '<tr><td class="kw">' . $keyword . '</td><td class="fig">N/A</td><td class="fig">N/A</td><td><div class="change metric-small right"><span class="large"> - </span></div></td><td><div class="change metric-small right"><span class="large"> - </span></div></td></tr>';
        continue;
      }

      if (!isset($values[$dates[1]][0]['rank'])) {
        $values[$dates[1]][0]['rank'] = 101;
      };

      if (!isset($values[$dates[0]][0]['rank'])) {
        $values[$dates[0]][0]['rank'] = 101;
      }

      $change = $values[$dates[1]][0]['rank'] - $values[$dates[0]][0]['rank'];

      $dex = $this->calcOneDex($values[$dates[0]][0]['rank'], $this->adwordsData[$keyword][3]);

      if ($values[$dates[0]][0]['rank'] > $values[$dates[1]][0]['rank']) {
        $out_sort[] = array ('dex' => $dex,  'val' => '<tr><td class="kw">'.$keyword.'</td><td class="fig"><small>' . $dex . '</small></td><td class="fig"><a class="white" href="'.$values[$dates[0]][0]['url'].'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td><td><div class="change m-red metric-small right"><span>'.$change.'</span></div></td><td><div class="change m-red metric-small right"><div class="arrow-down"></div></div></td></tr>');
      } else if ($values[$dates[0]][0]['rank'] < $values[$dates[1]][0]['rank']) {
        $out_sort[] = array ('dex' => $dex,  'val' => '<tr><td class="kw">'.$keyword.'</td><td class="fig"><small>' . $dex . '</small></td><td class="fig"><a class="white" href="'.$values[$dates[0]][0]['url'].'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td><td><div class="change m-green metric-small right"><span>'.$change.'</span></div></td><td><div class="change m-green metric-small right"><div class="arrow-up"></div></div></td></tr>');
      } else {
        $out_sort[] = array ('dex' => $dex,  'val' => '<tr><td class="kw">'.$keyword.'</td><td class="fig"><small>' . $dex . '</small></td><td class="fig"><a class="white" href="'.$values[$dates[0]][0]['url'].'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td><td><div class="change m-green metric-small right"><span> 0 </span></div></td><td><div class="change m-green metric-small right"><div class="arrow-dash"></div></div></td></tr>');
      }

    }

    // SORT DEX
    $s_dex = array();
    foreach ($out_sort as $key => $row)
    {
      $s_dex[$key] = $row['dex'];
    }

    array_multisort($s_dex, SORT_DESC, $out_sort);

    $out = '';
    foreach ($out_sort as $key => $value) {
      $out .= $value['val'];
    }

    return '<table class="cf-table">' . $out . $out_x . '</table>';

  }


  private function fetchSistrixData () {

    $host    = $this->hostname;
    $country = $this->customer['country'];

    $sql = "SELECT
              host,
              country,
              type,
              score,
              timestamp
            FROM
              sistrix
            WHERE
              host = '$host'
            AND 
              country = '$country'
            AND 
              type = 'desktop'
            ORDER BY
              timestamp DESC";

    $data = $this->db->query($sql);

    while ($row = $data->fetch_assoc()) {
      $sistrix[$row['timestamp']] = $row['score'];
      $sistrix_ts[] = $row['timestamp'];
    }

    // get only last seven values
    $sistrix = array_slice($sistrix, 0, 7);

    $thisweek = $sistrix[$sistrix_ts[0]];
    $lastweek = $sistrix[$sistrix_ts[1]];

    if ($thisweek != 0 && $lastweek != 0) {
      $perc = $thisweek * 100 / $lastweek;
      $perc = $perc - 100;
      $perc = round ($perc, 2);
    }

    // chart
    foreach ($sistrix as $ts => $value) {
      $this->sistrix_chart[] = $value;
    }
    krsort($this->sistrix_chart);
    $this->sistrix_chart = array_values($this->sistrix_chart);

    if ($thisweek < $lastweek) {
      $this->sistrix_view = '<div class="metric"><small>Sistrix:</small> ' . number_format($thisweek, 2, ',', '.') . '</div><div class="change m-red metric-small"><div class="arrow-down"></div><span class="large">' . number_format($perc, 2, ',', '.') . '%</span></div>';
    } else if ($thisweek > $lastweek) {
      $this->sistrix_view = '<div class="metric"><small>Sistrix:</small> ' . number_format($thisweek, 2, ',', '.') . '</div><div class="change m-green metric-small"><div class="arrow-up"></div><span class="large">' . number_format($perc, 2, ',', '.') . '%</span></div>';
    } else {
      $this->sistrix_view = '<div class="metric"><small>Sistrix:</small> ' . number_format($thisweek, 2, ',', '.') . '</div><div class="change metric-small"></div>';
    }

  }

  private function fetchSistrixDataMobile () {

    $host    = $this->hostname;
    $country = $this->customer['country'];

    $sql = "SELECT
              host,
              country,
              type,
              score,
              timestamp
            FROM
              sistrix
            WHERE
              host = '$host'
            AND 
              country = '$country'
            AND 
              type = 'mobile'
            ORDER BY
              timestamp DESC";

    $data = $this->db->query($sql);

    while ($row = $data->fetch_assoc()) {
      $sistrix[$row['timestamp']] = $row['score'];
      $sistrix_ts[] = $row['timestamp'];
    }

    // get only last seven values
    $sistrix = array_slice($sistrix, 0, 7);

    $thisweek = $sistrix[$sistrix_ts[0]];
    $lastweek = $sistrix[$sistrix_ts[1]];

    if ($thisweek != 0 && $lastweek != 0) {
      $perc = $thisweek * 100 / $lastweek;
      $perc = $perc - 100;
      $perc = round ($perc, 2);
    }

    // chart
    foreach ($sistrix as $ts => $value) {
      $this->sistrix_chart_mobile[] = $value;
    }
    krsort($this->sistrix_chart_mobile);
    $this->sistrix_chart_mobile = array_values($this->sistrix_chart_mobile);

    if ($thisweek < $lastweek) {
      $this->sistrix_view_mobile = '<div class="metric"><small>Sistrix:</small> ' . number_format($thisweek, 2, ',', '.') . '</div><div class="change m-red metric-small"><div class="arrow-down"></div><span class="large">' . number_format($perc, 2, ',', '.') . '%</span></div>';
    } else if ($thisweek > $lastweek) {
      $this->sistrix_view_mobile = '<div class="metric"><small>Sistrix:</small> ' . number_format($thisweek, 2, ',', '.') . '</div><div class="change m-green metric-small"><div class="arrow-up"></div><span class="large">' . number_format($perc, 2, ',', '.') . '%</span></div>';
    } else {
      $this->sistrix_view_mobile = '<div class="metric"><small>Sistrix:</small> ' . number_format($thisweek, 2, ',', '.') . '</div><div class="change metric-small"></div>';
    }

  }

  private function getOverallRankingData()
  {

    // get project_keyword_sets with project ID
    // get all keywords

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = '".$this->customer_id."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (!empty($row['keyword'])) {
        $this->keywords[] = $row['keyword'];
      }
    }

    // flip for comparison
    $keywords_flip = array_flip($this->keywords);

    $rows = parent::loadCacheFile($this->customer_id);

    foreach ($rows as $row) {

      if (isset($keywords_flip[$row['k']])) {

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {
          if ($row['p'] <= 5) {
           $this->rankset['_ov'][$row['t']]['rank1'] = $this->rankset['_ov'][$row['t']]['rank1'] + 1;
          }
          if ($row['p'] >= 6 && $row['p'] <= 10) {
           $this->rankset['_ov'][$row['t']]['rank2'] = $this->rankset['_ov'][$row['t']]['rank2'] + 1;
          }
          if ($row['p'] >11) {
           $this->rankset['_ov'][$row['t']]['rank3'] = $this->rankset['_ov'][$row['t']]['rank3'] + 1;
          }
        }

      }

    }

    $firstdate = reset($this->rankset['_ts']);
    $lastdate = end($this->rankset['_ts']);

    ksort($this->rankset['_ov'][$firstdate]);
    ksort($this->rankset['_ov'][$lastdate]);

    $this->rankings_today = array_values($this->rankset['_ov'][$firstdate]);
    $this->rankings_week = array_values($this->rankset['_ov'][$lastdate]);

  }


  private function getFigures ()
  {

    $this->google_searchconsole_data->getDataAPISearchAnalytics();
    $this->google_searchconsole_data->calcCtr();

    $data = $this->google_searchconsole_data->getData();

    $this->searchconsole = '
    	<div class="cf-funnel" id="cf-funnel-1">
    		<div class="cf-funnels cf-funnels-left">
    			<ul>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[1], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[2], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[3], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[4], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[5], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[6], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[7], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[8], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[9], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[10], 2).'%; opacity:0.6"></li>
    				<li class="m-green mb" style="height: 9.090909090909092%; width:'.round($data[11], 2).'%; opacity:0.6"></li>
    			</ul>
    		</div>
    		<div class="cf-figstxts">
    			<ul>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[1], 2).'%</p>
    						<p class="small">Platz 1</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[2], 2).'%</p>
    						<p class="small">Platz 2</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[3], 2).'%</p>
    						<p class="small">Platz 3</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[4], 2).'%</p>
    						<p class="small">Platz 4</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[5], 2).'%</p>
    						<p class="small">Platz 5</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[6], 2).'%</p>
    						<p class="small">Platz 6</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[7], 2).'%</p>
    						<p class="small">Platz 7</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[8], 2).'%</p>
    						<p class="small">Platz 8</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[9], 2).'%</p>
    						<p class="small">Platz 9</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[10], 2).'%</p>
    						<p class="small">Platz 10</p>
    					</div>
    				</li>
    				<li style="height: 9.090909090909092%;">
    					<div>
    						<p class="metric m-green mc" style="opacity:1">'.round($data[11], 2).'%</p>
    						<p class="small">Platz 11</p>
    					</div>
    				</li>
    			</ul>
    		</div>
    	</div>
    ';
  }


  private function appHandler ()
  {

    $out = array(
            'searchconsole'         => $this->searchconsole,
            'dex_view'              => $this->dex_view,
            'dex_chart'             => $this->dex_chart,
            'rankings_today'        => $this->rankings_today,
            'rankings_week'         => $this->rankings_week,
            'sistrix_view'          => $this->sistrix_view,
            'sistrix_chart'         => $this->sistrix_chart,
            'sistrix_view_mobile'   => $this->sistrix_view_mobile,
            'sistrix_chart_mobile'  => $this->sistrix_chart_mobile,
            'keywords'              => $this->keyword_results
          );

    $this->cache->writeFile('cache_cv'.$this->env_data['customer_id'].'.tmp', $out);

    echo json_encode($out);

  }


}

?>
