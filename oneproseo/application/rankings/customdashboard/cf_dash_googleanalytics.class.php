<?php

/*
Entwicklung der Besucherzahlen (SEO-Traffic) im Vergleich zur Vorwoche – wenn es möglich ist Kalenderwochen-Abständen
-> ANALYTICS

Gesamtumsatz nach Conversions – Vergleich zur Vorwoche – wenn es möglich ist Kalenderwochen-Abständen
-> ANALYTICS

Ladezeit im Durchschnitt (Website gesamt)
-> ANALYTICS

Bounce-Rate & Verweildauer (TOP Schlechteste Seiten)
-> ANALYTICS



*/

require_once(dirname(__FILE__) .' /../ruk.class.php');

include('/var/www/oneproapi/analytics_v3/api/GoogleAnalyticsAPI.class.php');

class cf_dash_googleanalytics extends ruk {


  public function __construct ($ga)
  {

    $this->ga  = $this->auth();
    $auth      = $this->ga->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      $this->data = false;
    }

    $this->ga_account_id = $ga;

    $this->ga->setAccessToken($accessToken);
    $this->ga->setAccountId($this->ga_account_id);

  }


  public function buildQueryTraffic ()
  {

    $enddate   = date('Y-m-d', strtotime("-1 day"));
    $startdate = date('Y-m-d', strtotime("-61 day"));

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate
    );

    $this->ga->setDefaultQueryParams($this->selected_date);

    $params = array(
      'dimensions'  => 'ga:week, ga:year',
      'metrics'     => 'ga:sessions, ga:pageviews',
      'segment'     => 'gaid::-5',
      'max-results' => 100
    );

    $visits = $this->ga->query($params);

    if (is_null($visits['rows'])) {

      $this->dataTraffic = false;

    } else {

      foreach ($visits['rows'] as $key => $arr) {
        $result[$arr[1].$arr[0]] = array($arr[2], $arr[3]);
      }

    }

    $this->dataTraffic = $result;

  }



  public function buildQueryRevenue ()
  {

    $enddate   = date('Y-m-d', strtotime("-1 day"));
    $startdate = date('Y-m-d', strtotime("-61 day"));

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate
    );

    $this->ga->setDefaultQueryParams($this->selected_date);

    $params = array(
      'dimensions'  => 'ga:week, ga:year',
      'metrics'     => 'ga:sessions, ga:TransactionRevenue',
      'max-results' => 100
    );

    $visits = $this->ga->query($params);

    if (is_null($visits['rows'])) {

      $this->dataRevenue = false;

    } else {

      foreach ($visits['rows'] as $key => $arr) {
        $result[$arr[1].$arr[0]] = array($arr[2], $arr[3]);
      }

    }

    $this->dataRevenue = $result;

  }


  public function getDataTraffic ()
  {
    return $this->dataTraffic;
  }

  public function getDataRevenue ()
  {
    return $this->dataRevenue;
  }


  private function auth ()
  {

    $ga = new GoogleAnalyticsAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/analytics_v3/api/key.p12');

    return $ga;

  }


}

?>
