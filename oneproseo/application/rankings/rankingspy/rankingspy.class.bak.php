<?php

/*

 Doku:
 http://www.ranking-spy.com/ranking/webserviceapi/doc/index.html?com/atenis/rankingspy/web/webservice/RankingSpyService.html

 Active Licence Types:

  https://oneproseo.advertising.de/oneproapi/rankingspy/getAvailableLicenceTypes.php

  Google.de wöchentlich - 17702 - google.de Interval: weekly
  Google.de wöchentlich - (mobile) - 17736 - google.de mobile Interval: weekly
  Google.de wöchentlich - (maps) - 17741 - google.de Maps Interval: weekly
  Google.de täglich - 17703 - google.de Interval: daily
  Google.de täglich - (mobile) - 17719 - google.de mobile Interval: daily
  Google.de täglich - (maps) - 17723 - google.de Maps Interval: daily

  Google.at wöchentlich - 17712 - google.at Interval: weekly
  Google.at wöchentlich - (mobile) - 17737 - google.at mobile Interval: weekly
  Google.at wöchentlich - (maps) - 17742 - google.at Maps Interval: weekly
  Google.at täglich - 17711 - google.at Interval: daily
  Google.at täglich - (mobile) - 17725 - google.at mobile Interval: daily
  Google.at täglich - (maps) - 17729 - google.at Maps Interval: daily

  Google.ch (deutsch) wöchentlich - 17708 - google.ch (de) Interval: weekly
  Google.ch (französisch) wöchentlich - 17715 - google.ch (fr français) Interval: weekly
  Google.ch wöchentlich (mobile) - 17739 - google.ch mobile Interval: weekly
  Google.ch wöchentlich (maps) - 17744 - google.ch Maps Interval: weekly
  Google.ch (deutsch) täglich - 17707 - google.ch (de) Interval: daily
  Google.ch (französisch) täglich - 17714 - google.ch (fr français) Interval: daily
  Google.ch täglich (mobile) - 17727 - google.ch mobile Interval: daily
  Google.ch täglich (maps) - 17731 - google.ch Maps Interval: daily

  Google.co.uk wöchentlich - 17710 - google.co.uk Interval: weekly
  Google.co.uk wöchentlich - (mobile) - 17738 - google.co.uk mobile Interval: weekly
  Google.co.uk wöchentlich - (maps) - 17743 - google.co.uk Maps Interval: weekly
  Google.co.uk täglich - 17709 - google.co.uk Interval: daily
  Google.co.uk täglich - (mobile) - 17726 - google.co.uk mobile Interval: daily
  Google.co.uk täglich - (maps) - 17730 - google.co.uk Maps Interval: daily

  Google.com wöchentlich - 17721 - google.com Interval: weekly
  Google.com wöchentlich - (mobile) - 17735 - google.com mobile Interval: weekly
  Google.com wöchentlich - (maps) - 17740 - google.com Maps Interval: weekly
  Google.com täglich - 17720 - google.com Interval: daily
  Google.com täglich - (mobile) - 17728 - google.com mobile Interval: daily
  Google.com täglich - (maps) - 17732 - google.com Maps Interval: daily

  Google.cz täglich - 17746 - google.cz Interval: daily
  Google.cz täglich - (mobile) - 17747 - google.cz mobile Interval: daily
  Google.cz wöchentlich - 17748 - google.cz Interval: weekly
  Google.cz wöchentlich - (mobile) - 17749 - google.cz mobile Interval: weekly

  Google.sk täglich - 17750 - google.sk Interval: daily
  Google.sk wöchentlich - 17751 - google.sk Interval: weekly
  Google.sk täglich - (mobile) - 17752 - google.sk mobile Interval: daily
  Google.sk wöchentlich - (mobile) - 17753 - google.sk mobile Interval: weekly

  Google.hu täglich - 17754 - google.hu Interval: daily
  Google.hu täglich - (mobile) - 17755 - google.hu mobile Interval: daily
  Google.hu wöchentlich - 17756 - google.hu Interval: weekly
  Google.hu wöchentlich - (mobile) - 17757 - google.hu mobile Interval: weekly

  Google.hr täglich - 17758 - google.hr Interval: daily
  Google.hr täglich - (mobile) - 17759 - google.hr mobile Interval: daily
  Google.hr wöchentlich - 17760 - google.hr Interval: weekly
  Google.hr wöchentlich - (mobile) - 17761 - google.hr mobile Interval: weekly

  Google.si täglich - 17762 - google.si Interval: daily
  Google.si täglich - (mobile) - 17763 - google.si mobile Interval: daily
  Google.si wöchentlich - 17764 - google.si Interval: weekly
  Google.si wöchentlich - (mobile) - 17765 - google.si mobile Interval: weekly

  Google.com.vn täglich - 17766 - google.com.vn Interval: daily
  Google.com.vn täglich - (mobile) - 17767 - google.com.vn mobile Interval: daily
  Google.com.vn wöchentlich - 17768 - google.com.vn Interval: weekly
  Google.com.vn wöchentlich - (mobile) - 17769 - google.com.vn mobile Interval: weekly

  Google.se täglich - 17770 - google.se Interval: daily
  Google.se täglich - (mobile) - 17771 - google.se mobile Interval: daily
  Google.se wöchentlich - 17772 - google.se Interval: weekly
  Google.se wöchentlich - (mobile) - 17773 - google.se mobile Interval: weekly
  Google.se täglich - (maps) - 17774 - google.se Maps Interval: daily
  Google.se wöchentlich - (maps) - 17775 - google.se mobile Interval: weekly

  Google.be täglich - 17776 - google.be (fr) Interval: daily
  Google.be täglich - (mobile) - 17777 - google.be mobile Interval: daily
  Google.be täglich - (maps) - 17778 - google.be Maps Interval: daily
  Google.be wöchentlich - 17779 - google.be (fr) Interval: weekly
  Google.be wöchentlich - (mobile) - 17780 - google.be mobile Interval: weekly
  Google.be wöchentlich - (maps) - 17781 - google.be Maps Interval: weekly

  Google.be (nl) täglich - 17782 - google.be (nl) Interval: daily
  Google.be (nl) wöchentlich - 17783 - google.be (nl) Interval: weekly

  Google.it täglich - 17784 - google.it Interval: daily
  Google.it täglich - (mobile) - 17785 - google.it mobile Interval: daily
  Google.it täglich - (maps) - 17786 - google.it Maps Interval: daily
  Google.it wöchentlich - 17787 - google.it Interval: weekly
  Google.it wöchentlich - (mobile) - 17788 - google.it mobile Interval: weekly
  Google.it wöchentlich - (maps) - 17789 - google.it Maps Interval: weekly

  Google.fr täglich - 17790 - google.fr Interval: daily
  Google.fr täglich - (mobile) - 17791 - google.fr mobile Interval: daily
  Google.fr täglich - (maps) - 17792 - google.fr Maps Interval: daily
  Google.fr wöchentlich - 17793 - google.fr Interval: weekly
  Google.fr wöchentlich - (mobile) - 17794 - google.fr mobile Interval: weekly
  Google.fr wöchentlich - (maps) - 17795 - google.fr Maps Interval: weekly

  Google.nl täglich - 17796 - google.nl Interval: daily
  Google.nl täglich - (mobile) - 17797 - google.nl mobile Interval: daily
  Google.nl täglich - (maps) - 17798 - google.nl Maps Interval: daily
  Google.nl wöchentlich - 17799 - google.nl Interval: weekly
  Google.nl wöchentlich - (mobile) - 17800 - google.nl mobile Interval: weekly
  Google.nl wöchentlich - (maps) - 17801 - google.nl Maps Interval: weekly

  Google.lu täglich - 17802 - google.lu (fr) Interval: daily
  Google.lu täglich - (mobile) - 17803 - google.lu mobile Interval: daily
  Google.lu täglich - (maps) - 17804 - google.lu Maps Interval: daily
  Google.lu wöchentlich - 17805 - google.lu (fr) Interval: weekly
  Google.lu wöchentlich - (mobile) - 17806 - google.lu mobile Interval: weekly
  Google.lu wöchentlich - (maps) - 17807 - google.lu Maps Interval: weekly

  Google.fi täglich - 17808 - google.fi Interval: daily
  Google.fi täglich - (mobile) - 17809 - google.fi mobile Interval: daily
  Google.fi täglich - (maps) - 17810 - google.fi Maps Interval: daily
  Google.fi wöchentlich - 17811 - google.fi Interval: weekly
  Google.fi wöchentlich - (mobile) - 17812 - google.fi mobile Interval: weekly
  Google.fi wöchentlich - (maps) - 17813 - google.fi Maps Interval: weekly

  Google.no täglich - 17814 - google.no Interval: daily
  Google.no täglich - (mobile) - 17815 - google.no mobile Interval: daily
  Google.no täglich - (maps) - 17816 - google.no Maps Interval: daily
  Google.no wöchentlich - 17817 - google.no Interval: weekly
  Google.no wöchentlich - (mobile) - 17818 - google.no mobile Interval: weekly
  Google.no wöchentlich - (maps) - 17819 - google.no Maps Interval: weekly

	Google.es täglich - 17820 - google.es Interval: daily
	Google.es täglich - (mobile) - 17821 - google.es mobile Interval: daily
	Google.es täglich - (maps) - 17822 - google.es Maps Interval: daily
	Google.es wöchentlich - 17823 - google.es Interval: weekly
	Google.es wöchentlich - (mobile) - 17824 - google.es mobile Interval: weekly
	Google.es wöchentlich - (maps) - 17825 - google.es Maps Interval: weekly


*/

ini_set("default_socket_timeout", 600);

class rankingspy {

  public $user          = 'advertisingAdmin';
  public $pass          = 'lucile089open!';
  public $channel       = '19595';
  public $channel_local = '19739';

  public function __construct() {

    $this->client = new SoapClient("https://www.ranking-spy.com/oneadvertisingapi1611/services/RankingSpyService?wsdl", array('trace' => 1));

  }


  public function addKeywordSet($url, $keywords, $name, $measuring, $country, $type = 'desktop') {

    if ($country == 'at') {
      if ($type == 'mobile') {
        $licence = '17725';
        if ($measuring == 'weekly') {$licence = '17737';}
      } elseif ($type == 'maps') {
        $licence = '17729';
        if ($measuring == 'weekly') {$licence = '17742';}
      } else {
        $licence = '17711';
        if ($measuring == 'weekly') {$licence = '17712';}
      }
    } else if ($country == 'ch') {
      if ($type == 'mobile') {
        $licence = '17727';
        if ($measuring == 'weekly') {$licence = '17739';}
      } elseif ($type == 'maps') {
        $licence = '17731';
        if ($measuring == 'weekly') {$licence = '17744';}
      } else {
        $licence = '17707';
        if ($measuring == 'weekly') {$licence = '17708';}
      }
    } else if ($country == 'ch-fr') {
      if ($type == 'mobile') {
        $licence = '17727';
        if ($measuring == 'weekly') {$licence = '17739';}
      } elseif ($type == 'maps') {
        $licence = '17731';
        if ($measuring == 'weekly') {$licence = '17744';}
      } else {
        $licence = '17714';
        if ($measuring == 'weekly') {$licence = '17715';}
      }
    } else if ($country == 'uk') {
      if ($type == 'mobile') {
        $licence = '17726';
        if ($measuring == 'weekly') {$licence = '17738';}
      } elseif ($type == 'maps') {
        $licence = '17730';
        if ($measuring == 'weekly') {$licence = '17743';}
      } else {
        $licence = '17709';
        if ($measuring == 'weekly') {$licence = '17710';}
      }
    } else if ($country == 'us') {
      if ($type == 'mobile') {
        $licence = '17728';
        if ($measuring == 'weekly') {$licence = '17735';}
      } elseif ($type == 'maps') {
        $licence = '17732';
        if ($measuring == 'weekly') {$licence = '17740';}
      } else {
        $licence = '17720';
        if ($measuring == 'weekly') {$licence = '17721';}
      }
    } else if ($country == 'si') {
      if ($type == 'mobile') {
        $licence = '17763';
        if ($measuring == 'weekly') {$licence = '17765';}
      } elseif ($type == 'maps') {
        $licence = '17762';
        if ($measuring == 'weekly') {$licence = '17764';}
      } else {
        $licence = '17762';
        if ($measuring == 'weekly') {$licence = '17764';}
      }
    } else if ($country == 'cz') {
      if ($type == 'mobile') {
        $licence = '17747';
        if ($measuring == 'weekly') {$licence = '17749';}
      } elseif ($type == 'maps') {
        $licence = '17746';
        if ($measuring == 'weekly') {$licence = '17748';}
      } else {
        $licence = '17746';
        if ($measuring == 'weekly') {$licence = '17748';}
      }
    } else if ($country == 'sk') {
      if ($type == 'mobile') {
        $licence = '17752';
        if ($measuring == 'weekly') {$licence = '17753';}
      } elseif ($type == 'maps') {
        $licence = '17750';
        if ($measuring == 'weekly') {$licence = '17751';}
      } else {
        $licence = '17750';
        if ($measuring == 'weekly') {$licence = '17751';}
      }
    } else if ($country == 'hu') {
      if ($type == 'mobile') {
        $licence = '17755';
        if ($measuring == 'weekly') {$licence = '17757';}
      } elseif ($type == 'maps') {
        $licence = '17754';
        if ($measuring == 'weekly') {$licence = '17756';}
      } else {
        $licence = '17754';
        if ($measuring == 'weekly') {$licence = '17756';}
      }
    } else if ($country == 'hr') {
      if ($type == 'mobile') {
        $licence = '17759';
        if ($measuring == 'weekly') {$licence = '17761';}
      } elseif ($type == 'maps') {
        $licence = '17758';
        if ($measuring == 'weekly') {$licence = '17760';}
      } else {
        $licence = '17758';
        if ($measuring == 'weekly') {$licence = '17760';}
      }
    } else if ($country == 'vn') {
      if ($type == 'mobile') {
        $licence = '17767';
        if ($measuring == 'weekly') {$licence = '17769';}
      } elseif ($type == 'maps') {
        $licence = '17766';
        if ($measuring == 'weekly') {$licence = '17768';}
      } else {
        $licence = '17766';
        if ($measuring == 'weekly') {$licence = '17768';}
      }
    } else if ($country == 'se') {
      if ($type == 'mobile') {
        $licence = '17771';
        if ($measuring == 'weekly') {$licence = '17773';}
      } elseif ($type == 'maps') {
        $licence = '17774';
        if ($measuring == 'weekly') {$licence = '17775';}
      } else {
        $licence = '17770';
        if ($measuring == 'weekly') {$licence = '17772';}
      }
    } else if ($country == 'be') {
      if ($type == 'mobile') {
        $licence = '17777';
        if ($measuring == 'weekly') {$licence = '17780';}
      } elseif ($type == 'maps') {
        $licence = '17778';
        if ($measuring == 'weekly') {$licence = '17781';}
      } else {
        $licence = '17776';
        if ($measuring == 'weekly') {$licence = '17779';}
      }
    } else if ($country == 'be-nl') {
      if ($type == 'mobile') {
        $licence = '17777';
        if ($measuring == 'weekly') {$licence = '17780';}
      } elseif ($type == 'maps') {
        $licence = '17778';
        if ($measuring == 'weekly') {$licence = '17781';}
      } else {
        $licence = '17782';
        if ($measuring == 'weekly') {$licence = '17783';}
      }
    } else if ($country == 'it') {
      if ($type == 'mobile') {
        $licence = '17785';
        if ($measuring == 'weekly') {$licence = '17788';}
      } elseif ($type == 'maps') {
        $licence = '17786';
        if ($measuring == 'weekly') {$licence = '17789';}
      } else {
        $licence = '17784';
        if ($measuring == 'weekly') {$licence = '17787';}
      }
    } else if ($country == 'fr') {
      if ($type == 'mobile') {
        $licence = '17791';
        if ($measuring == 'weekly') {$licence = '17794';}
      } elseif ($type == 'maps') {
        $licence = '17792';
        if ($measuring == 'weekly') {$licence = '17795';}
      } else {
        $licence = '17790';
        if ($measuring == 'weekly') {$licence = '17793';}
      }
    } else if ($country == 'nl') {
      if ($type == 'mobile') {
        $licence = '17797';
        if ($measuring == 'weekly') {$licence = '17800';}
      } elseif ($type == 'maps') {
        $licence = '17798';
        if ($measuring == 'weekly') {$licence = '17801';}
      } else {
        $licence = '17796';
        if ($measuring == 'weekly') {$licence = '17799';}
      }
    } else if ($country == 'lu') {
      if ($type == 'mobile') {
        $licence = '17803';
        if ($measuring == 'weekly') {$licence = '17806';}
      } elseif ($type == 'maps') {
        $licence = '17804';
        if ($measuring == 'weekly') {$licence = '17807';}
      } else {
        $licence = '17802';
        if ($measuring == 'weekly') {$licence = '17805';}
      }
    } else if ($country == 'fi') {
      if ($type == 'mobile') {
        $licence = '17803';
        if ($measuring == 'weekly') {$licence = '17812';}
      } elseif ($type == 'maps') {
        $licence = '17804';
        if ($measuring == 'weekly') {$licence = '17813';}
      } else {
        $licence = '17808';
        if ($measuring == 'weekly') {$licence = '17811';}
      }
    } else if ($country == 'no') {
      if ($type == 'mobile') {
        $licence = '17815';
        if ($measuring == 'weekly') {$licence = '17818';}
      } elseif ($type == 'maps') {
        $licence = '17816';
        if ($measuring == 'weekly') {$licence = '17819';}
      } else {
        $licence = '17814';
        if ($measuring == 'weekly') {$licence = '17817';}
      }
    } else if ($country == 'es') {
      if ($type == 'mobile') {
        $licence = '17821';
        if ($measuring == 'weekly') {$licence = '17823';}
      } elseif ($type == 'maps') {
        $licence = '17822';
        if ($measuring == 'weekly') {$licence = '17825';}
      } else {
        $licence = '17820';
        if ($measuring == 'weekly') {$licence = '17823';}
      }
    } else if ($country == 'rs') {
      if ($type == 'mobile') {
        $licence = '17832';
        if ($measuring == 'weekly') {$licence = '17835';}
      } elseif ($type == 'maps') {
        $licence = '17833';
        if ($measuring == 'weekly') {$licence = '17836';}
      } else {
        $licence = '17831';
        if ($measuring == 'weekly') {$licence = '17834';}
      }
    } else if ($country == 'ro') {
      if ($type == 'mobile') {
        $licence = '17827';
        if ($measuring == 'weekly') {$licence = '17829';}
      } elseif ($type == 'maps') {
        $licence = '17828';
        if ($measuring == 'weekly') {$licence = '17830';}
      } else {
        $licence = '17826';
        if ($measuring == 'weekly') {$licence = '17708';}
      }
    } else { // de
      if ($type == 'mobile') {
        $licence = '17719';
        if ($measuring == 'weekly') {$licence = '17736';}
      } elseif ($type == 'maps') {
        $licence = '17723';
        if ($measuring == 'weekly') {$licence = '17741';}
      } else {
        $licence = '17703';
        if ($measuring == 'weekly') {$licence = '17702';}
      }
    }


    $location = 0;

    //$channel = $this->client->createChannel($this->user, $this->pass, $this->channel, $licence, $location, $url, $keywords, true, $name);
    $channel = $this->client->createChannel($this->user, $this->pass, $this->channel, $licence, $location, $url, $keywords, true, $name, false, false);

    return $channel;

  }


  public function addKeywordSetLocal($url, $keywords, $name, $location, $type) {

    if ($type == 'mobile_de') {
      $licence = '17719';
    } else if ($type == 'maps_de') {
      $licence = '17723';
    } else if ($type == 'desktop_de') {
      $licence  = '17703';

    } else if ($type == 'mobile_at') {
      $licence = '17725';
    } else if ($type == 'maps_at') {
      $licence = '17729';
    } else if ($type == 'desktop_at') {
      $licence  = '17711';

    } else if ($type == 'mobile_ch') {
      $licence = '17727';
    } else if ($type == 'maps_ch') {
      $licence = '17731';
    } else if ($type == 'desktop_ch') {
      $licence  = '17707';

    } else if ($type == 'mobile_it') {
      $licence = '17785';
    } else if ($type == 'maps_it') {
      $licence = '17786';
    } else if ($type == 'desktop_it') {
      $licence  = '17784';
    }

    //$channel = $this->client->createChannel($this->user, $this->pass, $this->channel_local, $licence, $location, $url, $keywords, true, $name);
    $channel = $this->client->createChannel($this->user, $this->pass, $this->channel_local, $licence, $location, $url, $keywords, true, $name, false, false);

    return $channel;

  }

  public function removeKeywordSet($channel) {

    try {
      $result = $this->client->deleteChannel($this->user, $this->pass, $channel);
    } catch (Exception $e) {
      $result = 'Channel does not exist';
    }

  }

}

?>
