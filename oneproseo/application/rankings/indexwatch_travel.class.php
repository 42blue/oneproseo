<?php

require_once('ruk.class.php');

/*

Erzeugt die Detailansicht der Keywords und rankenden URLs
https://datatables.net/examples/plug-ins/dom_sort
*/


class indexwatch_travel extends ruk
{


  public function __construct ($env_data)
  {

    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['type'])) {

      $this->selected_project = 40;

      $this->selectAllKeywordSets();
      $this->getRankingData();
      $this->renderRankingViewKeyword();

      $this->checkForRankingsHint();

      $out_arr = array();
      $out_arr['html'] = $this->out;

      echo json_encode($out_arr);

    }

  }

  private function selectAllKeywordSets ()
  {

    $selected_project = $this->selected_project;

    // get project_keyword_sets with project ID
    // get all keywords
    // get url

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword,
                   c.url         AS url,
                   c.id          AS customerid
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function getRankingData()
  {

    $hostnames = array();

    $hostnames['hotel.de'] = 'hotel.de';
    $hostnames['hrs.de'] = 'hrs.de';
    $hostnames['trivago.de'] = 'trivago.de';
    $hostnames['booking.com'] = 'booking.com';
    $hostnames['expedia.de'] = 'expedia.de';
    $hostnames['holidaycheck.de'] = 'holidaycheck.de';
    $hostnames['tui.com'] = 'tui.com';
    $hostnames['neckermann-reisen.de'] = 'neckermann-reisen.de';
    $hostnames['thomascook.de'] = 'thomascook.de';
    $hostnames['tripadvisor.de'] = 'tripadvisor.de';

    //ADWORDS DATA
    $this->googleadwords = parent::getAdwordsData($this->keywords, 'de', 'de');

    $rows = parent::loadCacheFileIndexwatch (1, 'travel');

    // flip for comparison
    $this->keywords_flip = array_flip($this->keywords);

    $i = 0;

    foreach ($rows as $row) {

      if (isset($this->keywords_flip[$row['keyword']])) {

        $purehost = $this->pureHostName($row['url']);

        if (isset($hostnames[$purehost])) {

          // get all measured days
          $this->rankset['_ts'][$purehost] = $purehost;
          $this->rankset['_da'][$row['keyword']][$purehost][] = array ('rank' => $row['position'], 'ts' => $row['timestamp'], 'url' => $row['url']);

          $this->rankset['_ov'][$purehost]['avg'] = $this->rankset['_ov'][$purehost]['avg'] + $row['position'];
          $this->rankset['_ov'][$purehost]['divider'] = $this->rankset['_ov'][$purehost]['divider'] + 1;

          if ($row['position'] >= 1 && $row['position'] <= 10) {
           $this->rankset['_ov'][$purehost]['rank10'] = $this->rankset['_ov'][$purehost]['rank10'] + 1;
          }
          if ($row['position'] > 10 && $row['position'] <= 20) {
           $this->rankset['_ov'][$purehost]['rank20'] = $this->rankset['_ov'][$purehost]['rank20'] + 1;
          }
          if ($row['position'] > 20 && $row['position'] <= 30) {
           $this->rankset['_ov'][$purehost]['rank30'] = $this->rankset['_ov'][$purehost]['rank30'] + 1;
          }

        }

      }

    }

    $this->checkForRankingsHint();

  }


  private function renderRankingViewKeyword ()
  {

    $outhead = '';
    $out = '';

    // SORT BY BEST RANKS
    unset($this->rankset['_ts']);
    $this->rankset['_ts']['holidaycheck.de'] = 'holidaycheck.de';
    $this->rankset['_ts']['tripadvisor.de'] = 'tripadvisor.de';
    $this->rankset['_ts']['hotel.de'] = 'hotel.de';
    $this->rankset['_ts']['booking.com'] = 'booking.com';
    $this->rankset['_ts']['trivago.de'] = 'trivago.de';
    $this->rankset['_ts']['expedia.de'] = 'expedia.de';
    $this->rankset['_ts']['tui.com'] = 'tui.com';
    $this->rankset['_ts']['thomascook.de'] = 'thomascook.de';
    $this->rankset['_ts']['neckermann-reisen.de'] = 'neckermann-reisen.de';
    $this->rankset['_ts']['hrs.de'] = 'hrs.de';


    // merge ranking result with keywordset for keywords that do not rank
    foreach ($this->keywords as $keyword) {
      if (!isset($this->rankset['_da'][$keyword])) {
         $this->rankset['_da'][$keyword] = '';
      }
    }

    // sort by key
    ksort($this->rankset['_da']);

    // HOSTNAMES
    foreach ($this->rankset['_ts'] as $k => $hostname) {
     $out .= '<td style="width:150px;"><a class="highlight" target="_blank" href="url?index=travel&site=http://'.$hostname.'">'.$hostname.'</a></td>';
    }
    $out .= '</tr>';


    $out .= '<tr><td></td><td><a class="highlightruk" target="_blank" href="url">Eigene URL abfragen</a></td><td></td><td></td><td></td><td></td>';
    // HOSTNAMES
    foreach ($this->rankset['_ts'] as $host => $ts) {
      if (isset($this->rankset['_ov'][$host]['divider'])) {
        $div = round($this->rankset['_ov'][$host]['avg']/$this->rankset['_ov'][$host]['divider'],0);
      } else {
        $div = 0;
      }
      $out .= '<td>'. $this->rankset['_ov'][$host]['rank10'] .' <small>TOP 10</small></br>'. $this->rankset['_ov'][$host]['rank20'] .' <small>TOP 20</small> </br>'. $this->rankset['_ov'][$host]['rank30'] .' <small>TOP 30</small><br />' . $div .' ∅</br>  </td>';
    }
    $out .= '</tr>';

    $out .= '<tr><td>#</td><td>Keyword</td><td>OPI</td><td>SV</td><td>CPC</td><td>Wettb.</td>';
   // HOSTNAMES
    foreach ($this->rankset['_ts'] as $k => $hostname) {
     $out .= '<td>'.$hostname.'</td>';
    }
    $out .= '</tr>';

    $out .= '</thead><tbody>';

    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank">' . $keyword . '</td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {

        $out .= '<td data-order="0" class="gadw"> N/A </td>';
        $out .= '<td data-order="0" class="gadw"> N/A </td>';
        $out .= '<td data-order="0" class="gadw"> N/A </td>';
        $out .= '<td data-order="0" class="gadw"> N/A </td>';

      } else {

        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw"> ' . number_format($this->googleadwords[$keyword][3], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][0].'" class="gadw"> ' . number_format($this->googleadwords[$keyword][0], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][1].'" class="gadw"> ' . number_format($this->googleadwords[$keyword][1], 2, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][2].'" class="gadw"> ' . number_format($this->googleadwords[$keyword][2], 2, ',', '.')  . ' </td>';

      }


      // RANKING SETS
      $ii = 0;
      foreach ($this->rankset['_ts'] as $k => $ts) {

        if (!isset($timestamp[$ts])) {

          $out .= '<td data-order="999" class="rank"> - </td>';

        } else {

          //multiple rankings
          if (count($timestamp[$ts]) > 1) {

            $out .= '<td class="rank"';
            $k = 0;
            $matched_url = array();

            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_url[$value['url']])) {
                continue;
              }
              $matched_url[$value['url']] = $value['url'];

              if ($k == 0) {
                $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              } else {
                $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              }
              $k++;
            }

            $out .= '</td>';

          } else {
          //single rankings

            foreach ($timestamp[$ts] as $value) {

              if (empty($value['rank'])) {
                $out .= '<td data-order="999" class="rank"> - </td>';
              } else {
                $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a></td>';
              }

            }

          }

        }

      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Indexwatch: Hotels, Rankings am '. parent::germanTS(parent::dateYMD()).'</span>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td><td>OPI</td><td>SV</td><td>CPC</td><td>Wettb.</td>';

    $this->out = $outhead . $out;

  }

  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {
      $out_arr = array();
      $out_arr['html'] = '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für diese URL liegen keine Rankings vor!</strong></div></div></div>';
      echo json_encode($out_arr);
      exit;
    }

  }

}

?>
