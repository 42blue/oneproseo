<?php

require_once('ruk.class.php');

header('Content-type: application/json');

class rankings_chart extends ruk
{


  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_GET['projectid'])) {

      $this->keyword_setid = $_GET['setid'];
      $this->project_id    = $_GET['projectid'];
      $this->days          = $_GET['days'];
      $this->type          = $_GET['type'];

      // SCALE 7 or 31 DAYS
      if (!empty($this->days)) {
        $this->days   = '_31';
      } else {
        $this->days   = '';
      }

      if ($this->type == 'local') {
        $this->cache_filename = 'local/charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . $this->days . '.tmp';
        $res = $this->sql_cache->readFile($this->cache_filename);
      } else if ($this->type == 'month') {
        $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . '_monthly.tmp';
        $res = $this->sql_cache->readFile($this->cache_filename);
      } else if ($this->type == 'week') {
        $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . '_weekly.tmp';
        $res = $this->sql_cache->readFile($this->cache_filename);
      } else {
        $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . $this->days . '.tmp';
        $res = $this->sql_cache->readFile($this->cache_filename);
      }

      // WEEKLY CHART FALLBACK
      if ($res['q'] == 0 && $this->days != '_31') {
        $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . '_weekly.tmp';
        $res = $this->sql_cache->readFile($this->cache_filename);
      }

      // WEEKLY CHART FALLBACK
      if ($res['q'] == 0 && $this->days == '_31') {
        $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . '_monthly.tmp';
        $res = $this->sql_cache->readFile($this->cache_filename);
      }

      echo json_encode($res);

    }

  }

}

?>
