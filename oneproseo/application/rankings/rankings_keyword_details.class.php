<?php

require_once('ruk.class.php');

class rankings_keyword_details extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (!isset($_POST) || empty($_POST)) {exit;}

    $this->rankset    = array();
    $this->projectid  = $_POST['projectid'];
    $this->country    = $_POST['country'];
    $this->chart      = array();    
    $this->chartdates = array();

		$this->chartDaily 		= array();
		$this->chartdatesDaily = array();    

    $loc = $this->getAdwordsLoc($this->country);
    $this->adwords_country  = $loc[0];
    $this->adwords_language = $loc[1];

    $customer_data          = $this->getHostname($this->projectid);
    $this->hostname         = $this->pureHostName($customer_data['url']);
    $this->ignore_subd      = $this->getIgnoreSubdomain($this->projectid);

    $this->keyword = urldecode($_POST['keyword']);

    $this->getKeywordRankings();

    $this->renderDailyGraph();

    $this->renderView('keyword');

		$this->renderURLGraph();
    $this->renderURLGraphDaily();

    // CHART AND OUTPUT
   	$chart1 = array ();
    foreach ($this->chart as $keyword => $graph) {
      $chart1[] = array ('name' => $keyword, 'series' => $graph, 'color' => $this->randomcolor());
    }

   	$chart2 = array ();
    foreach ($this->chartUrl as $url => $graph) {
      $chart2[] = array ('name' => $url, 'series' => $graph, 'color' => $this->randomcolor());
    }

   	$chart3 = array ();
    foreach ($this->chartDaily as $keyword => $graph) {
      $chart3[] = array ('name' => $keyword, 'series' => $graph, 'color' => $this->randomcolor());
    }

    $chart4 = array ();
    foreach ($this->chartUrlDaily as $url => $graph) {
      $chart4[] = array ('name' => $url, 'series' => $graph, 'color' => $this->randomcolor());
    }


    echo json_encode(array(
    	'json' => array('ctitle' => 'Keyword Rankings', 'categories' => $this->chartdates, 'charts' => $chart1), 
    	'json2' => array('ctitle' => 'URL Rankings', 'categories' => $this->chartdates, 'charts' => $chart2), 
    	'json3' => array('ctitle' => 'Keyword Rankings', 'categories' => $this->chartdatesDaily, 'charts' => $chart3), 
      'json4' => array('ctitle' => 'URL Rankings', 'categories' => $this->chartdatesDaily, 'charts' => $chart4),       
    	'html' => $this->out));

  }


  private function getKeywordRankings()
  {

    $this->keywords = array($this->keyword);

    $sql = "SELECT
              id,
              keyword,
              timestamp
            FROM
              ruk_scrape_keywords
            WHERE
              keyword = '$this->keyword' COLLATE utf8_bin
            AND
              language = '$this->country'
            AND
              DATE(timestamp) <= CURDATE()
            AND
              DATE(timestamp) >= CURDATE() - INTERVAL 365 DAY
            ORDER BY timestamp DESC";

    $rank_add = 0;
    $result = $this->db->query($sql);
    $rows_kw = array();

    if ($result->num_rows == 0) {
      $this->checkForRankingsHint();
    }

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
      $this->rankset['_ts'][$row['timestamp']] = $row['timestamp'];
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)
              ORDER BY position DESC";

    $result2 = $this->db->query($sql);

    $rows_ra  = array();

    if ($result2->num_rows != 0) {
      while ($row = $result2->fetch_assoc()) {
        if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
          $row['url'] = strtolower($row['url']);
          $this->rankset['_da'][$rows_kw[$row['id_kw']]['keyword']][$rows_kw[$row['id_kw']]['timestamp']][] = array ('rank' => $row['position'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'url' => $row['url']);

          $this->rankset['_url'][$row['url']][$rows_kw[$row['id_kw']]['timestamp']][] = array ('kw' => $rows_kw[$row['id_kw']]['keyword'], 'rank' => $row['position'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'url' => $row['url']);

        }
      }
    }

    // ADWORDS
    $this->googleadwords = parent::getAdwordsData($this->keywords, $this->adwords_country, $this->adwords_language);

  }


  private function renderView($kind) {

    $this->checkForRankingsHint();

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition');

    // merge ranking result with keywordset for keywords that do not rank
    if (!isset($this->url_campaign)) {
      foreach ($this->keywords as $keyword) {
        if (!isset($this->rankset['_da'][$keyword])) {
           $this->rankset['_da'][$keyword] = '';
        }
      }
    }

    $ii = 0;
    // TABLE HEADER - DATES
    foreach ($this->rankset['_ts'] as $k => $ts) {

  	 if (date('D', strtotime($ts)) != 'Mon') {
 		  continue;
 		 }

     $ii++;

     $this->chartdates[] = parent::germanTSred($ts);

     $out .= '<td>Rank - '. parent::germanTS($ts) .'</td>';

     if ($ii < count($this->rankset['_ts'])) {
       $out .= '<td style="width:50px;">Change</td>';
     }

     array_push($this->csvarr['head'], 'Rank ' . $ts);
     array_push($this->csvarr['head'], 'Change');
     array_push($this->csvarr['head'], 'URL');

    }

    $out .= '</tr></thead><tbody>';

    // TABLE BODY - CONTENT
    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank"><a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">' . $keyword . '</a><small target="_blank" class="rankings"><a target="_blank" href="https://next.sistrix.de/seo/ranking_history/keyword/' . $keyword . '/domain/'.$this->hostname.'">SISTRIX</a></small></td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {

        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

      } else {

      // ONE DEX FETCH DATA FOR CALC

        $latest_ts = array_keys($this->rankset['_ts']);
        $latest_ts = $latest_ts[0];

        if (isset($timestamp[$latest_ts][0])) {
          $rank = $timestamp[$latest_ts][0]['rank'];
        } else {
          $rank = 101;
        }

        $opi      = $this->googleadwords[$keyword][3];
        $dex      = $this->calcOneDex($rank, $opi, true);
        $dex_format      = number_format($dex, 3, ',', '.');

      // ONE DEX FETCH DATA FOR CALC
        $out .= '<td data-order="'.$dex.'" class="gadw tdright"> ' . $dex_format . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][3], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][0].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][0], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][1].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][1], 2, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][2].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][2], 2, ',', '.')  . ' </td>';

      }

      $this->csvarr[$i] = array(
          $i,
          $keyword,
          $dex_format,
          number_format($this->googleadwords[$keyword][3], 0, ',', '.'),
          number_format($this->googleadwords[$keyword][0], 0, ',', '.'),
          number_format($this->googleadwords[$keyword][1], 2, ',', '.'),
          number_format($this->googleadwords[$keyword][2], 2, ',', '.')
        );

      // RANKING SETS
      $ii = 0;
      foreach ($this->rankset['_ts'] as $k => $ts) {

	  	 if (date('D', strtotime($ts)) != 'Mon') {
	 		  continue;
	 		 }

        $ii++;

        // NO RANKING TODAY
        if (!isset($timestamp[$ts])) {

          $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'">TOP100</a></small></td>';

          $yesterday = parent::getOneWeekAgoYMD($ts);
          if (isset( $this->rankset['_da'][$keyword][$yesterday][0]['rank'] )) {
            $change = $this->checkDifferenceBetweenRankings('101', $this->rankset['_da'][$keyword][$yesterday][0]['rank']);
            $change_csv = $this->checkDifferenceBetweenRankings('101', $this->rankset['_da'][$keyword][$yesterday][0]['rank'], false);
          } else {
            $change = '';
            $change_csv = '';
          }

          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change .'</td>';
          }

          if (isset($this->chart[$keyword])) {
            array_push($this->chart[$keyword],  null);
          } else {
            $this->chart[$keyword] = array(null);
          }

          //$out .= '<td class="rank"> </td>';

          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], $change_csv);
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '');

        // RANKINGS TODAY
        } else {

          //multiple rankings
          if (count($timestamp[$ts]) > 1) {

            // sort multiple rankings, lowest first
            krsort($timestamp[$ts]);

            $out .= '<td class="rank"';
            $k = 0;

            $matched_url = array();
            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_url[$value['url']])) {
                continue;
              }
              $matched_url[$value['url']] = $value['url'];

              $yesterday = parent::getOneWeekAgoYMD($ts);

              if ($k == 0) {
                $compare_index = count($this->rankset['_da'][$keyword][$yesterday]) - 1;
                if (isset($this->chart[$keyword])) {
                  array_push($this->chart[$keyword], intval($value['rank']));
                } else {
                  $this->chart[$keyword] = array(intval($value['rank']));
                }

                $csv = $value['rank'];
                $csv_url = $value['url'];
                $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank']);
                $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank'], false);
              } else {
                $csv .= ' / ' . $value['rank'];
                $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              }
              $k++;

            }

            $out .= '<small class="rankings"><a target="_blank" href="../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'">TOP100</a></small>';

            $out .= '</td>';

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change.'</td>';
            }

            array_push($this->csvarr[$i], $csv);
            array_push($this->csvarr[$i], $change_csv);
            array_push($this->csvarr[$i], $csv_url);

          } else {

          //single rankings
            foreach ($timestamp[$ts] as $value) {

              $yesterday = parent::getOneWeekAgoYMD($ts);

              if (empty($value['rank'])) {
                $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'">TOP100</a></small></td>';
              } else {
                $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small class="rankings"><a target="_blank" href="../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'">TOP100</a></small></td>';
              }

              if (isset($this->chart[$keyword])) {
                array_push($this->chart[$keyword], intval($value['rank']));
              } else {
                $this->chart[$keyword] = array(intval($value['rank']));
              }


              $compare_index = count($this->rankset['_da'][$keyword][$yesterday]) - 1;
              $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank']);
              $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank'], false);
              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'.$change .'</td>';
              }

              array_push($this->csvarr[$i], $value['rank']);
              array_push($this->csvarr[$i], $change_csv);
              array_push($this->csvarr[$i], $value['url']);

            }

          }

        }


      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords');

    $outhead .= '<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
            <div class="titleleft">
            	Rankings für das Keyword <span class="red">'.$this->keyword.'</span> / <span class="red">'.$this->hostname.'</span>
            </div> 
            <div class="titleright">Wochenansicht</div>
           </div>
            <div class="box-content padded">
              <div class="row">
                <div class="col-md-12"><div id="ops-histochart"></div></div>
              </div>
            </div>
          </div>
        </div>
      </div>';


			if (count($this->chartdatesDaily) > 56) {

			    $outhead .= '<div class="row">
			        <div class="col-md-12">
			          <div class="box">
			            <div class="box-header">
			            <div class="titleleft">
			            	Rankings für das Keyword <span class="red">'.$this->keyword.'</span> / <span class="red">'.$this->hostname.'</span>
			            </div> 
			            <div class="titleright">Tagesansicht</div>
			           </div>
			            <div class="box-content padded">
			              <div class="row">
			                <div class="col-md-12"><div id="ops-histochart3"></div></div>
			              </div>
			            </div>
			          </div>
			        </div>
			      </div>';
			}


    $outhead .= '<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
            <div class="titleleft">
            	URL Rankings für das Keyword <span class="red">'.$this->keyword.'</span> / <span class="red">'.$this->hostname.'</span>
            </div> 
            <div class="titleright">Wochenansicht</div>
           </div>
            <div class="box-content padded">
              <div class="row">
                <div class="col-md-12"><div id="ops-histochart2"></div></div>
              </div>
            </div>
          </div>
        </div>
      </div>';

    if (count($this->chartdatesDaily) > 56) {
    $outhead .= '<div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
            <div class="titleleft">
              URL Rankings für das Keyword <span class="red">'.$this->keyword.'</span> / <span class="red">'.$this->hostname.'</span>
            </div> 
            <div class="titleright">Tagesansicht</div>
           </div>
            <div class="box-content padded">
              <div class="row">
                <div class="col-md-12"><div id="ops-histochart4"></div></div>
              </div>
            </div>
          </div>
        </div>
      </div>';
    }

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Zeitraum: maximal 12 Monate (sofern Ranking Daten vorhanden sind)</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td><td>OneDex</td><td>OPI</td><td>SV</td><td>CPC</td><td>Wettb.</td>';

    $this->out = $outhead . $out;

  }


  private function renderEditSet () {

    if ($_SESSION['role2'] == 'admin' && $this->keyword_setid != 'all') {
      return '<li><a target="_blank" href="../../../../../keywords/'.$this->keyword_setid.'"> <i title="Edit set" class="icon-edit setedit"></i> </a></li>';
    }

  }


  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {
      $this->out = '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für die Keywords oder URLs in diesem Set wurden (noch) keine Rankings für die letzten Monate gefunden!</strong></div></div></div>';
      echo json_encode (array('html' => $this->out));
      exit;
    }

  }


  private function renderDailyGraph () {

  	$this->chartdatesDaily = array();
  	$this->chartDaily = array();

    foreach ($this->rankset['_ts'] as $k => $ts) {

     	$this->chartdatesDaily[] = parent::germanTSred($ts);

      // RANKING SETS
      $ii = 0;
      foreach ($this->rankset['_da'] as $keyword => $timestamp) {

        $ii++;

        // NO RANKING TODAY
        if (!isset($timestamp[$ts])) {

          if (isset($this->chartDaily[$keyword])) {
            array_push($this->chartDaily[$keyword],  null);
          } else {
            $this->chartDaily[$keyword] = array(null);
          }

				}  else {
          if (count($timestamp[$ts]) > 1) {

            // sort multiple rankings, lowest first
            krsort($timestamp[$ts]);

            $k = 0;

            foreach ($timestamp[$ts] as $value) {

              if ($k == 0) {

                if (isset($this->chartDaily[$keyword])) {
                  array_push($this->chartDaily[$keyword], intval($value['rank']));
                } else {
                  $this->chartDaily[$keyword] = array(intval($value['rank']));
                }

              }

              $k++;

            }

					} else {

          //single rankings
            foreach ($timestamp[$ts] as $value) {					

              if (isset($this->chartDaily[$keyword])) {
                array_push($this->chartDaily[$keyword], intval($value['rank']));
              } else {
                $this->chartDaily[$keyword] = array(intval($value['rank']));
              }

            }	

					}	

				}      	

			}

    }

  }


  private function renderURLGraph () {

  	$this->chartUrl = array();

    foreach ($this->rankset['_ts'] as $k => $ts) {

  	  if (date('D', strtotime($ts)) != 'Mon') {
 		   continue;
 		  }

    	foreach ($this->rankset['_url'] as $url => $timestamp) {

        if (!isset($timestamp[$ts])) {

          if (isset($this->chartUrl[$url])) {
            array_push($this->chartUrl[$url], 101);
          } else {
            $this->chartUrl[$url] = array(101);
          }
          
        } else {

					foreach ($timestamp[$ts] as $key => $value) {
			      if (isset($this->chartUrl[$url])) {
			        array_push($this->chartUrl[$url], intval($value['rank']));
			      } else {
			        $this->chartUrl[$url] = array(intval($value['rank']));
			      }
					}

    	  }

    	}

    }

  }


  private function renderURLGraphDaily () {

    $this->chartUrlDaily = array();

    foreach ($this->rankset['_ts'] as $k => $ts) {

      foreach ($this->rankset['_url'] as $url => $timestamp) {

        if (!isset($timestamp[$ts])) {

          if (isset($this->chartUrlDaily[$url])) {
            array_push($this->chartUrlDaily[$url], 101);
          } else {
            $this->chartUrlDaily[$url] = array(101);
          }
          
        } else {

          foreach ($timestamp[$ts] as $key => $value) {
            if (isset($this->chartUrlDaily[$url])) {
              array_push($this->chartUrlDaily[$url], intval($value['rank']));
            } else {
              $this->chartUrlDaily[$url] = array(intval($value['rank']));
            }
          }

        }

      }

    }

  }

}

?>
