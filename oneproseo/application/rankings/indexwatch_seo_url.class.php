<?php

require_once('ruk.class.php');

class indexwatch_seo_url extends ruk
{


  public function __construct ($env_data)
  {

    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->sql_cache = new Cache();
    $this->sql_cache->setEnv($this->env_data);

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {


    if (isset($_POST['index'])) {

      $this->index            = $_POST['index'];
      $this->agency_id        = $_POST['id'];
      $this->select_index     = 'seocompany';

      $this->selectAgency();
      $this->selectSeoCompanies();
      $this->selectKeywordSet(1097);
      $this->getRankingDataDex();
      $this->getRankingData();
      $this->renderRankingViewKeyword();

      $this->checkForRankingsHint();

      $out_arr = array();
      $out_arr['html'] = $this->out;

      echo json_encode($out_arr);

    }

  }


  private function selectAgency () {

    $sql = "SELECT * FROM ruk_indexwatch_seo_companies WHERE id = $this->agency_id";
    $result = $this->db->query($sql);
    $rows = array();
    while ($row = $result->fetch_assoc()) {
      $rows = $row;
    }
    $this->agency = $rows;

  }


  private function selectSeoCompanies () {

    $sql = "SELECT
              a.url AS url,
              a.name AS name,
              b.score AS score
            FROM ruk_indexwatch_seo_companies a
              LEFT JOIN ruk_indexwatch_seo_companies_sistrix b
                ON b.id_customer = a.id";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $hostname = $this->pureHostName($row['url']);
      $rows[$hostname] = array ( $row['url'], $row['name'], $row['score'] );
    }

    $this->seo_companies         = $rows;
    $this->seo_companies_sistrix = $rows;

  }

  private function selectKeywordSet ($set_id) {

    $sql = "SELECT
              keyword
            FROM ruk_project_keywords
            WHERE id_kw_set = $set_id";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    $this->keywords = $rows;

  }


  private function getRankingDataDex()
  {

    $this->cache = parent::loadCacheFileIndexwatch (6, 'seocompany');
    //ADWORDS DATA
    $this->googleadwords = parent::getAdwordsData($this->keywords);

    foreach ($this->cache as $row) {

      if ($row['timestamp'] != '2016-04-06') {continue;}

      $hostname = $this->pureHostName($row['url']);

      if (isset($this->seo_companies[$hostname])) {

        $dex = $this->calcOneDex($row['position'], $this->googleadwords[$row['keyword']][3]);

        if (isset($this->rankset['_dex'][$hostname])) {
          $this->rankset['_dex'][$hostname] = $this->rankset['_dex'][$hostname] + $dex;
        } else {
          $this->rankset['_dex'][$hostname] = $dex;
        }

      }

    }

  }

  private function getRankingData()
  {

    $hostname = $this->pureHostName($this->agency['url']);

    $rows = array_reverse($this->cache);

    // flip for comparison
    $this->keywords_flip = array_flip($this->keywords);

    $i = 0;

    foreach ($rows as $row) {

      if (isset($this->keywords_flip[$row['keyword']])) {

        if ($this->pureHostName($row['url']) == $hostname) {
          // get all measured days
          $this->rankset['_ts'][$row['timestamp']] = $row['timestamp'];
          $this->rankset['_da'][$row['keyword']][$row['timestamp']][] = array ('rank' => $row['position'], 'ts' => $row['timestamp'], 'url' => $row['url']);

        }

      }

    }

    $this->checkForRankingsHint();

  }


  private function renderRankingViewKeyword ()
  {

    // SORT RANKING FOR ALL SEO COMPANIES
    foreach ($this->seo_companies as $hostname => $dex) {
      if (isset($this->rankset['_dex'][$hostname])) {
        array_push($this->seo_companies[$hostname], $this->rankset['_dex'][$hostname]);
      } else {
        array_push($this->seo_companies[$hostname], 0);
      }
    }
    // SORT BY DEX
    uasort($this->seo_companies, function($a, $b) {
      return $b[3] > $a[3];
    });


    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'OPI', 'Search Volume', 'CPC', 'Competition');

    // merge ranking result with keywordset for keywords that do not rank
    foreach ($this->keywords as $keyword) {
      if (!isset($this->rankset['_da'][$keyword])) {
         $this->rankset['_da'][$keyword] = '';
      }
    }

    // sort by key
    ksort($this->rankset['_da']);

    $ii = 0;
    // DATES
    foreach ($this->rankset['_ts'] as $k => $ts) {

     $ii++;

     $out .= '<td>Rank - '. parent::germanTS($ts) .'</td>';

     if ($ii < count($this->rankset['_ts'])) {
       $out .= '<td style="width:50px;">Change</td>';
     }

     array_push($this->csvarr['head'], $ts);

    }

    $out .= '</tr></thead><tbody>';

    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank">' . $keyword . '</td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword])) {

        $out .= '<td data-order="0" class="gadw"> N/A </td>';

      } else {

        $out .= '<td data-order="'.$this->googleadwords[$keyword][3].'" class="gadw"> ' . number_format($this->googleadwords[$keyword][3], 0, ',', '.')  . ' </td>';

      }

      $this->csvarr[$i] = array($i, $keyword, $this->googleadwords[$keyword][3], $this->googleadwords[$keyword][0], $this->googleadwords[$keyword][1], $this->googleadwords[$keyword][2]);

      // RANKING SETS
      $ii = 0;
      foreach ($this->rankset['_ts'] as $k => $ts) {

        $ii++;

        if (!isset($timestamp[$ts])) {

          $out .= '<td data-order="999" class="rank"> - </td>';

          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td></td>';
          }

          array_push($this->csvarr[$i], '-');

        } else {

          //multiple rankings
          if (count($timestamp[$ts]) > 1) {

            $out .= '<td class="rank"';
            $k = 0;
            $matched_url = array();

            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_url[$value['url']])) {
                continue;
              }
              $matched_url[$value['url']] = $value['url'];

              $yesterday = key($this->rankset['_ts']);
              next($this->rankset['_ts']);

              if ($k == 0) {
                $csv = $value['rank'];
                $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][0]['rank']);
              } else {
                $csv .= ' / ' . $value['rank'];
                $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              }
              $k++;
            }

            array_push($this->csvarr[$i], $csv);

            $out .= '</td>';

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change.'</td>';
            }

          } else {
          //single rankings

            foreach ($timestamp[$ts] as $value) {

              $yesterday = key($this->rankset['_ts']);
              next($this->rankset['_ts']);

              array_push($this->csvarr[$i], $value['rank']);

              if (empty($value['rank'])) {
                $out .= '<td data-order="999" class="rank"> - </td>';
              } else {
                $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a></td>';
              }

              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'. $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][0]['rank']).'</td>';
              }

            }

          }

        }

      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $rank = array_search($this->pureHostName($this->agency['url']), array_keys($this->seo_companies));
    $rank = $rank + 1;

    $outhead .='
          <div class="row">
            <div class="col-md-12">
              <h1 class="indexwatch"><strong>'.$this->agency['name'].'</strong> rankt mit <a class="blue"  href="'.$this->agency['url'].'" target="_blank">' . $this->pureHostName($this->agency['url']) . '</a> auf <strong>Platz '.$rank.' </strong>im deutschen SEO Agentur Ranking.</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Detail Rankings für '.$this->agency['url'].'</span>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td><td>OPI</td>';

    $this->out = $outhead . $out;

  }


  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {

      $this->out = '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für diese URL liegen keine Rankings vor!</strong></div></div></div>';

      $out_arr = array();
      $out_arr['html'] = $this->out;

      echo json_encode($out_arr);

      exit;

    }

  }

}

?>
