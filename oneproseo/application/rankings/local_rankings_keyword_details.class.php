<?php

require_once('ruk.class.php');

class local_rankings_keyword_details extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (!isset($_POST) || empty($_POST)) {exit;}

    $this->rankset     = array();
    $this->projectid   = $_POST['projectid'];
    $this->country     = $_POST['country'];
    $this->location    = $_POST['location'];
    $this->rs_type     = $_POST['type'];
    $this->view        = $_POST['view'];
    $this->chart       = array();
    $this->chartdates  = array();
    $this->keyword_loc = array();

    $customer_data          = $this->getHostname($this->projectid);
    $this->hostname         = $this->pureHostName($customer_data['url']);
    $this->locations        = $this->getLocationsArray();
    $this->ignore_subd      = $this->getIgnoreSubdomain($this->projectid);

    if (isset($_POST['keyword']) && !empty($_POST['keyword']) && $_POST['view'] == 'keyword') {

      $this->keyword = urldecode($_POST['keyword']);
      $this->keyword_loc[] = array($this->keyword, $this->location);
      $this->getKeywordRankings();
      $this->renderView('keyword');

    } else if (isset($_POST['setid']) && !empty($_POST['setid']) && $_POST['view'] == 'keyword') {

      $this->setid = $_POST['setid'];
      $this->getKeywordRankings($this->getSetKeywords());
      $this->renderView('set_allkeywords');

    } else if (isset($_POST['view']) && $_POST['view'] == 'location') {

      $this->keyword = urldecode($_POST['keyword']);
      $this->setid   = $_POST['setid'];
      $this->rs_locations  = $this->getLocationsArray();
      $this->getKeywordRankings($this->getSetLocations());
      $this->renderViewLocations('set_alllocations');

    }

    // CHART AND OUTPUT
    foreach ($this->chart as $keyword => $graph) {
      $arr[] = array ('name' => $keyword, 'series' => $graph, 'color' => $this->randomcolor());
    }

    echo json_encode(array('json' => array('ctitle' => 'Rankings', 'categories' => $this->chartdates, 'charts' => $arr),'html' => $this->out));

  }


  private function getKeywordRankings($csk = false)
  {

    $day = date('l');

    if ($csk == false) {

      $this->keywords = array($this->keyword);

      $sql = "SELECT
                id,
                keyword,
                location,
                rs_type,
                timestamp
              FROM
                ruk_scrape_keywords_local
              WHERE
                keyword  = '$this->keyword'
              AND
                location = '$this->location'
              AND
                rs_type  = '$this->rs_type'
              AND
                language = '$this->country'
              AND
                DAYNAME( DATE( timestamp ) ) = '$day'
              AND
                DATE(timestamp) >= CURDATE() - INTERVAL 365 DAY
              ORDER BY timestamp DESC";

    } else if ($_POST['view'] == 'location') {

      $this->keywords = array($this->keyword);

      $sql = "SELECT
              id,
              keyword,
              location,
              rs_type,
              timestamp
            FROM
              ruk_scrape_keywords_local
            WHERE
              keyword  = '$this->keyword'
            AND
              location IN ($csk)
            AND
              rs_type  = '$this->rs_type'
            AND
              language = '$this->country'
            AND
              DAYNAME( DATE( timestamp ) ) = '$day'
            AND
              DATE(timestamp) >= CURDATE() - INTERVAL 365 DAY
            ORDER BY timestamp DESC";

    } else {

       $sql = "SELECT
              id,
              keyword,
              location,
              rs_type,
              timestamp
            FROM
              ruk_scrape_keywords_local
            WHERE
              keyword IN ($csk)
            AND
              location = '$this->location'
            AND
              rs_type  = '$this->rs_type'
            AND
              language = '$this->country'
            AND
              DAYNAME( DATE( timestamp ) ) = '$day'
            AND
              DATE(timestamp) >= CURDATE() - INTERVAL 365 DAY
            ORDER BY timestamp DESC";
    }

    $rank_add = 0;
    $result = $this->db->query($sql);
    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
      $this->rankset['_ts'][$row['timestamp']] = $row['timestamp'];
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings_local
            WHERE
              id_kw IN ($rows_kw_keys)
            ORDER BY position DESC";

    $result2 = $this->db->query($sql);

    $rows_ra  = array();

    if ($result2->num_rows != 0) {
      while ($row = $result2->fetch_assoc()) {
        if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
          $row['url'] = strtolower($row['url']);
          // WITH LOC
          if ($_POST['view'] == 'location') {
            $this->rankset['_da'][$rows_kw[$row['id_kw']]['keyword']][$rows_kw[$row['id_kw']]['location']][$rows_kw[$row['id_kw']]['timestamp']][$this->rs_type][] = array ('rank' => $row['position'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'url' => $row['url']);
          } else {
            $this->rankset['_da'][$rows_kw[$row['id_kw']]['keyword']][$rows_kw[$row['id_kw']]['timestamp']][] = array ('rank' => $row['position'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'url' => $row['url']);
          }
        }
      }
    }

    // ADWORDS
    $this->googleadwords = parent::getAdwordsDataLocal($this->keyword_loc);

  }

  private function getSetKeywords() {

    $this->keywords = array();

    $sql = "SELECT
              b.id_kw_set AS id_kw_set,
              b.keyword   AS keyword,
              a.name      AS setname
            FROM ruk_local_keyword_sets a
              LEFT JOIN ruk_local_keywords b
                ON b.id_kw_set = a.id
            WHERE
              a.id = $this->setid";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keyword_loc[md5($row['keyword'] . $this->location)] = array ($row['keyword'], $this->location);
      $this->keywords[$row['keyword']] = $row['keyword'];
      $this->setname                   = $row['setname'];
    }

    $comma_separated_kws = implode('", "', $this->keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    return $comma_separated_kws;

  }


  private function getSetLocations() {

    $this->keywords = array();

    $sql = "SELECT a.location AS location,
                   b.name     AS setname
            FROM ruk_local_sets2rs a
              LEFT JOIN ruk_local_keyword_sets b
                ON b.id = a.id_kw_set
            WHERE id_kw_set = $this->setid";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if ($row['location'] == 0) {continue;}
      $this->keyword_loc[]         = array ($this->keyword, $row['location']);
      $locations[$row['location']] = $row['location'];
      $this->setname               = $row['setname'];
    }

    $comma_separated_loc = implode('", "', $locations);
    $comma_separated_loc = '"' . $comma_separated_loc . '"';

    return $comma_separated_loc;

  }


  private function renderView($kind) {

    $this->checkForRankingsHint();

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition');

    // merge ranking result with keywordset for keywords that do not rank
    if (!isset($this->url_campaign)) {
      foreach ($this->keywords as $keyword) {
        if (!isset($this->rankset['_da'][$keyword])) {
           $this->rankset['_da'][$keyword] = '';
        }
      }
    }

    $ii = 0;
    // TABLE HEADER - DATES
    foreach ($this->rankset['_ts'] as $k => $ts) {

     $ii++;

     $this->chartdates[] = parent::germanTSred($ts);

     $out .= '<td>Rank - '. parent::germanTS($ts) .'</td>';

     if ($ii < count($this->rankset['_ts'])) {
       $out .= '<td style="width:50px;">Change</td>';
     }

     array_push($this->csvarr['head'], 'Rank ' . $ts);
     array_push($this->csvarr['head'], 'Change');
     array_push($this->csvarr['head'], 'URL');

    }

    $out .= '</tr></thead><tbody>';

    // TABLE BODY - CONTENT
    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $timestamp) {

      $i++;

      $out .= '<tr>';
      $out .= '<td class="rank">' . $i . '</td>';
      $out .= '<td class="rank"><a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">' . $keyword . '</a><small target="_blank" class="rankings"><a target="_blank" href="https://next.sistrix.de/seo/ranking_history/keyword/' . $keyword . '/domain/'.$this->hostname.'">SISTRIX</a></small></td>';

      // no adwords data
      if (!isset($this->googleadwords[$keyword][$this->location])) {

        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
        $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

      } else {

      // ONE DEX FETCH DATA FOR CALC

        $latest_ts = array_keys($this->rankset['_ts']);
        $latest_ts = $latest_ts[0];

        if (isset($timestamp[$latest_ts][0])) {
          $rank = $timestamp[$latest_ts][0]['rank'];
        } else {
          $rank = 101;
        }

        $opi      = $this->googleadwords[$keyword][$this->location][3];
        $dex      = $this->calcOneDex($rank, $opi, true);
        $dex_format      = number_format($dex, 3, ',', '.');

      // ONE DEX FETCH DATA FOR CALC
        $out .= '<td data-order="'.$dex.'" class="gadw tdright"> ' . $dex_format . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$this->location][3].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$this->location][3], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$this->location][0].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$this->location][0], 0, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$this->location][1].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$this->location][1], 2, ',', '.')  . ' </td>';
        $out .= '<td data-order="'.$this->googleadwords[$keyword][$this->location][2].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$this->location][2], 2, ',', '.')  . ' </td>';

      }

      $this->csvarr[$i] = array(
          $i,
          $keyword,
          $dex_format,
          number_format($this->googleadwords[$keyword][$this->location][3], 0, ',', '.'),
          number_format($this->googleadwords[$keyword][$this->location][0], 0, ',', '.'),
          number_format($this->googleadwords[$keyword][$this->location][1], 2, ',', '.'),
          number_format($this->googleadwords[$keyword][$this->location][2], 2, ',', '.')
        );

      // RANKING SETS
      $ii = 0;
      foreach ($this->rankset['_ts'] as $k => $ts) {

        $ii++;

        // NO RANKING TODAY
        if (!isset($timestamp[$ts])) {

          $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$this->location.'/'.$this->rs_type.'">TOP100</a></small></td>';

          $yesterday = parent::getOneWeekAgoYMD($ts);
          if (isset( $this->rankset['_da'][$keyword][$yesterday][0]['rank'] )) {
            $change = $this->checkDifferenceBetweenRankings(NULL, $this->rankset['_da'][$keyword][$yesterday][0]['rank']);
            $change_csv = $this->checkDifferenceBetweenRankings(NULL, $this->rankset['_da'][$keyword][$yesterday][0]['rank'], false);
          } else {
            $change = '';
            $change_csv = '';
          }

          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change .'</td>';
          }

          if (isset($this->chart[$keyword])) {
            array_push($this->chart[$keyword],  null);
          } else {
            $this->chart[$keyword] = array( null);
          }

          //$out .= '<td class="rank"> </td>';

          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], $change_csv);
          array_push($this->csvarr[$i], '-');
          array_push($this->csvarr[$i], '');

        // RANKINGS TODAY
        } else {

          //multiple rankings
          if (count($timestamp[$ts]) > 1) {

            // sort multiple rankings, lowest first
            krsort($timestamp[$ts]);

            $out .= '<td class="rank"';
            $k = 0;

            $matched_url = array();
            foreach ($timestamp[$ts] as $value) {

              // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
              if (isset($matched_url[$value['url']])) {
                continue;
              }
              $matched_url[$value['url']] = $value['url'];

              $yesterday = parent::getOneWeekAgoYMD($ts);

              if ($k == 0) {

                if (isset($this->chart[$keyword])) {
                  array_push($this->chart[$keyword], intval($value['rank']));
                } else {
                  $this->chart[$keyword] = array(intval($value['rank']));
                }

                $compare_index = count($this->rankset['_da'][$keyword][$yesterday]) - 1;
                $csv = $value['rank'];
                $csv_url = $value['url'];
                $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank']);
                $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank'], false);
              } else {
                $csv .= ' / ' . $value['rank'];
                $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
              }
              $k++;

            }

            $out .= '<small class="rankings"><a target="_blank" href="../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'/'.$this->location.'/'.$this->rs_type.'">TOP100</a></small>';

            $out .= '</td>';

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change.'</td>';
            }

            array_push($this->csvarr[$i], $csv);
            array_push($this->csvarr[$i], $change_csv);
            array_push($this->csvarr[$i], $csv_url);

          } else {

          //single rankings
            foreach ($timestamp[$ts] as $value) {

              $yesterday = parent::getOneWeekAgoYMD($ts);

              if (empty($value['rank'])) {
                $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'/'.$this->location.'/'.$this->rs_type.'">TOP100</a></small></td>';
              } else {
                $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small class="rankings"><a target="_blank" href="../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'/'.$this->location.'/'.$this->rs_type.'">TOP100</a></small></td>';
              }

              if (isset($this->chart[$keyword])) {
                array_push($this->chart[$keyword], intval($value['rank']));
              } else {
                $this->chart[$keyword] = array(intval($value['rank']));
              }

              $compare_index = count($this->rankset['_da'][$keyword][$yesterday]) - 1;
              $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank']);
              $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$yesterday][$compare_index]['rank'], false);
              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'.$change .'</td>';
              }

              array_push($this->csvarr[$i], $value['rank']);
              array_push($this->csvarr[$i], $change_csv);
              array_push($this->csvarr[$i], $value['url']);

            }

          }

        }


      }

      $out .= '</tr>';

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords');

    $outhead .= '<div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header">';

            if ($kind == 'keyword') {
              $outhead .= '<div class="titleleft">Rankings für das Keyword <span class="red">'.$this->keyword.'</span> am Ort <span class="red">' . $this->locations[$this->location] . '</span> in <span class="red">' . $this->rsTypeLocal($this->rs_type) . '</span></div><div class="titleright">Wochenansicht</div>';
            } else {
              $outhead .= '<div class="titleleft">Rankings für das Keywordset <span class="red">'.$this->setname.'</span> am Ort <span class="red">' . $this->locations[$this->location] . '</span> in <span class="red">' . $this->rsTypeLocal($this->rs_type) . '</span></div><div class="titleright">Wochenansicht</div>';
            }

    $outhead .= '
            </div>
            <div class="box-content padded">
              <div class="row"><div class="col-md-12"><div id="ops-histochart"></div></div></div>
            </div>
          </div>
        </div>

      </div>';

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Zeitraum: maximal 12 Monate (sofern Ranking Daten vorhanden sind)</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td><td>OneDex</td><td>OPI</td><td>SV</td><td>CPC</td><td>Wettb.</td>';

    $this->out = $outhead . $out;

  }


  /*
  KEYWORD -> DAILY RANKINGS -> CHANGE
  https://oneproseo.advertising.de/dev-oneproseo/rankings/local/23/detail-keyword/93
  */

  private function renderViewLocations ($kind)
  {

    $outhead = '';
    $out = '';

    $this->csvarr['head'] = array('#', 'Keyword', 'Ort', 'OneDex', 'OPI', 'Search Volume', 'CPC', 'Competition');

    // merge ranking result with keywordset for keywords that do not rank
    if (!isset($this->url_campaign)) {
      foreach ($this->keyword_loc as $id => $kw_loc) {
        if (!isset($this->rankset['_da'][$kw_loc[0]][$kw_loc[1]])) {
           $this->rankset['_da'][$kw_loc[0]][$kw_loc[1]] = '';
        }
      }
    }

    // sort by key
    ksort($this->rankset['_da']);

    // TABLE HEADER - DATES
    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {

      $this->chartdates[] = parent::germanTSred($ts);

      $ii++;
      $out .= '<td>Rank - '. parent::germanTS($ts) .'</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td style="width:50px;">Change</td>';
      }
      array_push($this->csvarr['head'], 'Rank ' . $ts);
      array_push($this->csvarr['head'], 'Change');
      array_push($this->csvarr['head'], 'URL');
    }
    $out .= '</tr></thead><tbody>';


    // TABLE BODY - CONTENT
    $i = 0;

    foreach ($this->rankset['_da'] as $keyword => $set) {

      foreach ($set as $loc => $timestamp) {

        $i++;

        $out .= '<tr>';
        $out .= '<td class="rank">' . $i . '</td>';
        $out .= '<td class="rank"><a href="http://www.google.de/trends/explore#q=' . $keyword . '&geo=DE&cmpt=q" target="_blank">' . $keyword . '</a><small target="_blank" class="rankings"><a target="_blank" href="https://next.sistrix.de/seo/ranking_history/keyword/' . $keyword . '/domain/'.$this->hostname.'">SISTRIX</a></small></td>';
        $out .= '<td class="rank">' . $this->rs_locations[$loc] . '</td>';

        // NO ADWORDS DATA
        if (!isset($this->googleadwords[$keyword][$loc])) {

          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';
          $out .= '<td data-order="0" class="gadw tdright"> N/A </td>';

        // ADWORDS DATA
        } else {

        // ONE DEX FETCH DATA FOR CALC

          $latest_ts = array_keys($this->rankset['_ts']);
          $latest_ts = $latest_ts[0];

          if (isset($timestamp[$latest_ts][$this->rs_type][0])) {
            $rank = $timestamp[$latest_ts][$this->rs_type][0]['rank'];
          } else {
            $rank = 101;
          }

          $opi        = $this->googleadwords[$keyword][3];
          $dex        = $this->calcOneDex($rank, $opi, true);
          $dex_format = number_format($dex, 3, ',', '.');

        // ONE DEX FETCH DATA FOR CALC
          $out .= '<td data-order="'.$dex.'" class="gadw tdright"> ' . $dex_format . ' </td>';
          $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][3].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][3], 0, ',', '.')  . ' </td>';
          $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][0].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][0], 0, ',', '.')  . ' </td>';
          $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][1].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][1], 2, ',', '.')  . ' </td>';
          $out .= '<td data-order="'.$this->googleadwords[$keyword][$loc][2].'" class="gadw tdright"> ' . number_format($this->googleadwords[$keyword][$loc][2], 2, ',', '.')  . ' </td>';

        }

        $this->csvarr[$i] = array(
            $i,
            $keyword,
            $this->rs_locations[$loc],
            $dex_format,
            number_format($this->googleadwords[$keyword][$loc][3], 0, ',', '.'),
            number_format($this->googleadwords[$keyword][$loc][0], 0, ',', '.'),
            number_format($this->googleadwords[$keyword][$loc][1], 2, ',', '.'),
            number_format($this->googleadwords[$keyword][$loc][2], 2, ',', '.')
          );


        // RANKING SETS
        $ii = 0;
        foreach ($this->rankset['_ts'] as $k => $ts) {

          $ii++;

          // NO RANKING TODAY
          if (!isset($timestamp[$ts][$this->rs_type])) {

            $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
            $yesterday = parent::getOneWeekAgoYMD($ts);
            if (isset( $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][0]['rank'] )) {
              $change = $this->checkDifferenceBetweenRankings(NULL, $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][0]['rank']);
              $change_csv = $this->checkDifferenceBetweenRankings(NULL, $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][0]['rank'], false);
            } else {
              $change = '';
              $change_csv = '';
            }

            if ($ii < count($this->rankset['_ts'])) {
              $out .= '<td>'.$change .'</td>';
            }

            if (isset($this->chart[$this->rs_locations[$loc]])) {
              array_push($this->chart[$this->rs_locations[$loc]],  null);
            } else {
              $this->chart[$this->rs_locations[$loc]] = array( null);
            }

            array_push($this->csvarr[$i], '-');
            array_push($this->csvarr[$i], $change_csv);
            array_push($this->csvarr[$i], '-');

          // RANKINGS TODAY
          } else {

            //multiple rankings
            if (count($timestamp[$ts][$this->rs_type]) > 1) {

              // sort multiple rankings, lowest first
              krsort($timestamp[$ts][$this->rs_type]);

              $out .= '<td class="rank"';
              $k = 0;

              $matched_url = array();
              foreach ($timestamp[$ts][$this->rs_type] as $value) {

                // this is an evil hack .. due to the scraper architecture there might be duplicate rankings for a specific day and keyword
                if (isset($matched_url[$value['url']])) {
                  continue;
                }
                $matched_url[$value['url']] = $value['url'];

                $yesterday = parent::getOneWeekAgoYMD($ts);

                if ($k == 0) {
                  $compare_index = count($this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type]) - 1;

                  if (isset($this->chart[$this->rs_locations[$loc]])) {
                    array_push($this->chart[$this->rs_locations[$loc]], intval($value['rank']));
                  } else {
                    $this->chart[$this->rs_locations[$loc]] = array(intval($value['rank']));
                  }

                  $csv = $value['rank'];
                  $csv_url = $value['url'];
                  $out .= 'data-order="'.$value['rank'].'"><a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a>';
                  $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank']);
                  $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank'], false);
                } else {
                  $csv .= ' / ' . $value['rank'];
                  $out .= ' <span class="/multiranking">/</span> <a class="rankings" target="_blank" href="'.$value['url'].'">' . $value['rank'] . '</a> ';
                }
                $k++;

              }

              $out .= '<small class="rankings"><a target="_blank" href="../../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']). '/' .$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small>';
              $out .= '</td>';

              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>'.$change.'</td>';
              }

              array_push($this->csvarr[$i], $csv);
              array_push($this->csvarr[$i], $change_csv);
              array_push($this->csvarr[$i], $csv_url);

            //single rankings
            } else {

              foreach ($timestamp[$ts][$this->rs_type] as $value) {

                $yesterday = parent::getOneWeekAgoYMD($ts);

                if (empty($value['rank'])) {
                  $out .= '<td data-order="999" class="rank"> - <small class="rankings"><a target="_blank" href="../../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($ts).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
                } else {
                  $out .= '<td data-order="'.$value['rank'].'" class="rank"><a class="rankings" target="_blank" href="'.$value['url'].'">'. $value['rank'] .'</a><small target="_blank" class="rankings"><a target="_blank" href="../../../../../../keywords/results/'.urlencode($keyword).'/'.urlencode($value['ts']).'/'.$this->country.'/'.$loc.'/'.$this->rs_type.'">TOP100</a></small></td>';
                }

                if (isset($this->chart[$this->rs_locations[$loc]])) {
                  array_push($this->chart[$this->rs_locations[$loc]], intval($value['rank']));
                } else {
                  $this->chart[$this->rs_locations[$loc]] = array(intval($value['rank']));
                }

                $compare_index = count($this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type]) - 1;
                $change = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank']);
                $change_csv = $this->checkDifferenceBetweenRankings($value['rank'], $this->rankset['_da'][$keyword][$loc][$yesterday][$this->rs_type][$compare_index]['rank'], false);
                if ($ii < count($this->rankset['_ts'])) {
                  $out .= '<td>'.$change .'</td>';
                }

                array_push($this->csvarr[$i], $value['rank']);
                array_push($this->csvarr[$i], $change_csv);
                array_push($this->csvarr[$i], $value['url']);

              }

            }

          }

        }

        $out .= '</tr>';

      }

    }

    $out .= '</tr></tbody></table></div></div></div></div>';

    // WRITE CSV
    $csv_filename = parent::csvBuilder($this->csvarr, 'export_ruk_keywords_local');

    // OUTPUT
    $outhead .= '<div class="row">

        <div class="col-md-12">

          <div class="box">
            <div class="box-header">
                <ul class="box-toolbar"><div class="titleleft">Rankings für das Keyword <span class="red">'.$this->keyword.'</span> an allen Orten des Sets <span class="red"> '.$this->setname.' </span> in <span class="red">' . $this->rsTypeLocal($this->rs_type) . '</span></div><div class="titleright">Wochenansicht</div></ul>
            </div>
            <div class="box-content padded">
              <div class="row"><div class="col-md-12"><div id="ops-histochart"></div></div></div>
            </div>
          </div>
        </div>
      </div>';

    $outhead .='<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                 <span class="title">Zeitraum: maximal 12 Monate (sofern Ranking Daten vorhanden sind)</span>
                   <ul class="box-toolbar">
                     <li><a title="CSV download" class="icon-cloud-download csv-request" href="'. $this->env_data['csvurl'] . $csv_filename . '" target="_blank"></a></li>
                     <li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProRanker | Ranking Export" data-filepath="'. $this->env_data['csvstore'] . $csv_filename.'"></i></li>
                   </ul>
                </div>
                <div class="box-content">
                <table class="table table-normal data-table dtoverflow">
                  <thead>
                    <tr>
                      <td style="width:50px;">#</td><td>Keyword</td><td>Ort</td><td>OneDex</td><td>OPI</td><td>SV</td><td>CPC</td><td>Wettb.</td>';

    $this->out = $outhead . $out;

  }


  private function renderEditSet () {

    if ($_SESSION['role2'] == 'admin' && $this->keyword_setid != 'all') {
      return '<li><a target="_blank" href="../../../../../keywords/'.$this->keyword_setid.'"> <i title="Edit set" class="icon-edit setedit"></i> </a></li>';
    }

  }


  private function checkForRankingsHint () {

    if (is_null($this->rankset['_ts'])) {
      $this->out = '<div class="row"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für die Keywords oder URLs in diesem Set wurden keine Rankings für die letzten Monaten gefunden!</strong></div></div></div>';
      echo json_encode (array('html' => $this->out));
      exit;
    }

  }


}

?>
