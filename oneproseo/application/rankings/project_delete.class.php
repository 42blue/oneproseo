<?php

require_once('ruk.class.php');

class project_delete extends ruk
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->rankingspy = new rankingspy();

    $this->appHandler();

    parent::mySqlClose();

  }


  private function appHandler ()
  {

    if (isset($_POST['id'])) {
      $this->removeProject();
      echo json_encode($this->out);
    }

  }


  private function removeProject ()
  {

    $project_id = strip_tags($_POST['id']);
    $local      = false;

    // RANKING SPY DELETE SETS
    $sql = "SELECT
              b.rankingspy_id AS rankingspy_id,
              a.local         AS local
            FROM ruk_project_customers a
              INNER JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
            WHERE a.id = '".$project_id."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if ($row['rankingspy_id'] != 0) {
        $ret = $this->rankingspy->removeKeywordSet($row['rankingspy_id']);
      }
      if ($row['local'] != 0) {
        $local = true;
      }

    }

    if ($local == true) {
      $this->removeLocal($project_id);
    }

    // remove project
    // remove project keyword set
    // remove project keyword set keywords

    $sql = "DELETE
              a.*, b.*, c.*
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
              LEFT JOIN ruk_project_keywords c
                ON b.id = c.id_kw_set
            WHERE a.id ='$project_id'";

    $this->db->query($sql);

    if ($this->db->affected_rows > 0) {
      $this->out = array ('success' => 'Das Projekt wurde geöscht!');
    } else {
      $this->out = array ('error' => 'Ein DB Fehler ist aufgetreten. Bitte informieren Sie den Administrator!');
    }

  }

  private function removeLocal ($project_id)
  {

    $id_kw_sets = array();
    $sql = "SELECT id FROM ruk_local_keyword_sets WHERE id_customer = '" . $project_id . "'";
    $result = $this->db->query($sql);
    while ($row = $result->fetch_assoc()) {
      $id_kw_sets[] = $row['id'];
    }

    if (empty($id_kw_sets)) {
      return;
    }

    foreach ($id_kw_sets as $kw_set_id) {

      // GET RS IDs and remove on RS SERVER
      $sql = "SELECT id_rs FROM ruk_local_sets2rs WHERE id_kw_set = '" . $kw_set_id . "'";
      $result = $this->db->query($sql);
      $rankingspy_id = array();
      while ($row = $result->fetch_assoc()) {
        $rankingspy_id[] = $row['id_rs'];
      }
      foreach ($rankingspy_id as $rs_id) {
        $ret = $this->rankingspy->removeKeywordSet($rs_id);
      }

      // REMOVE FROM RUK DB
      $query = "DELETE a.*, b.*, c.*
                FROM ruk_local_keyword_sets a
                  LEFT JOIN ruk_local_keywords b
                    ON b.id_kw_set = a.id
                  LEFT JOIN ruk_local_sets2rs c
                    ON c.id_kw_set = a.id
                WHERE a.id ='".$kw_set_id."'";

      $this->db->query($query);

    }

  }

}

?>
