<?php

header("Content-Type: text/html; charset=utf-8");

class landingpagefinder {

  private $scrapekeywords = array();

  public function __construct ($env_data)
  {
    
    $this->env_data = $env_data;

    $this->country   = $_POST['lang'];
    $this->lang      = $_POST['lang'];
    $this->hostname  = $this->createProperHostname($_POST['url']);
    $this->keywords  = $this->chunkKeywordSet($_POST['keywords']);

    if (empty($this->hostname) or empty($this->keywords)) {
      echo '<div class="row padded"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Bitte eine URL und Keywords angeben.<br/></strong></div></div></div>';
      exit;
    }

    foreach ($this->keywords as $key => $keyword) {
      $q = 'site:'. $this->hostname . ' ' . $keyword;
      $this->scrapekeywords[] = array($q, $this->lang);
    }

    $this->rankings = $this->fetchLiveRankings();

    $this->output();

  }


  private function output () {

    $out_table = '';
    $csv = array();

    foreach ($this->keywords as $keyword) {

      $q = 'site:'. $this->hostname . ' ' . $keyword;

      $url   = '-';
      $title = '-';
      $url_out = '-';

      if (isset($this->rankings[$q]['results'][0]['url'])) {
        $url   = $this->rankings[$q]['results'][0]['url'];
        $title = utf8_decode ($this->rankings[$q]['results'][0]['title'] );
        $url_out = '<a href="' . $url . '" target="_blank">' . $url . '</a>';
      }
    
      $out_table .= '

        <tr>
          <td title="'.$q.'">'. $keyword . '</td>
          <td>'. $url_out .'</td>
          <td>'. $title .'</td>
        </tr>';

      $csv[] = array ($keyword, $url, $title);

    }



    $csvh[]   = array('Keyword', 'Landingpage', 'Title');
    $csvn     = array_merge($csvh, $csv);
    $filename = $this->writeCsv($csvn);

    echo '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title">Landingpages für den Host: '.$this->hostname.'</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProSEA | Landingpage Finder" data-filepath="' . $this->env_data['csvstore'] . $filename . '"></i></li></ul></div><div class="box-content">';

    echo '<table class="table table-normal data-table">
            <thead>
              <tr>
                <td>Keyword</td>
                <td>Landingpage</td>
                <td>Titel</td>
              </tr>
            </thead>';

    echo $out_table;

    echo '</table></div></div></div>';

  }


  private function chunkKeywordSet () {

    $kw   = strip_tags($_POST['keywords']);
    $kw   = strip_tags($kw);
    $kw   = explode("\n", $kw);
    $kw   = array_map('trim', $kw);
    $umla = array('Ä', 'Ü', 'Ö');
    $umlo = array('ä', 'ü', 'ö');
    $kws  = array();

    foreach ($kw as $value) {
      if (empty($value)) {
        continue;
      }
      $value = str_replace($umla, $umlo, $value);
      $value = strtolower($value);
      $value = preg_replace('/\s+/', ' ',$value);
      $kws[$value] = $value;
    }

    // slice @ 100 keywords
    if (count($kws) > 100) {
      $kws = array_slice($kws, 0, 100);
    }

    $kws = array_values($kws);

    return $kws;

  }


  private function fetchLiveRankings ()
  {

    $payload = json_encode($this->scrapekeywords);
    $payload = base64_encode($payload);

    $postdata = http_build_query(array('data' => $payload));
    $opts     = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    $context = stream_context_create($opts);
    $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/trustedproxy/tpscraper.php', false, $context);

    $json = json_decode($output, true);

    return $json;

  }


  public function createProperHostname ($url)
  {

    $url = strtolower(trim($url));

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));

  }

  public function formatnumber($num) {
    return number_format($num, 2, ',', '.');
  }

  private function writeCsv ($csv) {

    $filename = 'sea-landingpagefinder-data-'.time().'.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');

    foreach ($csv as $k => $line) {
      fputcsv($fp, $line);
    }

    fclose($fp);

    return $filename;

  }

}

?>
