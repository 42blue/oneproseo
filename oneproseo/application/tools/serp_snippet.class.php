<?php


header("Content-Type: application/json; charset=utf-8");
libxml_use_internal_errors(true);

class serp_snippet {

  protected $timeout = 30;
  protected $titleTagNormal = '';
  protected $titleMDNormal  = '';
  protected $breadcrumb     = '';

  public function __construct() {

    $this->url = strip_tags($_GET['url']);

    if (empty($this->url)) {
      exit ('{error: params missing}');
    }

    $this->urlHost = parse_url($this->url, PHP_URL_SCHEME) . '://' . parse_url($this->url, PHP_URL_HOST);

    $site = $this->makeCurlRequest();

    $site[0] = preg_replace('/<svg\b[^<]*(?:(?!<\/svg>)<[^<]*)*<\/svg>/i','', $site[0]);

    $mNormal = @mb_convert_encoding($site[0], 'utf-8', mb_detect_encoding($site[0]));
    $mNormal = @mb_convert_encoding($mNormal, 'html-entities', 'utf-8');

    $this->createDomDocument($mNormal);
    $this->titleTag();

    echo json_encode(array('tt' => $this->titleTagNormal, 'md' => $this->titleMDNormal, 'bc' => $this->breadcrumb));

  }


  public function makeCurlRequest () {

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $this->url);
    curl_setopt($curl, CURLOPT_HEADER, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)' );
    curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
    curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');

    $curlExec = curl_exec($curl);
    $curlRaw = $curl;

    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $header      = substr($curlExec, 0, $header_size);
    $body        = substr($curlExec, $header_size);

    return array ($body, $curlRaw, $header);

  }

  public function createDomDocument ($html) {
    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);
    $this->websiteDOMNormal = $dom;
    $this->websiteXPATHNormal = $xpath;
  }

  private function titleTag() {

    $tt = $this->websiteXPATHNormal->query('//title');
    foreach ($tt as $node) {
      $this->titleTagNormal = trim($node->nodeValue);
    }

    $xpath_desc = $this->websiteXPATHNormal->query('//meta[@name="description"]/@content');
    foreach ($xpath_desc as $node) {
      $this->titleMDNormal = trim($node->nodeValue);
    }

  }

}

?>
