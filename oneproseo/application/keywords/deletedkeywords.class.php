<?php

class deletedkeywords {


  public function __construct ($env_data)
  {

    $this->env_data = $env_data;
    $this->mySqlConnect();

    $this->fetchKeywordsFromProjects();

    $this->mySqlClose();

  }


  private function fetchKeywordsFromProjects ()
  {

    // Paging
    $sLimit = '';
    if (isset($_GET['start']) && $_GET['length'] != '-1') {
      $sLimit = 'LIMIT '. $_GET['start'] .', '. $_GET['length'] ;
    }

    // SORTING
    $sOrder = '';
    if ( isset( $_GET['order']) )
    {
      $index     = $_GET['order'][0]['column'];
      $direction = $_GET['order'][0]['dir'];
      $columns   = $_GET['columns'];
      $cellname  = $columns[$index]['data'];

      $sOrder = 'ORDER BY '. $cellname . ' '. $direction;

    }

    // FILTER
    $sWhere = '';
    if ($_GET['search']['value'] != '') {
      $sWhere = 'WHERE (';
      $sWhere .= "keyword LIKE '%". $_GET['search']['value'] ."%' df";
      $sWhere = substr_replace( $sWhere, '', -3 );
      $sWhere .= ')';
    }

//echo $sWhere;

    $data = array();

    $sql = "SELECT
              a.keyword AS keyword,
              a.id_kw_set AS id_kw_set,
              b.id,
              b.id_customer,
              b.name AS setname,
              c.id AS id_customer,
              c.name AS customer
            FROM
              ruk_project_keywords_deleted a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id = a.id_kw_set
              LEFT JOIN ruk_project_customers c
                ON c.id = b.id_customer
            $sWhere
            $sOrder
            $sLimit";

    $res2 = $this->db->query($sql);
    $res2->num_rows;

    while ($row = $res2->fetch_assoc()) {
     // $data[] = $row;

       $data[] = array ('keyword'  => $row['keyword'],
                        'customer' => '<a target="_blank" href="../rankings/overview/'. $row['id_customer'] .'">' . $row['customer'] . '</a>',
                        'setname'  => $row['setname']
                       );

    }

    // COUNT ALL
    $sql = "SELECT
              COUNT(DISTINCT keyword)
            FROM
              ruk_project_keywords_deleted";

    $count = $this->db->query($sql);
    $count = $count->fetch_array(MYSQLI_NUM);
    $recordsFiltered = $count;

    if (!empty($sWhere)) {
      $recordsFiltered = $res2->num_rows;
    }

    // OUTPUT
    $output = array ('draw' => $_GET['draw'], 
                     'recordsTotal' => $count,
                     'recordsFiltered' => $recordsFiltered,
                     'data' => $data);


    echo json_encode($output);

  }


  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB 
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }


  public function mySqlClose ()
  {

    $this->db->close();

  }

}

?>