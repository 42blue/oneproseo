<?php

date_default_timezone_set('CET');

class traveldb {


  public function __construct ($env_data)
  {

    $this->env_data = $env_data;
    $this->mySqlConnect();

    $this->fetchKeywordsFromProjects();

    $this->mySqlClose();

  }


  private function fetchKeywordsFromProjects ()
  {


    // Paging
    $sLimit = '';
    if (isset($_GET['start']) && $_GET['length'] != '-1') {
      $sLimit = 'LIMIT '. $_GET['start'] .', '. $_GET['length'] ;
    }

    // SORTING
    $sOrder = '';
    if ( isset( $_GET['order']) )
    {
      $index     = $_GET['order'][0]['column'];
      $direction = $_GET['order'][0]['dir'];
      $columns   = $_GET['columns'];
      $cellname  = $columns[$index]['data'];

      $sOrder = 'ORDER BY '. $cellname . ' '. $direction;

    }

    // FILTER
    $sWhere = '';
    if ($_GET['search']['value'] != '') {
      $sWhere = 'AND (';
      $sWhere .= 'query LIKE "%'. $_GET['search']['value'] .'%"';
      $sWhere .= ')';
    }


    $data = array();

    $today = $this->dateYMD();

    $sql = "SELECT SQL_CALC_FOUND_ROWS
              a.id                AS id,
              a.hostname          AS customer,
              b.query             AS query,
              SUM(b.impressions)  AS impressions,
              SUM(b.clicks)       AS clicks,
              b.ctr               AS ctr,
              b.position_1        AS position,
              c.searchvolume      AS searchvolume,
              c.cpc               AS cpc,
              c.competition       AS competition,
              c.opi               AS opi,
              d.id                AS rank
            FROM
              gwt_hostnames a
              LEFT JOIN gwt_data b
                ON b.hostname_id = a.id
              LEFT JOIN gen_keywords c
                ON c.keyword = b.query
              LEFT JOIN ruk_scrape_keywords d
                ON b.query = d.keyword
            WHERE a.id IN ('5','8','13','15')
            AND d.timestamp = '$today'
            $sWhere
            GROUP BY customer, query 
            $sOrder
            $sLimit";


    $sql2 = "SELECT FOUND_ROWS()";

    $res2 = $this->db->query($sql);
    $res3 = $this->db->query($sql2);

    $filter_count = $res2->num_rows; 
    $total_rcount = $res3->fetch_row();

    $cid = array();

    while ($row = $res2->fetch_assoc()) {

     if (!empty($row['customer'])) {

        $cid = $row['rank'];
        $r_rank = ' - ';

        if (!empty ($cid)) {

          $sql2 = "SELECT
                   id,
                   position,
                   hostname,
                   url
                 FROM
                   ruk_scrape_rankings 
                  WHERE 
                   id_kw = $cid";

          $res3 = $this->db->query($sql2);

          while ($row2 = $res3->fetch_assoc()) {
            if ($row2['hostname'] == $row['customer']) {
              $r_rank = '<a href="'.$row2['url'].'" target="_blank">' . $row2['position'] . '</a>';
            }
          }

        }

       $data[] = array ('customer'     => $row['customer'],
                        'query'        => $row['query'],
                        'impressions'  => $row['impressions'],
                        'clicks'       => $row['clicks'],
                        'ctr'          => $row['ctr'] . '%',
                        'position'     => $row['position'],
                        'opi'          => $row['opi'],
                        'rank'         => $r_rank
                       );


      }

    }

    // OUTPUT
    $output = array ('draw' => $_GET['draw'], 
                     'recordsTotal' => $total_rcount,
                     'recordsFiltered' => $total_rcount,
                     'data' => $data);


    echo json_encode($output);

  }


  // YMD 
  public function dateYMD ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB 
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }


  public function mySqlClose ()
  {

    $this->db->close();

  }

}

?>