<?php

class keywords {


  public function __construct ($env_data)
  {

    $this->env_data = $env_data;
    $this->mySqlConnect();

    $this->fetchKeywordsFromProjects();

    $this->mySqlClose();

  }


  private function fetchKeywordsFromProjects ()
  {

    // Paging
    $sLimit = '';
    if (isset($_GET['start']) && $_GET['length'] != '-1') {
      $sLimit = 'LIMIT '. $_GET['start'] .', '. $_GET['length'] ;
    }

    // SORTING
    $sOrder = '';
    if ( isset( $_GET['order']) )
    {
      $index     = $_GET['order'][0]['column'];
      $direction = $_GET['order'][0]['dir'];
      $columns   = $_GET['columns'];
      $cellname  = $columns[$index]['data'];

      $sOrder = 'ORDER BY '. $cellname . ' '. $direction;

    }

    // FILTER
    $sWhere = '';
    if ($_GET['search']['value'] != '') {
      $sWhere = 'WHERE (';
      for ($i = 0; $i < count($_GET['columns']); $i++)
      {
        $sWhere .= $columns[$i]['data'].' LIKE "%'. $_GET['search']['value'] .'%" OR ';
      }
      $sWhere = substr_replace( $sWhere, '', -3 );
      $sWhere .= ')';
    }


    $data = array();

    $sql = "SELECT
              DISTINCT keyword,
              searchvolume,
              cpc,
              competition,
              opi
            FROM
              gen_keywords
            $sWhere
            $sOrder
            $sLimit";

    $res2 = $this->db->query($sql);
    $res2->num_rows;

    while ($row = $res2->fetch_assoc()) {
     // $data[] = $row;
     if (!empty($row['keyword'])) {
       $data[] = array ('keyword' => $row['keyword'], 
                         'searchvolume' => number_format($row['searchvolume'], 0, ',', '.'), 
                         'cpc' => number_format($row['cpc'], 2, ',', '.'), 
                         'competition' => number_format($row['competition'], 2, ',', '.'),
                         'opi' => number_format($row['opi'], 0, ',', '.'), );
     }

    }

    // COUNT ALL
    $sql = "SELECT
              COUNT(DISTINCT keyword)
            FROM
              gen_keywords";

    $count = $this->db->query($sql);
    $count = $count->fetch_array(MYSQLI_NUM);
    $recordsFiltered = $count;

    if (!empty($sWhere)) {
      $recordsFiltered = $res2->num_rows;
    }

    // OUTPUT
    $output = array ('draw' => $_GET['draw'], 
                     'recordsTotal' => $count,
                     'recordsFiltered' => $recordsFiltered,
                     'data' => $data);


    echo json_encode($output);

  }


  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB 
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }


  public function mySqlClose ()
  {

    $this->db->close();

  }

}

?>