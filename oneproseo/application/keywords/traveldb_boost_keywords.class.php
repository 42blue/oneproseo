<?php

class traveldb_boost_keywords {


  public function __construct ($env_data)
  {
    $this->env_data = $env_data;
    $this->mySqlConnect();
    $this->fetchKeywordsFromProjects();
    $this->mySqlClose();
  }


  private function fetchKeywordsFromProjects ()
  {

    // Paging
    $sLimit = '';
    if (isset($_GET['start']) && $_GET['length'] != '-1') {
      $sLimit = 'LIMIT '. $_GET['start'] .', '. $_GET['length'] ;
    }

    // SORTING
    $this->sortcellname = '';
    $this->sortdirection = '';
    $sOrder = '';
    if ( isset( $_GET['order']) )
    {
      $index     = $_GET['order'][0]['column'];
      $direction = $_GET['order'][0]['dir'];
      $columns   = $_GET['columns'];
      $cellname  = $columns[$index]['data'];

      if ($cellname == 'opi' || $cellname == 'keyword') {
        $sOrder = 'ORDER BY '. $cellname . ' '. $direction;
      } else {
        if ($cellname == 'boost' || $cellname == 'rank') {
         $cellname = $cellname . '_sort';
        }
        $this->sortcellname  = $cellname;
        $this->sortdirection = $direction;
      }

    }

    // FILTER
    $sWhere = '';
    if ($_GET['search']['value'] != '') {
      $sWhere = 'AND (';
      $sWhere .= 'query LIKE "%'. $_GET['search']['value'] .'%"';
      $sWhere .= ')';
    }


    $data = array();
    $index = 0;

    $today = $this->dateYMD();

    $sql = "SELECT SQL_CALC_FOUND_ROWS
              a.id                AS id,
              a.hostname          AS customer,
              b.query             AS keyword,
              SUM(b.impressions)  AS onedexpot,
              SUM(b.clicks)       AS onedex,
              b.ctr               AS onedexmax,
              b.position_1        AS boost,
              c.searchvolume      AS searchvolume,
              c.cpc               AS cpc,
              c.competition       AS competition,
              c.opi               AS opi,
              d.id                AS rank
            FROM
              gwt_hostnames a
              LEFT JOIN gwt_data b
                ON b.hostname_id = a.id
              LEFT JOIN gen_keywords c
                ON c.keyword = b.query
              LEFT JOIN ruk_scrape_keywords d
                ON b.query = d.keyword
            WHERE a.id IN ('5','8','13','15')
            AND b.impressions > 10
            AND b.clicks > 10
            AND d.timestamp = '$today'
            $sWhere
            GROUP BY keyword 
            $sOrder
            $sLimit";


    $sql2 = "SELECT FOUND_ROWS()";

    $res2 = $this->db->query($sql);
    $res3 = $this->db->query($sql2);

    $filter_count = $res2->num_rows; 
    $total_rcount = $res3->fetch_row();

    $cid = array();

    $r_rank = 0;

    while ($row = $res2->fetch_assoc()) {

      $cid = $row['rank'];
      $r_rank = ' - ';

      if (!empty ($cid)) {

        $sql2 = "SELECT
                 id,
                 position,
                 hostname,
                 url
                FROM
                 ruk_scrape_rankings 
                WHERE 
                 id_kw = $cid";

        $res3 = $this->db->query($sql2);

        while ($row2 = $res3->fetch_assoc()) {
          if ($row2['hostname'] == 'weg.de') {
            $r_rank = $row2['position'];
            $r_url  = $row2['url'];
          }
        }

      }

      // keyword / OPI / OneDex / OneDex MAX / Boost Potential / weg.de Rank today 

      $dex = $this->calcOneDex($r_rank, $row['opi']);
      $dex_max = $this->calcOneDexMax($row['opi']);

      $dex_diff = $dex_max - $dex;

      $dex_format      = number_format($dex, 3, ',', '.');
      $dex_max_format  = number_format($dex_max, 3, ',', '.');
      $dex_diff_format = number_format($dex_diff, 3, ',', '.');

      $row['opi'] = number_format($row['opi'], 2, ',', '.');

      $potential = $dex_diff * $row['cpc'] * $row['competition'] * $row['competition'];
      $potential_format = ceil($potential);

      // BOOST NUR WENN RANKING >= 9
      if ($r_rank > 8 || $r_rank == 0) {
        $boost = '<i class="status-error icon-certificate"></i>';
        $boost_sort = 1;
      } else {
        $boost = '';
        $boost_sort = 0;
      }

     $rank = '<a href="' . $r_url . '" target="_blank">' . $r_rank . '</a><small target="_blank" class="rankings"><a target="_blank" href="../rankings/overview/10/keywords/results/'. urlencode($row['keyword']) .'/'.$this->dateYMD().'">TOP100</a></small>';

     $index++;

     $data[] = array (
                      'index'      => $index,
                      'keyword'    => $row['keyword'],
                      'opi'        => $row['opi'],
                      'onedex'     => $dex_format,
                      'onedexmax'  => $dex_max_format,
                      'onedexpot'  => $potential_format,
                      'boost'      => $boost,
                      'boost_sort' => $boost_sort,
                      'rank'       => $rank,
                      'rank_sort'  => $r_rank
                     );

    }

    // MANUAL SORT
    if (!empty ($this->sortcellname)) {
      $data = $this->subval_sort($data, $this->sortcellname, $this->sortdirection);
    }

    // OUTPUT
    $output = array ('draw' => $_GET['draw'], 
                     'recordsTotal' => $total_rcount,
                     'recordsFiltered' => $total_rcount,
                     'data' => $data);


    echo json_encode($output);

  }

  // CUSTOM SORT
  public function subval_sort ($a, $subkey, $order) {

    foreach($a as $k=>$v) {
      $b[$k] = strtolower($v[$subkey]);
    }

    if ($order == 'asc') {
      arsort($b, SORT_NUMERIC);
    } else {
      asort($b, SORT_NUMERIC);
    }

    foreach($b as $key=>$val) {
      $c[] = $a[$key];
    }
    return $c;

  }

  // DEXMAX
  public function calcOneDexMax ($opi) {

    if ($opi == 0) {
      $opi = 1;
    }

    $dex_max = $opi * 100 / 100000;

    return round($dex_max, 3, 0);

  }

  // DEX
  public function calcOneDex ($rank, $opi)
  {

    $rank = (int) $rank;

    if ($rank > 101 || $rank == 0) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) { 

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 3, 0);

  }


  public function dateYMD ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  public function mySqlConnect ()
  {
    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);
    // set charset according to DB 
    $this->db->set_charset('utf8');
    if (mysqli_connect_errno()) {
      echo ('Connect failed:' .  mysqli_connect_error());
    }
  }


  public function mySqlClose ()
  {
    $this->db->close();
  }

}

?>