<?php

header("Content-Type: text/html; charset=utf-8");
libxml_use_internal_errors(true);

class ideas {

  protected $failcount = 0;

  public function __construct ($env_data)
  {

    $this->env_data = $env_data;

    $this->mySqlConnect();

    $this->keyword             = strip_tags($_POST['kw']);
    $this->lang                = strip_tags($_POST['lang']);

    $this->source              = false;

    if (isset($_POST['source'])) {
      $this->source = strip_tags($_POST['source']);      
    }

    $this->rankings_for_url_db = 'true';

    if (empty($this->keyword) || empty($this->lang)) {
      $out['error'] = 'params missing';
      echo json_encode ($out);
      exit;
    }

    require_once ($env_data['path']. 'oneproseo/includes/config.php');
    $this->scraper_hosts = unserialize(SCRAPER_HOSTS);

    require_once ($this->env_data['path']. 'oneproseo/includes/logtofile.class.php');
    $oLogToFile = new logtofile($env_data);


    if ($_POST['type'] == 'ideaskw') {

      $this->url = strip_tags($_POST['url']);
      $this->url = $this->createProperUrl($this->url);

      $this->restrictQuery();

      $oLogToFile->write('KW DB', 'Keyword: ' . $this->keyword, 'temp/logs/log_liveranking.txt');

      $this->querySuggestProxy();
      $this->queryIdeasProxy();
      $this->queryKeywordIdeas();
      $this->querySearchVolume();

    } else if ($_POST['type'] == 'ideasurl') {

      $this->rankings_for_url_db = strip_tags($_POST['rankings']);

      $this->url = strip_tags($_POST['kw']);
      $this->url = $this->createProperUrl($this->url);

      $this->restrictQuery();

      $oLogToFile->write('URL DB', 'Domain: ' . $this->url, 'temp/logs/log_liveranking.txt');

      $this->queryUrlIdeas();

    } else if ($_POST['type'] == 'suggest') {

      $oLogToFile->write('SUGGEST DB', 'Keyword: ' . $this->keyword, 'temp/logs/log_liveranking.txt');

      if ($this->source == 'youtube') {
        $this->queryYouTubeProxy();
      } else {
        $this->queryIdeasProxy();
      }

      $this->querySearchVolume();

    } else if ($_POST['type'] == 'questions') {

      $oLogToFile->write('W-FRAGEN DB', 'Keyword: ' . $this->keyword, 'temp/logs/log_liveranking.txt');

      $this->querySuggestProxy();
      $this->querySearchVolume();

    }

    $this->jumplinktype = $_POST['type'];

    $this->output();

  }


  // restrict querys in customer version to 20
  private function restrictQuery () {

    if (isset($this->url) && !empty($this->url)) {
      require_once ($this->env_data['path']. 'oneproseo/includes/limitquerys.class.php');
      $limit  = new limitquerys($this->db, $this->env_data);
      $limit->checkLimit();
    }

  }

  private function querySuggestProxy() {

    $server = rand(0, 9);

    $opts = array('http' =>
      array('timeout' => 90)
    );

    $context = stream_context_create($opts);
    $data = @file_get_contents('http://'.$this->scraper_hosts[$server].'/suggest/np_ask_google_extended.php?kw=' . urlencode($this->keyword) . '&lang=' . $this->lang . '&sug=false&key=M4B1st2IlvFfihHG42HQRTpE4nny5f', false, $context);

    // FALLBACK
    if ($data === FALSE) {
      $this->querySuggestProxy();
    }

    $json = json_decode($data);

    if (isset($json->error) or $json == NULL or $data == false) {

      $this->failcount++;

      if ($this->failcount > 10) {
        $out['error'] = 'api error';
        echo json_encode ($out);
        exit;
      }

      $this->querySuggestProxy();

    } else {

      $this->resultSuggestQuestions = $json;

    }

  }


  private function queryIdeasProxy() {

    $server = rand(0, 9);
    $opts = array('http' =>
      array('timeout' => 90)
    );

    $context = stream_context_create($opts);

    $data = @file_get_contents('http://'.$this->scraper_hosts[$server].'/suggest/np_google_extended.php?kw=' . urlencode($this->keyword) . '&lang=' . $this->lang . '&key=M4B1st2IlvFfihHG42HQRTpE4nny5f', false, $context);

    // FALLBACK
    if ($data === FALSE) {
      $this->queryIdeasProxy();
    }

    $json = json_decode($data);

    if (isset($json->error) or $json == NULL or $data == false) {

      $this->failcount++;

      if ($this->failcount > 10) {
        $out['error'] = 'api error';
        echo json_encode ($out);
        exit;
      }

      $this->queryIdeasProxy();

    } else {

      $this->resultSuggestIdeas = $json;

    }

  }


  private function queryYouTubeProxy() {

    $server = rand(0, 9);
    $opts = array('http' =>
      array('timeout' => 90)
    );

    $context = stream_context_create($opts);

    $data = @file_get_contents('http://'.$this->scraper_hosts[$server].'/suggest/youtube.php?kw=' . urlencode($this->keyword) . '&lang=' . $this->lang , false, $context);

    // FALLBACK
    if ($data === FALSE) {
      $this->queryYouTubeProxy();
    }

    $json = json_decode($data);

    if (isset($json->error) or $json == NULL or $data == false) {

      $this->failcount++;

      if ($this->failcount > 10) {
        $out['error'] = 'api error';
        echo json_encode ($out);
        exit;
      }

      $this->queryYouTubeProxy();

    } else {

      $this->resultSuggestIdeas = $json;

    }

  }


  private function queryKeywordIdeas () {

    $payload = array($this->keyword);
    $payload = base64_encode(json_encode($payload));

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => 'keyword_arr=' . $payload . '&lang=' . $this->lang . '&country='. $this->lang
      )
    );

    $context = stream_context_create($opts);
    $result  = file_get_contents('https://oneproseo.advertising.de/oneproapi/adwords/service/googleKeywordIdeas.php', false, $context);

    $this->resultIdeas = json_decode($result);

  }


  private function querySearchVolume () {

    $payload[$this->keyword] = array($this->keyword, $this->lang, $this->lang);

    if (isset($this->resultSuggestIdeas)) {
      foreach($this->resultSuggestIdeas as $searchterm => $keywords) {
        if (!empty($keywords)) {
          foreach ($keywords as $keyword) {
            $payload[$keyword] = array($keyword, $this->lang, $this->lang);
          }
        }
      }
    }

    if (isset($this->resultSuggestQuestions)) {
      foreach($this->resultSuggestQuestions as $searchterm => $keywords) {
        if (!empty($keywords)) {
          foreach ($keywords as $keyword) {
            $payload[$keyword] = array($keyword, $this->lang, $this->lang);
          }
        }
      }
    }

    if (empty($payload)) {

      if ($_POST['type'] == 'suggest' or $_POST['type'] == 'questions') {
        echo '<div class="row padded"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Für dieses Keyword wurden keine Ergebnisse gefunden.<br/></strong></div></div></div>';
        exit;
      }

    } else {

      require_once ($env_data['path']. 'oneproseo/includes/svolume.class.php');
      $oSvolume  = new svolume($this->db, $this->env_data);
      $result    = $oSvolume->querySV($payload);

      $results = array();
      foreach ($result as $k => $v) {
        $results[$v[0]] = array($v[1], $v[2], $v[3]);
      }

      $this->resultSV = $results;

    }

  }


  private function queryUrlIdeas () {

    $query = urlencode($this->url);

    $opts = array('http' =>
      array('timeout' => 60)
    );

    $context = stream_context_create($opts);
    $result  = @file_get_contents('https://oneproseo.advertising.de/oneproapi/adwords/service/googleKeywordIdeasUrl.php?lang='.$this->lang.'&url=' . $query, false, $context);

    $this->resultIdeasUrl = json_decode($result);

  }


  private function output() {

    $this->res_kw      = array();
    $this->in_campaign = array();
    $kw_sv             = array();
    $allkeywords       = array();
    $i                 = 0;
    $out               = '';
    $csv               = array();
    $this->source_suggest           = array();
    $this->source_suggest_questions = array();
    $this->source_adwords           = array();
    $this->source_adwords_url       = array();

    // all keywords from suggest
    if (isset($this->resultSuggestIdeas)) {
      foreach($this->resultSuggestIdeas as $searchterm => $keywords) {
        if (!empty($keywords)) {
          foreach ($keywords as $keyword) {
            $this->res_kw[$keyword] = array (0, 0, 0);
            $allkeywords[$keyword]  = $keyword;
            $this->source_suggest[$keyword] = $keyword;
          }
        }
      }
    }

    // all keywords from suggest questions
    if (isset($this->resultSuggestQuestions)) {
      foreach($this->resultSuggestQuestions as $searchterm => $keywords) {
        if (!empty($keywords)) {
          foreach ($keywords as $keyword) {
            $this->res_kw[$keyword] = array (0, 0, 0);
            $allkeywords[$keyword]  = $keyword;
            $this->source_suggest_questions[$keyword] = $keyword;
          }
        }
      }
    }

    if (isset($this->resultIdeas->set)) {
      foreach ($this->resultIdeas->set as $k => $v) {
        $this->res_kw[$v->kw] = array ($v->sv, $v->cpc, $v->comp);
        $allkeywords[$v->kw]  = $v->kw;
        $this->source_adwords[$v->kw] = $v->kw;
      }
    }

    if (isset($this->resultIdeasUrl->set)) {
      foreach ($this->resultIdeasUrl->set as $k => $v) {
        $this->res_kw[$v->kw] = array ($v->sv, $v->cpc, $v->comp);
        $allkeywords[$v->kw]  = $v->kw;
        $this->source_adwords_url[$v->kw] = $v->kw;
      }
    }

    if (isset($this->resultSV)) {
      foreach ($this->resultSV as $k => $v) {
        $this->res_kw[$k] = array ($v[0], $v[1], $v[2]);
        $allkeywords[$k]  = $k;
      }
    }

    if (empty($this->res_kw)) {
    	$out = '<div class="col-md-12"><div class="alert alert-error"><strong>Leider ist die Adwords API gerade überlastet.</strong> Bitte probiere es in ein paar Sekunden nochmal.</div></div>';
    	echo $out;
    	exit;
    }

    // RANKINGS INTERCEPT
    $allkeywords = array_values($allkeywords);
    if (isset($this->url) && !empty($this->url) && $this->rankings_for_url_db != 'false') {

      $this->hostname = $this->createProperHostname($this->url);

      require_once ($env_data['path']. 'oneproseo/includes/onedex.class.php');
      $oOnedex = new onedex;

      // TRUSTED PROXY SCRAPE INTERCEPT
      // -> array_push($this->res_kw[$v['keyword']], $row['position'], $row['url']);
      // -> $this->urls_to_find_rankings_scrape
      $this->fetchRankingsRealtimeScrape($this->res_kw, $this->source_suggest_questions);

    }

    foreach ($this->res_kw as $keyword => $data) {

      $i++;

      $vsv     = ($data[0] == 0) ? 0.01 : $data[0];
      $vcpc    = ($data[1] == 0) ? 0.01 : $data[1];
      $vcomp   = ($data[2] == 0) ? 0.01 : $data[2];
      $opi     = $vsv * $vcpc * $vcomp;
      $opi_f   = $this->formatnumber($opi);
      $cpc_f   = $this->formatnumber($data[1]);
      $comp_f  = $this->formatnumber($data[2]);

// reverse sort data0

      $data_0_sort = $data[0] * (-1);

      // has ranking?
      if (!isset($data[3]) || $data[3] == false) {
        $data[3] = '-';
        $data[4] = '-';
        $data_3_sort = 101;
        $dex = '-';
        $jumplink1 = '';
      } else {
        $data_3_sort = intval($data[3]);
        $dex = $oOnedex->calcOneDex($data[3], $opi);
        $jumplink1 = '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'keywords/url-db?kw='.$data[4].'&lang='.$this->lang.'">URL DB</a></small>';
      }

      // SOURCE
      $marker = $this->checkKeywordSource($keyword);

      // QUESTION OR KEYWORD
      if (isset($this->in_campaign[$keyword])) {
        $in_campaign = 'Ja';
        $in_campaign_sort = 1;
      } else {
        $in_campaign = 'Nein';
        $in_campaign_sort = 0;
      }

      if ($this->jumplinktype == 'ideaskw') {
        $jumplink2 = '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'keywords/w-fragen-db?kw='.$keyword.'&lang='.$this->lang.' ">W-Fragen DB</a> | <a target="_blank" href="'.$this->env_data['domain'].'keywords/suggest-db?kw='.$keyword.'&lang='.$this->lang.' ">Suggest DB</a></small>';
      } elseif ($this->jumplinktype == 'ideasurl') {
        $jumplink1 = '';
        $jumplink2 = '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'keywords/keyword-db?kw='.$keyword.'&lang='.$this->lang.' ">Keyword DB</a> | <a target="_blank" href="'.$this->env_data['domain'].'keywords/w-fragen-db?kw='.$keyword.'&lang='.$this->lang.' ">W-Fragen DB</a> | <a target="_blank" href="'.$this->env_data['domain'].'keywords/suggest-db?kw='.$keyword.'&lang='.$this->lang.' ">Suggest DB</a></small>';
      } elseif ($this->jumplinktype == 'suggest') {
        $jumplink1 = '';
        $jumplink2 = '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'keywords/keyword-db?kw='.$keyword.'&lang='.$this->lang.' ">Keyword DB</a> | <a target="_blank" href="'.$this->env_data['domain'].'keywords/w-fragen-db?kw='.$keyword.'&lang='.$this->lang.' ">W-Fragen DB</a></small>';
      } elseif ($this->jumplinktype == 'questions') {
        $jumplink1 = '';
        $jumplink2 = '<small class="rankings"><a target="_blank" href="'.$this->env_data['domain'].'keywords/keyword-db?kw='.$keyword.'&lang='.$this->lang.' ">Keyword DB</a> | <a target="_blank" href="'.$this->env_data['domain'].'keywords/suggest-db?kw='.$keyword.'&lang='.$this->lang.' ">Suggest DB</a></small>';
      }

      // OUTPUT
      if (!empty($this->hostname)) {
        $out_t .= '<tr><td>'. $i . '</td><td>'.$keyword . $jumplink2 .'</td><td data-sort="'. $data_3_sort .'"><a href="'.$data[4].'" target="_blank">' . $data[3] . '</a></td><td data-order="'.$data[0].'">'.$data[0].'</td><td>'.$marker.'</td><td data-order="'.$opi.'">'.$opi_f.'</td><td data-order="'.$data[1].'">'.$cpc_f.'</td><td data-sort="'.$data[2].'">'.$comp_f.'</td><td data-sort="'. $dex .'">' . $dex . '</td><td data-sort="'. $data_3_sort .'"><a href="'.$data[4].'" target="_blank">' . $data[4] . '</a>' . $jumplink1  . '</td><td data-sort="'. $in_campaign_sort .'">' . $in_campaign . '</td></tr>';
        $csv[] = array($keyword, $data[0], $marker, $opi_f, $data[1], $data[2], $dex, $data[3], $data[4], $in_campaign);
      } else {
        $out_t .= '<tr><td>'. $i . '</td><td>'.$keyword . $jumplink2 .'</td><td data-order="'.$data_0_sort.'">'.$data[0].'</td><td>'.$marker.'</td><td data-order="'.$opi.'">'.$opi_f.'</td><td data-order="'.$data[1].'">'.$cpc_f.'</td><td data-sort="'.$data[2].'">'.$comp_f.'</td></tr>';
        $csv[] = array($keyword, $data[0], $marker, $opi_f, $data[1], $data[2]);
      }

    }

    $csvh = array();
    if (!empty($this->hostname)) {
      $csvh[]   = array('Keyword', 'Suchvolumen / Monat', 'Quelle', 'OPI', 'CPC / Euro', 'Competition', 'OneDex', 'Rank', 'URL', 'Campaign');
    } else {
      $csvh[]   = array('Keyword', 'Suchvolumen / Monat', 'Quelle', 'OPI', 'CPC / Euro', 'Competition', );
    }

    $csvn     = array_merge($csvh, $csv);
    $filename = $this->writeCsv($csvn);

    $out = '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title">' . $i . ' Ergebnisse ';

    if (!empty($this->hostname)) {
      $out .= 'mit den Rankings für: ' . $this->hostname;
    }

    $out .= '</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | Keyworder Export" data-filepath="' . $this->env_data['csvstore'] . $filename . '"></i></li></ul></div><div class="box-content">';

    if (!empty($this->hostname)) {
      $out .= '<table class="table table-normal data-table dtoverflow"><thead><tr><td>#</td><td>Keyword</td><td>Ranking für '.$this->hostname.'</td><td>Suchvolumen / Monat</td><td>Quelle</td><td>OPI</td><td>CPC / Euro</td><td>Competition</td><td>OneDex</td><td>URL</td><td>Campaign</td>';
    } else {
      $out .= '<table class="table table-normal data-table dtoverflow"><thead><tr><td>#</td><td>Keyword</td><td>Suchvolumen / Monat</td><td>Quelle</td><td>OPI</td><td>CPC / Euro</td><td>Competition</td>';
    }

    $out .= '</tr></thead>';
    $out .= $out_t;
    $out .= '</table></div></div></div>';

    echo $out;

  }


  public function formatnumber($num) {
    return number_format($num, 2, ',', '.');
  }


  private function writeCsv ($csv) {

    $filename = 'keywords-data-' . time() . '.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');

    foreach ($csv as $k => $line) {
      fputcsv($fp, $line);
    }

    fclose($fp);

    return $filename;

  }


  public function fetchRankingsRealtimeScrape($res_kw, $suggest_questions) {

    // WENN KEIN RANKING VORLIEGT UND SICH DAS KEYWORD NICHT IN DER KW DB BEFINDET
    // UND DAS SV > 0 ist
    $fetch_live_ranking = array();
    foreach ($res_kw as $keyword => $data) {
      if (!isset($data[3]) && !isset($this->keyword_in_campaign[$keyword]) && $data[0] > 0) {
        $fetch_live_ranking[] = array($keyword, 'de', $data[0]);
      }
    }

    $fetch_live_ranking_suggest = array();
    foreach ($suggest_questions as $keyword => $data) {
      if (!isset($this->keyword_in_campaign[$keyword])) {
        $fetch_live_ranking_suggest[] = array($keyword, 'de', $data[0]);
      }
    }

    // SORT BY SEARCHVOLUME
    $ssv = array();
    foreach ($fetch_live_ranking as $key => $row)
    {
      $ssv[$key] = $row[2];
    }
    array_multisort($ssv, SORT_DESC, $fetch_live_ranking);

    // SORT BY SEARCHVOLUME
    $ssvs = array();
    foreach ($fetch_live_ranking_suggest as $key => $row)
    {
      $ssvs[$key] = $row[2];
    }
    array_multisort($ssvs, SORT_DESC, $fetch_live_ranking_suggest);

    // MERGE
    $fetch_live_ranking_all = array_merge ($fetch_live_ranking, $fetch_live_ranking_suggest);

    require_once ($env_data['path']. 'oneproseo/includes/liveranking.class.php');
    $rltRankings       = new liveranking($fetch_live_ranking_all, $this->hostname, $this->db, $this->lang);
    $realtime_rankings = $rltRankings->getResult();

    if (!empty($realtime_rankings)) {
      foreach ($realtime_rankings as $keyword => $data) {
        if ($data['p'] !== false) {
          // WHY???
          if (!isset($this->res_kw[$keyword])) {
            continue;
          }
          array_push($this->res_kw[$keyword], $data['p'], $data['u']);
          $this->urls_to_find_rankings_scrape[] = $row['url'];
        }
      }
    }

  }


  public function checkKeywordSource ($kw) {

    $marker = '';

    if (isset($this->source_adwords[$kw])) {
      $marker .= 'Keyword DB, ';
    }

    if (isset($this->source_suggest[$kw])) {
      $marker .= 'Suggest DB, ';
    }

    if (isset($this->source_suggest_questions[$kw])) {
      $marker .= 'W-Fragen DB, ';
    }

    if (isset($this->source_adwords_url[$kw])) {
      $marker .= 'URL DB, ';
    }

    $marker = rtrim($marker, ', ');

    return $marker;

  }


  public function createProperUrl ($url)
  {

    if (empty($url)) {
     return;
    }

    $url = trim($url);

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return $url;

  }


  public function createProperHostname ($url)
  {

    $url = strtolower(trim($url));

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));

  }


  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      echo ('Connect failed:' .  mysqli_connect_error());
    }

  }


}

?>
