<?php
class botti_dashboard
{
	public function __construct ($env_data)
	{
		require_once "botti_init.php";
		$this->env_data = $env_data;
		$this->fetchProjects();
	}

	private function fetchProjects()
	{

		$aCustomers = OnePro_DB::query("SELECT * FROM ruk_project_customers WHERE logfile IS NOT NULL");

		foreach($aCustomers as $key => $aCustomer)
		{

			if ($this->env_data['customer_id'] != NULL && $aCustomer['id'] != $this->env_data['customer_id']) {
				unset($aCustomers[$key]);
				continue;
			}

			$aCustomers[$key]['link'] = '<a href="project/'.$aCustomer['id'].'" class="label label-green">anzeigen</a>';

		}

		$aCustomers = array_values($aCustomers);

		// OUTPUT
		$output = array (
			 'data' => $aCustomers)
		;

		echo json_encode($output);

	}
}
?>