<?php
class botti_chart 
{
	public function __construct ($env_data)
	{
		require_once "botti_init.php";
		$this->env_data = $env_data;
		$this->project_id = $_REQUEST['project_id'];
		$this->log_table = "`botti_" . $this->project_id . "_logs`";
		
		$this->params = array();
		$this->params['bot_id'] = $_REQUEST['bot_id'];
		$this->params['url_id'] = $_REQUEST['url_id'];
		$this->params['url_full'] = $_REQUEST['url_full'];
		$this->params['datefrom'] = strtotime($_REQUEST['datefrom'] . "00:00:00");
		$this->params['dateto'] = strtotime($_REQUEST['dateto'] . "23:59:59");
		$this->params['statusCode'] = $_REQUEST['statusCode'];
		$this->params['ignoreQueryString'] = $_REQUEST['ignoreQueryString'];
		$this->params['checkReverseDns'] = $_REQUEST['checkReverseDns'];
		$this->params['timestamp_format_1'] = "%Y-%m-%d";
		$this->params['timestamp_format_2'] = "%Y%m%d";
		$this->params['ua_googlebot'] = onepro_getHashId("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
		$this->params['ua_bingbot'] = onepro_getHashId("Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)");
		$this->params['ua_yandexbot'] = onepro_getHashId("Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)");
		$this->where = " WHERE `timestamp` >= %i_datefrom AND `timestamp` <= %i_dateto";
		if($this->params['statusCode'] != "-1")
		{
			$this->where .= " AND status_last = %i_statusCode";
		}
		if($this->params['checkReverseDns'] == "on")
		{
			$this->where .= " AND dns_match = 1";
		}
		if(!is_null($this->params['bot_id']) && $this->params['bot_id'] != "")
		{
			$this->where .= " AND user_agent_id = %i_bot_id";
		}
		if(!is_null($this->params['url_id']) && $this->params['url_id'] != "")
		{
			if($this->params['ignoreQueryString'] == "on") $sTemp = " AND url_id = %i_url_id";
			else $sTemp = " AND url_id_full = %i_url_id";
			
			if($this->params['url_full'] == "off") $sTemp = " AND url_id = %i_url_id";
			else if($this->params['url_full'] == "on") $sTemp = " AND url_id_full = %i_url_id";
			
			$this->where .= $sTemp;
		}
		
		if($_REQUEST['action'] == "project")
		{
			$this->fetchProjectJson();
		}
		else if($_REQUEST['action'] == "bot")
		{
			$this->fetchBotJson();
		}
		else if($_REQUEST['action'] == "stats")
		{
			$aTempBots = explode(",",$this->params['bot_id']);
			$this->params['bot_id'] = $aTempBots[0];
			$this->params['bot_id_second'] = $aTempBots[1];
			$this->fetchStatsJson();
		}
		else if($_REQUEST['action'] == "url")
		{
			$this->fetchUrlJson();
		}
	}
	
	private function fetchProjectJson()
	{
		#onepro_debugDBQueries(1);
		$chart = array ();
		
		$chart['bots'] = onepro_formatNumber(OnePro_DB::queryOneField("googlebot", "SELECT count(*) as googlebot FROM " . $this->log_table . $this->where, $this->params));
		$chart['googlebot'] = onepro_formatNumber(OnePro_DB::queryOneField("googlebot", "SELECT count(*) as googlebot FROM " . $this->log_table . $this->where . " AND `user_agent_id` = %i_ua_googlebot ", $this->params));
		$chart['bingbot'] = onepro_formatNumber(OnePro_DB::queryOneField("bingbot", "SELECT count(*) as bingbot FROM " . $this->log_table . $this->where . " AND `user_agent_id` = %i_ua_bingbot", $this->params));
		$chart['yandexbot'] = onepro_formatNumber(OnePro_DB::queryOneField("yandexbot", "SELECT count(*) as yandexbot FROM " . $this->log_table . $this->where . " AND `user_agent_id` = %i_ua_yandexbot", $this->params));
		
		$aData = OnePro_DB::query("SELECT count(*) as visits, from_unixtime(`timestamp`, %s_timestamp_format_1) as `date`, from_unixtime(`timestamp`, %s_timestamp_format_2) as `sortcolumn` FROM " . $this->log_table . $this->where . " GROUP BY `date` ORDER BY `sortcolumn` ASC", $this->params);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) json_encode($chart);
		
		foreach($aData as $aRow)
		{
			$chart['chart'][] = array ('x' => $aRow['date'], 'y' => $aRow['visits']);
		}
		
		echo json_encode($chart);
	}
	
	private function fetchBotJson()
	{
		#onepro_debugDBQueries(1);
		$chart = array ();
		$chart['bots'] = onepro_formatNumber(OnePro_DB::queryOneField("googlebot", "SELECT count(*) as googlebot FROM " . $this->log_table . $this->where, $this->params));
		
		$aData = OnePro_DB::query("SELECT count(*) as visits, from_unixtime(`timestamp`, %s_timestamp_format_1) as `date`, from_unixtime(`timestamp`, %s_timestamp_format_2) as `sortcolumn` FROM " . $this->log_table . $this->where . " GROUP BY `date` ORDER BY `sortcolumn` ASC", $this->params);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) json_encode($chart);
		
		foreach($aData as $aRow)
		{
			$chart['chart'][] = array ('x' => $aRow['date'], 'y' => $aRow['visits']);
		}

		echo json_encode($chart);
	}
	
	private function fetchStatsJson()
	{
		#print_r($this->params);exit;		
		#onepro_debugDBQueries(1);
		$chart = array ();
		$chart['bot_first'] = onepro_formatNumber(OnePro_DB::queryOneField("googlebot", "SELECT count(*) as googlebot FROM " . $this->log_table . $this->where, $this->params));
		
		$aData = OnePro_DB::query("SELECT count(*) as visits, from_unixtime(`timestamp`, %s_timestamp_format_1) as `date`, from_unixtime(`timestamp`, %s_timestamp_format_2) as `sortcolumn` FROM " . $this->log_table . $this->where . " GROUP BY `date` ORDER BY `sortcolumn` ASC", $this->params);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) json_encode($chart);
		
		foreach($aData as $aRow)
		{
			#$chart['chart'][] = array ('x' => $aRow['date'], 'y' => $aRow['visits']);
			$chart['chart']['categories'][] = $aRow['date'];
			$chart['chart']['series_1'][] = $aRow['visits'];
		}
		//$chart['chart']['series_1'] = implode(",",$aTempSeries);
		
		//Second Bot data
		$this->where = str_replace("i_bot_id","i_bot_id_second",$this->where);
		
		$chart['bot_second'] = onepro_formatNumber(OnePro_DB::queryOneField("googlebot", "SELECT count(*) as googlebot FROM " . $this->log_table . $this->where, $this->params));
		
		$aData = OnePro_DB::query("SELECT count(*) as visits, from_unixtime(`timestamp`, %s_timestamp_format_1) as `date`, from_unixtime(`timestamp`, %s_timestamp_format_2) as `sortcolumn` FROM " . $this->log_table . $this->where . " GROUP BY `date` ORDER BY `sortcolumn` ASC", $this->params);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) json_encode($chart);
		
		$aTempSeries = array();
		foreach($aData as $aRow)
		{
			$chart['chart']['series_2'][] = $aRow['visits'];
		}
		
		#print_r($chart);exit;
		

		echo json_encode($chart);
	}
	
	private function fetchUrlJson()
	{
		#onepro_debugDBQueries(1);
		$chart = array ();
		$chart['bots'] = onepro_formatNumber(OnePro_DB::queryOneField("googlebot", "SELECT count(*) as googlebot FROM " . $this->log_table . $this->where, $this->params));
		
		$aData = OnePro_DB::query("SELECT count(*) as visits, from_unixtime(`timestamp`, %s_timestamp_format_1) as `date`, from_unixtime(`timestamp`, %s_timestamp_format_2) as `sortcolumn` FROM " . $this->log_table . $this->where . " GROUP BY `date` ORDER BY `sortcolumn` ASC", $this->params);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) json_encode($chart);
		
		foreach($aData as $aRow)
		{
			$chart['chart'][] = array ('x' => $aRow['date'], 'y' => $aRow['visits']);
		}

		echo json_encode($chart);
	}
}
?>