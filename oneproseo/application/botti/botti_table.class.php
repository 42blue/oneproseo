<?php
class botti_table 
{
	public function __construct ($env_data)
	{
		require_once "botti_init.php";
		$this->env_data = $env_data;
		$this->project_id = $_REQUEST['project_id'];
		$this->log_table = "`botti_" . $this->project_id . "_logs` a";
		
		$this->params['bot_id'] = $_REQUEST['bot_id'];
		$this->params['url_id'] = $_REQUEST['url_id'];
		$this->params['url_full'] = $_REQUEST['url_full'];
		$this->params['datefrom'] = strtotime($_REQUEST['datefrom'] . " 00:00:00");
		$this->params['dateto'] = strtotime($_REQUEST['dateto'] . " 23:59:59");
		$this->params['statusCode'] = $_REQUEST['statusCode'];
		$this->params['ignoreQueryString'] = $_REQUEST['ignoreQueryString'];
		$this->params['checkReverseDns'] = $_REQUEST['checkReverseDns'];
		$this->params['date_format_1'] = "%Y-%m-%d";
		$this->params['date_format_2'] = "%Y%m%d";
		$this->params['date_format_3'] = "%d.%m.%Y %H:%i:%s";
		
		$this->where = " WHERE a.`timestamp` >= %i_datefrom AND a.`timestamp` <= %i_dateto";
		if($this->params['statusCode'] != "-1")
		{
			$this->where .= " AND status_last = %i_statusCode";
		}
		if($this->params['checkReverseDns'] == "on")
		{
			$this->where .= " AND dns_match = 1";
		}
		if(!is_null($this->params['bot_id']) && $this->params['bot_id'] != "")
		{
			$this->where .= " AND user_agent_id = %i_bot_id";
		}
		if(!is_null($this->params['url_id']) && $this->params['url_id'] != "")
		{
			if($this->params['ignoreQueryString'] == "on") $sTemp = " AND url_id = %i_url_id";
			else $sTemp = " AND url_id_full = %i_url_id";
			
			if($this->params['url_full'] == "off") $sTemp = " AND url_id = %i_url_id";
			else if($this->params['url_full'] == "on") $sTemp = " AND url_id_full = %i_url_id";
			
			$this->where .= $sTemp;
		}
		if($this->params['ignoreQueryString'] == "on") 
		{
			$this->group_by_url = " GROUP BY url_id";
		}
		else 
		{
			$this->group_by_url = " GROUP BY url_id_full";
		}
		
		if($_REQUEST['type'] != "export")
		{
			if($_REQUEST['action'] == "project")
			{
				$this->fetchProjectData();
			}
			else if($_REQUEST['action'] == "bot")
			{
				$this->fetchBotData();
			}
			else if($_REQUEST['action'] == "url")
			{
				$this->fetchUrlData();
			}
		}
	}
	
	public function fetchProjectData()
	{
		#onepro_debugDBQueries(1);
		$data = array();
		
		// Paging
		$sLimit = '';
		if (isset($_REQUEST['start']) && $_REQUEST['length'] != '-1') {
			$sLimit = 'LIMIT '. $_REQUEST['start'] .', '. $_REQUEST['length'] ;
		}
		
		// SORTING
		$sOrder = '';
		if ( isset( $_REQUEST['order']) )
		{
			$index = $_REQUEST['order'][0]['column'];
			$direction = $_REQUEST['order'][0]['dir'];
			$columns = $_REQUEST['columns'];
			$cellname = $columns[$index]['data'];
			$sOrder = 'ORDER BY '. $cellname . ' '. $direction;
		}
		
		// FILTER
		$sWhere = $this->where;
		if ($_REQUEST['search']['value'] != '') {
			$sWhere .= ' AND (';
			$sWhere .= 'b.browser LIKE "%'. $_REQUEST['search']['value'] .'%" OR b.version LIKE "%'. $_REQUEST['search']['value'] .'%"';
			$sWhere .= ')';
		}
		
		$sql = "SELECT
				concat(b.browser, '/', b.version) as bot,
				count(*) as zugriffe,
				concat('<a href=\"". $this->project_id ."/bot/', a.user_agent_id ,'\" class=\"label label-green\">anzeigen</a>') as link,
				b.user_agent as useragent,
				user_agent_id
			FROM
				" . $this->log_table . "
				LEFT JOIN botti_user_agents b
					ON a.user_agent_id = b.id
			$sWhere
			GROUP BY a.user_agent_id
			$sOrder
			$sLimit";
			
		$data = OnePro_DB::query($sql, $this->params);
		$recordsFiltered = OnePro_DB::count();

		// COUNT ALL
		$sql = "SELECT COUNT(*) FROM " . $this->log_table . $this->where . " GROUP BY user_agent_id";
		OnePro_DB::query($sql, $this->params);
		$count = OnePro_DB::count();

		// OUTPUT
		$output = array ('draw' => $_REQUEST['draw'], 
						 'recordsTotal' => $count,
						 'recordsFiltered' => $recordsFiltered,
						 'data' => $data);

		if($_REQUEST['type'] == "export") return $output;
		else echo json_encode($output);
	}
	
	public function fetchBotData()
	{
		#onepro_debugDBQueries(1);
		$data = array();
		
		// Paging
		$sLimit = '';
		if (isset($_REQUEST['start']) && $_REQUEST['length'] != '-1') {
			$sLimit = 'LIMIT '. $_REQUEST['start'] .', '. $_REQUEST['length'] ;
		}
		
		// SORTING
		$sOrder = '';
		if ( isset( $_REQUEST['order']) )
		{
			$index = $_REQUEST['order'][0]['column'];
			$direction = $_REQUEST['order'][0]['dir'];
			$columns = $_REQUEST['columns'];
			$cellname = $columns[$index]['data'];
			$sOrder = 'ORDER BY '. $cellname . ' '. $direction;
		}
		
		// FILTER
		$sWhere = $this->where;
		if ($_REQUEST['search']['value'] != '') {
			$sWhere .= ' AND (';
			$sWhere .= 'request_path LIKE "%'. $_REQUEST['search']['value'] .'%"';
			$sWhere .= ')';
		}
		
		$sql = "SELECT
				hostname,
				hostname_client,
				request_path,
				query_string,
				user_agent_id,
				url_id,
				url_id_full,
				https,
				count(*) as zugriffe,
				CONCAT(IF(`https` = '1', 'https://', 'http://'), IF(`hostname_client` = '-', `hostname`, `hostname_client`), `request_path`" . (($this->params['ignoreQueryString'] != "on") ? ", IF(`query_string` IS  NULL, '', `query_string`)" : "") . ") as url_link,
				concat('<a href=\"". $this->params['bot_id'] . (($this->params['ignoreQueryString'] != "on") ? "/urlfull/', url_id_full" : "/url/', url_id") . " ,'\" class=\"label label-green\">anzeigen</a>') as link
			FROM
				" . $this->log_table . "
			$sWhere
			" . $this->group_by_url . " 
			$sOrder
			$sLimit";
			
		$data = OnePro_DB::query($sql, $this->params);
		$recordsFiltered = OnePro_DB::count();
		
		foreach($data as $i => $row)
		{
			$data[$i]['url'] = $row['url_link'];
			$data[$i]['url_link'] = '<a href="' . $row['url_link'] . '" target="_blank" alt="">' . $row['url_link'] . '  <i class="icon-external-link f12"></i></a>';
		}

		// COUNT ALL
		$sql = "SELECT COUNT(*) FROM " . $this->log_table . $this->where . $this->group_by_url;
		OnePro_DB::query($sql, $this->params);
		$count = OnePro_DB::count();

		// OUTPUT
		$output = array ('draw' => $_REQUEST['draw'], 
						 'recordsTotal' => $count,
						 'recordsFiltered' => $recordsFiltered,
						 'data' => $data);

		if($_REQUEST['type'] == "export") return $output;
		else echo json_encode($output);
	}
	
	public function fetchUrlData()
	{
		#onepro_debugDBQueries(1);
		$data = array();
		
		// Paging
		$sLimit = '';
		if (isset($_REQUEST['start']) && $_REQUEST['length'] != '-1') {
			$sLimit = 'LIMIT '. $_REQUEST['start'] .', '. $_REQUEST['length'] ;
		}
		
		// SORTING
		$sOrder = '';
		if ( isset( $_REQUEST['order']) )
		{
			$index = $_REQUEST['order'][0]['column'];
			$direction = $_REQUEST['order'][0]['dir'];
			$columns = $_REQUEST['columns'];
			$cellname = $columns[$index]['data'];
			$sOrder = 'ORDER BY '. $cellname . ' '. $direction;
		}
		
		// FILTER
		$sWhere = $this->where;
		if ($_REQUEST['search']['value'] != '') {
			$sWhere .= ' AND (';
			$sWhere .= 'request_path LIKE "%'. $_REQUEST['search']['value'] .'%"';
			$sWhere .= ')';
		}
		
		$sql = "SELECT
				*,
				CONCAT(IF(`https` = '1', 'https://', 'http://'), IF(`hostname_client` = '-', `hostname`, `hostname_client`), `request_path`, IF(`query_string` IS  NULL, '', `query_string`)) as url_link,
				REPLACE(REPLACE(REPLACE(FORMAT(response_time/1000, 2), '.', '@'), ',', '.'), '@', ',') as response_time,
				FROM_UNIXTIME(timestamp, %s_date_format_3) as zeitstempel
			FROM
				" . $this->log_table . "
			$sWhere
			$sOrder
			$sLimit";
			
		$data = OnePro_DB::query($sql, $this->params);
		$recordsFiltered = OnePro_DB::count();
		
		foreach($data as $i => $row)
		{
			$data[$i]['url'] = $data[$i]['url_link'];
			$data[$i]['url_link'] = '<a href="' . $row['url_link'] . '" target="_blank" alt="">' . $row['url_link'] . '  <i class="icon-external-link f12"></i></a>';
		}

		// COUNT ALL
		$sql = "SELECT * FROM " . $this->log_table . $this->where;
		OnePro_DB::query($sql, $this->params);
		$count = OnePro_DB::count();

		// OUTPUT
		$output = array ('draw' => $_REQUEST['draw'], 
						 'recordsTotal' => $count,
						 'recordsFiltered' => $recordsFiltered,
						 'data' => $data);


		if($_REQUEST['type'] == "export") return $output;
		else echo json_encode($output);
	}
}
?>