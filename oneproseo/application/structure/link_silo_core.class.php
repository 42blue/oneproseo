<?php

header("Content-Type: text/html; charset=utf-8");
libxml_use_internal_errors(true);

class link_silo_core {

  protected $failcount = 0;

  public function __construct ($env_data)
  {

    session_start();

    $this->env_data = $env_data;

    $this->restrictQuery();

    $this->keyword  = strip_tags($_POST['kw']);
    $this->url      = strip_tags($_POST['url']);
    $this->lang     = 'de';

    if (empty($this->keyword) || empty($this->url)) {
      $out['error'] = 'params missing -> contact admin';
      echo json_encode ($out);
      exit;
    }

    $this->hostname = $this->createProperHostname($this->url);

    require_once ($this->env_data['path']. 'oneproseo/includes/logtofile.class.php');
    $oLogToFile = new logtofile($env_data);

    $oLogToFile->write('LINK SILO TOOL', 'Domain: '.$this->hostname.' - Keyword: '.$this->keyword, 'temp/logs/log_liveranking.txt');

    // scraper IPs
    require_once ($env_data['path']. 'oneproseo/includes/config.php');
    $this->scraper_hosts = unserialize(SCRAPER_HOSTS);

    $this->output();

  }


  // restrict querys in customer version to 10
  private function restrictQuery () {

    require_once ($this->env_data['path']. 'oneproseo/includes/db.class.php');
    $db = new db;
    $this->db = $db->mySqlConnect($this->env_data);

    require_once ($this->env_data['path']. 'oneproseo/includes/limitquerys.class.php');
    $limit  = new limitquerys($this->db, $this->env_data);
    $limit->checkLimit();

  }

  private function output() {

    // GET THE GOOGLE TOP 100 for 'site:url.de keyword'
    require_once ($this->env_data['path'] . 'oneproseo/includes/rankings.class.php');
    $oRankings       = new rankings($this->hostname, $this->keyword, $this->lang);
    $scrape_rankings = $oRankings->getResult();
    $mHostname =  str_replace('.', '-', $this->hostname);
    $mPermalink = $mHostname . '-' . date('Y-m-d-h') . '-' . mt_rand(10, 99) . '-' . mt_rand(10, 99);

    if (empty($scrape_rankings)) {
      $oRankings       = new rankings($this->hostname, $this->keyword, $this->lang);
      $scrape_rankings = $oRankings->getResult();
    }

    if (empty($scrape_rankings)) {
      echo '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title"> Link Silo Analyse:</span></div><div class="box-content padded">Keine Ergebnisse gefunden!</div></div></div>';
      exit;
    }


    $out  = '<form class="form-horizontal fill-up" role="form" id="ops_silo_task" data-filename="'.$mPermalink.'"><div class="col-md-12"><div class="box"><div class="box-header"><span class="title"> Die relevantesten URLs für Keyword: <strong>'.$this->keyword.'</strong> / Domain: <strong>'.$this->hostname.'</strong></span></div><div class="box-content">';
    $out .= '<table class="table table-normal data-table"><thead><tr><td>#</td><td style="width: 50%;">URL</td><td style="width: 50%;">Keyword optimierte URL (3) für Analyse auswählen</td></thead>';

    $i = 1;

    $_SESSION['linksilo'] = $scrape_rankings;

    foreach ($scrape_rankings as $url) {

      if ($i == 1) {
        $out .= '<tr><td>'.$i++.'</td><td><a href="'.$url.'" target="_blank">'.$url.'</a></td><td><input type="radio" name="ops_silo_url_find" value="'.$url.'" class="icheck" checked="checked"></td></tr>';
        $first_url = $url;
      } else {
        $out .= '<tr><td>'.$i++.'</td><td><a href="'.$url.'" target="_blank">'.$url.'</a></td><td><input type="radio" name="ops_silo_url_find" value="'.$url.'" class="icheck"></td></tr>';
      }

      if ($i > 5) {break;}

    }

    $out .= '</tr></thead>';
    $out .= '</table></div></div></div>';

    $out .= '
        <div class="col-md-12">
          <div class="box">
            <div class="box-header"><span class="title"> Link Silo Analyse:</span></div>
            <div class="box-content padded">
                <div class="form-group">
                  <label for="ops_kolibri_kw" class="col-sm-2 label-1 control-label">(1) Keyword:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_silo_kw" name="kw" required="required" value="'.$this->keyword.'" disabled />
                  </div>
                </div>
                <div class="form-group">
                  <label for="ops_kolibri_url" class="col-sm-2 label-1 control-label">(2) Domain:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_silo_url" name="url" required="required" value="'.$this->hostname.'" disabled />
                  </div>
                </div>
                <div class="form-group">
                  <label for="ops_kolibri_url" class="col-sm-2 label-1 control-label">(3) Keyword optimierte URL:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_silo_url_find" name="url" required="required" value="'.$first_url.'" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_silo_submit">Datenbankabfrage starten</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </form>';

    echo $out;

  }


  public function createProperUrl ($url)
  {

    $url = trim($url);

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return $url;

  }


  public function createProperHostname ($url)
  {

    $url = trim($url);

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));

  }


}

?>
