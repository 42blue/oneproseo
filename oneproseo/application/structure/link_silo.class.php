<?php


/*

What happens here?

Parameter:

- $this->keyword ->Keyword
- $this->url -> Domain
- $this->url_find - > URL die optimiert werden soll

Prozess:

1. Keyword Vorschläge holen

    Keyword: Google Suggest W-Fragen -> $this->querySuggestProxy();
    Keyword: Google Suggest -> $this->queryIdeasProxy();
    Keyword: Adwords Vorschläge -> $this->queryKeywordIdeas();
    URL:     Adwords Vorschläge -> $this->queryUrlIdeas();

2. Suchvolumen holen

    Für alle Keywords SV über Adwords -> $this->querySearchVolume();

3. Alle Keywords mit Adwords Daten zusammenführen

4. Prüfen ob für die Keywords Rankings vorliegen

    Rückgabe: Rank / URL

5. Die Top 50 site:url.de keyword aus Google holen

    Rückgabe: Rank / URL

6. LiveScrape

    Top 100 Keywords (SV) die nicht in den Campaigns gefunden wurden
    Top 10 Keywords W-Fragen (SV) die nicht in den Campaigns gefunden wurden
    Live Ranking abfragen

    Rückgabe: Rank / URL

7. Linkfinder

    auf allen URLs schauen ob es einen Link zur Keyword optimierten URL gibt

8. Ausgabe Ergebnis

*/

ini_set('max_execution_time', 600);

header("Content-Type: text/html; charset=utf-8");
libxml_use_internal_errors(true);

class link_silo {

  protected $failcount = 0;

  public function __construct ($env_data)
  {

    session_start();
    
    $this->env_data = $env_data;

    $this->keyword      = strip_tags($_POST['kw']);
    $this->url          = strip_tags($_POST['url']);
    $this->url_find     = strip_tags($_POST['url_find']);
    $this->poll_filname = strip_tags($_POST['poll']);
    $this->lang         = 'de';

    if (empty($this->keyword) || empty($this->url) || empty($this->url_find)) {
      $out['error'] = 'params missing -> contact admin';
      echo json_encode ($out);
      exit;
    }

    $this->url      = $this->createProperUrl($this->url);
    $this->url_find = $this->createProperUrl($this->url_find);

    if (filter_var($this->url, FILTER_VALIDATE_URL) === FALSE) {
      echo '<div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>' . $this->url . '</strong> ist keine valide URL.</div></div>';
      exit;
    }
    if (filter_var($this->url_find, FILTER_VALIDATE_URL) === FALSE) {
      echo '<div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>' . $this->url_find . '</strong> ist keine valide URL.</div></div>';
      exit;
    }

    // scraper IPs
    require_once ($env_data['path']. 'oneproseo/includes/config.php');
    $this->scraper_hosts = unserialize(SCRAPER_HOSTS);


    $this->logdir = $env_data['csvstore'] . 'linksilo-'.$this->poll_filname.'.txt';

    // PROCESS
    $this->logToFile('Schritt 1/7: <br /> Suche Relevante Frageterme für das Keyword: <b>'. $this->keyword . '</b> auf Domain.');
    $this->querySuggestProxy();

    $this->logToFile('Schritt 2/7: <br /> Suche Suggest für das Keyword: <b>'. $this->keyword . '</b> auf Domain.');
    $this->queryIdeasProxy();

    $this->logToFile('Schritt 3/7: <br /> Suche relevante semantische Keywords für das Keyword: <b>'. $this->keyword . '</b> auf Domain.');
    $this->queryKeywordIdeas();

    $this->logToFile('Schritt 4/7: <br /> Suche relevante Keywords basierend auf der Ziel URL: <b>'. $this->url_find . '</b>');
    $this->queryUrlIdeas();

    $this->querySearchVolume();
    $this->mergeKeywordData();
    $this->getRankings();

    $this->findLinks();

    // OUT
    $this->output();

  }


  private function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'w');
    fwrite($fh, $content);
    fclose($fh);

  }

  private function querySuggestProxy() {

    $server = rand(0, count($this->scraper_hosts) - 1);

    $opts = array('http' =>
      array('timeout' => 90)
    );

    $context = stream_context_create($opts);
    $data = @file_get_contents('http://'.$this->scraper_hosts[$server].'/suggest/np_ask_google_extended.php?kw=' . urlencode($this->keyword) . '&lang=' . $this->lang . '&key=M4B1st2IlvFfihHG42HQRTpE4nny5f', false, $context);

    // FALLBACK
    if ($data === FALSE) {
      $this->querySuggestProxy();
    }

    $json = json_decode($data);

    if (isset($json->error) or $json == NULL) {

      $this->failcount++;

      if ($this->failcount > 10) {
        $out['error'] = 'api error';
        echo json_encode ($out);
        exit;
      }

      $this->querySuggestProxy();

    } else {

      $this->resultSuggestQuestions = $json;

    }

  }


  private function queryIdeasProxy() {

    $server = rand(0, count($this->scraper_hosts) - 1);

    $opts = array('http' =>
      array('timeout' => 90)
    );

    $context = stream_context_create($opts);
    $data = @file_get_contents('http://'.$this->scraper_hosts[$server].'/suggest/np_google_extended.php?kw=' . urlencode($this->keyword) . '&lang=' . $this->lang . '&key=M4B1st2IlvFfihHG42HQRTpE4nny5f', false, $context);

    // FALLBACK
    if ($data === FALSE) {
      $this->queryIdeasProxy();
    }

    $json = json_decode($data);

    if (isset($json->error) or $json == NULL) {

      $this->failcount++;

      if ($this->failcount > 10) {
        $out['error'] = 'api error';
        echo json_encode ($out);
        exit;
      }

      $this->queryIdeasProxy();

    } else {

      $this->resultSuggestIdeas = $json;

    }

  }


  private function queryKeywordIdeas () {

    $payload = array($this->keyword);
    $payload = base64_encode(json_encode($payload));

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => 'keyword_arr=' . $payload . '&lang=' . $this->lang . '&country='. $this->lang
      )
    );

    $context = stream_context_create($opts);
    $result  = @file_get_contents('https://oneproseo.advertising.de/oneproapi/adwords/service/googleKeywordIdeas.php', false, $context);

    $this->resultIdeas = json_decode($result);

  }


  private function queryUrlIdeas () {

    $query = urlencode($this->url_find);

    $opts = array('http' =>
      array('timeout' => 30)
    );

    $context = stream_context_create($opts);
    $result  = @file_get_contents('https://oneproseo.advertising.de/oneproapi/adwords/service/googleKeywordIdeasUrl.php?url=' . $query, false, $context);

    $this->resultIdeasUrl = json_decode($result);

  }


  private function querySearchVolume () {

    if (isset($this->resultSuggestIdeas)) {
      foreach($this->resultSuggestIdeas as $searchterm => $keywords) {
        if (!empty($keywords)) {
          foreach ($keywords as $keyword) {
            $payload[$keyword] = array($keyword, $this->lang, $this->lang);
          }
        }
      }
    }

    if (isset($this->resultSuggestQuestions)) {
      foreach($this->resultSuggestQuestions as $searchterm => $keywords) {
        if (!empty($keywords)) {
          foreach ($keywords as $keyword) {
            $payload[$keyword] = array($keyword, $this->lang, $this->lang);
          }
        }
      }
    }

    require_once ($env_data['path']. 'oneproseo/includes/svolume.class.php');
    $oSvolume  = new svolume($this->db, $this->env_data);
    $result    = $oSvolume->querySV($payload);

    $results = array();
    foreach ($result as $k => $v) {
      $results[$v[0]] = array($v[1], $v[2], $v[3]);
    }

    $this->resultSV = $results;

  }


  private function mergeKeywordData() {

    $this->res_kw                   = array();
    $this->allkeywords              = array();
    $this->source_suggest           = array();
    $this->scrape_suggest_questions = array();
    $this->source_suggest_questions = array();
    $this->source_adwords           = array();
    $this->source_adwords_url       = array();

    // all keywords from suggest
    foreach($this->resultSuggestIdeas as $searchterm => $keywords) {
      if (!empty($keywords)) {
        foreach ($keywords as $keyword) {
          $this->res_kw[$keyword] = array (0, 0, 0);
          $this->allkeywords[$keyword]  = $keyword;
          $this->source_suggest[$keyword] = $keyword;
        }
      }
    }

    // all keywords from suggest questions
    foreach($this->resultSuggestQuestions as $searchterm => $keywords) {
      if (!empty($keywords)) {
        foreach ($keywords as $keyword) {
          $this->res_kw[$keyword] = array (0, 0, 0);
          $this->allkeywords[$keyword]  = $keyword;
          $this->source_suggest_questions[$keyword] = $keyword;
        }
      }
    }

    if (isset($this->resultIdeas->set)) {
      // all keywords from adwords
      foreach ($this->resultIdeas->set as $k => $v) {
        $this->res_kw[$v->kw] = array ($v->sv, $v->cpc, $v->comp);
        $this->allkeywords[$v->kw]  = $v->kw;
        $this->source_adwords[$v->kw] = $v->kw;
      }
    }
    
    if (isset($this->resultIdeasUrl->set)) {
      // all keywords from adwords URL query
      foreach ($this->resultIdeasUrl->set as $k => $v) {
        $this->res_kw[$v->kw] = array ($v->sv, $v->cpc, $v->comp);
        $this->allkeywords[$v->kw]  = $v->kw;
        $this->source_adwords_url[$v->kw] = $v->kw;
      }
    }

    $this->logToFile('Schritt 5/7: <br /> Abfrage Suchvolumen, CPC für <b>' . count($allkeywords) . '</b> Keywords.');

    if (isset($this->resultSV)) {
      foreach ($this->resultSV as $k => $v) {
        $this->res_kw[$k] = array ($v[0], $v[1], $v[2]);
        $this->allkeywords[$k]  = $k;
        if (isset($this->source_suggest_questions[$k])) {
          $this->scrape_suggest_questions[$k] = array ($v[0], $v[1], $v[2]);
        }
      }
    }

  }

  private function getRankings() {

    $this->urls_to_find_rankings_scrape = array();
    $this->urls_to_find_rankings_db     = array();
    $this->urls_to_find_scrape_site     = array();
    $this->keyword_in_campaign          = array();

    $this->allkeywords                  = array_values($this->allkeywords);
    $j_c_url =  count($this->res_kw) + count($this->scrape_suggest_questions);
    $this->logToFile('Schritt 6/7: <br /> Abfrage Rankings für <b>' . $j_c_url . '</b> Keywords');

    $this->mySqlConnect();
    $this->hostname = $this->createProperHostname($this->url);

    // TRUSTED PROXY SCRAPE INTERCEPT
    // -> array_push($this->res_kw[$v['keyword']], $row['position'], $row['url']);
    // -> $this->urls_to_find_rankings_scrape
    $this->fetchRankingsRealtimeScrape($this->res_kw);

    $this->logToFile('Schritt 6/7: <br /> Abfrage URL Rankings für <b>' . $j_c_url . '</b> Keywords');

    // GET THE GOOGLE TOP 100 for 'site:url.de keyword'

    if (!isset($_SESSION['linksilo'])) {
      require_once ($env_data['path']. 'oneproseo/includes/rankings.class.php');
      $oRankings                = new rankings($this->hostname, $this->keyword, $this->lang);
      $this->urls_to_find_scrape_site = $oRankings->getResult();
    }
    
    $this->urls_to_find_scrape_site = $_SESSION['linksilo'];

    // PUT ALL FOUND URLs (DB and SCRAPE) together
    $this->urls_to_find = array_merge($this->urls_to_find_rankings_db, $this->urls_to_find_rankings_scrape, $this->urls_to_find_scrape_site);

    $this->logToFile('Schritt 7/7: <br /> Link Silo Crawl für <b>' . count($this->urls_to_find) . '</b> URLs.');

  }


  private function findLinks() {

    // OPEN ALL RELEVANT URLs and search for the user given url

    require_once ($env_data['path']. 'oneproseo/includes/linkfinder.class.php');
    $oLinkfinder            = new linkfinder($this->url_find, $this->urls_to_find);
    $this->linkfinder_res   = $oLinkfinder->getResult();

    $this->linkfinder_metas = $oLinkfinder->getMetaData();

  }


  private function output() {

    // ONEDEX CALC
    require_once ($env_data['path']. 'oneproseo/includes/onedex.class.php');
    $oOnedex = new onedex;

    // ALL SCRAPE URLs that could not be matched
    $solitude_urls_scrape = array_diff ($this->urls_to_find_scrape_site, $this->urls_to_find_rankings_db, $this->urls_to_find_rankings_scrape);

    $i                              = 0;
    $out_table_comprehensive        = '';
    $csv                            = array();
    $csv[]                          = array('Keyword', 'ToDo', 'Relevanz', 'KW in Campaign', 'Suchvolumen / Monat', 'Quelle', 'OPI', 'CPC / Euro', 'Competition', 'OneDex', 'Rank', 'URL', 'Link auf Keyword optimierte URL', 'Semantik', 'Keyword', 'Link set', 'OPO Keyword', 'OPO ID');



// OUTPUT - COMPREHENSIVE
    foreach ($this->res_kw as $keyword => $data) {

      $i++;

      $vsv     = ($data[0] == 0) ? 0.01 : $data[0];
      $vcpc    = ($data[1] == 0) ? 0.01 : $data[1];
      $vcomp   = ($data[2] == 0) ? 0.01 : $data[2];
      $opi     = $vsv * $vcpc * $vcomp;
      $opi_f   = $this->formatnumber($opi);
      $cpc_f   = $this->formatnumber($data[1]);
      $comp_f  = $this->formatnumber($data[2]);

      // has ranking?
      if (!isset($data[3])) {
        $data[3] = '-';
        $data[4] = '';
        $data_3_sort = 101;
        $dex = '-';
      } else {
        $data_3_sort = intval($data[3]);
        $dex = $oOnedex->calcOneDex($data[3], $opi);
      }

      $marker = $this->checkKeywordSource($keyword);

  /*

  INDEX
  KEYWORD
  TODO
  SUCHVOLUMEN (data[0])
  KEYWORD oder FRAGE
  OPI
  CPC  (data[1])
  COMPETITION (data[2])
  DEX
  RANKING PLACE (data[3])
  RANKING URL (data[4])
  LINK TO SEARCH URL
  RANKING URL in RS DATABASE
  RANKING URL in GOOGLE [SITE: HOST KEYWORD] TOP 50
  USER: SET A LINK!

  */

      $todo = 'Link vorhanden';
      // LINK nicht gefunden && ranking für URL vorhanden && nicht SUCH URL
      if (!isset($this->linkfinder_res[$data[4]]) && $data_3_sort < 101 && $data[4] != $this->url_find) {
        $todo = 'Link setzen';
      } else if (!isset($this->linkfinder_res[$data[4]]) && $data[4] != $this->url_find) {
        $todo = 'Seite erstellen';
      } else if ($data[4] == $this->url_find) {
        $todo = '';
      }

      $relevancy_s = array_search($data[4], $this->urls_to_find_scrape_site);
      if ($relevancy_s !== false) {
        $relevancy = 'MUST';
        $relevancy_sort = $relevancy_s + 1;
      } else {
        $relevancy = 'OPTION';
        $relevancy_sort = 101;
      }

      if (isset($this->keyword_in_campaign[$keyword])) {
        $kw_in_ruk = 'Ja';
      } else {
        $kw_in_ruk = 'Nein';
      }

      // OUTPUT LOOP
      $out_table_comprehensive .= '<tr>
          <td>'. $i . '</td>
          <td class="nowrap">'.$keyword.'</td>
          <td data-sort="'. $data_3_sort .'"><a href="'.$data[4].'" target="_blank">' . $data[3] . '</a></td>
          <td class="nowrap">'.$todo.'</td>
          <td data-order="'.$relevancy_sort.'">'.$relevancy.'</td>
          <td>'.$kw_in_ruk.'</td>
          <td data-order="'.$data[0].'">'.$data[0].'</td>
          <td class="nowrap">'.$marker.'</td>
          <td data-order="'.$opi.'">'.$opi_f.'</td>
          <td data-order="'.$data[1].'">'.$cpc_f.'</td>
          <td data-sort="'.$data[2].'">'.$comp_f.'</td>
          <td data-sort="'. $dex .'">' . $dex . '</td>
          <td data-sort="'. $data_3_sort .'"><a href="'.$data[4].'" target="_blank">' . $data[4] . '</a></td>';

      // LINK auf Keyword optimierte URL
      if (isset($this->linkfinder_res[$data[4]]) && $data[4] != $this->url_find) {
        $out_table_comprehensive .= '<td data-sort="2"><i class="icon-ok"></i></td>';
        $csv_linkfinder = 'Ja';
      } else if (!isset($this->linkfinder_res[$data[4]]) && $data_3_sort < 101 && $data[4] != $this->url_find) {
        $out_table_comprehensive .= '<td data-sort="1"><i class="icon-remove"></i></td>';
        $csv_linkfinder = 'Nein';
      } else {
        $out_table_comprehensive .= '<td data-sort="0"></td>';
        $csv_linkfinder = '-';
      }

      // RANKING URL in RS DATABASE
      // RANKING URL in GOOGLE [SITE: HOST KEYWORD] TOP 50
      if ($data[4] != '-') {
        if (array_search($data[4], $this->urls_to_find_rankings_db) !== false) {
          $out_table_comprehensive .= '<td data-sort="2"><i class="icon-ok"></i></td>';
          $csv_rankings =  'Ja';
        } else {
          $out_table_comprehensive .= '<td data-sort="1"><i class="icon-remove"></i></td>';
          $csv_rankings =  'Nein';
        }
        if (array_search($data[4], $this->urls_to_find_scrape_site) !== false) {
          $out_table_comprehensive .= '<td data-sort="2"><i class="icon-ok"></i></td>';
          $csv_scrape =  'Ja';
        } else {
          $out_table_comprehensive .= '<td data-sort="1"><i class="icon-remove"></i></td>';
          $csv_scrape =  'Nein';
        }
      } else {
        $out_table_comprehensive .= '<td data-sort="0"></td>';
        $out_table_comprehensive .= '<td data-sort="0"></td>';
        $csv_rankings = '-';
        $csv_scrape = '-';
      }

      // SET A LINK - YES / NO
      if (!isset($this->linkfinder_res[$data[4]]) && $data_3_sort < 101 && $data[4] != $this->url_find) {
        $out_table_comprehensive .= '<td data-sort="1">1</td>';
        $csv_linkset = '1';
      } else {
        $out_table_comprehensive .= '<td data-sort="0"></td>';
        $csv_linkset = '';
      }

      // EXTRACTED META DATA FROM SITE
      if (isset($this->linkfinder_metas[$data[4]])) {
        $out_table_comprehensive .= '<td>'.$this->linkfinder_metas[$data[4]]['keywords'].'</td>';
        $out_table_comprehensive .= '<td>'.$this->linkfinder_metas[$data[4]]['application_name'].'</td>';
        $csv_meta_kw = $this->linkfinder_metas[$url]['keywords'];
        $csv_meta_an = $this->linkfinder_metas[$url]['application_name'];
      } else {
        $out_table_comprehensive .= '<td></td>';
        $out_table_comprehensive .= '<td></td>';
        $csv_meta_kw = '';
        $csv_meta_an = '';
      }

      $out_table_comprehensive .= '</tr>';

      $csv[] = array($keyword, $todo, $relevancy, $kw_in_ruk, $data[0], $marker, $opi_f, $data[1], $data[2], $dex, $data[3], $data[4], $csv_linkfinder, $csv_rankings, $csv_scrape, $csv_linkset, $csv_meta_kw, $csv_meta_an);

    }


// OUTPUT - COMPREHENSIVE
// ADD SCRAPE URLs THAT ARE NOT matched to a keyword
    foreach ($solitude_urls_scrape  as $key => $url) {

      $i++;

      $data_single = $this->res_kw[$this->keyword];

      $vsv     = ($data_single[0] == 0) ? 0.01 : $data_single[0];
      $vcpc    = ($data_single[1] == 0) ? 0.01 : $data_single[1];
      $vcomp   = ($data_single[2] == 0) ? 0.01 : $data_single[2];
      $opi     = $vsv * $vcpc * $vcomp;
      $opi_f   = $this->formatnumber($opi);
      $cpc_f   = $this->formatnumber($data_single[1]);
      $comp_f  = $this->formatnumber($data_single[2]);

      $relevancy_s = array_search($url, $this->urls_to_find_scrape_site);
      if ($relevancy_s !== false) {
        $relevancy = 'MUST';
        $relevancy_sort = $relevancy + 1;
      } else {
        $relevancy = 'OPTION';
        $relevancy_sort = 101;
      }

      $todo = 'Link vorhanden';
      if (!isset($this->linkfinder_res[$url]) && $url != $this->url_find) {
        $todo = 'Link setzen';
      } else if ($url == $this->url_find) {
        $todo = '';
      }

      if (isset($this->keyword_in_campaign[$this->keyword])) {
        $kw_in_ruk = 'Ja';
      } else {
        $kw_in_ruk = 'Nein';
      }

      // OUTPUT LOOP
      $out_table_comprehensive .= '<tr>
          <td>'. $i . '</td>
          <td class="nowrap">'.$this->keyword.'</td>
          <td data-sort="101">-</td>
          <td class="nowrap">'.$todo.'</td>
          <td data-order="'.$relevancy_sort.'">'.$relevancy.'</td>
          <td>'.$kw_in_ruk.'</td>
          <td data-order="'.$data_single[0].'">'.$data_single[0].'</td>
          <td>Keyword / SiteSearch</td>
          <td data-order="'.$opi.'">'.$opi_f.'</td>
          <td data-order="'.$data_single[1].'">'.$cpc_f.'</td>
          <td data-sort="'.$data_single[2].'">'.$comp_f.'</td>
          <td data-sort="0">-</td>
          <td data-sort="101"><a href="'.$url.'" target="_blank">' . $url . '</a></td>';

      // LINK auf Keyword optimierte URL
      if (isset($this->linkfinder_res[$url]) && $url != $this->url_find) {
        $out_table_comprehensive .= '<td data-sort="2"><i class="icon-ok"></i></td>';
        $csv_linkfinder = 'Ja';
      } else if ($url != $this->url_find) {
        $out_table_comprehensive .= '<td data-sort="1"><i class="icon-remove"></i></td>';
        $csv_linkfinder = 'Nein';
      } else {
        $out_table_comprehensive .= '<td data-sort="0"></td>';
        $csv_linkfinder = '-';
      }

      // RANKING URL in RS DATABASE
      // RANKING URL in GOOGLE [SITE: HOST KEYWORD] TOP 50

      if (array_search($url, $this->urls_to_find_rankings_db) !== false) {
        $out_table_comprehensive .= '<td data-sort="2"><i class="icon-ok"></i></td>';
        $csv_rankings =  'Ja';
      } else {
        $out_table_comprehensive .= '<td data-sort="1"><i class="icon-remove"></i></td>';
        $csv_rankings =  'Nein';
      }
      if (array_search($url, $this->urls_to_find_scrape_site) !== false) {
        $out_table_comprehensive .= '<td data-sort="2"><i class="icon-ok"></i></td>';
        $csv_scrape =  'Ja';
      } else {
        $out_table_comprehensive .= '<td data-sort="1"><i class="icon-remove"></i></td>';
        $csv_scrape =  'Nein';
      }

      // LINK TO KEYWORD URL?
      if (!isset($this->linkfinder_res[$url]) && $url != $this->url_find) {
        $out_table_comprehensive .= '<td data-sort="1">1</td>';
        $csv_linkset = '1';
      } else {
        $out_table_comprehensive .= '<td data-sort="0"></td>';
        $csv_linkset = '';
      }

      // EXTRACTED META DATA FROM SITE
      if (isset($this->linkfinder_metas[$url])) {
        $out_table_comprehensive .= '<td>'.$this->linkfinder_metas[$url]['keywords'].'</td>';
        $out_table_comprehensive .= '<td>'.$this->linkfinder_metas[$url]['application_name'].'</td>';
        $csv_meta_kw = $this->linkfinder_metas[$url]['keywords'];
        $csv_meta_an = $this->linkfinder_metas[$url]['application_name'];
      } else {
        $out_table_comprehensive .= '<td></td>';
        $out_table_comprehensive .= '<td></td>';
        $csv_meta_kw = '';
        $csv_meta_an = '';
      }

      $out_table_comprehensive .= '</tr>';

      $csv[] = array($keyword, $todo, $relevancy, $kw_in_ruk, $data_single[0], 'Keyword / SiteSearch', $opi_f, $data_single[1], $data_single[2], '-', '-', $url, $csv_linkfinder, $csv_rankings, $csv_scrape, $csv_linkset, $csv_meta_kw, $csv_meta_an);

    }

/*

0 - $keyword,
1 - $todo,
2 - $relevancy,
3 - $data_single[0],
4 - 'Keyword / SiteSearch',
5 - $opi_f, $data_single[1],
6 - $data_single[2],
7 - '-',
8 - '-',
9 - $url,
0 - $csv_linkfinder,
0 - $csv_rankings,
0 - $csv_scrape,
0 - $csv_linkset,
0 - $csv_meta_kw,
0 - $csv_meta_an

*/

// OUTPUT - BASIC OPO
    $url_set = array();
    // ADD KEYWORDS
    foreach ($csv as $key => $arr) {
      if (empty($arr[11])) {
        continue;
      }

      $url_set[$arr[11]] = array ( );

    }

    $x = 0;
    $out_table_basic = '';
    $out_table_core  = '';
    $missing_link_basic = array();
    $missing_link_basic[] = array('Source URL', 'Keyword', 'Relevanz', 'OPO ID', 'Target URL');
    $missing_link_core = array();
    $missing_link_core[] = array('Source URL', 'Keyword', 'Relevanz', 'Target URL');

    foreach ($url_set as $url => $data) {

      // CSV HEADLINE FIX
      if ($url == 'URL') {continue;}

      $x++;

      $relevancy_s = array_search($url, $this->urls_to_find_scrape_site);
      if ($relevancy_s !== false) {
        $relevancy = 'MUST';
        $relevancy_sort = $relevancy_s;
      } else {
        $relevancy = 'OPTION';
        $relevancy_sort = 101;
      }

      // LINK auf Keyword optimierte URL
      if (isset($this->linkfinder_res[$url]) && $url != $this->url_find) {
        $link_to_kw_url = '<td data-sort="2"><i class="icon-ok"></i></td>';
      } else if ($url != $this->url_find) {
        $link_to_kw_url = '<td data-sort="1"><i class="icon-remove"></i></td>';
      } else {
        $link_to_kw_url = '<td data-sort="0"></td>';
      }

      $out_table_basic .= '<tr>';
      $out_table_basic .= '<td>'.$x.'</td>';
      $out_table_basic .= '<td><a href="'.$url.'" target="_blank">'.$url.'</a></td>';
      $out_table_basic .= '<td>'.$this->keyword.'</td>';
      $out_table_basic .= '<td data-order="'.$relevancy_sort.'">'.$relevancy.'</td>';
      $out_table_basic .= $link_to_kw_url;
      $out_table_basic .= '<td>'.$this->linkfinder_metas[$url]['keywords'].'</td>';
      $out_table_basic .= '<td>'.$this->linkfinder_metas[$url]['application_name'].'</td>';
      $out_table_basic .= '</tr>';


// OUTPUT - BASIC
      if ($url != $this->url_find && !isset($this->linkfinder_res[$url])) {
        $out_table_core .= '<tr>';
        $out_table_core .= '<td>'.$x.'</td>';
        $out_table_core .= '<td><a href="'.$url.'" target="_blank">'.$url.'</a></td>';
        $out_table_core .= '<td>'.$this->keyword.'</td>';
        $out_table_core .= '<td data-order="'.$relevancy_sort.'">'.$relevancy.'</td>';
        $out_table_core .= '<td>'.$this->url_find.'</td>';
        $out_table_core .= '</tr>';

        $missing_link_basic[] = array($url, $this->keyword, $relevancy, $this->linkfinder_metas[$url]['application_name'], $this->url_find);
        $missing_link_core[] = array($url, $this->keyword, $relevancy, $this->url_find);

      }

    }


// OUTPUT CSV
    $filename_comprehensive = $this->writeCsv($csv);
    $filename_basic         = $this->writeCsv($missing_link_basic, 'missing-opo-');
    $filename_core          = $this->writeCsv($missing_link_core, 'missing-');

    $urls_to_find_unique    = array_unique ($this->urls_to_find);
    $count_missing_links    = count($missing_link_basic) - 1;


// MAIN OUTPUT
    $out = '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title">Informationen zur Datenbankabfrage</span></div><div class="box-content padded"><ul class="fixul"><li>Keyword: <strong>' . $this->keyword . ' </strong></li><li>Silo URL: <strong>' . $this->url_find . ' </strong></li><li>' . $i . ' relevante Keywords gefunden</li><li>' . count($urls_to_find_unique) . ' rankende URLs gefunden</li><li>' . count($this->linkfinder_res) . ' URLs mit Link (unique)</li><li>' . $count_missing_links . ' URLs ohne Link (unique)</li>';
    if ($count_missing_links > 0) {
      $out .= '<li>Auf folgenden ' . $count_missing_links . ' URLs <a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $filename_basic . '" target="_blank"></a><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProStructure | Link Silo Export Missing" data-filepath="' . $this->env_data['csvstore'] . $filename_basic . '"></i> sollte ein Link mit dem Linktext <b>' . $this->keyword . '</b> zu dieser URL gesetzt werden.</li>';
    }
    $out .= '</ul></div></div>';

    $out .= '
    <div class="box">
      <div class="box-header">
        <ul class="nav nav-tabs nav-tabs-left">
          <li class="active"><a href="#silourls" data-toggle="tab"><span>Ergebnisse nach URLs - Basic</span>
              <i title="CSV download" class="icon-cloud-download csv-request" data-href="' . $this->env_data['csvurl'] . $filename_core . '"></i>
              <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProStructure | Link Silo Export" data-filepath="' . $this->env_data['csvstore'] . $filename_core . '"></i></a>
          </li>
          <li><a href="#silourlsopo" data-toggle="tab"><span>Ergebnisse nach URLs - OPO</span>
              <i title="CSV download" class="icon-cloud-download csv-request" data-href="' . $this->env_data['csvurl'] . $filename_basic . '"></i>
              <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProStructure | Link Silo Export" data-filepath="' . $this->env_data['csvstore'] . $filename_basic . '"></i></a>
          </li>
          <li><a href="#silokeywords" data-toggle="tab"><span>Ergebnisse nach Keywords - Advanced</span> &nbsp;
              <i title="CSV download" class="icon-cloud-download csv-request" data-href="' . $this->env_data['csvurl'] . $filename_comprehensive . '"></i>
              <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProStructure | Link Silo Export" data-filepath="' . $this->env_data['csvstore'] . $filename_comprehensive . '"></i></a>
          </li>
        </ul>
      </div>
      <div class="box-content">
        <div class="tab-content">';

    $out .= '<div class="tab-pane active" id="silourls">';
    $out .= '<table class="table table-normal data-table dtoverflow" id="opo_dt_1"><thead><tr><td>#</td><td>Source URL</td><td>Keyword</td><td>Relevanz</td><td>Target URL</td></tr></thead>';
    $out .= $out_table_core;
    $out .= '</table></div>';

    $out .= '<div class="tab-pane" id="silourlsopo">';
    $out .= '<table class="table table-normal data-table dtoverflow" id="opo_dt_2"><thead><tr><td>#</td><td>URL</td><td>Keyword</td><td>Relevanz</td><td>Link auf Keyword optimierte URL?</td><td>OPO Keyword</td><td>OPO ID</td></tr></thead>';
    $out .= $out_table_basic;
    $out .= '</table></div>';

    $out .= '<div class="tab-pane" id="silokeywords">';
    $out .= '<table class="table table-normal data-table dtoverflow" id="opo_dt_3"><thead><tr><td>#</td><td>Keyword</td><td>Ranking für '.$this->hostname.'</td><td>ToDo</td><td>Relevanz</td><td>in Campaign</td><td>Suchvolumen / Monat</td><td>Quelle</td><td>OPI</td><td>CPC / Euro</td><td>Competition</td><td>OneDex</td><td>URL</td><td>Link auf Keyword optimierte URL?</td><td>OneKeywordDB</td><td>OneSiteSearch</td><td>Link set</td><td>OPO Keyword</td><td>OPO ID</td></tr></thead>';
    $out .= $out_table_comprehensive;
    $out .= '</table></div>';

    $out .= '</div></div></div></div>';

    echo $out;

  }


  public function checkKeywordSource ($kw) {

    $marker = '';

    if (isset($this->source_adwords[$kw])) {
      $marker .= 'Keyword DB, ';
    }

    if (isset($this->source_suggest[$kw])) {
      $marker .= 'Suggest DB, ';
    }

    if (isset($this->source_suggest_questions[$kw])) {
      $marker .= 'W-Fragen DB, ';
    }

    if (isset($this->source_adwords_url[$kw])) {
      $marker .= 'URL DB, ';
    }

    $marker = rtrim($marker, ', ');

    return $marker;

  }


  public function formatnumber($num) {
    return number_format($num, 2, ',', '.');
  }


  private function writeCsv ($csv, $pre = '') {

    $filename = 'link-silo-data-' . $pre . time() . '.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');

    foreach ($csv as $k => $line) {
      fputcsv($fp, $line);
    }

    fclose($fp);

    return $filename;

  }


  public function fetchRankingsRealtimeScrape($res_kw) {

    // WENN KEIN RANKING VORLIEGT UND SICH DAS KEYWORD NICHT IN DER KW DB BEFINDET
    // UND DAS SV > 0 ist
    $fetch_live_ranking = array();
    foreach ($res_kw as $keyword => $data) {
      if (!isset($data[3]) && !isset($this->keyword_in_campaign[$keyword]) && $data[0] > 0) {
        $fetch_live_ranking[] = array($keyword, 'de', $data[0]);
      }
    }

    // SORT BY SEARCHVOLUME
    $ssv = array();
    foreach ($fetch_live_ranking as $key => $row)
    {
      $ssv[$key] = $row[2];
    }
    array_multisort($ssv, SORT_DESC, $fetch_live_ranking);

    $fetch_live_ranking = array_slice($fetch_live_ranking, 0, 100);

    $fetch_live_ranking_all = $fetch_live_ranking;

    require_once ($env_data['path']. 'oneproseo/includes/liveranking.class.php');
    $rltRankings       = new liveranking($fetch_live_ranking_all, $this->hostname, $this->db);
    $realtime_rankings = $rltRankings->getResult();

    if (!empty($realtime_rankings)) {
      foreach ($realtime_rankings as $keyword => $data) {
        if ($data['p'] !== false) {
          // WHY???
          if (!isset($this->res_kw[$keyword])) {
            continue;
          }
          array_push($this->res_kw[$keyword], $data['p'], $data['u']);
          $this->urls_to_find_rankings_scrape[] = $data['u'];
        }
      }
    }

  }


  public function createProperUrl ($url)
  {

    $url = trim($url);

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return $url;

  }


  public function createProperHostname ($url)
  {

    $url = strtolower(trim($url));

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));

  }


  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }


}

?>
