<?php

require_once('urls_index.class.php');

class urls_index_delete extends urls_index
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->job_id = $_POST['job_id'];

    $this->deleteFromDb();

  }

  private function deleteFromDb () {

    $id = $this->job_id;

    $sql = "DELETE FROM structure_indexed_jobs WHERE id = $id";
    $result = $this->db->query($sql);

    $sql = "DELETE FROM structure_indexed_urls WHERE id_job = $id";
    $result = $this->db->query($sql);

  }

}

?>
