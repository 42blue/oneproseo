<?php

header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('CET');

class urls_index {


  public function setEnv ($env_data)
  {

    $this->env_data = $env_data;

  }


  public function startSession ()
  {

    session_start();
    // allow multiple ajax requests (SESSION)
    session_write_close();

    if ($_SESSION['login'] != true) {
      exit;
    }

  }

  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }

  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }

  // YMD YESTERDAY
  public function getYesterdayYMD ($ts)
  {
    return date('Y-m-d', strtotime('' . $ts . ' -1 day'));
  }

  // YMD YESTERDAY
  public function getOneWeekAgoYMD ($ts)
  {
    return date('Y-m-d', strtotime('' . $ts . ' -7 day'));
  }

  // YMD ONE MONTH AGO FIRST
  public function getFirstDayPrevMonth ()
  {
    return date('Y-m-d', strtotime('first day of previous month'));
  }

  // CORE GERMAN TS
  public function germanTSred ($ts)
  {
    if ($ts == '0000-00-00') {
      $ts = '2015-01-01';
    }
    return date('d.m.Y', strtotime($ts));
  }

  // PROPER GERMAN TS
  public function germanTS ($ts)
  {

    $day[0] = 'Sonntag';
    $day[1] = 'Montag';
    $day[2] = 'Dienstag';
    $day[3] = 'Mittwoch';
    $day[4] = 'Donnerstag';
    $day[5] = 'Freitag';
    $day[6] = 'Samstag';

    $dayn = date('w', strtotime($ts));

    return $day[$dayn] . ', ' . date('d.m.Y', strtotime($ts));

  }

  // PROPER GERMAN TS
  public function germanWD ($ts)
  {

    $day[0] = 'Sonntag';
    $day[1] = 'Montag';
    $day[2] = 'Dienstag';
    $day[3] = 'Mittwoch';
    $day[4] = 'Donnerstag';
    $day[5] = 'Freitag';
    $day[6] = 'Samstag';

    $dayn = date('w', $ts);

    return $day[$dayn];

  }

  // PROPER GERMAN TS
  public function germanWDshort ($ts)
  {

    $day[0] = 'So';
    $day[1] = 'Mo';
    $day[2] = 'Di';
    $day[3] = 'Mi';
    $day[4] = 'Do';
    $day[5] = 'Fr';
    $day[6] = 'Sa';

    $dayn = date('w', $ts);

    return $day[$dayn];

  }

  // HOSTNAME
  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  // SUBDOMAIN -> HOSTNAME
  public function pureHostNameNoSubdomain ($host)
  {

    $domain = $host;

    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) && $this->ignore_subd == 1) {
      return $regs['domain'];
    } else {
      return $host;
    }

  }

  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }

  // MYSQL CLOSER
  public function mySqlClose ()
  {

    $this->db->close();

  }


}

?>
