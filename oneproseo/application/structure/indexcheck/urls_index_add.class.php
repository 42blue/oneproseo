<?php

require_once('urls_index.class.php');
require_once('getsitemap.class.php');

class urls_index_add extends urls_index
{

  private $limit = 10000;

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    if (!isset($_POST['data'])) {
      exit;
    }

    parse_str($_POST['data'], $data);

    if (isset($data['urls'])) {
      $this->taskname = $data['task'];
      $urls           = strip_tags($data['urls']);
      $this->urls     = explode("\n", $urls);
      $this->saveToDb();
    }

    if (isset($data['urlxml'])) {
      $this->taskname = $data['task'];
      $this->xml_url  = $data['urlxml'];
      $parse_sitemap  = new getsitemap($this->xml_url, $env_data['tempcache']);
      $this->urls     = $parse_sitemap->getUrls();
      $this->saveToDb();
    }

  }

  private function saveToDb () {

    $used   = array();
    $values = array();

    $created_by  = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];

    if ($this->env_data['customer_id'] !== NULL) {
      $customer_id   = $this->env_data['customer_id'];
      $customer_name = $this->env_data['customer_name'];
      $sql = "INSERT INTO structure_indexed_jobs (name, date, status, created_by, email, source, customer_id) VALUES ('" . $this->taskname . "', '" . $this->dateYMD() . "', 'new', '" . $created_by . "', '" . $_SESSION['email'] . "', '" . $customer_name . "', $customer_id)";
    } else {
      $sql = "INSERT INTO structure_indexed_jobs (name, date, status, created_by, email) VALUES ('" . $this->taskname . "', '" . $this->dateYMD() . "', 'new', '" . $created_by . "', '" . $_SESSION['email'] . "')";
    }
    // INSERT JOB
    $this->db->query($sql);
    $insert_id = mysqli_insert_id($this->db);

    $i = 0;
    foreach($this->urls as $url) {
      $url = preg_replace('/\s/', '', $url);
      if (empty($url)) { continue; }
      // no duplicate urls
      if (in_array ($url, $used)) { continue; }
      $used[]   = $url;
      if ($i > $this->limit) {
        continue;
      }
      $url      = $this->db->real_escape_string($url);
      $values[] = "('" . $insert_id ."', '".$url."', 0)";
      $i++;
    }

    // INSERT URLS
    $sql = "INSERT INTO structure_indexed_urls (id_job, url, status) values ";
    $sql .= implode(',', $values);
    $result = $this->db->query($sql);

    echo 'Job hinzugefügt!<br />Ein Indexcheck ist derzeit auf ' . $this->limit . ' URLs begrenzt.';

  }

}

?>
