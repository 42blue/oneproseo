<?php

require_once('urls_index.class.php');

class urls_index_job_show extends urls_index
{

  public function __construct ($env_data)
  {

    if (!isset($_POST['job_id'])) {
      exit;
    }

    $this->job_id = strip_tags($_POST['job_id']);

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->getJob();

  }

  private function getJob () {


    $sql_c    = "SELECT * FROM structure_indexed_jobs WHERE id = $this->job_id";
    $result_c = $this->db->query($sql_c);

    while ($row = $result_c->fetch_assoc()) {
      $job_meta = $row;
    }

    $sql    = "SELECT * FROM structure_indexed_urls WHERE id_job = $this->job_id";
    $result = $this->db->query($sql);

    $csvh   = array();
    $csvh[] = array('URL', 'im Index');
    $csv    = array();

    $this->out_view = '';

    if ($result->num_rows < 1) {

      $this->out_view .= '<p style="padding:20px;">Keine Daten vorhanden.</p>';

    } else {

      $this->out_view .= '<table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>URL gesucht</td><td>URL gefunden</td><td>Im Google Index?</td>';
      $this->out_view .= '</tr></thead><tbody>';
      $i = 1;

      $out_notfound = 0;
      $out_found = 0;
      $out_tocheck = 0;

      while ($row = $result->fetch_assoc()) {

        if ($row['status'] == 0) {
          $out_tocheck++;
          $column = ' N/A ';
        }

        if ($row['status'] == 1) {
          $out_found++;
          $column = '<a target="_blank" href="'.$row['url_found'].'">' . $row['url_found'] . '</a>';
        }

        if ($row['status'] == 2) {
          $out_notfound++;
          $column = ' - ';
        } 

        $this->out_view .= '<tr><td>' . $i++ . '</td><td><a target="_blank" href="'.$row['url'].'">' . $row['url'] . '</a><a target="_blank" href="https://www.google.de/search?q=site:'.$row['url'].'"><i class="icon-link right"></i></a></td><td>'.$column.'<td>' . $this->status($row['status']) . '</td>';
        $csv[] = array($row['url'], $this->statusCsv($row['status']));
      }

      $this->out_view .= '</tbody></table>';

    }

    $this->out_view .= '</div></div>';

    $csvn = array_merge($csvh, $csv);
    $this->filename = $this->writeCsv($csvn);

    $this->out_view_head = '<div class="box">

    <div class="box-header"><span class="title">Analyse: '.$job_meta['name'].' vom ' . $this->germanTSred($job_meta['date']) .' (' . $job_meta['created_by'] . ')</span></div><div class="box-content padded"><ul class="content">
                    <li>Anzahl der URLs die geprüft werden sollen: <b>'.$result->num_rows.'</b></li>
                    <li>Anzahl der gefundenen URLs: <b>'.$out_found.'</b></li>
                    <li>Anzahl der nicht gefundenen URLs: <b class="m-red">'.$out_notfound.'</b></li>
                    <li>Anzahl der noch zu prüfenden URLs: <b>'.$out_tocheck.'</b></li>
                  </ul></div></div>';

    $this->out_view_head .= '<div class="box"><div class="box-header"><span class="title">Ergebnisse:</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProStructure | Indexcheck" data-filepath="' . $this->env_data['csvstore'] . $this->filename . '"></i></li></ul></div><div class="box-content">';

    echo $this->out_view_head . $this->out_view;

  }

  private function status ($status) {

    if ($status == 0) {
      return '<span class="label label-blue">noch nicht geprüft</span>';
    }
    if ($status == 1) {
      return '<span class="label label-green">Ja</span>';
    }
    if ($status == 2) {
      return '<span class="label label-red">Nein</span>';
    }

  }


  private function statusCsv ($status) {

    if ($status == 0) {
      return 'nicht geprüft';
    }
    if ($status == 1) {
      return 'Ja';
    }
    if ($status == 2) {
      return 'Nein';
    }

  }

  private function writeCsv ($csv) {
    $filename = 'indexcheck-'.time().'.csv';
    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');
    foreach ($csv as $k => $line) {
      fputcsv($fp, $line);
    }
    fclose($fp);
    return $filename;
  }

}

?>
