<?php

require_once('urls_index.class.php');

class urls_index_show extends urls_index
{

  public function __construct ($env_data)
  {

    parent::startSession();
    parent::setEnv($env_data);
    parent::mySqlConnect();

    $this->getJobs();

  }

  private function getJobs () {

    if ($this->env_data['customer_id'] !== NULL) {
      $customer_id = $this->env_data['customer_id'];
      $sql = "SELECT * FROM structure_indexed_jobs WHERE customer_id = $customer_id";
    } else {
      $sql = "SELECT * FROM structure_indexed_jobs";
    }

    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      $this->out_view = '<p style="padding:20px;">Keine Jobs vorhanden.</p>';
    } else {

      $this->out_view = '<table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Jobname</td><td>Anzahl URLs</td><td>Datum</td><td>angelegt von</td><td>Status</td><td>löschen</td><td>Quelle</td>';
      $this->out_view .= '</tr></thead><tbody>';
      $i = 1;
      while ($row = $result->fetch_assoc()) {
        $job_id = $row['id'];
        $sql_q           = "SELECT id FROM structure_indexed_urls WHERE id_job = $job_id";
        $result_q        = $this->db->query($sql_q);
        $rowcount        = mysqli_num_rows($result_q);
        $this->out_view .= '<tr><td>' . $i++ . '</td><td><a href="'.$row['id'].'">' . $row['name'] . '</a></td><td> ' . $rowcount . '</td><td data-order="'.$row['date'].'"> '  . $this->germanTSred($row['date']) . '</td><td> ' .$row['created_by']. '</td><td>'.$this->showStatus($row['id'], $row['status']).'</a></td><td><div class="btn-group"><button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button><ul class="dropdown-menu"><li><a href="#" class="ops_delete_job" data-jobid="'.$row['id'].'">Job löschen</a></li></ul></div></td><td>'. $row['source'] .'</a></td>';
      }
      $this->out_view .= '</tbody></table>';
    }

    echo $this->out_view;

  }

  private function showStatus ($id, $status) {

    if ($status == 'working') {
      return '<a href="'.$id.'" class="label label-blue">'  . $status . '</a>';
    }
    if ($status == 'done') {
      return '<a href="'.$id.'" class="label label-green">'  . $status . '</a>';
    }
    if ($status == 'new') {
      return '<a href="'.$id.'" class="label label-red">'  . $status . '</a>';
    }

  }

}

?>
