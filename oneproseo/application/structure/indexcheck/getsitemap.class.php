<?php

class getsitemap {

  private $sitemap = '';
  private $rounds  = 1000;
  private $output;
  private $urls    = array();

/**
  * getSitemap class constructor
	*
	* @access public
	* @param
	* @return
	*/

  public function __construct($url, $cache) {

    $this->sitemap = $this->getCurlUrl($url);
    $this->cache   = $cache;

    if (!$this->sitemap){exit;};

    // compressed or not?
    $file = pathinfo($this->sitemap);

    if ($file['extension'] == 'gz') {

      $this->gzprocessing();

    } else {

      $this->xmlprocessing($this->sitemap);

    }

    $this->parseSitemap($this->output);

  }

  public function getUrls () {
    return $this->urls;
  }

/**
  * parseSitemap function
	*
	* @access private
	* @param $this->output;
	* @return
	*/

  private function parseSitemap($output) {

    if (empty($output)) {
      return;
    }

    $xml = new XMLReader();
    $xml->XML($output);

    while ($xml->read()) {

      if ($xml->name === 'loc') {

        $ct  = $xml->readString();
        $ext = pathinfo($ct, PATHINFO_EXTENSION);

        if (!empty($ct) && $ext != 'xml') {

          array_push($this->urls, $ct);

        } else {
          $this->xmlprocessing($ct);
          $this->parseSitemap($this->output);
        }

      }

    }

  }


/**
  * xmlprocessing function
  *
  * @access private
  * @param curl response
  * @return $this->output
  */

  private function xmlprocessing($sitemapurl) {

    $content = $this->curl($sitemapurl);

    if (empty($content)) {
      $this->output = '';
      return;
    }

    // temp files
    $rand = mt_rand();
    $file = $this->cache .'sitemap-'. $rand .'.xml';

    // save the xml to disc
    $handle = fopen ($file, 'wb');
    fwrite($handle, $content);
    fclose($handle);

    // open the xml
    $handle = fopen ($file, 'r');
    $contents = fread($handle, filesize($file));
    fclose($handle);

    unlink($file);

    $this->output = $contents;

  }


/**
  * gzprocessing function
  *
  * @access private
  * @param curl response
  * @return $this->output
  */

  private function gzprocessing() {

    $content = $this->curl($this->sitemap);

    // temp files
    $rand = mt_rand();

    $file_gz = $this->cache .'sitemap-'. $rand .'.gz';
    $file_extract = $this->cache .'sitemap-'. $rand .'.xml';

    // save the gz to disc
    $handle = fopen ($file_gz, 'wb');
    fwrite($handle, $content);
    fclose($handle);

    // extract gz in chunks for better performance
    $sfp = gzopen($file_gz, 'rb');
    $fp = fopen($file_extract, 'w');
    while ($string = gzread($sfp, 4096)) {
      fwrite($fp, $string, strlen($string));
    }
    gzclose($sfp);
    fclose($fp);

    // read file
    $handle = fopen($file_extract, 'r');
    $contents = fread($handle, filesize($file_extract));
    fclose($handle);

    unlink($file_extract);
    unlink($file_gz);

    $this->output = $contents;

  }


/**
  * curl function
  *
  * @access private
  * @param $url
  * @return $this->output
  */

  private function curl($url) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6");
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    $output = curl_exec($ch);

    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if ($httpCode === 404) {
      echo 'Error: 404. Could not retrive Sitemap or submitted URL is wrong!';
      exit;
    }

    return $output;

    curl_close($ch);

  }


/**
  * getCurlUrl function
  *
  * @access private
  * @param $url;
  * @return $url;
  */

  private function getCurlUrl ($url) {

    if (strpos($url, 'http://') !== false or strpos($url, 'https://') !== false) {

      $url = $url;

    } else {

      $url = 'http://'.$url;

    }

    return $url;

  }


}

?>
