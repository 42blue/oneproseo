<?php

require_once 'crawler.class.php';

class newcrawl extends crawler {

  public function __construct ($env_data) {

    parent::setEnv($env_data);

    /* FORM SUBMIT SCHEMA:
       ["url"]=> string(7) "test.de"
       ["include"]=> string(18) "asdf,asdfasdf,adsf"
       ["exclude"]=> string(34) "sdf,dsfe,wer,werwev,sdfergr,ewrwer"
       ["getcsv"]=> string(18) "dfsdf,wrwer,werwer"
       ["useragent"]=> string(5) "nobot"
       ["numbersites"]=> string(9) "unlimited"
       ["email"]=> string(49) "sdfsdf,sdf,sdfsdfsdf,sdfsdfsdfsdf,sdfsdfsdfsdfsfd"
       ["frequency"]=> string(4) "once"
       ["htaccessusername"]=> string(4) "aaaa"
       ["htaccesspassword"]=> string(4) "vvvv"
    } */

    if (empty($_POST)) {
      $out = array ('error' => 'Keine Daten erhalten.');
      echo json_encode($out);
      exit;
    }

    $this->processInput();

    if (isset($this->error_message)) {
      $out = array ('error' => $this->error_message);
    } else {
      $this->addToDatabase();
      $out = array ('success' => 'Die URL '.$this->dataset['url'].' ist in der Warteschlange!');
    }

    echo json_encode($out);

  }


  public function addToDatabase() {

    $mdb = $this->dbConnect();

    $collection = $mdb->activecrawls;

    $this->dataset['status']        = 'new';
    $this->dataset['crawledsites']  = 0;
    $this->dataset['sitestocrawl']  = 0;
    $this->dataset['lastupdate']    = new MongoDate();
    $this->dataset['assigned']      = new MongoDate();

    if ($this->env_data['customer_version'] == 1) {
      $this->dataset['customer_id']   = $this->env_data['customer_id'];
      $this->dataset['customer_name'] = $this->env_data['customer_name'];
    }

    $collection->save($this->dataset);

  }


  public function processInput () {

    foreach ($_POST as $k => $v) {
      $clean_post[$k] = strip_tags($v);
    }

    $clean_post['url'] = $this->checkHttp($clean_post['url']);

    if (filter_var($clean_post['url'], FILTER_VALIDATE_URL) === FALSE or stripos($clean_post['url'], '.') === FALSE) {
      $this->error_message[] = $clean_post['url'] . ' ist keine valide URL!';
    }

    $clean_post['include']    = $this->extractTextareaLine($clean_post['include']);
    $clean_post['exclude']    = $this->extractTextareaLine($clean_post['exclude']);
    $clean_post['removeget']  = $this->extractString($clean_post['removeget']);

    if (!isset($clean_post['removegetall'])) {
      $clean_post['removegetall'] = FALSE;
    }

    // check email validity
    if (!empty($clean_post['email'])) {
      $clean_post['email'] = $this->extractString($clean_post['email']);
      foreach ($clean_post['email'] as $k => $email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
          $this->error_message[] = $email . ' ist keine valide Emailadresse!';
        }
      }
    }

    // create crawl collection name
    $parse = parse_url($clean_post['url']);
    $hostname = $parse['host'];

    $collection_name = str_replace('.', '-', $hostname);
    $dyn = $collection_name . '-' . time() . '-'. mt_rand(10, 99);
    $collection_name_analyse         = 'analyse-' . $dyn;
    $collection_name_crawl           = 'crawl-' . $dyn;
    $collection_name_stack           = 'stack-' . $dyn;
    $clean_post['analysecollection'] = trim($collection_name_analyse);
    $clean_post['crawlcollection']   = trim($collection_name_crawl);
    $clean_post['stackcollection']   = trim($collection_name_stack);
    $clean_post['frequency']         = 'once';

    $this->dataset = $clean_post;

  }


  public static function extractTextareaLine ($string) {

    if (!empty($string)) {
      $string = array_map('trim', explode("\n", $string));
    }

    return $string;

  }


  public static function extractString ($string) {

    if (!empty($string)) {
      $string = explode(',', $string);
    }

    return $string;

  }


  public static function checkHttp ($url) {

    $url = trim($url);

    if (stripos($url, 'http://') !== 0 && stripos($url, 'https://') !== 0) {
      $url = 'http://' . $url;
    }

    return $url;

  }


}


?>
