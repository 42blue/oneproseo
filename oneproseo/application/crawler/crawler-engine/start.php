
<?php

require_once 'config.php';
require_once 'db.php';
require_once 'spider.class.php';
require_once 'utils.class.php';

/* MONGO DB SCHEMA: 

{
  "_id": ObjectId("5367428884cd1fed34315e20"),
  "analysecollection": "analyse-golem-de-1399276168-99",
  "assigned": new Date(1399276168320),
  "crawlcollection": "crawl-golem-de-1399276168-99",
  "crawledsites": 1001,
  "email": "",
  "exclude": "",
  "frequency": "once",
  "include": "",
  "lastupdate": new Date(1399276827477),
  "numbersites": "1000",
  "removeget": "",
  "removegetall": false,
  "sitestocrawl": 14366,
  "speed": "moderate",
  "stackcollection": "stack-golem-de-1399276168-99",
  "status": "done",
  "url": "http://golem.de",
  "useragent": "google",
  "htaccessusername": "aaaa"
  "htaccesspassword": "vvvv"
}

*/

class doCrawl {


  private $iterations           = 1;
  private $process_iterations   = 6;


  public function  __construct () {

    $this->mdb = db::mongoconnection();

    $this->collection = $this->mdb->activecrawls;

    // find all crawl tasks, sort asc by timestamp and return the oldest
    // $data = $this->collection->find(array('status' => array('$in' => array('new', 'working', 'waiting', 'analyse', 'error'))))->sort(array('timestamp'=> 1))->limit(1)->getNext();
    $data = $this->collection->find(array('status' => array('$in' => array('new', 'working', 'waiting', 'analyse', 'error'))))->sort(array('timestamp'=> 1))->limit(2);

    foreach ($data as $key => $value) {
      $this->multiCrawl($value);
    }

  }

  private function multiCrawl ($data) {

    $this->data = $data;

    if ($this->data === NULL) {

      exit ("NOTHING TO DO \n\n");

    } else {

      utils::logToFile("CHECK: " . $this->data['url']);

      $datenow = new MongoDate();
      $crawldelay = $datenow->sec - $this->data['lastupdate']->sec;

      // check if working is broken
      if ($this->data['status'] === 'working' && $crawldelay >= 300) {

        $this->collection->update(array('_id' => $this->data['_id']), array('$set' => array('status' => 'waiting', 'lastupdate' => new MongoDate() )));
        exit ("NOTHING TO DO \n\n");

      } else if ($this->data['status'] === 'new' || $this->data['status'] === 'waiting') {

        //utils::logToFile('START CRAWL');
        $this->startCrawl();

      } else if ($this->data['status'] === 'done') {

        //utils::logToFile('CRAWL DONE');

      }

    }

  }


  private function startCrawl () {

    $this->id             = $this->data['_id'];
    $this->url            = $this->data['url'];
    $this->email          = $this->data['email'];
    $this->numbersites    = $this->data['numbersites'];
    $useragent            = utils::userAgentSetter(strip_tags(trim($this->data['useragent'])));

    // HTACCESS LOGIN
    $htaccesslogin = '';
    if (!empty($this->data['htaccessusername']) && !empty($this->data['htaccesspassword'])) {
      $htaccesslogin = $this->data['htaccessusername'] .':'. $this->data['htaccesspassword'];
    }

    // get crawl meta data 
    $this->search_params                      = array();
    $this->search_params['id']                = $this->id;
    $this->search_params['url']               = $this->url;
    $this->search_params['include']           = $this->data['include'];
    $this->search_params['exclude']           = $this->data['exclude'];
    $this->search_params['removeget']         = $this->data['removeget'];
    $this->search_params['removegetall']      = $this->data['removegetall'];
    $this->search_params['useragent']         = $useragent;
    $this->search_params['numbersites']       = $this->numbersites;
    $this->search_params['crawl_coll_name']   = $this->data['crawlcollection'];
    $this->search_params['stack_coll_name']   = $this->data['stackcollection'];
    $this->search_params['speed']             = $this->data['speed'];
    $this->search_params['login']             = $htaccesslogin;

    // SET CRAWL STATUS TO WORKING
    $this->collection->update(array('_id' => $this->id), array('$set' => array('status' => 'working', 'lastupdate' => new MongoDate() )));

    if ($this->data['crawledsites'] > 0) {
      // READ CRAWLED SITES FROM DISK
      $crawled_sites = utils::readUrl(TEMP_PATH . $this->data['crawlcollection'] . '.txt');
    } else {
      $crawled_sites = array();
    }

    //utils::logToFile('CRAWLED SITES READ');

    // SWITCH TO STACK COLLECTION and get the sites to crawl
    $st_collection = $this->data['stackcollection'];
    $this->stack_collection = $this->mdb->$st_collection;
    $stack_urls = $this->stack_collection->find(array('url' => array('$exists' => true)), array('url' => true, 'foundon' => true, '_id' => false));
    $stack_urls->timeout(-1);

    $stack = array();
    foreach ($stack_urls as $url) {
      $stack[$url['url']] = array('url' => $url['url'], 'foundon' => $url['foundon']);
    }
  
    //utils::logToFile('STACK DONE');

    // CRAWL 
    $this->processChunkedCrawl($crawled_sites, $stack);

    // CRAWLSTACK DONE  
    if (count($this->crawled_sites) > $this->numbersites or count($this->stack) < 1) {
      // SET CRAWL STATUS TO DONE WHEN FINISHED
      $this->collection->update(array('_id' => $this->id), array('$set' => array('status' => 'done', 'lastupdate' => new MongoDate() )));
      // drop stack collection
      $this->stack_collection->drop();
      //utils::logToFile('SPIDER DONE');
    } else {
      // SET CRAWL STATUS TO WAITING
      utils::writeUrl(TEMP_PATH . $this->data['crawlcollection'] . '.txt', $this->crawled_sites);
      $this->collection->update(array('_id' => $this->id), array('$set' => array('status' => 'waiting', 'lastupdate' => new MongoDate() )));
      //utils::logToFile('WAITING -> NEXT');
    }

  }


  private function processChunkedCrawl ($crawled_sites, $stack) {

    // START SPIDER
    $spider = new spider ($this->search_params, $crawled_sites, $stack, $this->mdb);

    $crawled_sites = $spider->getCrawledSites();
    $stack         = $spider->getStack();

    // BE CAREFUL WITH MEMORY
    unset($spider);

    // SAVE SITES TO CRAWL
    $this->stack_collection->remove();
    foreach ($stack as $k => $val) {
      $this->stack_collection->save(array('url' => $val['url'], 'foundon' => $val['foundon']));
    }

    // UPDATE CRAWL WITH NEW INFORMATION
    $this->collection->update(array('_id' => $this->id), array('$set' => array('sitestocrawl' => count($stack), 'crawledsites' => count($crawled_sites), 'lastupdate' => new MongoDate())));

    utils::logToFile('SAVED TO DB');

    // BREAK CONDITION
    if ($this->iterations >= $this->process_iterations or count($crawled_sites) >= $this->numbersites or count($stack) < 1) {
      //utils::logToFile('ITERATION DONE');
      $this->crawled_sites = $crawled_sites;
      $this->stack         = $stack;
      return;
    }

    $this->iterations++;

    // SPIDER AGAIN
    $this->processChunkedCrawl($crawled_sites, $stack);

  }


}

new doCrawl();

?>