<?php


class utils {

/**
  * erzeugt eine absolute URL
  *
  * anhand von baseURL und gefundenem link
  *
  * @param string $requestFile
  * @param string $baseHost
  * @return string $requestPath
  *
  * about.html          http://WebReference.com/html/about.html
  * tutorial1/          http://WebReference.com/html/tutorial1/
  * tutorial1/2.html    http://WebReference.com/html/tutorial1/2.html
  * /                   http://WebReference.com/
  * //www.internet.com/ http://www.internet.com/
  * /experts/           http://WebReference.com/experts/
  * ../                 http://WebReference.com/
  * ../experts/         http://WebReference.com/experts/
  * ../../../           http://WebReference.com/
  * ./                  http://WebReference.com/html/
  * ./about.html        http://WebReference.com/html/about.html
  *
  */

  public static function makeRequestUrl ($requestFile, $baseHost) {

    $phpUrlHost = parse_url($baseHost, PHP_URL_HOST);
    
    // Leerzeichen in Url
    if (stripos($requestFile, ' ') !== FALSE) {
      $requestFile = str_replace(' ', '%20', $requestFile);
    }

    // gefundene url beginnt mit http(s)
    if (stripos($requestFile, 'http://') === 0 or stripos($requestFile, 'https://') === 0) {

      $requestPath = $requestFile;

    // gefundene url beginnt mit /
    } else if (stripos($requestFile, '/') === 0) {

      // gefundene url beginnt mit //
      if (stripos($requestFile, '//') === 0) {
        $requestPath = parse_url($baseHost, PHP_URL_SCHEME) . ':' . $requestFile;
      } else {
        $requestPath = parse_url($baseHost, PHP_URL_SCHEME) . '://' . $phpUrlHost . $requestFile;
      }

    // gefundene url beginnt mit ../
    } else if (stripos($requestFile, '../') === 0) {

        $requestPath = self::removeRelative($requestFile, $baseHost);

    // gefundene url beginnt mit ./
    } else if (stripos($requestFile, '.') === 0) {

      if (stripos($requestFile, './') === 0) {
        $requestFile = substr($requestFile, 2);

        if (stripos($requestFile, '../') === 0) {
          $requestPath = self::removeRelative($requestFile, $baseHost);
        } else {
          $requestPath = substr ($baseHost, 0, strrpos($baseHost, '/')) . '/' . $requestFile;
        }

      } else {

        $requestPath = $baseHost;

      }

    // gefundene url beginnt mit ?
    } else if (stripos($requestFile, '?') === 0) {
      
      // ? in der base url ?
      if (stripos($baseHost, '?') !== FALSE) {
        $requestPath = substr ($baseHost, 0, strrpos($baseHost, '?')) . $requestFile;
      } else {
        $requestPath = $baseHost . $requestFile;
      }

    } else {

      // base url mit slash am ende
      if (strrpos($baseHost,'/') === strlen($baseHost) - 1) {

        $requestPath = $baseHost . $requestFile;

      // base url ohne http und // 
      } else if (stripos($baseHost,'/') === false) {

         $requestPath = parse_url($baseHost, PHP_URL_SCHEME) . '://' . $baseHost .  '/' . $requestFile;

      // http url mit file am ende
      } else if (substr_count($baseHost, '/') >= 3 and strrpos($baseHost,'.') > strrpos($baseHost,'/')) {

         $requestPath = substr ($baseHost, 0, strrpos ($baseHost, '/')) .'/'. $requestFile;

      // http url ohne slash am ende und requestFile ohne slashes // änderung 07.07.2015
      } else if (substr_count($baseHost, '/') >= 3 and strrpos($baseHost,'/') !== strlen($baseHost) - 1 and stripos($requestFile, '/') === false) {

        $requestPath = substr ($baseHost, 0, strrpos ($baseHost, '/')) .'/'. $requestFile;
        // self::logToFile($requestPath);
        // http://www.myplace.ch/was-bietet-myplace
        // was_bietet_myplace/transporthilfe.html

      // www.kreuzfahrten-sonderpreise.de/root/index.php?c=home/home ++ index.php?c=home/home
      } else if (substr_count($baseHost, '/') >= 3 and strrpos($baseHost,'/') !== strlen($baseHost) - 1 and strrpos($baseHost,'/') > strrpos($baseHost,'?')) {

        $baseHost = substr($baseHost, 0, stripos($baseHost, '?'));
        $requestPath = substr ($baseHost, 0, strrpos ($baseHost, '/')) .'/'. $requestFile;

      // base url ohne slash am ende / 
      } else if (!empty ($requestFile)) {

        $requestPath = $baseHost . '/' . $requestFile;

      } else {

        $requestPath = $baseHost;

      }

    }

    return $requestPath;

  }

/**
  * entfernt relative paramenter einer URL
  *
  * ../ 
  *
  * @param string $requestFile
  * @param string $baseHost
  * @param string $relative
  * @return string $requestPath
  *
  */

  public static function removeRelative ($requestFile, $baseUrl, $relative = '../') {

    // base url mit fragezeichen
    if (stripos($baseUrl,'?') > 0) {
      $baseUrl = (substr($baseUrl, 0, strrpos($baseUrl,'?')));
    }

    while (stripos($requestFile, $relative) === 0) {

      // case of ../../../../subdir-1-3/relative-linking-test-6.html and http://www.localseo.wiki/relative/linking/test/relative-linking.html
      if (substr_count($baseUrl, '/') > 3 || strrpos($baseUrl,'.') > strrpos($baseUrl,'/')) {

        // baseURL mit FILENAME oder baseURL mit abschließendem /
        if (strrpos($baseUrl,'.') > strrpos($baseUrl,'/') || strrpos($baseUrl, '/') !== FALSE) {
          $baseUrl = substr ($baseUrl, 0, strrpos($baseUrl, '/'));
        }

        $baseUrl = substr ($baseUrl, 0, strrpos($baseUrl, '/') + 1);

      }

      // entfernt die relativen strings ../
      $requestFile = substr ($requestFile, strlen($relative));

    }

    // siemens bug ../en/../en/contact.html
    if (strrpos($requestFile,'../') > 0) {
      $requestFile = substr($requestFile, strrpos($requestFile,'../') + 3);
    }

    return $baseUrl . $requestFile;

  }


/**
  * compare first url with crawled urls
  *
  * for not following redis to other domains
  *
  * @param string $url
  * @return string cleanedUrl
  *
  */

  public static function getHostRemoveWWW ($url, $debugger='') {

    $cleanedUrl = parse_url($url);

    if (isset($cleanedUrl['host'])) {

      $cleanedUrl = $cleanedUrl['host'];

      if (stripos($cleanedUrl, '?') !== FALSE) {
        $cleanedUrl = substr($cleanedUrl, 0, stripos($cleanedUrl, '?'));
      }

      if (stripos($cleanedUrl, 'www.') === 0) {
        $cleanedUrl = substr($cleanedUrl, 4);
      }

      return $cleanedUrl;

    }

  }


/**
  * findet files die für die analyse nicht notwendig sind
  *
  * anhand von dateiendungen
  *
  * @param string $foundUrl
  * @return boolean
  *
  */

  public static function checkForShit ($foundUrl) {

    $firstShit = array(
        'ftp:', 'javascript:', 'skype:', 'mailto:', 'ymsgr:', 'callto:', 'file:', 'tel:', 'feed:', 'bitcoin:', 'server:', 'fax:'
    );

    $middleShit = array(
        '#'
    );

    $lastShit = array(
        '.jpg', '.gif', '.png', '.svg', '.pdf', '.zip', '.rar', '.xml', '.css', '.doc', '.tar', '.wmv', '.wav',
        '.swf', '.flv', '.mp4', '.mp3', '.rss', '.apk', '.rar', '.xls', '.exe', '.dmg', '.ppt', '.txt', '.crt', '.crl', '.gz',
        '.m4v', '.mov'
    );
    
    if (strlen($foundUrl) > 3) {

      foreach ($firstShit as $check) {
        if (stripos($foundUrl, $check) === 0) {
          return false;
        }
      }

      foreach ($lastShit as $check) {
        if (strrpos($foundUrl, $check, -3) === strlen($foundUrl) -4) {
          return false;
        }
      }

    }

    foreach ($middleShit as $check) {
      if (stripos($foundUrl, $check) !== FALSE) {
        return false;
      }
    }

  }


/**
  * findet files die für die analyse nicht notwendig sind
  *
  * anhand von mimetypes der CURL INFO
  *
  * http://de.selfhtml.org/diverses/mimetypen.htm
  *
  * @param string $curlinfo
  * @return boolean
  *
  */

  public static function checkForShitMimeType ($curlinfo) {

    $shit = array('application', 'audio', 'image', 'video', 'xml');

    foreach ($shit as $check) {

      if (stripos($curlinfo, $check) !== false) {
        return false;
      }

    }

  }


/**
  * extractText function
  *
  * remove html, javascript, comments and css from document and return pure text
  *
  * @access public
  * 
  */

  public static function extractText ($node) {

    if (!empty($node)) {

      if (XML_TEXT_NODE === $node->nodeType || XML_CDATA_SECTION_NODE === $node->nodeType) {

        return $node->nodeValue;

      } else if (XML_ELEMENT_NODE === $node->nodeType || XML_DOCUMENT_NODE === $node->nodeType || XML_DOCUMENT_FRAG_NODE === $node->nodeType) {

         if ('script' === $node->nodeName || 'style' === $node->nodeName) {

           return;

         }

         // leave whitespace for correct text and wordings
         $text = ' ';

         foreach($node->childNodes as $childNode) {

             // workaround for javascript that has HTML elements as children
             if ('script' !== $childNode->nodeName) {

                $text .= self::extractText($childNode);

             }

         }

         return $text;

      }

    }

  }

/**
* setzt den User Agent
*
* @param string $agent
* @return string $useragent
*
*/

  public static function userAgentSetter ($agent) {

    if ($agent == 'google') {

      return 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)';

    } else if ($agent == 'bing') {

       return 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.bing.com/bingbot.htm)';

    } else if ($agent == 'yandex') {

       return 'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)';

    } else if ($agent == 'firefox') {

       return 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0';

    } else if ($agent == 'mobilesafari') {

       return 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5';

    } else {

       return 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.15 (KHTML, like Gecko) Chrome/24.0.1295.0 Safari/537.15';

    }

  }


/**
  * fileSizeWebsite function
  *
  * gets filezize via temp file
  *
  * @param $content
  * @return $filesize
  */

  public static function fileSizeWebsite($content) {

    //remove multiple whitespaces
    $content = preg_replace('/\s{2}/', '', $content);

    $filename = TEMP_PATH . 'tmp' . mt_rand(10, 99) . '-' . mt_rand(10, 99) . '.html';
    file_put_contents($filename, $content);
    clearstatcache();

    $filesize = filesize($filename);

    unlink ($filename);

    return $filesize;

  }



  public static function checkExInclude ($url, $arr) {

    foreach ($arr as $k => $url_include) {
      if (stripos($url, $url_include) !== FALSE or empty($url_include)) {
        return true;
      }
    }

    return false;

  }


  public static function readUrl ($src) {

    $file = fopen($src, 'r');
    $crawled_sites = array();
    while (!feof($file)) {
      $url = trim(fgets($file));
      $crawled_sites[$url] = 1;
    }
    fclose($file);

    return $crawled_sites;

  }


  public static function writeUrl ($src, $array) {

    $fp = fopen($src, 'w');
    foreach ($array as $url => $val) {
      fputs($fp, $url . "\n");
    }
    fclose($fp);

  }


/**
  * errorlogger function
  *
  *
  * @param $content
  */

  public static function logToFile ($content, $filename = 'debugger') {

    //$fh = fopen(TEMP_PATH . $filename . '.txt', 'a');
    //fwrite($fh, $content . "\r\n");
    //fclose($fh);

  }


}

?>