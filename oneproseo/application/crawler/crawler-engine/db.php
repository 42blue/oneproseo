<?php

class db {

  public static function mongoconnection () {

    try {

      $mongoCL = 'mongodb://' . constant('MONGO_USERNAME') . ':' . constant('MONGO_PASSWORD') . '@' . constant('MONGO_DBHOST');

      $mongo = new MongoClient($mongoCL);
      $mdb = $mongo->selectDB(constant('MONGO_DBNAME'));
      return $mdb;

    } catch (MongoCursorException $e) {

      echo 'error message: ' . $e->getMessage() . '<br />';
      echo 'error code: ' . $e->getCode() . '<br />';

    }

  }

}

?>