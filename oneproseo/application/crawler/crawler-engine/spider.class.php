<?php

class spider {

  private $rounds             = 0;
  private $crawledUrl         = array();
  private $umlaute            = array('&auml;', '&ouml;', '&uuml;', '&Auml;', '&Ouml;', '&Uuml', '&szlig;', 'ö', 'Ö', 'ü', 'Ü', 'ä', 'Ä');
  private $uml2hex            = array('%E4', '%F6', '%FC', '%C4', '%D6', '%DC', '%DF', '%C3%B6', '%c3%96', '%c3%bc', '%c3%9c', '%c3%a4', '%c3%84');
  private $specialCharOrigin  = array('ä', 'ö', 'ü', 'Ä', 'Ö', 'Ü', 'ß', '®', '©', '-', '…', '“', '&#8220;', '&#8221;', '&#8230;', '*', '&nbsp;', '„', '“', '&middot;', '&euro;', '€', '&#8364;', '&#x20AC;', '&#8211;', '&trade;');
  private $specialCharReplace = array('&auml;', '&ouml;', '&uuml;', '&Auml;', '&Ouml;', '&Uuml','&szlig;','&reg;','&copy;','-', '', '', '', '', '', '', ' ', '', '', '', 'EURO', 'EURO', 'EURO', 'EURO','-', '&reg;');


  public function __construct ($search_params, $crawledSites, $stack, $mdb) {

    $this->id                 = $search_params['id'];
    $this->userUrl            = $search_params['url'];
    $this->include            = $search_params['include'];
    $this->exclude            = $search_params['exclude'];
    $this->removeDynamicParam = $search_params['removeget'];
    $this->dynamic            = $search_params['removegetall'];
    $this->useragent          = $search_params['useragent'];
    $this->sitesToCrawl       = $search_params['numbersites'];
    $this->login              = $search_params['login'];
    $crawl_collection         = $search_params['crawl_coll_name'];
    $crawlspeed               = $search_params['speed'];

    $this->mdb = $mdb;
    $this->crawl_collection = $this->mdb->$crawl_collection;

    $this->crawledUrl = $crawledSites;

    // THE VERY FIRST URL
    if (count($stack) > 1) {
      $this->stack = $stack;
    } else {
      $this->stack[$this->userUrl] = array('url' => $this->userUrl, 'foundon' => $this->userUrl);
    }

    $this->setCrawlSpeed($crawlspeed);

    $this->crawl();

  }


/**
  *
  * SPIDER
  *
  */


  private function crawl() {

    // PREPARE MULTI CURL LIMIT TO X CONCURRENT REQUESTS

    if (count($this->stack) >= $this->concurrent) {
      $to_crawl    = array_slice($this->stack, 0, $this->concurrent);
      $this->stack = array_slice($this->stack, $this->concurrent);
    } else {
      $to_crawl = $this->stack;
      $this->stack = array();
    }

    // MULTI CURL
    utils::logToFile('START MULTI CURL');

    $multi_scrape = $this->multiCurl($to_crawl);

    // MAIN CRAWL LOOP
    foreach ($multi_scrape as $row) {

      $urlc               = $row['url'];
      $foundOn            = $row['found_on'];
      $this->responseBody = $row['body'];
      $this->urlEffective = $row['effective_url'];
      $this->responseHead = $row['header'];

      $this->responseTime = $row['time_transfer'];

      utils::logToFile('PROCESSING: ' . $row['url']);

      $this->rounds++;

      // check for mime type, avoid files, images, audio, video
      if (utils::checkForShitMimeType($row['content_type']) === false) {
        continue;
      }
      utils::logToFile('FIND ALL LINKS');

      // MAKE DOM OBJ FOR ENCODING
      $this->parseDOM($this->responseBody);

// THE ENCODING
      $this->responseBody = str_replace($this->specialCharOrigin, $this->specialCharReplace, $this->responseBody);
      $this->encoding = 'UTF-8';
      $this->getMetaTagforEncoding();

      if ($this->encoding !== 'UTF-8') {

        // sometimes ICONV returns no content and alerts E_NOTICE -> http://www.renehauser.com
        set_error_handler(array($this, 'custom_e_notice_handler'));
        try {
          $responseBody = iconv($this->encoding, $this->encoding.'//IGNORE', $this->responseBody);
        } catch (Exception $e) {
          $responseBody = $this->responseBody;
        }

      } else {

// NEW ENCODING TEST 
        set_error_handler(array($this, 'custom_e_notice_handler_mb'));
        try {
          $responseBody = mb_convert_encoding($this->responseBody, 'utf-8', mb_detect_encoding($this->responseBody));
          $responseBody = mb_convert_encoding($this->responseBody, 'html-entities', 'utf-8');
        } catch (Exception $e) {
          $responseBody = $this->responseBody;
        }

      }

// MAKE DOM OBJ WITH RIGHT ENCODING
      $this->parseDOM($responseBody);

// Check For Meta Refresh
      $this->checkForMetaRefresh();

// effective url after the first redirect
      if ($this->rounds <= 1) {
        $this->urlAfterFirstRedirect = $this->urlEffective;
      }

// GET <BASE> TAG
      $this->base = '';
      $checkforbase = $this->dom->getElementsByTagName('base');
      foreach ($checkforbase as $base) {
        $this->base = $base->getAttribute('href');
      }

// inpage redirects?
      if ($this->checkForRedirect() === true) {
        $urleff = $this->urlEffective;
      }

// dont crawl 400 and 500 sites
      if ($this->errorPage() === true) {
        continue;
      }

      // FIND ALL LINKS
      $anchors = array();

      utils::logToFile('FIND ALL LINKS');

      foreach($this->dom->getElementsByTagName('a') as $item) {

        // MAYBE THE LINK HAS NO TEXT BUT AN IMAGE
        $guessIMG = $item->getElementsByTagName('img');
        if ($guessIMG->length == 0) {
          $linktext = $item->textContent;
        } else {
          foreach ($guessIMG as $key => $element) {
            $linktext = '[IMG ALT] ' . $element->getAttribute('alt');
          }
        }

        $url = $item->getAttribute('href');

        $foundUrl = trim($url);

        // crawlbare URL erzeugen
        if (!empty($this->base)) {
          $foundUrl = utils::makeRequestUrl($foundUrl, $this->base);
        } else {
          $foundUrl = utils::makeRequestUrl($foundUrl, $urlc);
        }

        $nofollow = false;
        if ($item->getAttribute('rel') == 'nofollow') {
          $nofollow = true;
        }

        $anchors[] = array('href' => $foundUrl, 'foundOn' => $urlc, 'linktext' => $linktext, 'nofollow' => $nofollow);

      }

      // PUSH TO RESULT
      $this->makeResult($urlc, $anchors, $foundOn);

      utils::logToFile('PROCESS ALL LINKS: ' . count($anchors));

      $processed_links = 0;

      // PROCESS ALL LINKS
      foreach ($anchors as $set) {

        $processed_links++;

        if ($processed_links > $this->links_to_process) {
           break;
        }

        $foundUrl = $set['href'];
        $foundOn  = $set['foundOn'];

        // don't count empty 
        if (empty($foundUrl)) {
          continue;
        }

       // UMLAUTE IN URL 2 HEX
        $foundUrl = str_replace($this->umlaute, $this->uml2hex, $foundUrl);

        // REMOVE DYN PARAMS
        if ($this->dynamic == 'true') {
          $foundUrl = $this->removeDynamicParams($foundUrl);
        }

        // Hash Tags, Bilder oder anderer Schwachsinn?
        if (utils::checkForShit($foundUrl) === false) {
          continue;
        }

        // nur interne urls parsen
        if (!empty($foundUrl) && utils::getHostRemoveWWW($foundUrl) != utils::getHostRemoveWWW($this->urlAfterFirstRedirect)) {
          continue;
        }

        if (!empty($this->include)) {
          if (utils::checkExInclude($foundUrl, $this->include) === false) {
            continue;
          }
        }

        if (!empty($this->exclude)) {
          if (utils::checkExInclude($foundUrl, $this->exclude) === true) {
            continue;
          }
        }

        // remove user specified dynamic params form url 
        if (!empty($this->removeDynamicParam)) {
          $foundUrl = $this->removeGetParams($foundUrl);
        }

        // POPULATE STACK WITH NEW FOUND LINKS
        if (!isset($this->crawledUrl[$foundUrl])) {
          $this->stack[$foundUrl] = array('url' => $foundUrl, 'foundon' => $foundOn);
        }

      }

    }

  }



/**
  * multi curl site
  *
  */

  private function multiCurl ($urls) {

    $handleArray = array();
    $result      = array();

    foreach ($urls as $k => $reqUrl) {

      $url = $reqUrl['url'];

      $k = curl_init();
      curl_setopt($k, CURLOPT_URL, $url);
      curl_setopt($k, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($k, CURLOPT_TIMEOUT, $this->curl_timeout);
      curl_setopt($k, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($k, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($k, CURLINFO_HEADER_OUT, 1);
      curl_setopt($k, CURLOPT_VERBOSE, 1);
      curl_setopt($k, CURLOPT_HEADER, 1);
      curl_setopt($k, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($k, CURLOPT_MAXREDIRS, 20); 
      curl_setopt($k, CURLOPT_USERAGENT, $this->useragent);
      curl_setopt($k, CURLOPT_REFERER, 'http://www.google.com');
      curl_setopt($k ,CURLOPT_ENCODING , ' ');

      if (!empty($this->login)) {
        curl_setopt ($k, CURLOPT_USERPWD, $this->login);
      }

      array_push($handleArray, array($k, $reqUrl['foundon'], $reqUrl['url']));

    }

    // create multi handle
    $mh = curl_multi_init();

    // add handles
    foreach ($handleArray as $handle) {
      curl_multi_add_handle($mh, $handle[0]);
    }

    $active = NULL;
    do {
      $ret = curl_multi_exec($mh, $active);
    } while ($ret == CURLM_CALL_MULTI_PERFORM);

    while ($active && $ret == CURLM_OK) {
      if (curl_multi_select($mh) != -1) {
        do {
           $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);
      }
    }

    foreach ($handleArray as $v => $handle) {

      $response = curl_multi_getcontent($handle[0]);

      $headerSize = curl_getinfo($handle[0], CURLINFO_HEADER_SIZE);

      $header        = substr($response , 0, $headerSize);
      $body          = substr($response , $headerSize);
      $urlEffective  = curl_getinfo($handle[0], CURLINFO_EFFECTIVE_URL);
      $contentType   = curl_getinfo($handle[0], CURLINFO_CONTENT_TYPE);
      $time_lookup   = curl_getinfo($handle[0], CURLINFO_NAMELOOKUP_TIME);
      $time_connect  = curl_getinfo($handle[0], CURLINFO_CONNECT_TIME);
      $time_pretrans = curl_getinfo($handle[0], CURLINFO_PRETRANSFER_TIME);
      $time_transfer = curl_getinfo($handle[0], CURLINFO_STARTTRANSFER_TIME);

      $result[$v] = array(
                           'header' => $header,
                           'body' => $body,
                           'effective_url' => $urlEffective,
                           'content_type' => $contentType,
                           'found_on' => $handle[1],
                           'url' => $handle[2],
                           'time_lookup' => $time_lookup,
                           'time_connect' => $time_connect,
                           'time_pretrans' => $time_pretrans,
                           'time_transfer' => $time_transfer
                          );

      curl_multi_remove_handle($mh, $handle[0]);

    }

    curl_multi_close($mh);

    return $result;

  }



/**
* erzeugt aus dem response string ein DOM object
*
* @param string $html
* @return object $this->dom
*
*/

  private function parseDOM ($html) {

    $this->dom = new DOMDocument('1.0');
    $this->dom->loadHTML($html);
    $this->xpath = new DOMXpath($this->dom);

  }


/**
  * entfernt User spezifische GET Parameter als dem URL String
  *
  * anhand der gefundenen url Merkmale
  *
  * @param string $url
  * @return string $url
  *
  */

  private function removeGetParams ($url) {

    foreach ($this->removeDynamicParam as $removeParam) {

      if (stripos($url, $removeParam) !== false) {

         $foundStart = stripos($url, $removeParam);

         if (stripos($url, '?' . $removeParam) !== false && stripos($url, '&', $foundStart) !== false) {
            // first get param with additional get params
            $foundEnd = stripos($url, '&', $foundStart)+1;
            $foundStart = $foundStart;
         } else if (stripos($url, '&', $foundStart) !== false) {
            // get param before and after
            $foundEnd = stripos($url, '&', $foundStart);
            $foundStart = $foundStart - 1;
         } else {
            // last get param or only one
            $foundEnd = strlen ($url) + 1;
            $foundStart = $foundStart - 1;
         }

        $url = substr_replace($url, '', $foundStart, $foundEnd - $foundStart);

      }

    }

   return $url;

  }


/**
  * entfernt dynamische parameter aus der url 
  *
  * anhand von stringvergleichen
  *
  * @param string $foundUrl
  * @return string $foundUrl
  *
  */

  private function removeDynamicParams ($foundUrl) {

    $query = stripos($foundUrl, '?');
    if ($query !== false) {
      $foundUrl = substr($foundUrl, 0, $query);
    }

    $jsession = stripos($foundUrl, ';jsession');
    if ($jsession !== false) {
      $foundUrl = substr($foundUrl, 0, $jsession);
    }

    $sid = stripos($foundUrl, ';sid');
    if ($sid !== false) {
      $foundUrl = substr($foundUrl, 0, $sid);
    }

    return $foundUrl;

  }

/**
  * response header parsen
  *
  * parst den header nach http response codes und ggf. neuen locations
  *
  * @param string $url
  * @return string $httpResponseCode
  *
  */

  private function parseHttpResponseCode ($url) {

    $httpResponseCode = array();

    if (!empty($this->responseHead)) {

      preg_match_all('|HTTP/\d\.\d\s+(\d+)|',$this->responseHead, $httpResponse);
      $newLocation = $this->parseHeaderForLocation();

      // add first url to array 
      array_unshift ($newLocation[1], $url);

      // follow the http hops
      for ($i = 0; $i < count($httpResponse[1]); ++$i) {
        $href = utils::makeRequestUrl($newLocation[1][$i], $url);
        $httpResponseCode[] = array($href, $httpResponse[1][$i]);
      }

    } else {

      $httpResponseCode[] = array($url, 'undefined');

    }


    return $httpResponseCode;

  }


/**
  * checkForRedirect
  *
  * während des crawls können redirects passieren 
  * stellt sicher das immer eine korrekte basisurl verwendet wird
  *
  * @return bool
  *
  */

  private function checkForRedirect() {

    if (!empty($this->responseHead)) {

      $loc = $this->parseHeaderForLocation();

      if(!empty($loc)) {
        return true;
      }

    } else {

      return false;

    }

  }


  private function parseHeaderForLocation () {

    preg_match_all ('|Location: (.*?)\\r\\n|',$this->responseHead, $result);

    return $result;

  }


/**
  * x-robots header parsen
  *
  * parst den header nach x-robots angaben
  *
  * @param string $url
  * @return string $xRobots
  *
  */

  private function parseXRobots () {

    $xRobots = '';

    if (!empty($this->responseHead)) {

      preg_match_all('|X-Robots-Tag: (.*?)\\r\\n|',$this->responseHead, $headerRobots);

      foreach($headerRobots[1] as $v) {
        $xRobots = $v;
      }

    }

    if (!empty($xRobots)) {
      return $xRobots;
    }

  }


/**
  * checkForMetaRefresh function
  *
  * look if there is a meta refresh with an url
  *
  * @access private
  * @requires $this->dom
  * @sets     $this->metaRefresh
  * @return void
  */

  public function checkForMetaRefresh () {

    $this->metaRefresh = '';

    $param = $this->dom->getElementsByTagName('meta');

    foreach ($param as $key => $value) {

      if ($value->getAttribute('http-equiv') == 'refresh') {

        if (stripos($value->getAttribute('content'), 'url')) {

          $this->metaRefresh = substr($value->getAttribute('content'), stripos($value->getAttribute('content'), '=')+1);

        }

      }

    }

  }


/**
  * getMetaTagforEncoding function
  *
  * gets encoding information from meta tag 
  *
  * @access private
  * @requires $this->websiteDOM 
  * @sets     $this->encoding
  * @return void
  */

  private function getMetaTagforEncoding () {

    $param = $this->dom->getElementsByTagName('meta');

    foreach ($param as $key => $value) {

      if (strtolower($value->getAttribute('http-equiv')) == 'content-type' or strtolower($value->getAttribute('name')) == 'content-type') {

         $mMeta = $param->item($key)->getAttribute('content');
         $teilen = explode('=', $mMeta);

         $this->encoding = strtoupper($teilen[1]);

      } else if (strtolower($value->getAttribute('charset')) !== ''){

         $this->encoding = strtoupper($param->item($key)->getAttribute('charset'));

      } 

    }

  }


/**
  * errorPage function
  *
  * does the page respond with a 4xx or a 5xx
  *
  * @access private
  * @requires $this->responseHead 
  * @return bool
  */

  private function errorPage () {

    if (!empty($this->responseHead)) {

      preg_match_all('|HTTP/\d\.\d\s+(\d+)|', $this->responseHead, $shortResponse);

      foreach ($shortResponse[1] as $status) {

        if (stripos($status, '4') === 0 || stripos($status, '5') === 0) {
          return true;
        } else {
          return false;
        }
      }

    } else {

       return false;

    }

  }

/**
  * custom_e_notice_handler function
  *
  * sometimes ICONV returns no content and alerts E_NOTICE -> http://www.renehauser.com
  *
  * @access public
  * @throws exception
  */

  public function custom_e_notice_handler ($errno, $errstr) {

    if ($errno == E_NOTICE && stripos($errstr, 'iconv') !== FALSE) {

      throw new Exception('ICONV EXCEPTION');

    }

  }

  public function custom_e_notice_handler_mb ($errno, $errstr) {

    if ($errno == E_NOTICE && stripos($errstr, 'character ') !== FALSE) {

      throw new Exception('ICONV EXCEPTION');

    }

  }


/**
  * setCrawlSpeed function
  *
  * @access private
  * @return void
  */

  private function setCrawlSpeed($crawlspeed) {

    if ($crawlspeed == 'zombie') {

      $this->concurrent = 100;
      $this->curl_timeout = 20;

      if (count($this->stack) > 200000) {
        $this->links_to_process = 35;
      } elseif (count($this->stack) < 40000) {
        $this->links_to_process = 5000;
      } else {
        $this->links_to_process = 500;
      }

    } elseif ($crawlspeed == 'fast') {

      $this->concurrent = 25;
      $this->curl_timeout = 30;

      if (count($this->stack) > 400000) {
        $this->links_to_process = 100;
      } elseif (count($this->stack) < 40000) {
        $this->links_to_process = 5000;
      } else {
        $this->links_to_process = 1000;
      }

    } else {

      $this->concurrent = 4;
      $this->curl_timeout = 40;

      if (count($this->stack) > 500000) {
        $this->links_to_process = 30;
      } elseif (count($this->stack) < 40000) {
        $this->links_to_process = 5000;
      } else {
        $this->links_to_process = 2500;
      }

    }

  }


/**
  * AUSGABE ARRAY erstellen
  *
  * erzeught die Ausgabe Infos 
  *
  * @param string $url
  * @param string $anchors
  *
  */

  private function makeResult ($url, $allLinks, $foundOn) {

    // parse HTTP Response
    $httpResponseCode = $this->parseHttpResponseCode($url);

    // filesize of the complete doc
    $fileSize = utils::fileSizeWebsite($this->responseBody);

    // DOM PARSING
    $titleTag       = array();
    $metaDesc       = array();
    $xRobot         = array();
    $metaRobot      = array();
    $metaKey        = '';
    $bodyTagContent = '';
    $canonical      = array();
    $relnext        = array();
    $relprev        = array();
    $missingAltImg  = array();
    $framsesets     = false;
    $inlineJS       = 0;
    $styleTags      = 0;
    $styleAttr      = 0;

    $linkTags   = $this->dom->getElementsByTagName('link');
    $aTags      = $this->dom->getElementsByTagName('a');
    $metaTags   = $this->dom->getElementsByTagName('meta');
    $h1Tags     = $this->dom->getElementsByTagName('h1');
    $h2Tags     = $this->dom->getElementsByTagName('h2');
    $imgTags    = $this->dom->getElementsByTagName('img');
    $frameTags  = $this->dom->getElementsByTagName('frame');

    $bodyTagContent = $this->dom->getElementsByTagName('body');
    $bodyTagContent = $bodyTagContent->item(0);
    $bodyTagContent = utils::extractText($bodyTagContent);

    $body_tag_striped = preg_replace('/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/i','', $bodyTagContent);
    $body_tag_striped = strip_tags($body_tag_striped);
    $body_tag_striped = trim($body_tag_striped);
    $words_body_tag   = str_word_count($body_tag_striped);

    // title tags
    foreach ($this->dom->getElementsByTagName('title') as $key => $element) {
      $titleTag[] = strip_tags($element->textContent);
    }

    // img tags without alt
    foreach ($imgTags as $key => $element) {
      $foundImgAlt = $element->getAttribute('alt');
      if (empty($foundImgAlt)){
        $missingAltImg[] = $element->getAttribute('src');
      }
    }

    // canonical tags
    foreach ($linkTags as $key => $value) {
      if ($value->getAttribute('rel') == 'canonical') {
        $canonical[] = $linkTags->item($key)->getAttribute('href');
      }
      if ($value->getAttribute('rel') == 'prev') {
        $relprev[] = $linkTags->item($key)->getAttribute('href');
      }
      if ($value->getAttribute('rel') == 'next') {
        $relnext[] = $linkTags->item($key)->getAttribute('href');
      }

    }

    // xRobots
    $xRobot[] = $this->parseXRobots();

    // meta description, meta robots
    foreach ($metaTags as $key => $value) {
      if (strtolower($value->getAttribute('name')) == 'description' or strtolower($value->getAttribute('name')) == 'dc.description') {
        $metaDesc[] = strip_tags($metaTags->item($key)->getAttribute('content'));
      }

      if (strtolower($value->getAttribute('name')) == 'keywords' or strtolower($value->getAttribute('name')) == 'dc.keywords') {
        $metaKey = $metaTags->item($key)->getAttribute('content');
      }

      if (strtolower($value->getAttribute('name')) == 'robots') {
        $metaRobot[] = $metaTags->item($key)->getAttribute('content');
      }
    }

    // h1 contents
    $h1content = array();
    foreach ($h1Tags as $value) {
      // maybe the content is an image?  
      $guessIMG = $value->getElementsByTagName('img');
      if ($guessIMG->length == 0) {
        $h1content[] = $value->textContent;
      } else {
        foreach ($guessIMG as $key => $element) {
          $h1content[] = $element->getAttribute('src');
        }
      }
    }

    // h2 contents
    $h2content = array();
    foreach ($h2Tags as $value) {
      // maybe the content is an image?  
      $guessIMG = $value->getElementsByTagName('img');
      if ($guessIMG->length == 0) {
        $h2content[] = $value->textContent;
      } else {
        foreach ($guessIMG as $key => $element) {
          $h2content[] = $element->getAttribute('src');
        }
      }
    }

    // framesets
    foreach ($frameTags as $value) {
      $framsesets = true;
    }

    // XPATH QUERYS
    $mXpathQuerys = array (
       'inlineJS'   => '//script[not(@src)]',
       'styleTags'  => '//style',
       'styleAttr'  => '//*[@style]'
     );

    foreach ($mXpathQuerys as $key => $query) {
      $entries = $this->xpath->query($query);
      foreach ($entries as $found) {
        ${$key}++;
      }
    }

    // OUTPUT ARRAY
    $output = array(
        'url'            => $url,                //string
        'response'       => $httpResponseCode,   //array
        'responseTime'   => $this->responseTime, //string
        'responseHead'   => $this->responseHead, //string
        'titleTag'       => $titleTag,           //array
        'metaDesc'       => $metaDesc,           //array
        'metaKey'        => $metaKey,            //string
        'metaRobot'      => $metaRobot,          //array 
        'xRobot'         => $xRobot,             //array 
        'canonical'      => $canonical,          //array
        'relPrev'        => $relprev,            //array
        'relNext'        => $relnext,            //array
        'h1content'      => $h1content,          //array
        'h2content'      => $h2content,          //array
        'bodyTagContent' => $bodyTagContent,     //string
        'bodyTagWords'   => $words_body_tag,     //number
        'allLinks'       => $allLinks,           //array
        'metaRefresh'    => $this->metaRefresh,  //string
        'fileSize'       => $fileSize,           //number
        'missingAltImg'  => $missingAltImg,      //array
        'framesets'      => $framsesets,         //bool
        'inlineJS'       => $inlineJS,           //number
        'styleTags'      => $styleTags,          //number
        'styleAttr'      => $styleAttr,          //number
        'foundOn'        => $foundOn             //string
        ); 


    try {

      $checkIfDuplicate = $this->crawl_collection->find(array('url' => $output['url']))->limit(1);

      if ($checkIfDuplicate->count() === 0) {

        $this->crawl_collection->save($output);
        $this->crawledUrl[$output['url']] = '';

      } else {

        $this->crawledUrl[$output['url']] = '';

      }

    } catch (Exception $e) {

      utils::logToFile('MONGO SAVE: ' . $e->getMessage());

    }

    unset($output);

  }


  public function getCrawledSites () {

    return $this->crawledUrl;

  }


  public function getStack () {

    return $this->stack;

  }


}

?>