<?php

require_once 'crawler.class.php';

class deletecrawl extends crawler {

  public function __construct ($env_data) {

    parent::setEnv($env_data);

    if (empty($_POST['id'])) {
      echo 'Crawl ID Missing!';
      exit;
    }

    $id = strip_tags($_POST['id']);

    $this->deleteCrawl($id);

  }


  public function deleteCrawl ($id) {

    $mdb = $this->dbConnect();

    $collection = $mdb->activecrawls;

    $cursor = $collection->findOne(array('_id' => new MongoId($id)), array('crawlcollection' => true, 'stackcollection' => true, '_id' => false));

    // drop crawl collection
    $collection2 = $mdb->$cursor['crawlcollection'];
    $collection3 = $mdb->$cursor['stackcollection'];

    $collection2->drop();
    $collection3->drop();

    // remove doc from active crawl collection
    $collection->remove(array('_id' => new MongoId($id)), array('justOne' => true));

  }


}

?>