<?php

require_once 'crawler.class.php';

class dashboard extends crawler {

/* MONGO DB SCHEMA: 
{
  "_id": { "$oid" : "533e5e8bdf3461533f000000" },
  "url": "http://hrs.de",
  "include": "",
  "exclude": "",
  "removeget": "",
  "useragent": "google",
  "numbersites": "10",
  "email": "",
  "frequency": "once",
  "removegetall": false,
  "crawlcollection": "crawl-hrs-de-1396596363-22",
  "status": "new",
  "crawledsites": [

  ],
  "sitestocrawl": [

  ],
  "lastupdate": { "$date": 1396596363599 },
  "assigned": { "$date": 1396596363599 }
}
*/


  public function __construct ($env_data) {

    parent::setEnv($env_data);

    $mdb = $this->dbConnect();

    $this->collection = $mdb->activecrawls;

    // ONCE, WEEKLY, MONTLY DASHBOARDS
    if (isset($_POST['status'])) {

      $this->status = $_POST['status'];

      if ($this->status == 'once') {

        $active_query  = array('frequency' => array('$in' => array ('once')));
        $query = $this->fetchTableData($active_query);

      } elseif ($this->status == 'active') {

        $active_query  = array('status' => array('$in' => array ('new', 'waiting', 'working', 'analyse')));
        $query = $this->fetchTableData($active_query);

      } elseif ($this->status == 'resolved') {

        $resolved_query = array('status' => array('$in' => array ('done', 'error')));
        $query = $this->fetchTableData($resolved_query);

      }

      $json_dataset = array ('active' => $this->renderTableData($query));

      echo json_encode($json_dataset);

    }

  }

  public function fetchTableData ($mongo_query) {
    $query = $this->collection->find(  $mongo_query  )->sort(array('assigned' => -1));
    return $query;
  }

  public function renderTableData ($query) {

    $out = '<table class="table table-normal" id="data-table">
               <thead>
                 <tr>
                   <td class="w20">Kunde</td>
                   <td class="w20">Seite</td>
                   <td>Crawl gestartet</td>
                   <td>Gecrawlte Seiten</td>';

    if ($this->status == 'active') {
      $out .= ' <td>Queue</td>';
    }

    $out .= '  <td class="w5">Löschen</td>
                   <td class="w10">Ergebnis</td>
                 </tr>
               </thead>
             <tbody>';

      foreach ($query as $data) {

        if ($data['customer_id'] != $this->env_data['customer_id'] && $this->env_data['customer_id'] != NULL) {
          continue;
        }

        if ($data['customer_name'] == NULL) {
          $data['customer_name'] = 'Enterprise';
        }

        $out .= '<tr>
            <td>'. $data['customer_name'] .'</td>
            <td><a href="analyse/'.$data['crawlcollection'].'" target="_blank">' . $data['url'] . '</a></td>
            <td>' . date("d.m.Y / H:i", $data['assigned']->sec) . '</td>
            <td>' . $data['crawledsites'] . '</td>';

        if ($this->status == 'active') {
          $out .= '   <td>' . $data['sitestocrawl'] . '</td>';
        }

        $out .= '<td>
              <div class="btn-group">
                <button class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="icon-cog"></i></button>
                <ul class="dropdown-menu">
                  <li><a href="#" class="ops_delete_crawl_'.$this->status.'" data-crawlid="' . $data['_id'] . '">Crawl löschen</a></li>
                </ul>
              </div>
            </td>
            <td>'. $this->renderCrawlStatus($data['status'], $data['crawlcollection']).'</td>
          </tr>';

      }

    $out .= '</tbody></table>';

    return $out;

  }


  public function renderCrawlStatus ($status, $collection_id) {

    if (!isset($status)) {
      return '<a href="#" class="label label-red">ERROR</a>';
    }

    switch ($status) {
      case 'new':
        $cell = '<a href="analyse/' . $collection_id . '" class="label label-blue">neu</a>';
        break;
      case 'waiting':
        $cell = '<a href="analyse/' . $collection_id . '" class="label label-blue">wartet</a>';
        break;
      case 'working':
        $cell = '<a href="analyse/' . $collection_id . '" class="label label-red">crawling</a>';
        break;
      case 'analyse':
        $cell = '<a href="analyse/' . $collection_id . '" class="label label-gray">Auswertung</a>';
        break;
      case 'done':
        $cell = '<a href="analyse/' . $collection_id . '" class="label label-green">anzeigen</a>';
        break;
      case 'error':
        $cell = '<a href="analyse/' . $collection_id . '" class="label label-red">ERROR</a>';
        break;
    }

    return $cell;

  }


}

?>