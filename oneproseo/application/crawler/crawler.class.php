<?php

class crawler {

  public function session ()
  {

    session_start();

  }


  public function setEnv ($env_data)
  {

    $this->env_data = $env_data;

  }


  public function dbConnect ()
  {

    $mdb = $this->mongoconnection();

    return $mdb;

  }


  public function mongoconnection ()
  {

    try {

      $mongoCL = 'mongodb://' . $this->env_data['mongo_dbuser'] . ':' . $this->env_data['mongo_dbpass'] . '@' . $this->env_data['mongo_dbhost'];

      $mongo = new MongoClient($mongoCL);
      $mdb = $mongo->selectDB($this->env_data['mongo_dbname']);
      return $mdb;

    } catch (MongoCursorException $e) {

      echo 'error message: ' . $e->getMessage() . '<br />';
      echo 'error code: ' . $e->getCode() . '<br />';

    }

  }


}

?>