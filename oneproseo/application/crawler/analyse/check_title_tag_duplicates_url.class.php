<?php

require_once 'analyse.class.php';

class check_title_tag_duplicates_url extends analyse {

  private $title_duplicates = array();

  protected function header() {

    echo '<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header"><span class="title">Title Tag Duplikate: '. $this->payload .'</span></div>';

  }


  protected function content() {

    echo '
                <div class="box-content">
                  <table class="table table-normal data-table">
                    <thead>
                      <tr>
                        <td>URL</td><td>Title Tag</td>
                      </tr>
                    </thead>';

                  foreach ($this->title_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../../site/'.$data[2].'" target="_blank">'. $data[0] .'</a></td><td>'.implode('<br />', $data[1]).'</td>';
                    echo '</tr>';
                  }
    echo '
                </div>
              </div>
            </div>
         </div>';

  }


  protected function prepareData() {

    $this->payload =  urldecode($this->payload);

    $cursor = $this->crawl_collection->find(
                                        array('titleTag' => $this->payload),
                                       // array('titleTag' => new MongoRegex('/'.$this->payload.'/i')),
                                        array('url' => true, 'titleTag' => true)
                                     );
    $cursor->timeout(-1);


    foreach ($cursor as $val) {

      $this->title_duplicates[] = array($val['url'], $val['titleTag'] ,$val['_id']);

    }

  }


}

?>