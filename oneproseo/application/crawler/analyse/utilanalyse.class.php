<?php


class utilanalyse {

  public static function umlaut_hex () {
    return array('%E4', '%F6', '%FC', '%C4', '%D6', '%DC', '%DF', '%C3%B6', '%c3%96', '%c3%bc', '%c3%9c', '%c3%a4', '%c3%84');
  }


  public static function splitter ($string) {
    return implode(PHP_EOL, str_split($string, 80));
  }


  public static function outputUseragent ($string) {

    switch ($string) {
        case 'google':
            return 'Google Bot';
            break;
        case 'bing':
            return 'Bing Bot';
            break;
        case 'yandex':
            return 'Yandex Bot';
            break;
        case 'chrome':
            return 'Desktop Chrome';
            break;
        case 'firefox':
            return 'Desktop Firefox';
            break;
        case 'mobilesafari':
            return 'Mobile Safari';
            break;
    }

  }


  public static function outputFrequency ($string) {

    switch ($string) {
        case 'once':
            return 'einmalig';
            break;
        case 'weekly':
            return 'wöchentlich';
            break;
        case 'monthly':
            return 'monatlich';
            break;
    }

  }

/**
  * checkt ein array auf duplikate 
  *
  * @param array $array
  * @param string $tag
  * @return array
  *
  */

  public static function checkForDuplicates ($array) {

    $result = array();

    foreach ($array as $key => $value) {

      if (empty($value[0])) {
        $value[0] = 'empty';
      }

      if (array_key_exists($value[0], $result)) {
        array_push($result[$value[0]], $value[1]);
      } else {
        $result[$value[0]] = array($value[1]);
      }

    }

    foreach ($result as $k => $v) {

      if (count($v) < 2) {
        unset($result[$k]);
      }

    }

    return $result;

  }



/**
  * erzeugt eine absolute URL
  *
  * anhand von baseURL und gefundenem link
  *
  * @param string $requestFile
  * @param string $baseHost
  * @return string $requestPath
  *
  * about.html          http://WebReference.com/html/about.html
  * tutorial1/          http://WebReference.com/html/tutorial1/
  * tutorial1/2.html    http://WebReference.com/html/tutorial1/2.html
  * /                   http://WebReference.com/
  * //www.internet.com/ http://www.internet.com/
  * /experts/           http://WebReference.com/experts/
  * ../                 http://WebReference.com/
  * ../experts/         http://WebReference.com/experts/
  * ../../../           http://WebReference.com/
  * ./                  http://WebReference.com/html/
  * ./about.html        http://WebReference.com/html/about.html
  *
  */

  public static function makeRequestUrl ($requestFile, $baseHost) {

    $phpUrlHost = parse_url($baseHost, PHP_URL_HOST);

    // Leerzeichen in Url
    if (stripos($requestFile, ' ') !== FALSE) {
      $requestFile = str_replace(' ', '%20', $requestFile);
    }

    // gefundene url beginnt mit http(s)
    if (stripos($requestFile, 'http://') === 0 or stripos($requestFile, 'https://') === 0) {

      $requestPath = $requestFile;

    // gefundene url beginnt mit /
    } else if (stripos($requestFile, '/') === 0) {

      // gefundene url beginnt mit //
      if (stripos($requestFile, '//') === 0) {
        $requestPath = parse_url($baseHost, PHP_URL_SCHEME) . ':' . $requestFile;
      } else {
        $requestPath = 'http://' . $phpUrlHost . $requestFile;
      }

    // gefundene url beginnt mit ../
    } else if (stripos($requestFile, '../') === 0) {

        $requestPath = self::removeRelative($requestFile, $baseHost);

    // gefundene url beginnt mit ./
    } else if (stripos($requestFile, '.') === 0) {

      if (stripos($requestFile, './') === 0) {
        $requestFile = substr($requestFile, 2);

        if (stripos($requestFile, '../') === 0) {
          $requestPath = self::removeRelative($requestFile, $baseHost);
        } else {
          $requestPath = substr ($baseHost, 0, strrpos($baseHost, '/')) . '/' . $requestFile;
        }

      } else {

        $requestPath = $baseHost;

      }

    // gefundene url beginnt mit ?
    } else if (stripos($requestFile, '?') === 0) {

      // ? in der base url ?
      if (stripos($baseHost, '?') !== FALSE) {
        $requestPath = substr ($baseHost, 0, strrpos($baseHost, '?')) . $requestFile;
      } else {
        $requestPath = $baseHost . $requestFile;
      }

    } else {

      // base url mit slash am ende
      if (strrpos($baseHost,'/') === strlen($baseHost) - 1) {

        $requestPath = $baseHost . $requestFile;

      // base url ohne http und // 
      } else if (stripos($baseHost,'/') === false) {

         $requestPath = 'http://' . $baseHost . '/' . $requestFile;

      // http url mit file am ende
      } else if (substr_count($baseHost, '/') >= 3 and strrpos($baseHost,'.') > strrpos($baseHost,'/')) {

         $requestPath = substr ($baseHost, 0, strrpos ($baseHost, '/')) .'/'. $requestFile;

      // http url ohne slash am ende und requestFile ohne slashes
      } else if (substr_count($baseHost, '/') >= 3 and strrpos($baseHost,'/') !== strlen($baseHost) - 1 and stripos($requestFile, '/') === false) {

        $requestPath = substr ($baseHost, 0, strrpos ($baseHost, '/')) .'/'. $requestFile;

      // www.kreuzfahrten-sonderpreise.de/root/index.php?c=home/home ++ index.php?c=home/home
      } else if (substr_count($baseHost, '/') >= 3 and strrpos($baseHost,'/') !== strlen($baseHost) - 1 and strrpos($baseHost,'/') > strrpos($baseHost,'?')) {

        $baseHost = substr($baseHost, 0, stripos($baseHost, '?'));
        $requestPath = substr ($baseHost, 0, strrpos ($baseHost, '/')) .'/'. $requestFile;

      // base url ohne slash am ende / 
      } else if (!empty ($requestFile)) {

        $requestPath = $baseHost . '/' . $requestFile;

      } else {

        $requestPath = $baseHost;

      }

    }

    return $requestPath;

  }


/**
  * entfernt relative paramenter einer URL
  *
  * ../ 
  *
  * @param string $requestFile
  * @param string $baseHost
  * @param string $relative
  * @return string $requestPath
  *
  */

  public static function removeRelative ($requestFile, $baseUrl, $relative = '../') {

    // base url mit fragezeichen
    if (stripos($baseUrl,'?') > 0) {
      $baseUrl = (substr($baseUrl, 0, strrpos($baseUrl,'?')));
    }

    while (stripos($requestFile, $relative) === 0) {

      // baseURL mit FILENAME oder baseURL mit abschließendem /
      if (strrpos($baseUrl,'.') > strrpos($baseUrl,'/') || strrpos($baseUrl, '/') !== FALSE) {
        $baseUrl = substr ($baseUrl, 0, strrpos($baseUrl, '/'));
      }

      // entfernt alles hinter dem letzten slash
      $baseUrl = substr ($baseUrl, 0, strrpos($baseUrl, '/') + 1);

      // entfernt die relativen strings ../
      $requestFile = substr ($requestFile, strlen($relative));

    }

    // siemens bug ../en/../en/contact.html
    if (strrpos($requestFile,'../') > 0) {
      $requestFile = substr($requestFile, strrpos($requestFile,'../') + 3);
    }


    return $baseUrl . $requestFile;

  }


/**
  * compare first url with crawled urls
  *
  * for not following redis to other domains
  *
  * @param string $url
  * @return string cleanedUrl
  *
  */

  public static function getHostRemoveWWW ($url, $debugger = '') {

    $cleanedUrl = parse_url($url);

    if (isset($cleanedUrl['host'])) {

      $cleanedUrl = $cleanedUrl['host'];

      if (stripos($cleanedUrl, '?') !== FALSE) {
        $cleanedUrl = substr($cleanedUrl, 0, stripos($cleanedUrl, '?'));
      }

      if (stripos($cleanedUrl, 'www.') === 0) {
        $cleanedUrl = substr($cleanedUrl, 4);
      }

      return $cleanedUrl;

    } else {

      // utilanalyse::logToFile('getHostRemoveWWW ->' . $url);

    }

  }


/**
  * findet files die für die analyse nicht notwendig sind
  *
  * anhand von dateiendungen
  *
  * @param string $foundUrl
  * @return boolean
  *
  */

  public static function checkForShit ($foundUrl) {

    $firstShit = array (
        'ftp:', 'javascript:', 'skype:', 'mailto:', 'ymsgr:', 'callto:', 'file:', 'tel:', 'feed:', 'bitcoin:'
    );

    $middleShit = array (
        '#'
    );

    $lastShit = array (
        '.jpg', '.gif', '.png', '.svg', '.pdf', '.zip', '.rar', '.xml', '.css', '.doc', '.tar', '.wmv', '.wav', '.swf', '.flv', 
        '.mp4', '.mp3', '.rss', '.apk', '.rar', '.xls', '.exe', '.dmg', '.ppt', '.txt', '.crt', '.crl', '.gz', '.m4v', '.mov'
    );
    
    if (strlen($foundUrl) > 4) {

      foreach ($firstShit as $check) {
        if (stripos($foundUrl, $check) === 0) {
          return false;
        }
      }

      foreach ($lastShit as $check) {
        if (strrpos($foundUrl, $check, -3) === strlen($foundUrl) -4) {
          return false;
        }
      }

    }

    foreach ($middleShit as $check) {
      if (stripos($foundUrl, $check) !== FALSE) {
        return false;
      }
    }

  }



/**
  * errorlogger function
  *
  *
  * @param $content
  */

  public static function logToFile ($content, $filename = 'debugger') {

    $fh = fopen(TEMP_PATH . $filename . '.txt', 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


}

?>