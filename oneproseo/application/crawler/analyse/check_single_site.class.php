
<?php

require_once dirname(__FILE__) . '/../crawler.class.php';
require_once 'utilanalyse.class.php';

/* MONGO DB SCHEMA: 
{
  "_id": { "$oid" : "533befc1df34615a13000004" },
  "url": "http://www.theskunk.cc",
  "response": {
    "http://www.theskunk.cc": "200"
  },
  "titleTag": [
    "Skunk The Fuck Up - SKUNK"
  ],
  "metaDesc": [
    "I can't be killed but I'm not too proud!"
  ],
  "metaKey": "",
  "metaRobot": [
    ""
  ],
  "h1content": [
    "the skunk ∞"
  ],
  "h2content": [
    "be patient .. Mat(t)hias"
  ],
  "allLinks": [
    {
      "href": "index.html",
      "basetag": "",
      "linktext": "START"
    },
    {
      "href": "watchlist.html",
      "basetag": "",
      "linktext": "WATCHLIST"
    },
    {
      "href": "show.html",
      "basetag": "",
      "linktext": "VIEWER"
    }
  ],
  "canonical": [

  ],
  "nofollow": null,
  "responseTime": 0.30999999999999999778,
  "bodyTagContent": " \n    the skunk  ∞\n        START\n          WATCHLIST\n          VIEWER\n       \n       \n          \n         \n           Start typing or try the examples:\n           Altria Group Inc.  (MO)  NYSE  Apollo Commercial Real Estate Finance, Inc.  (ARI)  NYSE\n           BALDA  (BAF.F)  Frankfurt\n           Barrick Gold Corporation  (ABX)  NYSE\n           Fifth Street Finance Corp.  (FSC)  NASDAQ\n           Gladstone Capital Corporation  (GLAD)  NASDAQ\n           IAMGOLD Corp.  (IAG)  NYSE\n           LIGHTSTREAM RESOURCES LTD  (LTS.TO)  Toronto\n           MPH Mittel. Pharma  (93MV.DE)  XETRA\n           Northern Tier Energy  (NTI)  NYSE  Pitney Bowes Inc.  (PBI)  NYSE  SeaDrill Limited  (SDRL)  NYSE  Vale S.A.  (VALE)  NYSE\n           VOC Energy Trust  (VOC)  NYSE\n        \n      \n         be patient .. Mat(t)hias\n       \n    \n\n    ",
  "metaRefresh": "",
  "fileSize": false,
  "missingAltImg": [

  ],
  "framesets": false,
  "inlineJS": 1,
  "styleTags": 0,
  "styleAttr": 0,
  "foundOn": ""
}
*/


class check_single_site extends crawler {

  public function __construct ($env_data) {

    parent::setEnv($env_data);

    $this->mdb = $this->dbConnect();

    if (isset($_POST['collid']) && isset($_POST['siteid'])) {

      $collection_id      = strip_tags($_POST['collid']);
      $this->site_id      = strip_tags($_POST['siteid']);
      $collection_crawl   = $this->mdb->activecrawls;
      $this->cursor_crawl = $collection_crawl->findOne(array('crawlcollection' => $collection_id));

      $this->prepareData();


    } else {

      exit ("NO VALID COLLECTION SET!");

    }

  }


  public function prepareData() {

    $cursor_crawl = $this->cursor_crawl;
    $crawl_collection = $this->mdb->$cursor_crawl['crawlcollection'];
    $cursor = $crawl_collection->findOne(array('_id' => new MongoId($this->site_id)));

    $text = preg_replace("/[\r\n]+/", "\n", $cursor['bodyTagContent']);

    echo '<div class="row-fluid">
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> URL</span>
          </div>
          <div class="box-content padded">
            <a href="'.$cursor['url'].'" target="_blank">'.$cursor['url'].'</a>
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> HTTP Response</span>
          </div>
          <div class="box-content padded">';

foreach($cursor['response'] as $k => $v) {

  echo 'HTTP ' . $v[1];
  echo ' -> ';
  echo $v[0];
  echo '<br />';

}
         echo'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Link zuerst gefunden</span>
          </div>
          <div class="box-content padded">
            <a href="'.$cursor['foundOn'].'" target="_blank">'.$cursor['foundOn'].'</a>
          </div>
        </div>
      </div>

      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Response Time</span>
          </div>
          <div class="box-content padded">
         '.$cursor['responseTime'].'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Robots</span>
          </div>
          <div class="box-content padded">
         '.$this->makeList($cursor['metaRobot']).'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Title Tag</span>
          </div>
          <div class="box-content padded">
         '.$this->makeList($cursor['titleTag']).'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Meta Desc</span>
          </div>
          <div class="box-content padded">
         '.$this->makeList($cursor['metaDesc']).'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> H1 Tags</span>
          </div>
          <div class="box-content padded">
         '.$this->makeList($cursor['h1content']).'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> H2 Tags</span>
          </div>
          <div class="box-content padded">
         '.$this->makeList($cursor['h2content']).'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Links</span>
          </div>
          <div class="box-content padded">
         ';

foreach($cursor['allLinks'] as $k => $v) {

  echo $v['href'];
  echo ' - ';
  echo $v['linktext'];
  echo ' - ';
  echo $v['nofollow'];

  echo '<br />';

}


         echo'
          </div>
        </div>
      </div>
      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Text</span>
          </div>
          <div class="box-content padded">
         '. preg_replace( "/\r|\n/", "", $text ) .'
          </div>
        </div>
      </div>

      <div class="span4">
        <div class="box">
          <div class="box-header">
            <span class="title"><i class="icon-th-list"></i> Seite</span>
          </div>
          <div class="box-content">
           <iframe src="'.$cursor['url'].'" width="100%" height="400" frameBorder="0" name=""></iframe>
          </div>
        </div>
      </div>

    </div>';


  }


  public function makeList ($array) {

    $ret = '<ul class="content">';

    foreach ($array as $k=>$v) {

      $ret .= '<li>' . $k . ' / ' . $v . '</li>';

    }

    $ret .= '</ul>';

    return $ret;

  }


}


?>