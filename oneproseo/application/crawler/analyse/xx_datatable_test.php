<?php

require_once 'analyse.class.php';

class canonical extends analyse {

  private $canonical_empty = array();
  private $canonical_multi = array();
  private $canonical_relative = array();

  protected function header() {}


// http://legacy.datatables.net/release-datatables/examples/server_side/select_rows.html

  protected function content() {

    $output['sEcho'] = intval($_POST['sEcho']);
    $output['iTotalRecords'] = $this->count_results;
    $output['iTotalDisplayRecords'] = $this->count_results;

    if (!empty($this->slow_response)) {
      $output['aaData'] = $this->slow_response;
    } else {
      $output['aaData'] = array(array('-', '-'));
    }


    echo json_encode($output);

  }

  protected function prepareData() {

    if (isset($_POST['sSearch']) && $_POST['sSearch'] != "" ) {

      $cursor = $this->crawl_collection->find(
                                       array('$or' => array (
                                          array('responseTime' => $_POST['sSearch']), array('url' => new MongoRegex('/'.$_POST['sSearch'].'/i'))
                                       )),
                                          array('url' => true, 'responseTime' => true)
                                       );

    } else {

      $cursor = $this->crawl_collection->find(
                                          array('responseTime' => array('$gt' => 3)),
                                          array('url' => true, 'responseTime' => true)
                                       );

    }


    $cursor->timeout(-1);
    $this->count_results = $cursor->count();

    $cursor->skip($_POST['iDisplayStart']);
    $cursor->limit($_POST['iDisplayLength']);

    try {
      $cursor->sort(array('responseTime' => -1));
      $ursort = false;
    } catch (MongoCursorException $e) {
      $ursort = true;
    }

    foreach ($cursor as $val) {
      $url = '<a href="'.$val['_id'].'">' . utilanalyse::splitter($val['url']) . '</a>';
      $this->slow_response[] = array($url, round($val['responseTime'], 2));
    }

    if ($ursort == true) {
      usort($this->slow_response, array($this, 'sortByOrder'));
    }

  }

  protected function sortByOrder($a, $b) {
    return $b[1] - $a[1] ;
  }




}

new canonical();

?>