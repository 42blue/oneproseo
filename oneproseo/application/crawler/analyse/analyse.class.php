<?php

require_once dirname(__FILE__) . '/../crawler.class.php';
require_once 'utilanalyse.class.php';

abstract class analyse extends crawler {

  public function __construct ($env_data) {

    parent::setEnv($env_data);

    $this->mdb = $this->dbConnect();

    if (isset($_POST['collid'])) {

      $this->collection_id = strip_tags($_POST['collid']);
      $collection_crawl    = $this->mdb->activecrawls;
      $cursor_crawl  = $collection_crawl->findOne(array('crawlcollection' => $this->collection_id));
      $this->crawl_collection = $this->mdb->$cursor_crawl['crawlcollection'];

      if (isset($_POST['payload'])) {
        $this->payload = $_POST['payload'];
      }

      $this->prepareData();

      $this->header();

      $this->content();

    } else {

      exit ("NO VALID COLLECTION SET!");

    }

  }

  abstract protected function prepareData();

  abstract protected function header();

  abstract protected function content();

}


?>