<?php

require_once 'analyse.class.php';

class check_h2 extends analyse {

  private $h2_duplicates = array();
  private $h2_duplicates_tmp = array();
  private $h2_missing = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Überschrift H2 - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Jede Seite sollte mindestens eine, besser mehrere H2 Überschriften haben (je nach Textlänge)</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Überschrift H2: fehlende / leere </span> ';

          if (!empty($this->h2_missing)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="h2_missing"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="h2_missing"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Überschrift H2: doppelte</span> ';

          if (!empty($this->h2_duplicates)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="h2_duplicates"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="h2_duplicates"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->h2_missing as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.utilanalyse::splitter($data[0]).'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>H2</td>
                  </tr>
                </thead>';

                  foreach ($this->h2_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../taskcheck/h2_duplicates_url/'.urlencode($k).'" target="_blank">'.$k.'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

          </div>
        </div>

      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'h2content' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
        if (stripos($value[1], '301') === 0) {
          continue 2;
        }
        if (stripos($value[1], '302') === 0) {
          continue 2;
        }
        if (stripos($value[1], '303') === 0) {
          continue 2;
        }
        if (stripos($value[1], '307') === 0) {
          continue 2;
        }
      }

      foreach ($val['h2content'] as $value) {
        $value = trim($value);
        if (isset($this->h2_duplicates_tmp[$value])) {
          $this->h2_duplicates[$value] = '';
        } else {
          $this->h2_duplicates_tmp[$value] = '';
        }
      }
      if (empty($val['h2content'])) {
        $this->h2_missing[] = array($val['url'], $val['_id']);
      }

    }

  }


}

?>