<?php

require_once 'analyse.class.php';

class check_details_url extends analyse {

  protected function header() {

/*

array(7) {
  ["draw"]=>
  string(1) "1"
  ["columns"]=>
  array(2) {
    [0]=>
    array(5) {
      ["data"]=>
      string(3) "url"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
    [1]=>
    array(5) {
      ["data"]=>
      string(10) "httpstatus"
      ["name"]=>
      string(0) ""
      ["searchable"]=>
      string(4) "true"
      ["orderable"]=>
      string(4) "true"
      ["search"]=>
      array(2) {
        ["value"]=>
        string(0) ""
        ["regex"]=>
        string(5) "false"
      }
    }
  }
  ["order"]=>
  array(1) {
    [0]=>
    array(2) {
      ["column"]=>
      string(1) "1"
      ["dir"]=>
      string(4) "desc"
    }
  }
  ["start"]=>
  string(1) "0"
  ["length"]=>
  string(3) "100"
  ["search"]=>
  array(2) {
    ["value"]=>
    string(0) ""
    ["regex"]=>
    string(5) "false"
  }
  ["collid"]=>
  string(34) "crawl-advertising-de-1423579267-63"
}

*/

  }


  protected function content() {

    $output = array(
        "draw" => intval($_POST['draw']),
        "recordsTotal" => $this->crawl_collection->count(),
        "recordsFiltered" => $this->cursor->count(),
        "data" => array(),
    );

    if (empty($this->data)) {
      $this->data = array ('url'=>'nüscht');
    }

    $output['data'] = $this->data;

    echo json_encode( $output );

  }

/// columns[2][search][value]

  protected function prepareData() {

    $input = &$_POST;

    // set base field
    $base_query     = array();
    $return_fields  = array();

    // return fields
    foreach ($input['columns'] as $key => $dataset) {
      if (!empty($dataset['data']) && $dataset['data'] != 'null') {
        $return_fields[$dataset['data']] = true;
        if ($dataset['data'] != 'allLinks') {
          // create index for sorting, not if exists
          $this->crawl_collection->createIndex(array($dataset['data'] => 1));
        }
      }
    }

    // filter
    $filter = array();
    foreach ($input['columns'] as $key => $dataset) {
      if (!empty($dataset['search']['value'])) {
        $sRegex = $dataset['search']['value'];
        if ($dataset['data'] == 'bodyTagWords') {
          if (is_numeric($sRegex)) {
            $sRegex = intval($sRegex);
            $filter[] = array($dataset['data'] => $sRegex );
          }
        } elseif ($dataset['data'] == 'responseTime') {
          if (is_numeric($sRegex)) {
            $sRegex = floatval($sRegex);
            $filter[] = array($dataset['data'] => $sRegex );
          }
        } elseif ($dataset['data'] == 'allLinks') {
// dont search
        } elseif ($dataset['data'] == 'response') {
// dont search
          $filter[] = array($dataset['data'] => '' );
        } else {
          $filter[] = array($dataset['data'] => new MongoRegex('/'.$sRegex.'/i' ));
        }
      }
    }
    $base_filter = array('$and' => $filter);

    // search
    if (!empty($input['search']['value'])) {
      $sRegex = $input['search']['value'];
      $search = array();
      foreach ($input['columns'] as $key => $dataset) {
        if ($dataset['data'] == 'bodyTagWords') {
          if (is_numeric($sRegex)) {
            $sRegex = intval($sRegex);
            $search[] = array($dataset['data'] => $sRegex );
          }
        } elseif ($dataset['data'] == 'responseTime') {
          if (is_numeric($sRegex)) {
            $sRegex = floatval($sRegex);
            $search[] = array($dataset['data'] => $sRegex );
          }
        } elseif ($dataset['data'] == 'allLinks') {
// dont search
        } elseif ($dataset['data'] == 'response') {
// dont search
        } else {
          $search[] = array($dataset['data'] => new MongoRegex('/'.$sRegex.'/i' ));
        }
      }
      $base_search = array('$or' => $search);
    }

    // COMBINE SEARCH AND FILTER
    if (count($search) > 0 && count($filter) > 0) {
      $base_query = array_merge ($base_filter, $base_search);
    } elseif (count($search) > 0 && count($filter) == 0) {
      $base_query = $base_search;
    } elseif (count($search) == 0 && count($filter) > 0) {
      $base_query = $base_filter;
    }


    // query
    $this->cursor = $this->crawl_collection->find($base_query, $return_fields);
    $this->cursor->timeout(-1);

    // paging
    if (isset($input['start']) && $input['length'] != '-1') {
      $this->cursor->limit($input['length'])->skip($input['start']);
    }

    // ordering
    if (isset($input['order'])) {
      if ($input['order'][0]['dir'] == 'asc' ) {
        $sort_order = 1;
      } else {
        $sort_order = -1;
      }
      $input['columns'][$input['order'][0]['column']]['data'];
      $sort = array($input['columns'][$input['order'][0]['column']]['data'] => $sort_order);
      $this->cursor->sort($sort);
    }


    foreach ($this->cursor as $val) {

      $response = '';
      foreach ($val['response'] as $k => $resp) {
        $response .= '<a href="'. $resp[0] . '" target="_blank">' . $resp[1] . '</a><br />';
      }

      $tt = '';
      foreach ($val['titleTag'] as $k => $resp) {
        $tt .= $resp . '<br />';
      }

      $md = '';
      foreach ($val['metaDesc'] as $k => $resp) {
        $md .= $resp . '<br />';
      }

      $h1 = '';
      foreach ($val['h1content'] as $k => $resp) {
        $h1 .= $resp . '<br />';
      }

      $h2 = '';
      foreach ($val['h2content'] as $k => $resp) {
        $h2 .= $resp . '<br />';
      }

      $canonical = '';
      foreach ($val['canonical'] as $k => $resp) {
        $canonical .= $resp . '<br />';
      }

      $this->externalUrlTemp = array();
      $this->internalUrlTemp = array();

      foreach ($val['allLinks'] as $set) {
        $href     = $set['href'];
        if (utilanalyse::getHostRemoveWWW($href) != utilanalyse::getHostRemoveWWW($val['url'])) {
          $this->externalUrlTemp[] = $set['href'];
        } else {
          $this->internalUrlTemp[] = $set['href'];
        }
      }
      $external_links = count($this->externalUrlTemp);
      $internal_links = count($this->internalUrlTemp);

      $this->data[] = array(
        
          'url'          => '<a href="../site/'.$val['_id'].'" target="_blank">'.utilanalyse::splitter($val['url']).'</a>', 
          'response'     => $response, 
          'responseTime' => round($val['responseTime'], 2),
          'titleTag'     => $tt,
          'metaDesc'     => $md,
          'h1content'    => $h1,
          'h2content'    => $h2,
          'bodyTagWords' => $val['bodyTagWords'],
          'canonical'    => $canonical,
          'allLinks'     => $internal_links . ' / ' . $external_links,
        );

    }

  }


}
/*

    // OUTPUT ARRAY
    $output = array(
        'url'            => $url,                //string
        'response'       => $httpResponseCode,   //array
        'responseTime'   => $this->responseTime, //string
        'responseHead'   => $this->responseHead, //string
        'titleTag'       => $titleTag,           //array
        'metaDesc'       => $metaDesc,           //array
        'metaKey'        => $metaKey,            //string
        'metaRobot'      => $metaRobot,          //array 
        'xRobot'         => $xRobot,             //array 
        'canonical'      => $canonical,          //array
        'relPrev'        => $relprev,            //array
        'relNext'        => $relnext,            //array
        'h1content'      => $h1content,          //array
        'h2content'      => $h2content,          //array
        'bodyTagContent' => $bodyTagContent,     //string
        'bodyTagWords'   => $words_body_tag,     //number
        'allLinks'       => $allLinks,           //array
        'metaRefresh'    => $this->metaRefresh,  //string
        'fileSize'       => $fileSize,           //number
        'missingAltImg'  => $missingAltImg,      //array
        'framesets'      => $framsesets,         //bool
        'inlineJS'       => $inlineJS,           //number
        'styleTags'      => $styleTags,          //number
        'styleAttr'      => $styleAttr,          //number
        'foundOn'        => $foundOn             //string
        );



         */
?>