
<?php

require_once dirname(__FILE__) . '/../crawler.class.php';
require_once 'utilanalyse.class.php';

class overview extends crawler {

  private $http_response_undefined = 0;
  private $cummulated_response_time = 0;
  private $robots_no_index = 0;
  private $robots_no_follow = 0;
  private $robots_no_snippet = 0;
  private $robots_no_archive = 0;
  private $long_url = 0;
  private $url_invalid = 0;
  private $url_umlaut = 0;
  private $title_tag_missing = 0;
  private $title_short_long = 0;
  private $title_duplicates = 0;
  private $title_duplicates_tmp = array();
  private $meta_desc_missing = 0;
  private $meta_desc_short_long = 0;
  private $meta_desc_duplicates = 0;
  private $meta_desc_duplicates_tmp = array();
  private $h1_duplicates = 0;
  private $h1_duplicates_tmp = array();
  private $h1_multi = 0;
  private $h1_missing = 0;
  private $h2_duplicates = 0;
  private $h2_duplicates_tmp = array();
  private $h2_missing = 0;
  private $http_response_301 = 0;
  private $http_response_302 = 0;
  private $http_response_4xx = 0;
  private $http_response_5xx = 0;


  public function __construct ($env_data) {

    parent::setEnv($env_data);

    $this->mdb = $this->dbConnect();

    if (isset($_GET['collid'])) {

      $this->collection_id = strip_tags($_GET['collid']);
      $collection_crawl    = $this->mdb->activecrawls;
      $this->cursor_crawl  = $collection_crawl->findOne(array('crawlcollection' => $this->collection_id));

      $this->prepareData();

      $this->renderOutputHeader();
      $this->renderOutputAggregation();
      $this->renderOutputReports();


    } else {

      exit ("NO VALID COLLECTION SET!");

    }

  }


  public function prepareData() {

    $cursor_crawl = $this->cursor_crawl;
    $crawl_collection = $this->mdb->$cursor_crawl['crawlcollection'];

    $cursor = $crawl_collection->find(  array('url' => array('$exists' => true)), 
                                        array('url' => true, 'responseTime' => true, 'titleTag' => true, 'metaDesc' => true, 'metaRobot' => true, 
                                              'response' => true, 'h1content' => true, 'h2content' => true)
                                     );
    $cursor->timeout(-1);
    $cursor->limit(100000);


    foreach ($cursor as $val) {

      $do_not_count = false;

      // IGNORE UNDEFINED SERVER RESPONSES
      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          $this->http_response_undefined++;
          continue 2;
        }
        if (stripos($value[1], '301') === 0) {
          $do_not_count = true;
        }
        if (stripos($value[1], '302') === 0) {
          $do_not_count = true;
        }
        if (stripos($value[1], '303') === 0) {
          $do_not_count = true;
        }
        if (stripos($value[1], '307') === 0) {
          $do_not_count = true;
        }
      }

      // RESPONSE TIME OVER LIMIT OF 15 SEC OR 0
      if ($val['responseTime'] > 20 or $val['responseTime'] == 0) {
         $this->http_response_undefined++;
         continue;
       }

      $this->site = $val['url'];

      // URL RESPONSE TIME
      $this->cummulated_response_time += $val['responseTime'];

      // ROBOTS
      $this->dashboardRobots($val['metaRobot']);


      // URL LENGTH
      if (strlen($val['url']) > 115) {
        $this->long_url++;
      }

      // VALIDATE URL
      if (filter_var($val['url'], FILTER_VALIDATE_URL) === FALSE) {
        $this->url_invalid++;
      }

      // UMLAUTE URL
      foreach (utilanalyse::umlaut_hex() as $enc) {
        if (stripos($val['url'], $enc) !== FALSE) {
          $this->url_umlaut++;
        }
      }


      // TITLE TAG
      foreach ($val['titleTag'] as $value) {
        if ($do_not_count == true) {
          continue;
        }
        if (isset($this->title_duplicates_tmp[$value])) {
          $this->title_duplicates++;
        } else {
          $this->title_duplicates_tmp[$value] = '';
        }
        if (strlen($value) < 40 or strlen($value) > 57) {
          $this->title_short_long++;
        }
      }

      if (empty($val['titleTag'])) {
        $this->title_tag_missing++;
      }

      // META DESCRIPTION
      foreach ($val['metaDesc'] as $value) { 
        if ($do_not_count == true) {
          continue;
        }
        // maybe there are html tags in MD
        $stripped_md = strip_tags($value);
        if (isset($this->meta_desc_duplicates_tmp[$stripped_md])) {
          $this->meta_desc_duplicates++;
        } else {
          $this->meta_desc_duplicates_tmp[$stripped_md] = '';
        }
        $this->metaDescription = $value;
        if (strlen($value) < 80 or strlen($value) > 165) {
          $this->meta_desc_short_long++;
        }
      }

      if (empty($val['metaDesc'])) {
        $this->meta_desc_missing++;
      }

      // HEADINGS
      foreach ($val['h1content'] as $value) {
        if ($do_not_count == true) {
          continue;
        }
        if (isset($this->h1_duplicates_tmp[$value])) {
          $this->h1_duplicates++;
        } else {
          $this->h1_duplicates_tmp[$value] = '';
        }
      }
      if (empty($val['h1content'])) {
        $this->h1_missing++;
      }
      if (count($val['h1content']) > 1) {
        $this->h1_multi++;
      }

      foreach ($val['h2content'] as $value) {
        if ($do_not_count == true) {
          continue;
        }
        if (isset($this->h2_duplicates_tmp[$value])) {
          $this->h2_duplicates++;
        } else {
          $this->h2_duplicates_tmp[$value] = '';
        }
      }
      if (empty($val['h2content'])) {
        $this->h2_missing++;
      }


      // HTTP RESONSE CODES
      foreach ($val['response'] as $k => $response) {
        if (stripos($response[1], '301') === 0) {
          $this->http_response_301++;
        } else if (stripos($response[1], '302') === 0) {
          $this->http_response_302++;
        } else if (stripos($response[1], '4') === 0) {
          $this->http_response_4xx++;
        } else if (stripos($response[1], '5') === 0) {
          $this->http_response_5xx++;
        } 
      }

    }

  }


  public function renderOutputReports() {

    echo '
      <div class="box">
        <div class="box-header">
          <div class="title">Einzelanalysen - Inhaltlich</div>
        </div>
      </div>

      <div class="row action-nav-row">
        <div class="action-nav-normal">

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/crawl_error" target="_blank">
              <i class="icon-medkit"></i>
              <span>Crawl Fehler</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/title_tag" target="_blank">
              <i class="icon-bookmark"></i>
              <span>Title Tag</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/meta_description" target="_blank">
              <i class="icon-bookmark-empty"></i>
              <span>Meta Description</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/h1" target="_blank">
              <i class="icon-text-width"></i>
              <span>Überschrift: H1</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/h2" target="_blank">
              <i class="icon-text-width"></i>
              <span>Überschrift: H2</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/short_text" target="_blank">
              <i class="icon-cut"></i>
              <span>zu kurze Texte</span>
            </a>
          </div>


        </div>
      </div>

      <div class="row action-nav-row">
        <div class="action-nav-normal">

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/directory_levels" target="_blank">
              <i class="icon-folder-open-alt"></i>
              <span>Verzeichnisebenen</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/links_directory_levels" target="_blank">
              <i class="icon-folder-open"></i>
              <span>Links / Verzeichnisebene</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/links_internal" target="_blank">
              <i class="icon-signin"></i>
              <span>Interne Links</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/links_external" target="_blank">
              <i class="icon-signout"></i>
              <span>Externe Links</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/links_error" target="_blank">
              <i class="icon-screenshot"></i>
              <span> keine / zuviele Links</span>
            </a>
          </div>

        </div>
      </div>

      <div class="box">
        <div class="box-header">
          <div class="title">Einzelanalysen - Technisch</div>
        </div>
      </div>

      <div class="row action-nav-row">
        <div class="action-nav-normal">

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/http_statis_3xx" target="_blank">
              <i class="icon-hand-left"></i>
              <span>HTTP 301 / 302</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/http_statis_45x" target="_blank">
              <i class="icon-hand-left"></i>
              <span>HTTP 4xx / 5xx</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/http_chains" target="_blank">
              <i class="icon-random"></i>
              <span>Weiterleitungsketten</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/canonical" target="_blank">
              <i class="icon-asterisk"></i>
              <span>Canonical Tags</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/robots" target="_blank">
              <i class="icon-qrcode"></i>
              <span>Robots</span>
            </a>
          </div>
          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/url_parameter" target="_blank">
              <i class="icon-thumbs-down"></i>
              <span>URL Parameter</span>
            </a>
          </div>

        </div>
      </div>

      <div class="row action-nav-row">
        <div class="action-nav-normal">

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/urls" target="_blank">
              <i class="icon-exchange"></i>
              <span>URLs</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/response_time" target="_blank">
              <i class="icon-bolt"></i>
              <span>Slow Response Time</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/source_code_large" target="_blank">
              <i class="icon-building"></i>
              <span>Quelltext über 100 kb</span>
            </a>
          </div>

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/task/img_alt_tag_empty" target="_blank">
              <i class="icon-tag"></i>
              <span>Leere ALT Tags</span>
            </a>
          </div>

        </div>
      </div>

      <div class="box">
        <div class="box-header">
          <div class="title">Komplettanalyse - URL</div>
        </div>
      </div>

      <div class="row action-nav-row">
        <div class="action-nav-normal">

          <div class="col-sm-2 action-nav-button">
            <a href="'.$this->collection_id.'/tasks/details_url" target="_blank">
              <i class="icon-list-alt"></i>
              <span>Details / URL</span>
            </a>
          </div>

        </div>
      </div>';
  }


  public function renderOutputHeader() {

    $cursor_crawl = $this->cursor_crawl;

    $crawl_duration  = $cursor_crawl['lastupdate']->sec - $cursor_crawl['assigned']->sec;
    $include         = is_array($cursor_crawl['include']) ? implode('<br />', $cursor_crawl['include']) : '-';
    $exclude         = is_array($cursor_crawl['exclude']) ? implode('<br />', $cursor_crawl['exclude']) : '-';
    $removeget       = is_array($cursor_crawl['removeget']) ? implode('<br />', $cursor_crawl['removeget']) : $cursor_crawl['removeget'];
    $removeget       = empty($removeget) ? '-' : $removeget;
    $removeget       = $cursor_crawl['removegetall'] == true ? ' alle' : $removeget;
    $crawled_sites   = number_format($cursor_crawl['crawledsites'], 0, '', '.');
    $sites_to_crawl  = number_format($cursor_crawl['sitestocrawl'], 0, '', '.');
    $number_of_sites = number_format($cursor_crawl['numbersites'], 0, '', '.');
    $response_time   = round($this->cummulated_response_time / $cursor_crawl['crawledsites'], 2);

    $cursor_crawl['crawledsites'] = $cursor_crawl['crawledsites'] == 0 ? $cursor_crawl['crawledsites'] = 1 : $cursor_crawl['crawledsites'];
    $crawl_duration               = $crawl_duration == 0 ? $crawl_duration = 1 : $crawl_duration;
    $sites_to_crawl               = $sites_to_crawl == 0 ? $sites_to_crawl = 1 : $sites_to_crawl;

    $crawled_sites_percent = $cursor_crawl['crawledsites'] * 100 / $cursor_crawl['numbersites'];

    $rate = $cursor_crawl['crawledsites'] / round($crawl_duration / 60, 2);

    echo '<div class="row"><div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <span class="title">Crawldaten</span>
            </div>
            <div class="box-content">

              <table class="table table-normal">
                <tbody>
                  <tr class="status-pending">
                    <td>Beauftragt:</td>
                    <td>' . date("d.m.Y / H:i", $cursor_crawl['assigned']->sec)  . '</td>
                  </tr>
                  <tr class="status-pending">
                    <td>Start URL:</td>
                    <td>' . $cursor_crawl['url'] . '</td>
                  </tr>
                  <tr class="status-pending">
                    <td>Crawlgeschwindigkeit:</td>
                    <td>' . $cursor_crawl['speed'] .' <small>(' .round($rate, 0) .' Seiten/min)</small> </td>
                  </tr>
                  <tr class="status-pending">
                    <td>Dauer:</td>
                    <td>' . round($crawl_duration / 60, 0)  . ' Minuten</td>
                  </tr>
                  <tr class="status-pending">
                    <td>Status:</td>
                    <td>' . $cursor_crawl['status']  . '</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box">
            <div class="box-header">
              <span class="title">Einstellungen</span>
            </div>
            <div class="box-content">
              <table class="table table-normal">
                <tbody>
                  <tr class="status-pending">
                    <td>User Agent:</td>
                    <td>' . utilanalyse::outputUseragent($cursor_crawl['useragent']) . '</td>
                  </tr>
                  <tr class="status-pending">
                    <td>Anzahl Seiten:</td>
                    <td>' . $number_of_sites. '</td>
                  </tr>
                  <tr class="status-pending">
                    <td>GET Parameter ignorieren:</td>
                    <td>' . $removeget . '</td>
                  </tr>
                  <tr class="status-pending">
                    <td>Ausgeschlossene Verzeichnisse:</td>
                    <td>' . $exclude . '</td>
                  </tr>
                  <tr class="status-pending">
                    <td>Eingeschlossene Verzeichnisse:</td>
                    <td>' . $include  . '</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="box">
        <div class="box-header">
          <div class="title">Übersicht</div>
        </div>
        <div class="box-content padded">
          <div class="row">
            <div class="col-md-6 separate-sections" style="margin-top: 5px;">

              <div class="row">
                <div class="col-md-12">
                  <div class="dashboard-stats">
                    <ul class="list-inline">
                      <li class="glyph"><i class="icon-cogs icon-2x"></i></li>
                      <li class="count">'.$crawled_sites.' von '.$number_of_sites.'</li>
                    </ul>
                    <div class="progress">
                      <div class="progress-bar progress-blue tip" data-percent="'.$crawled_sites_percent.'"></div>
                    </div>
                    <div class="stats-label">Gecrawlte Seiten</div>
                  </div>
                </div>
              </div>

              <div class="row" style="margin-top:30px;">
                <div class="col-md-6">
                  <div class="dashboard-stats small">
                    <ul class="list-inline">
                      <li class="glyph"><i class="icon-bolt"></i></li>
                      <li class="count">'.$response_time.' sec.</li>
                    </ul>
                    <div class="progress">
                      <div class="progress-bar progress-blue tip" title="80%" data-percent="80"></div>
                    </div>
                    <div class="stats-label">Durchschnittliche Server Response Time / Seite</div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="dashboard-stats small">
                    <ul class="list-inline">
                      <li class="glyph"><i class="icon-medkit"></i></li>
                      <li class="count"> '. $this->http_response_undefined .' </li>
                    </ul>
                    <div class="progress">
                      <div class="progress-bar progress-blue tip" title="6%" data-percent="6"></div>
                    </div>
                    <div class="stats-label">Seiten mit technischen Fehlern</div>
                  </div>
                </div>

              </div>
            </div>

            <div class="col-md-3">
              <div class="title">HTTP Statis:</div>
              <div class="box-content padded" style="text-align: center">
                <div class="ops-spark-pie-http"></div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="title">Robots / X-Robots Statis:</div>
              <div class="box-content padded" style="text-align: center">
                <div class="ops-spark-pie-robots"></div>
              </div>
            </div>

          </div>
        </div>
      </div>';

  }


  public function renderOutputAggregation() {

    echo '
      <div class="box">
        <div class="box-header">
          <div class="title">Zusammenfassung</div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-content">
              <table class="table table-normal">
                <thead>
                  <tr>
                    <td>Check</td>
                    <td>Ergebnis</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Title Tag: fehlende / leere</td>
                    <td> <a href="'.$this->collection_id.'/task/title_tag" target="_blank">'. $this->makeErrorAlertGood(0, $this->title_tag_missing) .'</a></td>
                  </tr>
                  <tr>
                    <td>Title Tag: doppelte</td>
                    <td> <a href="'.$this->collection_id.'/task/title_tag" target="_blank">'. $this->makeErrorAlertGood(0, $this->title_duplicates) .'</a></td>
                  </tr>
                  <tr>
                    <td>Title Tag: zu lang/kurz</td>
                    <td> <a href="'.$this->collection_id.'/task/title_tag" target="_blank">'. $this->makeErrorAlertGood(0, $this->title_short_long) .'</a></td>
                  </tr>
                  <tr>
                    <td>Meta Description: fehlende / leere</td>
                    <td><a href="'.$this->collection_id.'/task/meta_description" target="_blank">'. $this->makeErrorAlertGood(0, $this->meta_desc_missing) .'</a></td>
                  </tr>
                  <tr>
                    <td>Meta Description: doppelte</td>
                    <td><a href="'.$this->collection_id.'/task/meta_description" target="_blank">'. $this->makeErrorAlertGood(0, $this->meta_desc_duplicates) .'</a></td>
                  </tr>
                  <tr>
                    <td>Meta Description: zu lang / kurze</td>
                    <td><a href="'.$this->collection_id.'/task/meta_description" target="_blank">'. $this->makeErrorAlertGood(0, $this->meta_desc_short_long) .'</a></td>
                  </tr>
                  <tr>
                    <td>H1: fehlende / leere</td>
                     <td><a href="'.$this->collection_id.'/task/h1" target="_blank">'. $this->makeErrorAlertGood(0, $this->h1_missing) .'</a></td>
                  </tr>
                  <tr>
                    <td>H1: doppelte</td>
                    <td><a href="'.$this->collection_id.'/task/h1" target="_blank">'. $this->makeErrorAlertGood(0, $this->h1_duplicates) .'</a></td>
                  </tr>
                  <tr>
                    <td>H1: mehrfach vorhanden</td>
                    <td><a href="'.$this->collection_id.'/task/h1" target="_blank">'. $this->makeErrorAlertGood(0, $this->h1_multi) .'</a></td>
                  </tr>
                  <tr>
                    <td>H2: fehlende / leere</td>
                    <td><a href="'.$this->collection_id.'/task/h2" target="_blank">'. $this->makeErrorAlertGood(0, $this->h2_missing) .'</a></td>
                  </tr>
                  <tr>
                    <td>H2: doppelte</td>
                    <td><a href="'.$this->collection_id.'/task/h2" target="_blank">'. $this->makeErrorAlertGood(0, $this->h2_duplicates) .'</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="box">
            <div class="box-content">
              <table class="table table-normal">
                <thead>
                  <tr>
                    <td>Check</td>
                    <td>Ergebnis</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>HTTP Status: 301</td>
                    <td><a href="'.$this->collection_id.'/task/http_statis_3xx" target="_blank">'. $this->makeErrorAlertGood(0, $this->http_response_301) .'</a></td>
                  </tr>
                  <tr>
                    <td>HTTP Status: 302</td>
                    <td><a href="'.$this->collection_id.'/task/http_statis_3xx" target="_blank">'. $this->makeErrorAlertGood(0, $this->http_response_302) .'</a></td>
                  </tr>
                  <tr>
                    <td>HTTP Status: 4xx</td>
                    <td><a href="'.$this->collection_id.'/task/http_statis_45x" target="_blank">'. $this->makeErrorAlertGood(0, $this->http_response_4xx) .'</a></td>
                  </tr>
                  <tr>
                    <td>HTTP Status: 5xx</td>
                    <td><a href="'.$this->collection_id.'/task/http_statis_45x" target="_blank">'. $this->makeErrorAlertGood(0, $this->http_response_5xx) .'</a></td>
                  </tr>
                  <tr>
                    <td>Robots: nofollow</td>
                    <td><a href="'.$this->collection_id.'/task/robots" target="_blank">'. $this->makeErrorAlertGood(0, $this->robots_no_follow) .'</a></td>
                  </tr>
                  <tr>
                    <td>Robots: noindex</td>
                    <td><a href="'.$this->collection_id.'/task/robots" target="_blank">'. $this->makeErrorAlertGood(0, $this->robots_no_index) .'</a></td>
                  </tr>
                  <tr>
                    <td>Robots: noarchive</td>
                    <td><a href="'.$this->collection_id.'/task/robots" target="_blank">'. $this->makeErrorAlertGood(0, $this->robots_no_archive) .'</a></td>
                  </tr>
                  <tr>
                    <td>Robots: nosnippet</td>
                    <td><a href="'.$this->collection_id.'/task/robots" target="_blank">'. $this->makeErrorAlertGood(0, $this->robots_no_snippet) .'</a></td>
                  </tr>
                  <tr>
                    <td>URL: mehr als 115 Zeichen</td>
                    <td><a href="'.$this->collection_id.'/task/urls" target="_blank">'. $this->makeErrorAlertGood(0, $this->long_url) .'</a></td>
                  </tr>
                  <tr>
                    <td>URL: unerlaubte Zeichen</td>
                    <td><a href="'.$this->collection_id.'/task/urls" target="_blank">'. $this->makeErrorAlertGood(0, $this->url_invalid) .'</a></td>
                  </tr>
                  <tr>
                    <td>URL: Umlaute</td>
                    <td><a href="'.$this->collection_id.'/task/urls" target="_blank">'. $this->makeErrorAlertGood(0, $this->url_umlaut) .'</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>';

  }



  private function dashboardRobots ($array) {

    // erase empty kv pairs from regex
    $array = array_filter($array);

    $this->robots = '';

    if (count($array) > 0) {
      foreach ($array as $robot) {
        $singlerobot = explode(',', $robot);
          foreach ($singlerobot as $sRobot) {
            $this->robots .= $sRobot . ', ';
            if (stripos($sRobot, 'noindex') !== FALSE or stripos($sRobot, 'none') !== FALSE) {
              $this->robots_no_index++;
            } else if (stripos($sRobot, 'nofollow') !== FALSE or stripos($sRobot, 'none') !== FALSE ) {
              $this->robots_no_follow++;
            } else if (stripos($sRobot, 'noarchive') !== FALSE) {
              $this->robots_no_archive++;
            } else if (stripos($sRobot, 'nosnippet') !== FALSE) {
              $this->robots_no_snippet++;
            }
        }
      }
    }

  }


  public function makeErrorAlertGood ($threshold, $value) {

    if ($threshold < $value) {
      $ret = '<span class="label label-red">' . $value . '</span>';
    } else {
      $ret = '<span class="label label-green">' . $value . '</span>';
    }

    return $ret;

  }


}

?>