<?php

require_once 'analyse.class.php';

class check_directory_levels extends analyse {

  private $levels = array();

  protected function header() {

    echo '<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <span class="title">Anzahl Seiten auf den Verzeichnisebenen</span>
                </div>
                <div class="box-content">
                  <div id="ops_chart_levels" class="xcharts-line-dotted" style="width: 100%; height: 300px"></div>
                </div>
              </div>
            </div>
          </div>';

  }


  protected function content() {

    echo '<div class="row" id="ops_result_block"><div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <span class="title">Anzahl Seiten auf den Verzeichnisebenen</span>
              </div>
              <div class="box-content">
            <table class="table table-normal">
              <thead>
              <tr>
                <td>Level</td>
                <td>Anzahl Seiten</td>
                <td>Prozent</td>
              </tr>
              </thead>
              <tbody>';

      $js_sum ='';

      foreach ($this->levels as $k => $sum) {

        echo '<tr>
               <td><a href="../taskcheck/directory_levels_url/'. $k .'">Level ' . $k . ' </td>
               <td>'.$sum.'</td>
               <td class="status-info">' . round($sum * 100 / $this->all_sites , 3) . ' %</td>
              </tr>';
        $js_sum .= $sum . ',';

      }

    echo '<i id="ops_chart_values" data-sum="'. $js_sum .'"></i></tbody></table></div></div></div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true)
                                     );
    $cursor->timeout(-1);

    $this->all_sites = $cursor->count();

    foreach ($cursor as $val) {

      $parsed_url   = parse_url($val['url']);

      if (!isset($parsed_url['path'])) {
        continue;
      }

      $split_url = explode('/', $parsed_url['path']);
      $split_url = array_filter($split_url);
      $levels = count($split_url);
      $this->calcLevels($levels);

    }

    ksort($this->levels);

  }



  private function calcLevels($levels) {

    if (isset($this->levels[$levels])) {
      $this->levels[$levels] = $this->levels[$levels] + 1;
    } else {
      $this->levels[$levels] = 1;
    }

  }


}

?>