<?php

require_once 'analyse.class.php';

class check_urls extends analyse {

  private $long_url = array();
  private $url_invalid = array();
  private $url_umlaut = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">URLs</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>URLs sollten nicht länger als 115 Zeichen sein</li>
                    <li>Umlaute in URLs können unter Umständen problematisch sein</li>
                    <li>URLs müssen valide sein</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>URLs über 115 Zeichen</span> ';

          if (!empty($this->long_url)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_long"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_long"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Invalide URLs</span> ';

          if (!empty($this->url_invalid)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_invalid"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_invalid"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>URLs mit Umlauten</span> ';

          if (!empty($this->url_umlaut)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_umlaut"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_umlaut"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane4" data-toggle="tab">
                <span>Alle URLs</span> ';

          if (!empty($this->all_url)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_all"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_all"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Länge</td>
                  </tr>
                </thead>';

                  foreach ($this->long_url as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td><td>'.strlen($data[0]).'</td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->url_invalid as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->url_umlaut as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

            <div class="tab-pane" id="pane4">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->all_url as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  public function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'canonical' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      // ALL URLS
      $this->all_url[] = array($val['url'], $val['_id']);

      // URL LENGTH
      if (strlen($val['url']) > 115) {
        $this->long_url[] = array($val['url'], $val['_id']);
      }

      // VALIDATE URL
      if (filter_var($val['url'], FILTER_VALIDATE_URL) === FALSE) {
        $this->url_invalid[] = array($val['url'], $val['_id']);
      }

      // UMLAUTE URL
      foreach (utilanalyse::umlaut_hex() as $enc) {
        if (stripos($val['url'], $enc) !== FALSE) {
          $this->url_umlaut[] = array($val['url'], $val['_id']);
        }
      }


    }

  }

}

?>