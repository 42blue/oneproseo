<?php

require_once 'analyse.class.php';

class check_response_time extends analyse {

  private $slow_response = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">'.$this->count_results.' Seiten mit einer Response Time von über 3 Sekunden</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Seiten die länger als 3 Sekunden für eine Antwort benötigen weißen auf ein performance Problem hin</li>
                    <li>Aus Performance Gründen werden nur die ersten 10.000 Ergebnisse angezeigt</li>
                    <li>Die komplette Liste erhälst du über den CSV oder Google Docs Export</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Slow response time</span> ';

          if (!empty($this->slow_response)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="slow_response_time"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="slow_response_time"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Response Time (sec)</td>
                  </tr>
                </thead>';

                  foreach ($this->slow_response as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/' .$data[2] .'" target="_blank">' . $data[1] .'</a></td><td>' . round($data[0], 2) .'</td>';
                    echo '</tr>';
                  }

    echo '    </table>
            </div>
          </div>
        </div>
      </div>';

  }

  public function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('responseTime' => array('$gt' => 3)),
                                        array('url' => true, 'responseTime' => true)
                                     );
    $cursor->timeout(-1);
    $this->count_results = $cursor->count();
    $cursor->limit(10000);

    foreach ($cursor as $val) {

      $this->slow_response[] = array($val['responseTime'], $val['url'], $val['_id']);

    }


  }


}

?>