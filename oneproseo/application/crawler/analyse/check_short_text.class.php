<?php

require_once 'analyse.class.php';

class check_short_text extends analyse {

  private $short_text = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">'.$this->count_results.' zu kurze Texte - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Der Text sollte mindestens 400 Wörter haben.</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Texte mit weniger als 400 Wörtern</span> ';

          if (!empty($this->short_text)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="short_text"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="short_text"></i>';
          }

          echo'</a>
            </li>

          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Anzahl Wörter</td>
                  </tr>
                </thead>';

                  foreach ($this->short_text as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.utilanalyse::splitter($data[0]).'</a></td><td>'.$data[1].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>
      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('bodyTagWords' => array('$lt' => 400, '$gt' => 0)),
                                        array('url' => true, 'bodyTagWords' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);
    $this->count_results = $cursor->count();

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
      }

      $this->short_text[] = array($val['url'], $val['bodyTagWords'], $val['_id']);

    }

  }

}

?>