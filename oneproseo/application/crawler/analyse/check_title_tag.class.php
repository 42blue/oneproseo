<?php

require_once 'analyse.class.php';

class check_title_tag extends analyse {

  private $title_duplicates = array();
  private $title_duplicates_tmp = array();
  private $title_tag_missing = array();
  private $title_short_long = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Title Tag - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Der Title-Tag gehört zu einem der wichtigsten Ranking Faktoren</li>
                    <li>Erscheint in den Google Serp Snippets</li>
                    <li>Optimale Länge 57 Zeichen</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Title Tag: fehlende / leere </span> ';

          if (!empty($this->title_tag_missing)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="title_tag_missing"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="title_tag_missing"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Title Tag: doppelte</span> ';

          if (!empty($this->title_duplicates)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="title_duplicates"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="title_duplicates"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Title Tag: zu lang/kurz</span> ';

          if (!empty($this->title_short_long)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="title_short_long"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="title_short_long"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->title_tag_missing as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Title Tag</td>
                  </tr>
                </thead>';

                  foreach ($this->title_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../taskcheck/title_tag_duplicates_url/'.urlencode($k).'">'.utilanalyse::splitter($k) .'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Inhalt</td><td>Länge</td>
                  </tr>
                </thead>';

                  foreach ($this->title_short_long as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[3].'" target="_blank">'. utilanalyse::splitter($data[0]) .'</a></td><td>'.$data[1].'</td><td>'.$data[2].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'titleTag' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
        if (stripos($value[1], '301') === 0) {
          continue 2;
        }
        if (stripos($value[1], '302') === 0) {
          continue 2;
        }
        if (stripos($value[1], '303') === 0) {
          continue 2;
        }
        if (stripos($value[1], '307') === 0) {
          continue 2;
        }
      }

      foreach ($val['titleTag'] as $value) {
        $value = trim($value);
        if (isset($this->title_duplicates_tmp[$value])) {
          $this->title_duplicates[$value] = '';
        } else {
          $this->title_duplicates_tmp[$value] = '';
        }
        if (strlen($value) < 40 or strlen($value) > 57) {
          $this->title_short_long[] = array($val['url'], $value, strlen($value), $val['_id']);
        }
      }

      if (empty($val['titleTag'])) {
        $this->title_tag_missing[] = array($val['url'], $val['_id']);
      }

    }

  }


}

?>