<?php

require_once 'analyse.class.php';

class check_canonical extends analyse {

  private $canonical_empty = array();
  private $canonical_multi = array();
  private $canonical_relative = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Canonical Tag - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Der Canonical Tag dient dazu, Webseiten mit ähnlichem oder gleichem Inhalt zu kennzeichnen.</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Canonical: nicht vorhanden/leer</span></a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Canonical: mehrfach</span></a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Canonical: keine absolute URL</span></a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->canonical_empty as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->canonical_multi as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->canonical_relative as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'canonical' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
      }

      if (count($val['canonical']) > 1) {
        $this->canonical_multi[] = array($val['url'], $val['_id']);
      }

     if (empty($val['canonical'])) {
        $this->canonical_empty[] = array($val['url'], $val['_id']);
      }

      foreach ($val['canonical'] as $k => $url) {
       if (stripos($url, 'http') !== 0) {
          $this->canonical_relative[] = array($val['url'], $val['_id']);
        }
      }

    }

  }


}

?>