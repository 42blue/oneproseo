<?php

require_once 'analyse.class.php';

class check_url_parameter extends analyse {

  private $multi_get_param = array();
  private $get_param = array();
  private $get_param_no_canonical = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">URL Parameter</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>URL Parameter sollten wenn möglich vermieden werden</li>
                    <li>Wenn URL Parameter eingesetzt werden sollte ebenfalls ein Canonical Tag benutzt werden</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>URL Parameter</span> ';

          if (!empty($this->get_param)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_parameter"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_parameter"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Seiten mit mehr als einem URL Parameter</span> ';

          if (!empty($this->multi_get_param)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_parameter_multi"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_parameter_multi"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Seiten mit URL Parametern und ohne Canonical Tag</span> ';

          if (!empty($this->get_param_no_canonical)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_parameter_multi_no_canonical"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="url_parameter_multi_no_canonical"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Parameter</td><td>Vorkommen</td>
                  </tr>
                </thead>';

                  foreach ($this->get_param as $k => $data) { 
                    echo '<tr>';
                      echo '<td>'.$k.'</td><td>'.$data.'</td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->multi_get_param as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->get_param_no_canonical as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  public function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'canonical' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      $site_url      = $val['url'];
      $siteid        = $val['_id'];

      if (stripos($site_url, '?') !== false) {

        $paramsQ = explode('?', $site_url);
        $params = explode('&', $paramsQ[1]);

        if (substr_count($paramsQ[1], '&') >= 1) {
          $this->multi_get_param[] = array($site_url, $siteid);
          if (empty($val['canonical'])) {
            $this->get_param_no_canonical[] = array($site_url, $siteid);
          }
        }

        foreach ($params as $val) {
          if (substr($val, 0, stripos($val, '=')) !== '') {
            $val = substr($val, 0, stripos($val, '='));
            if (isset($this->get_param[$val])) {
              $this->get_param[$val] = $this->get_param[$val] + 1;
            } else {
              $this->get_param[$val] = 1;
            }
          } else {
            if (isset($this->get_param[$val])) {
              $this->get_param[$val] = $this->get_param[$val] + 1;
            } else {
              $this->get_param[$val] = 1;
            }
          }

        }

      }

    }

  }

}

?>