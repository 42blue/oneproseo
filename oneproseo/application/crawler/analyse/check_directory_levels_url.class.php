<?php

require_once 'analyse.class.php';

class check_directory_levels_url extends analyse {

  private $sites_on_level = array();

  protected function header() {

    echo '<div class="row" id="ops_result_block"><div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <span class="title">Seiten auf Verzeichnisebene: ' . $this->payload . '</span>
              </div>
              <div class="box-content">';

  }


  protected function content() {

    echo '
            <table class="table table-normal data-table">
              <thead>
              <tr>
                <td>URL</td>
                <td>Level</td>
              </tr>
              </thead>';

      foreach ($this->sites_on_level as $k => $data) {

        echo '<tr>
                <td><a href="../../site/'.$data[1].'" target="_blank">' . $data[0] . ' </td>
                <td>' . $this->payload .'</td>
              </tr>';

      }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true)
                                     );
    $cursor->timeout(-1);


    foreach ($cursor as $val) {

      $siteid       = $val['_id'];
      $parsed_url   = parse_url($val['url']);

      if (!isset($parsed_url['path'])) {
        continue;
      }

      $split_url = explode('/', $parsed_url['path']);
      $split_url = array_filter($split_url);
      $level = count($split_url);

      if ($level == $this->payload) {
        $this->sites_on_level[] = array($val['url'], $val['_id']);
      }

    }

  }


}

?>