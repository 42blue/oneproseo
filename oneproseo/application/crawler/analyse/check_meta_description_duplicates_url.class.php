<?php

require_once 'analyse.class.php';

class check_meta_description_duplicates_url extends analyse {

  private $md_duplicates = array();

  protected function header() {

    echo '<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header"><span class="title">Meta Description Duplikate: '. $this->payload .'</span></div>';

  }


  protected function content() {

    echo '
                <div class="box-content">
                  <table class="table table-normal data-table">
                    <thead>
                      <tr>
                        <td>URL</td><td>Title Tag</td>
                      </tr>
                    </thead>';

                  foreach ($this->md_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../../site/'.$data[2].'" target="_blank">'. $data[0] .'</a></td><td>'.implode('<br />', $data[1]).'</td>';
                    echo '</tr>';
                  }
    echo '
                </div>
              </div>
            </div>
         </div>';

  }


  protected function prepareData() {

    $this->payload =  urldecode($this->payload);

    $cursor = $this->crawl_collection->find(
                                        //array('metaDesc' => $this->payload),
                                        array('metaDesc' => new MongoRegex('/'.$this->payload.'/i')),
                                        array('url' => true, 'metaDesc' => true)
                                     );
    $cursor->timeout(-1);


    foreach ($cursor as $val) {

      $this->md_duplicates[] = array($val['url'], $val['metaDesc'] ,$val['_id']);

    }

  }


}

?>