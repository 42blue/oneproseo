<?php

require_once 'analyse.class.php';

class check_meta_description extends analyse {

  private $md_duplicates = array();
  private $md_duplicates_tmp = array();
  private $md_tag_missing = array();
  private $md_short_long = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Meta Description - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Die Meta Description gehört zu einem der wichtigsten Ranking Faktoren</li>
                    <li>Erscheint in den Google Serp Snippets</li>
                    <li>Optimale Länge nicht mehr als 156 Zeichen (inkl. Leerzeichen)</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Meta Description: fehlende / leere </span> ';

          if (!empty($this->md_tag_missing)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_desc_missing"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_desc_missing"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Meta Description: doppelte</span> ';

          if (!empty($this->md_duplicates)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_desc_duplicates"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_desc_duplicates"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Meta Description: zu lang/kurz</span> ';

          if (!empty($this->md_short_long)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_desc_short_long"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_desc_short_long"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->md_tag_missing as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'">'.utilanalyse::splitter($data[0]).'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Meta Description</td>
                  </tr>
                </thead>';

                  foreach ($this->md_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../taskcheck/meta_description_duplicates_url/'.urlencode($k).'">'.utilanalyse::splitter($k).'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Inhalt</td><td>Länge</td>
                  </tr>
                </thead>';

                  foreach ($this->md_short_long as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[3].'" target="_blank">'.utilanalyse::splitter($data[0]).'</a></td><td>'.$data[1].'</td><td>'.$data[2].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'metaDesc' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
        if (stripos($value[1], '301') === 0) {
          continue 2;
        }
        if (stripos($value[1], '302') === 0) {
          continue 2;
        }
        if (stripos($value[1], '303') === 0) {
          continue 2;
        }
        if (stripos($value[1], '307') === 0) {
          continue 2;
        }
      }

      foreach ($val['metaDesc'] as $value) {
        $value = trim($value);
        if (isset($this->md_duplicates_tmp[$value])) {
          $this->md_duplicates[$value] = '';
        } else {
          $this->md_duplicates_tmp[$value] = '';
        }
        if (strlen($value) < 120 or strlen($value) > 156) {
          $this->md_short_long[] = array($val['url'], $value, strlen($value), $val['_id']);
        }
      }

      if (empty($val['metaDesc'])) {
        $this->md_tag_missing[] = array($val['url'], $val['_id']);
      }

    }

  }


}

?>