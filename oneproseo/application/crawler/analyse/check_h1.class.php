<?php

require_once 'analyse.class.php';

class check_h1 extends analyse {

  private $h1_duplicates = array();
  private $h1_duplicates_tmp = array();
  private $h1_missing = array();
  private $h1_multi = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Überschrift H1 - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Jede Seite sollte genau eine H1 Überschrift haben</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Überschrift H1: fehlende / leere </span> ';

          if (!empty($this->h1_missing)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="h1_missing"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="h1_missing"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Überschrift H1: doppelte</span> ';

          if (!empty($this->h1_duplicates)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="h1_duplicates"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="h1_duplicates"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Überschrift H1: mehrfache</span> ';

          if (!empty($this->h1_multi)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="h1_multi"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="h1_multi"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->h1_missing as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.utilanalyse::splitter($data[0]).'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>H1</td>
                  </tr>
                </thead>';

                  foreach ($this->h1_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../taskcheck/h1_duplicates_url/'.urlencode($k).'" target="_blank">'.$k.'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Anzahl H1</td>
                  </tr>
                </thead>';

                  foreach ($this->h1_multi as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.utilanalyse::splitter($data[0]).'</a></td><td>'.count($data[1]).'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'h1content' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
        if (stripos($value[1], '301') === 0) {
          continue 2;
        }
        if (stripos($value[1], '302') === 0) {
          continue 2;
        }
        if (stripos($value[1], '303') === 0) {
          continue 2;
        }
        if (stripos($value[1], '307') === 0) {
          continue 2;
        }
      }

      foreach ($val['h1content'] as $value) {
        $value = trim($value);
        if (isset($this->h1_duplicates_tmp[$value])) {
          $this->h1_duplicates[$value] = '';
        } else {
          $this->h1_duplicates_tmp[$value] = '';
        }
      }
      if (empty($val['h1content'])) {
        $this->h1_missing[] = array($val['url'], $val['_id']);
      }
      if (count($val['h1content']) > 1) {
        $this->h1_multi[] = array($val['url'], $val['h1content'], $val['_id']);
      }

    }

  }


}

?>