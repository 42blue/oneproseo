<?php

require_once 'analyse.class.php';

class check_http_chains extends analyse {

  private $httpChain = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">HTTP Weiterleitungsketten - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>HTTP Weiterleitungsketten weisen generell auf ein Problem hin und sollten auf keinen Fall verlinkt sein.</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>HTTP Weiterleitungsketten </span> ';

          if (!empty($this->httpChain)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_chain"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_chain"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpChain as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a></td><td>'. implode ( '<br/>' , $data[0])  .'</td>';
                    echo '</tr>';
                  }

    echo '    </table>
            </div>
          </div>
        </div>
      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      if (count($val['response']) > 2) {

        $response = array();

        foreach ($val['response'] as $k => $v) {

          $response[] = $v[1];

        }

        $this->httpChain[] = array($response, $val['url'], $val['_id']);

      }

    }

  }


}

?>