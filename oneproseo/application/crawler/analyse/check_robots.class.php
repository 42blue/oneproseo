<?php

require_once 'analyse.class.php';

class check_robots extends analyse {

  private $nofollow = array();
  private $noindex = array();
  private $noarchive = array();
  private $nosnippet = array();
  private $multirobot = array();
  private $norobot = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Meta-Robots / X-Robots</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Meta-Robots: Meta Tag im HTML Quelltext</li>
                    <li>X-Robots: Robots Tag im Response Header</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Robots: NOINDEX</span> ';

          if (!empty($this->noindex)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Robots: NOFOLLOW</span> ';

          if (!empty($this->nofollow)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Robots: NOARCHIVE</span> ';

          if (!empty($this->noarchive)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane4" data-toggle="tab">
                <span>Robots: NOSNIPPET</span> ';

          if (!empty($this->nosnippet)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane5" data-toggle="tab">
                <span>Multiple Robots</span> ';

          if (!empty($this->multirobot)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane6" data-toggle="tab">
                <span>Keine Robots</span> ';

          if (!empty($this->norobot)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="meta_x_robots"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->noindex as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->nofollow as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->noarchive as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane4">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->nosnippet as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane5">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->multirobot as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane6">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td>
                  </tr>
                </thead>';

                  foreach ($this->norobot as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[1].'" target="_blank">'.$data[0].'</a></td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'metaRobot' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    $cursor->limit(10000);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
      }


      if (count($val['metaRobot']) > 1) {
        $this->multirobot[] = array($val['url'], $val['_id']);
      }

     if (empty($val['metaRobot'])) {
        $this->norobot[] = array($val['url'], $val['_id']);
      }

      if (count($val['metaRobot']) > 0) {

        foreach ($val['metaRobot'] as $robot) {

          $singlerobot = explode(',', $robot);

          foreach ($singlerobot as $sRobot) {

            if (stripos($sRobot, 'noindex') !== FALSE or stripos($sRobot, 'none') !== FALSE) {
              $this->noindex[] = array($val['url'], $val['_id']);
            } else if (stripos($sRobot, 'nofollow') !== FALSE or stripos($sRobot, 'none') !== FALSE ) {
              $this->nofollow[] = array($val['url'], $val['_id']);
            } else if (stripos($sRobot, 'noarchive') !== FALSE) {
              $this->noarchive[] = array($val['url'], $val['_id']);
            } else if (stripos($sRobot, 'nosnippet') !== FALSE) {
              $this->nosnippet[] = array($val['url'], $val['_id']);
            }

          }

        }

      }

    }

  }


}

?>