<?php

require_once 'analyse.class.php';

class check_http_statis_3xx extends analyse {

  private $httpResponse301 = array();
  private $httpResponse302 = array();
  private $httpResponse3xx = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">HTTP Status 301, 302, 3xx - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>HTTP 301 sind okay sollten aber nicht intern verlinkt sein</li>
                    <li>HTTP 302 sollen intern auf keinen Fall verlinkt sein und wenn dann nur kurzzeitig eingesetzt werden (3-4 Tage)</li>
                    <li>HTTP 300, 303 und 305 sollen gar nicht verwendet werden</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>HTTP Status 301 </span> ';

          if (!empty($this->httpResponse301)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_3xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_3xx"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>HTTP Status 302 </span> ';

          if (!empty($this->httpResponse302)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_3xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_3xx"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>HTTP Status 3xx </span> ';

          if (!empty($this->httpResponse3xx)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_3xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_3xx"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpResponse301 as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a> <a href="../taskcheck/links_from/'.base64_encode($data[1]).'" target="_blank"><i title="verlinkt von" class="icon-question-sign"></i></a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpResponse302 as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a> <a href="../taskcheck/links_from/'.base64_encode($data[1]).'" target="_blank"><i title="verlinkt von" class="icon-question-sign"></i></a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpResponse3xx as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a> <a href="../taskcheck/links_from/'.base64_encode($data[1]).'" target="_blank"><i title="verlinkt von" class="icon-question-sign"></i></a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $k => $status) {

        if (stripos($status[1], '301') === 0) {
          $this->httpResponse301[] = array($status[1], $status[0], $val['_id']);
        } else if (stripos($status[1], '302') === 0) {
          $this->httpResponse302[] = array($status[1], $status[0], $val['_id']);
        } else if (stripos($status[1], '3') === 0) {
          $this->httpResponse3xx[] = array($status[1], $status[0], $val['_id']);
        }

      }

    }

  }


}

?>