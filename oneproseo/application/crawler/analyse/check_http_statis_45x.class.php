<?php

require_once 'analyse.class.php';

class check_http_statis_45x extends analyse {

  private $httpResponse4xx = array();
  private $httpResponse5xx = array();
  private $httpResponseUnd = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">HTTP Status 4xx, 5xx, Undefinded - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>HTTP 4xx sind Seiten die verlinkt aber nicht zu erreichen sind</li>
                    <li>HTTP 5xx sind Seiten die der Server nicht erfolgreich beantworten kann</li>
                    <li>undefined sind Seiten die keine HTTP Response während des Crawls geliefert haben</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>HTTP Status 4xx </span> ';

          if (!empty($this->httpResponse4xx)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>HTTP Status 5xx </span> ';

          if (!empty($this->httpResponse5xx)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>HTTP Status undefined </span> ';

          if (!empty($this->httpResponseUnd)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>';
          }

          echo'</a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpResponse4xx as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a> <a href="../taskcheck/links_from/'.base64_encode($data[1]).'" target="_blank"><i title="verlinkt von" class="icon-question-sign"></i></a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>


            </div>
            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpResponse5xx as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a> <a href="../taskcheck/links_from/'.base64_encode($data[1]).'" target="_blank"><i title="verlinkt von" class="icon-question-sign"></i></a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Status</td>
                  </tr>
                </thead>';

                  foreach ($this->httpResponseUnd as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[1].'</a> <a href="../taskcheck/links_from/'.base64_encode($data[1]).'" target="_blank"><i title="verlinkt von" class="icon-question-sign"></i></a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $k => $status) {

        if (stripos($status[1], '4') === 0) {
          $this->httpResponse4xx[] = array($status[1], $status[0], $val['_id']);
        }
        if (stripos($status[1], '5') === 0) {
          $this->httpResponse5xx[] = array($status[1], $status[0], $val['_id']);
        }
        if (stripos($status[1], 'undefined') == 'undefined') {
          $this->httpResponseUnd[] = array($status[1], $status[0], $val['_id']);
        }

      }

    }

  }


}

?>