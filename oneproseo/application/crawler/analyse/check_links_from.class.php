<?php

require_once 'analyse.class.php';

class check_links_from extends analyse {

  private $links_from = array();
  private $redirects_from = array();

  protected function header() {

    echo '<div class="row">
            <div class="col-md-12">
              <div class="box">';


  }


  protected function content() {

    echo '
              <div class="box-header"><span class="title">'. $this->payload .' wird verlinkt von:</span></div>
                <div class="box-content">
                  <table class="table table-normal data-table">
                    <thead>
                      <tr>
                        <td>URL</td>
                      </tr>
                    </thead>';

                  foreach ($this->links_from as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../../site/'.$data[1].'" target="_blank">'. $data[0] .'</a></td>';
                    echo '</tr>';
                  }
    echo '
                </div>
              </div>
            </div>
         </div>';

  }


  protected function prepareData() {

    $this->payload = base64_decode($this->payload);

    // search for redicrect sources in combination with https
    $this->payload = $this->removeHttp($this->payload);

    $cursor = $this->crawl_collection->find(
                                          array('allLinks.href' => new MongoRegex('/'.$this->payload.'/i')),
                                          array('url' => true)
                                       );
    $cursor->timeout(-1);

    foreach ($cursor as $val) {
      $this->links_from[] = array($val['url'] ,$val['_id']);
    }


    $cursor2 = $this->crawl_collection->find(
                                          array('response.1.0' => new MongoRegex('/'.$this->payload.'/i')),
                                          array('url' => true)
                                       );
    $cursor2->timeout(-1);

    foreach ($cursor2 as $val) {
      $this->redirects_from[] = array($val['url'] ,$val['_id']);
    }

  }


  protected function removeHttp ($url) {

    $disallowed = array('http://', 'https://');

    foreach($disallowed as $d) {

      if (strpos($url, $d) === 0) {

         return str_replace($d, '', $url);

      }

    }

    return $url;

  }


}

?>