<?php

require_once 'analyse.class.php';

class check_links_error extends analyse {

  private $links100 = array();
  private $links5 = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Zuviele, Zuwenige, keine Links</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Zur besseren Crawlbarkeit sollten Seiten maximal 100 interne Links haben.</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Seiten mit mehr als 100 Links</span></a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Seiten mit weniger als 5 Links</span> </a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Anzahl</td>
                  </tr>
                </thead>';

                  foreach ($this->links100 as $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[0].'</a></td><td>'.$data[1].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Anzahl</td>
                  </tr>
                </thead>';

                  foreach ($this->links5 as $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[0].'</a></td><td>'.$data[1].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>
          </div>
        </div>

      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'allLinks' => true, 'response' => true)
                                     );
    $cursor->limit(1000);
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {
        if (stripos($value[1], 'undefined') == 'undefined') {
          continue 2;
        }
      }

      if (count($val['allLinks']) > 100) {
        $this->links100[] = array($val['url'], count($val['allLinks']), $val['_id']);
      }

      if (count($val['allLinks']) < 5) {
        $this->links5[] = array($val['url'], count($val['allLinks']), $val['_id']);
      }

    }


  }


}


?>