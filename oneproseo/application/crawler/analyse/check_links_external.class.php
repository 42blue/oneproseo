<?php

require_once 'analyse.class.php';

class check_links_external extends analyse {

  private $externallUrl = array();
  private $externalUrlNoFollow = array();
  private $externalUrlVerbose = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Externe Links</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Während des Crawls werden hier nur die Links der ersten 1000 gecrawlten Seiten angezeigt.</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Externe Links</span> ';

          if (empty($this->externallUrl)) {
           echo'<i title="CSV download" class="icon-cloud-download csv-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>
                <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-crawlcollection="'.$this->collection_id.'" data-type="http_4xx_5xx"></i>';
          }

          echo'</a>
            </li>
            <li>
              <a href="#pane2" data-toggle="tab">
                <span>Externe Links (nofollow)</span> </a>
            </li>
            <li>
              <a href="#pane3" data-toggle="tab">
                <span>Externe Links (komplett)</span> </a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Link</td><td>Anzahl</td>
                  </tr>
                </thead>';

                  foreach ($this->externallUrl as $k => $data) { 
                    echo '<tr>';
                      echo '<td>' . utilanalyse::splitter($k) . '</td><td>'.$data.'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

            <div class="tab-pane" id="pane2">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Link</td><td>Anzahl</td>
                  </tr>
                </thead>';

                  foreach ($this->externalUrlNoFollow as $k => $data) { 
                    echo '<tr>';
                      echo '<td>' . utilanalyse::splitter($k) . '</td><td>'.$data.'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

            <div class="tab-pane" id="pane3">

              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>Ziel</td><td>Linktext</td><td>Herkunft</td><td>Follow</td>
                  </tr>
                </thead>';

                  foreach ($this->externalUrlVerbose as $k => $data) { 
                    echo '<tr>';
                      echo '<td>' . utilanalyse::splitter($data[1]) . '</td><td>'.$data[3].'</td><td>' . utilanalyse::splitter($data[0]) . '</td><td>'.($data[2] ? 'Nofollow' : 'Follow').'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

          </div>
        </div>

      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'allLinks' => true)
                                     );
    // memory problem
    $cursor->limit(1000);
    $cursor->timeout(-1);

    foreach ($cursor as $val) {

      foreach ($val['allLinks'] as $set) {

        $href     = $set['href'];
        $nofollow = $set['nofollow'];
        $linktext = $set['linktext'];
        $base     = $val['url'];

        // nur interne urls parsen
        if (utilanalyse::getHostRemoveWWW($href) != utilanalyse::getHostRemoveWWW($base)) {
          $this->externallUrl[] = $href;
          $this->externalUrlVerbose[] = array($base, $href, $nofollow, $linktext);
          if ($nofollow == true) {
            $this->externalUrlNoFollow[] = $href;
          }
        }

      }

    }

    $this->externallUrl = array_count_values($this->externallUrl);
    $this->externalUrlNoFollow = array_count_values($this->externalUrlNoFollow);

  }


}

?>