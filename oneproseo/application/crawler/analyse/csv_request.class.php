
<?php

require_once dirname(__FILE__) . '/../crawler.class.php';
require_once 'utilanalyse.class.php';

class csv_request extends crawler {

  public function __construct ($env_data) {

    parent::setEnv($env_data);

    $this->mdb = $this->dbConnect();

    if (isset($_GET['collid']) && isset($_GET['type'])) {

      $collection_id      = strip_tags($_GET['collid']);
      $collection_crawl   = $this->mdb->activecrawls;
      $this->cursor_crawl = $collection_crawl->findOne(array('crawlcollection' => $collection_id));

      // defines mapping
      $this->type = strip_tags($_GET['type']);

      // load mapping depending on type
      $this->mapping();

      // data base query
      $this->fetchData();

      // post and preprocess the data
      $dynfuncPre  = $this->mapping[$this->type]['preprocess'] . 'Preprocess';
      $dynfuncPost = $this->mapping[$this->type]['postprocess'] . 'Postprocess';
      $this->$dynfuncPre();
      $this->$dynfuncPost();

      // create csv and return
      $filename = $this->csvOutput();

      // GOOGLE DRIVE OUTPUT
      if (isset($_GET['show'])) {

        $filepath = $this->env_data['csvstore'] . $filename;
        $_SESSION['google_drive_filepath'] = $filepath;
        $_SESSION['google_drive_filename'] = $this->mapping[$this->type]['filename'];
        $url = file_get_contents($this->env_data['googledrive']);

        echo $url;

      // PLAIN CSV OUTPUT
      } else {

        echo $filename;

      }


    } else {

      exit ("NO VALID COLLECTION SET!");

    }

  }

  private function mapping () {

    $this->mapping = array();

  // TITLE TAG
    $this->mapping['title_tag_missing'] = array (
       'fieldid' => 'titleTag', 'preprocess' => 'titleTagMissing', 'postprocess' => 'list', 
       'filename' => 'Fehlende Title Tags',
       'csvheadline' => array('URL')
       );
    $this->mapping['title_duplicates'] = array (
      'fieldid' => 'titleTag', 'preprocess' => 'titleTagDuplicates', 'postprocess' => 'duplicate', 
      'filename' => 'Doppelte Title Tags',
      'csvheadline' => array('Inhalt', 'Anzahl', 'URL')
      );
    $this->mapping['title_short_long'] = array (
      'fieldid' => 'titleTag', 'preprocess' => 'titleTagShortLong', 'postprocess' => 'list', 
      'filename' => 'Kurze & Lange Title Tags',
      'csvheadline' => array('URL', 'Inhalt', 'Länge')
      );

  // META DESCRIPTION
    $this->mapping['meta_desc_missing'] = array (
      'fieldid' => 'metaDesc', 'preprocess' => 'titleTagMissing', 'postprocess' => 'list', 
      'filename' => 'Fehlende Meta Description',
       'csvheadline' => array('URL')
      );
    $this->mapping['meta_desc_duplicates'] = array (
      'fieldid' => 'metaDesc', 'preprocess' => 'titleTagDuplicates', 'postprocess' => 'duplicate', 
      'filename' => 'Doppelte Meta Description',
      'csvheadline' => array('Inhalt', 'Anzahl', 'URL')
      );
    $this->mapping['meta_desc_short_long'] = array (
      'fieldid' => 'metaDesc', 'preprocess' => 'titleTagShortLong', 'postprocess' => 'list', 
      'filename' => 'Kurze & Lange Meta Description',
      'csvheadline' => array('URL', 'Inhalt', 'Länge')
      );

  // H1
    $this->mapping['h1_missing'] = array (
      'fieldid' => 'h1content', 'preprocess' => 'titleTagMissing', 'postprocess' => 'list', 
      'filename' => 'Fehlende H1 Tags',
       'csvheadline' => array('URL')
      );
    $this->mapping['h1_duplicates'] = array (
      'fieldid' => 'h1content', 'preprocess' => 'titleTagDuplicates', 'postprocess' => 'duplicate', 
      'filename' => 'Doppelte H1 Tags',
      'csvheadline' => array('Inhalt', 'Anzahl', 'URL')
      );
    $this->mapping['h1_multi'] = array (
      'fieldid' => 'h1content', 'preprocess' => 'h1Multi', 'postprocess' => 'count', 
      'filename' => 'Multiple H1 Tags',
      'csvheadline' => array('URL', 'Anzahl')
      );

  // H2
    $this->mapping['h2_missing'] = array (
      'fieldid' => 'h2content', 'preprocess' => 'titleTagMissing', 'postprocess' => 'list', 
      'filename' => 'Fehlende H2 Tags',
      'csvheadline' => array('URL')
      );
    $this->mapping['h2_duplicates'] = array (
      'fieldid' => 'h2content', 'preprocess' => 'titleTagDuplicates', 'postprocess' => 'duplicate', 
      'filename' => 'Doppelte H2 Tags',
      'csvheadline' => array('Inhalt', 'Anzahl', 'URL')
      );

  // SHORT TEXT
    $this->mapping['short_text'] = array (
      'fieldid' => 'bodyTagWords', 'preprocess' => 'shortText', 'postprocess' => 'list', 
      'filename' => 'Zu kurze Texte',
      'csvheadline' => array('URL', 'Anzahl Wörter')
      );

  // HTTP STATIS
    $this->mapping['http_3xx'] = array (
      'fieldid' => 'response', 'preprocess' => 'http3xx', 'postprocess' => 'list', 
      'filename' => 'HTTP 301 / HTTP 302 / HTTP 3xx',
      'csvheadline' => array('URL', 'HTTP Status')
      );
    $this->mapping['http_4xx_5xx'] = array (
      'fieldid' => 'response', 'preprocess' => 'http4xx5xx', 'postprocess' => 'list', 
      'filename' => 'HTTP 4xx / HTTP 5xx / Undefined',
      'csvheadline' => array('URL', 'HTTP Status')
      );
    $this->mapping['http_chain'] = array (
      'fieldid' => 'response', 'preprocess' => 'httpchain', 'postprocess' => 'list', 
      'filename' => 'HTTP Weiterleitunsketten',
      'csvheadline' => array('URL', 'Anzahl Weiterleitungen')
      );

  // RESPONSE TIME
    $this->mapping['slow_response_time'] = array (
      'fieldid' => 'responseTime', 'preprocess' => 'slowResponseTime', 'postprocess' => 'list', 
      'filename' => 'Langsame Antwortzeiten',
      'csvheadline' => array('URL', 'Response Time')
      );

  // X-ROBOTS META ROBOTS
    $this->mapping['meta_x_robots'] = array (
      'fieldid' => 'metaRobot', 'preprocess' => 'metaRobots', 'postprocess' => 'array', 
      'filename' => 'Robots',
      'csvheadline' => array('URL', 'Robots')
      );

  // URL PARAMS
    $this->mapping['url_parameter'] = array (
      'fieldid' => 'url', 'preprocess' => 'urlParams', 'postprocess' => 'array', 
      'filename' => 'URL Parameter',
      'csvheadline' => array('Parameter', 'Anzahl')
      );
    $this->mapping['url_parameter_multi'] = array (
      'fieldid' => 'url', 'preprocess' => 'urlParamsMulti', 'postprocess' => 'list', 
      'filename' => 'Multiple URL Parameter',
      'csvheadline' => array('URL')
      );
    $this->mapping['url_parameter_multi_no_canonical'] = array (
      'fieldid' => 'canonical', 'preprocess' => 'urlParamsMultiNoCanonical', 'postprocess' => 'list', 
      'filename' => 'Multiple URL Parameter und kein Canonical',
      'csvheadline' => array('URL')
      );

  // URLs
    $this->mapping['url_long'] = array (
      'fieldid' => 'url', 'preprocess' => 'urlLong', 'postprocess' => 'list', 
      'filename' => 'Zu lange URLs',
      'csvheadline' => array('URL')
      );
    $this->mapping['url_invalid'] = array (
      'fieldid' => 'url', 'preprocess' => 'urlInvalid', 'postprocess' => 'list', 
      'filename' => 'Invalide URLs',
      'csvheadline' => array('URL')
      );
    $this->mapping['url_umlaut'] = array (
      'fieldid' => 'url', 'preprocess' => 'urlUmlaut', 'postprocess' => 'list', 
      'filename' => 'URLs mit Umlauten',
      'csvheadline' => array('URL')
      );

    $this->mapping['url_all'] = array (
      'fieldid' => 'url', 'preprocess' => 'list', 'postprocess' => 'list', 
      'filename' => 'Alle URLs',
      'csvheadline' => array('URL')
      );


  }




// PREPROCESSORS


  private function listPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      $this->res[] = array($val[$this->mapping[$this->type]['fieldid']]);

    }

  }


  private function urlLongPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (strlen($val[$this->mapping[$this->type]['fieldid']]) > 115) {
        $this->res[] = array($val['url']);
      }

    }

  }

  private function urlInvalidPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (filter_var($val[$this->mapping[$this->type]['fieldid']], FILTER_VALIDATE_URL) === FALSE) {
        $this->res[] = array($val['url']);
      }

    }

  }

  private function urlUmlautPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      // UMLAUTE URL
      foreach (utilanalyse::umlaut_hex() as $enc) {
        if (stripos($val[$this->mapping[$this->type]['fieldid']], $enc) !== FALSE) {
          $this->res[] = array($val['url']);
        }
      }

    }

  }


  private function urlParamsPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (stripos($val['url'], '?') !== false) {

        $paramsQ = explode('?', $val['url']);
        $params = explode('&', $paramsQ[1]);

        foreach ($params as $value) {
          if (substr($value, 0, stripos($value, '=')) !== '') {
            $value = substr($value, 0, stripos($value, '='));
            if (isset($this->res[$value])) {
              $this->res[$value] = $this->res[$value] + 1;
            } else {
              $this->res[$value] = 1;
            }
          } else {
            if (isset($this->res[$value])) {
              $this->res[$value] = $this->res[$value] + 1;
            } else {
              $this->res[$value] = 1;
            }
          }

        }

      }

    }

  }


  private function urlParamsMultiPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (stripos($val['url'], '?') !== false) {

        $paramsQ = explode('?', $val['url']);
        $params = explode('&', $paramsQ[1]);

        if (substr_count($paramsQ[1], '&') >= 1) {
          if (empty($val['canonical'])) {
            $this->get_param_no_canonical[] = array($val['url']);
          }
        }

      }

    }

  }


  private function urlParamsMultiNoCanonicalPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (stripos($val['url'], '?') !== false) {

        $paramsQ = explode('?', $val['url']);
        $params = explode('&', $paramsQ[1]);

        if (substr_count($paramsQ[1], '&') >= 1) {
          $this->res[] = array($val['url']);
        }

      }

    }

  }


  private function shortTextPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (isset($val[$this->mapping[$this->type]['fieldid']]) && $val[$this->mapping[$this->type]['fieldid']] < 400) {
        $this->res[] = array($val['url'], $val[$this->mapping[$this->type]['fieldid']]);
      }

    }

  }


  private function h1MultiPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (count($val[$this->mapping[$this->type]['fieldid']]) > 1) {

        $this->res[] = array($val['url'], $val[$this->mapping[$this->type]['fieldid']]);

      }

    }

  }


  private function titleTagMissingPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (empty($val[$this->mapping[$this->type]['fieldid']])) {

        $this->res[] = array($val['url']);

      }

    }

  }


  private function titleTagDuplicatesPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      foreach ($val[$this->mapping[$this->type]['fieldid']] as $value) {

        $this->res[] = array($value, $val['url']);

      }

    }

  }


  private function http3xxPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      foreach ($val[$this->mapping[$this->type]['fieldid']] as $value) {
        if (stripos($value[1], '3') === 0) {
          $this->res[] = array($val['url'], $value[1]);
        }

      }

    }

  }


  private function http4xx5xxPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      foreach ($val[$this->mapping[$this->type]['fieldid']] as $value) {
        if (stripos($value[1], '4') === 0 or stripos($value[1], '5') === 0 or $value[1] == 'undefined') {
          $this->res[] = array($val['url'], $value[1]);
        }

      }

    }

  }


  private function httpchainPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      if (count($val[$this->mapping[$this->type]['fieldid']]) > 2 ) {
        $this->res[] = array($val['url'], count($val[$this->mapping[$this->type]['fieldid']]));
      }

    }

  }


  private function metaRobotsPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      $this->res[$val['url']] = array();

      if (count($val[$this->mapping[$this->type]['fieldid']]) > 0) {

        foreach ($val[$this->mapping[$this->type]['fieldid']] as $robot) {

          $singlerobot = explode(',', $robot);

          foreach ($singlerobot as $sRobot) {

            if (stripos($sRobot, 'noindex') !== FALSE or stripos($sRobot, 'none') !== FALSE) {
              array_push($this->res[$val['url']], 'noindex');
            } else if (stripos($sRobot, 'nofollow') !== FALSE or stripos($sRobot, 'none') !== FALSE ) {
              array_push($this->res[$val['url']], 'nofollow');
            } else if (stripos($sRobot, 'noarchive') !== FALSE) {
              array_push($this->res[$val['url']], 'noarchive');
            } else if (stripos($sRobot, 'nosnippet') !== FALSE) {
              array_push($this->res[$val['url']], 'nosnippet');
            }

          }

        }

      }

    }

  }


  private function titleTagShortLongPreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      foreach ($val[$this->mapping[$this->type]['fieldid']] as $value) {

        if (strlen($value) < 40 or strlen($value) > 58) {
          $this->res[] = array($val['url'], $value, strlen($value));
        }

      }

    }

  }


  private function slowResponseTimePreprocess () {

    $this->res = array();

    foreach ($this->cursor as $val) {

      $value = $val[$this->mapping[$this->type]['fieldid']];

      if ($value > 3) {
        $this->res[] = array($val['url'], $value);
      }

    }

  }



// POSTPROCESSORS

  private function listPostprocess () {

    $this->list = array();
    $this->list[] = $this->mapping[$this->type]['csvheadline'];

    foreach ($this->res as $k => $url) {

      $this->list[] = $url;

    }

  }

  private function countPostprocess () {

    $this->list = array();
    $this->list[] = $this->mapping[$this->type]['csvheadline'];

    foreach ($this->res as $k => $arr) {

      $this->list[] = array($arr[0], count($arr[1]));

    }

  }


  private function arrayPostprocess () {

    $this->list = array();
    $this->list[] = $this->mapping[$this->type]['csvheadline'];

    foreach ($this->res as $url => $arr) {

      if (!empty($arr) && is_array($arr)) {
        $this->list[] = array($url, implode(',', $arr));
      } else if (!empty($arr)) {
        $this->list[] = array($url, $arr);
      }

    }

  }


  private function duplicatePostprocess () {

    $res = utilanalyse::checkForDuplicates($this->res);

    $this->list = array();
    $this->list[] = $this->mapping[$this->type]['csvheadline'];

    foreach ($res as $title => $urls) {

      $this->list[] = array($title, count($urls), implode(',"', $urls));

    }

  }


// DB QUERY
  private function fetchData() {

    $cursor_crawl = $this->cursor_crawl;
    $crawl_collection = $this->mdb->$cursor_crawl['crawlcollection'];

    $this->cursor = $crawl_collection->find(array('url' => array('$exists' => true)), array('url' => true, $this->mapping[$this->type]['fieldid'] => true));

  }


// CSV BUILDER
  private function csvOutput () {

    $date = date_create();
    $filename = $this->type . '_' . date_timestamp_get($date) . '.csv';

    $fp = fopen($this->env_data['csvstore'] . $filename, 'a');

    foreach ($this->list as $value) {
      fputcsv($fp, $value);
    }

    fclose($fp);

    return $filename;

  }


}

?>