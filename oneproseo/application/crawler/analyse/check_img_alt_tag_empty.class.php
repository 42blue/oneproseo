<?php

require_once 'analyse.class.php';

class check_img_alt_tag_empty extends analyse {

  private $missing_alt_tags = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">'.$this->count_results.' Seiten haben leere IMG ALT Tags - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Bilder benötigen einen beschreibenden ALT Tag</li>
                    <li>Aus Performance Gründen werden nur die ersten 10.000 Ergebnisse angezeigt</li>
                    <li>Die komplette Liste erhälst du über den CSV oder Google Docs Export</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Leere oder fehlende ALT Tags</span></a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>Anzahl leerer ALT Tags</td>
                  </tr>
                </thead>';

                  foreach ($this->missing_alt_tags as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.substr($data[0],0,200).'</a></td><td>'. $data[1]  .'</td>';
                    echo '</tr>';
                  }

    echo '    </table>
            </div>
          </div>
        </div>
      </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('missingAltImg' => array('$exists' => true), '$where' => 'this.missingAltImg.length > 0'),
                                        array('url' => true, 'missingAltImg' => true)
                                     );
    $cursor->timeout(-1);

    $this->count_results = $cursor->count();
    $cursor->limit(10000);

    foreach ($cursor as $val) {

      if (!empty($val['missingAltImg'])) {
        $this->missing_alt_tags[] = array($val['url'], count($val['missingAltImg']), $val['_id']);
      }

    }

  }


}

?>