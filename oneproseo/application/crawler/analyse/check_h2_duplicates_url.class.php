<?php

require_once 'analyse.class.php';

class check_h2_duplicates_url extends analyse {

  private $md_duplicates = array();

  protected function header() {

    echo '<div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header"><span class="title">H2 Duplikate: ' . $this->payload . '</span></div>';

  }


  protected function content() {

    echo '
                <div class="box-content">
                  <table class="table table-normal data-table">
                    <thead>
                      <tr>
                        <td>URL</td><td>H2 Tag</td>
                      </tr>
                    </thead>';

                  foreach ($this->md_duplicates as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../../site/' . $data[2] . '" target="_blank">' . $data[0] . '</a></td><td>' . implode('<br />', $data[1]) . '</td>';
                    echo '</tr>';
                  }
    echo '
                </div>
              </div>
            </div>
         </div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('h2content' => new MongoRegex('/'.$this->payload.'/i')),
                                        //array('h2content' => $this->payload),
                                        array('url' => true, 'h2content' => true)
                                     );
    $cursor->timeout(-1);


    foreach ($cursor as $val) {

      $this->md_duplicates[] = array($val['url'], $val['h2content'] ,$val['_id']);

    }

  }


}

?>