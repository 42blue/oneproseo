<?php

require_once 'analyse.class.php';

class check_crawl_error extends analyse {

  private $crawl_error = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Crawl Errors - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Während des Crawls kommt es vor das Seiten nicht antworten, um dies zu verhindern kann man die Crawlgeschwindigkeit reduzieren</li>
                    <li>Diese Seiten werden in der Analyse nicht berücksichtigt</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Crawl Error </span></a>
            </li>

          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>HTTP Status</td>
                  </tr>
                </thead>';

                  foreach ($this->crawl_error as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'. utilanalyse::splitter($data[1]) .'</a></td><td>'.$data[0].'</td>';
                    echo '</tr>';
                  }

    echo '    </table>

            </div>

          </div>
        </div>

      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);


    foreach ($cursor as $val) {

      foreach ($val['response'] as $value) {

        if (stripos($value[1], 'undefined') == 'undefined') {
          $this->crawl_error[] = array($value[1], $value[0], $val['_id']);
        }

      }

    }

  }


}


?>