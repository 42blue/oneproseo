<?php

require_once 'analyse.class.php';

class check_links_directory_levels extends analyse {

  private $levels = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">Links je Verzeichnisebene (Level)</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Ausgehende Links per Page Level</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '<div class="row" id="ops_result_block"><div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <span class="title">Übersicht</span>
              </div>
              <div class="box-content">
            <table class="table table-normal">
              <thead>
              <tr>
                <td>Vorhandene Level</td>
                <td>Anzahl Seiten</td>
                <td>#</td>
              </tr>
              </thead>
              <tbody>';

      $js_sum ='';

      foreach ($this->levels as $k => $sum) {

        echo '<tr>
               <td><a href="../taskcheck/links_directory_levels_url/'. $k .'">Level ' . $k . ' </td>
               <td>'.$sum.'</td>
               <td><a href="../taskcheck/links_directory_levels_url/'. $k .'">Links anzeigen</td>
              </tr>';

      }

    echo '</tbody></table></div></div></div>';

  }


  protected function prepareData() {

    $cursor = $this->crawl_collection->find(
                                        array('url' => array('$exists' => true)),
                                        array('url' => true)
                                     );
    $cursor->timeout(-1);


    foreach ($cursor as $val) {

      $parsed_url   = parse_url($val['url']);

      if (!isset($parsed_url['path'])) {
        continue;
      }

      $split_url = explode('/', $parsed_url['path']);
      $split_url = array_filter($split_url);
      $levels = count($split_url);

      $this->calcLevels($levels);

    }

    ksort($this->levels);

  }



  private function calcLevels($levels) {

    if (isset($this->levels[$levels])) {
      $this->levels[$levels] = $this->levels[$levels] + 1;
    } else {
      $this->levels[$levels] = 1;
    }

  }


}


?>