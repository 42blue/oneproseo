<?php

require_once 'analyse.class.php';

class check_source_code_large extends analyse {

  private $large_file_size = array();

  protected function header() {

    echo '<div class="row">

            <div class="col-md-12">

              <div class="box">
                <div class="box-header"><span class="title">'.$this->count_results.' Seiten haben einen Quelltext über 100 KB - Info</span></div>
                <div class="box-content padded">
                  <ul class="content">
                    <li>Je umfangreicher der Quelltext desto schlechter lässt sich die Seite crawlen und desto länger benötigt sie um zu laden</li>
                    <li>Aus Performance Gründen werden nur die ersten 10.000 Ergebnisse angezeigt</li>
                    <li>Die komplette Liste erhälst du über den CSV oder Google Docs Export</li>
                  </ul>
                </div>
              </div>';

  }


  protected function content() {

    echo '
      <div class="box">

        <div class="box-header">
          <ul class="nav nav-tabs nav-tabs-left">
            <li class="active">
              <a href="#pane1" data-toggle="tab"> 
                <span>Quelltext über 100 KB</span></a>
            </li>
          </ul>
        </div>

        <div class="box-content">
          <div class="tab-content">
            <div class="tab-pane active" id="pane1">

               <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <td>URL</td><td>KB</td>
                  </tr>
                </thead>';

                  foreach ($this->large_file_size as $k => $data) { 
                    echo '<tr>';
                      echo '<td><a href="../site/'.$data[2].'" target="_blank">'.$data[0].'</a></td><td>'. round($data[1], 2)  .'</td>';
                    echo '</tr>';
                  }

    echo '    </table>
            </div>
          </div>
        </div>
      </div>';

  }


  protected function prepareData() {


    $cursor = $this->crawl_collection->find(
                                        array('fileSize' => array('$gt' => 102000)),
                                        array('url' => true, 'fileSize' => true, 'response' => true)
                                     );
    $cursor->timeout(-1);
    $this->count_results = $cursor->count();
    $cursor->limit(10000);

    foreach ($cursor as $val) {

      $this->large_file_size[] = array($val['url'], $val['fileSize']/1024, $val['_id']);

    }

  }


}

?>