<?php

class advertising {

  public function __construct ($env_data) {

    $this->view = 'standard';

    if ($_POST['view']) {
      $this->view = strip_tags($_POST['view']);
    }

    $tmpfilename = 'adv_xml_feed.tmp';

    $cache = new Cache();
    $cache->setEnv($env_data);
    $data = $cache->readFile($tmpfilename, 4);

    if (empty($data)) { 

    	$arrContextOptions = array (
    		'ssl' => array (
        	'verify_peer' => false,
        	'verify_peer_name' => false
    		)
    	);  

      $data = file_get_contents('https://www.advertising.de/feed.xml', false, stream_context_create($arrContextOptions));
      $cache->writeFile($tmpfilename, $data);
    }

    $xml = simplexml_load_string($data);

    $items = $xml->channel->item;

    $c = 0;
    foreach ($items as $item) {

      if ($c >= 20) {
        break;
      }

      $c++;

      $title = $item->title;
      $link  = $item->link;
      $date  = strtotime($item->pubDate);
      $desc  = $item->description;

      $day = date('d', $date);
      $month = date('M', $date);
      $month_m = date('m', $date);

      if ($this->view == 'short') {

        echo '
              <div class="cf-news">
                <span class="title"><a href="'.$link.'" target="_blank">'.$title.'</a></span>
              </div>';

      } else {

        echo '<div class="box-section news with-icons">
                <div class="avatar green"><i class="icon-lightbulb icon-2x"></i></div>
                <div class="news-time">
                  <span>'.$day.'</span> '.$month.'
                </div>
                <div class="news-content">
                  <div class="news-title"><a href="'.$link.'" target="_blank">'.$title.'</a></div>
                  <div class="news-text">
                    '.substr($desc,0,200).' ...
                  </div>
                </div>
              </div>';

      }


    }

  }

}

?>