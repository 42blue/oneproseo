<?php

header("Content-Type: text/html; charset=utf-8");

class searchvolume {

  public function __construct ($env_data)
  {

    $this->env_data = $env_data;

    if (isset($_POST['lang'])) {
      $lang = strip_tags($_POST['lang']);
      $this->lang    = $lang;
      $this->country = $lang;
    } else {
      $this->lang    = 'de';
      $this->country = 'de';
    }

    if (isset($_POST['keywords']) && !empty($_POST['keywords']) && $_POST['monthly'] == 'false') {
      $this->chunkKeywordSetAverage();
    } else if (isset($_POST['keywords']) && !empty($_POST['keywords']) && $_POST['monthly'] == 'true') {
      $this->chunkKeywordSetMonthly();
    }

  }


  private function chunkKeywordSetMonthly () {

    $kw = strip_tags($_POST['keywords']);
    $kw = strip_tags($kw);
    $kw = explode("\n", $kw);
    $kw = array_map('trim', $kw);

    // slice @ 500 keywords
    if (count($kw) > 500) {
      $kw = array_slice($kw, 0, 500);
    }

    $csvhl = array();
    $csvhl = array('Keyword', 'Durchschnittliches Suchvolumen / Monat', 'CPC / Euro', 'Competition');
    for ($i = 1; $i <= 12; $i++) {
      array_push($csvhl, 'SV ' . date("M Y", strtotime( date( 'Y-m-01' )." -$i months")));
    }
    array_push($csvhl, 'Hochsaison');
    
    $csvh[] = $csvhl;
    $csv    = array();

    $adwords_kws = array();
    foreach($kw as $k => $keyword) {
      if (empty($keyword)) {
        continue;
      }
      $keyword = str_replace("'", " ", $keyword);
      $keyword = str_replace('"', ' ', $keyword);
      $keyword = mb_strtolower($keyword);
      $kws[$keyword] = array($keyword, $this->lang, $this->country);
    }

    require_once ($this->env_data['path']. 'oneproseo/includes/svolume.class.php');
    $oSvolume  = new svolume($this->db, $this->env_data);
    $result    = $oSvolume->getAdwordsDataLiveMonthly($kws);

    if (isset($result->error)) {
      $this->overload();
    }

    foreach ($result as $k => $v) {

      $kw   = $v->kw;
      $sv   = number_format($v->sv, 0, ',', '.');
      $cpc  = number_format($v->cpc, 2, ',', '.');
      $comp = number_format($v->comp, 2, ',', '.');

      $adwords_kws[$kw] = $kw;

      $csvf = array($kw, $sv, $cpc, $comp);
      
      $saison = array();
      foreach ($v->monthly as $date => $sv) {

        $saison[date("M", strtotime($date))] = $sv;

        $sv = number_format($sv, 0, ',', '.');
        array_push($csvf, $sv);
      }

      $maxvalue = max($saison);
      $saison_mo = array_search($maxvalue, $saison);

      array_push($csvf, $saison_mo);

      $csv[] = $csvf;

    }

    $csvn = array_merge($csvh, $csv);

    $this->filename = $this->writeCsv($csvn);

    // MERGE MISSING
    foreach ($kws as $key => $keyw) {
      if (!isset($adwords_kws[$key])) {
        $csv[] = array($keyw[0], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      }
    }

    $this->outputMonthly($csv);

  }


  private function outputMonthly ($array) {

    echo '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title">Ergebnisse</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProCampBuilder | Searchvolume Export" data-filepath="' . $this->env_data['csvstore'] . $this->filename . '"></i></li></ul></div><div class="box-content">';
    echo '<table class="table table-normal data-table"><thead><tr><td>Keyword</td><td>&Oslash; Suchvolumen / Monat</td><td>CPC / Euro</td><td>Competition</td>';

    for ($i = 1; $i <= 12; $i++) {
      echo '<td>SV ' . date("M Y", strtotime( date( 'Y-m-01' ) . " -$i months")) . '</td>';
    }

    echo '<td>Hochsaison</td></tr></thead>';

    foreach ($array as $k => $v) {
      echo '<tr>
            <td>'.$v[0].'</td>
            <td data-order="'.str_replace('.', '', $v[1]).'">'.$v[1].'</td>
            <td data-order="'.str_replace(',', '', $v[2]).'">'.$v[2].'</td>
            <td data-order="'.str_replace(',', '', $v[3]).'">'.$v[3].'</td>
            <td data-order="'.str_replace('.', '', $v[4]).'">'.$v[4].'</td>
            <td data-order="'.str_replace('.', '', $v[5]).'">'.$v[5].'</td>
            <td data-order="'.str_replace('.', '', $v[6]).'">'.$v[6].'</td>
            <td data-order="'.str_replace('.', '', $v[7]).'">'.$v[7].'</td>
            <td data-order="'.str_replace('.', '', $v[8]).'">'.$v[8].'</td>
            <td data-order="'.str_replace('.', '', $v[9]).'">'.$v[9].'</td>
            <td data-order="'.str_replace('.', '', $v[10]).'">'.$v[10].'</td>
            <td data-order="'.str_replace('.', '', $v[11]).'">'.$v[11].'</td>
            <td data-order="'.str_replace('.', '', $v[12]).'">'.$v[12].'</td>
            <td data-order="'.str_replace('.', '', $v[13]).'">'.$v[13].'</td>
            <td data-order="'.str_replace('.', '', $v[14]).'">'.$v[14].'</td>
            <td data-order="'.str_replace('.', '', $v[15]).'">'.$v[15].'</td>
            <td>'.$v[16].'</td>
        </tr>';
    }

    echo '</table></div></div></div>';

  }


  private function chunkKeywordSetAverage () {

    $kw = strip_tags($_POST['keywords']);
    $kw = strip_tags($kw);
    $kw = explode("\n", $kw);
    $kw = array_map('trim', $kw);

    // slice @ 500 keywords
    if (count($kw) > 500) {
      $kw = array_slice($kw, 0, 500);
    }

    $csvh   = array();
    $csvh[] = array('Keyword', 'OPI', 'Suchvolumen / Monat', 'CPC / Euro', 'Competition');
    $csv    = array();

    $adwords_kws = array();
    foreach($kw as $k => $keyword) {
      if (empty($keyword)) {
        continue;
      }
      $keyword = str_replace("'", " ", $keyword);
      $keyword = str_replace('"', ' ', $keyword);
      $keyword = mb_strtolower($keyword);
      $kws[$keyword] = array($keyword, $this->lang, $this->country);
    }


    require_once ($this->env_data['path']. 'oneproseo/includes/svolume.class.php');
    $oSvolume  = new svolume($this->db, $this->env_data);
    $result    = $oSvolume->querySV($kws);

        if (isset($result->error)) {
            $this->overload();
        }

    foreach ($result as $k => $v) {

      if ($this->country == $v[6]) {

        $opi  = number_format($v[4], 2, ',', '.');
        $sv   = number_format($v[1], 0, ',', '.');
        $cpc  = number_format($v[2], 2, ',', '.');
        $comp = number_format($v[3], 2, ',', '.');

        $csv[] = array($v[0], $opi, $sv, $cpc, $comp);

        $adwords_kws[$v[0]] = $v[0];

      }

    }

    $csvn = array_merge($csvh, $csv);

    $this->filename = $this->writeCsv($csvn);

    $this->outputAverage($result);

  }


  private function outputAverage ($array) {

    echo '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title">Ergebnisse</span><ul class="box-toolbar"><li><a title="CSV download" class="icon-cloud-download csv-request" href="' . $this->env_data['csvurl'] . $this->filename . '" target="_blank"></a></li><li><i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="OneProSeo | OneProCampBuilder | Searchvolume Export" data-filepath="' . $this->env_data['csvstore'] . $this->filename . '"></i></li></ul></div><div class="box-content">';
    echo '<table class="table table-normal data-table"><thead><tr><td>Keyword</td><td>OPI</td><td>&Oslash; Suchvolumen / Monat</td><td>CPC / Euro</td><td>Competition</td><td>API Status</td></tr></thead>';

    foreach ($array as $k => $v) {
      echo '<tr><td>'.$v[0].'</td>
      <td data-order="'.$v[4].'">'.number_format($v[4], 2, ',', '.').'</td>
      <td data-order="'. $v[1] .'">'.number_format($v[1], 0, ',', '.').'</td>
      <td>'.number_format($v[2], 2, ',', '.').'</td>
      <td>'.number_format($v[3], 2, ',', '.').'</td>
      <td>';

      if ($v[7] == 'api_no_data') {
        echo '<span class="data-tooltip" data-tooltip="Keine Adwords API Daten."><i class="status-error icon-remove-circle"></i></span>';
      } else {
        echo '<span class="data-tooltip" data-tooltip="OK - '.$v[7].' db"><i class="status-success icon-ok-circle"></i></span>';
      }
      echo '</td></tr>';
    }

    echo '</table></div></div></div>';

  }

  private function overload() {
    echo '<div class="col-md-12"><div class="box"><div class="box-header"><span class="title">Fehler</span></div><div class="box-content padded">Aktuell ist die Schnittstelle überlastet.<br />Bitte in ein paar Minuten noch einmal probieren.</div></div></div>';
    exit;
  }

  private function writeCsv ($csv) {
    $filename = 'search-volume-data-'.time().'.csv';
    $fp = fopen($this->env_data['csvstore'] . $filename, 'w');
    foreach ($csv as $k => $line) {
      if ($this->lang != 'jp') {
        $line = $this->convertToWindowsCharset($line);  
      }
      fputcsv($fp, $line);
    }
    fclose($fp);
    return $filename;
  }

  // WINDOWS EXCEL FIX
  private function convertToWindowsCharset($arr) {
    foreach ($arr as $key => $value) {
      $charset =  mb_detect_encoding($value, "UTF-8, ISO-8859-1, ISO-8859-15", true);
      $newarr[] =  mb_convert_encoding($value, "Windows-1252", $charset);
    }
    return $newarr;
  }

}

?>
