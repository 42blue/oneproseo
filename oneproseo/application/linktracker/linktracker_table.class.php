<?php
class linktracker_table 
{
	public function __construct ($env_data)
	{
		require_once "linktracker_init.php";
		$this->env_data = $env_data;
		$this->backlink_table = "`linktracker_backlinks` a";
		
		$this->params['project_id'] = $_REQUEST['project_id'];
		$this->params['datefrom'] = strtotime($_REQUEST['datefrom'] . "00:00:00");
		$this->params['dateto'] = strtotime($_REQUEST['dateto'] . "23:59:59");
		$this->params['statusCode'] = $_REQUEST['statusCode'];
		$this->params['date_format_1'] = "%Y-%m-%d";
		$this->params['date_format_2'] = "%Y%m%d";
		$this->params['date_format_3'] = "%d.%m.%Y %H:%i:%s";
		
		$this->where = " WHERE `customer_id` = %i_project_id AND `timestamp` >= %i_datefrom AND `timestamp` <= %i_dateto";
		if($this->params['statusCode'] != "-1")
		{
			$this->where .= " AND link_status = %i_statusCode";
		}
		
		if($_REQUEST['type'] != "export")
		{
			if($_REQUEST['action'] == "project")
			{
				$this->fetchProjectData();
			}
		}
	}
	
	public function fetchProjectData()
	{
		#onepro_debugDBQueries(1);
		$data = array();
		
		// Paging
		$sLimit = '';
		if (isset($_REQUEST['start']) && $_REQUEST['length'] != '-1') {
			$sLimit = 'LIMIT '. $_REQUEST['start'] .', '. $_REQUEST['length'] ;
		}
		
		// SORTING
		$sOrder = '';
		if ( isset( $_REQUEST['order']) )
		{
			$index = $_REQUEST['order'][0]['column'];
			$direction = $_REQUEST['order'][0]['dir'];
			$columns = $_REQUEST['columns'];
			$cellname = $columns[$index]['data'];
			$sOrder = 'ORDER BY '. $cellname . ' '. $direction;
		}
		
		// FILTER
		$sWhere = $this->where;
		if ($_REQUEST['search']['value'] != '') {
			$sWhere .= ' AND (';
			$sWhere .= 'source LIKE "%'. $_REQUEST['search']['value'] .'%" OR target LIKE "%'. $_REQUEST['search']['value'] .'%"';
			$sWhere .= ')';
		}
		
		$sqlExport = "SELECT
				`source`,
				`target`,
				DATE_FORMAT(from_unixtime(`timestamp`), %s_date_format_3) as found_at,
				IF(`timestamp_checked` > 0, DATE_FORMAT(from_unixtime(`timestamp_checked`), %s_date_format_3), '-') as checked_at,
				target_http_code
			FROM
				" . $this->backlink_table . "
			$sWhere
			$sOrder
			$sLimit";

		$sql = "SELECT
				concat('<a href=\"', `source` ,'\" target=\"_blank\">', substr(`source`, 1, 100),' <i class=\"icon-link\"/></a>') as source,
				concat('<a href=\"', `target` ,'\" target=\"_blank\">', substr(`target`, 1, 100),' <i class=\"icon-link\"/></a>') as target,
				DATE_FORMAT(from_unixtime(`timestamp`), %s_date_format_3) as found_at,
				IF(`timestamp_checked` > 0, DATE_FORMAT(from_unixtime(`timestamp_checked`), %s_date_format_3), '-') as checked_at,
				target_http_code,
				link_text,
				link_nofollow
			FROM
				" . $this->backlink_table . "
			$sWhere
			$sOrder
			$sLimit";
		
		#die($sql);
		$dataExport = OnePro_DB::query($sqlExport, $this->params);
		$data = OnePro_DB::query($sql, $this->params);
		$recordsFiltered = OnePro_DB::count();

		// COUNT ALL
		$sql = "SELECT COUNT(*) FROM " . $this->backlink_table . $this->where;
		OnePro_DB::query($sql, $this->params);
		$count = OnePro_DB::count();

		// OUTPUT
		$output = array ('draw' => $_REQUEST['draw'], 
						 'recordsTotal' => $count,
						 'recordsFiltered' => $recordsFiltered,
						 'data' => $data);
						 
		$outputExport = array ('draw' => $_REQUEST['draw'], 
						 'recordsTotal' => $count,
						 'recordsFiltered' => $recordsFiltered,
						 'data' => $dataExport);

		if($_REQUEST['type'] == "export") return $outputExport;
		else echo json_encode($output);
	}
}
?>