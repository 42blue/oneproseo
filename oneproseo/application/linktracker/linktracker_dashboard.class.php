<?php
class linktracker_dashboard
{
	public function __construct ($env_data)
	{
		require_once "linktracker_init.php";
		$this->env_data = $env_data;
		$this->fetchProjects();
	}

	private function fetchProjects()
	{
		$aCustomers = OnePro_DB::query("SELECT * FROM ruk_project_customers WHERE logfile IS NOT NULL");
		foreach($aCustomers as $key => $aCustomer)
		{
			if ($this->env_data['customer_id'] != NULL && $data['customer_id'] != $this->env_data['customer_id']) {
				continue;
			}
			$aCustomers[$key]['link'] = '<a href="project/'.$aCustomer['id'].'" class="label label-green">anzeigen</a>';
		}
		
		// OUTPUT
		$output = array (
			 'data' => $aCustomers)
		;
		echo json_encode($output);
	}
}
?>