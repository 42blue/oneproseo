<?php
class linktracker_chart 
{
	public function __construct ($env_data)
	{
		require_once "linktracker_init.php";
		$this->env_data = $env_data;
		$this->backlink_table = "`linktracker_backlinks` a";
		
		$this->params = array();
		$this->params['project_id'] = $_REQUEST['project_id'];
		$this->params['datefrom'] = strtotime($_REQUEST['datefrom'] . "00:00:00");
		$this->params['dateto'] = strtotime($_REQUEST['dateto'] . "23:59:59");
		$this->params['statusCode'] = $_REQUEST['statusCode'];
		$this->params['date_format_1'] = "%Y-%m-%d";
		$this->params['date_format_2'] = "%Y%m%d";
		$this->params['date_format_3'] = "%d.%m.%Y %H:%i:%s";
		
		$this->where = " WHERE `customer_id` = %i_project_id AND `timestamp` >= %i_datefrom AND `timestamp` <= %i_dateto";
		if($this->params['statusCode'] != "-1")
		{
			$this->where .= " AND link_status = %i_statusCode";
		}
				
		if($_REQUEST['action'] == "project")
		{
			$this->fetchProjectJson();
		}
	}
	
	private function fetchProjectJson()
	{
		#onepro_debugDBQueries(1);
		$chart = array ();
		
		$chart['backlinks'] = onepro_formatNumber(OnePro_DB::queryOneField("backlinks", "SELECT count(*) as backlinks FROM " . $this->backlink_table . $this->where, $this->params));
		$chart['backlinks_checked'] = onepro_formatNumber(OnePro_DB::queryOneField("backlinks_checked", "SELECT count(*) as backlinks_checked FROM " . $this->backlink_table . $this->where . " AND link_status >= '1'", $this->params));
		$chart['backlinks_notchecked'] = onepro_formatNumber(OnePro_DB::queryOneField("backlinks_notchecked", "SELECT count(*) as backlinks_notchecked FROM " . $this->backlink_table . $this->where . " AND link_status = '0'", $this->params));
		
		$aData = OnePro_DB::query("SELECT count(*) as links, from_unixtime(`timestamp`, %s_date_format_1) as `date`, from_unixtime(`timestamp`, %s_date_format_2) as `sortcolumn` FROM " . $this->backlink_table . $this->where . " GROUP BY `date` ORDER BY `sortcolumn` ASC", $this->params);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) json_encode($chart);
		
		foreach($aData as $aRow)
		{
			$chart['chart'][] = array ('x' => $aRow['date'], 'y' => $aRow['links']);
		}
		
		echo json_encode($chart);
	}
}
?>