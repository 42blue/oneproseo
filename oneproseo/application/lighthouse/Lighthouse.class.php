<?php


namespace OneAdvertising;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
require_once "GeneralUtilities.class.php";
class Lighthouse
{
    /**
     * @var string
     */
    protected $websiteurl;
    protected $websiteurllist;
    protected $websiteurlfile;
    protected $apikey;
    protected $parameters;
    public $apiResponse;


    /**
     * Lighthouse constructor.
     *
     * @param $formPostData
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function __construct($formPostData)
    {
        $this->websiteurl = $formPostData['url'];
        $this->websiteurllist = $formPostData['--sites'];
        $this->apikey = $formPostData['apikey'];
        $this->parameters = $formPostData['parameters'];

        $this->apiResponse = $this->sendApiRequest();
        if (isset($this->apiResponse['response']['requestId'])) {
            //@todo: save data
            $requestId = $this->apiResponse['response']['requestId'];
            $response = serialize($this->apiResponse);
            $request = serialize($formPostData);
            // save data
            GeneralUtilities::saveRequestToDatabase(
                $requestId,
                $request,
                $response,
                $type = 'lighthouse',
                $user = 1,
                $usergroup = 2
            );
        }
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendApiRequest()
    {
        $postDataArray = [
            'form_params' => [
                'url' => $this->websiteurl,
                '--sites' => $this->websiteurllist,
                'apikey' => $this->apikey,
                'parameters' => $this->parameters
            ]
        ];
        $client = new Client();
        $apiResponse = $client->request('POST', Application['apiUrl'] . '/lighthouse', $postDataArray);
        return [
            'response' => json_decode((string)$apiResponse->getBody(), true)
        ];
    }

    /**
     * Gets all crawler data from user or usergroup
     *
     * @param $user
     * @param $usergroup
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getStatus($user, $usergroup)
    {
        $processHashArray = GeneralUtilities::getAllUsersRequestsFromDatabase($user, $usergroup, $type = 'lighthouse');
        if (is_array($processHashArray)) {
            $crawlerStatus = [];
            foreach ($processHashArray as $processHash) {
                if (empty($processHash)) {
                    continue;
                }
                $crawlerStatus[] = [
                    'processid' => $processHash,
                    'response' => self::getCrawlerStatusFromAPI($processHash)
                ];
            }
            return $crawlerStatus;
        }

//         single string request
        return json_encode('No Data found');
    }

    /**
     * @param $processHash
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private static function getCrawlerStatusFromAPI($processHash)
    {
        return file_get_contents('http://api.oneproseo.com' . '/lighthouse/summary/' . $processHash);
    }


    public static function getFileFromAPI($fileWithPath)
    {
        $client = new Client();
        $response = $client->request('GET', Application['apiUrl'] . $fileWithPath);
        $responseArray = (string)$response->getBody()->getContents();
        return $responseArray;
    }
}
