<?php

namespace OneAdvertising;

use PDO;

class GeneralUtilities
{
    private static $dbPdo;

    private static function initDatabase()
    {
        try {
            self::$dbPdo = new \PDO('mysql:host:127.0.0.1;dbname=oneproseo', 'root', 'root');
            self::$dbPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $ex) {
            echo $ex->getTraceAsString();
            die();
        }
    }

    /**
     * Saves request data to database
     *
     * @param $processid
     * @param $request
     * @param $response
     * @param $type
     * @return mixed
     */
    public static function saveRequestToDatabase($processid, $request, $response, $type, $user, $usergroup)
    {
        self::initDatabase();
        $timestamp = time();
        $crdate = $timestamp;

        $insertStatement = self::$dbPdo->prepare(
            'INSERT into oneproseo.oneproseo_screamingfrog (processid, request, response, type, tstamp, crdate, user, usergroup) VALUES (?, ?, ?, ?, ?, ?, ?, ? )'
        );
        return $insertStatement->execute(
            [
                $processid,
                $request,
                $response,
                $type,
                $timestamp,
                $crdate,
                $user,
                $usergroup
            ]
        );
    }

    /**
     * Saves urls to database
     *
     * @param $projectname
     * @param $urls
     * @param $status
     * @return mixed
     */
    public static function saveUrls($projectname, $urls, $status, $user, $usergroup, $id = 0)
    {
        self::initDatabase();
        $timestamp = time();
        $crdate = $timestamp;

        if($id > 0)
        {
            $updateStatement = self::$dbPdo->prepare(
                'UPDATE oneproseo.oneproseo_pagetypes SET urls = ? WHERE uid = ' . $id
            );
            return $updateStatement->execute(
                [
                    $urls
                ]
            );
        }
        else
        {
            $insertStatement = self::$dbPdo->prepare(
                'INSERT into oneproseo.oneproseo_pagetypes (projectname, urls, status, tstamp, crdate, user, usergroup) VALUES (?, ?, ?, ?, ?, ?, ? )'
            );
            return $insertStatement->execute(
                [
                    $projectname,
                    $urls,
                    $status,
                    $timestamp,
                    $crdate,
                    $user,
                    $usergroup
                ]
            );
        }
    }

    /**
     * Update test status
     *
     * @param $status
     */
    public static function updateStatus($id = 0, $status)
    {
        self::initDatabase();
        $timestamp = time();

        if($id > 0)
        {
            if($status == 'completed')
            {
                $updateStatement = self::$dbPdo->prepare(
                    'UPDATE oneproseo.oneproseo_pagetypes SET status = ?, tstamp = ?  WHERE uid = ' . $id
                );
                return $updateStatement->execute(
                    [
                        $status,
                        $timestamp
                    ]
                );
            }
            else
            {
                $updateStatement = self::$dbPdo->prepare(
                    'UPDATE oneproseo.oneproseo_pagetypes SET status = ? WHERE uid = ' . $id
                );
                return $updateStatement->execute(
                    [
                        $status
                    ]
                );
            }

        }
    }

    /**
     * Saves response to database
     *
     * @param $iProjectId
     * @param $sRequestID
     * @param $sUrl
     * @param $sHtmlFile
     * @param $sReport
     * @return mixed
     */
    public static function saveResponse($iProjectId, $sRequestID, $sUrl, $sHtmlFile, $sReport)
    {
        self::initDatabase();
        $timestamp = time();

        $insertStatement = self::$dbPdo->prepare(
            'INSERT into oneproseo.oneproseo_lighthouse (projectid, requestid, url, html_report, result, tstamp) VALUES (?, ?, ?, ?, ?, ? )'
        );

        return $insertStatement->execute(
            [
                $iProjectId,
                $sRequestID,
                $sUrl,
                $sHtmlFile,
                $sReport,
                $timestamp
            ]
        );
    }

    /**
     * Gets all saved request by user
     *
     * @param string $user
     * @param string $usergroup
     * @return mixed
     */
    public static function getAllUsersRequestsFromDatabase($user, $usergroup, $type)
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT processid FROM oneproseo.oneproseo_screamingfrog WHERE (user = ? OR usergroup = ?) AND (hidden = 0 AND deleted = 0) AND type = ?'
        );
        $selectStatement->execute(
            [
                $user,
                $usergroup,
                $type
            ]
        );
        $processHashArray = [];
        while ($row = $selectStatement->fetch()) {
            $processHashArray[] = $row['processid'];
        }
        return $processHashArray;
    }

    /**
     * @param $processid
     * @param $user
     * @param $usergroup
     * @param $type
     * @return mixed
     */
    public static function getProcessDataByProcessid($processid, $user, $usergroup, $type)
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT
                *
            FROM
                oneproseo.oneproseo_screamingfrog
            WHERE
                (processid = ? AND type = ?)
            AND
                (user = ? OR usergroup = ?)
            AND
                (hidden = 0 AND deleted = 0)'
        );
        $selectStatement->execute(
            [
                $processid,
                $type,
                $user,
                $usergroup
            ]
        );
        $result = $selectStatement->fetch();
        return [
            'request' => unserialize($result['request']),
            'crdate' => $result['crdate']
        ];
    }

    /**
     * @return mixed
     */
    public static function getLighthouseProjects()
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT * FROM oneproseo.oneproseo_pagetypes WHERE hidden = 0 AND deleted = 0 ORDER BY uid DESC'
        );
        $selectStatement->execute();

        while ($row = $selectStatement->fetch()) {
            $aProjects[] = $row;
        }
        return $aProjects;
    }

    /**
     * @return mixed
     */
    public static function getDataForLighthouseProject($id)
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT * FROM oneproseo.oneproseo_pagetypes WHERE uid = ? AND hidden = 0 AND deleted = 0'
        );

        $selectStatement->execute(
            [
                $id
            ]
        );

        $result = $selectStatement->fetch();
        return $result;
    }

    /**
     * @return mixed
     */
    public static function getDataForUrl($sUrl)
    {
        self::initDatabase();
        $selectStatement = self::$dbPdo->prepare(
            'SELECT * FROM oneproseo.oneproseo_lighthouse WHERE url = ? ORDER BY id DESC LIMIT 0, 7'
        );
        $selectStatement->execute(
            [
                $sUrl
            ]
        );

        while ($row = $selectStatement->fetch()) {
            $aData[] = $row;
        }
        return $aData;
    }

    /**
     * @return mixed
     */
    public static function getAPIRequestId($sUrlsForAPI)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "api.oneproseo.com/lighthouse",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('--sites' => $sUrlsForAPI,'--html' => '','--use-global' => '','apikey' => '9b022806c4ae93a22c07d7cf6f458dab'),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $obj = json_decode($response);
        $sRequestID = $obj->{'requestId'};

        if(empty($sRequestID))
            return false;

        return $sRequestID;
    }

    /**
     * @return mixed
     */
    public static function getChartsForProject($aProject)
    {
        $aUrls = unserialize($aProject['urls']);
        $iPos = 0;
        $chartData = '';

        $aUrls = array_slice($aUrls, 0, 6); //setze auf 15

        foreach($aUrls as $sUrl)
        {
            $bSaveResult = true;

            $aRes = GeneralUtilities::getDataForUrl($sUrl);

            #print_r($aRes);

            foreach($aRes as $aUrl)
            {
                $sResponse = $aUrl['result'];

                if(empty($sResponse))
                    continue;

                $sResponse = unserialize($sResponse);
                $aResponse = json_decode($sResponse, true);

                //fetch info
                $iResponseID = $aUrl['id'];
                $sRequestId = $aUrl['requestid'];
                $aData[$sUrl][$iResponseID]['requestid'] = $sRequestId;
                $aData[$sUrl][$iResponseID]['url'] = $sUrl;
                $aData[$sUrl][$iResponseID]['fetchTime'] = date('Y-m-d', $aUrl['tstamp']);
                $aData[$sUrl][$iResponseID]['date'] = date("j. M Y", $aUrl['tstamp']);

                //pagespeed scores
                $aData[$sUrl][$iResponseID]['performance_score'] = $aResponse['categories']['performance']['score'] * 100;
                $aData[$sUrl][$iResponseID]['performance_score-color'] = GeneralUtilities::getColorByScore($aResponse['categories']['performance']['score']);

                //web vitals
                $aData[$sUrl][$iResponseID]['largest-contentful-paint'] = $aResponse['audits']['largest-contentful-paint']['score'] * 100;
                $aData[$sUrl][$iResponseID]['largest-contentful-paint-seconds'] = $aResponse['audits']['largest-contentful-paint']['displayValue'];
                $aData[$sUrl][$iResponseID]['largest-contentful-paint-color'] = GeneralUtilities::getColorByScore($aResponse['audits']['largest-contentful-paint']['score']);

                $aData[$sUrl][$iResponseID]['total-blocking-time'] = $aResponse['audits']['total-blocking-time']['score'] * 100;
                $aData[$sUrl][$iResponseID]['total-blocking-time-seconds'] = $aResponse['audits']['total-blocking-time']['displayValue'];
                $aData[$sUrl][$iResponseID]['total-blocking-time-color'] = GeneralUtilities::getColorByScore($aResponse['audits']['total-blocking-time']['score']);

                $aData[$sUrl][$iResponseID]['cumulative-layout-shift'] = $aResponse['audits']['cumulative-layout-shift']['score'] * 100;
                $aData[$sUrl][$iResponseID]['cumulative-layout-shift-seconds'] = $aResponse['audits']['cumulative-layout-shift']['displayValue'];
                $aData[$sUrl][$iResponseID]['cumulative-layout-shift-color'] = GeneralUtilities::getColorByScore($aResponse['audits']['cumulative-layout-shift']['score']);

                //more metrics
                $aData[$sUrl][$iResponseID]['first-contentful-paint'] = $aResponse['audits']['first-contentful-paint']['score'] * 100;
                $aData[$sUrl][$iResponseID]['first-contentful-paint-seconds'] = $aResponse['audits']['first-contentful-paint']['displayValue'];
                $aData[$sUrl][$iResponseID]['first-contentful-paint-color'] = GeneralUtilities::getColorByScore($aResponse['audits']['first-contentful-paint']['score']);

                $aData[$sUrl][$iResponseID]['speed-index'] = $aResponse['audits']['speed-index']['score'] * 100;
                $aData[$sUrl][$iResponseID]['speed-index-seconds'] = $aResponse['audits']['speed-index']['displayValue'];
                $aData[$sUrl][$iResponseID]['speed-index-color'] = GeneralUtilities::getColorByScore($aResponse['audits']['speed-index']['score']);

                $aData[$sUrl][$iResponseID]['interactive'] = $aResponse['audits']['interactive']['score'] * 100;
                $aData[$sUrl][$iResponseID]['interactive-seconds'] = $aResponse['audits']['interactive']['displayValue'];
                $aData[$sUrl][$iResponseID]['interactive-color'] = GeneralUtilities::getColorByScore($aResponse['audits']['interactive']['score']);

                //result url
                if($bSaveResult)
                {
                    $sLastResult = $sRequestId . '/' . $aUrl['html_report'];
                    $bSaveResult = false;
                }
            }

            //create highcharts
            if(is_array($aData[$sUrl]))
            {
                $iPos++;
                $chartData .= GeneralUtilities::getHighchart($aData[$sUrl],$sUrl,$iPos);

                $aData[$sUrl]['html_file'] = $sLastResult;
            }
        }

        /*
        echo '<pre>';
        print_R($aData);
        echo '</pre>';
        */

        $iPos = 1;
        foreach($aData as $url => $test)
        {
            $aHtml[] = '
            <div class="col-lg-6 col-sm-12">
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-clock-o fa-fw"></i>
                        <a href="' . $url . '" target="_blank">' . $url . '</a> 
                        <a href="' . $test['html_file'] . '" style="float: right; color: #59A300;">Letzter Test</a>
                    </h3>
                </div>
                <div class="panel-body">
                    <div id="lightContainer_' . $iPos . '"></div>
                </div>
            </div>
            </div>
            ';

            $iPos++;
        }

        $sOut = '<div class="row">' . implode("", $aHtml) . '</div>';
        $sOut .= '<script>' . $chartData . '</script>';

        return $sOut;
    }

    public static function getColorByScore($sScore)
    {
        //Scores Documentation: https://github.com/GoogleChrome/lighthouse/blob/d2ec9ffbb21de9ad1a0f86ed24575eda32c796f0/docs/scoring.md#how-are-the-scores-weighted
        $sScore = $sScore * 100;

        if($sScore < 50)
            $color = 'danger';
        elseif($sScore < 90)
            $color = 'warning';
        else
            $color = 'success';

        return $color;
    }

    public static function getHighchart($aData,$sUrl,$iPos)
    {
        ksort($aData);
        $finalOut = "";

        foreach($aData as $aPageData)
        {
            $aInfoData['performance_score'][] = '{myData:' . $aPageData['performance_score'] .',y: ' . $aPageData['performance_score'] . '}';
            $aInfoData['largest-contentful-paint'][] = '{myData: "' . $aPageData['largest-contentful-paint-seconds'] .'",y: ' . $aPageData['largest-contentful-paint'] . '}';
            $aInfoData['first-contentful-paint'][] = '{myData: "' . $aPageData['first-contentful-paint-seconds'] .'",y: ' . $aPageData['first-contentful-paint'] . '}';
            $aInfoData['total-blocking-time'][] = '{myData: "' . $aPageData['total-blocking-time-seconds'] .'",y: ' . $aPageData['total-blocking-time'] . '}';
            $aInfoData['speed-index'][] = '{myData: "' . $aPageData['speed-index-seconds'] .'",y: ' . $aPageData['speed-index'] . '}';
            $aInfoData['cumulative-layout-shift'][] = '{myData: "' . $aPageData['cumulative-layout-shift'] .'",y: ' . $aPageData['cumulative-layout-shift'] . '}'; //ignore the seconds metric
            $aInfoData['interactive'][] = '{myData: "' . $aPageData['interactive-seconds'] .'",y: ' . $aPageData['interactive'] . '}';

            $aInfoData['fetchTime'][] = $aPageData['fetchTime'];
        }

        $sOut = "
        Highcharts.chart('lightContainer_" . $iPos . "', {
            title: {
                text: ''
            },
            xAxis: {
                categories: ['" . implode("','",$aInfoData['fetchTime']) . "']
            },
            yAxis: {
                title: {
                    text: 'Score'
                },
                min: 0,
                max: 100
            },
            tooltip: {
                formatter: function() {
                    return this.series.name + ': ' + this.point.myData + '';
                }
            },
            series: [
            {
                type: 'column',
                name: 'Largest Contentful Paint',
                color: '#095F96',
                data: [" . implode(",",$aInfoData['largest-contentful-paint']) . "]
            }, {
                type: 'column',
                name: 'Total Blocking Time',
                color: '#59A300',
                data: [" . implode(",",$aInfoData['total-blocking-time']) . "]
            }, {
                type: 'column',
                name: 'Cumulative Layout Shift',
                color: '#eee',
                data: [" . implode(",",$aInfoData['cumulative-layout-shift']) . "]
            }, {
                type: 'column',
                name: 'First Contentful Paint',
                visible: false,
                data: [" . implode(",",$aInfoData['first-contentful-paint']) . "]
            }, {
                type: 'column',
                name: 'Speed Index',
                visible: false,
                data: [" . implode(",",$aInfoData['speed-index']) . "]
            }, {
                type: 'column',
                name: 'Time to Interactive',
                visible: false,
                data: [" . implode(",",$aInfoData['interactive']) . "]
            },
            {
                type: 'spline',
                name: 'Performance score',
                data: [" . implode(",",$aInfoData['performance_score']) . "],
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'white'
                }
            }]
        });
        ";

        return $sOut;
    }

}
