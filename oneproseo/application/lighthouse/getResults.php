<?php

$iProjectId = $argv[1];
$sRequestId = $argv[2];

//get api results
$i = 0;
while (!getAPIResponse($sRequestId,$iProjectId))
{
    $i++;

    if($i > 360) //more than 1 hour
        continue;

    sleep(10);
}

echo 'done';

function getAPIResponse($sRequestID,$iProjectId)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "api.oneproseo.com/lighthouse/summary/" . $sRequestID,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
    ));

    $response = curl_exec($curl);

    curl_close($curl);

    if(strstr($response, "Summary file not found"))
        return false;


    //connect to DB
    $mysqli = new mysqli("127.0.0.1", "root", "root", "oneproseo");
    /* check connection */
    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        exit();
    }

    //update project status
    $timestamp = time();
    $mysqli->query("UPDATE oneproseo.oneproseo_pagetypes SET status = 'completed', tstamp = '" . $timestamp . "'  WHERE uid = " . $iProjectId);

    //make folder for results
    $sFolder = __DIR__ .'/../../../temp/LighthouseResult/' . $sRequestID;
    mkdir($sFolder, 0775);

    //api response
    $sApiFiles = 'http://api.oneproseo.com/lighthouse/file/' . $sRequestID . '/';
    $obj = json_decode($response);

    foreach($obj as $oTemp)
    {
        $sJsonFile = $oTemp->file;
        $sHtmlFile = $oTemp->html;

        //copy json
        copy($sApiFiles . $sJsonFile, $sFolder . '/' . $sJsonFile);
        //copy html
        copy($sApiFiles . $sHtmlFile, $sFolder . '/' . $sHtmlFile);

        $aFiles[$oTemp->url]['json'] = $sJsonFile;
        $aFiles[$oTemp->url]['html'] = $sHtmlFile;
    }

    //save results to DB
    foreach($aFiles as $key => $value)
    {
        $sReport = file_get_contents($sApiFiles . $value['json']);
        $sReport = serialize($sReport);

        // save data
        saveResponse($mysqli, $iProjectId, $sRequestID, $key, $value['html'], $sReport);
    }

    return true;
}

function saveResponse($mysqli, $iProjectId, $sRequestID, $sUrl, $sHtmlFile, $sReport)
{
    $timestamp = time();

    //save response data
    $insertStatement = $mysqli->prepare(
        'INSERT into oneproseo.oneproseo_lighthouse (projectid, requestid, url, html_report, result, tstamp) VALUES (?, ?, ?, ?, ?, ? )'
    );
    $insertStatement->bind_param("isssss", $iProjectId, $sRequestID, $sUrl, $sHtmlFile, $sReport, $timestamp);
    $insertStatement->execute();
    $insertStatement->close();
}
