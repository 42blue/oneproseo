  <script type="text/javascript">
    var themeColour = 'black';
  </script>
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/css/controlfrog.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/custom/custom-cf.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/js/moment.js"></script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/js/easypiechart.js"></script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/js/gauge.js"></script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/js/chart.js"></script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/js/controlfrog-plugins.js"></script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/controlfrog/js/controlfrog.js"></script>
