<?php

  if(!isset($_POST['filepath'])) {
    exit;
  }

  // SET THE FILENAME / PATH to the SESSION 
  // DRIVE CLIENT Takes it from the SESSION

  $_SESSION['google_drive_filepath'] = $_POST['filepath'];
  $_SESSION['google_drive_filename'] = $_POST['filename'];  

  $arrContextOptions=array(

    "ssl" => array(
      "verify_peer" => false,
      "verify_peer_name" => false
    ),

  );  

  $url = file_get_contents($this->data['domain'] . 'googledrive', false, stream_context_create($arrContextOptions));

  echo $url;

?>