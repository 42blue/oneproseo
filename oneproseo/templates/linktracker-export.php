<?php
require_once "linktracker-init.php";
require_once($this->data['path'] . 'oneproseo/application/linktracker/linktracker_table.class.php');
$linktracker_table = new linktracker_table($this->data);

header("Content-type: text/csv");
header("Pragma: no-cache");
header("Expires: 0");

if($_REQUEST['action'] == "project")
{
	header("Content-Disposition: attachment; filename=linktracker_project_export.csv");
	echo '"Link-Quelle","Link-Ziel","Ziel Status-Code","Erfasst am","Geprüft am"' . "\n";
	
	$data = $linktracker_table->fetchProjectData();
	if(is_array($data['data']) && count($data['data']) > 0)
	{
		foreach($data['data'] as $row)
		{
			echo '"' . addslashes($row['source']) . '","' . addslashes($row['target']) . '","' . $row['target_http_code'] . '","' . $row['found_at'] . '","' . $row['checked_at'] . '"' . "\n";
		}
	}
}