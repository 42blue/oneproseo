    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              <a href="<?php echo $this->data['domain'] ?>crawler/dashboard"> <?php echo $this->data['ops_menu']['crawler'] ?></a>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Dashboard
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">
      <div class="row-fluid">

        <div class="span6">
          <div class="box">
            <div class="box-header">
              <span class="title">Aktive Crawls</span>
            </div>
            <div class="box-content" id="ops_crawls_active">
               <i class="oneproseo-preloader"></i>
            </div>
          </div>
        </div>
        <div class="span6">
          <div class="box">
            <div class="box-header">
              <span class="title">Abgeschlossene Crawls</span>
            </div>
            <div class="box-content" id="ops_crawls_resolved">
              <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </div>
