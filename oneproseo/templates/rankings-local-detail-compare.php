    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/dashboard"><span class="breadcrumb-label"> <?php echo $this->data['ops_menu']['rankings'] ?></span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>"><span class="breadcrumb-label"> Projekt </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/local/<?php echo $id ?>"><span class="breadcrumb-label"> Local Rankings </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label"> Keyword Datumsvergleich</span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_project" data-setid="<?php echo $set_id ?>" data-projectid="<?php echo $id ?>">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Datumsvergleich</span>
            </div>
            <div id="ops_loading" class="box-content padded">


              <?php

                if (!isset($startdate)) {$startdate = '01.01.2017';}

                $yesterday = date('d.m.Y', strtotime('-1 days'));

                // Convert to timestamp
                $start_ts = strtotime('01.11.2015');
                $end_ts   = strtotime($yesterday);
                $user_ts  = strtotime($startdate);

                // Check that user date is between start & end
                if (($user_ts >= $start_ts) && ($user_ts <= $end_ts)) {
                  $startdate_out = $startdate;
                } else {
                  $startdate_out = '01.11.2015';
                }

              ?>

              <form class="form-horizontal fill-up" role="form" id="ops_date_compare">
                <div class="form-group">
                  <label for="name" class="col-sm-2 label-1 control-label">Datum 1:</label>
                  <div class="col-sm-8">
                    <input class="datepicker_ruk fill-up" required="required" type="text" name="date1" placeholder="Frühestes Datum: 01.11.2015" value="<?php echo $startdate_out ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 label-1 control-label">Datum 2:</label>
                  <div class="col-sm-8">
                    <input class="datepicker_ruk fill-up" required="required" type="text" name="date2" placeholder="Spätestes Datum: <?php echo $yesterday ?>" value="<?php echo $yesterday ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 label-1 control-label">Art:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="distribution" value="0" class="icheck" id="iradio3" checked="checked">
                      <label for="iradio3">Detailrankings anzeigen</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="distribution" value="1" class="icheck" id="iradio4">
                      <label for="iradio4">Rankingverteilung anzeigen</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_new_project_submit">vergleichen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>

        </div>

      </div>

    </div>

  </div>
