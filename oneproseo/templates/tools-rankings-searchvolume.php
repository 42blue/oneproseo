<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i><?php echo $headline ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['tools'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               </i><?php echo $headline ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12" id="ops_add_block">

          <div class="box">
            <div class="box-header">
              <span class="title">Rankings / Suchvolumen Tool</span>
            </div>
            <div class="box-content padded">
              <ul class="fixul">
                <li>für alle Ranking und Suchvolumen Abfragen die mehr als 100 Keywords beeinhalten</li>
                <li>die Abfrage ist auf maximal 5000 Keywords beschränkt</li>
                <li>die Daten werden nicht live ermittelt, es kann (je nach Menge der Keywords) bis zu zwei Stunden dauern bis die Daten verfügbar sind</li>
                <li>sobald die Daten vorhanden sind, bekommst du eine Email</li>
              </ul>
            </div>
          </div>

          <div class="box">
            <div class="box-header">
              <span class="title">Job erstellen</span>
            </div>
            <div class="box-content padded" id="ops_add_block">

              <form class="form-horizontal fill-up" role="form" id="ops_rasv">

                <div class="form-group">
                  <label for="ops_kolibri_url" class="col-sm-2 label-1 control-label">Name des Jobs:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_jobname" name="jobname" placeholder="Bezeichnung" required="required" value="" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">Keywords:</label>
                  <div class="col-sm-8">
                    <textarea class="searchvolume" id="ops_rasv_keywords" name="keywords" placeholder="Ein Keyword pro Zeile!" required="required"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Umfang:</label>
                  <div class="col-sm-8">
                   <div class="crawl-input-radio">
                     <input type="radio" name="ops_type" value="ra" class="icheck" id="iradio1">
                     <label for="iradio1">Nur Rankings</label>
                   </div>
                   <div class="crawl-input-radio">
                     <input type="radio" name="ops_type" value="sv" class="icheck" id="iradio2" checked="checked">
                     <label for="iradio2">Nur Suchvolumen</label>
                   </div>
                   <div class="crawl-input-radio">
                     <input type="radio" name="ops_type" value="rasv" class="icheck" id="iradio3">
                     <label for="iradio3">Rankings und Suchvolumen</label>
                   </div>
                  </div>
                </div>                

                <div id="ops_rasv_toggle" style="display: none;">
  
                  <div class="form-group">
                    <label for="ops_kolibri_url" class="col-sm-2 label-1 control-label">Domain:</label>
                    <div class="col-sm-8">
                      <input type="url" class="form-control" id="ops_rasv_url" name="url" placeholder="URL" />
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label">Google Version:</label>
                    <div class="col-sm-8">
                      <select class="uniform" name="type" id="ops_rasv_type">
                        <option value="desktop">Google Desktop</option>
                        <option value="mobile">Google Mobile</option>
                      </select>
                    </div>
                  </div>

                </div>

                <div class="form-group">
                  <label for="lang" class="col-sm-2 control-label">Land:</label>
                  <div class="col-sm-8">
                    <select id="ops_liverankings_lang" class="uniform" name="lang" id="ops_rasv_lang">
                      <option value="de">Germany</option>                      
                      <option value="al">Albania</option>
                      <option value="dz">Algeria</option>
                      <option value="ar">Argentina</option>
                      <option value="am">Armenia</option>
                      <option value="au">Australia</option>
                      <option value="at">Austria</option>
                      <option value="az">Azerbaijan</option>
                      <option value="bs">Bahamas</option>
                      <option value="bh">Bahrain</option>
                      <option value="bd">Bangladesh</option>
                      <option value="by">Belarus</option>
                      <option value="be">Belgium (FR)</option>
                      <option value="be-nl">Belgium (NL)</option>                      
                      <option value="bo">Bolivia</option>
                      <option value="ba">Bosnia and Herzegovina</option>
                      <option value="br">Brazil</option>
                      <option value="bn">Brunei Darussalam</option>
                      <option value="bg">Bulgaria</option>
                      <option value="kh">Cambodia</option>
                      <option value="ca">Canada (EN)</option>
                      <option value="ca-fr">Canada (FR)</option>
                      <option value="cl">Chile</option>
                      <option value="cn">China</option>
                      <option value="co">Colombia</option>
                      <option value="cr">Costa Rica</option>
                      <option value="hr">Croatia</option>
                      <option value="cy">Cyprus</option>
                      <option value="cz">Czechia</option>
                      <option value="dk">Denmark</option>
                      <option value="do">Dominican Republic</option>
                      <option value="ec">Ecuador</option>
                      <option value="eg">Egypt</option>
                      <option value="sv">El Salvador</option>
                      <option value="ee">Estonia</option>
                      <option value="fi">Finland</option>
                      <option value="fr">France</option>
                      <option value="fr">French Antilles</option>
                      <option value="ge">Georgia</option>
                      <option value="gr">Greece</option>
                      <option value="gt">Guatemala</option>
                      <option value="ht">Haiti</option>
                      <option value="hn">Honduras</option>
                      <option value="hk">Hong Kong</option>
                      <option value="hu">Hungary</option>
                      <option value="is">Iceland</option>
                      <option value="in">India</option>
                      <option value="id">Indonesia</option>
                      <option value="iq">Iraq</option>
                      <option value="ie">Ireland</option>
                      <option value="il">Israel</option>
                      <option value="it">Italy</option>
                      <option value="jm">Jamaica</option>
                      <option value="jp">Japan</option>
                      <option value="jo">Jordan</option>
                      <option value="kz">Kazakhstan</option>
                      <option value="kw">Kuwait</option>
                      <option value="la">Lao</option>
                      <option value="lv">Latvia</option>
                      <option value="lb">Lebanon</option>
                      <option value="ly">Libya</option>
                      <option value="lt">Lithuania</option>
                      <option value="lu-de">Luxemburg (DE)</option>
                      <option value="lu">Luxemburg (FR)</option>
                      <option value="mk">Macedonia</option>
                      <option value="mw">Malawi</option>
                      <option value="my">Malaysia</option>
                      <option value="mt">Malta</option>
                      <option value="mu">Mauritius</option>
                      <option value="md">Moldova</option>
                      <option value="ma">Morocco</option>
                      <option value="nl">Netherlands</option>
                      <option value="nz">New Zealand</option>
                      <option value="ni">Nicaragua</option>
                      <option value="no">Norway</option>
                      <option value="om">Oman</option>
                      <option value="pk">Pakistan</option>
                      <option value="ps">Palestinian Territory</option>
                      <option value="pa">Panama</option>
                      <option value="py">Paraguay</option>
                      <option value="pe">Peru</option>
                      <option value="ph">Philippines</option>
                      <option value="pl">Poland</option>
                      <option value="pt">Portugal</option>
                      <option value="qa">Qatar</option>
                      <option value="re">Reunion</option>
                      <option value="ro">Romania</option>
                      <option value="ru">Russia</option>
                      <option value="sa">Saudi Arabia</option>
                      <option value="rs">Serbia</option>
                      <option value="sg">Singapore</option>
                      <option value="si">Slovenia</option>
                      <option value="sk">Slowakia</option>
                      <option value="za">South Africa</option>
                      <option value="kr">South Korea</option>
                      <option value="es">Spain</option>
                      <option value="lk">Sri Lanka</option>
                      <option value="se">Sweden</option>
                      <option value="ch">Switzerland (DE)</option>
                      <option value="ch-fr">Switzerland (FR)</option>
                      <option value="ch-it">Switzerland (IT)</option>
                      <option value="pf">Tahiti</option>
                      <option value="tw">Taiwan</option>
                      <option value="th">Thailand</option>
                      <option value="tt">Trinidad and Tobago</option>
                      <option value="tn">Tunisia</option>
                      <option value="tr">Turkey</option>
                      <option value="ua">Ukraine</option>
                      <option value="ae">United Arab Emirates</option>
                      <option value="uk">United Kingdom</option>
                      <option value="uy">Uruguay</option>
                      <option value="us">USA</option>
                      <option value="vn">Vietnam</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_liverankings_submit">Job erstellen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>


        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Laufende und abgeschlossene Jobs</span>
            </div>
            <div class="box-content" id="ops_show_block"></div>
          </div>
        </div>
      </div>

    </div>

  </div>
