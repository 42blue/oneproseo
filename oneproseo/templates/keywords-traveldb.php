<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i><?php echo $headline ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['analytics'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               </i><?php echo $headline ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

     <div class="row">
        <div class="col-md-12">
          <div class="box" id="ops_keyword_db">
            <div class="box-header">
              <span class="title">Travel DB</span>
            </div>
            <div class="box-content">
            <div id="processingIndicator"> <i class="icon-spinner icon-2x icon-spin"></i><br />loading</div>
              <table class="table table-normal data-table">
                <thead>
                  <tr>
                    <th>Kunde</th>
                    <th>Keyword</th>
                    <th>OPI</th>
                    <th>&Oslash; Position</th>
                    <th>Impressions</th>
                    <th>Clicks</th>
                    <th>CTR</th>
                    <th>Rank (today)</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>