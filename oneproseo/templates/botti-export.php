<?php
require_once "botti-init.php";
require_once($this->data['path'] . 'oneproseo/application/botti/botti_table.class.php');
$botti_table = new botti_table($this->data);

header("Content-type: text/csv");
header("Pragma: no-cache");
header("Expires: 0");

if($_REQUEST['action'] == "project")
{
	header("Content-Disposition: attachment; filename=botti_project_export.csv");
	echo '"User-Agent","Bot","Zugriffe"' . "\n";
	
	$data = $botti_table->fetchProjectData();
	if(is_array($data['data']) && count($data['data']) > 0)
	{
		foreach($data['data'] as $row)
		{
			echo '"' . $row['useragent'] . '","' . $row['bot'] . '","' . $row['zugriffe'] . '"' . "\n";
		}
	}
}
else if($_REQUEST['action'] == "bot")
{
	header("Content-Disposition: attachment; filename=botti_bot_export.csv");
	echo '"Url","Zugriffe"' . "\n";
	
	$data = $botti_table->fetchBotData();
	if(is_array($data['data']) && count($data['data']) > 0)
	{
		foreach($data['data'] as $row)
		{
			echo '"' . $row['url'] . '","' . $row['zugriffe'] . '"' . "\n";
		}
	}
}
else if($_REQUEST['action'] == "url")
{
	header("Content-Disposition: attachment; filename=botti_url_export.csv");
	echo '"Url","Zeitstempel","Status","Antwortzeit (in Millisekunden)","Location (bei Redirects)"' . "\n";
	
	$data = $botti_table->fetchUrlData();
	if(is_array($data['data']) && count($data['data']) > 0)
	{
		foreach($data['data'] as $row)
		{
			echo '"' . $row['url'] . '","' . $row['zeitstempel'] . '","' . $row['status'] . '","' . $row['response_time'] . '","' . $row['location'] . '"' . "\n";
		}
	}
}
