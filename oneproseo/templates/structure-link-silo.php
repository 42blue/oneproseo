<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i> Link Silo Tool</h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['structure'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Link Silo Tool
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row" id="ops_silo_hint">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Link Silo Tool?</span>
            </div>
            <div class="box-content padded">
              <ul class="fixul">
                <li>Einfach das <b>Keyword (1)</b> eingeben, dann die <b>Domain (2)</b> auf die das Keyword geprüft werden soll.</li>
                <li>Dann werden die relevantesten URLs ermittelt.</li>
                <li>Jetzt die Keyword optimierte URL wählen und die die Datenbankabfrage starten.</li>
                <li>Das OneProSeo Link Silo Tool ermittelt jetzt alle relevanten Keywords aus der <a target="_blank" href="../keywords/keyword-db">One Keyword Datenbank</a>,<br />prüft die Rankings für alle Keywords auf die <b>Domain (2)</b>, die eingegeben wurde. </li>
                <li>Dann prüft unsere OneProCrawler in Realtime, ob von der rankenden URL ein Link zu der eingebenen <b>Keyword optimierte URL (3)</b> besteht.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="row" id="ops_start_block">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Link Silo Tool!</span>
            </div>
            <div class="box-content padded">

              <form class="form-horizontal fill-up" role="form" id="ops_silo_form">
                <div class="form-group">
                  <label for="ops_kolibri_kw" class="col-sm-2 label-1 control-label">(1) Keyword:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_silo_kw" name="kw" placeholder="z.B. mallorca reisen" required="required" value="" />
                  </div>
                </div>
                <div class="form-group" id="ops_kolibri_toggle">
                  <label for="ops_kolibri_url" class="col-sm-2 label-1 control-label">(2) Domain:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_silo_url" name="url" placeholder="z.B. domain.de" required="required" value="" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_silo_submit">weiter</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_loading_block">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Ergebnisse</span>
            </div>
            <div class="box-content padded jsonloading">
              <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
              <p class="jsonloading" id="ops_poll_info"><span class="jsonloading">Die Analyse kann mehrere Minuten dauern.</span></p>
              <button id="ops_abort_xhr" class="btn label-red-super jsonloading">Anfrage abbrechen</button>
            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_result_block"></div>

    </div>

  </div>
