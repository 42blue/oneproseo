<!doctype html>
<html>
<head>

<?php

if ($this->data['path_info'] == '/indexwatch/seo') {
  $ogdesc = 'OnePro SEO Agenturen Ranking 2015 / SEO / Agenturen Täglicher Google Ranking Check der TOP 100 SEO Agenturen in Deutschland nach OneDex. Welche Keywords fehlen? Welche Agentur fehlt? Bitte Kommentieren (-:';
} else {
  $ogdesc = $description;
}

?>

  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0" />
  <meta name="google" content="notranslate">
  <meta name="mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-title" content="OneProSeo" />
  <meta name="apple-mobile-web-app-status-bar-style" content="black" />
  <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
  <title><?php echo $titletag ?></title>
  <meta name="description" content="<?php echo $description ?>" />
  <meta property="og:title" content="<?php echo $titletag ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="<?php echo $this->data['domain'] . ltrim($this->data['path_info'], '/') ?>" />
  <meta property="og:image" content="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/oneproseo-indexwatch.png" />
  <meta property="og:site_name" content="OneProSeo.com/Tools" />
  <meta property="og:description" content="<?php echo $ogdesc ?>" />
  <meta name="twitter:card" content="summary">
  <meta name="twitter:url" content="<?php echo $this->data['domain'] . ltrim($this->data['path_info'], '/')?>">
  <meta name="twitter:title" content="<?php echo $titletag ?>">
  <meta name="twitter:description" content="<?php echo $ogdesc ?>">
  <meta name="twitter:image" content="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/oneproseo-indexwatch.png">
  <link rel="icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-57.png" sizes="57x57" />
  <link rel="apple-touch-icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-76.png" sizes="76x76" />
  <link rel="apple-touch-icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-114.png" sizes="114x114" />
  <link rel="apple-touch-icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-120.png" sizes="120x120" />
  <link rel="apple-touch-icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-144.png" sizes="144x144" />
  <link rel="apple-touch-icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-152.png" sizes="152x152" />
  <link rel="shortcut icon" href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/icon-196.png" sizes="196x196" />
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/apple-startup-320x460.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image" />
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/apple-startup-640x920.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/apple-startup-640x1096.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" />
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/stylesheets/application.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?php echo $this->data['domain'] ?>oneproseo/src/custom/custom.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/custom/config.js" type="text/javascript"></script>
  <script type="text/javascript">
    OPS.base.www = '<?php echo $this->data['domain'] ?>';
  </script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/javascripts/application.js" type="text/javascript"></script>
  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/custom/<?php echo $javascript ?>" type="text/javascript"></script>
</head>
<body>
  <!-- HEADER -->
  <nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">
    <div class="navbar-header">
      <a class="navbar-brand" href="http://www.oneproseo.com/">
        <img src="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/one-pro-seo-logo.svg" alt="OneProSeo - Desktop" class="hidden-xs hidden-sm hidden-md logo-big" />
        <img src="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/one-pro-seo-logo-smallscreen.svg" alt="OneProSeo - Mobile" class="hidden-lg logo-small" >
      </a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-primary">
        <span class="sr-only">Toggle Side Navigation</span>
        <i class="icon-align-justify icon-2x smallscreen-menu-icon"></i>
        <i class="icon-remove icon-2x smallscreen-menu-icon hidden"></i>
      </button>
    </div>
  </nav>