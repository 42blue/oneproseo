<?php

class changepassword {

  public function __construct ($data)
  {

   $this->out = '';
   $this->env_data = $data;

   if (isset($_POST['new_pw'])) {
     $this->changePW();
   } else {
     $this->showForm();
   }

  }

  private function changePW ()
  {

    $db     = new db();
    $conn   = $db->mySqlConnect($this->env_data);
    $userid = $_SESSION['userid'];
    $sql    = "SELECT id, email, password, firstname, lastname, login, role FROM onepro_users WHERE id = $userid";
    $query  = $conn->query($sql);

    $user = $query->fetch_array(MYSQLI_ASSOC);

    $new_pw = md5($_POST['new_pw']);
    $old_pw = md5($_POST['old_pw']);

    if ($user['password'] == $old_pw) {
      $sql = "UPDATE onepro_users SET password = '$new_pw' WHERE id = $userid";
      $result = $conn->query($sql);
      $this->out = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Yeah .. das Passwort wurde geändert!</div>';
      $this->showForm();
    } else {
      $this->out = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Das alte Passwort ist falsch!</div>';
      $this->showForm();
    }

  }

  private function showForm ()
  {

    echo '
      <div class="main-content"><div class="container"><div class="row"><div class="area-top clearfix"><div class="pull-left header"><h3 class="title"></i>Passwort ändern</h3></div></div></div></div>
      <!-- BREADCRUMB -->
      <div class="container padded"><div class="row"><div id="breadcrumbs"><div class="breadcrumb-button"><a href="' . $this->data['domain'] . '"><span class="breadcrumb-label"> Home </span></a><span class="breadcrumb-arrow"><span></span></span></div><div class="breadcrumb-button"><span class="breadcrumb-label"><i class="icon-user"></i> Benutzerverwaltung</span><span class="breadcrumb-arrow"><span></span></span></div><div class="breadcrumb-button"><span class="breadcrumb-label"><i class="icon-lock"></i> Passwort ändern</span><span class="breadcrumb-arrow"><span></span></span></div></div></div></div>

      <!-- MAIN CONTENT -->
      <div class="container">
        '. $this->out .'
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header">
                <span class="title">Benutzer: '.$_SESSION['firstname'].' '.$_SESSION['lastname'].' / Email: '.$_SESSION['email'].' </span>
              </div>
              <div class="box-content padded">
                  <form class="form-horizontal fill-up" role="form" action="password" method="post">
                  <div class="form-group">
                    <label for="old_pw" class="col-sm-2 label-1 control-label">Altes Passwort:</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="old_pw" name="old_pw" required="required" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="new_pw" class="col-sm-2 label-1 control-label">Neues Passwort:</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="new_pw" name="new_pw" required="required" value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2 control-label"></div>
                    <div class="col-sm-8">
                      <button type="submit" class="btn btn-primary btn-lg">Passwort ändern</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>';
  }


}

new changepassword($this->data);

?>
