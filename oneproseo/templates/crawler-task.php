
    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              <a href="<?php echo $this->data['domain'] ?>crawler/dashboard"> <?php echo $this->data['ops_menu']['crawler'] ?></a>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Neuer Crawl
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Neuen Crawl starten</span>
            </div>
            <div class="box-content padded">

              <div id="ops_flash_message"></div>

              <form class="form-horizontal fill-up" role="form" id="ops_crawl_task">
                <div class="form-group">
                  <label for="url" class="col-sm-2 label-1 control-label">URL die gecrawlt werden soll:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="url" name="url" required="required" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="getall" class="col-sm-2 label-1 control-label">Seite mit htaccess Login?</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="htaccessusername" name="htaccessusername" placeholder="Benutzername" value="">
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="htaccesspassword" name="htaccesspassword" placeholder="Passwort" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">Nur Seiten in diesen<br />Verzeichnissen crawlen:</label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="include" name="include" placeholder="Nur Seiten in diesen Verzeichnissen crawlen (eine URL pro Zeile)"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">Seiten in diesen<br />Verzeichnissen nicht crawlen:</label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="exclude" name="exclude" placeholder="Seiten in diesen Verzeichnissen nicht crawlen (eine URL pro Zeile)"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">Folgende<br />GET Parameter ignorieren:</label>
                  <div class="col-sm-8">
                    <textarea class="tags" name="removeget" placeholder="Folgende GET Parameter ignorieren"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="getall" class="col-sm-2 label-1 control-label">GET Parameter:</label>
                  <div class="col-sm-8">
                    <div class="checkbox">
                      <input type="checkbox" class="icheck" id="removegetall" name="removegetall" value="true">
                      <label for="removegetall">Keine Seiten mit dynamischen Parametern crawlen z.B. <strong>sid=563894756349875</strong> oder <strong>index.php?id=4</strong></label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="useragent" class="col-sm-2 control-label">User Agent:</label>
                  <div class="col-sm-8">
                    <select class="uniform" name="useragent" id="useragent">
                      <option value="google">Google Bot</option>
                      <option value="bing">Bing Bot</option>
                      <option value="yandex">Yandex Bot</option>
                      <option value="chrome">Desktop Chrome</option>
                      <option value="firefox">Desktop Firefox</option>
                      <option value="mobilesafari">Mobile Safari</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="numbersites" class="col-sm-2 control-label">Anzahl:</label>
                  <div class="col-sm-8">
                    <select class="uniform" id="numbersites" name="numbersites">
                      <option value="200">200 Seiten crawlen (am schnellsten)</option>
                      <option value="1000">1.000 Seiten crawlen</option>
                      <option value="5000">5.000 Seiten crawlen</option>
                      <option value="10000">10.000 Seiten crawlen</option>
                      <option value="50000">50.000 Seiten crawlen</option>
                      <option value="100000">100.000 Seiten crawlen (am langsamsten)</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Crawl Geschwindigkeit:</label>
                  <div class="col-sm-8">
                   <div class="crawl-input-radio">
                     <input type="radio" name="speed" value="moderate" class="icheck" checked="checked" id="iradio1">
                     <label for="iradio1">moderat (ca. 50 Seiten / min.)</label>
                   </div>
                   <div class="crawl-input-radio">
                     <input type="radio" name="speed" value="fast" class="icheck" id="iradio2">
                     <label for="iradio2">schnell (ca. 150 Seiten / min.)</label>
                   </div>
                   <div class="crawl-input-radio">
                     <input type="radio" name="speed" value="zombie" class="icheck" id="iradio3">
                     <label for="iradio2">zombie (ca. 400 Seiten / min.)</label>
                   </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_crawl_submit">Crawl starten</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>

      </div>
    </div>

  </div>