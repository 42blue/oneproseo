<!-- BREADCRUMB -->
<div class="container padded">
    <div class="row">
        <div id="breadcrumbs">
            <div class="breadcrumb-button">
                <a href="<?php
                echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
                <span class="breadcrumb-arrow"><span></span></span>
            </div>
            <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              <a href="<?php
              echo $this->data['domain'] ?>lighthouse/dashboard"> <?php
                  echo $this->data['ops_menu']['lighthouse'] ?></a>
            </span>
                <span class="breadcrumb-arrow"><span></span></span>
            </div>
            <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Dashboard
            </span>
                <span class="breadcrumb-arrow"><span></span></span>
            </div>
        </div>
    </div>
</div>

<!-- MAIN CONTENT -->
<div class="container">

    <div class="col-sm-12">
        <h3>Lighthouse Tests</h3>
        <hr>
    </div>

    <div class="col-sm-12">
        <?php

        //lighthouse projects
        $aProjects = \OneAdvertising\GeneralUtilities::getLighthouseProjects();
        if(!empty($aProjects))
        {
            foreach($aProjects as $aProject)
            {
                $sStatus = $aProject['status'];
                if($sStatus == 'running')
                {
                    $sDisplayStatus = '';
                    $sDisplayButton = 'hide';
                    $sStatus = '<span class="text-danger">' . $sStatus . '</span>';
                }
                else
                {
                    $sDisplayStatus = 'hide';
                    $sDisplayButton = '';
                    $sStatus = '<span class="text-success pr' . $aProject['uid'] . '">done</span>';
                }

                $aTbl[] = '
                <tr>
                  <td scope="col">' . $aProject['projectname'] . '</td>
                  <td scope="col" class="text-center">' . sizeof(unserialize($aProject['urls'])) . '</td>
                  <td scope="col" class="text-center">' . date("j. M Y", $aProject['tstamp']) . '</td>
                  <td scope="col" class="text-center">' . $sStatus . '</td>
                  <td scope="col" class="text-center">
                    <a class="label label-green summarybutton" href="/lighthouse/summary/' . $aProject['uid'] . '">anzeigen</a>
                    
                  </td>
                  <td scope="col" class="text-center">
                  <button class="btn btn-xs btn-default formbutton" type="button"
                            data-select="lighthouse"
                            data-action="form"
                            data-template="/lighthouse/lighthouse_pagetypes.html"
                            data-toggle="collapse"
                            data-target="#formContainer" aria-expanded="false"
                            data-project="' . $aProject['uid'] . '"
                            aria-controls="formContainer">
                        <i class="icon-cog"></i>
                    </button>
                  </td>
                  <td scope="col" class="text-center">
                      <button class="btn btn-xs btn-default startbutton ' . $sDisplayButton . '" type="button"
                                data-select="lighthouse"
                                data-action="/start"
                                data-template="/lighthouse/lighthouse_pagetypes.html"
                                data-toggle="collapse"
                                data-target="#formContainer" aria-expanded="false"
                                data-project="' . $aProject['uid'] . '"
                                aria-controls="formContainer">
                            <i class="icon-play"></i>
                        </button>
                        <div id="processingIndicator" class="' . $sDisplayStatus . '" style="padding: 0;"> <i class="icon-spinner icon-2x icon-spin"></i></div>
                  </td>
                </tr>';
            }

            $content = '
            <div class="box">
            <table class="table table-normal data-table dtoverflow">
              <thead>
                <tr>
                  <td scope="col">Projektname</td>
                  <td scope="col" class="text-center">Seitenanzahl</td>
                  <td scope="col" class="text-center">Letzter Test</td>
                  <td scope="col" class="text-center">Status</td>
                  <td scope="col" class="text-center">Ergebnis</td>
                  <td scope="col" class="text-center">Seiten bearbeiten</td>
                  <td scope="col" class="text-center">Test starten</td>
                </tr>
              </thead>
              <tbody>
                    ' . implode("", $aTbl) . '
              </tbody>
            </table>
            </div>
            
            <style>
            table {display: table !important;}
            .text-center {text-align: center !important;}
            </style>
            ';

            echo $content;
        }

        ?>
    </div>

    <div class="col-sm-12">
        <button class="btn btn-primary btn-lg formbutton" type="button"
                data-select="lighthouse"
                data-action="form"
                data-template="/lighthouse/lighthouse_pagetypes.html"
                data-toggle="collapse"
                data-target="#formContainer" aria-expanded="false"
                aria-controls="formContainer">
            Projekt hinzufügen
        </button>
    </div>


    <br>
    <div id="loaderDiv" style="display: none;">
        <div class="d-flex justify-content-center" style="margin-top:10%;">
            <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <br>
    <div class="collapse" id="formContainer">
        <div class="row">
            <div class="col-sm-12">
                <div id="form"></div>
            </div>
        </div>
    </div>
</div>

