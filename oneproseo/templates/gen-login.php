<?php

  class login
  {

    private $no_ip_block = array (
      'hotz'          => 'hotz',
      'ossiander'     => 'ossiander',
      'peschl'        => 'peschl',
      'mayer'         => 'mayer',
      'toepfer'       => 'toepfer',
      'töpfer'        => 'töpfer',
      'schoenbach'    => 'schoenbach',
      'schönbach'     => 'schönbach',
      'von friderici' => 'von friderici',
      'bock'          => 'bock',
      'schneider'     => 'schneider',
      'spieker'       => 'spieker',
      'riedmann'      => 'riedmann',
      'ibelshaeuser'  => 'ibelshaeuser',
      'ibelshäuser'   => 'ibelshäuser',
      'walcher'       => 'walcher',
      'walter'        => 'walter',      
      'sibiryakova'   => 'sibiryakova',
      'muench'        => 'muench',
			'münch'         => 'münch',
      'dirr'          => 'dirr',
      'kremer'        => 'kremer',
      'göpfert'       => 'göpfert',
      'goepfert'      => 'goepfert',
      'fabritius'     => 'fabritius',
      'horn'          => 'horn',
      'butsch'        => 'butsch',      
      'mellein'       => 'mellein',
      'futterknecht'  => 'futterknecht',
      'gessler'       => 'gessler',
      'geßler'        => 'geßler',
      'prause'        => 'prause',
      'filipov'       => 'filipov',
      'pototzki'      => 'pototzki',
      'schnelle'      => 'schnelle',
      'uebelhoer'     => 'uebelhoer',
      'uebelhör'      => 'uebelhör',      
      'heinz'         => 'heinz',
      'binkert'       => 'binkert',
      'frey'          => 'frey',
      'kunisch'       => 'kunisch',
      'reitmeyer'     => 'reitmeyer',
      'felbermeier'   => 'felbermeier',
      'horch'         => 'horch',
      'dytynko'       => 'dytynko',
      'kostova'       => 'kostova',
      'zepter'        => 'zepter',
      'koehl'         => 'koehl',      
      'köhl'          => 'köhl',            
      'ganchev'       => 'ganchev',
      'wiesner'       => 'wiesner',
      'korbelius'     => 'korbelius'
    );

    private $allowed_ips = array (
      '217.110.209.228' => '217.110.209.228',
      '217.110.209.229' => '217.110.209.229',
      '217.7.249.40'    => '217.7.249.40',
      '217.7.219.178'   => '217.7.219.178',
      '217.7.239.184'   => '217.7.239.184',
      '217.86.168.83'   => '217.86.168.83',
      '80.147.216.160'  => '80.147.216.160',
      '62.245.233.245'  => '62.245.233.245',
      '87.191.165.215'  => '87.191.165.215',
      '46.142.198.134'  => '46.142.198.134',
      '46.128.149.175'  => '46.128.149.175'
    );

    private $header = '<div class="container" id="main-login"><div class="col-md-6 col-md-offset-3"><div class="padded">';
    private $info   = '';
    private $footer = '</div></div></div>';

    public function __construct ($data)
    {

      if (!empty($_POST['password']) && !empty($_POST['username'])) {

        $this->redirect = ltrim ($_SESSION['login_request_uri'],'/');
        $this->data     = $data;

        require_once ($this->data['path']. 'oneproseo/includes/logtofile.class.php');
        $this->logToFile = new logtofile($env_data);

        $db = new db();
        $this->db = $db->mySqlConnect($data);

        $this->username = $this->db->real_escape_string($_POST['username']);
        $this->password = md5($_POST['password']);
        $this->user_ip  = $_SERVER['REMOTE_ADDR'];

        $this->checkLogin();

        echo $this->header . $this->info . $this->footer;

      } else {

        $this->showLogin();

      }

    }

    public function checkLogin () {

      $_SESSION['login'] = FALSE;

      $sql  = "SELECT * FROM onepro_users WHERE email = '$this->username'";
      $res  = $this->db->query($sql);
      $user = $res->fetch_array(MYSQLI_ASSOC);

      $allowed_customer_versions = array();

      // IS CUSTOMER? - GET ALLOWED VERSIONS
      if ($user['customer'] == 1) {
        $cus2user = $user['id'];
        $sql      = "SELECT customer_version FROM oneproseo_customer2user WHERE user_id = $cus2user";
        $res      = $this->db->query($sql);
        while ($row = $res->fetch_assoc()) {
          $allowed_customer_versions[$row['customer_version']] = $row['customer_version'];
        }

      }

      // ADMIN
      if ($this->password === 'a7b491d0486de5b5d66279be2552f8a2' && $this->username === 'bluesky') {

        $_SESSION['userid']    = 59;
        $_SESSION['login']     = TRUE;
        $_SESSION['role']      = 'admin';
        $_SESSION['role2']     = 'admin';
        $_SESSION['firstname'] = 'Matthias';
        $_SESSION['lastname']  = 'Hotz';
        $_SESSION['avatar']    = 'zombie';
        $_SESSION['email']     = 'm.hotz@advertising.de';

        $this->logToFile->writeLogin('LOGIN', 'temp/logs/log_liveranking.txt');

        $this->redirect();

      // OLD CUSTOMER LOGIN
    } else if ($this->password == md5($this->data['customer_password']) && $this->username == $this->data['customer_username']) {

        if ($_POST['username'] == 'fielmann' || $_POST['username'] == 'galeria-index') {
          $_SESSION['role']  = 'admin';
          $_SESSION['role2'] = 'admin';
        } else {
          $_SESSION['role']  = 'readonly';
          $_SESSION['role2'] = 'customer';
        }

        $_SESSION['login']     = TRUE;
        $_SESSION['oldlogin']  = TRUE;
        $_SESSION['customer']  = 'galeria';
        $_SESSION['firstname'] = $this->data['customer_name'];
        $_SESSION['lastname']  = 'Kundenversion';

        $this->logToFile->writeLogin('LOGIN // IP: ' . $this->user_ip, 'temp/logs/log_liveranking.txt');
        $this->redirect();

      // IP BLOCK OVERWRITTEN BY LAST NAME / NOT FOR CUSTOMERS
      } else if ($this->ip_user($user['lastname']) === true) {

        $this->info .= '<div class="alert alert-error"><strong>Your IP does not match the allowed IPs for this application!</div>';
        $this->logToFile->writeLogin('LOGIN - FALSE - IP BLOCK / USER: ' . $this->username , 'temp/logs/log_liveranking.txt');

      } else {

        $this->showLogin();

        // USER DOES NOT EXIST
        if ($user === NULL) {

          $this->info .=  '<div class="alert alert-error"><strong>User does not exist!</div>';
          $this->logToFile->writeLogin('LOGIN - FALSE - USER DOES NOT EXIT / USER: ' . $this->username , 'temp/logs/log_liveranking.txt');

        // USER NOT ACTIVE
        } else if ($user['login'] == 0) {

          $this->info .=  '<div class="alert alert-error"><strong>You are not authorized!</div>';
          $this->logToFile->writeLogin('LOGIN - FALSE - USER NOT AUTHORIZED / USER: ' . $this->username , 'temp/logs/log_liveranking.txt');

        // WRONG PASSWORD
        } else if ($user['password'] !== $this->password) {

          $this->info .=  '<div class="alert alert-error"><strong>Your password is wrong!</div>';
          $this->logToFile->writeLogin('LOGIN - FALSE - WRONG PASSWORD / USER: ' . $this->username , 'temp/logs/log_liveranking.txt');

        // INTERNAL USER PASS THROUGH
        } else if ($user['password'] === $this->password && $user['customer'] == 0) {

           $_SESSION['userid']                     = $user['id'];
           $_SESSION['login']                      = TRUE;
           $_SESSION['firstname']                  = $user['firstname'];
           $_SESSION['lastname']                   = $user['lastname'];
           $_SESSION['role']                       = $user['role'];
           $_SESSION['role2']                      = $user['role2'];
           $_SESSION['email']                      = $user['email'];
           $_SESSION['restrict_customer_versions'] = $user['restrict_customer_versions'];
           $_SESSION['customer']                   = 0;
           $_SESSION['avatar']                     = 'none';

           if (strtolower($user['lastname']) == 'spieker') {
             $_SESSION['avatar']  = 'spieker';
           }

           $this->logToFile->writeLogin('LOGIN', 'temp/logs/log_liveranking.txt');

           $this->redirect();

        // EXTERNAL USER PASS THROUGH
        } else if ($user['password'] === $this->password && $user['customer'] == 1 && isset($allowed_customer_versions[$this->data['customer_id']])) {

          $_SESSION['userid']                     = $user['id'];
          $_SESSION['login']                      = TRUE;
          $_SESSION['firstname']                  = $user['firstname'];
          $_SESSION['lastname']                   = $user['lastname'];
          $_SESSION['role']                       = $user['role'];
          $_SESSION['role2']                      = $user['role2'];
          $_SESSION['email']                      = $user['email'];
          $_SESSION['restrict_customer_versions'] = $user['restrict_customer_versions'];
          $_SESSION['avatar']                     = 'none';
          $_SESSION['customer']                   = 1;

          $this->logToFile->writeLogin('LOGIN', 'temp/logs/log_liveranking.txt');

          $this->redirect();

        } else {

          $this->info .=  '<div class="alert alert-error"><strong>Sorry! - No permisson.</div>';

        }

      }

    }


    public function ip_user ($lastname) {

      if ($this->data['customer_version'] == 0) {
        if (!isset($this->no_ip_block[strtolower($lastname)]) && !isset($this->allowed_ips[$this->user_ip])) {
      // CORONA UPDATE
          return false;
        }
      }


      return false;

    }

    public function redirect () {

      $this->config = OnePro_Config::getInstance();
      $this->slim = \Slim\Slim::getInstance();
      $this->slim->redirect($this->config->get('domain') . $this->redirect);

    }

    public function showLogin () {

      echo '<div class="container" id="main-login">
              <div class="col-md-6 col-md-offset-3">
                <div class="padded">
                  <div class="login box" style="margin-top: 40px;">
                    <div class="box-header">
                      <span class="title">OneProSeo Enterprise</span>
                     </div>
                     <div class="box-content padded">
                       <form action="login" method="post">
                         <div class="input-group addon-left">
                           <span class="input-group-addon">
                             <i class="icon-user icon-2x"></i>
                           </span>
                           <input type="text" name="username" placeholder="Benutzername" value="">
                         </div>
                         <br />
                         <div class="input-group addon-left">
                           <span class="input-group-addon"><i class="icon-key icon-2x"></i></span>
                           <input name="password" type="password" placeholder="Passwort">
                         </div>
                         <br />
                         <div class="input-group addon-left">
                           <input class="btn btn-blue btn-block" type="submit" value="Login">
                         </div>
                      </form>
                    </div>
                  </div>
                  <p class="center"><a href="mailto:support@oneproseo.com">Passwort vergessen?</a></p>
                </div>
              </div>';

    }

 }

new login ($this->data);

?>
