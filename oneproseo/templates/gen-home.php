    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
           <span class="breadcrumb-label"> Home </span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-3">

          <!--big normal buttons-->
          <div class="action-nav-normal">
            <div class="row action-nav-row">
              <div class="col-sm-6 action-nav-button">
                <a href="rankings/dashboard" title="OneProRanker">
                  <i class="icon-signal"></i>
                  <span>OneProRanker</span>
                </a>
              </div>
              <div class="col-sm-6 action-nav-button">
                <a href="crawler/dashboard" title="OneProCrawler">
                  <i class="icon-cogs"></i>
                  <span>OneProCrawler</span>
                </a>
              </div>
            </div>

            <div class="row action-nav-row">
              <div class="col-sm-6 action-nav-button">
                <a href="tools/page-indexing" title="OneProAnalytics">
                  <i class="icon-tasks"></i>
                  <span>OneProAnalytics</span>
                </a>
              </div>
              <div class="col-sm-6 action-nav-button">
                <a href="keywords/keyword-db" title="One Keyword DB">
                  <i class="icon-key"></i>
                  <span>One Keyword DB</span>
                </a>
              </div>
            </div>

            <div class="row action-nav-row">
              <div class="col-sm-6 action-nav-button">
                <a href="rankings/dashboard-rankings" title="Visibility Daily">
                  <i class="icon-camera-retro"></i>
                  <span>Visibility Daily</span>
                </a>
              </div>
              <div class="col-sm-6 action-nav-button">
                <a href="rankings/dashboard-visibility" title="Visibility Weekly">
                  <i class="icon-legal"></i>
                  <span>Visibility Weekly</span>
                </a>
              </div>
            </div>

          </div>

        </div>

        <div class="col-md-9">

          <div class="box">
            <div class="box-header">
              <div class="title">Stats</div>
            </div>

            <div class="box-content padded">
              <div class="row">
                <div class="col-md-4 separate-sections" style="margin-top: 5px;">

                  <div class="row">
                    <div class="col-md-6">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-key"></i></li>
                          <li class="count"><?php echo number_format ($this->data['core_data_metrics'][3], 0, ',', '.') ?></li>
                        </ul>
                        <div class="progress">
                          <div class="progress-bar progress-green tip" title="35%" data-percent="35"></div>
                        </div>
                        <div class="stats-label">Anzahl Keywords</div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-search"></i></li>
                          <li class="count"><?php echo number_format ($this->data['core_data_metrics'][2], 0, ',', '.') ?></li>
                        </ul>
                        <div class="progress">
                          <div class="progress-bar progress-red tip" title="80%" data-percent="80"></div>
                        </div>
                        <div class="stats-label">Überwachte Keywords</div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-signin"></i></li>
                          <li class="count"><?php echo $this->data['core_data_metrics'][5] ?></li>
                        </ul>
                        <div class="progress">
                          <div class="progress-bar progress-blue tip" title="40%" data-percent="40"></div>
                        </div>
                        <div class="stats-label">Keyword Campaigns</div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="dashboard-stats small">
                        <ul class="list-inline">
                          <li class="glyph"><i class="icon-signout"></i></li>
                          <li class="count"><?php echo $this->data['core_data_metrics'][6] ?></li>
                        </ul>
                        <div class="progress">
                          <div class="progress-bar progress-blue tip" title="35%" data-percent="35"></div>
                        </div>
                        <div class="stats-label">URL Campaigns</div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-md-8">
                  <div class="sine-chart" id="xchart-sine"></div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="row">


      </div>
    </div>
  </div>