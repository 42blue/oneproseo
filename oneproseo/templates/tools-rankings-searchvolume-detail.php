<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i><?php echo $headline ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['tools'] ?>
            </span>
            <span class="breadcrumb-arrow"><span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <a href="../rankings-searchvolume"><?php echo $headline ?></a>
            </span>
            <span class="breadcrumb-arrow"><span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Ergebnis
            </span>
            <span class="breadcrumb-arrow"><span>
          </div>          
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_project" data-jobid="<?php echo $id ?>">

      <div class="row">
        <div class="col-md-12" id="ops_show_block">

        </div>
      </div>

    </div>

  </div>
