<!-- BREADCRUMB -->
<div class="container padded">
    <div class="row">
        <div id="breadcrumbs">
            <div class="breadcrumb-button">
                <a href="<?php
                echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
                <span class="breadcrumb-arrow"><span></span></span>
            </div>
            <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              <a href="<?php
              echo $this->data['domain'] ?>seocrawler/dashboard"> <?php
                  echo $this->data['ops_menu']['seocrawler'] ?></a>
            </span>
                <span class="breadcrumb-arrow"><span></span></span>
            </div>
            <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Dashboard
            </span>
                <span class="breadcrumb-arrow"><span></span></span>
            </div>
        </div>
    </div>
</div>

<!-- MAIN CONTENT -->
<div class="container">


    <div id="crawleroptions">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-body box-content padded list-group-item-action">
                    <h5 class="box-title">Crawler</h5>
                    <p class="text-muted">Start a new crawling with Screaming Frog SEO Spider.</p>
                    <p>
                        <button class="btn btn-info formbutton" type="button"
                                data-toggle="collapse"
                                data-select="seocrawler"
                                data-action="form"
                                data-template="/crawler/crawler.html"
                                data-target="#formContainer" aria-expanded="false"
                                aria-controls="formContainer">
                            Add
                        </button>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-body box-content padded list-group-item-action">
                    <h5 class="box-title">Scheduled Crawler</h5>
                    <p class="text-muted">Start a new scheduled Screaming Frog SEO Spider crawling.</p>
                    <p>
                        <button class="btn btn-info formbutton" type="button"
                                data-select="crawler"
                                data-action="form"
                                data-template="/crawler/crawler_scheduler.html"
                                data-toggle="collapse"
                                data-target="#formContainer" aria-expanded="false"
                                aria-controls="formContainer">
                            Add
                        </button>
                    </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box box-body box-content padded list-group-item-action">
                    <h5 class="box-title">Crawler Status</h5>
                    <p>Checks the status of Screaming Frog SEO Spider</p>
                    <p>
                        <button class="btn btn-info formbutton" type="button"
                                data-select="crawler"
                                data-action="status"
                                data-template="/crawler/crawler_status.html"
                                data-toggle="collapse"
                                data-target="#formContainer"
                                aria-expanded="false"
                                aria-controls="formContainer">
                            Show
                        </button>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <br>
    <div id="loaderDiv" style="display: none;">
        <div class="d-flex justify-content-center" style="margin-top:10%;">
            <div class="spinner-border text-secondary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <br>
    <div class="collapse" id="formContainer">
        <div class="row">
            <div class="col-sm-12">
                <div id="form"></div>
            </div>
        </div>
    </div>
</div>
</div>

