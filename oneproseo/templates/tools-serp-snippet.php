<style type="text/css">

#tagmaker {
	padding: 0 20px;
	width: 760px;
}

#tagmaker em {
	font-style: normal;
}

#tagmaker h4 {
	margin-bottom: 10px;
}

#tagmaker .google-desktop-bg {
	width: 620px;
}

#tagmaker-result table {
	width:100%;
}

.description-vmax,
.title-vmax {
	float: right;
	font-size: 8px;
}

.google-serp {
  background: #fff;
  padding: 10px;
  margin-bottom: 5px;
  border: 1px solid #E0E0E0;
}

.bghit {
  background: #DAFFDD;
}

.google-serp h3 {
  font-family: arial,sans-serif;
  color: #1a0dab;
  font-size: 20px;
  font-weight: normal;
  padding: 0;
  margin: 0;
}

.google-serp h3:hover {
  text-decoration: underline;
}

.google-serp a {
  color: #2216ac;
}

.google-serp cite {
  font-family: arial,sans-serif;
  color: #202124;
  display: block;
  font-style: normal;
  font-size: 14px;
  line-height: 16px;
  margin-bottom: 8px;
}

.google-serp cite b {
  font-weight: bold;
  color: #202124;
}

.google-serp em.f {
  color: #808080;
}

.google-serp span {
  font-family: arial,sans-serif;
  color: #545454;
  font-size: small;
  word-wrap: break-word;
  display: block;
  line-height: 1.4;
  font-size: 14px;
  margin-top: 6px;
}

.google-serp small {
  font-size: .6em;
  float: right;
}

.google-serp b {
  color: #6a6a6a;
  font-weight: bold;
}

.google-mobile-serp .osl,
.google-serp .osl {
  font-family: arial,sans-serif;
  font-size: 14px;
  color:#777;
  margin-top: 2px;
  display: none;
}

.google-serp b {
  color: #5f6368;
  font-weight: bold;
}

.osl a {
  color:#1a0dab;
}

.osl a:hover {
  color:#1a0dab;
  text-decoration: underline;
}

.slp {
  font-family: arial,sans-serif;
  font-size: small;
  color: #808080;
  display: none;
  padding-top: 4px;
  font-size: 14px;
}

.date-opt-google,
.mobile-opt-google {
  display: none;
}

.google-mobile-bg {
  background-color: #f2f2f2;
  padding: 10px;
  margin-top: 10px;
}

.google-mobile-serp {
  background-color: #fff;
  box-shadow: 0 1px 2px rgba(0,0,0,0.2);
  border-radius: 2px;
  padding: 15px;
  line-height: 20px;
}

.google-mobile-serp h3 {
  font-family: arial,sans-serif;
  color: #1558d6;
  font-size: 18px;
  padding: 0;
  margin: 0;
  font-weight: normal;
}

.google-mobile-serp a {
  color: #1558d6;
}

.google-mobile-serp cite {
  font-family: arial,sans-serif;
  color: #202124;
  display: block;
  font-size: 12px;
  font-style: normal;
  line-height: 16px;
  margin-bottom: 12px;
}

.google-mobile-serp  b {
  font-weight: normal;
  color: #202124;
}

.google-mobile-serp em.f {
  color: #808080;
}

.google-mobile-serp span {
  font-family: arial,sans-serif;
  font-size: 14px;
  color: #3C4043;
  display: block;
  line-height: 20px;
}

.google-mobile-serp .oxf {
  display: block;
  line-height: 20px;
  margin: 2px -16px 0px -16px;
  padding: 0 16px 11px 16px;
}

.google-mobile-serp .oxf img {
  float: left;
  padding-right: 10px;
}

.google-mobile-serp small {
  font-size: .6em;
  float: right;
}

.pixelinfo {
  font-size: 11px;
  color: #000;
  background: #CECECE;
  padding: 10px;
  margin-top: 5px;
  margin-bottom: 40px;
}

.pixelinfo em {
  font-weight: bold;
}	


small {
	display: inline-block;
	padding-bottom: 10px;
}

#fetchurl {
	display: inline-block;
	float: right;
	color: red;
	cursor: pointer;
	font-size: 80%;
}

</style>

<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i><?php echo $headline ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['tools'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               </i><?php echo $headline ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">

          <div class="box">
            <div class="box-header">
              <span class="title">Google SERP Snippet Tool</span>
            </div>
            <div class="box-content padded">
							
							<div class="jsonld-table-row">
								<div class="jsonld-table-cell" style="border-right: 1px solid grey; padding-right: 20px;">	

									<div class="form-group">

										<div class="form-group jsonld-answerbox">
											<span class="title-vmax">&#126; 500 px left / &#126; 57 chars left</span>  
									    <label class="label-2 control-label" for="title-ipt">Title Tag with the most important keywords</label>
									    <small>The ideal length of the Title Tag is 50 - 60 characters (including spaces).</small>
									    <div>
									      <input id="title-ipt" type="text" name="title-ipt" class="col-sm-12 title-ipt">
									    </div>

									    <br/>
											<span class="description-vmax">&#126; 510 px left / &#126; 160 chars left</span>
									    <label class="label-2 control-label" for="description-ipt">Description of the content</label>
									    <small>The ideal length of the Meta Description is 130 - 160 characters (including spaces).</small>
									    <div>
												<textarea style="height: 100px;" id="description-ipt" class="col-sm-12" name="description-ipt" ></textarea>      
									    </div>

									    <br />

									    <label class="label-2 control-label" for="url-ipt">URL</label>
									    <span id="fetchurl">Fetch data from URL</span>
									    <div>
												<input id="url-ipt" class="col-sm-12" type="text" name="url-ipt">
									    </div>

									    <br />

									    <label class="label-2 control-label" for="keyword-ipt">Main keyword of the URL</label>
									    <div>
									      <input id="keyword-ipt" class="col-sm-12" type="text" name="keyword-ipt">
									    </div>    

									  </div>

									</div>

								</div>
								<div class="jsonld-table-cell">
									<div id="tagmaker">

							          <h4>Desktop - Google SERP Snippet preview</h4>
							          <div class="google-desktop-bg">
							            <div class="vsc google-serp">
							              <cite class="url-opt-google">www.diva-e.com</cite>
							              <h3 class="r"><em class="title-opt-google">diva-e | The Leading Transactional Experience Partner (TXP...</em></h3>
							              <span class="st">
							                <em class="f date-opt-google"> 07.07.2020 - </em>
							                <em class="description-opt-google">diva-e is Germany's leading Transactional Experience Partner (TXP), which creates digital experiences that inspire customers and take your  ...</em>
							              </span>
							            </div>
							          </div>

							          <div class="pixelinfo">
							            Width title: <em id="tt_pixel_with_desktop">0</em> px / 500 px max. | Width description: <em id="md_pixel_with_desktop">0</em> px / 510 px max. | Words Title Tag: <em class="tt_words">0</em> / 5 min. and 15 max. | Words Meta Description: <em class="md_words">0</em> / 14 min. and 30 max.
							          </div>



							          <h4>Mobile - Google SERP Snippet Phone (iPhone, 375 px / portait)</h4>
							          <div class="google-mobile-bg" style="width:395px;">
							            <div class="vsc google-mobile-serp" style="width:360px;">
							              <div class="oxf">
							              	<img src="../oneproseo/src/images/oneproseo/favicon.ico" />
							                <cite class="url-opt-google-mobile">www.diva-e.com</cite>
							                <h3 class="r"><em class="title-opt-google">The Leading Transactional Experience...</em></h3>
							              </div>
							              <span class="st">
							                <em class="f date-opt-google"> 07.07.2020 - </em>
							                <em class="description-opt-google-mobile">diva-e is Germany's leading Transactional Experience Partner (TXP), which creates digital experiences that inspire customers and take your ...</em>
							              </span>
							            </div>
							          </div>
							          <div class="pixelinfo">
							            Number of chars mobile: <em class="md_chars_mobile">0</em> / &#126; 129 max. | Words Title Tag: <em class="tt_words">0</em> / 5 min. and 15 max. | Words Meta Description: <em class="md_words">0</em> / 14 min. and 30 max.
							          </div>


							          <h4>Mobile - Google SERP Snippet Tablet (iPad, 768 px / portait)</h4>
							          <div class="google-mobile-bg" style="width:748px;">
							            <div class="vsc google-mobile-serp" style="width:718px;">
							              <div class="oxf">
							              	<img src="../oneproseo/src/images/oneproseo/favicon.ico" />
							                <cite class="url-opt-google-mobile">www.diva-e.com</cite>
							                <h3 class="r"><em class="title-opt-google">diva-e | The Leading Transactional Experience Partner (TXP ...</em></h3>
							              </div>
							              <span class="st">
							                <em class="f date-opt-google"> 07.07.2020 - </em>
							                <em class="description-opt-google-mobile">diva-e is Germany's leading Transactional Experience Partner (TXP), which creates digital experiences that inspire customers and take your ...</em>
							              </span>       
							            </div>
							          </div>
							          <div class="pixelinfo">
							            Number of chars mobile: <em class="md_chars_mobile">0</em> / &#126; 129 max. | Words Title Tag: <em class="tt_words">0</em> / 5 min. and 15 max. | Words Meta Description: <em class="md_words">0</em> / 14 min. and 30 max.
							          </div>

									</div>	
								</div>	
							</div>
						</div>
					
					</div>
				
				</div>
			
			</div>


      <div class="row">
        <div class="col-md-12">

          <div class="box">
            <div class="box-header">
              <span class="title">HTML Markup</span>
            </div>
            <div class="box-content padded">
            	<div id="tagmaker-result">
			          <table>
			            <tbody>
			              <tr>
			                <td>
			                  Title
			                  <br /><br /><br />
			                  Meta Description
			                  <br /><br /><br />
			                  Canonical
			                </td>
			                <td>
			                  <pre>&lt;title&gt;<span class="title-opt"></span>&lt;/title&gt;</pre>
			                  <pre>&lt;meta name="description" content="<span class="description-opt"></span>"&gt;</pre>
			                  <pre>&lt;link rel="canonical" href="<span class="url-opt"></span>" /&gt;</pre>
			                </td>
			              </tr>
			            </tbody>
			          </table>
            	</div>
            </div>
          </div>
        </div>   

      </div>

      <div id="tagmaker">

        <div class="tagmaker-section">
          <div class="title">
          </div>
        </div>
        <div class="tagmaker-section">
        </div>

        <div class="urlarea">
          <div class="tagmaker-section">
          </div>
          <div class="tagmaker-section">
          </div>
          <div class="tagmaker-section">
          </div>
        </div>

      </div>

        </div>

    </div>

  </div>

</div>
