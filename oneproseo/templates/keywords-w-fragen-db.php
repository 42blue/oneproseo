<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i> One W-Fragen DB</h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['keywords'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               One W-Fragen DB
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Finde alle Fragen zu einem Keyword</span>
            </div>
            <div class="box-content padded">

              <div id="ops_flash_message"></div>

              <form class="form-horizontal fill-up" role="form" id="ops_kolibri_task">
                <div class="form-group">
                  <label for="ops_kolibri_kw" class="col-sm-2 label-1 control-label">Keyword:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_kolibri_kw" name="kw" required="required" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="lang" class="col-sm-2 control-label">Für welche Sprache:</label>
                  <div class="col-sm-8">
                    <select id="ops_kolibri_lang" class="uniform" name="lang">
                      <option value="de">deutsch</option>
                      <option value="en">englisch</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_kolibri_submit">W-Fragen suchen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_loading_block">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Ergebnisse</span>
            </div>
            <div class="box-content padded">
              <i class="oneproseo-preloader"></i>
            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_result_block"></div>

    </div>

  </div>