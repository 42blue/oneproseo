<?php

// multiple folders in route?
$route_pattern = '';
if (is_array($route)) {
  foreach ($route as $key => $part) {
    $route_pattern = $route_pattern . '/' . $part;
  }
} else {
  $route_pattern = '/'.$route;
}

require_once($this->data['path'] . 'oneproseo/application' . $route_pattern . '/' . $file . '.class.php');

new $file($this->data);

?>