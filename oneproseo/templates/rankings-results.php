    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/dashboard"><span class="breadcrumb-label"> <?php echo $this->data['ops_menu']['rankings'] ?></span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>"><span class="breadcrumb-label"> Projekt </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>

<?php if (!empty($location)) { ?>

          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/local/<?php echo $id ?>"><span class="breadcrumb-label"> Local Rankings </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>

<?php } ?>

          <div class="breadcrumb-button">
            <span class="breadcrumb-label"> Top 100 </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_project" data-projectid="<?php echo $id ?>" data-keyword="<?php echo urlencode($keyword) ?>" data-timestamp="<?php echo $timestamp ?>" data-country="<?php echo $country ?>" data-location="<?php echo $location ?>" data-type="<?php echo $type ?>">
      <div class="row-fluid">

        <div class="span6">
          <div class="box">
            <div class="box-header">
              <span class="title">Die Übersicht wird generiert ..</span>
            </div>
            <div class="box-content" id="ops_projects">
              <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
