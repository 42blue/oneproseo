<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i><?php echo $headline ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['tools'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               </i><?php echo $headline ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12" id="ops_add_block">

          <div class="box">
            <div class="box-header">
              <span class="title">Welches JSON-LD Markup möchtest du erstellen?</span>
            </div>
            <div class="box-content padded">

              <form class="form-horizontal fill-up" role="form">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Selection</label>
                  <div class="col-sm-8">
                    <select class="uniform" name="type" id="ops_select_type">
                      <option value="ops_faq">F.A.Q. Markup</option>
                      <option value="ops_video">Video Markup</option>
                    </select>
                  </div>
                </div>

              </form>

            </div>
          </div>
          <div class="box" id="ops_faq">
            <div class="box-header">
              <span class="title">F.A.Q. Markup</span>
            </div>
            <div class="box-content padded">
							<div class="jsonld">
							  <div class="jsonld-table-row">
							     <div class="jsonld-table-cell" style="border-right: 1px solid grey; padding-right: 20px;">
                		<div class="form-group">
		                  <label class="label-2 control-label" for="question">Question:</label>
		                  <div>
		                    <textarea class="col-sm-12" name="question" required="required"></textarea>
		                  </div>
		                </div>
		                <label class="label-2 control-label" for="answer">Answer:</label>
                		<div class="form-group jsonld-answerbox">
		                  <div>
		                    <textarea class="col-sm-12" name="answer" required="required"></textarea>
		                  </div>
		                  <br/>
		                  <label class="label-2 control-label" for="answer_link">Link in answer (optional):</label>
		                  <div>
		                    <input type="url" name="answer_link" class="col-sm-12">
		                  </div>
		                  <br/>
		                  <label class="label-2 control-label" for="answer_link_text">Linktext in answer (optional):</label>
		                  <div>
		                    <input type="url" name="answer_link_text" class="col-sm-12">
		                  </div>
		                </div>
										<button class="btn btn-primary btn-lg" id="ops_jsonld_add_question">+ add another question</button>
							     </div>
							     <div class="jsonld-table-cell">
							     		<button class="btn btn-xs btn-blue" id="ops_copy_faq" style="float:right;">copy to clipboard</button>
							       <pre class="jsonld" id="ops_jsonld_out">
&lt;script type="application/ld+json"&gt;
{
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [
        {
            "@type": "Question",
            "name": "Hello. How are you today?",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "Maybe &lt;a href=\"https://www.diva-e.com/\"&gt;totally&lt;/a&gt; fine?"
            }
        }
    ]
}
&lt;/script&gt;
							      </pre>
							    </div>
							  </div>
							</div>

            </div>
          </div>


          <div class="box" id="ops_video"  style="display: none;">
            <div class="box-header">
              <span class="title">Video Markup</span>
            </div>
            <div class="box-content padded">
							<div class="jsonld">
							  <div class="jsonld-table-row">
							     <div class="jsonld-table-cell" style="border-right: 1px solid grey; padding-right: 20px;">
	                		<div class="form-group">
			                  <label for="video_name" class="label-2 control-label">Name:</label>
			                  <div>
			                    <textarea class="col-sm-12" name="video_name" required="required"></textarea>
			                  </div>
			                </div>
	                		<div class="form-group">		                  
			                  <label for="video_description" class="label-2 control-label">Description:</label>
			                  <div>
			                    <textarea class="col-sm-12" name="video_description" required="required"></textarea>
			                  </div>
			                </div>
			                <div class="form-group">
			                  <label for="video_url" class="label-1 control-label">Thumbnail URL:</label>
			                  <div>
			                    <input type="url" class="form-control" name="video_url" required="required" value="https://" />
			                  </div>
			                </div>
							        <div class="form-group">
							          <label for="video_upload_date" class="label-1 control-label">Upload Date:</label>
							          <div>
							            <input class="datepicker_ruk fill-up" type="text" name="video_upload_date" required="required">
							          </div>
							        </div>
			                <div class="form-group">
							          <label for="name" class="label-1 control-label">Duration:</label>			                	
							        </div>
							        <div class="form-group">							              
			                  <label for="video_minutes" class="col-sm-1 control-label" style="padding:5px;">Minutes</label>
			                  <div class="col-sm-2">
			                    <select class="uniform" name="video_minutes" style="width: 62px;">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
														<option value="16">16</option>
														<option value="17">17</option>
														<option value="18">18</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="21">21</option>
														<option value="22">22</option>
														<option value="23">23</option>
														<option value="24">24</option>
														<option value="25">25</option>
														<option value="26">26</option>
														<option value="27">27</option>
														<option value="28">28</option>
														<option value="29">29</option>
														<option value="30">30</option>
														<option value="31">31</option>
														<option value="32">32</option>
														<option value="33">33</option>
														<option value="34">34</option>
														<option value="35">35</option>
														<option value="36">36</option>
														<option value="37">37</option>
														<option value="38">38</option>
														<option value="39">39</option>
														<option value="40">40</option>
														<option value="41">41</option>
														<option value="42">42</option>
														<option value="43">43</option>
														<option value="44">44</option>
														<option value="45">45</option>
														<option value="46">46</option>
														<option value="47">47</option>
														<option value="48">48</option>
														<option value="49">49</option>
														<option value="50">50</option>
														<option value="51">51</option>
														<option value="52">52</option>
														<option value="53">53</option>
														<option value="54">54</option>
														<option value="55">55</option>
														<option value="56">56</option>
														<option value="57">57</option>
														<option value="58">58</option>
														<option value="59">59</option>
														<option value="60">60</option>
			                    </select>
			                  </div>
			                  <label for="video_seconds" class="col-sm-1 control-label" style="padding:5px;">Seconds</label>
			                  <div class="col-sm-2">
			                    <select class="uniform" name="video_seconds" style="width: 62px;">
														<option value="0">0</option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
														<option value="16">16</option>
														<option value="17">17</option>
														<option value="18">18</option>
														<option value="19">19</option>
														<option value="20">20</option>
														<option value="21">21</option>
														<option value="22">22</option>
														<option value="23">23</option>
														<option value="24">24</option>
														<option value="25">25</option>
														<option value="26">26</option>
														<option value="27">27</option>
														<option value="28">28</option>
														<option value="29">29</option>
														<option value="30">30</option>
														<option value="31">31</option>
														<option value="32">32</option>
														<option value="33">33</option>
														<option value="34">34</option>
														<option value="35">35</option>
														<option value="36">36</option>
														<option value="37">37</option>
														<option value="38">38</option>
														<option value="39">39</option>
														<option value="40">40</option>
														<option value="41">41</option>
														<option value="42">42</option>
														<option value="43">43</option>
														<option value="44">44</option>
														<option value="45">45</option>
														<option value="46">46</option>
														<option value="47">47</option>
														<option value="48">48</option>
														<option value="49">49</option>
														<option value="50">50</option>
														<option value="51">51</option>
														<option value="52">52</option>
														<option value="53">53</option>
														<option value="54">54</option>
														<option value="55">55</option>
														<option value="56">56</option>
														<option value="57">57</option>
														<option value="58">58</option>
														<option value="59">59</option>
														<option value="60">60</option>
			                    </select>
			                  </div>

			                </div>

							     </div>
							     <div class="jsonld-table-cell">
							     		<button class="btn btn-xs btn-blue" id="ops_copy_video" style="float:right;">copy to clipboard</button>							     	
											<pre class="jsonld" id="ops_jsonld_video_out">
&lt;script type="application/ld+json"&gt;
{
  "@context": "https://schema.org",
  "@type": "VideoObject",
   "name": "My nice video.",
   "description": "A video about me and my hobbies.",
   "thumbnailUrl": "https://www.example.de/thumbnail.png",
   "uploadDate": "2020-05-06",
   "duration": "PT0H3M12S"
}
&lt;/script&gt;
							      </pre>
							    </div>
							  </div>
							</div>

            </div>
          </div>
		      <script>
		        $(".datepicker_ruk").datepicker({
		          format: "yyyy-mm-dd",
		          autoclose: true,
		          endDate: "+0d",
		        });
		      </script>
        </div>
      </div>

    </div>

  </div>
