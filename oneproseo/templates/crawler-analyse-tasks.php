
    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              <a href="<?php echo $this->data['domain'] ?>crawler/dashboard"> <?php echo $this->data['ops_menu']['crawler'] ?></a>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              <a href="<?php echo $this->data['domain'] ?>crawler/analyse/<?php echo $crawlid ?>"> Analyse </a>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Einzelanalyse
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_anaylse_head" data-collection="<?php echo $crawlid ?>" data-taskid="<?php echo $taskid ?>">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <ul class="nav nav-tabs nav-tabs-left">
                <li class="active">
                  <a href="#pane1" data-toggle="tab"> 
                    <span>Details / URL</span></a>
                </li>
              </ul>
            </div>

            <div class="box-content">
              <div class="tab-content">
                <div class="tab-pane active" id="pane1">
                   <div id="processingIndicator"> <i class="icon-spinner icon-2x icon-spin"></i><br />loading</div>
                   <table class="table table-normal data-table">
                    <thead>
                      <tr>
                        <td>URL</td>
                        <td>HTTP</td>
                        <td>Title Tag</td>
                        <td>Meta Description</td>
                        <td>H1 Content</td>
                        <td>H2 Content</td>
                        <td>Anzahl Wörter</td>
                        <td>Canonical</td>
                        <td>Links (i/e)</td>
                        <td>Response Time</td>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>

  </div>