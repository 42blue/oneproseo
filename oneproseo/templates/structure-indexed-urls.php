<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i> Indexcheck Google </h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['structure'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Indexcheck Google
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Laufende und abgeschlossene Indexchecks</span>
            </div>
            <div class="box-content" id="ops_show_block"></div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12" id="ops_add_block">

          <div class="box">

            <div class="box-header">
              <ul class="nav nav-tabs nav-tabs-left">
                <li class="active"><a href="#home" data-toggle="tab"> <span>Manuelle Eingabe der URLs</span></a></li>
                <li class=""><a href="#sitemap" data-toggle="tab"> <span>Eingabe mit XML Sitemap</span></a></li>
              </ul>
              <div class="title">Neuen Indexcheck beauftragen</div>
            </div>

            <div class="box-content padded">
              <div class="tab-content">
                <div class="tab-pane active" id="home">
                  <form class="form-horizontal fill-up ops_form" role="form">
                    <div class="form-group">
                      <label for="task" class="col-sm-2 label-1 control-label">Name des Jobs:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="task" placeholder="z.b. Namen des Kunden" required="required" value="" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="urls" class="col-sm-2 label-2 control-label">URLs:</label>
                      <div class="col-sm-8">
                        <textarea class="rankings" name="urls" placeholder="Eine URL pro Zeile" required="required"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2 control-label"></div>
                      <div class="col-sm-8">
                        <button type="submit" class="btn btn-primary btn-lg ops_submit">Indexcheck starten</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="tab-pane" id="sitemap">
                  <form class="form-horizontal fill-up ops_form" role="form">
                    <div class="form-group">
                      <label for="task" class="col-sm-2 label-1 control-label">Name des Jobs:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="task" placeholder="z.b. Namen des Kunden" required="required" value="" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="urls" class="col-sm-2 label-2 control-label">URL der XML Sitemap:</label>
                      <div class="col-sm-8">
                        <input type="url" class="form-control" name="urlxml" placeholder="Vollständige URL der Sitemap" required="required" value="" />
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2 control-label"></div>
                      <div class="col-sm-8">
                        <button type="submit" class="btn btn-primary btn-lg ops_submit">Indexcheck starten</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>

    </div>

  </div>
