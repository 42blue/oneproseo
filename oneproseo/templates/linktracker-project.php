<?php
require_once "linktracker-init.php";

$iProjectID = $id;
$aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = " . $iProjectID);
$aProject['dataFrom'] = onepro_formatDateFromTimestamp(OnePro_DB::queryOneField("timestamp", "SELECT * FROM `botti_" . $iProjectID . "_logs` WHERE `timestamp` != 0 ORDER BY `timestamp` ASC LIMIT 1"));
$aProject['dataTo'] = onepro_formatDateFromTimestamp(OnePro_DB::queryOneField("timestamp", "SELECT * FROM `botti_" . $iProjectID . "_logs` WHERE `timestamp` != 0 ORDER BY `timestamp` DESC LIMIT 1"));

#$aProject['dataFrom'] = "01.01.2015";
#$aProject['dataTo'] = date("d.m.Y");

require_once "linktracker-controlpanel.php";
?>
<div class="main-content">

	<div class="container">
		<div class="row">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title"></i><?php echo $headline ?></h3>
				</div>
			</div>
		</div>
	</div>

	<!-- MAIN CONTENT -->
	<div class="container" id="ops_content" data-projectid="<?php echo $iProjectID ?>">
		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<span class="title">Domain Daten</span>
					</div>
					<div class="box-content">
						<table class="table table-normal">
							<tbody>
								<tr class="status-pending">
									<td class="icon"><i class="icon-bookmark"></i></td>
									<td>Domain:</td>
									<td><span id="ops_url" data-url="<?php echo $aProject['url']; ?>"><?php echo $aProject['url']; ?></span></td>
								</tr>
								<tr class="status-pending">
									<td class="icon"><i class="icon-eye-open"></i></td>
									<td>Verfügbare Daten</td>
									<td><?php echo $aProject['dataFrom'] . " - " . $aProject['dataTo']; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<span class="title">Projekt Daten</span>
					</div>
					<div class="box-content">
						<table class="table table-normal">
							<tbody>
								<tr class="status-pending">
									<td class="icon"><i class="icon-lightbulb"></i></td>
									<td>Projektname:</td>
									<td><?php echo $aProject['name']; ?></td>
								</tr>
								<tr class="status-pending">
									<td class="icon"><i class="icon-user"></i></td>
									<td>Project Manager:</td>
									<td><?php echo $aProject['manager']; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<span class="title">Control Panel</span>
					</div>
					<div id="ops_controlpanel" class="box-content padded">
						<form class="fill-up validatable">
							<div class="row">
								<div class="col-md-2">
									<label class="col-md-12"> Von</label>
									<div class="col-md-12"><input id="datepicker_linktracker_from" class="datepicker_linktracker_from fill-up" required="required" type="text" name="datefrom" value="<?php echo $_GET['datefrom']; ?>"></div>
								</div>

								<div class="col-md-2">
									<label class="col-md-12"> Bis</label>
									<div class="col-md-12"><input id="datepicker_linktracker_to" class="datepicker_linktracker_to fill-up" required="required" type="text" name="dateto" value="<?php echo $_GET['dateto']; ?>"></div>
								</div>

								<div class="col-md-2">
									<label class="col-md-12">Status</label>
									<div class="col-md-12">
										<select id="linktracker_status_code" class="uniform" name="status_code">
											<option value="-1"<?php if($sStatusCode == "-1") echo ' selected="selected"'; ?>>Alle</option>
											<option value="0"<?php if($sStatusCode == "0") echo ' selected="selected"'; ?>>Nicht geprüfte Links</option>
											<option value="1"<?php if($sStatusCode == "1") echo ' selected="selected"'; ?>>Geprüfte Links</option>
											<option value="2"<?php if($sStatusCode == "2") echo ' selected="selected"'; ?>>Gefundene Links</option>
										</select>
									</div>
								</div>

								<div class="col-md-2">
									<div>&nbsp;</div>
									<button type="submit" class="btn btn-blue">Übernehmen</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<div class="title">Linkliste</div>
					</div>

					<div class="box-content padded">
						<div class="row">
							<div class="col-md-8">
								<div id="linktracker_chart"><div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
	              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div></div>
							</div>
							<div class="col-md-4 separate-sections">

								<div class="row">
									<div class="col-md-12">
										<div class="dashboard-stats">
											<ul class="list-inline">
												<li class="glyph"><i class="icon-signal icon-2x"></i></li>
												<li class="count" id="linktracker_count"></li>
											</ul>
											<div class="stats-label">Anzahl neue Links</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6">
										<div class="dashboard-stats small">
											<ul class="list-inline">
												<li class="glyph"</li>
												<li class="count" id="linktracker_checked_count"></li>
											</ul>
											<div class="stats-label">Anzahl geprüfte Links</div>
										</div>
									</div>

									<div class="col-md-6">
										<div class="dashboard-stats small">
											<ul class="list-inline">
												<li class="glyph"</li>
												<li class="count" id="linktracker_notchecked_count"></li>
											</ul>
											<div class="stats-label">Anzahl nicht geprüfte Links</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box" id="ops_bots">
				<div class="box-header">
					<span class="title">Links</span>
					<ul class="box-toolbar">
						<li><a title="CSV download" class="icon-cloud-download csv-request" href="" target="_blank"></a></li>
						<li><i title="In Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="Daily Link Tracker / Project Export <?php echo $aProject['name']; ?>" data-filepath=""></i></li>
					</ul>
				</div>
				<div class="box-content">
					<table class="table table-normal data-table">
						<thead>
						<tr>
							<th>Link-Quelle</th>
							<th>Link-Ziel</th>
							<th>Ziel Status-Code</th>
							<th>Link-Text</th>
							<th>Follow/Nofollow</th>
							<th>Erfasst am</th>
							<th>Geprüft am</th>
						</tr>
						</thead>
					</table>
				</div>
				</div>
			</div>
		</div>
	</div>

</div>
