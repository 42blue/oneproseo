<?php
if ($_SESSION['role2'] == 'nodash') {
  exit;
}
?>

  <div class="cf-container">

    <div class="row"> <!-- 1 ROW -->

      <div class="col-sm-4 cf-item">
        <header>
          <p><span></span><a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>">Customer</a></p>
        </header>

        <div class="col-sm-6 cf-item">
          <div class="content">
            <img class="biglogo" alt="<?php echo $this->data['customer_name'] ?>" src="<?php echo $this->data['domain'] ?>oneproseo/src/images/customer/transparent_logo<?php echo $id ?>.png" />
          </div>
        </div>

        <div class="col-sm-6 cf-item">
          <div class="content">
            <div class="cf-td">
              <div class="cf-td-time metric">12:00</div>
              <div class="cf-td-dd">
                <p class="cf-td-date metric-small">1st October, 2020</p>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="col-sm-4 cf-item">
        <header>
          <p><span></span><a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>">OneDex Daily</a></p>
        </header>
        <div class="content">
          <div class="cf-svmc-sparkline">
            <div class="cf-svmc" id="OPSCF_dex_view">
              <i class="oneproseo-preloader-dark"></i>
            </div>
            <div class="cf-sparkline clearfix">
              <div id="spark-1" class="sparkline-dex"></div>
              <div class="sparkline-value">
                <div class="metric-small"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-4 cf-item">
        <header>
          <p><span></span><a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>">CTR Rate</a></p>
        </header>
        <div class="content" id="OPSCF_figures">
          <i class="oneproseo-preloader-dark"></i>
        </div>
      </div>

    </div> <!-- /1 ROW -->


    <div class="row"> <!-- 2 ROW -->

      <div class="col-sm-4 cf-item cf-small">
        <header>
          <p><span></span><a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>">Rankings (today)</a></p>
        </header>
        <div class="content">
          <div id="cf-rag-1" class="cf-rankings-1 cf-rag"><i class="oneproseo-preloader-dark"></i></div>
        </div>
      </div>

      <div class="col-sm-4 cf-item cf-small">
        <header>
          <p><span></span><a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>">Rankings (1 week ago)</a></p>
        </header>
        <div class="content">
          <div id="cf-rag-2" class="cf-rankings-2 cf-rag"><i class="oneproseo-preloader-dark"></i></div>
        </div>
      </div>

      <div class="col-sm-4 cf-item">
        <header>
          <p><span></span><a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>">Keywords / Rank</a></p>
        </header>
        <div class="content" id="OPSCF_keywords">
          <i class="oneproseo-preloader-dark"></i>
        </div>
      </div>

    </div> <!-- /2 ROW -->

    <div class="row"> <!-- 3 ROW -->

      <div class="col-sm-4 cf-item">
        <header>
          <p><span></span><a target="_blank" href="https://de.sistrix.com/toolbox/overview/domain/<?php echo $this->data['customer_hostname'] ?>">Sistrix Weekly (Desktop)</a></p>
        </header>
        <div class="content">
          <div class="cf-svmc-sparkline">
            <div class="cf-svmc" id="OPSCF_sistrix_view">
              <i class="oneproseo-preloader-dark"></i>
            </div>
            <div class="cf-sparkline clearfix">
              <div id="spark-1" class="sparkline-sistrix"></div>
              <div class="sparkline-value">
                <div class="metric-small"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-sm-4 cf-item">
        <header>
          <p><span></span><a target="_blank" href="https://de.sistrix.com/toolbox/overview/domain/<?php echo $this->data['customer_hostname'] ?>">Sistrix Weekly (Mobile)</a></p>
        </header>
        <div class="content">
          <div class="cf-svmc-sparkline">
            <div class="cf-svmc" id="OPSCF_sistrix_view_mobile">
              <i class="oneproseo-preloader-dark"></i>
            </div>
            <div class="cf-sparkline clearfix">
              <div id="spark-1" class="sparkline-sistrix-mobile"></div>
              <div class="sparkline-value">
                <div class="metric-small"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div> <!-- /3 ROW -->

  </div>
