  <!-- SIDEBAR -->
  <div class="sidebar-background">
    <div class="primary-sidebar-background"></div>
  </div>

  <div class="primary-sidebar">
    <ul class="nav navbar-collapse collapse navbar-collapse-primary">

<?php

  if (stripos($this->data['path_info'], '/crawler/') === 0) {
    $pt_glow = 'active';
    $pt_collapse = 'in';
  } else {
    $pt_glow = '';
    $pt_collapse = '';
  }

  if ($this->data['modules_seocrawler'] == '1') {

    echo '<li class="dark-nav '. $pt_glow .'">
      <span class="glow"></span>
      <a class="accordion-toggle" data-toggle="collapse" href="#seocrawler">
        <i class="icon-cogs icon"></i>
        <span>'. $this->data['ops_menu']['seocrawler'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="crawler" class="collapse '. $pt_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'seocrawler/dashboard"><i class="icon-dashboard"></i> Dashboard </a>
        </li>
      </ul>
    </li>';

  }
//  if ($this->data['modules_crawler'] == '1') {
//
//    echo '<li class="dark-nav '. $pt_glow .'">
//      <span class="glow"></span>
//      <a class="accordion-toggle" data-toggle="collapse" href="#crawler">
//        <i class="icon-cogs icon"></i>
//        <span>'. $this->data['ops_menu']['crawler'] .'<i class="icon-caret-down"></i></span>
//      </a>
//      <ul id="crawler" class="collapse '. $pt_collapse .'">
//        <li>
//          <a href="'. $this->data['domain'] .'crawler/dashboard"><i class="icon-dashboard"></i> Dashboard </a>
//        </li>
//        <li>
//          <a href="'. $this->data['domain']  .'crawler/task"><i class="icon-edit"></i> Neuer Crawl </a>
//        </li>
//        <li>
//          <a href="'. $this->data['domain']  .'crawler/siteclinic"><i class="icon-hospital"></i> Siteclinic </a>
//        </li>
//      </ul>
//    </li>';
//
//  }


  if (stripos($this->data['path_info'], '/rankings/') === 0) {
    $pt_glow = 'active';
    $pt_collapse = 'in';
  } else {
    $pt_glow = '';
    $pt_collapse = '';
  }

  if ($this->data['modules_ranker'] == '1') {

    echo '<li class="dark-nav '. $pt_glow .'">
      <span class="glow"></span>
      <a class="accordion-toggle" data-toggle="collapse" href="#rankings">
        <i class="icon-signal icon"></i>
        <span>'. $this->data['ops_menu']['rankings'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="rankings" class="collapse '. $pt_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'rankings/dashboard"><i class="icon-dashboard"></i> Projektübersicht </a>
        </li>';

    if ($this->data['customer_version'] == 0 || !isset($_SESSION['userid'])) {
      echo
        '<li>
          <a href="'. $this->data['domain'] .'rankings/dashboard-custom"><i class="icon-asterisk"></i> Personal Dashboard </a>
         </li>
         <li>
           <a href="'. $this->data['domain'] .'rankings/live-ranking"><i class="icon-fighter-jet"></i> Live Ranking </a>
         </li>';
    }

  echo '<li>
          <a href="'. $this->data['domain'] .'rankings/dashboard-rankings"><i class="icon-camera-retro"></i> Visibility Daily </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'rankings/dashboard-visibility"><i class="icon-legal"></i> Visibility Weekly </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'rankings/top-rankings"><i class="icon-gift"></i> Top Rankings </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'rankings/projects"><i class="icon-edit"></i> Administration </a>
        </li>
      </ul>
    </li>';

  }

  if (stripos($this->data['path_info'], '/sea-') === 0) {
    $pt_glow = 'active';
    $pt_collapse = 'in';
  } else {
    $pt_glow = '';
    $pt_collapse = '';
  }

  if ($this->data['modules_sea'] == '1') {
    echo '<li class="dark-nav '. $pt_glow .'">
      <span class="glow"></span>
      <a class="accordion-toggle" data-toggle="collapse" href="#sea-seo-balancer">
        <i class="icon-money icon"></i>
        <span>'.  $this->data['ops_menu']['seaseobalancer'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="sea-seo-balancer" class="collapse '. $pt_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'sea-seo-balancer/dashboard"><i class="icon-exchange"></i> SEA/SEO Balancer </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'sea-landingpage-finder"><i class="icon-screenshot"></i> SEA Landingpage Finder </a>
        </li>
      </ul>
    </li>';
  }

  if (stripos($this->data['path_info'], '/keywords/') === 0) {
    $pt_glow = 'active';
    $pt_collapse = 'in';
  } else {
    $pt_glow = '';
    $pt_collapse = '';
  }

  if ($this->data['modules_keyworder'] == '1') {

    echo '<li class="dark-nav '. $pt_glow .'">
      <span class="glow"></span>
      <a class="accordion-toggle" data-toggle="collapse" href="#keywords">
        <i class="icon-key icon"></i>
        <span>'.  $this->data['ops_menu']['keywords'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="keywords" class="collapse '. $pt_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'keywords/url-db"><i class="icon-road"></i> One URL DB</a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'keywords/keyword-db"><i class="icon-search"></i> One Keyword DB</a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'keywords/suggest-db"><i class="icon-bell"></i> One Suggest DB</a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'keywords/w-fragen-db"><i class="icon-twitter"></i> One W-Fragen DB</a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'keywords/searchvolume"><i class="icon-bar-chart"></i> Suchvolumen</a>
        </li>
      </ul>
    </li>';

  }

  if (stripos($this->data['path_info'], '/structure/') === 0) {
    $pt_glow = 'active';
    $pt_collapse = 'in';
  } else {
    $pt_glow = '';
    $pt_collapse = '';
  }

  if ($this->data['modules_structure'] == '1') {

    echo '<li class="dark-nav '. $pt_glow .'">
      <span class="glow"></span>
      <a class="accordion-toggle" data-toggle="collapse" href="#structure">
        <i class="icon-sitemap icon"></i>
        <span>'. $this->data['ops_menu']['structure'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="structure" class="collapse '. $pt_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'structure/link-silo"><i class="icon-link"></i> Link Silo Tool</a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'structure/indexed-urls"><i class="icon-stethoscope"></i> Indexcheck Google</a>
        </li>
      </ul>
    </li>';

  }


  if (stripos($this->data['path_info'], '/tools/') === 0) {
    $pt_glow = 'active';
    $pt_collapse = 'in';
  } else {
    $pt_glow = '';
    $pt_collapse = '';
  }

  if ($this->data['modules_tools'] == '1') {

    echo '<li class="dark-nav '. $pt_glow .'">
      <span class="glow"></span>
      <a class="accordion-toggle" data-toggle="collapse" href="#tools">
        <i class="icon-wrench icon"></i>
        <span>'. $this->data['ops_menu']['tools'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="tools" class="collapse '. $pt_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'tools/rankings-searchvolume"><i class="icon-hdd"></i> Rankings / Suchvolumen </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'tools/search-console"><i class="icon-beaker"></i> Search Console Export </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'tools/structured-data"><i class="icon-pushpin"></i> Mark-Up Generator </a>
        </li>
        <li>
          <a href="'. $this->data['domain'] .'tools/serp-snippet"><i class="icon-list"></i> SERP Snippet </a>
        </li>
      </ul>
    </li>';

  }


  if (stripos($this->data['path_info'], '/admin/') === 0) {
    $pet_glow = 'active';
    $pet_collapse = 'in';
  } else {
    $pet_glow = '';
    $pet_collapse = '';
  }

  if ($this->data['modules_admin'] == '1') {

    echo '<li class="dark-nav '. $pt_glow .'">
      <a class="accordion-toggle" data-toggle="collapse" href="#dev">
        <i class="icon-cogs icon"></i>
        <span>'. $this->data['ops_menu']['admin'] .'<i class="icon-caret-down"></i></span>
      </a>
      <ul id="dev" class="collapse '. $pet_collapse .'">
        <li>
          <a href="'. $this->data['domain'] .'admin/user"><i class="icon-user-md"></i> Benutzerverwaltung </a>
        </li>
      </ul>
    </li>';

  }

?>

    </ul>

    <div class="text-center hidden-xs crawler-pie">
      <?php $perc = $this->data['core_data_metrics'][0] * 100 / $this->data['ruk_max_keywords'] ?>
      <div class="crawler-chart" id="ops_ruk_keywords" data-percent="<?php echo $perc ?>"><span><?php echo number_format ($this->data['core_data_metrics'][0], 0, ',', '.') ?></span><small class="subpie"> von <?php echo number_format ($this->data['ruk_max_keywords'], 0, ',', '.') ?></small></div>
      <div class="subline"><b>Anzahl Keywords</b></div>
    </div>
    <div class="text-center hidden-xs crawler-pie">
      <div class="crawler-chart" id="ops_ruk_urls" data-percent="80"><span><?php echo number_format($this->data['core_data_metrics'][1] / 1000000, 0) ?> Mio.</span></div>
      <div class="subline"><b>Anzahl URLs</b></div>
    </div>

  </div>
