    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/dashboard"><span class="breadcrumb-label"> <?php echo $this->data['ops_menu']['rankings'] ?></span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label"> Neues Projekt</span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <!-- AJAX ALERT & SUCCESS MSG -->
          <div id="ops_flash_message"></div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Neues Projekt</span>
            </div>
            <div class="box-content padded">

              <form class="form-horizontal fill-up" role="form" id="ops_new_project">
                <div class="form-group">
                  <label for="name" class="col-sm-2 label-1 control-label">Projektname:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" name="name" required="required" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="url" class="col-sm-2 label-1 control-label">Domain / URL:</label>
                  <div class="col-sm-8">
                    <input type="url" class="form-control" id="url" name="url" required="required" value="http://">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Subdomains überwachen:</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="ignore_subdomain" value="0" class="icheck" id="iradio3" checked="checked">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="ignore_subdomain" value="1" class="icheck" id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Google Version:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="country">
                      <option value="de">Germany</option>                      
                      <option value="al">Albania</option>
                      <option value="dz">Algeria</option>
                      <option value="ar">Argentina</option>
                      <option value="am">Armenia</option>
                      <option value="au">Australia</option>
                      <option value="at">Austria</option>
                      <option value="az">Azerbaijan</option>
                      <option value="bs">Bahamas</option>
                      <option value="bh">Bahrain</option>
                      <option value="bd">Bangladesh</option>
                      <option value="by">Belarus</option>
                      <option value="be">Belgium (FR)</option>
                      <option value="be-nl">Belgium (NL)</option>
                      <option value="bo">Bolivia</option>
                      <option value="ba">Bosnia and Herzegovina</option>
                      <option value="br">Brazil</option>
                      <option value="bn">Brunei Darussalam</option>
                      <option value="bg">Bulgaria</option>
                      <option value="kh">Cambodia</option>
                      <option value="ca">Canada (EN)</option>
                      <option value="ca-fr">Canada (FR)</option>
                      <option value="cl">Chile</option>
                      <option value="hk">China (HK)</option>
                      <option value="co">Colombia</option>
                      <option value="cr">Costa Rica</option>
                      <option value="hr">Croatia</option>
                      <option value="cy">Cyprus</option>
                      <option value="cz">Czechia</option>
                      <option value="dk">Denmark</option>
                      <option value="do">Dominican Republic</option>
                      <option value="ec">Ecuador</option>
                      <option value="eg">Egypt</option>
                      <option value="sv">El Salvador</option>
                      <option value="ee">Estonia</option>
                      <option value="fi">Finland</option>
                      <option value="fr">France</option>
                      <option value="fr">French Antilles</option>
                      <option value="ge">Georgia</option>
                      <option value="gr">Greece</option>
                      <option value="gt">Guatemala</option>
                      <option value="ht">Haiti</option>
                      <option value="hn">Honduras</option>
                      <option value="hk">Hong Kong</option>
                      <option value="hu">Hungary</option>
                      <option value="is">Iceland</option>
                      <option value="in">India</option>
                      <option value="id">Indonesia</option>
                      <option value="iq">Iraq</option>
                      <option value="ie">Ireland</option>
                      <option value="il">Israel</option>
                      <option value="it">Italy</option>
                      <option value="jm">Jamaica</option>
                      <option value="jp">Japan</option>
                      <option value="jo">Jordan</option>
                      <option value="kz">Kazakhstan</option>
                      <option value="kw">Kuwait</option>
                      <option value="la">Lao</option>
                      <option value="lv">Latvia</option>
                      <option value="lb">Lebanon</option>
                      <option value="ly">Libya</option>
                      <option value="lt">Lithuania</option>
                      <option value="lu">Luxemburg (FR)</option>
                      <option value="lu-de">Luxemburg (DE)</option>                      
                      <option value="mk">Macedonia</option>
                      <option value="mw">Malawi</option>
                      <option value="my">Malaysia</option>
                      <option value="mt">Malta</option>
                      <option value="mu">Mauritius</option>
                      <option value="mx">Mexico</option>                      
                      <option value="md">Moldova</option>
                      <option value="ma">Morocco</option>
                      <option value="nl">Netherlands</option>
                      <option value="nz">New Zealand</option>
                      <option value="ni">Nicaragua</option>
                      <option value="no">Norway</option>
                      <option value="om">Oman</option>
                      <option value="pk">Pakistan</option>
                      <option value="ps">Palestinian Territory</option>
                      <option value="pa">Panama</option>
                      <option value="py">Paraguay</option>
                      <option value="pe">Peru</option>
                      <option value="ph">Philippines</option>
                      <option value="pl">Poland</option>
                      <option value="pt">Portugal</option>
                      <option value="qa">Qatar</option>
                      <option value="re">Reunion</option>
                      <option value="ro">Romania</option>
                      <option value="ru">Russia</option>
                      <option value="sa">Saudi Arabia</option>
                      <option value="rs">Serbia</option>
                      <option value="sg">Singapore</option>
                      <option value="si">Slovenia</option>
                      <option value="sk">Slowakia</option>
                      <option value="za">South Africa</option>
                      <option value="kr">South Korea</option>
                      <option value="es">Spain</option>
                      <option value="lk">Sri Lanka</option>
                      <option value="se">Sweden</option>
                      <option value="ch">Switzerland (DE)</option>
                      <option value="ch-fr">Switzerland (FR)</option>
                      <option value="ch-it">Switzerland (IT)</option>
                      <option value="pf">Tahiti</option>
                      <option value="tw">Taiwan</option>
                      <option value="th">Thailand</option>
                      <option value="tt">Trinidad and Tobago</option>
                      <option value="tn">Tunisia</option>
                      <option value="tr">Turkey</option>
                      <option value="ua">Ukraine</option>
                      <option value="ae">United Arab Emirates</option>
                      <option value="uk">United Kingdom</option>
                      <option value="uy">Uruguay</option>
                      <option value="us">USA</option>
                      <option value="vn">Vietnam</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="manager" class="col-sm-2 label-1 control-label">SEO Project Manager:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="manager" name="manager" required="required" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="team" class="col-sm-2 label-1 control-label">E-Mail Team:</label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="team" name="team" placeholder="Eine Emailadresse pro Zeile!"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="competition" class="col-sm-2 label-1 control-label">Wettbewerb überwachen:</label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="competition" name="competition" placeholder="Eine URL pro Zeile! (max. 8)"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="brandkeywords" class="col-sm-2 label-1 control-label">Brand Keywords:</label>
                  <div class="col-sm-8">
                    <textarea class="crawler" id="brandkeywords" name="brandkeywords" placeholder="Eine Keyword pro Zeile!"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="category" class="col-sm-2 label-1 control-label">Kategorie:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="category">
                      <option value="brand">Brand</option>
                      <option value="electronic-retail">Electronic Retail</option>
                      <option value="fashion">Fashion</option>
                      <option value="retail">Retail</option>
                      <option value="travel">Travel</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="type" class="col-sm-2 label-1 control-label">Typ:</label>
                  <div class="col-sm-8">
                    <select class="custom" name="type">
                      <option value="competitor">Competitor</option>
                      <option value="customer">Customer</option>
                      <option value="one_internal">One Intern</option>
                      <option value="new_business">New Business</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Local Rankings aktivieren<br/>(nur für DE/AT/CH):</label>
                  <div class="col-sm-8">
                    <div class="crawl-input-radio">
                      <input type="radio" name="local_rankings" value="0" class="icheck" id="iradio3" checked="checked">
                      <label for="iradio1">Nein</label>
                    </div>
                    <div class="crawl-input-radio">
                      <input type="radio" name="local_rankings" value="1" class="icheck" id="iradio4">
                      <label for="iradio2">Ja</label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_new_project_submit">Projekt hinzufügen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>

          <div class="box">
            <div class="box-header">
              <span class="title">Vorhandene Projekte</span>
            </div>
            <div class="box-content" id="ops_content">
               <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
               <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
            </div>
          </div>
        </div>

      </div>
    </div>

  </div>
