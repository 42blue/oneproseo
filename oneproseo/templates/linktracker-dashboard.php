<div class="main-content">

	<div class="container">
		<div class="row">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title"></i><?php echo $headline ?></h3>
				</div>
			</div>
		</div>
	</div>

	<!-- BREADCRUMB -->
	<div class="container padded">
		<div class="row">
			<div id="breadcrumbs">
				<div class="breadcrumb-button">
					<a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
					<span class="breadcrumb-arrow"><span></span></span>
				</div>
				<div class="breadcrumb-button">
					<span class="breadcrumb-label"> LinkTracker</span>
					<span class="breadcrumb-arrow"><span></span></span>
				</div>
			</div>
		</div>
	</div>

	<!-- MAIN CONTENT -->
	<div class="container" id="ops_content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<span class="title">Die Übersicht wird generiert ..</span>
					</div>
					<div class="box-content padded"><div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
					<div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div></div>
				</div>
			</div>
		</div>

	</div>

</div>
