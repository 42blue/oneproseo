<?php
require_once "botti-init.php";

$iProjectID = $id;
$iBotID = $bot;

$aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = %i", $iProjectID);
$aProject['dataFrom'] = onepro_formatDateFromTimestamp(OnePro_DB::queryOneField("timestamp", "SELECT * FROM `botti_" . $iProjectID . "_logs` WHERE `timestamp` != 0 ORDER BY `timestamp` ASC LIMIT 1"));
$aProject['dataTo'] = onepro_formatDateFromTimestamp(OnePro_DB::queryOneField("timestamp", "SELECT * FROM `botti_" . $iProjectID . "_logs` WHERE `timestamp` != 0 ORDER BY `timestamp` DESC LIMIT 1"));
$aBotData = OnePro_DB::queryFirstRow("SELECT * FROM `botti_user_agents` WHERE `id` = %i", $iBotID);

require_once "botti-controlpanel.php";
?>

<div class="main-content">

	<div class="container">
		<div class="row">
			<div class="area-top clearfix">
				<div class="pull-left header">
					<h3 class="title"></i><?php echo $headline ?></h3>
				</div>
			</div>
		</div>
	</div>

	<!-- BREADCRUMB -->
	<div class="container padded">
		<div class="row">
			<div id="breadcrumbs">
				<div class="breadcrumb-button">
					<a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
					<span class="breadcrumb-arrow"><span></span></span>
				</div>
				<div class="breadcrumb-button">
					<a href="<?php echo $this->data['domain'] ?>botti/dashboard"><span class="breadcrumb-label"> Botti</span></a>
					<span class="breadcrumb-arrow"><span></span></span>
				</div>
				<div class="breadcrumb-button">
					<a href="<?php echo $this->data['domain'] ?>botti/project/<?php echo $iProjectID; ?>"><span class="breadcrumb-label"> <?php echo $aProject['name']; ?></span></a>
					<span class="breadcrumb-arrow"><span></span></span>
				</div>
				<div class="breadcrumb-button">
					<span class="breadcrumb-label"> <?php echo $aBotData['browser'] . '/' . $aBotData['version'] ; ?></span>
					<span class="breadcrumb-arrow"><span></span></span>
				</div>
			</div>
		</div>
	</div>


	<!-- MAIN CONTENT -->
	<div class="container" id="ops_content" data-projectid="<?php echo $iProjectID ?>" data-botid="<?php echo $iBotID ?>">

		<div class="row">
			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<span class="title">Projekt Daten</span>
					</div>
					<div class="box-content">
						<table class="table table-normal">
							<tbody>
								<tr class="status-pending">
								<td class="icon"><i class="icon-lightbulb"></i></td>
								<td>Projektname:</td>
								<td><?php echo $aProject['name']; ?></td>
							</tr>
								<tr class="status-pending">
									<td class="icon"><i class="icon-bookmark"></i></td>
									<td>Domain:</td>
									<td><span id="ops_url" data-url="<?php echo $aProject['url']; ?>"><?php echo $aProject['url']; ?></span></td>
								</tr>
								<tr class="status-pending">
									<td class="icon"><i class="icon-eye-open"></i></td>
									<td>Verfügbare Daten</td>
									<td><?php echo $aProject['dataFrom'] . " - " . $aProject['dataTo']; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="box">
					<div class="box-header">
						<span class="title">Ansicht Daten</span>
					</div>
					<div class="box-content">
						<table class="table table-normal">
							<tbody>
								<tr class="status-pending">
									<td class="icon"><i class="icon-lightbulb"></i></td>
									<td>Bot:</td>
									<td><?php echo $aBotData['browser'] . '/' . $aBotData['version'] ; ?></td>
								</tr>
								<tr class="status-pending">
									<td class="icon"><i class="icon-lightbulb"></i></td>
									<td>User Agent:</td>
									<td><?php echo $aBotData['user_agent']; ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<span class="title">Control Panel</span>
					</div>
					<div id="ops_controlpanel" class="box-content padded">
						<form class="fill-up validatable">
							<input type="hidden" name="ignore_query_string_prev" type="checkbox" value="<?php if($sIgnoreQueryString == "on") echo 'on'; else echo 'off' ?>"/>
							<input type="hidden" name="check_reverse_dns_prev" type="checkbox" value="<?php if($sCheckReverseDns == "on") echo 'on'; else echo 'off' ?>"/>
							<div class="row">
								<div class="col-md-2">
									<label class="col-md-12"> Von</label>
									<div class="col-md-12"><input id="datepicker_botti_from" class="datepicker_botti_from fill-up" required="required" type="text" name="datefrom" value="<?php echo $_GET['datefrom']; ?>"></div>
								</div>

								<div class="col-md-2">
									<label class="col-md-12"> Bis</label>
									<div class="col-md-12"><input id="datepicker_botti_to" class="datepicker_botti_to fill-up" required="required" type="text" name="dateto" value="<?php echo $_GET['dateto']; ?>"></div>
								</div>

								<div class="col-md-2">
									<label class="col-md-12">Status Code</label>
									<div class="col-md-12">
										<select id="botti_status_code" class="uniform" name="status_code">
											<option value="-1"<?php if($sStatusCode == "-1") echo ' selected="selected"'; ?>>Alle</option>
											<option value="200"<?php if($sStatusCode == "200") echo ' selected="selected"'; ?>>200 OK</option>
											<option value="301"<?php if($sStatusCode == "301") echo ' selected="selected"'; ?>>301 Moved Permanently (Redirect)</option>
											<option value="302"<?php if($sStatusCode == "302") echo ' selected="selected"'; ?>>302 Found (Temporary Redirect)</option>
											<option value="404"<?php if($sStatusCode == "404") echo ' selected="selected"'; ?>>404 Not Found</option>
											<option value="410"<?php if($sStatusCode == "410") echo ' selected="selected"'; ?>>410 Gone</option>
											<option value="500"<?php if($sStatusCode == "500") echo ' selected="selected"'; ?>>500 Internal Server Error</option>
											<option value="502"<?php if($sStatusCode == "502") echo ' selected="selected"'; ?>>502 Bad Gateaway (Proxy)</option>
											<option value="503"<?php if($sStatusCode == "503") echo ' selected="selected"'; ?>>503 Service Unavailable</option>
										</select>
									</div>
								</div>

								<div class="col-md-2">
									<label class="col-md-12">Parameter in URL ignorieren</label>
									<div class="col-md-12"><input id="botti_ignore_query_string" class="iButton-icons-tab" name="ignore_query_string" type="checkbox" <?php if($sIgnoreQueryString == "on") echo 'checked="checked"'; ?>/></div>
								</div>

								<div class="col-md-2">
									<label class="col-md-12">Reverse DNS prüfen</label>
									<div class="col-md-12"><input id="botti_check_reverse_dns" class="iButton-icons-tab" name="check_reverse_dns" type="checkbox" <?php if($sCheckReverseDns == "on") echo 'checked="checked"'; ?>/></div>
								</div>

								<div class="col-md-2">
									<div>&nbsp;</div>
									<button type="submit" class="btn btn-blue">Übernehmen</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box">

					<div class="box-header">
						<div class="title">Botzugriffe</div>
					</div>

					<div class="box-content padded">
						<div class="row">
							<div class="col-md-8">
								<div id="OPSC_bot_chart"><div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
                <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div></div>
							</div>

							<div class="col-md-4 separate-sections">
								<div class="row">
									<div class="col-md-12">
										<div class="dashboard-stats">
											<ul class="list-inline">
												<li class="glyph"><i class="icon-bolt icon-2x"></i></li>
												<li class="count" id="ops_botcount"></li>
											</ul>
											<div class="stats-label">Anzahl Botzugriffe</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box" id="ops_bots">
					<div class="box-header">
						<span class="title">URLs</span>
						<ul class="box-toolbar">
							<li><a title="CSV download" class="icon-cloud-download csv-request" href="" target="_blank"></a></li>
							<li><i title="In Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="Botti / Bot Export <?php echo $aProject['name']; ?>" data-filepath=""></i></li>
						</ul>
					</div>
					<div class="box-content">
						<table class="table table-normal data-table">
							<thead>
							<tr>
								<th>URL</th>
								<th>Zugriffe</th>
								<th>anzeigen</th>
							</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

</div>
