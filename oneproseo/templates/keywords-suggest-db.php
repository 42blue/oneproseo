<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i> One Suggest DB</h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['keywords'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               One Suggest DB
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Finde alle Suggest Daten zu einem Keyword</span>
            </div>
            <div class="box-content padded">

              <div id="ops_flash_message"></div>

              <form class="form-horizontal fill-up" role="form" id="ops_kolibri_task">
                <div class="form-group">
                  <label for="ops_kolibri_kw" class="col-sm-2 label-1 control-label">Keyword:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_kolibri_kw" name="kw" required="required" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="lang" class="col-sm-2 control-label">Quelle:</label>
                  <div class="col-sm-8">
                    <select id="ops_kolibri_source" class="uniform" name="source">
                      <option value="google">Google</option>
                      <option value="youtube">YouTube</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="lang" class="col-sm-2 control-label">Für welches Land (Google):</label>
                  <div class="col-sm-8">
                    <select id="ops_kolibri_lang" class="uniform" name="lang">
                      <option value="de">Deutschland</option>
                      <option value="at">Österreich</option>
                      <option value="ch">Schweiz</option>
                      <option value="au">Australien</option>
                      <option value="be">Belgien</option>
                      <option value="br">Brasilien</option>
                      <option value="dk">Dänemark</option>
                      <option value="fi">Finnland</option>
                      <option value="fr">Frankreich</option>
                      <option value="in">Indien</option>
                      <option value="id">Indonesien</option>
                      <option value="it">Italien</option>
                      <option value="ca">Kanada</option>
                      <option value="hr">Kroatien</option>
                      <option value="lu">Luxemburg</option>
                      <option value="mx">Mexiko</option>
                      <option value="nz">Neuseeland</option>
                      <option value="nl">Niederlande</option>
                      <option value="no">Norwegen</option>
                      <option value="pl">Polen</option>
                      <option value="pt">Portugal</option>
                      <option value="ro">Rumänien</option>
                      <option value="ru">Russland</option>
                      <option value="se">Schweden</option>
                      <option value="rs">Serbien</option>
                      <option value="sk">Slowakei</option>
                      <option value="si">Slowenien</option>
                      <option value="es">Spanien</option>
                      <option value="th">Thailand</option>
                      <option value="cz">Tschechien</option>
                      <option value="tk">Türkei</option>
                      <option value="uk">UK</option>
                      <option value="en">USA</option>
                      <option value="hu">Ungarn</option>
                      <option value="ae">Vereinigte Arabische Emirate</option>
                      <option value="vn">Vietnam</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_kolibri_submit">Suggest Daten suchen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_loading_block">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Ergebnisse</span>
            </div>
            <div class="box-content padded">
              <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_result_block"></div>

    </div>

  </div>
