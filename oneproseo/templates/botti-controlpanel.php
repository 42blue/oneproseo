<?php
$sIgnoreQueryString = "off";
if(isset($_GET['ignore_query_string']) && $_GET['ignore_query_string'] == "on")
{
	$sIgnoreQueryString = "on";
}
else if(!isset($_GET['ignore_query_string']) && isset($_GET['ignore_query_string_prev']) && $_GET['ignore_query_string_prev'] == "on")
{
	unset($_COOKIE['botti_ignore_query_string']);
	setcookie('botti_ignore_query_string', null, -1, '/');
}
else if($_COOKIE['botti_ignore_query_string'] == "on")
{
	$sIgnoreQueryString = "on";
}

$sCheckReverseDns = "off";
if(isset($_GET['check_reverse_dns']) && $_GET['check_reverse_dns'] == "on")
{
	$sCheckReverseDns = "on";
}
else if(!isset($_GET['check_reverse_dns']) && isset($_GET['check_reverse_dns_prev']) && $_GET['check_reverse_dns_prev'] == "on")
{
	unset($_COOKIE['botti_check_reverse_dns']);
	setcookie('botti_check_reverse_dns', null, -1, '/');
}
else if($_COOKIE['botti_check_reverse_dns'] == "on")
{
	$sCheckReverseDns = "on";
}

$sStatusCode = "-1";
if(isset($_GET['status_code']))
{
	$sStatusCode = $_GET['status_code'];
}
else if(isset($_COOKIE['botti_status_code']))
{
	$sStatusCode = $_COOKIE['botti_status_code'];
}
?>