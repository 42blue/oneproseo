<?php
require_once __DIR__ . "/../../onepro_library/onepro.conf.php";

$config = OnePro_Config::getInstance();

if($config->getEnvironment() == "main" && strpos(ONEPRO_BASE_PATH, "dev-")) $config->setEnvironment("dev");
else if($config->getEnvironment() == "main") $config->setEnvironment("prod");

onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");