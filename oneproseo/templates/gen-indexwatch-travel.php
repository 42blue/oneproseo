    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <ul class="nav navbar-nav navbar-main">
            <li><a href="http://www.oneproseo.com/de/">Home<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/">Gratis Seo Tools<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/seo-site-check/">Sitecheck<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/seo-ranking-check/">Rankingcheck<mark class="bar"></mark></a></li>
            <li><a href="http://www.seo-agentur-ranking.org/">SEO Agentur Ranking 2015<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/de/faq/">FAQ<mark class="bar"></mark></a></li>
          </ul>
        </div>
      </div>
    </div>


    <div class="col-md-12">

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="https://www.oneproseo.com/indexwatch.html"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $headline ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_project">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Einen Moment, der Top 1000 Hotel Rankings der wichtigsten Wettbewerber wird erstellt.</span>
            </div>
            <div id="ops_loading" class="box-content padded">
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Eigene URL abfragen</span>
            </div>
            <div id="ops_loading" class="box-content padded">

              <form class="form-horizontal fill-up" role="form" id="ops_travelindex_form">
                <div class="form-group">
                  <label for="ops_travelindex_url" class="col-sm-2 label-1 control-label">URL:</label>
                  <div class="col-sm-8">
                    <input type="url" class="form-control" id="ops_travelindex_url" name="url" required="required" value="http://" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_travelindex_submit">Prüfen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- GOOGLE PLUS -->
    <div class="container center padded">
      <a href="https://www.advertising.de/seo.html" target="_blank"><img src="<?php echo $this->data['domain'] ?>oneproseo/src/images/seo-help.jpg" alt="Enterprise SEO Help"/></a>
    </div>

    <!-- GOOGLE PLUS -->
    <div class="container center padded">
      <script src="https://apis.google.com/js/plusone.js"></script>
      <div id="comments"></div>
      <script>
        gapi.comments.render('comments', {
            href: 'http://enterprise.oneproseo.com/indexwatch/travel-hotel-benchmarking',
            width: '624',
            first_party_property: 'BLOGGER',
            view_type: 'FILTERED_POSTMOD'
        });
      </script>
    </div>

  </div>