    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/dashboard"><span class="breadcrumb-label"> <?php echo $this->data['ops_menu']['rankings'] ?></span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/overview/<?php echo $id ?>"><span class="breadcrumb-label"> Projekt </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label"> Keyword Datumsvergleich</span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>
    <?php

      if (!isset($startdate)) {
        $startdate = date('d.m.Y', strtotime('-364 days'));
      }

      $yesterday = date('d.m.Y', strtotime('-1 days'));

      // Convert to timestamp
      $start_ts = strtotime('-364 days');
      $end_ts   = strtotime($yesterday);
      $user_ts  = strtotime($startdate);

      // Check that user date is between start & end
      if (($user_ts >= $start_ts) && ($user_ts <= $end_ts)) {
        $startdate_out = $startdate;
      } else {
        $startdate_out = date('d.m.Y', strtotime('-364 days'));
      }
    ?>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_project" data-setid="<?php echo $set_id ?>" data-projectid="<?php echo $id ?>" data-startdate="<?php echo $startdate_out ?>">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Datumsvergleich</span>
            </div>
            <div class="box-content padded" id="ops_content">
              <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
            </div>
          </div>

        </div>

      </div>

    </div>

  </div>
