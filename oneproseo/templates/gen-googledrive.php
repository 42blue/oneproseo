<?php

  session_start();

  set_include_path('/var/www/oneproapi/drive/google-api-php-client-master/src/' . PATH_SEPARATOR . get_include_path());

  require_once 'Google/Client.php';
  require_once 'Google/Http/MediaFileUpload.php';
  require_once 'Google/Service/Drive.php';

  $client = new Google_Client();
  
  $client->setClientId('352755655277-ruo66ojr3ai6kj2hv3ese22bkvrjuodd.apps.googleusercontent.com');
  $client->setClientSecret('Avo7f5dnNC5qaA5Cj4ri_Po4');
  $client->setRedirectUri($this->data['domain'] . 'googledrive');
  $client->addScope('https://www.googleapis.com/auth/drive');

  $service = new Google_Service_Drive($client);


  if (isset($_REQUEST['logout'])) {
    unset($_SESSION['upload_token']);
  }

  if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['upload_token'] = $client->getAccessToken();
    $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
  }

  if (isset($_SESSION['upload_token']) && $_SESSION['upload_token']) {

    $client->setAccessToken($_SESSION['upload_token']);

    if ($client->isAccessTokenExpired()) {
      unset($_SESSION['upload_token']);
    }

  } else {
    $authUrl = $client->createAuthUrl();
  }

  /************************************************
    If we're signed in then lets try to upload our
    file. For larger files, see fileupload.php.
   ************************************************/
  if ($client->getAccessToken()) {

    if (empty($_SESSION['google_drive_filename']) || !isset($_SESSION['google_drive_filename'])) {
      $filename = 'OneProSeo | Datenexport';
    } else {
      $filename = $_SESSION['google_drive_filename'];
    }

    $file = new Google_Service_Drive_DriveFile();
    $file->setTitle($filename);
    $result = $service->files->insert(
        $file,
        array(
          'data' => file_get_contents($_SESSION['google_drive_filepath']),
          'mimeType' => 'text/csv',
          'uploadType' => 'multipart',
          'convert' => true,
          'useContentAsIndexableText' => true
        )
    );
  }


  if (isset($result)) {

    header("Location: " . $result->alternateLink);
    die();

  } else {

    echo $authUrl;

  }

?>
