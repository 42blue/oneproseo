<?php


  header('Content-Type: application/json');

  class api
  {

    public function __construct ($data)
    {

      $this->data = $data;
      $this->key  = $data['key'];
      $this->type = $data['type'];
      $this->cid  = $this->extractCid();

      $db = new db();
      $this->db = $db->mySqlConnect($data);

      $this->checkKey();
      $this->loadFile();

    }

    public function checkKey () {

      $sql  = "SELECT api_key FROM ruk_project_customers WHERE id = '$this->cid'";
      $res  = $this->db->query($sql);

      while ($row = $res->fetch_assoc()) {

        if ($row['api_key'] !== $this->key) {
          echo json_encode(array('error' => 'invalid key'));
          exit;
        }

      }

    }


    private function loadFile () {

      $fn = $this->data['tempcache'] . 'json/' . $this->type . '.json';

      if (file_exists($fn)) {

        $file = file_get_contents($fn);
        $file = json_decode($file, true);
        echo json_encode($file, JSON_PRETTY_PRINT);
        exit;

      } else {

        echo json_encode(array('error' => 'file does not exist'));
        exit;

      }

    }


    private function extractCid () {

      $ex = explode('_', $this->type);

      foreach ($ex as $str) {
        if (is_numeric($str) == true) {
          return $str;
        }
      }

    }

 }

new api ($this->data);

?>
