<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"><?php echo $headline ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['tools'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <a href="../search-console"><?php echo $headline ?></a>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo base64_decode($url)?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12" id="ops_add_block">

          <div class="box">

            <div class="box-header">
              <div class="title">Neuen Search Console Export für <?php echo base64_decode($url)?> starten</div>
            </div>

            <div class="box-content padded">
              <div class="tab-content">
 
                  <form class="form-horizontal fill-up ops_form" role="form">
                    <div class="form-group">
                      <label for="task" class="col-sm-2 label-1 control-label">Name des Jobs:</label>
                      <div class="col-sm-8">
                        <input type="text" class="form-control" name="task" placeholder="z.b. Kleines Katzenbaby" required="required" value="" />
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-sm-2 label-1 control-label">Start:</label>
                      <div class="col-sm-8">
                        <input class="datepicker_ruk fill-up" required="required" type="text" name="date1" placeholder="Frühestes Datum: <?php echo date('d.m.Y', strtotime('-540 days')) ?>" value="">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="name" class="col-sm-2 label-1 control-label">Ende:</label>
                      <div class="col-sm-8">
                        <input class="datepicker_ruk fill-up" required="required" type="text" name="date2" placeholder="Spätestes Datum: <?php echo date('d.m.Y', strtotime('-3 days')) ?>" value="">
                      </div>
                    </div>

                    <script>
                      $(".datepicker_ruk").datepicker({
                        format: "dd.mm.yyyy",
                        autoclose: true,
                        startDate: "<?php echo date('d.m.Y', strtotime('-540 days')) ?>",
                        endDate: "-3d",
                        beforeShowDay: function(dx) {
                          var day = dx.getDay();
                          if (day != 1) {
                            return '.$datepicker.';
                          } else {
                            return true;
                          }
                        }
                      });
                    </script>

                    <input type="hidden" id="mail" name="scurl" value="<?php echo $url ?>">
                    <input type="hidden" id="name" name="name" value="<?php echo $_SESSION['firstname'] ?> <?php echo $_SESSION['lastname'] ?>">
                    <input type="hidden" id="mail" name="email" value="<?php echo $_SESSION['email'] ?>">

                    <div class="form-group">
                      <div class="col-sm-2 control-label"></div>
                      <div class="col-sm-8">
                        <button type="submit" class="btn btn-primary btn-lg ops_submit">Export anlegen</button>
                      </div>
                    </div>
                  </form>

              </div>
            </div>

          </div>

        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Laufende und abgeschlossene Exporte</span>
            </div>
            <div class="box-content" id="ops_data"></div>
          </div>
        </div>
      </div>

    </div>

  </div>
