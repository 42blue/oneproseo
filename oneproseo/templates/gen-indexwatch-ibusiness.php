    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <ul class="nav navbar-nav navbar-main">
            <li><a href="http://www.oneproseo.com/de/">Home<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/">Gratis Seo Tools<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/seo-site-check/">Sitecheck<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/seo-ranking-check/">Rankingcheck<mark class="bar"></mark></a></li>
            <li><a href="http://www.seo-agentur-ranking.org/">SEO Agentur Ranking 2015<mark class="bar"></mark></a></li>
            <li><a href="http://www.oneproseo.com/de/faq/">FAQ<mark class="bar"></mark></a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="col-md-12">

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="https://www.oneproseo.com/indexwatch.html"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $headline ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <div class="container padded">
      <div class="row">
        <h1 class="indexwatch">iBusiness SEO Agenturen Ranking</h1>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container" id="ops_project">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Einen Moment, das iBusiness SEO Agenturen Ranking wird erstellt.</span>
            </div>
            <div id="ops_loading" class="box-content padded">
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Eigene URL abfragen</span>
            </div>
            <div id="ops_loading" class="box-content padded">

              <form class="form-horizontal fill-up" role="form" id="ops_travelindex_form">
                <div class="form-group">
                  <label for="ops_travelindex_url" class="col-sm-2 label-1 control-label">URL:</label>
                  <div class="col-sm-8">
                    <input type="url" class="form-control" id="ops_travelindex_url" name="url" required="required" value="http://" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_travelindex_submit">Prüfen</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>

      <div id="disqus_thread"></div>
      </div>
      <script type="text/javascript">
          /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
          var disqus_shortname = 'oneadvertising';
          var disqus_identifier = 'oneproseo_indexwatch_ibusiness';
          var disqus_url = 'https://enterprise.oneproseo.com/indexwatch/ibusiness';
          /* * * DON'T EDIT BELOW THIS LINE * * */
          (function() {
              var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
              dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
              (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
          })();
      </script>
      <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

    </div>

  </div>