    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>rankings/dashboard"><span class="breadcrumb-label"> <?php echo $this->data['ops_menu']['seaseobalancer'] ?></span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
              SEA Landingpage Finder
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Landingpages ermitteln</span>
            </div>
            <div class="box-content padded">
              <p>Dieses Tool ermittelt die - aus Google-Sicht - idealen Landingpages für das jeweilige Keyword. Die Abfrage ist auf maximal 100 Keywords beschränkt.</p>
              <form class="form-horizontal fill-up" role="form" id="ops_liverankings">
                <div class="form-group">
                  <label class="col-sm-2 label-2 control-label">Keywords:</label>
                  <div class="col-sm-8">
                    <textarea class="searchvolume" id="ops_liverankings_keywords" name="domains" placeholder="Ein Keyword pro Zeile!" required="required"></textarea>
                  </div>
                </div>
                <div class="form-group" id="ops_kolibri_toggle">
                  <label for="ops_kolibri_url" class="col-sm-2 label-1 control-label">Domain:</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="ops_liverankings_url" name="url" placeholder="URL" required="required" value="" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="lang" class="col-sm-2 control-label">Land (Google):</label>
                  <div class="col-sm-8">
                    <select id="ops_liverankings_lang" class="uniform" name="lang" id="ops_searchvolume_lang">
                      <option value="de">Deutschland</option>
                      <option value="at">Österreich</option>
                      <option value="ch">Schweiz</option>
                      <option value="au">Australien</option>
                      <option value="be">Belgien</option>
                      <option value="br">Brasilien</option>
                      <option value="dk">Dänemark</option>
                      <option value="fi">Finnland</option>
                      <option value="fr">Frankreich</option>
                      <option value="in">Indien</option>
                      <option value="id">Indonesien</option>
                      <option value="it">Italien</option>
                      <option value="ca">Kanada</option>
                      <option value="hr">Kroatien</option>
                      <option value="lu">Luxemburg</option>
                      <option value="mx">Mexiko</option>
                      <option value="nz">Neuseeland</option>
                      <option value="nl">Niederlande</option>
                      <option value="no">Norwegen</option>
                      <option value="pl">Polen</option>
                      <option value="pt">Portugal</option>
                      <option value="ro">Rumänien</option>
                      <option value="ru">Russland</option>
                      <option value="se">Schweden</option>
                      <option value="rs">Serbien</option>
                      <option value="sk">Slowakei</option>
                      <option value="si">Slowenien</option>
                      <option value="es">Spanien</option>
                      <option value="th">Thailand</option>
                      <option value="cz">Tschechien</option>
                      <option value="tk">Türkei</option>
                      <option value="uk">UK</option>
                      <option value="en">USA</option>
                      <option value="hu">Ungarn</option>
                      <option value="ae">Vereinigte Arabische Emirate</option>
                      <option value="vn">Vietnam</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2 control-label"></div>
                  <div class="col-sm-8">
                    <button type="submit" class="btn btn-primary btn-lg" id="ops_liverankings_submit">Landingpages ermitteln</button>
                  </div>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_loading_block">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <span class="title">Ergebnisse</span>
            </div>
            <div class="box-content padded">
              <div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div>
              <div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>
            </div>
          </div>
        </div>
      </div>

      <div class="row hidden" id="ops_result_block"></div>

    </div>
