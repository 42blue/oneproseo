<?php

 class useradmin
 {

   function __construct($data)
   {

     if ($_SESSION['role'] != 'admin') {
       exit;
     }

    if (empty($_POST['edit']) && empty($_POST['change'])) {
      exit;
    }

     $this->data = $data;
     $this->out_view = '';

     $db = new db();
     $this->db = $db->mySqlConnect($data);

     if (isset($_POST['change'])) {
       $this->user_id = $_POST['change'];
       $this->changeUser();
       $this->showUserInput();
     }

     if (isset($_POST['edit'])) {
       $this->user_id = $_POST['edit'];
       $this->showUserInput();
     }


   }

   function changeUser() {

     $login                      = strip_tags($_POST['login']);
     $firstname                  = strip_tags($_POST['firstname']);
     $lastname                   = strip_tags($_POST['lastname']);
     $email                      = strip_tags($_POST['email']);

     if (!empty($_POST['password'])) {
       $password                 = md5($_POST['password']);
     }

     $role                       = strip_tags($_POST['role']);
     $role2                      = strip_tags($_POST['role2']);
     $restrict_customer_versions = strip_tags($_POST['restrict_customer_versions'], ',');
     $customer                   = strip_tags($_POST['customer']);
     $customer_v                 = $_POST['customer_versions'];

      if ($customer_v === NULL && $customer == 1) {
        $this->out_view .= '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>HEY!</strong> Bitte mindestens eine Kundenversionen angeben.</div>';
        return;
      }

      if (empty($restrict_customer_versions)) {
        $restrict_customer_versions = 0;
      }

      if (empty($role2)) {
        $role2 = 'false';
      }


      if (!empty($_POST['password'])) {
        $sql = "UPDATE onepro_users SET firstname = '$firstname', lastname = '$lastname', email = '$email', password  = '$password', role = '$role', role2 = '$role2',  customer = '$customer', login = '$login', restrict_customer_versions = '$restrict_customer_versions' WHERE id = '$this->user_id'";
      } else {
        $sql = "UPDATE onepro_users SET firstname = '$firstname', lastname = '$lastname', email = '$email', role = '$role', role2 = '$role2', customer = '$customer', login = '$login', restrict_customer_versions = '$restrict_customer_versions' WHERE id = '$this->user_id'";
      }

      $result = $this->db->query($sql);

     if ($customer_v != NULL)  {

     $sql = "DELETE FROM oneproseo_customer2user WHERE user_id = $this->user_id";
     $x = $this->db->query($sql);

     $this->db->affected_rows;

      foreach ($customer_v as $key => $ruk_id) {
        $sql = "INSERT INTO oneproseo_customer2user (customer_version, user_id) VALUES ('" . $ruk_id . "', '" . $this->user_id . "')";
        $this->db->query($sql);
       }

     }

     $this->out_view .= '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Yeah!</strong> Der Daten wurden gespeichert!.</div>';

   }

   function editUser() {

     $sql = "SELECT * FROM onepro_users WHERE id = $this->user_id";
     $res = $this->db->query($sql);
     $customer_versions = array();
     while ($row = $res->fetch_assoc()) {
       $userdata = $row;
     }

     // GET ALL USER 2 CUSTOMER VERSIONS
     $sql = "SELECT ruk_id, name FROM oneproseo_customer_versions ORDER BY name";
     $res = $this->db->query($sql);
     while ($row = $res->fetch_assoc()) {
       $customer_versions[$row['ruk_id']] = '<option value="' . $row['ruk_id'] . '"> ' . $row['name'] . '</option>';
     }

     // GET SELECTED USER 2 CUSTOMER VERSIONS
     $sql = "SELECT
             *
           FROM oneproseo_customer2user a
           LEFT JOIN oneproseo_customer_versions b
             ON b.ruk_id = a.customer_version
           WHERE user_id = $this->user_id";

     $res = $this->db->query($sql);
     while ($row = $res->fetch_assoc()) {
      $customer_versions[$row['ruk_id']] = '<option value="' . $row['ruk_id'] . '" selected="selected"> ' . $row['name'] . '</option>';
     }

     echo '
       <div class="row">
         <div class="col-md-12">
           <div class="box">
             <div class="box-header">
               <span class="title">Benutzer bearbeiten</span>
             </div>
             <div class="box-content padded">
              <form class="form-horizontal fill-up" role="form" action="user-edit" method="POST">
               <input type="hidden" name="change" value="'.$this->user_id.'">
               <div class="form-group">
                 <label class="col-sm-2 control-label">Login aktiv:</label>
                 <div class="col-sm-8">
                   <div class="crawl-input-radio">';
                    if ($userdata['login'] == 0) {
                      echo '<input type="radio" name="login" value="0" class="icheck" checked="checked">';
                    } else {
                      echo '<input type="radio" name="login" value="0" class="icheck">';
                    }
                    echo '<label for="iradio1">Nein</label>
                   </div>
                   <div class="crawl-input-radio">';
                   if ($userdata['login'] == 1) {
                     echo '<input type="radio" name="login" value="1" class="icheck" checked="checked">';
                   } else {
                     echo '<input type="radio" name="login" value="1" class="icheck">';
                   }
                   echo '<label for="iradio2">Ja</label>
                   </div>
                 </div>
               </div>
               <div class="form-group">
                 <label class="col-sm-2 control-label">Extern (Kunde):</label>
                 <div class="col-sm-8">
                   <div class="crawl-input-radio">';
                    if ($userdata['customer'] == 0) {
                      echo '<input type="radio" name="customer" value="0" class="icheck" checked="checked">';
                    } else {
                      echo '<input type="radio" name="customer" value="0" class="icheck">';
                    }
                    echo '<label for="iradio1">Nein</label>
                   </div>
                   <div class="crawl-input-radio">';
                   if ($userdata['customer'] == 1) {
                     echo '<input id="ops_click" type="radio" name="customer" value="1" class="icheck"  checked="checked">';
                   } else {
                     echo '<input id="ops_click" type="radio" name="customer" value="1" class="icheck">';
                   }
                   echo '<label for="iradio2">Ja</label>
                   </div>
                 </div>
               </div>
               <div class="form-group">
                 <label for="getall" class="col-sm-2 label-1 control-label">Name:</label>
                 <div class="col-sm-4">
                   <input type="text" class="form-control" name="firstname" placeholder="Vorname" value="'.$userdata['firstname'].'" required="required">
                 </div>
                 <div class="col-sm-4">
                   <input type="text" class="form-control" name="lastname" placeholder="Nachname" value="'.$userdata['lastname'].'" required="required">
                 </div>
               </div>
               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Email Adresse: (Login)</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="email" required="required" value="'.$userdata['email'].'">
                 </div>
               </div>
               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Neues Passwort:</label>
                 <div class="col-sm-8">
                   <input type="password" class="form-control" name="password" value="" autocomplete="off">
                 </div>
               </div>
               <div class="form-group">
                 <label for="type" class="col-sm-2 label-1 control-label">Rechte:</label>
                 <div class="col-sm-8">
                   <select class="custom" name="role">';
                    if ($userdata['role'] == 'redakteur') {
                      echo '<option value="redakteur" selected="selected"> Redakteur </option>';
                    } else {
                      echo '<option value="redakteur"> Redakteur </option>';
                    }
                    if ($userdata['role'] == 'pjm') {
                      echo '<option value="pjm" selected="selected"> Projektmanager </option>';
                    } else {
                      echo '<option value="pjm"> Projektmanager </option>';
                    }
                    if ($userdata['role'] == 'admin') {
                      echo '<option value="admin" selected="selected"> Admin </option>';
                    } else {
                      echo '<option value="admin"> Admin </option>';
                    }
                   echo '</select>
                 </div>
               </div>';

                  if ($userdata['customer'] != 1) {
                     echo '<div class="form-group hidden" id="ops_show">';
                  } else {
                     echo '<div class="form-group" id="ops_show">';
                  }

                 echo '<label for="type" class="col-sm-2 label-1 control-label">Zugriff auf Kundenversionen:</label>
                 <div class="col-sm-8">
                   <select class="chzn-select" multiple name="customer_versions[]">
                   '.implode ('', $customer_versions).'
                   </select>
                 </div>
               </div>

               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Role 2:</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="role2" value="'.$userdata['role2'].'" autocomplete="off" placeholder="Standard: false // nodash">
                 </div>
               </div>

               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Customer Versions:</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="restrict_customer_versions" value="'.$userdata['restrict_customer_versions'].'" autocomplete="off" placeholder="Standard: 0 // 204,203,199">
                 </div>
               </div>

               <div class="form-group">
                 <div class="col-sm-2 control-label"></div>
                 <div class="col-sm-8">
                   <button type="submit" class="btn btn-primary btn-lg">Benutzer speichern</button>
                 </div>
               </div>
             </form>
           </div>
           </div>
         </div>
       </div>';
   }

   public function showUserInput ()
   {

    echo '
    <div class="main-content">

        <div class="container">
          <div class="row">
            <div class="area-top clearfix">
              <div class="pull-left header">
                <h3 class="title"></i>' . $this->data['titletag'] . '</h3>
              </div>
            </div>
          </div>
        </div>

        <div class="container padded">
          <div class="row">
            <div id="breadcrumbs">
              <div class="breadcrumb-button">
                <a href="' .  $this->data['domain'] . '"><span class="breadcrumb-label"> Home </span></a>
                <span class="breadcrumb-arrow"><span></span></span>
              </div>
              <div class="breadcrumb-button">
                <span class="breadcrumb-label">
                   ' .  $this->data['ops_menu']['admin'] . '
                </span>
                <span class="breadcrumb-arrow"><span></span></span>
              </div>
              <div class="breadcrumb-button">
                <span class="breadcrumb-label">
                   Benutzer bearbeiten
                </span>
                <span class="breadcrumb-arrow"><span></span></span>
              </div>
            </div>
          </div>
        </div>

        <div class="container">

          <div class="row">
            <div class="col-md-12">

               ' . $this->out_view;

               $this->editUser() .';

            </div>
          </div>

        </div>

      </div>';

   }

 }

 $obj = new useradmin ($this->data);

?>
