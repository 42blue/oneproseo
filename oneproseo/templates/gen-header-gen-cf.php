  <script src="<?php echo $this->data['domain'] ?>oneproseo/src/custom/loader-home-cf<?php echo $id ?>.js" type="text/javascript"></script>
</head>
<body class="black" data-customer="<?php echo $id ?>">

  <!-- HEADER -->
  <nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">

    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo $this->data['domain'] ?>">
        <img src="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/one-pro-seo-logo.svg" alt="OneProSeo - Desktop" class="hidden-xs hidden-sm hidden-md logo-big" />
        <img src="<?php echo $this->data['domain'] ?>oneproseo/src/images/oneproseo/one-pro-seo-logo-smallscreen.svg" alt="OneProSeo - Mobile" class="hidden-lg logo-small" >
      </a>

      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse-primary">
        <span class="sr-only">Toggle Side Navigation</span>
        <i class="icon-align-justify icon-2x smallscreen-menu-icon"></i>
        <i class="icon-remove icon-2x smallscreen-menu-icon hidden"></i>
      </button>

    </div>

    <div class="navbar-collapse nav-collapse-top collapse">

      <ul class="nav full pull-right">
        <li class="dropdown user-avatar">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span>
              <img class="menu-avatar" src="<?php echo $this->data['domain'] ?>oneproseo/src/images/customer/logo<?php echo $id ?>.jpg" /> <span><?php echo $this->data['customer_name'] ?><i class="icon-caret-down"></i></span>
            </span>
          </a>
          <ul class="dropdown-menu">
            <li class="with-image">
              <div class="avatar">
                <img src="<?php echo $this->data['domain'] ?>oneproseo/src/images/customer/logo<?php echo $id ?>.jpg" />
              </div>
              <span>
                <?php
                echo $this->data['customer_name'];
                if ($_SESSION['oldlogin'] != true ) {
                  echo '<br />' . $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];
                }
                ?>
              </span>
            </li>
            <li class="divider"></li>
            <?php
            if ($_SESSION['oldlogin'] != true) {
              echo '<li><a href="' . $this->data['domain'] . 'password"><i class="icon-lock"></i> <span>Passwort ändern</span></a></li>';
            }
            ?>
            <li><a href="<?php echo $this->data['domain'] ?>logout"><i class="icon-off"></i> <span>Abmelden</span></a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav menubar navbar pull-right">
        <li><a href="<?php echo $this->data['domain'] ?>rankings/dashboard"><i class="icon-bar-chart"></i> zurück zu OneProSeo </a></li>
      </ul>

    </div>

  </nav>
