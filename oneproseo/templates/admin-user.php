<?php

 class useradmin
 {

   function __construct($data)
   {

     if ($_SESSION['role'] != 'admin') {
       exit;
     }

     $this->out_view = '';

     $db = new db();
     $this->db = $db->mySqlConnect($data);

     if (isset($_POST['delete'])) {
       $this->user_id = $_POST['delete'];
       $this->deleteUser();
     }

     if (isset($_POST['add'])) {
       $this->addUser();
     }

   }

   function addUser() {

     $firstname                  = strip_tags($_POST['firstname']);
     $lastname                   = strip_tags($_POST['lastname']);
     $email                      = strip_tags($_POST['email']);
     $password                   = md5($_POST['password']);
     $role                       = strip_tags($_POST['role']);
     $customer                   = strip_tags($_POST['customer']);
     $customer_v                 = $_POST['customer_versions'];
     $role2                      = strip_tags($_POST['role2']);
     $restrict_customer_versions = strip_tags($_POST['restrict_customer_versions'], ',');
     $ar                         = array();
     $blob                       = serialize($ar);

     if ($customer_v === NULL && $customer == 1) {
       $this->out_view .= '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>HEY!</strong> Bitte mindestens eine Kundenversionen angeben.</div>';
       return;
     }

     if (empty($restrict_customer_versions)) {
       $restrict_customer_versions = 0;
     }

     if (empty($role2)) {
       $role2 = 'false';
     }

     $sql = "INSERT INTO
               onepro_users (firstname, lastname, email, password, login, role, role2, ruk_personal_dash, customer, restrict_customer_versions)
             VALUES ('" . $firstname . "', '" . $lastname . "', '" . $email . "', '" . $password . "', 1, '" . $role . "', '" . $role2 . "','" . $blob . "', '" . $customer . "', '" . $restrict_customer_versions . "')";

     $this->db->query($sql);

     $new_user_id = $this->db->insert_id;

     if ($customer_v != NULL)  {
      foreach ($customer_v as $key => $ruk_id) {

        $sql = "INSERT INTO
                  oneproseo_customer2user (customer_version, user_id)
                VALUES ('" . $ruk_id . "', '" . $new_user_id . "')";
        $this->db->query($sql);

       }
     }

     $this->mailUserData($firstname, $lastname, $email, $customer, $customer_v);

     $this->out_view .= '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Yeah!</strong> Der neue Benutzer wurde angelegt.</div>';

   }

   function mailUserData($firstname, $lastname, $email, $customer, $customer_v) {

     $password = $_POST['password'];

     $host = 'enterprise';

     if ($customer == 1) {
       $hostid = $customer_v[0];
       $sql = "SELECT subdomain FROM oneproseo_customer_versions WHERE ruk_id = $hostid";
       $res = $this->db->query($sql);
       $host = '';
       while ($row = $res->fetch_assoc()) {
         $host = $row['subdomain'];
       }
     }

     $emailcontent = '
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
           '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
             <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
           </td>
         </tr>
         <tr>
           '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
          <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
           <span style="font-size: 16px; font-color: #636363;">
           Hallo '.$firstname.' '.$lastname .',<br /><br />

           Ein neuer Benutzer wurde für Sie angelegt. Hier die Login Daten:<br /><br /></span>

           URL: <b><a href="https://'.$host.'.oneproseo.com/login">https://'.$host.'.oneproseo.com/</a></b><br />
           Benutzername: <b>'.$email.'</b><br />
           Passwort: <b>'.$password.'</b><br /><br />

           Bitte änderen Sie Ihr Passwort nach dem ersten Login:<br />
           <a href="https://'.$host.'.oneproseo.com/password">https://'.$host.'.oneproseo.com/password</a><br /><br />

         </td>
         '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         </tr>
         <tr>
           '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
         </tr>
      </table>';


     $subject  = 'OneProSeo | Ein neuer Benutzer wurde angelegt';
     $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" . 'From: OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;
     $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
     $message .= $emailcontent;
     $message .= '</body></html>';

     mail($email, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');
     mail('m.hotz@advertising.de', $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

   }

   function createUser() {

     // GET USER 2 CUSTOMER VERSIONS
     $sql = "SELECT ruk_id, name FROM oneproseo_customer_versions ORDER BY name";
     $res = $this->db->query($sql);
     $customer_versions = '';
     while ($row = $res->fetch_assoc()) {
       $customer_versions .= '<option value="' . $row['ruk_id'] . '"> ' . $row['name'] . '</option>';
     }

     echo '
       <div class="row">
         <div class="col-md-12">
           <div class="box">
             <div class="box-header">
               <span class="title">Neuen Benutzer anlegen</span>
             </div>
             <div class="box-content padded">
              <form class="form-horizontal fill-up" role="form" action="user" method="POST">
               <input type="hidden" name="add" value="add">
               <div class="form-group">
                 <label class="col-sm-2 control-label">Extern (Kunde):</label>
                 <div class="col-sm-8">
                   <div class="crawl-input-radio">
                     <input type="radio" name="customer" value="0" class="icheck" checked="checked">
                     <label for="iradio1">Nein</label>
                   </div>
                   <div class="crawl-input-radio">
                     <input id="ops_click" type="radio" name="customer" value="1" class="icheck">
                     <label for="iradio2">Ja</label>
                   </div>
                 </div>
               </div>
               <div class="form-group">
                 <label for="getall" class="col-sm-2 label-1 control-label">Name:</label>
                 <div class="col-sm-4">
                   <input type="text" class="form-control" name="firstname" placeholder="Vorname" value="" required="required">
                 </div>
                 <div class="col-sm-4">
                   <input type="text" class="form-control" name="lastname" placeholder="Nachname" value="" required="required">
                 </div>
               </div>
               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Email Adresse: (Login)</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="email" required="required" value="" autocomplete="off">
                 </div>
               </div>
               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Temporäres Passwort:</label>
                 <div class="col-sm-8">
                   <input type="password" class="form-control" name="password" required="required" value="" autocomplete="off">
                 </div>
               </div>
               <div class="form-group">
                 <label for="type" class="col-sm-2 label-1 control-label">Rechte:</label>
                 <div class="col-sm-8">
                   <select class="custom" name="role">
                     <option value="redakteur" selected="selected"> Redakteur </option>
                     <option value="pjm"> Projektmanager </option>
                     <option value="admin"> Admin </option>
                   </select>
                 </div>
               </div>

               <div class="form-group hidden" id="ops_show">
                 <label for="type" class="col-sm-2 label-1 control-label">Zugriff auf Kundenversionen:</label>
                 <div class="col-sm-8">
                   <select class="chzn-select" multiple name="customer_versions[]">
                   '.$customer_versions.'
                   </select>
                 </div>
               </div>
               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Role 2:</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="role2" value="" autocomplete="off" placeholder="Standard: false // nodash">
                 </div>
               </div>
               <div class="form-group">
                 <label for="url" class="col-sm-2 label-1 control-label">Customer Versions:</label>
                 <div class="col-sm-8">
                   <input type="text" class="form-control" name="restrict_customer_versions" value="" autocomplete="off" placeholder="Standard: 0 // 204,203,199">
                 </div>
               </div>
               <div class="form-group">
                 <div class="col-sm-2 control-label"></div>
                 <div class="col-sm-8">
                   <button type="submit" class="btn btn-primary btn-lg">Neuen Benutzer anlegen</button>
                 </div>
               </div>
             </form>
           </div>
           </div>
         </div>
       </div>';
   }


   function deleteUser() {

    $sql = "DELETE
               a.*, b.*
             FROM onepro_users a
               LEFT JOIN oneproseo_customer2user b
                 ON b.user_id = a.id
             WHERE a.id ='$this->user_id'";

     $this->db->query($sql);

     $this->out_view .= '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button> Benutzer gelöscht! </div>';

   }


   function fetchUsers() {

     // GET USER 2 CUSTOMER VERSIONS
     $sql = "SELECT
              *
            FROM oneproseo_customer2user b
            LEFT JOIN oneproseo_customer_versions c
              ON c.ruk_id = b.customer_version";

      $res = $this->db->query($sql);

      $customer2user = array();

      while ($row = $res->fetch_assoc()) {

        if (isset($customer2user[$row['user_id']])) {
          array_push ($customer2user[$row['user_id']], $row['name']);
        } else {
          $customer2user[$row['user_id']] = array($row['name']);
        }

      }

    $sql = "SELECT * FROM onepro_users";
    $res = $this->db->query($sql);

     $this->out_view .= '<div class="box"><div class="box-header"><span class="title">OneProSeo Benutzer</span></div><div class="box-content"><table style="display:table;" class="table table-normal data-table dtoverflow" id="data-table"><thead><tr><td>#</td><td>Name</td><td>Email</td><td>Login aktiv</td><td>Rechte</td><td>Extern / Intern</td><td>Zugriff auf:</td><td>bearbeiten</td><td>löschen</td></tr></thead><tbody>';

     while ($row = $res->fetch_assoc()) {

       $this->out_view .= '
        <tr>

          <td><a href="'. $row['id'].'">' . $row['id'] . '</a></td>
          <td> ' . $row['firstname']. ' ' .$row['lastname']. '</td>
          <td> ' . $row['email']. '</td>
          <td> ' . $this->canLogin($row['login']). '</td>
          <td> ' . ucfirst($row['role']). '</td>
          <td> ' . $this->extInt($row['customer']). '</td>';

          if (!isset($customer2user[$row['id']])) {
            $this->out_view .= '<td> Alles </td>';
          } else {
            $this->out_view .= '<td> ' . implode('<br/>',  $customer2user[$row['id']]) . '</td>';
          }

         $this->out_view .= '
         <td>
           <form action="user-edit" method="POST">
             <input type="hidden" name="edit" value="'.$row['id'].'">
             <button class="btn btn-mini btn-blue" type="submit"> bearbeiten</button>
           </form>
          </td>
          <td>
            <form action="user" method="POST">
              <input type="hidden" name="delete" value="'.$row['id'].'">
              <button class="ops_delete btn btn-mini btn-red" type="submit"> löschen</button>
            </form>
          </td>
        </tr>
       ';
     }

     $this->out_view .= '</tbody></table></div></div>';
     echo $this->out_view;

   }

   function canLogin($int) {
     if ($int == 0) {
       return 'Nein';
     }
     return 'Ja';
   }

   function extInt($int) {
     if ($int > 0) {
       return 'Extern (Kunde)';
     }
     return 'Intern';
   }

 }

 $obj = new useradmin ($this->data);

?>


<div class="main-content">

    <div class="container">
      <div class="row">
        <div class="area-top clearfix">
          <div class="pull-left header">
            <h3 class="title"></i><?php echo $this->data['titletag'] ?></h3>
          </div>
        </div>
      </div>
    </div>

    <!-- BREADCRUMB -->
    <div class="container padded">
      <div class="row">
        <div id="breadcrumbs">
          <div class="breadcrumb-button">
            <a href="<?php echo $this->data['domain'] ?>"><span class="breadcrumb-label"> Home </span></a>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               <?php echo $this->data['ops_menu']['admin'] ?>
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
          <div class="breadcrumb-button">
            <span class="breadcrumb-label">
               Benutzerverwaltung
            </span>
            <span class="breadcrumb-arrow"><span></span></span>
          </div>
        </div>
      </div>
    </div>

    <!-- MAIN CONTENT -->
    <div class="container">

      <div class="row">
        <div class="col-md-12">

          <? $obj->createUser() ?>

        </div>
      </div>

      <div class="row">
        <div class="col-md-12">

          <? $obj->fetchUsers() ?>

        </div>
      </div>

    </div>

  </div>
