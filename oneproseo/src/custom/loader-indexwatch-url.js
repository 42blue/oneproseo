$( document ).ready(function() {

  $.getScript(OPS.base.www + 'oneproseo/src/custom/functions.js', function() {

    if (window.location.search.substring(1)) {
      var query = window.location.search.substring(1).split('&');
      for (var i = 0; i < query.length; i++) {
        var pair = query[i].split('=');
        if (pair[0] === 'site') {
          var url = pair[1];
        }
        if (pair[0] === 'index') {
          var index = pair[1];
        }
      }
    }

    if (typeof(url) === 'undefined') {
      OPS.indexwatch.fetchRanking();
    } else {
      OPS.indexwatch.fetchRankingUrl(url, index);
    }

  });

});