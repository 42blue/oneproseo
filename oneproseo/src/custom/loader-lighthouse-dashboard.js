$(document).ready(function () {
    // hides form container
    $('button').on('click', function () {
        $('#formContainer').removeClass('show');
    });
    // hides lighthouse options
    $('button#showcrawleroptions').on('click', function () {
        $('#lighthouseoptions').removeClass('show in');
        $('#lighthouseoptions').addClass('collapse');
        $('#form').empty();
    });
    // hides crawler options
    $('button#showlighthouseoptions').on('click', function () {
        $('#crawleroptions').removeClass('show in');
        $('#crawleroptions').addClass('collapse');
        $('#form').empty();
    })
    // Post requested formulars
    $('button.formbutton').on('click', function (e) {
        e.preventDefault();
        $("#loaderDiv").show();
        $('#form').empty();
        let templateFile = $(this).attr('data-template')
        let application = $(this).attr('data-select')
        let action = $(this).attr('data-action')
        let project = $(this).attr('data-project')
        let requestUrl
        if (action === 'form') {
            requestUrl = '/forms';
        }
        if (action === 'status') {
            requestUrl = '/status';
        }
        $.ajax({
            url: '/lighthouse' + requestUrl,
            type: 'POST',
            data: {
                templatefile: templateFile,
                application: application,
                project: project,
                action: action
            },
            success: function (success) {
                // $("#loaderDiv").hide();
                $('#form').empty();
                $('#form').append(success);
                $('#formContainer').addClass('show');
                // scroll to form
                $('html, body').animate({
                    scrollTop: $("#formContainer").offset().top
                }, 1000);
                /**
                 * Submit forms
                 */
                $('#newForm').submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: $(this).serialize(),
                        success: function (success) {
                            let jsonResponse = JSON.parse(success);
                            $('#newForm').fadeOut('slow', function () {
                                $('#newForm').empty();
                                $('#form').empty();
                                if (jsonResponse.response.requestId === undefined) {
                                    $('#form').append('<div class="alert alert-warning alert-dismissible fade show" role="alert">\n' +
                                        '  <strong>Holy guacamole!</strong> Something went wrong.\n' +
                                        '  <pre>' + jsonResponse.response[0] + '</pre>\n' +
                                        '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                        '    <span aria-hidden="true">&times;</span>\n' +
                                        '  </button>\n' +
                                        '</div>');
                                } else {
                                    $('#form').append('<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
                                        '  <h4 class="alert-heading">Well done!</h4>\n' +
                                        '  <p>Your crawler is successfully startet. Check the status of your crawlers.</p>\n' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                        '    <span aria-hidden="true">&times;</span>\n' +
                                        '  </button>' +
                                        '</div>');
                                }
                            });
                        },
                        error: function (error) {
                            console.log('error: ' + error);
                            alert('Something went terribly wrong.');
                        },
                        complete: function (complete) {

                        }
                    })
                });

                $('#newFormUrls').submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: $(this).serialize(),
                        success: function (success) {
                            if(success == 'error')
                                alert('Something went wrong. Please check if all urls are valid (max 15 urls)!');
                            else
                            {
                                $('#newFormUrls').fadeOut('slow', function () {
                                    $('#newFormUrls').empty();
                                    $('#form').empty();
                                    $('#form').append('<div class="alert alert-success alert-dismissible show" role="alert">\n' +
                                        '  <h4 class="alert-heading">Well done!</h4>\n' +
                                        '  <p>Your url list is now saved.</p>\n' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                        '    <span aria-hidden="true">&times;</span>\n' +
                                        '  </button>' +
                                        '</div>');
                                });
                            }
                        },
                        error: function (error) {
                            console.log('error: ' + error);
                            alert('Something went wrong. Please check if all urls are valid (max 15 urls)!');
                        },
                        complete: function (complete) {

                        }
                    })
                });

                // filedownload link
                $('button.filedownload').on('click', function (e) {
                    e.preventDefault();
                    let application = $(this).attr('data-select');
                    let action = $(this).attr('data-action');
                    let processid = $(this).attr('data-processid');
                    let filename = $(this).attr('data-file');
                    $.ajax({
                        url: '/file',
                        type: 'POST',
                        data: {
                            application: application,
                            action: action,
                            processid: processid,
                            filename: filename
                        },
                        success: function (success) {
                            // window.open(success);

                            // window.location.href = 'uploads/file.doc';
                            // console.log(success);
                        },
                        error: function (error) {
                            console.log('error: ' + error);
                        }
                    });

                });

            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                $("#loaderDiv").hide();
                $(".lighthouse-charts").hide();
            }
        })
    });

    //start project test
    $('button.startbutton').on('click', function (e) {
        e.preventDefault();

        var agree=confirm("Wirklich starten?");
        if (!agree)
            return false;

        $(this).addClass('hide');
        $(this).next().removeClass('hide');
        let projectid = $(this).attr('data-project');
        let curelement = $(this);

        $('.pr' + projectid).removeClass('text-success').addClass('text-danger').text('running');

        $.ajax({
            url: '/lighthouse' + $(this).attr('data-action'),
            type: 'POST',
            data: {
                project: $(this).attr('data-project')
            },

            success: function (success) {
                //console.dir(success);
                //$('.pr' + projectid).removeClass('text-danger').addClass('text-success').text('completed');
                //$(curelement).removeClass('d-none');
                //$(curelement).next().addClass('d-none');
            },
            error: function (error) {
                console.log('error: ' + error);
                //alert('Something went wrong!');
            },
            complete: function (complete) {

            }
        })

    });

});
