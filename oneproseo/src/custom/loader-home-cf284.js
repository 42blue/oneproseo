OPSCF = {};

OPSCF.rag = {

  rankings: function(elem, vals) {

    if (vals == null || typeof vals !== 'object') {
      return;
    }

    $(elem).each(function(){
      ragData = vals;
      ragLabels = ['Platz 1-5', 'Platz 6-10', 'Platz 11-100'];
      ragOpts = {postfix:'', rgaLength: 3};
      cf_rRags[$(this).prop('id')] = new RagChart($(this).prop('id'), ragData, ragLabels, ragOpts);
    });

  }

};

OPSCF.sparkline = {

  render: function(elem, vals) {

    if (vals == null || typeof vals !== 'object') {
      return;
    }

    $(elem).each(function(){
      var sparkOptions = cf_defaultSparkOpts;
      data = vals;
      createSparkline($(this), data, sparkOptions);
    });

  }

};

OPSCF.linechart = {

  render: function(elem, vals, lables) {
  
    var data_f = new Array();

    $.each( vals, function( key, value ) {
      data_f[key] = parseFloat(value);
    });

    $(elem).each(function(){
  
      datav = vals;
      var ldata = {
        labels : lables,
        datasets : [
          {
            strokeColor : metric,
            data : data_f
          }
        ]
      }

      var $container = $(this);
      var lId = $container.prop('id');

      cf_rLs[lId] = {};
      cf_rLs[lId].data = ldata;

      customOptions = {};
      customOptions.scaleMaxMinLabels = false;
      cf_rLs[lId].options = customOptions;

      createLineChart($container);

    });

  }

};

$( document ).ready(function() {

  var cid = $('body').data('customer');

  $.getJSON(OPS.base.www + 'ajax/rankings/customdashboard/cf_dash_coredata_id', {id: cid}, function(json) {

    $('#OPSCF_dex_view').html(json.dex_view);
    OPSCF.sparkline.render('.sparkline-dex', json.dex_chart);

    $('#OPSCF_sistrix_view').html(json.sistrix_view);
    OPSCF.sparkline.render('.sparkline-sistrix', json.sistrix_chart);

    $('#OPSCF_sistrix_view_mobile').html(json.sistrix_view_mobile);
    OPSCF.sparkline.render('.sparkline-sistrix-mobile', json.sistrix_chart_mobile);

    OPSCF.rag.rankings('.cf-rankings-1', json.rankings_today);
    OPSCF.rag.rankings('.cf-rankings-2', json.rankings_week);

    $('#OPSCF_keywords').html(json.keywords);

    $('#OPSCF_keywords_ber').html(json.keywords_ber);
    $('#OPSCF_keywords_muc').html(json.keywords_muc);    
    $('#OPSCF_keywords_aug').html(json.keywords_aug);

    $('#OPSCF_figures').html(json.figures);

    $('#OPSCF_figures').html(json.searchconsole);

    $('#OPSCF_searchconsole_errors').html(json.searchconsole_errors_view);
    OPSCF.sparkline.render('.sparkline-searchconsole-errors', json.searchconsole_errors);

    $('#OPSCF_ga_traffic_view').html(json.ga_traffic_view);
    OPSCF.linechart.render('.sparkline-ga-traffic', json.ga_traffic, json.ga_traffic_lables);

    $('#OPSCF_ga_revenue_view').html(json.ga_revenue_view);
    OPSCF.linechart.render('.sparkline-ga-revenue', json.ga_revenue, json.ga_revenue_lables);

  });


});
