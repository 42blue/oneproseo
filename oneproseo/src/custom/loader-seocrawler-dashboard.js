$(document).ready(function () {
    // hides form container
    $('button').on('click', function () {
        $('#formContainer').removeClass('show');
    });
    // hides lighthouse options
    $('button#showcrawleroptions').on('click', function () {
        $('#lighthouseoptions').removeClass('show in');
        $('#lighthouseoptions').addClass('collapse');
        $('#form').empty();
    });
    // hides crawler options
    $('button#showlighthouseoptions').on('click', function () {
        $('#crawleroptions').removeClass('show in');
        $('#crawleroptions').addClass('collapse');
        $('#form').empty();
    })
    // Post requested formulars
    $('button.formbutton').on('click', function (e) {
        e.preventDefault();
        $("#loaderDiv").show();
        $('#form').empty();
        let templateFile = $(this).attr('data-template')
        let application = $(this).attr('data-select')
        let action = $(this).attr('data-action')
        let requestUrl
        if (action === 'form') {
            requestUrl = '/forms';
        }
        if (action === 'status') {
            requestUrl = '/status';
        }
        $.ajax({
            url: '/seocrawler' + requestUrl,
            type: 'POST',
            data: {
                templatefile: templateFile,
                application: application,
                action: action
            },
            success: function (success) {
                // $("#loaderDiv").hide();
                $('#form').empty();
                $('#form').append(success);
                $('#formContainer').addClass('show');
                // scroll to form
                $('html, body').animate({
                    scrollTop: $("#formContainer").offset().top
                }, 1000);
                /**
                 * Submit forms
                 */
                $('#newForm').submit(function (e) {
                    e.preventDefault();
                    console.log($(this).serialize());
                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: $(this).serialize(),
                        success: function (success) {
                            let jsonResponse = JSON.parse(success);
                            $('#newForm').fadeOut('slow', function () {
                                $('#newForm').empty();
                                $('#form').empty();
                                if (jsonResponse.response.requestId === undefined) {
                                    $('#form').append('<div class="alert alert-warning alert-dismissible fade show" role="alert">\n' +
                                        '  <strong>Holy guacamole!</strong> Something went wrong.\n' +
                                        '  <pre>' + jsonResponse.response[0] + '</pre>\n' +
                                        '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                        '    <span aria-hidden="true">&times;</span>\n' +
                                        '  </button>\n' +
                                        '</div>');
                                } else {
                                    $('#form').append('<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
                                        '  <h4 class="alert-heading">Well done!</h4>\n' +
                                        '  <p>Your crawler is successfully startet. Check the status of your crawlers.</p>\n' +
                                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                                        '    <span aria-hidden="true">&times;</span>\n' +
                                        '  </button>' +
                                        '</div>');
                                }
                            });
                        },
                        error: function (error) {
                            console.log('error: ' + error);
                            alert('Something went terribly wrong.');
                        },
                        complete: function (complete) {

                        }
                    })
                });
                // filedownload link
                $('button.filedownload').on('click', function (e) {
                    e.preventDefault();
                    let application = $(this).attr('data-select');
                    let action = $(this).attr('data-action');
                    let processid = $(this).attr('data-processid');
                    let filename = $(this).attr('data-file');
                    $.ajax({
                        url: '/file',
                        type: 'POST',
                        data: {
                            application: application,
                            action: action,
                            processid: processid,
                            filename: filename
                        },
                        success: function (success) {
                            // window.open(success);

                            // window.location.href = 'uploads/file.doc';
                            // console.log(success);
                        },
                        error: function (error) {
                            console.log('error: ' + error);
                        }
                    });

                });

            },
            error: function (error) {
                console.log(error);
            },
            complete: function () {
                $("#loaderDiv").hide();
            }
        })
    });
});
