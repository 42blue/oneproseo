$( document ).ready(function() {

  $.getScript(OPS.base.www + '/oneproseo/src/custom/functions.js', function() {

    OPS.fetchCrawlStats.coreNumbers();
    OPS.pieChartHttp.init();
    OPS.pieChartRobots.init();
    OPS.analyseTaskJson.fetch($('#ops_anaylse_head').data('taskid'));

  });

});