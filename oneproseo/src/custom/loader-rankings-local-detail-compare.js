$( document ).ready(function() {

  $.getScript(OPS.base.www + '/oneproseo/src/custom/functions.js', function() {

    OPS.fetchCrawlStats.coreNumbers();

    $('.datepicker_ruk').datepicker({
      format: 'dd.mm.yyyy',
      autoclose: true,
      startDate: '31.07.2014',
      endDate: '+0d'
    });

    OPS.localRankings.dateCompareLocal();

  });

});