$( document ).ready(function() {

  $.getScript(OPS.base.www + '/oneproseo/src/custom/functions.js', function() {

    OPS.fetchCrawlStats.coreNumbers();

			var HELPER = {

			  getWidthTT: function() {
			    var w = $(".title-opt-google").width();
			    return w;
			  },

			  getWidthMD: function() {
			    var w = $(".description-opt-google").width();
			    return w;
			  }

			};



	  var titleIn                = $(".title-ipt"),
	      titleOut               = $(".title-opt"),
	      titleOutGoogle         = $(".title-opt-google"),
	      titleVmax              = $(".title-vmax"),
	      titleChars             = 59,
	      titlePixel             = 500,
	      titleCharsR            = 0,
	      titlePixelR            = 0,
	      tt_pixel_with_desktop  = $("#tt_pixel_with_desktop"),
	      tt_words               = $(".tt_words"),
	      descriptionIn          = $("#description-ipt"),
	      descriptionOut         = $(".description-opt"),
	      descriptionOutGoogle   = $(".description-opt-google"),
	      descriptionOutMoGoogle = $(".description-opt-google-mobile"),
	      descriptionVmax        = $(".description-vmax"),
	      descriptionChars       = 160,
	      descriptionCharsR      = 0,
	      descriptionPixel       = 510,
	      descriptionPixelR      = 0,
	      descriptionMoChars     = 135,
	      descriptionMoCharsR    = 0,
	      md_pixel_with_desktop  = $("#md_pixel_with_desktop"),
	      md_chars_mobile        = $(".md_chars_mobile"),
	      md_words               = $(".md_words"),
	      urlIn                  = $("#url-ipt"),
	      urlOut                 = $(".url-opt"),
	      urlOutGoogle           = $(".url-opt-google"),
	      urlOutMoGoogle         = $(".url-opt-google-mobile"),
	      urlChars               = 75,
	      urlCharsR              = 0,
	      urlMoChars             = 42,
	      urlMoCharsR            = 0,
	      urlBreadcrumb          = '',
	      imageIn                = $("#image-ipt"),
	      imageOut               = $(".image-opt"),
	      chkbxGDate             = $("#google-date"),
	      chkbxGRich             = $("#google-richsnippet"),
	      chkbxGLink             = $("#google-sitelinks"),
	      fetchurl               = $("#fetchurl"),
	      keywordIpt             = $("#keyword-ipt"),
	      ext;

	    // catch data from site and apply to fields
	    fetchurl.click(function() {
	      if (urlIn.val() !== '') {
	        $.ajax({
	          type: "GET",
	          url: 'https://enterprise.oneproseo.com/ajaxp/tools/serp_snippet',
	          data: {url: urlIn.val()},
	          dataType: "json",
	          error:function (xhr, ajaxOptions, thrownError){},
	          success: function(data) {
	            titleIn.val(data.tt);
	            descriptionIn.val(data.md);
	            titleOut.text(data.tt);
	            descriptionOutMoGoogle.text(data.md);
	            descriptionOutGoogle.text(data.md);
	            urlOut.text(data.bc);
	            urlOutGoogle.text(data.bc);
	            urlOutMoGoogle.text(data.bc);
	            urlBreadcrumb = data.bc;
	            descriptionIn.keyup();
	            titleIn.keyup();
	            urlIn.keyup();
	          }
	        });
	      }
	    });

	    // show date and subtract length
	    chkbxGDate.change(function() {
	      var cla = $(".date-opt-google");
	      if($(this).is(":checked")) {
	        cla.show();
	        descriptionChars = descriptionChars - 13;
	        descriptionMoChars = descriptionMoChars - 13;
	      } else {
	        cla.hide();
	        descriptionChars = descriptionChars + 13;
	        descriptionMoChars = descriptionMoChars + 13;
	      }
	      descriptionIn.keyup();
	    });

	    chkbxGRich.change(function() {
	      var cla = $(".slp");
	      if($(this).is(":checked")) {
	        cla.show();
	      } else {
	        cla.hide();
	      }
	    });

	    chkbxGLink.change(function() {
	      var cla = $(".osl");
	      if($(this).is(":checked")) {
	        cla.show();
	      } else {
	        cla.hide();
	      }
	    });

	    keywordIpt.keyup(function() {
	      descriptionIn.keyup();
	      urlIn.keyup();
	    });

	    titleIn.keyup(function() {

	      var width = HELPER.getWidthTT();
	      tt_pixel_with_desktop.text(width);

	      if (!titleIn.val()) {

	        titleOutGoogle.text('diva-e | The Leading Transactional Experience Partner (TXP...');
	        titleOut.text("");

	      } else {

	        titlePixelR = titlePixel - width;
	        titleCharsR = titleChars - titleIn.val().length;
	        titleCountWords = titleIn.val().split(' ').length;
	        tt_words.text(titleCountWords);

	        ext = titleIn.val();
	        titleOut.text(ext);

	        if (titlePixelR < 0 && titleCharsR < 0) {
	          titleOutGoogle.text(ext.substr(0, 57 - 3) + ' ...');
	        } else {
	          titleOutGoogle.text(ext);
	        }

	      }

	      if (titlePixelR < 0 && titleCharsR < 0) {
	        titleVmax.css("color", "red").text('~ ' + titlePixelR  * -1 + ' px ' + 'too much' + ' / ~ ' + titleCharsR * -1 + ' ' + 'chars too much');
	      } else if (titlePixelR < 0) {
	        titleVmax.css("color", "red").text('~ ' + titlePixelR  * -1 + ' px ' + 'too much' + ' / ~ ' + titleCharsR + ' ' + 'chars left');
	      } else if (titleCharsR < 0) {
	        titleVmax.css("color", "red").text('~ ' + titlePixelR + ' px ' + 'left' + ' / ~ ' + titleCharsR * -1 + ' ' + 'chars too much');
	      } else {
	        titleVmax.css("color", "#000").text('~ ' + titlePixelR + ' px ' + 'left' + ' / ~ ' + titleCharsR + ' ' + 'chars left');
	      }

	    });


	    descriptionIn.keyup(function() {

	      descriptionCharsR = descriptionChars - descriptionIn.val().length;
	      descriptionMoCharsR = descriptionMoChars - descriptionIn.val().length;
	      md_chars_mobile.text(descriptionMoCharsR);
	      descriptionCountWords = descriptionIn.val().split(' ').length;
	      md_words.text(descriptionCountWords);

	      if (!descriptionIn.val()) {

	        descriptionOutGoogle.text('diva-e is Germanys leading Transactional Experience Partner (TXP), which creates digital experiences that inspire customers and take your  ...');
	        descriptionOutMoGoogle.text('diva-e is Germanys leading Transactional Experience Partner (TXP), which creates digital experiences that inspire customers and take your  ...');
	        descriptionOut.text("");

	      } else {

	        var width = HELPER.getWidthMD();
	        md_pixel_with_desktop.text(width);

	        descriptionPixelR = descriptionPixel - width;

	        ext = descriptionIn.val();

	        var keyword = keywordIpt.val();
	        var keywords = keyword.split(" ");
	        var keywords_regex = keywords.join('|');

	        if (keyword.length > 2) {
	          extr = ext.replace(new RegExp('(' + keywords_regex + ')','ig'), '<b>$1</b>');
	        } else {
	          extr = ext;
	        }

	        descriptionOut.text(ext);

	       if (descriptionPixelR < 0 && descriptionCharsR < 0) {
	          descriptionOutGoogle.html(extr.substr(0, descriptionChars - 3) + ' ...');
	        } else {
	          descriptionOutGoogle.html(extr);
	        }

	       if (descriptionMoCharsR < 0) {
	          descriptionOutMoGoogle.html(extr.substr(0, descriptionMoChars - 3) + ' ...');
	        } else {
	          descriptionOutMoGoogle.html(extr);
	        }
	      }

	      if (descriptionPixelR < 0 && descriptionCharsR < 0) {
	        descriptionVmax.css("color", "red").text('~ ' + descriptionPixelR * -1 + ' px ' + 'too much' + ' / ~ ' + descriptionCharsR * -1 + ' ' + 'chars too much');
	      } else if (descriptionPixelR < 0) {
	        descriptionVmax.css("color", "red").text('~ ' + descriptionPixelR * -1 + ' px ' + 'too much' + ' / ~ ' + descriptionCharsR + ' ' + 'chars left');
	      } else if (descriptionCharsR < 0) {
	        descriptionVmax.css("color", "red").text('~ ' + descriptionPixelR + ' px ' + 'left' + ' / ~ ' + descriptionCharsR * -1 + ' ' + 'chars too much');
	      } else {
	        descriptionVmax.css("color", "#000").text('~ ' + descriptionPixelR + ' px ' + 'left' + ' / ~ ' + descriptionCharsR + ' ' + 'chars left');
	      }

	    });


	    urlIn.keyup(function() {


	      if( ! urlIn.val() ) {

	        urlOut.text("seorch.de/");
	        urlOutGoogle.text("seorch.de/");

	      } else {
	        // NO BREADCRUMB
	        if (urlBreadcrumb == '') {

	          urlCharsR = urlChars - urlIn.val().length;
	          urlMoCharsR = urlMoChars - urlIn.val().length;
	          var url = urlIn.val();
	          var protomatch = /^(https?):\/\//;
	          var serpurl = url.replace(protomatch, '');

	          var ext = serpurl;

	          var keyword = keywordIpt.val();
	          var keywords = keyword.split(" ");
	          var keywords_regex = keywords.join('|');

	          if (keyword.length > 2) {
	            extr = ext.replace(new RegExp('(' + keywords_regex + ')','ig'), '<b>$1</b>');
	          } else {
	            extr = ext;
	          }

	          urlOut.text(urlIn.val());

	         if (urlCharsR < 0) {
	            urlOutGoogle.text(serpurl.substr(0, urlChars - 3) + '...');
	          } else {
	            urlOutGoogle.html(extr);
	          }

	          if (urlMoCharsR < 0) {
	            urlOutMoGoogle.text(serpurl.substr(0, urlMoChars - 3) + '...');
	          } else {
	            urlOutMoGoogle.html(serpurl);
	          }

	        } else {
	          // BREADCRUMB
	          urlOutGoogle.html(urlBreadcrumb);
	          urlOutMoGoogle.html(urlBreadcrumb);
	        }

	      }

	    });


	    imageIn.keyup(function() {
	      if( ! imageIn.val() ) {
	        imageOut.text("");
	      } else {
	        imageOut.text(imageIn.val());
	      }
	    });


  });

});