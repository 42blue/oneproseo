OPSCF = {};

OPSCF.rag = {

  rankings: function(elem, vals) {

    $(elem).each(function(){
      ragData = vals;
      ragLabels = ['Platz 1-5', 'Platz 6-10', 'Platz 11-100'];
      ragOpts = {postfix:'', rgaLength: 3};
      cf_rRags[$(this).prop('id')] = new RagChart($(this).prop('id'), ragData, ragLabels, ragOpts);
    });

  }

};

OPSCF.sparkline = {

  render: function(elem, vals) {

    $(elem).each(function(){
      var sparkOptions = cf_defaultSparkOpts;
      data = vals;
      createSparkline($(this), data, sparkOptions);
    });

  }

};

OPSCF.feed = {

  advertising: function() {

    var ops_news = $('#ops_news');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/feed/advertising',
      data: {view: 'short'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_news.html('Es ist ein Fehler aufgetreten.');
      },
      success: function(data) {
        ops_news.html(data);
      }
    });

  }

};


$( document ).ready(function() {

  var cid = $('body').data('customer');

  $.getJSON(OPS.base.www + 'ajax/rankings/customdashboard/cf_dash_coredata_id', {id: cid}, function(json) {

    $('#OPSCF_dex_view').html(json.dex_view);
    OPSCF.sparkline.render('.sparkline-dex', json.dex_chart);

    $('#OPSCF_sistrix_view').html(json.sistrix_view);
    OPSCF.sparkline.render('.sparkline-sistrix', json.sistrix_chart);

    $('#OPSCF_sistrix_view_mobile').html(json.sistrix_view_mobile);
    OPSCF.sparkline.render('.sparkline-sistrix-mobile', json.sistrix_chart_mobile);

    $('#OPSCF_sites_view').html(json.sites_view);
    OPSCF.sparkline.render('.sparkline-sites', json.sites_chart);

    OPSCF.rag.rankings('.cf-rankings-1', json.rankings_today);
    OPSCF.rag.rankings('.cf-rankings-2', json.rankings_week);

    $('#OPSCF_keywords').html(json.keywords);

    $('#OPSCF_figures').html(json.figures);

    $('#OPSCF_figures').html(json.searchconsole);


  });

  OPSCF.feed.advertising();

});
