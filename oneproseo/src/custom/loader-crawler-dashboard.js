$( document ).ready(function() {

  $.getScript(OPS.base.www + '/oneproseo/src/custom/functions.js', function() {

    OPS.newCrawlTask.init();

    OPS.fetchCrawlStats.coreNumbers();

    OPS.fetchTableData.tableData('active');
    OPS.fetchTableData.tableData('resolved');

  });

});