OPS.theme = {

  colors: {
    green: "#96c877",
    blue: "#6e97aa",
    orange: "#ff9f01",
    red: "#C75D5D",
    darkGreen: "#779148",
    gray: "#6B787F",
    lightBlue: "#D4E5DE",
  },

  erro: {
    ajax: '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Ein Fehler ist aufgetreten.</strong> Bitte informiere den Administrator.</div>'
  },

  gfx: {
    loading: '<div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div><div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div>',
    loadinglong: '<div id="ops_pulse"><div id="ops_pulse_1" class="ops_pulse"></div><div id="ops_pulse_2" class="ops_pulse"></div><div id="ops_pulse_3" class="ops_pulse"></div><div id="ops_pulse_4" class="ops_pulse"></div><div id="ops_pulse_5" class="ops_pulse"></div><div id="ops_pulse_6" class="ops_pulse"></div><div id="ops_pulse_7" class="ops_pulse"></div><div id="ops_pulse_8" class="ops_pulse"></div></div><br />Die Abfrage kann bis zu 5 Minuten dauern .. ',
    abortxhr: '<button id="OPS_abort_xhr" class="btn btn-mini btn-red">Anfrage abbrechen</button>'
  },

  msgSuccess: function(msg) {
    return '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+msg+'</strong></div>';
  },

  msgError: function(msg) {
    return '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+msg+'</strong></div>';
  }

};

// activate radiobuttons and checkbox after ajax
OPS.icheck = {
  render: function() {
    $('.icheck').iCheck({
      checkboxClass: 'icheckbox_flat-aero',
      radioClass: 'iradio_flat-aero'
    });
  }
};

OPS.basefunc = {

  removeSingleRankings: function() {
    $('#ops_multiple_show').click(function() {
      var multi = $('.ops_single').parent().remove();
    });
  },

  removeSingleRankingsPrompt: function() {
    $('#ops_multiple_show').remove();
    var multi = $('.ops_single').parent().remove();
  },

  copyToClipboard(text) {

    var textArea = document.createElement( "textarea" );
    textArea.value = text;
    document.body.appendChild( textArea );
    textArea.select();

    try {
      var successful = document.execCommand( 'copy' );
      var msg = successful ? 'erfolgreich' : 'nicht';
      alert('Die Daten wurden in die Zwischenablage kopiert!');
    } catch (err) {
      alert('Oops, unable to copy');
    }

   document.body.removeChild( textArea );

  }

};

OPS.pieChartHttp = {

  init: function() {

    var tooltipLabels = {
      names: {
        0: 'HTTP 301',
        1: 'HTTP 302',
        2: 'HTTP 404',
        3: 'HTTP 500'
      }
    };

    var colors = $.map(OPS.theme.colors, function(item) {
      return item;
    });

    var width;

    width = $(".ops-spark-pie-http").width();

    if (width > 200) {
      width = 150;
    }

    return $(".ops-spark-pie-http").sparkline([240, 177, 7, 0], {
      type: 'pie',
      height: width,
      sliceColors: colors,
      tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
      tooltipValueLookups: tooltipLabels
    });

  }

};


OPS.pieChartRobots = {

  init: function() {

    var tooltipLabels = {
      names: {
        0: 'ROBOTS: noindex',
        1: 'ROBOTS: nofollow',
        2: 'ROBOTS: noarchive',
        3: 'ROBOTS: nosnippet'
      }
    };

    var colors = $.map(OPS.theme.colors, function(item) {
      return item;
    });

    var width;

    width = $(".ops-spark-pie-robots").width();

    if (width > 200) {
      width = 150;
    }

    return $(".ops-spark-pie-robots").sparkline([10, 270, 110, 6], {
      type: 'pie',
      height: width,
      sliceColors: colors,
      tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
      tooltipValueLookups: tooltipLabels
    });

  }

};


OPS.siteLevelChart = {

  init: function() {

    if ($('#ops_chart_levels').length > 0) {

      var values, model = [], i, tt, chartdata = {},  opts = {};

      values = $('#ops_chart_values').data('sum');
      values = values.split(',');
      values = values.filter(Number);

      for (i = 0; i < values.length; i = i + 1) {
        model[i] = {'x': 'Level ' + i, 'y': parseInt(values[i])};
      }


      tt = document.createElement('div'),
      leftOffset     = -($('html').css('padding-left').replace('px', '') + $('body').css('margin-left').replace('px', '')),
      topOffset      = -32;
      tt.className   = 'ex-tooltip';
      document.body.appendChild(tt);

      chartdata = {
        "xScale": "ordinal",
        "yScale": "linear",
        "main": [{
                  "className": ".pizza",
                  "data": model
                }]
        };

      opts = {
        paddingLeft: 55,
        paddingRight: 25,
        paddingTop: 25,
        axisPaddingTop: 5,
        axisPaddingLeft: 5,

        mouseover: function (d, i) {
            var pos = $(this).offset();
            $(tt).text((d.x) + ': ' + d.y + ' Seiten')
                .css({top: topOffset + pos.top, left: pos.left + leftOffset})
                .show();
        },
        "mouseout": function (x) { $(tt).hide(); }
      };

      new xChart('line-dotted', chartdata, '#ops_chart_levels', opts);


      }

  }

};


OPS.pieAnimation = {

  init: function(id) {

    $(id).easyPieChart({
      animate: 1000,
      trackColor: "#5d5d5d",
      scaleColor: "#5d5d5d",
      lineCap: 'square',
      lineWidth: 15,
      size: 150,
      barColor: function(percent) {
        // return "rgb(" + Math.round(200 * percent / 100) + ", " + Math.round(200 * (1 - percent / 100)) + ", 0)";
        return "rgb(22, 178, 0)";
      }
    });

  }

};

// SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION - SIDEBAR ANIMATION

OPS.fetchCrawlStats = {

  coreNumbers: function() {

    OPS.pieAnimation.init('#ops_ruk_keywords');
    OPS.pieAnimation.init('#ops_ruk_urls');
    OPS.pieAnimation.init('#ops_crawler_pie');

  }

};

// HISTORICAL LOCAL AND RUK CHART

OPS.historicalRankings = {

  highchart: function(chartdata, elem) {

    if (typeof chartdata === 'undefined'){
      chartdata = '';
    }

    if (typeof elem === 'undefined'){
      elem = '#ops-histochart';
    }

    var htmlelem = $(elem);
    var series_data = new Array();
    var i = 0;
    chartdata.charts.forEach(function(entry) {
      series_data[i] = {visible: true, name: entry.name, yAxis: 0, color: entry.color, data: entry.series};
      i++;
    });

    var yAxis_data  = [{type: 'logarithmic', min:1, minRange: 1, max: 100, title: { text: 'POSITION' }, labels: {formatter: function () {if (this.value  == 0) { return 1 } else { return this.value };}}, reversed: true, minorTickInterval: 0.5}];

    if(typeof chartdata === 'undefined'){
      $(htmlelem).html('<div style="display: block; width: 240px; margin: 0 auto; font-size: 20px;">Keine Daten verfügbar.</div>');
    } else {
      $(htmlelem).highcharts({
        chart:   {type: 'spline'},
        title:   {text: chartdata.ctitle, align: 'center'},
        legend:  {align: 'right', verticalAlign: 'top', x: 0,y: 0,  borderWidth: 1, backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'},
        tooltip: {shared: true, pointFormat: '<span style="color:{point.color}">-</span> {series.name}: <b>{point.y}</b><br/>'},
        credits: {enabled: false},
        xAxis:   {categories: chartdata.categories, reversed: true},
        yAxis:   yAxis_data,
        series:  series_data
      });
    }

  }

};


// KEYWORDER UND W-FRAGEN - KEYWORDER UND W-FRAGEN - KEYWORDER UND W-FRAGEN - KEYWORDER UND W-FRAGEN - KEYWORDER UND W-FRAGEN - KEYWORDER UND W-FRAGEN - KEYWORDER UND W-FRAGEN

OPS.keyword = {

  init: function(type) {

    var ops_kolibri_task     = $('#ops_kolibri_task'),
        ops_kolibri_kw       = $('#ops_kolibri_kw'),
        ops_kolibri_url      = $('#ops_kolibri_url'),
        ops_kolibri_lang     = $('#ops_kolibri_lang'),
        ops_kolibri_source   = $('#ops_kolibri_source'),
        ops_kolibri_rankings = $(':radio[name=ops_kolibri_rankings]'),
        ops_kolibri_toggle   = $('#ops_kolibri_toggle'),
        show_rankings        = true;

    ops_kolibri_rankings.on('ifClicked', function(event){
      if ($(this).val() == 'true') {
        ops_kolibri_toggle.show();
      } else {
        ops_kolibri_toggle.hide();
      }
      show_rankings = ($(this).val());
    });

    // INTERCEPT GET PARAMS
    var get_params = window.location.search.substring(1);
    if (get_params !== '') {
      this.getInterceptor(type, get_params);
    }

    // SUBMIT
    ops_kolibri_task.submit(function(event) {
      event.preventDefault();
      OPS.keyword.query(type, ops_kolibri_url.val(), ops_kolibri_kw.val(), ops_kolibri_lang.val(), show_rankings, ops_kolibri_source.val());
      // MODIFIY URL
      OPS.keyword.historyModifier(ops_kolibri_kw.val(), ops_kolibri_lang.val(), ops_kolibri_url.val());
    });

  },

  getInterceptor: function(type, params) {

    var query = params.split('&');
    var url = '', lang = '';

    for (var i = 0; i < query.length; i++) {

      var pair = query[i].split('=');

      if (pair[0] === 'url') {
        url = decodeURIComponent(pair[1]);
      }
      if (pair[0] === 'lang') {
        lang = pair[1];
      }
      if (pair[0] === 'kw') {
        keyword = decodeURIComponent(pair[1]);
      }

    }

    // START QUERY
    if (keyword !== '' && lang !== '') {

      $('#ops_kolibri_kw').val(keyword);
      $('#ops_kolibri_url').val(url);
      $('#ops_kolibri_lang').val(lang);

      if (url !== '') {
        $('#iradio1').iCheck('check');
        $('#ops_kolibri_toggle').toggle();
      }

      if (type != 'ideaskw') {
        OPS.keyword.query(type, url, keyword, lang);
      }

    }

  },


  historyModifier: function(keyword, lang, url) {

    var urlpart = '';

    if (url) {
      urlpart = "?url=" + url + "&kw=" + keyword  + "&lang=" + lang;
    } else {
      urlpart = "?kw=" + keyword  + "&lang=" + lang;
    }

    history.pushState('', '', urlpart);

  },


  query: function(type, url, kw, lang, show_rankings, source) {

    var ops_kolibri_submit  = $('#ops_kolibri_submit'),
        ops_kolibri_result  = $('#ops_kolibri_result'),
        ops_result_block    = $('#ops_result_block'),
        ops_loading_block   = $("#ops_loading_block");

    ops_result_block.addClass("hidden");
    ops_loading_block.removeClass("hidden")
    ops_result_block.html(OPS.theme.gfx.loading);
    ops_kolibri_submit.attr('disabled', true);

    var checkurl = decodeURIComponent(url);

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/keywords/ideas',
      data: { kw: kw, url: checkurl, lang: lang, type: type, rankings: show_rankings, source: source },
      error: function (xhr, ajaxOptions, thrownError) {
        ops_kolibri_result.html('ERROR');
        ops_kolibri_submit.attr('disabled', false);
      },
      success: function(data) {
       ops_loading_block.addClass("hidden");
       ops_result_block.removeClass("hidden");
       ops_result_block.html(data);
       ops_kolibri_submit.attr('disabled', false);
       OPS.dataTable.columnsearch('.data-table', 2, 'asc');
       OPS.googleDrive.fileSubmit();
      }
    });

  }

};

// INDEXED URLS INDEXED URLS INDEXED URLS INDEXED URLS INDEXED URLS INDEXED URLS INDEXED URLS INDEXED URLS INDEXED URLS

OPS.indexcheck = {

  show: function() {

    var ops_show_block = $('#ops_show_block');
    ops_show_block.html(OPS.theme.gfx.loading);

    $.ajax({
      type: "GET",
      url: OPS.base.www + 'ajax/structure/indexcheck/urls_index_show',
      error: function (xhr, ajaxOptions, thrownError) {
        ops_show_block.html('ERROR');
      },
      success: function(data) {
        ops_show_block.html(data);
        OPS.indexcheck.delete();
        OPS.dataTable.columnsearch('.data-table', 3, 'desc');
      }

    });

  },

  delete: function() {

    var job_id,
        ops_delete_job = $('.ops_delete_job');

    ops_delete_job.click(function() {

      job_id = this.getAttribute('data-jobid');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/structure/indexcheck/urls_index_delete',
        data: {job_id: job_id},
        error: function (xhr, ajaxOptions, thrownError) {},
        success: function(data) {
          OPS.indexcheck.show();
        }
      });

    });

  },

  submit: function() {

    var ops_form       = $('.ops_form'),
        ops_show_block = $('#ops_add_block'),
        ops_submit     = $('.ops_submit');

    ops_form.submit(function(event) {

      var data = $(this).serialize();

      ops_submit.attr('disabled', true);

      event.preventDefault();

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/structure/indexcheck/urls_index_add',
        data: { data: data },
        error: function (xhr, ajaxOptions, thrownError) {
          ops_show_block.html('ERROR');
        },
        success: function(data) {
          ops_show_block.html(OPS.theme.msgSuccess(data));
          OPS.indexcheck.show();
        }

      });

    });

  },

  job: function() {

    var job_id         = $('#ops_project').data('jobid'),
        ops_show_block = $('#ops_show_block');

    ops_show_block.html('<div class="box">' + OPS.theme.gfx.loading + '</div>');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/structure/indexcheck/urls_index_job_show',
      data: { job_id: job_id },
      error: function (xhr, ajaxOptions, thrownError) {
        ops_show_block.html('ERROR');
      },
      success: function(data) {
        ops_show_block.html(data);
        OPS.dataTable.columnsearch('.data-table', 1, 'asc');
        OPS.googleDrive.fileSubmit();
        OPS.csv.download();
      }

    });

  },

};


// LINK STRUCTURE TOOL CORE - STEP 1 - LINK STRUCTURE TOOL CORE - STEP 1 - LINK STRUCTURE TOOL CORE - STEP 1 - LINK STRUCTURE TOOL CORE - STEP 1 - LINK STRUCTURE TOOL CORE - STEP 1

OPS.structure_core = {

  init: function() {

    var ops_silo_form         = $('#ops_silo_form'),
        ops_silo_kw           = $('#ops_silo_kw'),
        ops_silo_url          = $('#ops_silo_url'),
        ops_start_block       = $('#ops_start_block'),
        ops_silo_hint         = $('#ops_silo_hint'),
        ops_silo_submit       = $('#ops_silo_submit'),
        ops_result_block      = $('#ops_result_block'),
        ops_loading_block     = $("#ops_loading_block");

    // INTERCEPT GET PARAMS
    var get_params = window.location.search.substring(1);
    if (get_params !== '') {
      this.getInterceptor(get_params);
    }

    // SUBMIT
    ops_silo_form.submit(function(event) {

      event.preventDefault();
      // START
      OPS.structure_core.query(ops_silo_kw.val(), ops_silo_url.val());
      // MODIFIY URL
      OPS.structure_core.historyModifier(ops_silo_kw.val(), ops_silo_url.val());

      ops_result_block.addClass("hidden");
      ops_loading_block.removeClass("hidden");
      ops_result_block.html(OPS.theme.gfx.loading);
      ops_silo_submit.attr('disabled', true);

    });

  },

  getInterceptor: function(params) {

    var query = params.split('&');
    var url = '', lang = '';

    for (var i = 0; i < query.length; i++) {

      var pair = query[i].split('=');

      if (pair[0] === 'url') {
        url = decodeURIComponent(pair[1]);
      }
      if (pair[0] === 'kw') {
        keyword = decodeURIComponent(pair[1]);
      }

    }

    // START QUERY
    if (keyword !== ''  && url !== '') {
      $('#ops_silo_kw').val(keyword);
      $('#ops_silo_url').val(url);
      OPS.structure_core.query(keyword, url);
    }

  },


  historyModifier: function(kw, url) {
    var urlpart = "?kw=" + kw  + "&url=" + url;
    history.pushState('', '', urlpart);
  },


  query: function(kw, url) {

    var ops_kolibri_submit  = $('#ops_kolibri_submit'),
        ops_result_block    = $('#ops_result_block'),
        ops_loading_block   = $("#ops_loading_block");

    ops_result_block.addClass("hidden");
    ops_loading_block.removeClass("hidden");
    ops_kolibri_submit.attr('disabled', true);

    ops_abort_xhr = $("#ops_abort_xhr");

    var xhr_running = $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/structure/link_silo_core',
      data: { kw: kw, url: url },
      error: function (xhr, ajaxOptions, thrownError) {
        ops_result_block.html('ERROR');
        ops_silo_submit.attr('disabled', false);
      },
      success: function(data) {
        ops_loading_block.addClass("hidden");
        ops_result_block.removeClass("hidden");
        ops_result_block.html(data);
        OPS.icheck.render();
        OPS.structure.init();
      }
    });

    ops_abort_xhr.click(function() {
      xhr_running.abort();
      ops_loading_block.addClass("hidden");
    });

  }

};

OPS.tools = {

  structuredData: function() {

    OPS.icheck.render();

    $("#ops_select_type").change(function() {
      var show = $(this).val();
      $('#ops_faq').hide();
      $('#ops_video').hide();
      $('#'+show).show();
    });

    // FAQ
    OPS.tools.structuredDataFAQQuestion = [];
    OPS.tools.structuredDataFAQAnswer = [];
		OPS.tools.structuredDataFAQAnswerLink = [];
    OPS.tools.structuredDataFAQAnswerLinkText = [];    
    
    var addQuestion = $("#ops_jsonld_add_question");

    OPS.tools.structuredDataFAQ();

    addQuestion.click(function() {
      $( '<div class="form-group" style="margin-top:30px;"><label class="label-2 control-label" for="question">Question:</label><div><textarea class="col-sm-12" name="question" required="required"></textarea></div></div><label class="label-2 control-label" for="answer">Answer:</label><div class="form-group jsonld-answerbox"><div><textarea class="col-sm-12" name="answer" required="required"></textarea></div><br/><label class="label-2 control-label" for="answer_link">Link in answer (optional):</label><div><input type="url" name="answer_link" class="col-sm-12"></div><br/><label class="label-2 control-label" for="answer_link_text">Linktext in answer (optional):</label><div><input type="url" name="answer_link_text" class="col-sm-12"></div></div>').insertBefore(addQuestion);
      OPS.tools.structuredDataFAQ();
    });

    $('#ops_copy_faq').click(function() {
      OPS.basefunc.copyToClipboard($('#ops_jsonld_out').text());
    });

    // VIDEO
    OPS.tools.structuredDataVideo();

    $('#ops_copy_video').click(function() {
      OPS.basefunc.copyToClipboard($('#ops_jsonld_video_out').text());
    });
  },


  structuredDataVideo: function() {

    var 
        vid_name  = $('textarea[name="video_name"]'), 
        vid_desc  = $('textarea[name="video_description"]'),
        vid_url   = $('input[name="video_url"]'),
        vid_date  = $('input[name="video_upload_date"]'),
        vid_min   = $('select[name="video_minutes"]'),        
        vid_sec   = $('select[name="video_seconds"]'),
        video_out = $('#ops_jsonld_video_out');

    $(vid_name).add(vid_desc).add(vid_url).add(vid_date).add(vid_min).add(vid_sec).on('keydown change',function() {

			var duration = 'PT00H' + vid_min.val() + 'M' + vid_sec.val() + 'S';
	    var json = {"@context": "https://schema.org", "@type": "VideoObject", "name": vid_name.val(), "description": vid_desc.val(), "thumbnailUrl": vid_url.val(), "uploadDate": vid_date.val(), "duration": duration};
	    var out  = '&lt;script type=\"application/ld+json\"&gt;\n' + JSON.stringify(json,null,4) + '\n&lt;/script&gt;';

	    video_out.html(out);

    });

  },

  structuredDataFAQ: function() {

    var 
        faq_question   = $('textarea[name="question"]'), 
        faq_answer     = $('textarea[name="answer"]'),
        faq_link       = $('input[name="answer_link"]'),
        faq_link_text  = $('input[name="answer_link_text"]'),
        question       = [];

    faq_question.keyup(function() {
      faq_question.each(function( index ) {
        var q = $(this).val();
        if (q != '') {
          OPS.tools.structuredDataFAQQuestion[index] = q;
          OPS.tools.structuredDataFAQrender();
        }
      });
    });

    faq_answer.keyup(function() {
      faq_answer.each(function( index ) {
        var a = $(this).val();
        if (a != '') {
          OPS.tools.structuredDataFAQAnswer[index] = a;
          OPS.tools.structuredDataFAQrender();
        }
      });
    });

    faq_link.keyup(function() {
      faq_link.each(function( index ) {
        var a = $(this).val();
        if (a != '') {
          OPS.tools.structuredDataFAQAnswerLink[index] = a;
          OPS.tools.structuredDataFAQrender();
        }
      });
    });

    faq_link_text.keyup(function() {
      faq_link_text.each(function( index ) {
        var a = $(this).val();
        if (a != '') {
          OPS.tools.structuredDataFAQAnswerLinkText[index] = a;
          OPS.tools.structuredDataFAQrender();
        }
      });
    });

  },

  structuredDataFAQrender: function() {
    
    var faq_out = $('#ops_jsonld_out');
    var jj      = [];

    OPS.tools.structuredDataFAQQuestion.forEach(function(question, index) {

    	var link = '';
    	if (OPS.tools.structuredDataFAQAnswerLink[index]) {
        var linktext = 'Link'
        if (OPS.tools.structuredDataFAQAnswerLinkText[index]) {
          var linktext = OPS.tools.structuredDataFAQAnswerLinkText[index];
        }
    		var link = '\n&lt;a href="'+OPS.tools.structuredDataFAQAnswerLink[index]+'"&gt;'+linktext+'&lt;/a&gt;';	
    	}

    	var answer = '';
    	if (OPS.tools.structuredDataFAQAnswer[index]) {
    		answer = OPS.tools.structuredDataFAQAnswer[index];
    	}

      jj.push({"@type":"Question","name":question,"acceptedAnswer":{"@type":"Answer","text":answer+link}});
    });

    var json = {"@context":"https://schema.org","@type":"FAQPage","mainEntity":jj};
    var out  = '&lt;script type=\"application/ld+json\"&gt;\n' + JSON.stringify(json,null,4) + '\n&lt;/script&gt;';

    faq_out.html(out);

  },


  searchConsole: function() {

    var ops_data = $('#ops_data');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/search-console/overview',
      error: function (xhr, ajaxOptions, thrownError) {
        ops_data.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_data.html(data);
        OPS.dataTable.init(1, 'asc', false);
      }
    });

  },

  searchConsoleDetail: function() {

    var ops_data = $('#ops_data');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/search-console/detail',
      error: function (xhr, ajaxOptions, thrownError) {
        ops_data.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_data.html(data);
        OPS.tools.searchConsoleDetailAdd();
        OPS.tools.searchConsoleDetailDelete();
      }
    });

  },


  searchConsoleDetailAdd: function() {

    var ops_add_block = $('#ops_add_block');

    var ops_form       = $('.ops_form'),
        ops_show_block = $('#ops_add_block'),
        ops_submit     = $('.ops_submit');

    ops_form.submit(function(event) {

      event.preventDefault();
      
      var data = $(this).serialize();

        $.ajax({
          type: "POST",
          url: OPS.base.www + 'ajax/search-console/detail',
          data: { action: 'add', data: data},
          error: function (xhr, ajaxOptions, thrownError) {
            ops_data.html(OPS.theme.erro.ajax);
          },
          success: function(data) {
            ops_add_block.html(data);
            OPS.tools.searchConsoleDetail();
          }
        });

    });

  },

  searchConsoleDetailDelete: function() {

    var del = $('.ops_delete_job');

    del.click(function() {

      var jobid  = this.getAttribute('data-jobid');

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/search-console/detail',
        data: {action: 'delete', jobid: jobid},      
        error: function (xhr, ajaxOptions, thrownError) {
          ops_data.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.tools.searchConsoleDetail();
        }
      });

    });

  },


  rankingsSearchvolume: function() {

    OPS.icheck.render();

    var ops_type     = $(':radio[name=ops_type]'),
    ops_rasv_toggle  = $('#ops_rasv_toggle'),
    ops_rasv_url     = $('#ops_rasv_url');

    ops_type.on('ifClicked', function(event){

      if ($(this).val() == 'sv') {
        ops_rasv_toggle.hide();
        ops_rasv_url.prop('required',false);
      } else {
        ops_rasv_toggle.show();
        ops_rasv_url.prop('required',true);
      }
    });

    var ops_show = $('#ops_show_block');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/rankings-searchvolume/overview',
      data: { action: 'show'},      
      error: function (xhr, ajaxOptions, thrownError) {
        ops_data.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_show.html(data);
        OPS.tools.rankingsSearchvolumeAdd();
        OPS.tools.rankingsSearchvolumeDelete();        
      }
    });

  },  


  rankingsSearchvolumeAdd: function() {

    var ops_add_block  = $('#ops_add_block')
        ops_form       = $('#ops_rasv'),
        ops_submit     = $('.ops_submit');

    ops_form.submit(function(event) {

      event.preventDefault();
      
      var data = $(this).serialize();

        $.ajax({
          type: "POST",
          url: OPS.base.www + 'ajax/rankings-searchvolume/overview',
          data: { action: 'add', data: data },
          error: function (xhr, ajaxOptions, thrownError) {
            ops_data.html(OPS.theme.erro.ajax);
          },
          success: function(data) {
            ops_add_block.html(data);
            OPS.tools.rankingsSearchvolume();
          }
        });

    });

  },

  rankingsSearchvolumeDelete: function() {

    var del = $('.ops_delete_job');

    del.click(function() {

      var jobid  = this.getAttribute('data-jobid');

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/rankings-searchvolume/overview',
        data: {action: 'delete', jobid: jobid},      
        error: function (xhr, ajaxOptions, thrownError) {
          ops_data.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          location.reload();
        }
      });

    });

  },

  rankingsSearchvolumeDetail: function() {

    var jobid  = $('#ops_project').data('jobid'),
        ops_show_block = $('#ops_show_block');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/rankings-searchvolume/detail',
      data: { jobid: jobid},      
      error: function (xhr, ajaxOptions, thrownError) {
        ops_data.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_show_block.html(data);
        OPS.dataTable.init(1, 'asc', false);
        OPS.googleDrive.fileSubmit();
        OPS.csv.download();
      }
    });

  }

};

// LINK STRUCTURE TOOL - STEP 2 - LINK STRUCTURE TOOL - STEP 2 - LINK STRUCTURE TOOL - STEP 2 - LINK STRUCTURE TOOL - STEP 2 - LINK STRUCTURE TOOL - STEP 2 - LINK STRUCTURE TOOL - STEP 2

OPS.structure = {

  poll_filename: '',

  init: function() {

    var ops_silo_task       = $('#ops_silo_task'),
        ops_silo_kw         = $('#ops_silo_kw'),
        ops_silo_url        = $('#ops_silo_url'),
        ops_result_block    = $('#ops_result_block'),
        ops_loading_block   = $("#ops_loading_block");

    var poll_filename = ops_silo_task.data('filename');

    $('input[type="radio"]').on('ifChecked', function(event){
      $('#ops_silo_url_find').val($(this).val());
    });

    // SUBMIT
    ops_silo_task.submit(function(event) {
      event.preventDefault();
      var ops_silo_url_find = $('#ops_silo_url_find');
      OPS.structure.query(ops_silo_kw.val(), ops_silo_url.val(), ops_silo_url_find.val(), poll_filename);
    });

  },

  query: function(kw, url, find, poll_filename) {

    var ops_kolibri_submit  = $('#ops_kolibri_submit'),
        ops_kolibri_result  = $('#ops_kolibri_result'),
        ops_result_block    = $('#ops_result_block'),
        ops_loading_block   = $("#ops_loading_block");

    ops_result_block.addClass("hidden");
    ops_loading_block.removeClass("hidden");

    ops_abort_xhr = $("#ops_abort_xhr");

    ops_result_block.html(OPS.theme.gfx.loading);
    ops_kolibri_submit.attr('disabled', true);

    var that = this;
    var interval = setInterval( function() { that.doPoll(poll_filename); }, 500 );

    var xhr_running = $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/structure/link_silo',
      data: { kw: kw, url: url, url_find: find, poll: poll_filename },
      error: function (xhr, ajaxOptions, thrownError) {
        ops_kolibri_result.html('ERROR');
        ops_kolibri_submit.attr('disabled', false);
      },
      success: function(data) {
       clearInterval(interval);
       ops_loading_block.addClass("hidden");
       ops_result_block.removeClass("hidden");
       ops_result_block.html(data);
       ops_kolibri_submit.attr('disabled', false);
       OPS.dataTable.columnsearch('#opo_dt_1', 3, 'asc');
       OPS.dataTable.columnsearch('#opo_dt_2', 3, 'asc');
       OPS.dataTable.columnsearch('#opo_dt_3', 2, 'asc');
       OPS.googleDrive.fileSubmit();
       OPS.csv.download();
      }
    });

    ops_abort_xhr.click(function() {
      xhr_running.abort();
      ops_loading_block.addClass("hidden");
      $('#ops_silo_hint').removeClass("hidden");
      $('#ops_start_block').removeClass("hidden");
    });

  },

  doPoll: function(filename){
    $.post(OPS.base.www + 'temp/export/linksilo-'+filename+'.txt', function(data) {
      $('#ops_poll_info').html(data);
    });
  }

};


// ADWORDS SEARCH VOLUME - ADWORDS SEARCH VOLUME - ADWORDS SEARCH VOLUME - ADWORDS SEARCH VOLUME - ADWORDS SEARCH VOLUME - ADWORDS SEARCH VOLUME - ADWORDS SEARCH VOLUME

OPS.newSearchVolumeTask = {

  init: function() {

    var crawldata, errormsg,
        ops_searchvolume_task     = $("#ops_searchvolume_task"),
        ops_searchvolume_result   = $("#ops_searchvolume_result"),
        ops_searchvolume_submit   = $("#ops_searchvolume_submit"),
        ops_result_block          = $("#ops_result_block"),
        ops_loading_block         = $("#ops_loading_block");

    ops_searchvolume_task.submit(function(event) {

      var ops_searchvolume_lang     = $("#ops_searchvolume_lang"),
          ops_searchvolume_keywords = $("#ops_searchvolume_keywords"),
          ops_searchvolume_monthly  = $('.ops_searchvolume_monthly:checked').val();

      ops_result_block.addClass("hidden");
      ops_loading_block.removeClass("hidden");
      ops_searchvolume_result.html(OPS.theme.gfx.loading);
      ops_searchvolume_submit.attr("disabled", true);

      event.preventDefault();

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/analytics/searchvolume',
        data: { keywords: ops_searchvolume_keywords.val(), lang: ops_searchvolume_lang.val(), monthly: ops_searchvolume_monthly},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_searchvolume_result.html('ERROR');
          ops_searchvolume_submit.attr("disabled", false);
        },
        success: function(data) {
          ops_loading_block.addClass("hidden");
          ops_result_block.removeClass("hidden");
          ops_result_block.html(data);
          OPS.dataTable.init(1, 'desc', false);
          ops_searchvolume_submit.attr("disabled", false);
          OPS.googleDrive.fileSubmit();
        }

      });

    });

  }

};


// LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL - LIVE RANKINGS TOOL

OPS.liveRankings = {

  init: function() {

    var ops_task      = $("#ops_liverankings"),
        ops_keywords  = $("#ops_liverankings_keywords"),
        ops_url       = $("#ops_liverankings_url"),
        ops_lang      = $("#ops_liverankings_lang"),
        ops_version   = $("#ops_liverankings_type"),        
        ops_submit    = $("#ops_liverankings_submit"),
        ops_loading   = $("#ops_loading_block"),
        ops_result    = $("#ops_result_block");

    ops_task.submit(function(event) {

      var ops_subdomain = $("input[name=ops_liverankings_subdomain]:checked").val();
      ops_result.addClass("hidden");
      ops_loading.removeClass("hidden");
      ops_result.html(OPS.theme.gfx.loading);
      ops_submit.attr("disabled", true);

      event.preventDefault();

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/rankings/liverankings',
        data: { keywords: ops_keywords.val(), lang: ops_lang.val(), url: ops_url.val(), subdomain: ops_subdomain, type: ops_version.val()},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_result.html('ERROR');
          ops_submit.attr("disabled", false);
        },
        success: function(data) {
          ops_loading.addClass("hidden");
          ops_result.removeClass("hidden");
          ops_result.html(data);
          OPS.dataTable.init(1, 'asc', false);
          ops_submit.attr("disabled", false);
          OPS.googleDrive.fileSubmit();
        }

      });

    });

  }

};


// PANDA CLINIC TOOL - PANDA CLINIC TOOL - PANDA CLINIC TOOL - PANDA CLINIC TOOL - PANDA CLINIC TOOL - PANDA CLINIC TOOL - PANDA CLINIC TOOL - PANDA CLINIC TOOL 

OPS.siteclinic = {

  init: function() {

    var ops_result = $("#ops_content");

    $.ajax({
      type: "GET",
      url: OPS.base.www + 'ajax/siteclinic/domains',
      error: function (xhr, ajaxOptions, thrownError) {
        ops_result.html('ERROR');
      },
      success: function(data) {
        ops_result.html(data);
        OPS.dataTable.init(0, 'asc', false);

        $('.ops_siteclinic_start').click(function() {
          var host  = this.getAttribute('data-hostname');
          OPS.siteclinic.startFromDomain(host, $(this));
        });

      }

    });

  },
  
  host: function () {

    var ops_result = $('#ops_host');

    var host = ops_result.data('hostname');

    $.ajax({

      type: "POST",
      url: OPS.base.www + 'ajax/siteclinic/host',
      data: {host: host},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_result.html('ERROR');
      },
      success: function(data) {
        ops_result.html(data);
        OPS.dataTable.init(0, 'desc', false);
        OPS.siteclinic.start();
        OPS.siteclinic.delete();
      }

    });

  },

  startFromDomain: function (host, elem) {

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/siteclinic/host',
      data: {host: host, action: 'start'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_result.html('ERROR');
      },
      success: function(data) {
        elem.off();
        elem.text('läuft');
      }

    });

  },

  start: function () {

    var start = $('#ops_siteclinic_start');

    start.click(function() {

      var host  = this.getAttribute('data-hostname');

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/siteclinic/host',
        data: {host: host, action: 'start'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_result.html('ERROR');
        },
        success: function(data) {
          OPS.siteclinic.host();
        }

      });

    });

  },

  delete: function () {

    var del = $('.ops_siteclinic_delete');

    del.click(function() {

      var host  = this.getAttribute('data-hostname');
      var date  = this.getAttribute('data-date');

      $.ajax({

        type: "POST",
        url: OPS.base.www + 'ajax/siteclinic/host',
        data: {host: host, date: date, action: 'delete'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_result.html('ERROR');
        },
        success: function(data) {
          OPS.siteclinic.host();
        }

      });

    });

  },


  detail: function (action) {

    var ops_result = $('#ops_host');

    var host = ops_result.data('hostname');
    var date = ops_result.data('date');

    $.ajax({

      type: "POST",
      url: OPS.base.www + 'ajax/siteclinic/detail',
      data: {host: host, date: date, action: action},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_result.html('ERROR');
      },
      success: function(data) {
        ops_result.html(data);
        OPS.dataTable.init(0, 'desc', true);

        OPS.siteclinic.columRangeFilter(5);
        OPS.siteclinic.columRangeFilter(6);
        OPS.siteclinic.columRangeFilter(7);
        OPS.siteclinic.columRangeFilter(8);
        OPS.siteclinic.columRangeFilter(9);        
        OPS.siteclinic.columRangeFilter(10);
        OPS.siteclinic.columRangeFilter(11);
        OPS.siteclinic.columRangeFilter(12);
        OPS.siteclinic.columRangeFilter(13);
        OPS.siteclinic.columRangeFilter(14);        

        $('table td input').click(function(e) {
          e.stopPropagation();
        });

        OPS.googleDrive.fileSubmit();
      }

    });

  },

  columRangeFilter: function (col) {

    $.fn.dataTable.ext.search.push (

      function( settings, data, dataIndex, obj ) {

        var min = parseFloat( $('#min'+col).val());
        var max = parseFloat( $('#max'+col).val());
        var val = 0;

        if (data[col]) {
         val = obj[col]['@data-order'];
        }

        var age = parseFloat( val ) || 0;

        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age && isNaN( max ) ) ||
             ( min <= age && age <= max ) )
        {
          return true;
        }

        return false;

      }
    );

  }

};



// CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER - CRAWLER

OPS.fetchTableData = {

  deleteCrawl: function(crawl_id, table) {

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/crawler/deletecrawl',
      data: {id: crawl_id},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_active_crawls.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        OPS.fetchTableData.tableData(table);
      }
    });

  },


  deleteCrawlListener: function(table) {

    var crawl_id,
        ops_delete_crawl = $( '.ops_delete_crawl_' + table),
        ops_reload_crawls = $('#ops_crawls_' + table);

    ops_delete_crawl.click(function() {

      crawl_id = this.getAttribute('data-crawlid');

      ops_reload_crawls.html(OPS.theme.gfx.loading);

      OPS.fetchTableData.deleteCrawl(crawl_id, table);

    });

  },


  tableData: function(table) {

    var divid   = '#ops_crawls_' + table,
        ops_id  = $(divid);

    if (ops_id.length > 0) {

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/crawler/dashboard',
        data: {status: table},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_active_crawls.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          data = $.parseJSON(data);
          ops_id.html(data.active);
          OPS.fetchTableData.deleteCrawlListener(table);
        }
      });

    }

  }

};


OPS.newCrawlTask = {

  init: function() {

    var crawldata, errormsg,
        ops_crawl_task    = $( "#ops_crawl_task"),
        ops_crawl_submit  = $( "#ops_crawl_submit"),
        ops_flash_message = $( "#ops_flash_message");

    ops_crawl_task.submit(function(event) {

      ops_crawl_submit.attr("disabled", true);

      event.preventDefault();
      crawldata = ops_crawl_task.serialize();

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/crawler/newcrawl',
        data: crawldata,
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.append(OPS.theme.erro.ajax);
        },
        success: function(data) {
          data = $.parseJSON(data);
          errormsg = '';
          if (data.success) {
            ops_flash_message.append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+data.success+'</strong></div>');
          } else {
            for (var elem in data.error) {
             errormsg +=  data.error[elem] + '<br />';
            }
            ops_flash_message.append('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+errormsg+'</strong></div>');
          }

         ops_crawl_submit.attr("disabled", false);

        }

      });

    });

  }

};


OPS.csvDownload = {

  init: function() {

    var csv_request = $('.csv-request'),
        drive_request = $('.google-drive-request');

    csv_request.click(function() {

      var that = $(this);

      var cc = that.data('crawlcollection'),
          tt = that.data('type');

          that.removeClass('icon-cloud-download');
          that.addClass('icon-spinner icon-spin');

        $.ajax({
          type: "GET",
          url: OPS.base.www + 'ajax/crawler/analyse/csv_request',
          data: {collid: cc, type: tt},
          error: function (xhr, ajaxOptions, thrownError) {
            that.removeClass('icon-spinner icon-spin');
            that.addClass('icon-warning-sign');
          },
          success: function(data) {
            that.removeClass('icon-spinner icon-spin');
            that.addClass('icon-download');
            that.off('click');
            that.click(function() {
              window.open(OPS.base.www + OPS.base.temp + data);
            });
          }

        });

    });

    drive_request.click(function() {

      var that = $(this);

      var cc = that.data('crawlcollection'),
          tt = that.data('type');

          that.removeClass('icon-google-plus-sign');
          that.addClass('icon-spinner icon-spin');

        $.ajax({
          type: "GET",
          url: OPS.base.www + 'ajax/crawler/analyse/csv_request',
          data: {collid: cc, type: tt, show: 'drive'},
          error: function (xhr, ajaxOptions, thrownError) {
            that.removeClass('icon-spinner icon-spin');
            that.addClass('icon-warning-sign');
          },
          success: function(data) {
            that.removeClass('icon-spinner icon-spin');
            that.addClass('icon-google-plus');
            that.off('click');
            that.click(function() {
              window.open(data);
            });
          }

        });

    });

  }

};


OPS.analyse = {

  fetch: function(collection) {

    var ops_anaylse_head = $('#ops_anaylse_head');

    $.ajax({
      type: "GET",
      url: OPS.base.www + 'ajax/crawler/analyse/overview',
      data: {collid: ops_anaylse_head.data('collection')},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_anaylse_head.html(thrownError);
      },
      success: function(data) {
        ops_anaylse_head.html(data);

      // temp
        var progress_bar = $(".progress-bar");
        progress_bar.animate({ 'width': progress_bar.data('percent') + '%' }, "slow" );
        OPS.pieChartRobots.init();
        OPS.pieChartHttp.init();
      // temp

        OPS.csvDownload.init();

      }
    });

  }

};


OPS.analyseSite = {

  fetch: function(collection) {

    var ops_anaylse_head = $('#ops_anaylse_head');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/crawler/analyse/check_single_site',
      data: {collid: ops_anaylse_head.data('collection'), siteid: ops_anaylse_head.data('siteid')},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_anaylse_head.html(thrownError);
      },
      success: function(data) {
        ops_anaylse_head.html(data);
      }
    });

  }

};


OPS.analyseTask = {

  fetch: function(task) {

    var ops_anaylse_head = $('#ops_anaylse_head');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/crawler/analyse/check_' + task,
      data: {collid: ops_anaylse_head.data('collection')},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_anaylse_head.html(thrownError);
      },
      success: function(data) {
        ops_anaylse_head.html(data);
        OPS.siteLevelChart.init();

        if (task == 'details_url') {
          OPS.dataTable.columnsearchAjax();
        } else {
          OPS.dataTable.init_noindex();
        }

        OPS.csvDownload.init();
      }
    });

  }

};


OPS.analyseTaskJson = {

  fetch: function(task) {

    var ops_anaylse_head = $('#ops_anaylse_head');

    // multicolumn filter
    var fields = '';
    $('.data-table thead td').each( function () {
      var title = $('.data-table thead td').eq($(this).index()).text();
      if (title == 'HTTP') {
        // MONGO DB CAUSED
        fields = fields + '<td style="min-width: 70px;"><input type="search" class="colsearch hidden" placeholder="Search '+title+'" /></td>';
      } else {
        fields = fields + '<td><input type="search" class="colsearch" placeholder="Search '+title+'" /></td>';
      }

    });
    $('.data-table thead').prepend('<tr>'+fields+'</tr>');

    // init table
    var table = $('.data-table').DataTable({
        sPaginationType: "full_numbers",
        sDom: "<\"table-header\"fil>t<\"table-footer\"ip>",
        aaSorting: [[ 0, "asc" ]],
        aLengthMenu: [[100], [100]],
        processing: true,
        bJQueryUI: false,
        serverSide: true,
        ajax: {
          url: OPS.base.www + 'ajax/crawler/analyse/check_' + task,
          type: 'POST',
          'data': { collid: ops_anaylse_head.data('collection') }
        },
        columns: [
          {"data": "url"},
          {"data": "response"},
          {"data": "titleTag"},
          {"data": "metaDesc"},
          {"data": "h1content"},
          {"data": "h2content"},
          {"data": "bodyTagWords"},
          {"data": "canonical"},
          {"data": "allLinks"},
          {"data": "responseTime"}
        ]

    }).on('processing.dt', function ( e, settings, processing ) {
        $('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
        $('.data-table').css( 'display', processing ? 'none' : 'inline-table' );
    });

    // search after 3
    $(".dataTables_filter input")
      // unbind previous default bindings
      .unbind()
      .bind("input", function(e) { // Bind our desired behavior
        if (this.value.length >= 3 || e.keyCode == 13) {
          table.search(this.value).draw();
        }
        if (this.value == "") {
          table.search("").draw();
        }
        return;
    });

    // apply the filter search
    $('.colsearch').each( function ( colIdx ) {
      $(this).on('focusout', function () {
        table.column(colIdx).search(this.value).draw();
      });
    });

  }

};


OPS.analyseTaskCheck = {

  fetch: function(task, value) {

    var ops_anaylse_head = $('#ops_anaylse_head');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/crawler/analyse/check_' + task,
      data: {collid: ops_anaylse_head.data('collection'), payload: value},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_anaylse_head.html(thrownError);
      },
      success: function(data) {
        ops_anaylse_head.html(data);

          OPS.siteLevelChart.init();

         $('.data-table').dataTable( {
            bJQueryUI: false,
            bAutoWidth: false,
            sPaginationType: "full_numbers",
            iDisplayLength: 100,
            aaSorting: [[ 1, "desc" ]],
            sDom: "<\"table-header\"fl>t<\"table-footer\"ip>"
          });

         OPS.csvDownload.init();

      }
    });

  }

};


// RANKINGS INDEXWATCH - RUK TOOL - RANKINGS INDEXWATCH - RUK TOOL - RANKINGS INDEXWATCH - RUK TOOL - RANKINGS INDEXWATCH - RUK TOOL - RANKINGS INDEXWATCH - RUK TOOL - RANKINGS INDEXWATCH - RUK TOOL

OPS.indexwatch = {

  fetchOverviewTravel: function() {

    var ops_project = $('#ops_project'),
        ops_loading = $('#ops_loading');

      ops_loading.html(OPS.theme.gfx.loading);

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajaxp/rankings/indexwatch_travel',
        data: {type: 'travel'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.indexwatch.renderView(data, ops_project, 2);
          // FORMSUBMIT
          var ops_form   = $('#ops_travelindex_form'),
              ops_data   = $('#ops_travelindex_url');

          ops_form.submit(function(event) {
            event.preventDefault();
            window.open('url?index=travel&site=' + ops_data.val(), 'URL Check', '');
          });

        }
      });

  },


  fetchOverviewSeo: function() {

    var ops_project = $('#ops_project'),
        ops_loading = $('#ops_loading');

      ops_loading.html(OPS.theme.gfx.loading);

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajaxp/rankings/indexwatch_seo',
        data: {type: 'seo'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.indexwatch.renderView(data, ops_project, 4);
          // FORMSUBMIT
          var ops_form   = $('#ops_travelindex_form'),
              ops_data   = $('#ops_travelindex_url');

          ops_form.submit(function(event) {
            event.preventDefault();
            window.open('url?index=seo&site=' + ops_data.val(), 'URL Check', '');
          });

        }
      });

  },


  fetchOverviewiBusiness: function() {

    var ops_project = $('#ops_project'),
        ops_loading = $('#ops_loading');

      ops_loading.html(OPS.theme.gfx.loading);

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajaxp/rankings/indexwatch_ibusiness',
        data: {type: 'ibusiness'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.indexwatch.renderView(data, ops_project, 4);
          // FORMSUBMIT
          var ops_form   = $('#ops_travelindex_form'),
              ops_data   = $('#ops_travelindex_url');

          ops_form.submit(function(event) {
            event.preventDefault();
            window.open('url?index=ibusiness&site=' + ops_data.val(), 'URL Check', '');
          });

        }
      });

  },

  fetchRanking: function() {

    var ops_form    = $('#ops_travelindex_form'),
        ops_data    = $('#ops_travelindex_url'),
        ops_project = $('#ops_project'),
        ops_loading = $('#ops_loading');

    ops_form.submit(function(event) {

      event.preventDefault();

      ops_loading.html(OPS.theme.gfx.loadinglong);

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajaxp/rankings/indexwatch_url',
        data: {index: 'travel', url: ops_data.val()},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.indexwatch.renderView(data, ops_project, 2);
        }
      });

    });

  },

  fetchRankingUrl: function(url, index) {

    var ops_project = $('#ops_project'),
        ops_loading = $('#ops_loading');

      ops_loading.html(OPS.theme.gfx.loadinglong);

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajaxp/rankings/indexwatch_url',
        data: {index: index, url: url},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.indexwatch.renderView(data, ops_project, 2);
        }
      });
  },


  renderView: function(data, elem, sort) {

    var data = $.parseJSON(data);

    elem.html(data.html);

    var table = $('.data-table').dataTable( {
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 100,
       sPaginationType: "full_numbers",
       sDom: "<\"table-header\"fil>t<\"table-footer\"ip>",
       aaSorting: [[ sort, "desc" ]],
       aLengthMenu: [[100, 500, 1000, -1], [100, 500, 1000, "All"]]
     });

    new $.fn.dataTable.FixedHeader(table);

  }

};



// NORMAL RANKINGS - RUK TOOL - NORMAL RANKINGS - RUK TOOL - NORMAL RANKINGS - RUK TOOL - NORMAL RANKINGS - RUK TOOL - NORMAL RANKINGS - RUK TOOL - NORMAL RANKINGS - RUK TOOL - NORMAL RANKINGS - RUK TOOL

OPS.rankings = {

  showProject: function(view) {

    var ops_content       = $('#ops_content'),
        ops_flash_message = $('#ops_flash_message');

    ops_content.html(OPS.theme.gfx.loading);

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/summary_dashboard',
      data: {type: view},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_flash_message.append(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_content.html(data);
        if (view == 'rankings') {
          OPS.dataTable.init(5, 'asc');
        } else {
          OPS.dataTable.init();
        }
        OPS.rankings.deleteProject();
      }
    });

  },


  deleteProject: function() {

    var project_id,
        ops_delete_project = $('.ops_delete_project'),
        ops_flash_message  = $('#ops_flash_message');

    ops_delete_project.click(function() {

      project_id = this.getAttribute('data-projectid');

        $.ajax({
          type: 'POST',
          url: OPS.base.www + 'ajax/rankings/project_delete',
          data: {id: project_id},
          error: function (xhr, ajaxOptions, thrownError) {
            ops_flash_message.append(OPS.theme.erro.ajax);
          },
          success: function(data) {
            data = $.parseJSON(data);
            if (data.success) {
              ops_flash_message.append(OPS.theme.msgSuccess(data.success));
            } else {
              ops_flash_message.append(OPS.theme.msgError(data.error));
            }
            OPS.rankings.showProject('admin');
          }
        });

    });

  },


  addProject: function() {

    var crawldata, errormsg,
        ops_new_project        = $('#ops_new_project'),
        ops_new_project_submit = $('#ops_new_project_submit'),
        ops_flash_message      = $('#ops_flash_message');

    ops_new_project.submit(function(event) {

      ops_new_project_submit.attr("disabled", true);

      event.preventDefault();
      projectdata = ops_new_project.serialize();

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/project_add',
        data: projectdata,
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.append(OPS.theme.erro.ajax);
        },
        success: function(data) {
          data = $.parseJSON(data);
          if (data.success) {
            ops_flash_message.append(OPS.theme.msgSuccess(data.success));
          } else {
            ops_flash_message.append(OPS.theme.msgError(data.error));
          }
         ops_new_project_submit.attr('disabled', false);
         OPS.rankings.showProject('admin');
        }

      });

    });

  },


  changeProject: function() {

    var ops_project        = $('#ops_project'),
        ops_flash_message  = $('#ops_flash_message');

    project_id = ops_project.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/project_change',
      data: {projectid: project_id},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_flash_message.append(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.icheck.render();
        OPS.rankings.changeProjectSave();
      }
    });

  },


  changeProjectSave: function() {

    var ops_project_change = $('#ops_project_change'),
        ops_flash_message  = $('#ops_flash_message');

    ops_project_change.submit(function(event) {

      event.preventDefault();
      data = ops_project_change.serialize();

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/project_change',
        data: data,
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.append(OPS.theme.erro.ajax);
        },
        success: function(data) {
          ops_flash_message.append(OPS.theme.msgSuccess('Änderungen gespeichert'));
          ops_project.html(data);
        }
      });

    });

  },


  fetchOverview: function() {

    var ops_project = $('#ops_project');

    project_id = ops_project.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/summary_overview',
      data: {projectid: project_id},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        var ops_url = $('#ops_url').attr("data-url");
        window.location.hash = ops_url;
        OPS.rankings.rankingsHighChartData('general');
        OPS.rankings.fetchSetData();
        OPS.rankings.fetchWinnerLooser(project_id);
        // ADD TO PERSONAL DASHBOARD
        OPS.sortable.rukAddSet();
        OPS.googleDrive.fileSubmit();
      }
    });

  },

  fetchWinnerLooser: function(project_id) {

    var ops_wl = $('#ops_winner_looser');
    var ops_load_wl_1 = $('#ops_load_wl_1');
    var ops_load_wl_6  = $('#ops_load_wl_6');

    this.fetchWinnerLooserLoad(project_id, ops_wl, 1);

    var that = this;
    // SET LISTENER FOR LOAD ALL TABLE
    ops_load_wl_1.click(function() {
      ops_wl.html('');
      ops_load_wl_1.removeClass('label-blue');
      ops_load_wl_1.addClass('label-red');
      ops_load_wl_6.removeClass('label-red');
      ops_load_wl_6.addClass('label-blue');
      that.fetchWinnerLooserLoad(project_id, ops_wl, 1);
    });

    ops_load_wl_6.click(function() {
      ops_wl.html('');
      ops_load_wl_6.removeClass('label-blue');
      ops_load_wl_6.addClass('label-red');
      ops_load_wl_1.removeClass('label-red');
      ops_load_wl_1.addClass('label-blue');
      that.fetchWinnerLooserLoad(project_id, ops_wl, 6);
    });

    $('#ops_open').click(function() {
      if ($(this).hasClass('open') === true) {
        $('#ops_winner_looser').animate({height:'300'});
        $(this).text('ausklappen');
        $(this).removeClass('open');
      } else {
        $('#ops_winner_looser').animate({height:'2875'});
        $(this).text('einklappen');
        $(this).addClass('open');
      }
    });

  },

  fetchWinnerLooserLoad: function(project_id, ops_wl, day) {

    $.ajax({
      type: 'GET',
      url: OPS.base.www + 'ajax/rankings/rankings_winner_looser',
      data: {projectid: project_id, days: day},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_wl.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_wl.html(data);
      }
    });

  },

  fetchSetData: function() {

    var ops_project = $('.ops_setdata');
    var ops_load_seven_12m     = $('#ops_load_seven_graph12m');
    var ops_load_seven_12w     = $('#ops_load_seven_graph12w');
    var ops_load_seven_graph31 = $('#ops_load_seven_graph31');
    var ops_load_seven_graph7  = $('#ops_load_seven_graph7');
    var ops_load_seven_table   = $('#ops_load_seven_table');
    var ops_clear_views        = $('#ops_clear_views');

    // SET LISTENER FOR LOAD ALL TABLE
    ops_load_seven_table.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataTable(that, that_node, 'general', result);
      });
    });

    // SET LISTENER FOR LOAD ALL GRAPH

    ops_load_seven_12m.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, '', 'month', result);
      });
    });

    ops_load_seven_12w.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, '', 'week', result);
      });
    });

    ops_load_seven_graph7.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, '', 'general', result);
      });
    });

    ops_load_seven_graph31.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, 31, 'general', result);
      });
    });

    // RESET VIEWS
    ops_clear_views.click(function() {
      ops_project.each(function() {
        var that   = $(this);
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.show();
        result.hide();
      });
    });


    // SET LISTENER FOR EACH SET
    ops_project.each(function() {

      var that_node = this;
      var that = $(this);

      // load table
      that.find('.ops_setload').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataTable(that, that_node, 'general', result);
      });

      // load graph
      that.find('.ops_setload_dex7').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, '', 'general', result);
      });

      // load graph
      that.find('.ops_setload_dex31').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, 31, 'general', result);
      });

      // load graph
      that.find('.ops_setload_dex12m').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, 12, 'month', result);
      });

      // load graph
      that.find('.ops_setload_dex12w').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, 12, 'week', result);
      });

    });

  },


  loadSetdataGraph:function(scope, elem, days, type, result) {

    var that = scope,
        that_node = elem;

    var projectid   = that.data('projectid'),
        setid       = that.data('setid');

    result.height(300);
    $.getJSON(OPS.base.www + 'ajax/rankings/rankings_chart', {projectid: projectid, setid: setid, days: days, type: type}, function(data) {
      OPS.rankings.rankingsHighChart(data.chart, result);
    });

  },


  loadSetdataTable:function(scope, elem, type, result) {

    var that = scope,
        that_node = elem;

    result.css('height', '');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/summary_setdata',
      data: {setid: that.data('setid'), projectid: that.data('projectid'), type: type},
      error: function (xhr, ajaxOptions, thrownError) {
        result.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        result.html(data);
      }
    });

  },


  loadSetdataLocal:function(that, choose, result, type, value) {

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/local_summary_setdata',
      data: {setid: that.data('setid'), projectid: that.data('projectid'), type: type, val: value},
      error: function (xhr, ajaxOptions, thrownError) {
        result.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        if (type === 'keyword') {
          result.html(data);
        } else  {
          chartdata = $.parseJSON(data);
          result.height(300);

          if (typeof chartdata === 'undefined'){
            chartdata = '';
          }

          var series_data = [];
          var i = 0;
          chartdata.charts.forEach(function(entry) {
            series_data[i] = {visible: true, name: entry.name, yAxis: 0, color: entry.color, data: entry.series};
            i++;
          });

          var yAxis_data  = [{min:0, minRange:1, title: { text: 'Anzahl Keywords' }, labels: {formatter: function () {if (this.value === 0) { return 1; } else { return this.value; }}}, reversed: false, minorTickInterval: 0.5}];

          if(typeof chartdata === 'undefined'){
            result.html('<div style="display: block; width: 240px; margin: 0 auto; font-size: 20px;">Keine Daten verfügbar.</div>');
          } else {
            result.highcharts({
              chart:   {type: 'spline'},
              title:   {text: chartdata.ctitle, align: 'left'},
              legend:  {align: 'right', verticalAlign: 'top', x: 0,y: 0,  borderWidth: 1, backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'},
              tooltip: {shared: true, pointFormat: '<span style="color:{point.color}">-</span> {series.name}: <b>{point.y}</b><br/>'},
              credits: {enabled: false},
              xAxis:   {categories: chartdata.categories, reversed: true},
              yAxis:   yAxis_data,
              series:  series_data
            });
          }

        }
      }
    });

  },


  fetchKwUrlDetails: function(view) {

    var ops_project = $('#ops_project'),
        setid       = ops_project.data('setid'),
        projectid   = ops_project.data('projectid');
        days        = ops_project.data('days');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/rankings_details',
      data: {setid: setid, projectid: projectid, type: view, days: days},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.googleDrive.fileSubmit();
        // ADD TO PERSONAL DASHBOARD
        OPS.sortable.rukAddSet();

        // sort by onedex or not
        if (view == 'url') {
          OPS.dataTable.init(2, 'desc');
        } else if (view == 'dexcomplete-analytics') {
          OPS.dataTable.init(4, 'desc');
        } else if (view == 'keyword') {
          OPS.dataTable.columnsearch('.data-table', 3, 'desc');
          OPS.rankings.rankingsHighChartData('general');
          OPS.rankings.genericTop10Rankings(setid, projectid, view);
        } else if (view == 'keyword-url-duplicates') {
          OPS.dataTable.columnsearch('.data-table', 2, 'desc');
          OPS.basefunc.removeSingleRankingsPrompt();
        } else if (view == 'keyword-url') {
          OPS.dataTable.columnsearch('.data-table', 2, 'desc');
          OPS.rankings.rankingsHighChartData('general');
        } else {       
          OPS.dataTable.init(2, 'desc');
        }

        $('.tags').click(function() {
          OPS.rankings.initTags($(this));
        });

        OPS.basefunc.removeSingleRankings();

      }

    });

  },

  genericTop10Rankings: function(setid, projectid, view) {


    $('#ops_generictop10').click(function() {

      var clicked = $(this);

      clicked.off();

      clicked.html('<i class="icon-spinner icon-spin"></i>');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/rankings_generictop10',
        data: {setid: setid, projectid: projectid, type: view},
        error: function (xhr, ajaxOptions, thrownError) {
          clicked.html('Ajax Fehler');
        },
        success: function(data) {
          clicked.html(data);
        }
      });

    });

  },

  topRankings: function() {

    var ops_project = $('#ops_top_rankings');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/rankings_top_rankings',
      data: {},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.googleDrive.fileSubmit();
        OPS.dataTable.columnsearch('.data-table', 3, 'desc');
      }
    });

  },


  fetchApi: function() {

    var ops_project = $('#ops_project');

    project_id = ops_project.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/api',
      data: {projectid: project_id},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
      }
    });

  },



  saveKeywords: function(project_id, set_id) {

    var ops_rankings_save_keywords        = $('#ops_rankings_save_keywords'),
        ops_rankings_save_keywords_submit = $('#ops_rankings_save_keywords_submit'),
        ops_flash_message                 = $( "#ops_flash_message");

    $('#ops_rankings_keywords').on('scroll', function () {
        $('#ops_rankings_tags').scrollTop($(this).scrollTop());
    });


    ops_rankings_save_keywords.submit(function(event) {

      var ops_keywordset_name   = $('#ops_keywordset_name').val();
      var ops_rankings_keywords = $('#ops_rankings_keywords').val();
      var ops_rankings_tags     = $('#ops_rankings_tags').val();      
      var ops_customer_view     = $('.OPS_show:checked').val();
      var ops_measuring         = $('.OPS_interval:checked').val();
      var ops_kind              = $('#ops_type').val();
      var ops_competition       = $('#OPS_competition').val();
      var team                  = $('#team').val();      

      ops_rankings_save_keywords_submit.attr("disabled", true);
      ops_rankings_save_keywords_submit.addClass("hidden");
      $('#ops_spin').addClass('icon-spinner icon-spin');

      event.preventDefault();
      data = ops_rankings_save_keywords.serialize();

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/keywords_edit',
        data: {projectid: project_id, setid: set_id, mo_type: ops_kind, action: 'change', keywords: ops_rankings_keywords, tags: ops_rankings_tags, setname: ops_keywordset_name, customer_view: ops_customer_view, measuring: ops_measuring, competition: ops_competition, team: team},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.append(OPS.theme.erro.ajax);
        },
        success: function(data) {
          $('#ops_spin').removeClass('icon-spinner icon-spin');
          ops_rankings_save_keywords_submit.removeClass("hidden");
          ops_flash_message.append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+data+'</strong></div>');
          ops_rankings_save_keywords_submit.attr("disabled", false);
        }
      });

    });

  },


  fetchKeywords: function() {

    var ops_project = $('#ops_keywordset');

    project_id = ops_project.data('projectid');
    set_id = ops_project.data('setid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/keywords_edit',
      data: {projectid: project_id, setid: set_id, action: 'show'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.rankings.saveKeywords(project_id, set_id);
        OPS.icheck.render();
      }
    });

  },


  fetchKeywordsets: function() {

    var ops_project = $('#ops_project');

    project_id = ops_project.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/keywordsets_edit',
      data: {id: project_id, action: 'show'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.rankings.addKeywordset();
        OPS.rankings.deleteKeywordset();
        OPS.icheck.render();
        OPS.dataTable.init(0, 'asc', false);
      }
    });

  },


  copyKeywords: function() {

    var ops = $('#ops_keywordset');

    var set_id = ops.data('setid'),
        project_id = ops.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/keywords_copy',
      data: {setid: set_id, project_id: project_id,  action: 'show'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops.html(data);
        OPS.rankings.copyKeywordsExecute();
      }
    });

  },


  copyKeywordsExecute: function() {

    $('.ops_copy_to').click(function() {

      var clicked = $(this);

      clicked.off();

      var copyset = clicked.data('setid'),
          copytype = clicked.data('type'),
          toproject = clicked.data('projectid');

      clicked.html('<i class="icon-spinner icon-spin"></i>');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/keywords_edit',
        data: {copy_setid: copyset, copy_projectid: toproject, copy_type: copytype, action: 'copy'},
        error: function (xhr, ajaxOptions, thrownError) {
          clicked.html('Ajax Fehler');
        },
        success: function(data) {
          clicked.html(data);
        }
      });

    });

  },


  addKeywordset: function() {

    var ops_add_keywordset        = $('#ops_add_keywordset'),
        ops_project               = $('#ops_project'),
        ops_flash_message         = $('#ops_flash_message'),
        ops_add_keywordset_submit = $('#ops_add_keywordset_submit');

    ops_add_keywordset.submit(function(event) {

      ops_add_keywordset_submit.attr("disabled", true);

      event.preventDefault();

      var project_id    = ops_project.data('projectid');
      var kw_set_name   = $('#ops_add_keywordset_name').val();
      var kw_set_type   = $('.OPS_type:checked').val();
      var customer_view = $('.OPS_show:checked').val();
      var interval      = $('.OPS_interval:checked').val();
      var ops_kind      = $('#ops_type').val();

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/keywordsets_edit',
        data: {id: project_id, name: kw_set_name, mo_type: ops_kind, action: 'add', type: kw_set_type, interval: interval, customer_view: customer_view},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.html(OPS.theme.erro.ajax);
        },
        success: function(data) {

          data = $.parseJSON(data);
          if (data.error) {
            ops_flash_message.append(OPS.theme.msgError(data.error));
            ops_add_keywordset_submit.attr("disabled", false);
          } else {
            OPS.rankings.fetchKeywordsets();
            ops_flash_message.append(OPS.theme.msgSuccess(data.success));
          }

        }
      });

    });

  },


  deleteKeywordset: function() {

    var ops_delete_keywordset = $(".ops_delete_keywordset"),
        ops_flash_message     = $('#ops_flash_message');

    ops_delete_keywordset.click(function(event) {

      event.preventDefault();
      var that = $(this);
      kw_set_id = that.data('kwsetid');

      var spinner = $('div').find("[data-setid='" + kw_set_id + "']");
      spinner.empty();
      spinner.addClass('icon-spinner icon-spin');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/keywordsets_edit',
        data: {id: kw_set_id, action: 'delete'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.append(OPS.theme.erro.ajax);
        },
        success: function(data) {
          data = $.parseJSON(data);
          if (data.error) {
            ops_flash_message.append(OPS.theme.msgError(data.error));
          } else {
            OPS.rankings.fetchKeywordsets();
            ops_flash_message.append(OPS.theme.msgSuccess(data.success));
          }
        }
      });

    });

  },


  fetchKeywordDetails: function(view) {

    var ops_project = $('#ops_project'),
        projectid   = ops_project.data('projectid'),
        keyword     = ops_project.data('keyword'),
        country     = ops_project.data('country');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/rankings_keyword_details',
      data: {projectid: projectid, keyword: keyword, country: country},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        data = $.parseJSON(data);
        ops_project.html(data.html);
        OPS.googleDrive.fileSubmit();
        OPS.dataTable.init(2, 'desc');
        OPS.historicalRankings.highchart(data.json);
        OPS.historicalRankings.highchart(data.json2, '#ops-histochart2');
        OPS.historicalRankings.highchart(data.json3, '#ops-histochart3');
        OPS.historicalRankings.highchart(data.json4, '#ops-histochart4');        
      }

    });

  },

  fetchTop100: function() {

    var ops_project = $('#ops_project');
    var kw = ops_project.data('keyword');
    var ts = ops_project.data('timestamp');
    var co = ops_project.data('country');
    var loc = ops_project.data('location');
    var type = ops_project.data('type');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/rankings_results',
      data: {kw: kw, ts: ts, co: co, loc: loc, type: type},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.dataTable.init(0, 'asc', false);
      }
    });

  },


  fetchCompetitionInput: function() {

    var result     = $('#ops_content'),
        project    = $('#ops_project'),
        startdate  = project.data('startdate');
        setid      = project.data('setid'),
        projectid  = project.data('projectid');


      $.ajax({
        type: 'GET',
        url: OPS.base.www + 'ajax/rankings/rankings_details_competition',
        data: {startdate: startdate, setid: setid},
        error: function (xhr, ajaxOptions, thrownError) {
          result.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          result.html(data);
          OPS.rankings.fetchCompetition();
        }
      });

  },


  fetchCompetition: function() {

    var ops_date_compare = $('#ops_competition'),
        ops_loading      = $('#ops_content'),
        ops_project      = $('#ops_project'),
        setid            = ops_project.data('setid'),
        projectid        = ops_project.data('projectid');

    ops_date_compare.submit(function(event) {

      ops_loading.html(OPS.theme.gfx.loading);

      event.preventDefault();

      var dates = ops_date_compare.serialize();
      var data_submit = dates + '&setid=' + setid + '&projectid=' + projectid;

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/rankings_details_competition',
        data: data_submit,
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          ops_project.html(data);
          OPS.googleDrive.fileSubmit();
          OPS.dataTable.columnsearch('.data-table', 3, 'desc');
          $('.tags').click(function() {
            OPS.rankings.initTags($(this));
          });          

        }
      });


    });

  },

  dateCompareInput: function() {

    var result     = $('#ops_content'),
        project    = $('#ops_project'),
        startdate  = project.data('startdate'),
        setid      = project.data('setid'),
        projectid  = project.data('projectid');


      $.ajax({
        type: 'GET',
        url: OPS.base.www + 'ajax/rankings/rankings_details_compare',
        data: {startdate: startdate, setid: setid},
        error: function (xhr, ajaxOptions, thrownError) {
          result.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          result.html(data);
          OPS.icheck.render();
          OPS.rankings.dateCompare();
        }
      });

  },

  dateCompare: function() {

    var ops_date_compare = $('#ops_date_compare'),
        ops_loading      = $('#ops_content'),
        ops_project      = $('#ops_project'),
        setid            = ops_project.data('setid'),
        projectid        = ops_project.data('projectid');

    ops_date_compare.submit(function(event) {

      ops_loading.html(OPS.theme.gfx.loading);

      event.preventDefault();

      var dates = ops_date_compare.serialize();
      var data_submit = dates + '&setid=' + setid + '&projectid=' + projectid;

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/rankings_details_compare',
        data: data_submit,
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          ops_project.html(data);
          OPS.googleDrive.fileSubmit();
          OPS.dataTable.columnsearch('.data-table', 4, 'desc');
          $('.tags').click(function() {
            OPS.rankings.initTags($(this));
          });          
        }
      });

    });

  },

  rankingsHighChartData: function(type) {

    var ops_top10   = $('#ops_top10'),
        ops_top20   = $('#ops_top20'),
        ops_top30   = $('#ops_top30'),
        ops_topQ    = $('#ops_topQ'),
        ops_project = $('#ops_project'),
        ops_chart   = $('#OPSC_opi_highchart'),
        projectid   = ops_project.data('projectid'),
        setid       = ops_project.data('setid'),
        days        = ops_project.data('days');

    $.getJSON(OPS.base.www + 'ajax/rankings/rankings_chart', {projectid: projectid, setid: setid, days: days, type: type}, function(data) {

      ops_top10.append(data.top10);
      ops_top20.append(data.top20);
      ops_top30.append(data.top30);
      ops_topQ.append(data.q);
      OPS.rankings.rankingsHighChart(data.chart, ops_chart);

    });

  },

  rankingsHighChart: function(chartdata, htmlelem) {

    Highcharts.setOptions({
      lang: {
        decimalPoint: ',',
        thousandsSep: '.'
      }});

    if(typeof chartdata === 'undefined'){
      chartdata = '';
    }

    // ALTER MAX AND MIN VALUES
    var vals = [];
    if (chartdata.series_1) {
      chartdata.series_1.forEach(function(entry) {
        vals.push(entry);
      });
      var max_of_array = Math.max.apply(Math, vals) * 1.2;
      var min_of_array = Math.min.apply(Math, vals) * 0.8;
    }

    if(typeof chartdata.s_5 === 'undefined'){
      chartdata.s_5 = false;
    }

    var yAxis_data  = [
                        {title: { text: 'OneDex' }, enabled: true, labels: {enabled: true} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} },
                        {title: { text: null }, enabled: false, labels: {enabled: false} }
                      ];


    if (typeof chartdata.name_1 === 'undefined') {
      chartdata.name_1 = 'OneDex';
    }

    var series_data = [
                        {visible: true, name: chartdata.name_1, yAxis: 0, color: '#3880aa', data: chartdata.series_1},
                        {visible: chartdata.s_2, name: 'SEO Umsatz', yAxis: 1, color: '#6eb056', data: chartdata.series_2, tooltip: { valueSuffix: ' €' }},
                        {visible: chartdata.s_3, name: 'SEO PageViews', yAxis: 2, color: '#d5a44c', data: chartdata.series_3, type: 'spline'},
                        {visible: chartdata.s_4, name: 'SEO Clicks', yAxis: 3, color: '#d20202', data: chartdata.series_4, type: 'spline', dashStyle: 'shortdot'},
                        {visible: chartdata.s_5, name: 'SEO Zielseiten', yAxis: 4, color: '#009CFF', data: chartdata.series_5, type: 'spline', dashStyle: 'DashDot'},
                        {visible: chartdata.s_15, name: 'SEO Transactions', yAxis: 5, color: '#7F6C00', data: chartdata.series_15, type: 'spline', dashStyle: 'ShortDashDotDot'}                        
                      ];


        if (typeof chartdata.s_6 !== 'undefined' && chartdata.s_6 !== false) {
          series_data.push({visible: chartdata.s_6, name: chartdata.name_6, yAxis: 0, color: '#7F5110', data: chartdata.series_6, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_7 !== 'undefined' && chartdata.s_7 !== false) {
          series_data.push({visible: chartdata.s_7, name: chartdata.name_7, yAxis: 0, color: '#FF9500', data: chartdata.series_7, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_8 !== 'undefined' && chartdata.s_8 !== false) {
          series_data.push({visible: chartdata.s_8, name: chartdata.name_8, yAxis: 0, color: '#FFB54C', data: chartdata.series_8, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_9 !== 'undefined' && chartdata.s_9 !== false) {
          series_data.push({visible: chartdata.s_9, name: chartdata.name_9, yAxis: 0, color: '#7F4B00', data: chartdata.series_9, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_10 !== 'undefined' && chartdata.s_10 !== false) {
          series_data.push({visible: chartdata.s_10, name: chartdata.name_10, yAxis: 0, color: '#CC7800', data: chartdata.series_10, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_11 !== 'undefined' && chartdata.s_11 !== false) {
          series_data.push({visible: chartdata.s_11, name: chartdata.name_11, yAxis: 0, color: '#FFD900', data: chartdata.series_11, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_12 !== 'undefined' && chartdata.s_12 !== false) {
          series_data.push({visible: chartdata.s_12, name: chartdata.name_12, yAxis: 0, color: '#7F6F10', data: chartdata.series_12, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_13 !== 'undefined' && chartdata.s_13 !== false) {
          series_data.push({visible: chartdata.s_13, name: chartdata.name_13, yAxis: 0, color: '#CCAD00', data: chartdata.series_13, type: 'spline', dashStyle: 'Dot'});
        }
        if (typeof chartdata.s_14 !== 'undefined' && chartdata.s_14 !== false) {
          series_data.push({visible: chartdata.s_14, name: chartdata.name_14, yAxis: 0, color: '#7F6C00', data: chartdata.series_14, type: 'spline', dashStyle: 'Dot'});
        }

    if (typeof chartdata.series_1 === 'undefined'){

      // NO CHART DATA
      htmlelem.html('<div class="padded">Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br> Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</div>');

    } else {

      var options = {
        chart:   {type: 'areaspline', backgroundColor: '#fbfbfb'},
        title:   {useHTML: true, text: chartdata.ctitle + " <span class='open-magnify'>Vollbild anzeigen</span>", align: 'left'},
        legend:  {align: 'left', verticalAlign: 'top', x: 0, y: 30,  borderWidth: 1, backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'},
        tooltip: {shared: true, pointFormat: '<span style="color:{point.color}">-</span> {series.name}: <b>{point.y}</b><br/>'},
        credits: {enabled: false},plotOptions: {areaspline: {fillOpacity: 0.3}},
        xAxis:   {categories: chartdata.categories},
        yAxis:   yAxis_data,
        series:  series_data
      };

      htmlelem.highcharts(options);

      //MAGNIFY CHARTS
      htmlelem.find('.open-magnify').click(function () {
        var $div = $( "<div class='lightbox-container'><i class='icon-remove icon-2x' id='close-magnify'></i><div class='magnify'></div></div>" );
        $('body').append($div);
        $('.magnify').highcharts(options);
        $('.lightbox-container').find('.open-magnify').remove();
        $('#close-magnify').click(function() {
          $('.lightbox-container').remove();
        });
      });

    }

  },

  fetchKeywordsTags: function() {

    var ops_project = $('#ops_keywordset');

    project_id = ops_project.data('projectid');
    set_id = ops_project.data('setid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/keywords_edit_tags',
      data: {projectid: project_id, setid: set_id, action: 'show'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);

        $('.tags').tagsInput({
           'height':'40px',
           'width':'100%',
           'interactive':true,
           'onAddTag': OPS.rankings.modifyTags,
           'onRemoveTag': OPS.rankings.modifyTags,
           'defaultText':'Tag hinzufügen',
           'removeWithBackspace' : true,
           'placeholderColor' : '#666666'
        });

      }

    });

  },

  initTags: function(elem) {

    elem.tagsInput({
     'height':'60px',
     'width':'100%',
     'interactive':true,
     'onAddTag': OPS.rankings.modifyTags,
     'onRemoveTag': OPS.rankings.modifyTags,
     'defaultText':'...',
     'removeWithBackspace' : true,
     'placeholderColor' : '#666666'
    });

    var foc  = '#' + elem[0].id + '_tag';
    $(foc).focus();

  },

  modifyTags: function(tag) {

    var kwid = this.getAttribute('data-kwid');
    var nid  = '#' + this.id + '_tagsinput>span>span';
    var ntag = $(nid).text();
    
    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/keywords_edit_tags',
      data: {action:'change', kwid: kwid, tags: ntag},
      error: function (xhr, ajaxOptions, thrownError) {
        //console.log('error')
      },
      success: function(data) {
        //console.log('good')
      }
    });

  }

};

// CUSTOMIZED SET - PERSONAL DASHBOARD - CUSTOMIZED SET - PERSONAL DASHBOARD - CUSTOMIZED SET - PERSONAL DASHBOARD - CUSTOMIZED SET - PERSONAL DASHBOARD - CUSTOMIZED SET - PERSONAL DASHBOARD

OPS.sortable = {

  show: function() {

    var ops_content       = $('#ops_content'),
        ops_flash_message = $('#ops_flash_message');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/summary_dashboard_custom',
      data: {show: 'show'},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_content.html(thrownError);
      },
      success: function(data) {
        ops_content.html(data);
        OPS.sortable.rukPersonalDash();
        OPS.rankings.fetchSetData();
        // SAVE AGAIN - DUE TO SOME MAYBE DELETED
        OPS.sortable.rukSortDelete();
      }
    });

  },

  rukPersonalDash: function() {
/*
    $('#sortable').sortable({
      revert: true,
      placeholder: 'sortable-highlight',
      stop: function(event, ui) {
        OPS.sortable.rukSortDelete();
      }
    });*/

    $('#sortable .delete-sortable').click(function() {
      $(this).parents(':eq(3)').remove();
      OPS.sortable.rukSortDelete();
    });

  },

  rukAddSet: function() {

    var newadd = new Object();

    $('.add-sortable').click(function() {

      newadd['setid']     = $(this).data('setid');
      newadd['projectid'] = $(this).data('projectid');

      var that = $(this);
      var json = JSON.stringify(newadd);
      var elem = that.parent().html('<i class="icon-spinner icon-spin"></i>');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/summary_dashboard_custom',
        data: {add: json},
        error: function (xhr, ajaxOptions, thrownError) {
          elem.html('<i class="info-sortable icon-remove"></i>');
        },
        success: function(data) {
          elem.html('<i class="info-sortable icon-ok"></i>');
        }
      });

    });

  },

  rukSortDelete: function() {

    var newsort = [];

    $('.ops_setdata').each(function( index ) {
      newsort[index]              = new Object();
      newsort[index]['setid']     = $(this).data('setid');
      newsort[index]['projectid'] = $(this).data('projectid');
    });

    var json = JSON.stringify(newsort);
    OPS.sortable.saveNewSort(json);

  },

  saveNewSort: function (newsort) {

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/summary_dashboard_custom',
      data: {sort: newsort},
      error: function (xhr, ajaxOptions, thrownError) {
        alert('Error - Please contact admin!');
      },
      success: function() {}
    });

  }

};


// LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS - LOCALRANKINGS

OPS.localRankings = {

  fetchOverview: function() {

    var ops_project = $('#ops_project');

    project_id = ops_project.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/local_overview',
      data: {projectid: project_id},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.localRankings.fetchSetData();
      }
    });

  },


  fetchKeywordsets: function() {

    var ops_project = $('#ops_project');

    project_id = ops_project.data('projectid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/local_keywordsets',
      data: {projectid: project_id},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.localRankings.deleteKeywordsetLocal();
        OPS.dataTable.init(0, 'asc', false);
      }
    });

  },

  showKeywordsetLocal: function(kind) {

    var ops_project = $('#ops_project');

    var project_id = ops_project.data('projectid');
    var set_id     = ops_project.data('setid');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/local_keywords',
      data: {projectid: project_id, setid: set_id, kind: kind},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        $(".chzn-select").select2();
        OPS.icheck.render();
        OPS.localRankings.addKeywordsetLocal();
      }
    });

  },


  addKeywordsetLocal: function() {

    var ops_add_keywordset        = $('#ops_add_keywordset'),
        ops_project               = $('#ops_project'),
        ops_flash_message         = $('#ops_flash_message'),
        ops_add_keywordset_submit = $('#ops_add_keywordset_submit');

    ops_add_keywordset.submit(function(event) {

      ops_flash_message.empty();
      event.preventDefault();

      ops_add_keywordset_submit.addClass("hidden");
      $('#ops_spin').addClass('icon-spinner icon-spin');

      var project_id  = ops_project.data('projectid');
      var set_id      = ops_project.data('setid');

      var datastring  = $("#ops_add_keywordset").serialize();
      var formData    = new FormData($(this)[0]);

      $.ajax({
          url: OPS.base.www + 'ajax/rankings/local_keywords',
          type: 'POST',
          data: formData,
          async: true,
          success: function (data) {

            $('#ops_spin').removeClass('icon-spinner icon-spin');
            ops_add_keywordset_submit.removeClass("hidden");

            data = $.parseJSON(data);
            if (data.error) {
              ops_flash_message.html(OPS.theme.msgError(data.error));
              ops_add_keywordset_submit.html("Campaign nicht gespeichert");
              ops_add_keywordset_submit.attr("disabled", false);
            } else {
              ops_flash_message.html(OPS.theme.msgSuccess(data.msg));
              ops_add_keywordset_submit.html("Campaign gespeichert");
              ops_add_keywordset_submit.attr("disabled", true);
            }

          },
          cache: false,
          contentType: false,
          processData: false
      });

    });

  },

  deleteKeywordsetLocal: function() {

    var ops_delete_keywordset = $( ".ops_delete_keywordset"),
        ops_flash_message     = $('#ops_flash_message');

    ops_delete_keywordset.click(function(event) {

      event.preventDefault();

      var that = $(this);
      kw_set_id = that.data('kwsetid');

      var spinner = $('div').find("[data-setid='" + kw_set_id + "']");
      spinner.empty();
      spinner.addClass('icon-spinner icon-spin');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/local_keywords',
        data: {id: kw_set_id, action: 'delete'},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_flash_message.append(OPS.theme.erro.ajax);
        },
        success: function(data) {
          OPS.localRankings.fetchKeywordsets();
        }
      });

    });

  },


  fetchKwUrlDetails: function(view) {

    var ops_project = $('#ops_project'),
        setid       = ops_project.data('setid'),
        projectid   = ops_project.data('projectid');
        days        = ops_project.data('days');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/local_rankings_details',
      data: {setid: setid, projectid: projectid, type: view, days: days},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_project.html(data);
        OPS.googleDrive.fileSubmit();
        // ADD TO PERSONAL DASHBOARD
        OPS.sortable.rukAddSet();

        // sort by onedex or not
        if (view == 'url') {
          OPS.dataTable.columnsearch('.data-table', 2, 'desc');
        } else if (view == 'dexcomplete-analytics') {
          OPS.dataTable.columnsearch('.data-table', 4, 'desc');
        } else if (view == 'keyword') {
          OPS.dataTable.columnsearch('.data-table', 3, 'desc');
          OPS.rankings.rankingsHighChartData('local');
        } else {
          OPS.dataTable.columnsearch('.data-table', 2, 'desc');
        }

        // hide column on click
        $('.hidecolumn').click(function() {
          var cell = $(this).parent().get( 0 ).cellIndex + 1;
          $('td:nth-child('+cell+')').hide();
        });

      }

    });

  },


  fetchKeywordDetails: function(view) {

    var ops_project = $('#ops_project'),
        projectid   = ops_project.data('projectid'),
        setid       = ops_project.data('setid'),
        keyword     = ops_project.data('keyword'),
        country     = ops_project.data('country'),
        location    = ops_project.data('location'),
        type        = ops_project.data('type');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/local_rankings_keyword_details',
      data: {projectid: projectid, setid: setid, keyword: keyword, country: country, location: location, type: type, view: view},
      error: function (xhr, ajaxOptions, thrownError) {
        ops_project.html(OPS.theme.erro.ajax);
      },
      success: function(data) {
        data = $.parseJSON(data);
        ops_project.html(data.html);
        OPS.googleDrive.fileSubmit();
        if (view == 'location') {
          OPS.dataTable.init(2, 'asc');
        } else {
          OPS.dataTable.init(2, 'desc');
        }
        OPS.historicalRankings.highchart(data.json);
      }
    });

  },


  fetchSetData: function() {

    var ops_project            = $('.ops_setdata');
    var ops_load_seven_graph31 = $('#ops_load_seven_graph31');
    var ops_load_seven_graph7  = $('#ops_load_seven_graph7');
    var ops_load_seven_table   = $('#ops_load_seven_table');
    var ops_clear_views        = $('#ops_clear_views');

    // SET LISTENER FOR LOAD ALL TABLE
    ops_load_seven_table.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataTable(that, that_node, 'local', result);
      });
    });

    // SET LISTENER FOR LOAD ALL GRAPH
    ops_load_seven_graph7.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, '', 'local', result);
      });
    });

    ops_load_seven_graph31.click(function() {
      ops_project.each(function() {
        var that_node = this;
        var that      = $(this);
        var choose    = that.find('.ops_select_view');
        var result    = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, 31, 'local', result);
      });
    });

    // RESET VIEWS
    ops_clear_views.click(function() {
      ops_project.each(function() {
        var that = $(this);
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.show();
        result.hide();
      });
    });

    // SET LISTENER FOR EACH SET
    ops_project.each(function() {

      var that_node = this;
      var that = $(this);

      // load table
      that.find('.ops_setload').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataTable(that, that_node, 'local', result);
      });

      // load graph
      that.find('.ops_setload_dex7').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, '', 'local', result);
      });

      // load graph
      that.find('.ops_setload_dex31').click(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        choose.hide();
        result.show().html(OPS.theme.gfx.loading);
        OPS.rankings.loadSetdataGraph(that, that_node, 31, 'local', result);
      });

      // load graph LOCAL KEYWORD
      that.find('.ops_setload_keyword').change(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        if (this.value !== '0') {
          choose.hide();
          result.show().html(OPS.theme.gfx.loading);
          OPS.rankings.loadSetdataLocal(that, choose, result, 'keyword', this.value);
        }
      });

      // load graph LOCAL GRAPH
      that.find('.ops_setload_graph').change(function() {
        var choose = that.find('.ops_select_view');
        var result = that.find('.ops_show_view');
        if (this.value !== '0') {
          choose.hide();
          result.show().html(OPS.theme.gfx.loading);
          OPS.rankings.loadSetdataLocal(that, choose, result, 'graph', this.value);
        }
      });

    });

  },


  dateCompareLocal: function() {

    var ops_date_compare = $('#ops_date_compare'),
        ops_loading      = $('#ops_loading'),
        ops_project      = $('#ops_project'),
        setid            = ops_project.data('setid'),
        projectid        = ops_project.data('projectid');

    ops_date_compare.submit(function(event) {

      ops_loading.html(OPS.theme.gfx.loading);

      event.preventDefault();

      var dates = ops_date_compare.serialize();
      var data_submit = dates + '&setid=' + setid + '&projectid=' + projectid;

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/local_details_compare',
        data: data_submit,
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          ops_project.html(data);
          OPS.googleDrive.fileSubmit();
          OPS.dataTable.columnsearch('.data-table', 3, 'desc');
        }
      });


    });

  },

};

// SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA - SEA


OPS.seobalancer = {

  fetchData:function(view) {

      var ops_project = $('#ops_project');

      var project_id = ops_project.data('projectid');
      var timestart  = ops_project.data('startdate');
      var timeend    = ops_project.data('enddate');

      $.ajax({
        type: 'POST',
        url: OPS.base.www + 'ajax/rankings/seo_sea_balancer',
        data: {id: project_id, timestart: timestart, timeend: timeend, view: view},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_project.html(OPS.theme.erro.ajax);
        },
        success: function(data) {
          ops_project.html(data);
          var ops_submit = $('#ops_submit');
          if (ops_submit.length > 0) {
            ops_submit.click(function(event) {
              event.preventDefault();
              var date1 = $('input[name="date1"').val();
              var date2 = $('input[name="date2"').val();
              window.location.href = '../overview/'+project_id+'/'+date1+'/'+date2;
            });
          } else {
            OPS.googleDrive.fileSubmit();
            OPS.dataTable.columnsearchBalancer('.data-table', 8, 'desc', false);            
          }
        }
      });

    }

};


OPS.sea = {

  landingpagefinder: function() {

    var ops_task      = $("#ops_liverankings"),
        ops_keywords  = $("#ops_liverankings_keywords"),
        ops_url       = $("#ops_liverankings_url"),
        ops_lang      = $("#ops_liverankings_lang"),
        ops_submit    = $("#ops_liverankings_submit"),
        ops_loading   = $("#ops_loading_block"),
        ops_result    = $("#ops_result_block");

    ops_task.submit(function(event) {

      ops_result.addClass("hidden");
      ops_loading.removeClass("hidden");
      ops_result.html(OPS.theme.gfx.loading);
      ops_submit.attr("disabled", true);

      event.preventDefault();

      $.ajax({
        type: "POST",
        url: OPS.base.www + 'ajax/sea/landingpagefinder',
        data: { keywords: ops_keywords.val(), lang: ops_lang.val(), url: ops_url.val()},
        error: function (xhr, ajaxOptions, thrownError) {
          ops_result.html('ERROR');
          ops_submit.attr("disabled", false);
        },
        success: function(data) {
          ops_loading.addClass("hidden");
          ops_result.removeClass("hidden");
          ops_result.html(data);
          OPS.dataTable.init(0, 'asc', false);
          OPS.googleDrive.fileSubmit();
        }

      });

    });

  }

};


// ANALYTICS - ANALYTICS TOOL - ANALYTICS - ANALYTICS TOOL - ANALYTICS - ANALYTICS TOOL - ANALYTICS - ANALYTICS TOOL - ANALYTICS - ANALYTICS TOOL - ANALYTICS - ANALYTICS TOOL

OPS.analytics = {

  pageIndexing: function() {

    var ops_content       = $('#ops_content'),
        ops_flash_message = $('#ops_flash_message');

    $.ajax({
      type: 'POST',
      url: OPS.base.www + 'ajax/rankings/summary_page_indexing',
      error: function (xhr, ajaxOptions, thrownError) {
        ops_flash_message.append(OPS.theme.erro.ajax);
      },
      success: function(data) {
        ops_content.html(data);
        OPS.dataTable.init(3, 'desc');
      }
    });

  },

  traveldb: function() {

    $('.data-table')
      .on('processing.dt', function ( e, settings, processing ) {
        $('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
        $('.data-table').css( 'display', processing ? 'none' : 'inline-table' ); })
      .dataTable( {
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 100,
       pagingType: "full_numbers",
       sDom: "<\"table-header\"fil>t<\"table-footer\"ip>",
       aaSorting: [[ 2, "desc" ]],
       aLengthMenu: [[100, 500, 1000, 5000], [100, 500, 1000, 5000]],
       processing: true,
       serverSide: true,
       ajax: OPS.base.www + 'ajax/keywords/traveldb',
       columns: [
          { "data": "customer" },
          { "data": "query" },
          { "data": "opi" },
          { "data": "position" },
          { "data": "impressions" },
          { "data": "clicks" },
          { "data": "ctr" },
          { "data": "rank" }
        ]
     });

  },

  traveldbBoost: function() {

    $('.data-table')
      .on('processing.dt', function ( e, settings, processing ) {
        $('#processingIndicator').css( 'display', processing ? 'block' : 'none' );
        $('.data-table').css( 'display', processing ? 'none' : 'inline-table' ); })
      .dataTable( {
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 100,
       pagingType: "full_numbers",
       sDom: "<\"table-header\"fil>t<\"table-footer\"ip>",
       aaSorting: [[ 2, "asc" ]],
       aLengthMenu: [[10, 50, 100], [10, 50, 100]],
       processing: true,
       serverSide: true,
       ajax: OPS.base.www + 'ajax/keywords/traveldb_boost_keywords',
       columns: [
          { "data": "index" },
          { "data": "keyword" },
          { "data": "boost" },
          { "data": "onedexpot" },
          { "data": "rank" },
          { "data": "opi" },
          { "data": "onedex" },
          { "data": "onedexmax" }
        ]
     });

  }

};

// GENERIC - GOOGLE DRIVE CSV EXPORT
// takes the path of the CSV File, Filename and puts it to the Session, calls the Google Drive API and submits the CSV to GDRIVE
// HTML SCHEMA:
// <i title="in Google Drive öffnen" class="icon-google-plus-sign google-drive-request" data-filename="COOL" data-filepath="/var/www/oneproseo/oneproseo/temp/export_ruk_keywords_1406640528.csv"></i>

OPS.googleDrive = {

  fileSubmit: function() {

    var drive_request = $('.google-drive-request');

    drive_request.click(function() {

      var that = $(this);

      var filename = that.data('filename'),
          filepath = that.data('filepath');

          that.removeClass('icon-google-plus-sign');
          that.addClass('icon-spinner icon-spin');

        $.ajax({
          type: "POST",
          url: OPS.base.www  + 'googledrive-csv',
          data: {filename: filename, filepath: filepath},
          error: function (xhr, ajaxOptions, thrownError) {
            that.removeClass('icon-spinner icon-spin');
            that.addClass('icon-warning-sign');
          },
          success: function(data) {
            that.removeClass('icon-spinner icon-spin');
            that.addClass('icon-google-plus');
            that.off('click');
            that.click(function() {
              window.open(data);
            });
          }
        });

    });

  }

};


// GENERIC - CSV EXPORT
// HTML SCHEMA:
// <i title="CSV download" class="icon-cloud-download csv-request" data-href="https://oneproseo.advertising.de/dev-oneproseo/temp/export/link-silo-data-1434370890.csv"></i>

OPS.csv = {

  download: function() {

    var csv_request = $('.csv-request');

    csv_request.click(function() {

      var that = $(this);
      var filepath = that.data('href');
      that.click(function() {
        window.open(filepath);
      });

    });

  }

};

// DATA TABLE GENERATORS - DATA TABLE GENERATORS - DATA TABLE GENERATORS - DATA TABLE GENERATORS - DATA TABLE GENERATORS - DATA TABLE GENERATORS - DATA TABLE GENERATORS - DATA TABLE GENERATORS

OPS.dataTable = {

  init: function(sortrow, sortdir, index) {

    if(typeof sortrow === 'undefined'){
       var sortrow = 0;
    };
    if(typeof sortdir === 'undefined'){
       var sortdir = 'asc';
    };
    if(typeof index === 'undefined'){
       var index = true;
    };

    var table = $('.data-table').DataTable( {
    // var table = $('.data-table').dataTable( {

       // create row index
       fnDrawCallback: function ( oSettings ) {
        if ( index === true ) {
          if ( oSettings.bSorted || oSettings.bFiltered ) {
            for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ) {
              $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
            }
          }
        }
       },
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 1000,
       sPaginationType: 'full_numbers',
       sDom: '<\"table-header\"fil>t<\"table-footer\"ip>',
       order: [[ sortrow, sortdir ]],
       aLengthMenu: [[100, 500, 1000, 5000, -1], [100, 500, 1000, 5000, 'All']]
     });

    // search after 3
    $(".dataTables_filter input")
      // unbind previous default bindings
      .unbind()
      .keydown(function(e) { // Bind our desired behavior
        if (this.value.length >= 3 || e.keyCode == 13) {
          table.search(this.value).draw();
        }
        if (this.value == "") {
          table.search("").draw();
        }
        return;
    });

  },

  init_noindex: function(sortrow, sortdir, index) {

    if(typeof sortrow === 'undefined'){
       var sortrow = 0;
    };
    if(typeof sortdir === 'undefined'){
       var sortdir = 'asc';
    };
    if(typeof index === 'undefined'){
       var index = true;
    };

    var t = $('.data-table').dataTable( {
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 1000,
       sPaginationType: 'full_numbers',
       sDom: '<\"table-header\"fil>t<\"table-footer\"ip>',
       order: [[ sortrow, sortdir ]],
       aLengthMenu: [[100, 500, 1000, 5000, -1], [100, 500, 1000, 5000, 'All']]
     });
  },

  columnsearchAjax: function() {

    // create search input fields
    $('.data-table thead td').each( function () {
      var title = $('.data-table thead td').eq($(this).index()).text();
      $(this).append( '<br /><input type="text" class="colsearch" placeholder="Search '+title+'" />' );
    });

    // init table
    var table = $('.data-table').DataTable({

       sPaginationType: "full_numbers",
       sDom: "<\"table-header\"il>t<\"table-footer\"ip>",
       aaSorting: [[ 1, "desc" ]],
       aLengthMenu: [[100, 500], [100, 500]],
       processing: true,
       serverSide: true,
       ajax: OPS.base.www + 'ajax/keywords/keywords',
       columns: [
          { "data": "keyword" },
          { "data": "opi" },
          { "data": "searchvolume" },
          { "data": "cpc" },
          { "data": "competition" }
        ]

    });

    // apply the search
    table.columns().eq(0).each( function (colIdx) {
      $('input', table.column(colIdx).header() ).on( 'keyup change', function () {
        table.column(colIdx).search(this.value).draw();
      });
    });

  },

  columnsearch: function(element, sortrow, sortdir, index) {

    if(typeof sortrow === 'undefined'){
       var sortrow = 0;
    };
    if(typeof sortdir === 'undefined'){
       var sortdir = 'asc';
    };
    if(typeof index === 'undefined'){
       var index = true;
    };

    // create search input fields
    $( element + ' thead tr:first-child td').each( function () {
      var title  = $(element + ' thead tr:first-child td').eq($(this).index()).text();
      var search = $(element + ' thead tr:first-child td').eq($(this).index()).attr('class');
      if (title != '#' && title.length > 1 && search != 'nosearch') {
        $(this).append( '<br /><input type="text" class="colsearch" placeholder="Search '+title+'" />' );
      }
    });

    // init table
    var table = $(element).DataTable( {
       // create row index
       fnDrawCallback: function ( oSettings ) {
        if ( index === true ) {
          if ( oSettings.bSorted || oSettings.bFiltered ) {
            for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ) {
              $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
            }
          }
        }
       },
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 1000,
       sPaginationType: 'full_numbers',
       sDom: '<\"table-header\"fil>t<\"table-footer\"ip>',
       order: [[ sortrow, sortdir ]],
       aLengthMenu: [[100, 500, 1000, 5000, -1], [100, 500, 1000, 5000, 'All']]
     });

    // apply the search
    table.columns().eq(0).each( function (colIdx) {
      $('input', table.column(colIdx).header() ).on( 'keyup change', function () {

        let type = this.value.startsWith('--');

        if (type === true) {
          let str = this.value.substring(2);
          regExSearch = '^((?!'+str+').)*$';
          table.column(colIdx).search(regExSearch ,true,false).draw();
        } else {
          table.column(colIdx).search(this.value).draw();
        }

      });

    });

    // search after 3
    $(".dataTables_filter input")
      // unbind previous default bindings
      .unbind()
      .keydown(function(e) { // Bind our desired behavior
        if (this.value.length >= 3 || e.keyCode == 13) {
          table.search(this.value).draw();
        }
        if (this.value === "") {
          table.search("").draw();
        }
        return;
    });

    $('a.toggle-vis').on( 'click', function (e) {
      e.preventDefault();
      var column = table.column( $(this).attr('data-column') );
      column.visible( ! column.visible() );
    });

  },

  columRangeBalancer: function (col) {

    $.fn.dataTable.ext.search.push (
      function( settings, data, dataIndex ) {
        var min = parseInt( $('#min'+col).val(), 10 );
        var max = parseInt( $('#max'+col).val(), 10 );
        var val = data[col];
        var age = parseFloat(val) || 0;
        if ( ( isNaN( min ) && isNaN( max ) ) || ( isNaN( min ) && age <= max ) || ( min <= age   && isNaN( max ) ) || ( min <= age   && age <= max ) )
        {
          return true;
        }
        return false;
      }
    );

  },

  columnsearchBalancer: function(element, sortrow, sortdir, index) {

    if(typeof sortrow === 'undefined'){
       var sortrow = 0;
    };
    if(typeof sortdir === 'undefined'){
       var sortdir = 'asc';
    };
    if(typeof index === 'undefined'){
       var index = true;
    };

    // create search input fields
    $( element + ' thead tr:first-child td').each( function () {
      var title = $(element + ' thead tr:first-child td').eq($(this).index()).text();
      if (title != '#' && title.length > 1) {
        $(this).append( '<br /><input type="text" class="colsearch" placeholder="Search '+title+'" />' );
      }
    });

    OPS.dataTable.columRangeBalancer(7);
    OPS.dataTable.columRangeBalancer(8);
    OPS.dataTable.columRangeBalancer(9);
    OPS.dataTable.columRangeBalancer(10);
    OPS.dataTable.columRangeBalancer(14);
    OPS.dataTable.columRangeBalancer(15);
    OPS.dataTable.columRangeBalancer(16);
    OPS.dataTable.columRangeBalancer(17);
    OPS.dataTable.columRangeBalancer(18);
    OPS.dataTable.columRangeBalancer(21);
    OPS.dataTable.columRangeBalancer(22);
    OPS.dataTable.columRangeBalancer(23);
    OPS.dataTable.columRangeBalancer(24);

    // init table
    var table = $(element).DataTable( {
       // create row index
       fnDrawCallback: function ( oSettings ) {
        if ( index === true ) {
          if ( oSettings.bSorted || oSettings.bFiltered ) {
            for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ) {
              $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
            }
          }
        }
       },
       bJQueryUI: false,
       bAutoWidth: false,
       iDisplayLength: 1000,
       sPaginationType: 'full_numbers',
       sDom: '<\"table-header\"fil>t<\"table-footer\"ip>',
       order: [[ sortrow, sortdir ]],
       aLengthMenu: [[100, 500, 1000, 5000, -1], [100, 500, 1000, 5000, 'All']]
     });

    table.column(2).visible(false);
    table.column(3).visible(false);
    table.column(4).visible(false);
    table.column(6).visible(false);
    table.column(7).visible(false);
    table.column(8).visible(false);
    table.column(11).visible(false);
    table.column(12).visible(false);
    table.column(14).visible(false);
    table.column(15).visible(false);
    table.column(16).visible(false);
    table.column(17).visible(false);
    table.column(19).visible(false);
    table.column(21).visible(false);
    table.column(22).visible(false);
    table.column(23).visible(false);

    // apply the search
    table.columns().eq(0).each( function (colIdx) {
      $('input', table.column(colIdx).header() ).on( 'keyup change', function () {
        if (this.value.length >= 3 || e.keyCode == 13) {
          table.column(colIdx).search(this.value).draw();
        }
        if (this.value === "") {
          table.search("").draw();
        }
      });
    });

    // search after 3
    $(".dataTables_filter input")
      // unbind previous default bindings
      .unbind()
      .keydown(function(e) { // Bind our desired behavior
        if (this.value.length >= 3 || e.keyCode == 13) {
          table.search(this.value).draw();
        }
        if (this.value === "") {
          table.search("").draw();
        }
        return;
    });

    $('#min7, #max7, #min8, #max8, #min9, #max9, #min10, #max10, #min14, #max14, #min15, #max15, #min16, #max16, #min17, #max17, #min18, #max18, #min21, #max21, #min22, #max22, #min23, #max23, #min24, #max24').keyup( function() {
      table.draw();
    });


    $('.toggle-vis').on( 'click', function (e) {
      e.preventDefault();
      var column = table.column( $(this).attr('data-column') );
      column.visible( ! column.visible() );
      $('#min7, #max7, #min8, #max8, #min9, #max9, #min10, #max10, #min14, #max14, #min15, #max15, #min16, #max16, #min17, #max17, #min18, #max18, #min21, #max21, #min22, #max22, #min23, #max23, #min24, #max24').keyup( function() {
        table.draw();
      });

    });

    $("#selectall").change(function() {
      $(".selectkw").prop('checked', $(this).prop("checked"));
    });

    $("#copytoclipboard").click(function() {
      var selectedCheckboxValue = "";
      $(".selectkw:checked").each(function() {
        selectedCheckboxValue += $(this).val() + "\n";
      });
      OPS.basefunc.copyToClipboard(selectedCheckboxValue);
    });

  }

};

// RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED - RSS BLOG FEED

OPS.feed = {

  advertising: function() {

    var ops_news = $('#ops_news');

    $.ajax({
      type: "POST",
      url: OPS.base.www + 'ajax/feed/advertising',
      error: function (xhr, ajaxOptions, thrownError) {
        ops_news.html('Es ist ein Fehler aufgetreten.');
      },
      success: function(data) {
        ops_news.html(data);
      }
    });

  }

};
