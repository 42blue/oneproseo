
OPSC = {

  socialGetPinterest: function() {

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://api.pinterest.com/v1/urls/count.json?callback=OPSC.socialPinterest&url=http://enterprise.oneproseo.com/indexwatch/travel-hotel-benchmarking';
    $("body").append(script);

  },


  socialPinterest: function(payload) {

    $('.ag-social-pinterest span').html(payload.count);

  },

  socialButtons: function() {

    $.getJSON(OPS.base.www + 'ajaxp/socialcount/socialcount', {url: "http://enterprise.oneproseo.com/indexwatch/travel-hotel-benchmarking"}, function( data ) {

      $.each( data, function( key, val ) {

        if (key == 'facebook') {
          $('.ag-social-facebook span').html(val);
        }
        if (key == 'twitter') {
          $('.ag-social-twitter span').html(val);
        }
        if (key == 'googleplus') {
          $('.ag-social-google span').html(val);
        }
        if (key == 'xing') {
          $('.ag-social-xing span').html(val);
        }
        if (key == 'linkedin') {
          $('.ag-social-linkedin span').html(val);
        }

      });
     
    });

    var socialButtons = $('.ag-social-button');

    socialButtons.click(function() {
      var new_window =  window.open(this.href, "Teile diesen Inhalt", "height=500,width=600,status=yes,toolbar=no,menubar=no,location=no");
      new_window.moveTo((screen.width / 2) - 300, (screen.height / 2) - 450);
      new_window.focus();
      return false;
    });

    var socialComp = $('.sb-group');

    if (document.referrer) {

      $.each(socialComp, function() {

        var ref = document.referrer,
            resort = $(this).children();

        if (ref.indexOf("facebook.com") > -1) {
          $(resort[0]).insertBefore(resort[0]);
          $(resort[0]).addClass('magnify');
        }

        if (ref.indexOf("twitter.com") > -1 || ref.indexOf("t.co") > -1) {
          $(resort[1]).insertBefore(resort[0]);
          $(resort[1]).addClass('magnify magnify-twitter');
        }

        if (ref.indexOf("plus.url.google.com") > -1) {
          $(resort[2]).insertBefore(resort[0]);
          $(resort[2]).addClass('magnify magnify-google');
        }

        if (ref.indexOf("xing.com") > -1) {
          $(resort[3]).insertBefore(resort[0]);
          $(resort[3]).addClass('magnify magnify-xing');
        }

        if (ref.indexOf("linkedin.com") > -1) {
          $(resort[4]).insertBefore(resort[0]);
          $(resort[4]).addClass('magnify magnify-linkedin');
        }

      });

    }

  }

};



$( document ).ready(function() {

  OPSC.socialGetPinterest();
  OPSC.socialButtons();

  $.getScript(OPS.base.www + 'oneproseo/src/custom/functions.js', function() {

    OPS.indexwatch.fetchOverviewTravel();

  });

});