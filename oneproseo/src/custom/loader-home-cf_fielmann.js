OPSCF = {};

OPSCF.rag = {

  rankings: function(elem, vals) {

    $(elem).each(function(){

      ragData   = vals;
      ragLabels = ['Platz 1-5', 'Platz 6-10', 'Platz 11-100'];
      ragOpts   = {postfix:'', rgaLength: 3};
      cf_rRags[$(this).prop('id')] = new RagChart($(this).prop('id'), ragData, ragLabels, ragOpts);

    });

  }

};


OPSCF.ragchange = {

  rankings: function(elem, vals, last) {

    $(elem).each(function() {

      var change1 = OPSCF.ragchange.calc(vals[0], last[0]);
      var change2 = OPSCF.ragchange.calc(vals[1], last[1]);
      var change3 = OPSCF.ragchange.calc(vals[2], last[2]);

      ragData   = vals;
      ragLabels = ['Platz 1-5: ' +  change1, 'Platz 6-10: ' +  change2, 'Platz 11-100: ' +  change3];
      ragOpts   = {postfix:'', rgaLength: 3};
      cf_rRags[$(this).prop('id')] = new RagChart($(this).prop('id'), ragData, ragLabels, ragOpts);

    });

  },

  calc: function (today, week) {

    var val  = today - week;
    var perc = val * 100 / week;

    if (val < 0) {
      return '<br /><span class="m-red">' + Math.round(perc * 100) / 100 + '%</span>';
    } else {
      return '<br /><span class="m-green">+' + Math.round(perc * 100) / 100 + '%</span>';
    }

  }

};

OPSCF.sparkline = {

  render: function(elem, vals) {

    $(elem).each(function(){
      var sparkOptions = cf_defaultSparkOpts;
      data = vals;
      createSparkline($(this), data, sparkOptions);
    });

  }

};


$( document ).ready(function() {

  $.getJSON(OPS.base.www + 'ajax/rankings/customdashboard/cf_dash_coredata', function(json) {

    $('#OPSCF_sistrix_view').html(json.sistrix_view);
    OPSCF.sparkline.render('.sparkline-sistrix', json.sistrix_chart);

    $('#OPSCF_sistrix_view').html(json.sistrix_view);
    OPSCF.sparkline.render('.sparkline-sistrix', json.sistrix_chart);

    OPSCF.ragchange.rankings('.cf-rankings-1', json.rankings_today, json.rankings_week);
    OPSCF.rag.rankings('.cf-rankings-2', json.rankings_week);

    $('#OPSCF_keywords').html(json.keywords);
    $('#OPSCF_gwt').html(json.gwt);
    $('#OPSCF_gwt_2').html(json.gwtMonth);

    $('#OPSCF_figures_1').html(json.figuresOrganic);
    $('#OPSCF_figures_2').html(json.figuresDevices);

  });


});
