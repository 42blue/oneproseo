$( document ).ready(function() {

  $.getScript(OPS.base.www + '/oneproseo/src/custom/functions.js', function() {

    $(':radio[name=customer]').on('ifClicked', function(event){
      if ($(this).val() == 1) {
        $("#ops_show").removeClass('hidden');
      } else {
        $("#ops_show").addClass('hidden');
      }
    });

    $('.ops_delete').click(function(e){
      var r = confirm('Sicher?');
      if (r !== true) {
        e.preventDefault();
      }
    });

    OPS.fetchCrawlStats.coreNumbers();

    OPS.dataTable.init();

  });

});
