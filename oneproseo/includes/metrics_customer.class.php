<?php

class metrics
{

  public function __construct ($env_data)
  {

    $this->env_data = $env_data;
    $this->customer_id        = $this->env_data['customer_id'];
    $this->customer_available = $this->env_data['customer_available'];

    $this->mySqlConnect();

    $this->fetchData();

    $this->mySqlClose();

  }

  public function getData () 
  {

    return $this->rows;

  }


  // MYSQL MAIN CONNECTOR
  public function mySqlConnect ()
  {

    $this->db = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

    // set charset according to DB 
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

  }

  // MYSQL CLOSER
  public function mySqlClose ()
  {

    $this->db->close();

  }


  private function fetchData()
  {

    // COUNT KEYWORDS
    $sql = "SELECT
                   a.id             AS id,
                   a.country        AS country,
                   b.id_customer    AS id_customer,
                   b.rs_type        AS rs_type,
                   c.keyword        AS keyword
            FROM ruk_project_customers a
              LEFT JOIN ruk_project_keyword_sets b
                ON b.id_customer = a.id
                LEFT JOIN ruk_project_keywords c
                  ON c.id_kw_set = b.id
            WHERE a.id IN ($this->customer_available)";

    $res = $this->db->query($sql);

    $project_keywords = array();
    while ($row = $res->fetch_assoc()) {
      if (!empty($row['keyword'])) {
        $project_keywords[$row['keyword'].$row['country'].$row['rs_type']] = $row['keyword'];
      }

    }

    $this->rows[2] = count($project_keywords);

    $sql = "SELECT 
              id,
              id_customer,
              gen_results_host,
              gen_results_url,
              api_results_host,
              api_results_url,
              timestamp
            FROM 
              ruk_project_sites_index
            WHERE
              DATE(timestamp) > CURDATE() - INTERVAL 7 DAY
            AND 
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            AND
              id_customer = '$this->customer_id'
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $sitesinindex[] = $row['gen_results_url'];
    }


    $this->rows[0] = $sitesinindex[0];
    $this->rows[1] = $this->rows[0] * 0.96;

  }

}

?>