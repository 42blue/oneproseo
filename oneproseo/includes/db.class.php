<?php

class db {

  public function __construct () {}

  public function mySqlConnect ($env_data)
  {

    $db = new mysqli($env_data['mysql_dbhost'], $env_data['mysql_dbuser'], $env_data['mysql_dbpass'], $env_data['mysql_dbname']);

    // set charset according to DB 
    $db->set_charset('utf8');

    if (mysqli_connect_errno()) {

      echo ('Connect failed:' .  mysqli_connect_error());

    }

    return $db;

  }

}

?>