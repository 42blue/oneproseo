<?php

/*

-> take array -> keyword, lang
-> get searchvolume from local db
-> if keyword not in local db query adwords api
-> save new adwords data to local db
-> return array -> keyword, lang, sv, cpc, comp, opi

*/


class svolume
{

  private $api_endpoint = 'https://oneproseo.advertising.de/oneproapi/adwords/service/gSearchVolume.php';
  private $api_endpoint_monthly = 'https://oneproseo.advertising.de/oneproapi/adwords/service/gSearchVolumeMonthly.php';

  public function __construct ($db, $env_data)
  {

    $this->db       = $db;
    $this->env_data = $env_data;
    $this->db       = new mysqli($this->env_data['mysql_dbhost'], $this->env_data['mysql_dbuser'], $this->env_data['mysql_dbpass'], $this->env_data['mysql_dbname']);

  }


  public function querySV ($array)
  {

    //alter data for comparison
    foreach ($array as $key => $arr) {
      $hash              = $arr[0] .'_' . $arr[2];
      $user_query[$hash] = array(strtolower($arr[0]), $arr[1], $arr[2]);
      $kw                = str_replace("'", " ", $arr[0]);
      $keywords[]        = str_replace('"', ' ', $kw);
      $this->lang        = $arr[1];
      $this->country     = $arr[2];
    }

    $local_data = $this->getAdwordsDataLocal($keywords);

    // no data in local db
    $payload = array();
    foreach ($user_query as $hash => $data) {
      if (!isset($local_data[$hash])) {
        $payload[] = $data[0];
      }
    }

    // query adwords api and save data local
    if (!empty($payload)) {

      $live_data = array();
      $live      = $this->getAdwordsDataLive($payload);

      if (!isset($live->error)) {

        $live_data  = $this->saveToLocal($live, $payload);
        $local_data = array_merge ($local_data, $live_data);
        //echo 'adword';

      } else {

      	$livegrep = $this->getGrepwordsDataLive($payload);
        $live_data  = $this->saveToLocal($livegrep, $payload);
        $local_data = array_merge ($local_data, $live_data);
				//echo 'grepword';
      }

    }

    // API NO DATA
    foreach ($user_query as $hash => $arr) {
    	if (!isset($local_data[$hash])) {
    		$local_data[$hash] = array ($arr[0], false, false, false, false, $arr[2], $arr[2], 'api_no_data');
    	}    	
    }

    return $local_data;

  }

  private function saveToLocal ($live) {

    $live_data = array();

		if (empty($live->set)) {
			return $live_data = array();
		}

    foreach ($live->set as $k => $v) {

      $language = $this->lang;
      $country  = $this->country;
      $rep      = array(';', '"', ',', ', ', "'");
      $keyword  = str_replace($rep, '', $v->kw);
      $keyword  = $this->db->real_escape_string($keyword);
      $row1     = floatval($v->cpc);
      $row2     = floatval($v->sv);
      $row3     = floatval($v->comp);

      if (($row1 == 0) && ($row2 == 0) && ($row3 == 0)) {
        $opi   = 0;
        $larry = 0;
      } else {

        $r1 = $row1;
        $r2 = $row2;
        $r3 = $row3;

        if ($row1 == 0) { $r1 = 0.01; }
        if ($row2 == 0) { $r2 = 0.01; }
        if ($row3 == 0) { $r3 = 0.01; }

        $opi   = $r1 * $r2 * $r3;
        $larry = round($r2 * $r1 / $r3);
        
      }

      $hash             = $keyword .'_'. $language;
      $live_data[$hash] = array ($keyword, $row2, $row1, $row3, $opi, $language, $country, 'live');

      $keyword = $this->db->real_escape_string($keyword);

      $sql = 'INSERT INTO
                gen_keywords (keyword, language, country, timestamp, searchvolume, cpc, competition, opi_larry, opi)
              VALUES
                ("'.$keyword.'", "'.$language.'", "'.$country.'", CURDATE(), "' . $row2 .'","' . $row1 .'","' . $row3 .'","' . $larry .'","' . $opi .'")
              ON DUPLICATE KEY UPDATE
                keyword      = "'.$keyword.'",
                language     = "'.$language.'",
                country      = "'.$country.'",
                timestamp    = CURDATE(),
                searchvolume = "'.$row2.'",
                cpc          = "'.$row1.'",
                competition  = "'.$row3.'",
                opi_larry    = "'.$larry.'",
                opi          = "'.$opi.'"
            ';

      $result = $this->db->query($sql);

    }

    return $live_data;

  }


  private function getAdwordsDataLive ($payload) {

    $payload = base64_encode(json_encode($payload));

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => 'keyword_arr=' . $payload . '&lang=' . $this->lang . '&country=' . $this->country
      )
    );

    $context  = stream_context_create($opts);
    $result   = @file_get_contents($this->api_endpoint, false, $context);

    if (stripos($result, 'RATE_EXCEEDED') !== FALSE) {
      sleep(35);
      $result   = @file_get_contents($this->api_endpoint, false, $context);
    }

    $res = json_decode($result);

    return $res;

  }


  private function getGrepwordsDataLive ($payload) {

    $payload = base64_encode(json_encode($payload));

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => 'keyword_arr=' . $payload . '&lang=' . $this->lang . '&country=' . $this->country
      )
    );

    $context  = stream_context_create($opts);
    $result   = @file_get_contents('https://oneproseo.advertising.de/oneproapi/grepwords/service.php', false, $context);

    $res = json_decode($result);

    return $res;

  }


  public function getAdwordsDataLiveMonthly ($array) {

    //alter data for comparison
    foreach ($array as $key => $arr) {
      $hash              = $arr[0] .'_' . $arr[2];
      $user_query[$hash] = array(strtolower($arr[0]), $arr[1], $arr[2]);
      $kw                = str_replace("'", " ", $arr[0]);
      $keywords[]        = str_replace('"', ' ', $kw);
      $this->lang        = $arr[1];
      $this->country     = $arr[2];
    }

    // no data in local db
    $payload = array();
    foreach ($user_query as $hash => $data) {
      if (!isset($local_data[$hash])) {
        $payload[] = $data[0];
      }
    }


    $payload = base64_encode(json_encode($payload));

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => 'keyword_arr=' . $payload . '&lang=' . $this->lang . '&country=' . $this->country
      )
    );

    $context  = stream_context_create($opts);
    $result   = @file_get_contents($this->api_endpoint_monthly, false, $context);

    if ($result == false) {
      sleep(5);
      $result   = @file_get_contents($this->api_endpoint_monthly, false, $context);
    }

    $res = json_decode($result);

    return $res;

  }


  public function getAdwordsDataLocal ($keywords)
  {

    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              keyword,
              searchvolume,
              cpc,
              competition,
              opi,
              language,
              country
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              country = '$this->country'
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $local_data = array();

    if ($res->num_rows < 1) {
      return $local_data;
    }

    while ($row = $res->fetch_assoc()) {
      $hash              = $row['keyword'] .'_'. $row['country'];
      $local_data[$hash] = array ($row['keyword'], $row['searchvolume'], $row['cpc'], $row['competition'], $row['opi'], $row['language'], $row['country'], 'local');
    }

    return $local_data;

  }

}

?>
