<?php

class localsession
{

  public function __construct ()
  {

    $this->getActiveSessions();

  }

  private function getActiveSessions ()
  {
    $filename = session_save_path() . '/sess_' . strip_tags(trim($_GET['sid']));
    if (!isset($_SESSION['login'])) {
      $_SESSION['login'] = false;
    }
    if (file_exists($filename)) {
      $fh = file_get_contents($filename);
      if (!empty($fh) && stripos($fh, 'user') !== FALSE) {
        $_SESSION['login'] = true;
      }
    }
  }

}

?>
