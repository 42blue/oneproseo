<?php


class rankings
{

  public $result = array();

  public function __construct ($hostname, $keyword, $lang)
  {

    $this->str_hostname  = $hostname;
    $this->str_keyword   = $keyword;
    $this->lang          = $lang;
    $this->scraper_hosts = unserialize(SCRAPER_HOSTS);

    $this->gofindTP();

  }


  public function gofind ()
  {

    $urls = array();

    $query = urlencode('site:' . $this->str_hostname . ' ' . $this->str_keyword);
    $request = 'http://'.$this->scraper_hosts[rand(0,9)].'/scrape/evilScraperSite.php?lang='.$this->lang.'&kw=' . $query;

    $ctx = stream_context_create(array('http'=> array('timeout' => 120)));

    $resobj = $this->gofindTP();
    
    reset($resobj);
    $key = key($resobj);
    $resobj = $resobj->$key;

    // SCRAPER FALLBACK
    if (empty($resobj)) {
      $result = file_get_contents($request, false, $ctx);
      $resobj = json_decode($result);
    }

    if (!is_object($resobj) || $resobj == FALSE || is_null($resobj) || empty($resobj) || $resobj == NULL) {
      $urls = array('error' => 'Not JSON - Please contact Matthias');
    } else {

      foreach ($resobj->results as $k => $value) {
        if (stripos($value->url, 'maps.google') !== false){
          continue;
        }
        $urls[] = $value->url;
      }

    }
    // only use the first 50 urls
    $urls = array_slice($urls, 0, 50);

    $this->result = $urls;

  }


  public function gofindTP () {

    $keywords = array();
    $keywords[] = array('site:' . $this->str_hostname . ' ' . $this->str_keyword, 'de');

    $payload = json_encode($keywords);
    $payload = base64_encode($payload);

    $postdata = http_build_query(array('data' => $payload));
    $opts     = array('http' =>
        array(
            'method'  => 'POST',
            'timeout' => 240,
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    $context = stream_context_create($opts);
    $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/crawlinski/client.php', false, $context);

    $json = json_decode($output);

    reset($json);
    $key = key($json);
    $resobj = $json->$key;

    if (!is_object($resobj)) {
      $urls = array('error' => 'Not JSON - Please contact Matthias');
    } else {

      foreach ($resobj->results as $k => $value) {
        if (stripos($value->url, 'maps.google') !== false){
          continue;
        }
        $urls[] = $value->url;
      }

    }

    // only use the first 50 urls
    $urls = array_slice($urls, 0, 50);

    $this->result = $urls;

  }

  public function getResult () {

    return $this->result;

  }


}

?>
