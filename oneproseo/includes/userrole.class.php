<?php

class Userrole {

  public function __construct ($slim) {

    if ($_SESSION['role'] == 'admin') {

      $_SESSION['role2'] = 'admin';
      $slim->view->setData('modules_crawler', 0);
      $slim->view->setData('modules_seocrawler', 1);
      $slim->view->setData('modules_ranker', 1);
      $slim->view->setData('modules_keyworder', 1);
      $slim->view->setData('modules_structure', 1);
      $slim->view->setData('modules_botti', 1);
      $slim->view->setData('modules_sea', 1);
      $slim->view->setData('modules_tools', 1);
      $slim->view->setData('modules_dev', 1);
      $slim->view->setData('modules_admin', 1);
      $slim->view->setData('modules_linktracker', 1);

    } else if ($_SESSION['role'] == 'redakteur') {

      $_SESSION['role2'] = 'editor';
      $slim->view->setData('modules_crawler', 0);
      $slim->view->setData('modules_seocrawler', 1);
      $slim->view->setData('modules_ranker', 0);
      $slim->view->setData('modules_keyworder', 1);
      $slim->view->setData('modules_structure', 0);
      $slim->view->setData('modules_botti', 0);
      $slim->view->setData('modules_tools', 1);
      $slim->view->setData('modules_dev', 0);
      $slim->view->setData('modules_admin', 0);
      $slim->view->setData('modules_linktracker', 0);

    } else if ($_SESSION['role'] == 'pjm') {

      $_SESSION['role2'] = 'admin';
      $slim->view->setData('modules_crawler', 1);
      $slim->view->setData('modules_ranker', 1);
      $slim->view->setData('modules_keyworder', 1);
      $slim->view->setData('modules_structure', 1);
      $slim->view->setData('modules_botti', 1);
      $slim->view->setData('modules_sea', 1);      
      $slim->view->setData('modules_tools', 1);
      $slim->view->setData('modules_dev', 0);
      $slim->view->setData('modules_admin', 0);
      $slim->view->setData('modules_linktracker', 1);

    } else {

      $_SESSION['role2'] = 'editor';
      $slim->view->setData('modules_crawler', 0);
      $slim->view->setData('modules_seocrawler', 1);
      $slim->view->setData('modules_ranker', 0);
      $slim->view->setData('modules_keyworder', 1);
      $slim->view->setData('modules_structure', 0);
      $slim->view->setData('modules_botti', 0);
      $slim->view->setData('modules_tools', 0);
      $slim->view->setData('modules_dev', 0);
      $slim->view->setData('modules_admin', 0);
      $slim->view->setData('modules_linktracker', 0);

    }

  }

}

?>
