<?php

class logtofile {

  public function __construct ($env_data) {

    $this->env_data = $env_data;

  }

  public function write ($modul, $content, $filepath) {

    $file = '/var/www/enterprise.oneproseo.com/' . $filepath;

    if (isset($_SESSION['lastname'])) {
      $user = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];
    } else if (isset($this->env_data['customer_username'])) {
      $user = $this->env_data['customer_username'];
    } else {
      $user = 'UNKOWN USER (NO SESSION DATA)';
    }

    $file_data = $this->dateTS() . ' - User: ' . $user . ' // ' . $modul . ' - ' . $content . "\r\n";

    if (@file_get_contents($file) !== FALSE) { 
      $file_data .= file_get_contents($file);
      file_put_contents($file, "\xEF\xBB\xBF" . $file_data);
    } else {
      file_put_contents($file, "\xEF\xBB\xBF" . $file_data);
    }

  }

  public function writeLogin ($content, $filepath) {

    $file = '/var/www/enterprise.oneproseo.com/' . $filepath;

    if (isset($_SESSION['lastname'])) {
      $user = $_SESSION['firstname'] . ' ' . $_SESSION['lastname'];
    } else if (isset($this->env_data['customer_username'])) {
      $user = $this->env_data['customer_username'];
    } else {
      $user = 'UNKOWN USER (NO SESSION DATA)';
    }

    $file_data = $this->dateTS() . ' - User: ' . $user . ' // ' . $content . "\r\n";

    if (@file_get_contents($file) !== FALSE) { 
      $file_data .= file_get_contents($file);
      file_put_contents($file, "\xEF\xBB\xBF" . $file_data);
    } else {
      file_put_contents($file, "\xEF\xBB\xBF" . $file_data);
    }

  }

  public function dateTS ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("H:i @ Y-m-d", $date);

  }

}

?>