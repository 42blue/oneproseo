<?php

class Cache {

  public function setEnv ($env) {

    $this->env = $env;

  }

  public function readFile ($fileName, $resetTimeH = false) {

    $fileName = $this->env['tempcache'] . $fileName;

    if ($resetTimeH !== false && file_exists($fileName)) {
      if (date("U",filectime($fileName) < time() - $resetTimeH * 3600)) {
        unlink($fileName);
      }
    }

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      return unserialize($variable);

    } else {

      return array();

    }

  }

  public function writeFile ($fileName, $variable) {

    $fileName = $this->env['tempcache'] . $fileName;

    $handle = fopen($fileName, 'a');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }


  public function readFileRaw ($fileName, $resetTimeH = false) {

    $fileName = $this->env['tempcache'] . $fileName;

    if ($resetTimeH !== false && file_exists($fileName)) {
      if (date("U",filectime($fileName) < time() - $resetTimeH * 3600)) {
        unlink($fileName);
      }
    }

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      return $variable;

    } else {

      return array();

    }

  }

  public function writeFileRaw ($fileName, $variable) {

    $fileName = $this->env['tempcache'] . $fileName;

    $handle = fopen($fileName, 'a');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function deleteFile ($fileName) {

    $fileName = $this->env['tempcache'] . $fileName;

    $del = false;
    if (file_exists($fileName)) {
      $del = unlink($fileName);
    }

    return $del;

  }

}

?>
