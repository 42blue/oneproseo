<?php

class ops_menu {

  public function __construct () {

    $this->menu = array();

//    $this->menu['crawler']        = 'OneProCrawler';
    $this->menu['seocrawler']     = 'OneProSeoCrawler';
    $this->menu['siteclinic']     = 'Siteclinic';
    $this->menu['rankings']       = 'OneProRanker';
    $this->menu['analytics']      = 'OneProAnalytics';
    $this->menu['botti']          = 'OneProBotti';
    $this->menu['linktracker']    = 'OneProLinkTracker';
    $this->menu['keywords']       = 'OneCampBuilder';
    $this->menu['tools']          = 'OneProTools';
    $this->menu['dev']            = 'OneProDev';
    $this->menu['admin']          = 'Administration';
    $this->menu['structure']      = 'OneProStructure';
    $this->menu['seaseobalancer'] = 'OneProSEA';
    $this->menu['lighthouse']     = 'Lighthouse';

  }

  public function getMenu() {
    return $this->menu;
  }

}

?>
