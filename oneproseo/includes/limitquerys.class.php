<?php

/*

Limit Live Ranking Querys for customer versions

*/


class limitquerys
{

  public function __construct ($db, $env_data)
  {

    $this->db       = $db;
    $this->env_data = $env_data;

  }


  public function checkLimit () 
  {

    if ($this->env_data['customer_version'] == 1) {

      $customer_id = $this->env_data['customer_id'];
      $max_querys  = 100;

      // check if project name exists
      $sql = "SELECT id, liveranking_count FROM ruk_project_customers WHERE id = $customer_id";
      $result = $this->db->query($sql);
      while ($row = $result->fetch_assoc()) {
        $count = $row['liveranking_count'];
      }

      // no limit
      if ($count == -1) {
        return;
      }

      if ($count < $max_querys) {

        $count = $count + 1;
        $sql = "UPDATE ruk_project_customers SET liveranking_count = $count WHERE id = $customer_id";
        $result = $this->db->query($sql);
        return;

      } else {

        echo '<div class="row padded"><div class="col-md-12"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
        <strong>Es wurden heute schon '.$count.' Abfragen ausgeführt.<br/>Um den Service zuverlässig anbieten zu können, ist das Tool auf ' . $max_querys . ' Abfragen pro Tag begrenzt.<br/>Versuchen Sie es bitte morgen wieder.</strong></div></div></div>';
        exit;

      }

    }

  }

}

?>