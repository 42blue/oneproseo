<?php

class metrics
{

  public function __construct ($env_data)
  {

    $this->env_data = $env_data;

    $this->fetchData();

  }

  public function getData () 
  {

    return $this->rows;

  }


  private function fetchData()
  {

    $cache = new Cache();
    $cache->setEnv($this->env_data);

    $tmpfilename = 'metrics_rankings.tmp';

    $row = $cache->readFile($tmpfilename, 24);

    $this->rows = $row;

  }

}

?>