<?php

set_time_limit(800);

/*

Helper Class for the Silo / Structure Tool to gather live rankings.

https://oneproseo.advertising.de/oneproapi/trustedproxy/tpscraper.php

1. delete all db entrys where timestamp > 7 days

2. check if keywords are scraped within the last 7 days

  -> SQL structure_scrape_keywords
  -> SQL structure_scrape_rankings
  = keyword / rank / url  -> result

3. scrape all keywords that have no data

  -> write results to db
  -> find hostname in scrape
  = keyword / rank / url  -> result

4. result

*/


class liveranking
{

  public function __construct ($keywords, $hostname, $db, $lang = 'de', $subdomain = '', $type = 'desktop')
  {

    // 'mobile_de'

    $this->keywords  = $keywords;
    $this->hostname  = $hostname;
    $this->db        = $db;
    $this->lang      = $lang;
    $this->subdomain = $subdomain;
    $this->type      = $type;

    // rankings are stored for 7 days and will then deleted
    $this->deleteExpiredRankings();

    $this->extractKeywords();

    $this->getRankingData();
    $this->getRankingDataStructure();

    // all keywords that are not in db
    $this->keywordstoScrape();

    // LIMIT TO 250 KEYWORDS
    $this->scrapekeywords = array_slice($this->scrapekeywords, 0, 300);    

    // chunk request to 50 cause the proxy does not take more
    $chunked_scrapekeywords = array_chunk ($this->scrapekeywords, 100);

    foreach ($chunked_scrapekeywords as $key => $chunked_array) {

      $this->scrapekeywords = $chunked_array;

      if (!empty($this->scrapekeywords)) {
        // scrape and save all keywords that have no ranking
        $this->scrapeCurl();
        $this->saveRankings();
        $this->checkRankings();
      }

    }

  }


  public function getResult ()
  {

    return $this->result;

  }

  public function getResultMulti ()
  {

    return $this->resultMulti;

  }


  private function extractKeywords ()
  {

    $this->searchkeywords = array();
    foreach ($this->keywords as $key => $value) {
      $this->searchkeywords[] = $value[0];
    }

  }


  private function getRankingData()
  {

    $this->result  = array();
    $this->resultMulti  = array();    

    $comma_separated_kws = implode('", "', $this->searchkeywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $lang = $this->lang;
    if ($this->type == 'mobile') {
      $lang = 'mobile_' . $this->lang;
    }

    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$lang'
            AND
              DATE(timestamp) = CURDATE()";

    $result = $this->db->query($sql);

    $rows_kw = array();
    $this->kw_in_db = array();

    if (!$result) {
      return;
    }

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
      $this->kw_in_db[$row['keyword']] = $row['keyword'];
      // polyfill
      $timestamp = strtotime($row['timestamp']);
      $this->result[$row['keyword']] = array();      
    }

    if (count($rows_kw) == 0) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    while ($row = $result2->fetch_assoc()) {

      if ($this->subdomain == 'true') {
        if ($this->removeSubdomain($row['hostname']) == $this->hostname) {

          $timestamp = strtotime($rows_kw[$row['id_kw']]['timestamp']);
          $this->resultMulti[$rows_kw[$row['id_kw']]['keyword']][] = array('p' => $row['position'], 'u' => $row['url'], 'ts' => $timestamp, 'fe' => 0);

          // take only the first match
          if ($this->result[$rows_kw[$row['id_kw']]['keyword']]['p'] !== false) {continue;}

          $this->result[$rows_kw[$row['id_kw']]['keyword']] = array('p' => $row['position'], 'u' => $row['url'], 'ts' => $timestamp, 'fe' => 0);

        }
      } else {
        if ($row['hostname'] == $this->hostname) {

          $timestamp = strtotime($rows_kw[$row['id_kw']]['timestamp']);
          $this->resultMulti[$rows_kw[$row['id_kw']]['keyword']][] = array('p' => $row['position'], 'u' => $row['url'], 'ts' => $timestamp, 'fe' => 0);

          // take only the first match
          if ($this->result[$rows_kw[$row['id_kw']]['keyword']]['p'] !== false) {continue;}
          $this->result[$rows_kw[$row['id_kw']]['keyword']] = array('p' => $row['position'], 'u' => $row['url'], 'ts' => $timestamp, 'fe' => 0);

        }
      }

    }

  }


  private function getRankingDataStructure()
  {

    $comma_separated_kws = implode('", "', $this->searchkeywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $lang = $this->lang;
    if ($this->type == 'mobile') {
      $lang = 'mobile_' . $this->lang;
    }


    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              structure_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$lang'";

    $result = $this->db->query($sql);

    if (!$result) {
      return;
    }

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {

      $rows_kw[$row['id']] = $row;

      $this->kw_in_db[$row['keyword']] = $row['keyword'];

      if (!isset($this->result[$row['keyword']])) {
        $this->result[$row['keyword']] = array();
      }

    }


    if (count($rows_kw) == 0) {
      return;
    }


    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname,
              description
            FROM
              structure_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    while ($row = $result2->fetch_assoc()) {

      if ($this->subdomain == 'true') {
        if ($this->removeSubdomain($row['hostname']) == $this->hostname) {
          
          // ONLY GET FIRST RANKING
          if ($this->result[$rows_kw[$row['id_kw']]['keyword']]['p'] == false) {
            $this->result[$rows_kw[$row['id_kw']]['keyword']] = array(
              'p' => $row['position'], 'u' => $row['url'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'fe' => $row['description']
            );
          }

          $this->resultMulti[$rows_kw[$row['id_kw']]['keyword']][] = array(
            'p' => $row['position'], 'u' => $row['url'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'fe' => $row['description']
          );

        }
      } else {
        if ($row['hostname'] == $this->hostname) {
          
          // ONLY GET FIRST RANKING
          if ($this->result[$rows_kw[$row['id_kw']]['keyword']]['p'] == false) {
            $this->result[$rows_kw[$row['id_kw']]['keyword']] = array(
              'p' => $row['position'], 'u' => $row['url'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'fe' => $row['description']
            );
          }

          $this->resultMulti[$rows_kw[$row['id_kw']]['keyword']][] = array(
            'p' => $row['position'], 'u' => $row['url'], 'ts' => $rows_kw[$row['id_kw']]['timestamp'], 'fe' => $row['description']
          );

        }
      }

    }

  }


  private function keywordstoScrape ()
  {

    $this->scrapekeywords = array();
    foreach ($this->searchkeywords as $keyword) {
      if (!isset($this->kw_in_db[$keyword])) {
        $this->scrapekeywords[] = array($keyword, $this->lang);
        // POLYFILL .. if no rankings
        $this->result[$keyword] = array();
        $this->resultMulti[$keyword] = array();
      }
    }

  }


  private function scrapeCurl ()
  {

    $payload = json_encode($this->scrapekeywords);
    $payload = base64_encode($payload);

    $postdata = http_build_query(array('data' => $payload));
    $opts     = array('http' =>
        array(
            'header'  => 'User-Agent:MyAgent/1.0\r\n',
            'method'  => 'POST',
            'timeout' => 360,
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    $context = stream_context_create($opts);


    if ($this->type == 'mobile') {
      $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/crawlinski/client-mobile.php', false, $context);
    } else {
      $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/crawlinski/client.php', false, $context);      
    }

    // JAPANSESE
    $replacedString = preg_replace("/\\\\u([0-9abcdef]{4})/", "&#x$1;", $output);
    $json = mb_convert_encoding($replacedString, 'UTF-8', 'HTML-ENTITIES');

    $json = json_decode($output);

    $this->scrapeResult = $json;

  }

  private function MOCKscrapeCurl()
  {

    $data = file_get_contents('https://oneproseo.advertising.de/oneproapi/mock.json');

    $json = json_decode($data);

    $this->scrapeResult = $json;

  }


  private function saveRankings ()
  {

    if (!is_object($this->scrapeResult) || empty ($this->scrapeResult)) {
      echo '<div class="box-content padded"><div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Ein Fehler ist aufgetreten.</strong><br /> Es kann vorkommen, dass die Liveranking Schnittstelle überlastet ist.<br />Probiere es bitte in ein paar Minuten nocheinmal.<br /><br /> Sollte der Fehler öfter auftreten informiere bitte Matthias Hotz.</div></div>';
      exit;
    }

    foreach ($this->scrapeResult as $k => $result) {

      // SQL QUERY FOR KEYWORD TABLE
      $serp_keyword = trim($result->keyword);
      $serp_keyword = $this->db->real_escape_string($serp_keyword);

      $lang = $result->lang;
      if ($this->type == 'mobile') {
        $lang = 'mobile_' . $result->lang;
      }

      // IS IN DB?
      $sql = "SELECT id, keyword FROM structure_scrape_keywords WHERE keyword = '$serp_keyword' AND language = '$lang'";
      $res = $this->db->query($sql);

      if ( $res->num_rows > 0 ) {
        continue;
      }

      $sql_kw = "('".$serp_keyword."', '".$lang."', '" . time() . "')";

      // add to structure_scrape_keywords table
      $query_kw = 'INSERT INTO
                      structure_scrape_keywords (keyword, language, timestamp)
                    VALUES
                      '. $sql_kw .' ';

      $x = $this->db->query($query_kw);

      $last_insert_id = $this->db->insert_id;

      // add to structure_scrape_rankings table
      $rank = 0;
      $sql_ra = array();

      foreach ($result->results as $k => $serp) {
        $rank++;
        $pure_host    = str_ireplace('www.', '', parse_url($serp->url, PHP_URL_HOST));
        $serp_url     = $this->db->real_escape_string($serp->url);
        $serp_feature = $this->db->real_escape_string($serp->feature);
        $pure_host    = $this->db->real_escape_string($pure_host);
        $sql_ra[]     = "('".$last_insert_id."', '".$rank."', '".$serp_url."', '".$pure_host."', '".$serp_feature."')";
      }

      $q_string_ra = implode(',', $sql_ra);

       // add to keyword ranking table
      $query_ra = 'INSERT INTO
                      structure_scrape_rankings (id_kw, position, url, hostname, description)
                    VALUES
                      '. $q_string_ra .' ';

      $this->db->query($query_ra);

    }

  }

  private function checkRankings ()
  {

    if (!is_object($this->scrapeResult)) {
      return;
    }

    foreach ($this->scrapeResult as $k => $result) {

      $rank = 0;
      foreach ($result->results as $k => $serp) {
        $rank++;
        $pure_host  = str_ireplace('www.', '', parse_url($serp->url, PHP_URL_HOST));

        if ($this->hostname == $pure_host) {
          $this->result[$result->keyword] = array('p' => $rank, 'u' => $serp->url, 'ts' => time(), 'fe' => $serp->feature);
          $this->resultMulti[$result->keyword][] = array('p' => $rank, 'u' => $serp->url, 'ts' => time(), 'fe' => $serp->feature);
          continue;
        }

      }

    }

  }


  private function deleteExpiredRankings ()
  {

    $seven_back  = strtotime('-1 week');

    $sql = "SELECT id, keyword FROM structure_scrape_keywords WHERE timestamp <= '$seven_back'";
    $results = $this->db->query($sql);

    if ($results->num_rows == 0) {
      return;
    }

    $rows_kw = array();

    while ($row = $results->fetch_assoc()) {
      $rows_kw[$row['id']] = $row['keyword'];
    }

    if (count($rows_kw) == 0) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql1 = "DELETE FROM structure_scrape_keywords WHERE id IN ($rows_kw_keys)";
    $result = $this->db->query($sql1);

    $sql2 = "DELETE FROM structure_scrape_rankings WHERE id_kw IN ($rows_kw_keys)";
    $result = $this->db->query($sql2);

  }

  public function removeSubdomain ($host)
  {

    $ex = explode('.', $host);
    $count_dots = substr_count($host, '.');

    if (stripos($host, '.co.uk') !== FALSE) {
      $si = array_slice ($ex, $count_dots - 2);
    } else {
      $si = array_slice ($ex, $count_dots - 1);
    }

    $host = implode ('.', $si);
    return $host;

  }

}

?>
