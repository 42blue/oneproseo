<?php

mb_internal_encoding('UTF-8');
date_default_timezone_set('CET');

class rasv {

  private $logdir       = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  private $api_endpoint = 'https://oneproseo.advertising.de/oneproapi/adwords/service/gSearchVolume.php';

  public function __construct () {

    $this->mySqlConnect();

    $this->fetchOpenJob();
    $this->fetchData();
    $this->createResult();
    $this->sendAlertMailHTML();

    $this->db->close();

  }

  private function fetchOpenJob () {

    $sql = "SELECT id FROM rankings_searchvolume_jobs WHERE status ='working'";
    $result = $this->db->query($sql);

    // ONLY ONE JOB AT A TIME
    if ($result->num_rows > 0) {
      echo 'LOCK ' . PHP_EOL;
      exit;
    }

    // FETCH ONE JOB
    $sql = "SELECT * FROM rankings_searchvolume_jobs WHERE status ='new' LIMIT 1";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      echo 'NO JOB ' . PHP_EOL;
      exit;
    }

    while ($row = $result->fetch_assoc()) {
      $this->jobid = $row['id'];
      $this->job_metas = $row;
    }

    // SET STATUS WORKING
    $sql = "UPDATE rankings_searchvolume_jobs SET status = 'working' WHERE id = '$this->jobid'";
    $this->db->query($sql);

  }

  private function fetchData() {

    $this->email         = $this->job_metas['email'];
    $this->keywords      = unserialize(($this->job_metas['keywords']));
    $this->type          = $this->job_metas['type'];
    $this->url           = $this->job_metas['url'];
    $this->rankings_type = $this->job_metas['rankings_type'];
    $this->lang          = $this->job_metas['lang'];

    $this->hostname = str_ireplace('www.', '', parse_url($this->url, PHP_URL_HOST));

    $this->keywords_result = array();
    foreach ($this->keywords as $keyword) {
      $this->keywords_result[$keyword] = array('sv' => -1, 'cpc' => -1, 'comp' => -1, 'rank' => -1, 'url' => 'N/A');
    }

    if ($this->type == 'sv') {

      $this->getSearchvolume();

    } else if ($this->type == 'ra') {

      $this->getRankings();
      $this->getRankings();

    } else {

      $this->getSearchvolume();
      $this->getRankings();
      $this->getRankings();

    }

  }

  private function getSearchvolume() {

    // kws in local db
    $local = $this->getAdwordsDataLocal($this->keywords);
    if (count($local) > 0) {
      foreach ($local as $keyword => $data) {
        $this->keywords_result[$keyword]['sv']   = $data[1];
        $this->keywords_result[$keyword]['cpc']  = $data[2];
        $this->keywords_result[$keyword]['comp'] = $data[3];
      }
    }

    // kws not in local db
    $payload = [];
    foreach ($this->keywords_result as $keyword => $value) {
      if ($value['sv'] == -1) {
        $payload[] = $keyword;
      }
    }    

    if (count($payload) > 0) {

      $payload_chunked = array_chunk($payload, 250);

      foreach ($payload_chunked as $key => $chunk) {

        $adwords = $this->getAdwordsDataLive($chunk);

        foreach ($adwords['set'] as $value) {

          $keyword = $value['kw'];

          $this->keywords_result[$keyword]['sv']   = $value['sv'];
          $this->keywords_result[$keyword]['cpc']  = $value['cpc'];
          $this->keywords_result[$keyword]['comp'] = $value['comp'];

        }

        sleep(7);

      }

    }

  }

  private function getRankings() {

    $local = $this->getRankingDataLocal();
    if (count($local) > 0) {
      foreach ($local as $keyword => $data) {
        if (!isset($data['p'])) {
          $data['p'] = 101;
          $data['u'] = 'N/A';
        }
        $this->keywords_result[$keyword]['rank'] = $data['p'];
        $this->keywords_result[$keyword]['url']  = $data['u'];
      }
    }

    $structure = $this->getRankingDataStructure();
    if (count($structure) > 0) {
      foreach ($structure as $keyword => $data) {
        if (!isset($data['p'])) {
          $data['p'] = 101;
          $data['u'] = 'N/A';
        }
        $this->keywords_result[$keyword]['rank'] = $data['p'];
        $this->keywords_result[$keyword]['url']  = $data['u'];
      }
    }

    // kws not in local db
    $payload = [];
    foreach ($this->keywords_result as $keyword => $value) {
      if ($value['rank'] == -1) {
				$payload[] = array($keyword, $this->lang);
      }
    }

    if (count($payload) > 0) {

      $payload_chunked = array_chunk($payload, 20);

      foreach ($payload_chunked as $key => $chunk) {
        
        $rankings = $this->getRankingDataLive($chunk);

        sleep(10);

        $this->saveRankings($rankings);

        foreach ($rankings as $key => $ranks) {

          $keyword = $ranks['keyword'];
          $p = 1;

          $this->keywords_result[$keyword]['rank'] = 101;
          $this->keywords_result[$keyword]['url']  = 'N/A';

          echo count($ranks['results']);
          echo PHP_EOL;

          $seen = false;

          foreach ($ranks['results'] as $rank) {

            $p++;
            $host = str_ireplace('www.', '', parse_url($rank['url'], PHP_URL_HOST));

            if ($host == $this->hostname && $seen == false) {
              $this->keywords_result[$keyword]['rank'] = $p;
              $this->keywords_result[$keyword]['url']  = $rank['url'];
              $seen = true;
              continue;
            }

          }

        }

      }

    }

  }


  private function getRankingDataLive ($payload) {

    $payload = json_encode($payload);
    $payload = base64_encode($payload);

    $postdata = http_build_query(array('data' => $payload));
    $opts     = array('http' =>
        array(
            'header'  => 'User-Agent:MyAgent/1.0\r\n',
            'method'  => 'POST',
            'timeout' => 360,
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context = stream_context_create($opts);

    if ($this->rankings_type == 'mobile') {
      $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/crawlinski/client-mobile.php', false, $context);
      //$output = file_get_contents('https://enterprise.oneproseo.com/cron/rankings_searchvolume/mock.json');
    } else {
      $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/crawlinski/client.php', false, $context);
      //$output = file_get_contents('https://enterprise.oneproseo.com/cron/rankings_searchvolume/mock.json');
    }

    // JAPANSESE
    $replacedString = preg_replace("/\\\\u([0-9abcdef]{4})/", "&#x$1;", $output);
    $json = mb_convert_encoding($replacedString, 'UTF-8', 'HTML-ENTITIES');

    $data = [];

    $data = json_decode($output, true);

    return $data;

  }


  private function getRankingDataLocal() {

    $ranking_result = array();

    $comma_separated_kws = implode('", "', $this->keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $lang = $this->lang;

    if ($this->rankings_type == 'mobile') {
      $lang = 'mobile_' . $this->lang;
    }

    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$lang'
            AND
              DATE(timestamp) = CURDATE()";

    $result = $this->db->query($sql);

    $rows_kw = array();

    if (!$result) {
      return;
    }

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
      $timestamp = strtotime($row['timestamp']);
      $ranking_result[$row['keyword']] = array();      
    }

    if (count($rows_kw) == 0) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    while ($row = $result2->fetch_assoc()) {

      if ($row['hostname'] == $this->hostname) {

        if (!isset($this->result[$rows_kw[$row['id_kw']]['keyword']]['p'])) {
          $ranking_result[$rows_kw[$row['id_kw']]['keyword']] = array('p' => $row['position'], 'u' => $row['url'], 'fe' => 0);
        }

      }

    }

    return $ranking_result;

  }




  private function getRankingDataStructure()
  {

    $ranking_result = array();

    $comma_separated_kws = implode('", "', $this->keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $lang = $this->lang;

    if ($this->rankings_type == 'mobile') {
      $lang = 'mobile_' . $this->lang;
    }

    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              structure_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$lang'";

    $result = $this->db->query($sql);

    $rows_kw = array();

    if (!$result) {
      return;
    }

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
      $timestamp = strtotime($row['timestamp']);
      $ranking_result[$row['keyword']] = array();      
    }

    if (count($rows_kw) == 0) {
      return;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              structure_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    while ($row = $result2->fetch_assoc()) {

      if ($row['hostname'] == $this->hostname) {

        if (!isset($ranking_result[$rows_kw[$row['id_kw']]['keyword']]['p'])) {
          $ranking_result[$rows_kw[$row['id_kw']]['keyword']] = array('p' => $row['position'], 'u' => $row['url'], 'fe' => 0);
        }

      }

    }

    return $ranking_result;


  }



  private function saveRankings ($rankings)
  {

    $this->reconMySql();

    foreach ($rankings as $k => $result) {

      // SQL QUERY FOR KEYWORD TABLE
      $serp_keyword = trim($result['keyword']);
      $serp_keyword = $this->db->real_escape_string($serp_keyword);

      $lang = $this->lang;
      if ($this->rankings_type == 'mobile') {
        $lang = 'mobile_' . $this->lang;
      }

      // IS IN DB?
      $sql = "SELECT id, keyword FROM structure_scrape_keywords WHERE keyword = '$serp_keyword' AND language = '$lang'";
      $res = $this->db->query($sql);

      if ( $res->num_rows > 0 ) {
        continue;
      }

      $sql_kw = "('".$serp_keyword."', '".$lang."', '" . time() . "')";

      // add to structure_scrape_keywords table
      $query_kw = 'INSERT INTO
                      structure_scrape_keywords (keyword, language, timestamp)
                    VALUES
                      '. $sql_kw .' ';

      $x = $this->db->query($query_kw);

      $last_insert_id = $this->db->insert_id;

      // add to structure_scrape_rankings table
      $rank = 0;
      $sql_ra = array();

      foreach ($result['results'] as $k => $serp) {
        $rank++;
        $pure_host    = str_ireplace('www.', '', parse_url($serp['url'], PHP_URL_HOST));
        $serp_url     = $this->db->real_escape_string($serp['url']);
        $serp_feature = $this->db->real_escape_string($serp['feature']);
        $pure_host    = $this->db->real_escape_string($pure_host);
        $sql_ra[]     = "('".$last_insert_id."', '".$rank."', '".$serp_url."', '".$pure_host."', '".$serp_feature."')";
      }

      $q_string_ra = implode(',', $sql_ra);

       // add to keyword ranking table
      $query_ra = 'INSERT INTO
                      structure_scrape_rankings (id_kw, position, url, hostname, description)
                    VALUES
                      '. $q_string_ra .' ';

      $this->db->query($query_ra);

    }

  }

  private function createResult() {

    $this->reconMySql();

    $id_job = $this->jobid;

    foreach($this->keywords_result as $keyword => $data) {
      $serp_url = $this->db->real_escape_string($data['url']);
      $values[] = "('" . $id_job ."', '" .$keyword ."', '".$data['sv']."', '".$data['cpc']."', '".$data['comp']."', '".$data['rank']."', '".$serp_url."')";
    }

    $sql = "INSERT INTO rankings_searchvolume_keywords (id_job, keyword, searchvolume, cpc, competition, rank, url) values ";
    $sql .= implode(',', $values);
    $this->db->query($sql);

    $sql = "UPDATE rankings_searchvolume_jobs SET status = 'done' WHERE id = '$this->jobid'";
    $this->db->query($sql);

  }

  public function getAdwordsDataLocal ($keywords) {

    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              keyword,
              searchvolume,
              cpc,
              competition
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              country = '$this->lang'
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $local_data = array();

    if ($res->num_rows < 1) {
      return $local_data;
    }

    while ($row = $res->fetch_assoc()) {
      $local_data[$row['keyword']] = array ($row['keyword'], $row['searchvolume'], $row['cpc'], $row['competition']);
    }

    return $local_data;

  }

  private function getAdwordsDataLive ($payload) {

    $payload = base64_encode(json_encode($payload));

    $opts = array('http' =>
      array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => 'keyword_arr=' . $payload . '&lang=' . $this->lang . '&country=' . $this->lang
      )
    );

    $context  = stream_context_create($opts);
    $result   = @file_get_contents($this->api_endpoint, false, $context);

    if (stripos($result, 'RATE_EXCEEDED') !== FALSE) {
      sleep(35);
      $result   = @file_get_contents($this->api_endpoint, false, $context);
    }

    $res = json_decode($result, true);

    return $res;

  }

  private function sendAlertMailHTML () {

    $emailcontent = '
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi '.$this->job_metas['created_by'].',<br /><br />
          <span style="font-size: 28px; font-color: #636363;">Dein Rankings / Suchvolumen Job <b>' . $this->job_metas['name'] . '</b> ist fertig!<br /></span>
          <br /><br /><br />

          <a href="https://enterprise.oneproseo.com/tools/rankings-searchvolume/'.$this->jobid.'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>

          <br /><br /></span>
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';

    $sendto   = $this->job_metas['email'];
    $subject  = 'OneProSeo | Rankings / Suchvolumen | ' . $this->job_metas['name'];
    $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" . 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;
    $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
    $message .= $emailcontent;
    $message .= '</body></html>';

    mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

  }

  public function mySqlConnect () {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFile($this->dateHIS() . ' MYSQLERROR SITESINDEX');
      exit;
    }

  }

  public function logToFile ($content) {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }

  public function reconMySql () {

    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

  }

}

new rasv;

?>
