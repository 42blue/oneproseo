<?php


include('/var/www/oneproapi/analytics_v3/api/GoogleAnalyticsAPI.class.php');

class google_analytics {

  public function __construct ($ga_account, $scheme)
  {

    $this->scheme = $scheme;
    $this->gaauth($ga_account);

  }

  public function getResult () {
  	
  	return $this->result_array;

  }

  private function gaauth ($ga_account)
  {

    $this->ga  = $this->auth();
    $auth      = $this->ga->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
    	return;
    }

    $this->ga->setAccessToken($accessToken);
    $this->ga_account_id = $ga_account;
    $this->ga->setAccountId($this->ga_account_id);
    $this->buildQuery();

  }


  private function buildQuery ($startindex = 1)
  {

    $startdate = $this->startDate30();
    $enddate   = $this->endDate30();

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate,
    );

    $this->ga->setDefaultQueryParams($this->selected_date);

    $params_all = array(
      'dimensions'  => 'ga:hostname,ga:landingPagePath',
      'metrics'     => 'ga:pageviews,ga:organicSearches,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:entrances',
      'filters'     => 'ga:pageviews!=0',
      'sort'        => 'ga:landingPagePath',
      'start-index' => $startindex,
      'max-results' => 10000
    );

    $visits = $this->ga->query($params_all);

    if (!isset($visits['rows'])) {
    	$this->result_array = array();
      return;
    }

    if (is_null($visits['rows'])) {
    	$this->result_array = array();
    } else {
      $this->processResult($visits);
    }

    if ($visits["totalResults"] > 10000) {

      $new_start_index = $startindex + 10000;

      if ($new_start_index < $visits["totalResults"]) {

        $this->buildQuery($new_start_index);

      }

    }

  }


  private function processResult ($visits)
  {

    $result_array = array();

    foreach ($visits['rows'] as $row) {

      foreach ($row as $cell => $val) {

        if ($cell == 0) {

          $host = $val;

        } elseif ($cell == 1) {

          if (stripos($val, 'www.') === 0 ) {
            $url = $this->scheme . $val;
          } else {
            $url = $this->scheme . $host . $val;
          }

          $result_array[$url] = array('url' => $url);

          // create proper path
          if (stripos($val, 'not set') !== false) {
            $path = $this->scheme . $host;
          } elseif (stripos($val, $host) === 0) {
            $path = $this->scheme . $val;
          } else {
            $path = $this->scheme . $host . $val;
          }

        // echo $host . '<br />' . $val . '<br />' . $path .  '<br /><br />';

          $result_array[$url]['path'] = $path;

        } elseif ($cell == 2) {
          if (isset($result_array[$url]['ga:pageviews'])) {
            $result_array[$url]['ga:pageviews'] = $result_array[$url]['ga:pageviews'] + $val;
          } else {
            $result_array[$url]['ga:pageviews'] = $val;
          }
        } elseif ($cell == 3) {
          if (isset($result_array[$url]['ga:organicSearches'])) {
            $result_array[$url]['ga:organicSearches'] = $result_array[$url]['ga:organicSearches'] + $val;
          } else {
            $result_array[$url]['ga:organicSearches'] = $val;
          }
        } elseif ($cell == 4) {
          if (isset($result_array[$url]['ga:avgTimeOnPage'])) {
            $result_array[$url]['ga:avgTimeOnPage'] = $result_array[$url]['ga:avgTimeOnPage'] + $val;
          } else {
            $result_array[$url]['ga:avgTimeOnPage'] = $val;
          }
        } elseif ($cell == 5) {
          if (isset($result_array[$url]['ga:avgPageLoadTime'])) {
            $result_array[$url]['ga:avgPageLoadTime'] = $result_array[$url]['ga:avgPageLoadTime'] + $val;
          } else {
            $result_array[$url]['ga:avgPageLoadTime'] = $val;
          }
        } elseif ($cell == 6) {
          if (isset($result_array[$url]['ga:entrances'])) {
            $result_array[$url]['ga:entrances'] = $result_array[$url]['ga:entrances'] + $val;
          } else {
            $result_array[$url]['ga:entrances'] = $val;
          }
        
        }

      }

    }

    $this->result_array = $result_array;

  }


  private function auth ()
  {

    $ga = new GoogleAnalyticsAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/analytics_v3/api/key.p12');

    return $ga;

  }


  public function startDate30 ()
  {
    return date("Y-m-d", strtotime('- 93 days'));
  }

  public function endDate30 ()
  {
    return date("Y-m-d", strtotime('- 3 days'));
  }

}

?>
