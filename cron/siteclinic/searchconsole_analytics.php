<?php

include('api/GoogleSearchConsoleAPI.class.php');
include('analytics.php');

class siteclinic_searchconsole {

  private $store = '/var/www/enterprise.oneproseo.com/temp/siteclinic'; 

  public function __construct () {

    $folders = $this->readDirectory($this->store);

    foreach ($folders as $key => $attribute) {

      $this->hoststore    = $this->store .'/'. $attribute['filename'];
      $this->projectstore = $this->store .'/'. $attribute['filename'] .'/'. $this->today();

      // SITECLINIC FOR TODAY?
      if (is_dir($this->projectstore)) {
        
        $dir  = $this->projectstore . '/meta.lock';
        $file = file_get_contents($dir);
        $meta = unserialize($file);

        $dir  = $this->hoststore . '/url.lock';
        $file = file_get_contents($dir);
        $url  = unserialize($file);

        if ($meta['searchconsole'] == 'ausstehend') {

          echo 'DOING ' . $attribute['filename'];
          echo PHP_EOL;

          if (isset($url['gaid']) && !empty($url['gaid'])) {
            $scheme = parse_url($url['url'], PHP_URL_SCHEME) . '://';
            $ga     = new google_analytics($url['gaid'], $scheme);
            $result = $ga->getResult();
            $this->writeGA($result);
          }

          $this->host = $attribute['filename'];
          $this->doQuery();
          $this->updateMeta($meta);

        }

      } 

    }

  }


  private function doQuery() {

    $this->sc      = $this->auth();
    $auth          = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'No auth token created!';
    }

    $this->sc->setAccessToken($accessToken);

    $profiles = $this->sc->getProfiles();

    $this->accounts = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }
      $this->accounts[] = $item['siteUrl'];
    }

		$start = $this->startDate90();
		$end   = $this->endDate90();
    $this->queryAPI('searchconsole_past90', $start, $end);

		$start = $this->startDate30();
		$end   = $this->endDate30();
    $this->queryAPI('searchconsole_past30', $start, $end);

  }


  private function queryAPI($name, $start, $end)
  {

    $urls = array();
    foreach ($this->accounts as $url) {
      if (stripos($url, $this->host) !== FALSE) {
        $urls[] = $url;
      }
    }
		
    if (count($urls) > 1) {

      // sort by strlen of URL
      array_multisort(array_map('strlen', $urls), $urls);  
      // reverse .. shortest at the end
      $urls = array_reverse($urls); 
      // use https if present
      foreach ($urls as $url) {
        if (stripos($url, 'https://') !== false) {
          $urls = array();
          $urls[] = $url;
        }
      }

    }


		// 50k pages
    $rows = array (0, 5001, 10002, 15003, 20004, 25005, 30006, 35007, 40008, 45009);

    foreach ($rows as $startRow) {
      $dataset[]  = $this->sc->getData($urls[0], $start, $end, $startRow);
    }

    foreach ($dataset as $key => $data) {
      
      if (!isset($data['rows'])) {continue;}
      
      foreach ($data['rows'] as $key => $urls) {

        $outputCSV = array();

        $url         = $urls['keys'][0];
        // $keyword     = $urls['keys'][1];
        $clicks      = $urls['clicks'];
        $impressions = $urls['impressions'];
        $ctr         = round($urls['ctr']*100, 2);
        $position    = round($urls['position'], 2);

        $outputCSV[] = array($url, $clicks, $impressions, $ctr, $position);

        $this->writeCsv($outputCSV, $name);

      }

    }

  }

  private function writeGA($result) {

    foreach ($result as $url => $data) {
      $outputCSV = array();
      $outputCSV[] = array($url, $data['ga:pageviews'], $data['ga:organicSearches'], $data['ga:avgTimeOnPage'], $data['ga:avgPageLoadTime'], $data['ga:entrances']);
      $this->writeCsv($outputCSV, 'analytcis_past30');
    }

  }


  private function updateMeta($meta) {

    $newmeta = array();

    $newmeta['status']        = '50%';
    $newmeta['crawl']         = $meta['crawl'];
    $newmeta['searchconsole'] = 'erledigt';
    $newmeta['user']          = $meta['user'];
    $newmeta['email']         = $meta['email'];
    $newmeta['start']         = $meta['start'];
    $newmeta['lastupdate']    = time();

    $data = serialize($newmeta);

    file_put_contents($this->projectstore . '/meta.lock', $data);

  }

  private function readDirectory($dir) {
    
    $fileinfo = array();
    
    $mDirectory = new RecursiveDirectoryIterator(realpath($dir));

    foreach ($mDirectory as $file) {

      if ($file->getFilename() != '.' && $file->getFilename() != '..' && $file->getFilename() != 'index.html') {

        $filetime = filemtime($file);

        $fileinfo[] = array(
           'filename' => $file->getFilename(),
           'filetime' => $filetime,
           'timediff' => time() - $filetime,
           'filesize' => filesize($file)
        );

      }

    }

    return $fileinfo;
  
  }


  private function writeCsv ($outputCSV, $name) {
    $fp = fopen($this->projectstore . '/' . $name . '.csv', 'a');
    foreach ($outputCSV as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }


  public function today ()
  {
    return date("Y-m-d", strtotime('today'));
  }

  public function startDate90 ()
  {
    return date("Y-m-d", strtotime('- 90 days'));
  }

  public function endDate90 ()
  {
    return date("Y-m-d", strtotime('- 60 days'));
  }

  public function startDate30 ()
  {
    return date("Y-m-d", strtotime('- 33 days'));
  }

  public function endDate30 ()
  {
    return date("Y-m-d", strtotime('- 3 days'));
  }


  private function auth ()
  {

    $ga = new GoogleSearchConsoleAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }

}

new siteclinic_searchconsole;

?>
