<?php

class crawler
{
	private $depth = 2;
	private $url;
	private $results = array();
	private $same_host = false;
	private $host;
	private $stack = array ();

	public function setDepth($depth) { $this->depth = $depth; }
	public function setHost($host) { $this->host = $host; }
	public function getResults() { return $this->results; }
	public function setSameHost($same_host) { $this->same_host = $same_host; }

	public function setUrl($url)
	{
		$this->url = $url;
		$this->setHost($this->getHostFromUrl($url));
	}

	public function __construct($url = null, $depth = null, $same_host = false, $store, $meta)
	{
		if (!empty($url)) {
			$this->setUrl($url);
		}
		if (isset($depth) && !is_null($depth)) {
			$this->setDepth($depth);
		}

		$this->setSameHost($same_host);

		$this->projectstore = $store;
		$this->meta         = $meta;

	}

	public function crawl()
	{

		if (empty($this->url)) {
			throw new \Exception('URL must be set');
		}

		$this->stack[$this->url] = $this->url;		
		
		$this->_crawl($this->url, $this->depth);
	
		// sort links alphabetically
		usort($this->results, function($a, $b) {
			if ($a['url'] == $b['url']) return 0;
			return (strtolower($a['url']) > strtolower($b['url'])) ? 1 : -1;
		});

		return $this->results;

	}


	private function _crawl($url, $depth)
	{
		static $seen = array();

		if (empty($url)) return;

		if (!$url = $this->buildUrl($this->url, $url)) {
			return;
		}

		if ($depth === 0 || isset($this->results[$url])) {
			return;
		}

		$html = $this->curl($url);

		$dom = new \DOMDocument('1.0');
		@$dom->loadHTML($html);

		$this->results[$url] = array(
			'url' => $url,
			'depth' => $depth,
			// 'content' => $dom->saveHTML()
		);

		foreach ($this->stack as $url => $val) {
			$st   = array();
			$st[] = array($url);
			if ($this->redirect == false) {
				$this->writeCsv($st);
			}
		}

		// saving links to find difference later
		$crawled = $seen;

		$anchors = $dom->getElementsByTagName('a');

		foreach ($anchors as $element)
		{
			
			if (!$href = $this->buildUrl($url, $element->getAttribute('href'))) {
				continue;
			}
			
			if (!in_array($href, $seen)) {
				$seen[] = $href;
			}
		}
		
		// set array difference from links already marked to crawl
		$crawl = array_diff($seen, $crawled);

		$this->logToFile($this->dateHI() . ' CRAWLED: ' . count ($this->results));
		$this->logToFile($this->dateHI() . ' SEEN: ' . count ($seen));		

		echo PHP_EOL;
		echo count ($this->results);
		echo PHP_EOL;
		echo 'SEEN: ' . count ($seen);
		echo PHP_EOL;	
		echo 'DEPTH: ' . $depth;
		echo PHP_EOL;	
		echo PHP_EOL;

		$this->stack = array_flip($crawl);

		if (count ($seen) > 50000) {
			$this->updateMeta($this->meta, '100%', 'erledigt');
			exit;
		}

		// check if there are links to crawl
		if (!empty($crawl)) {
			array_map(array($this, '_crawl'), $crawl, array_fill(0, count($crawl), $depth - 1));
		}

		return $url;

	}

	private function buildUrl($url, $href)
	{
		$url = trim($url);
		$href = trim($href);
		if (0 !== strpos($href, 'http'))
		{
			if (0 === strpos($href, 'javascript:') || 0 === strpos($href, '#'))
			{
				return false;
			}
			$path = '/' . ltrim($href, '/');
			if (extension_loaded('http'))
			{
				$new_href = http_build_url($url, array('path' => $path), HTTP_URL_REPLACE, $parts);
			}
			else
			{
				$parts = parse_url($url);
				$new_href = $this->buildUrlFromParts($parts);
				$new_href .= $path;
			}
			// Relative urls... (like ./viewforum.php)
			if (0 === strpos($href, './') && !empty($parts['path']))
			{
				// If the path isn't really a path (doesn't end with slash)...
				if (!preg_match('@/$@', $parts['path'])) {
					$path_parts = explode('/', $parts['path']);
					array_pop($path_parts);
					$parts['path'] = implode('/', $path_parts) . '/';
				}

				$new_href = $this->buildUrlFromParts($parts) . $parts['path'] . ltrim($href, './');
			}
			$href = $new_href;
		}
		if ($this->same_host && $this->host != $this->getHostFromUrl($href)) {
			return false;
		}
		if ($this->checkForShit($href) == false) {
			return false;
		}
		return $href;
	}

	private function buildUrlFromParts($parts)
	{
		$new_href = $parts['scheme'] . '://';
		if (isset($parts['user']) && isset($parts['pass'])) {
			$new_href .= $parts['user'] . ':' . $parts['pass'] . '@';
		}
		$new_href .= $parts['host'];
		if (isset($parts['port'])) {
			$new_href .= ':' . $parts['port'];
		}
		return $new_href;
	}

	private function getHostFromUrl($url)
	{
		$parts = parse_url($url);
		preg_match("@([^/.]+)\.([^.]{2,6}(?:\.[^.]{2,3})?)$@", $parts['host'], $host);
		return array_shift($host);
	}

  private function curl ($url) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, 'https://www.google.de/');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);

    $output = curl_exec($ch);
		
		$this->redirect = false;
		if (curl_getinfo($ch, CURLINFO_REDIRECT_COUNT) > 0) {
			$this->redirect = true;
		}


		if(curl_exec($ch) === false) {
			$this->logToFile($this->dateHI() . ' CURL Fehler: ' . curl_error($ch) . ' (' . $url . ') ');
			$this->updateMeta($this->meta, 'Crawl Error', 'Fehler');
		}

    curl_close($ch);

    return $output;
 
  }

  private function checkForShit ($foundUrl) {

    $firstShit = array(
        'ftp:', 'javascript:', 'skype:', 'mailto:', 'ymsgr:', 'callto:', 'file:', 'tel:', 'feed:', 'bitcoin:', 'server:', 'fax:'
    );

    $middleShit = array(
        '#', 'mailto:', 'tel:'
    );

    $lastShit = array(
        '.jpg', '.gif', '.png', '.svg', '.pdf', '.zip', '.rar', '.xml', '.css', '.doc', '.tar', '.wmv', '.wav', '.docx', '.JPG', '.GIF', '.PNG', '.SVG',
        '.swf', '.flv', '.mp4', '.mp3', '.rss', '.apk', '.rar', '.xls', '.exe', '.dmg', '.ppt', '.txt', '.crt', '.crl', '.gz', 'mailto:', 'tel:',
        '.m4v', '.mov'
    );

    if (strlen($foundUrl) > 3) {

      foreach ($firstShit as $check) {
        if (stripos($foundUrl, $check) === 0) {
          return false;
        }
      }

      foreach ($lastShit as $check) {
        if (strrpos($foundUrl, $check, -3) === strlen($foundUrl) -4) {
          return false;
        }
      }

    }

    foreach ($middleShit as $check) {
      if (stripos($foundUrl, $check) !== FALSE) {
        return false;
      }
    }

    return true;

  }

	private function writeCsv ($outputCSV) {
    $fp = fopen($this->projectstore . '/crawl.csv', 'a');
    foreach ($outputCSV as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }

  public function logToFile ($content) {
    $fh = fopen($this->projectstore . '/crawl.log', 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);
  }

	public function dateHI () {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i / d-m", $date);
  }

  private function updateMeta($meta, $status, $crawl) {
    $meta['status']        = $status;
    $meta['crawl']         = $crawl;
    $meta['lastupdate']    = time();
    $data = serialize($meta);
    file_put_contents($this->projectstore . '/meta.lock', $data);
  }

}

