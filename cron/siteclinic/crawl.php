<?php

include('/var/www/enterprise.oneproseo.com/cron/siteclinic/crawler/crawler.php');

class siteclinic_crawler {

  private $store = '/var/www/enterprise.oneproseo.com/temp/siteclinic'; 

  public function __construct () {

    $folders = $this->readDirectory($this->store);

    foreach ($folders as $key => $attribute) {

      $this->hoststore    = $this->store .'/'. $attribute['filename'];
      $this->projectstore = $this->store .'/'. $attribute['filename'] .'/'. $this->today();

      // SITECLINIC FOR TODAY?
      if (is_dir($this->projectstore)) {
        
        $dir         = $this->projectstore . '/meta.lock';
        $file        = file_get_contents($dir);
        $meta        = unserialize($file);
        $this->email = $meta['email'];

        $dir         = $this->hoststore . '/url.lock';
        $file        = file_get_contents($dir);
        $url         = unserialize($file);
        $this->host  = parse_url($url['url'], PHP_URL_HOST); 

        if ($meta['crawl'] == 'ausstehend') {

          $this->updateMeta($meta, '75%', 'in Arbeit');
          $this->startCrawl($url['url'], $meta);

        }

      } 

    }

  }

  private function startCrawl ($url, $meta) {

		try {

			$crawler = new crawler($url, 7, true, $this->projectstore, $meta);
      $crawler->crawl();
			$this->updateMeta($meta, '100%', 'erledigt');

		} catch (Exception $e) {

      $this->updateMeta($meta, 'Error', 'FEHLER');
			die($e->getMessage());

		}

    $this->sendAlertMailHTML();

  }


 private function updateMeta($meta, $status, $crawl) {

    $meta['status']        = $status;
    $meta['crawl']         = $crawl;
    $meta['lastupdate']    = time();

    $data = serialize($meta);

    file_put_contents($this->projectstore . '/meta.lock', $data);

  }

  private function readDirectory($dir) {
    
    $fileinfo = array();
    
    $mDirectory = new RecursiveDirectoryIterator(realpath($dir));

    foreach ($mDirectory as $file) {

      if ($file->getFilename() != '.' && $file->getFilename() != '..' && $file->getFilename() != 'index.html') {

        $filetime = filemtime($file);

        $fileinfo[] = array(
           'filename' => $file->getFilename(),
           'filetime' => $filetime,
           'timediff' => time() - $filetime,
           'filesize' => filesize($file)
        );

      }

    }

    return $fileinfo;
  
  }

  public function today () {
    return date("Y-m-d", strtotime('today'));
  }


  private function sendAlertMailHTML ()
  {

    $host    = $this->host;

    $sendto  = $this->email;
    $subject = 'OneProSeo / SiteClinic / '.$host.'';

    $header  = 'MIME-Version: 1.0' . "\r\n";
    $header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
    $header .= 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;

    $message = '<html><head><title>OneProSeo / SiteClinic / '.$host.'</title>';
    $message .= '<style>#btn:hover {background-color: #6ed032;}</style></head><body>';

    $message .= '
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
              <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
            </td>
          </tr>
          <tr>
            <td style="width:10px; background-color:#444444;"></td>
            <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
              <span style="font-size: 16px; font-color: #636363;">
              Hi,<br /><br />
              the <b>Siteclinic for '.$host.'</b> is ready.<br /><br /><br />
              cheers,<br />
              OneProSeo.com<br />
              Big Data SEO Analytics<br /><br /></span>
            </td>
            <td style="width:10px; background-color:#444444;"></td>
          </tr>
          <tr>
            <td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
          </tr>
        </table>';
    $message .= '</body></html>';

    mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

  }

}

new siteclinic_crawler;

?>


