<?php

/*

In den Kundenversionen haben die Kunden einen max. Anzahl von Liveranking Abfragen pro Tag
Hier werden diese jede Nacht auf 0 zurückgesetzt

*/

date_default_timezone_set('CET');

class liverankings_reset {

  public function __construct () {

    $this->mySqlConnect();

    $this->resetCounter();

    $this->db->close();

  }


  private function resetCounter ()
  {

    $sql = "UPDATE ruk_project_customers SET liveranking_count = 0 WHERE liveranking_count > 0";
    $result = $this->db->query($sql);

  }


  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }

}

new liverankings_reset;

?>
