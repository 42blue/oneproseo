<?php

mb_internal_encoding('UTF-8');
date_default_timezone_set('CET');

include('/var/www/oneproapi/searchconsole/api/GoogleSearchConsoleAPI.class.php');

class indexcheck {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/searchconsole/';

  public function __construct () {

    $this->mySqlConnect();

    $this->fetchOpenJob();

    $this->getSCData();

    $this->db->close();

  }

  private function fetchOpenJob ()
  {

    $sql = "SELECT id FROM search_console_jobs WHERE status ='working'";
    $result = $this->db->query($sql);

    // ONLY ONE JOB AT A TIME
    if ($result->num_rows > 0) {
      echo 'LOCK ' . PHP_EOL;
      exit;
    }

    // FETCH ONE JOB
    $sql = "SELECT * FROM search_console_jobs WHERE status ='new' LIMIT 1";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      echo 'NO JOB ' . PHP_EOL;
      exit;
    }

    while ($row = $result->fetch_assoc()) {
      $this->jobid = $row['id'];
      $this->job_metas = $row;
    }

    // SET STATUS WORKING
    $sql = "UPDATE search_console_jobs SET status = 'working' WHERE id = '$this->jobid'";
    $this->db->query($sql);

  }


  private function getSCData ()
  {

    $queryurl  = base64_decode($this->job_metas['url']);
    $purehost  = str_ireplace('www.', '', parse_url($queryurl, PHP_URL_HOST));
    $filename  = str_replace('.', '_', $purehost);
    $this->filename  = $filename .'_'. $this->job_metas['date1'] .'_'. $this->job_metas['date2'];

    $this->scAuth();

    $this->queryAPI();

    $this->reconMySql();

    // SET STATUS DONE
    $sql = "UPDATE search_console_jobs SET status = 'done', result = '$this->filename' WHERE id = '$this->jobid'";
    $this->db->query($sql);

    $this->sendAlertMailHTML();

  }


  private function queryAPI () {

    $queryurl  = base64_decode($this->job_metas['url']);

    $urls = array();

    foreach ($this->accounts as $url) {

      if (stripos($url, $queryurl) !== FALSE) {
        $urls[] = $url;
      }

    }

    $this->loop_url   	= $urls[0];
		$this->loop_start 	= $this->job_metas['date1'];
		$this->loop_end   	= $this->job_metas['date2'];
		$this->loop_dataset = array();

		$this->queryLoop(1);

    $outputCSV = array();
    $outputCSV[] = array('Keyword', 'URL', 'Clicks', 'Impressions', 'CTR', 'Average Position');

    foreach ($this->loop_dataset as $key => $data) {

      echo 'DO DS' . PHP_EOL;

      foreach ($data['rows'] as $key => $urls) {

        $keyword     = $urls['keys'][0];
        $url         = $urls['keys'][1];        
        $clicks      = $urls['clicks'];
        $impressions = $urls['impressions'];
        $ctr         = round($urls['ctr']*100, 2);
        $position    = round($urls['position'], 2);

        $outputCSV[] = array($keyword, $url, $clicks, $impressions, $ctr, $position);  

      }

    }

    $this->writeCsv($outputCSV, $this->filename);

  }


  private function queryLoop ($startRow) {

    $dataset = $this->sc->getDataOneProSeo($this->loop_url, $this->loop_start, $this->loop_end, 5000, $startRow);

    $this->loop_dataset[] = $dataset;

    echo 'ROUND';
    echo PHP_EOL;

    if (count($dataset['rows']) < 5000) {
    	return;
    }

    $newRow = $startRow + 5000;

		$this->queryLoop($newRow);

  }


  public function writeCsv ($outputCSV, $file) {
    $fp = fopen($this->cache_dir . $file . '.csv', 'a');
    foreach ($outputCSV as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }


  private function sendAlertMailHTML ()
  {

    $emailcontent = '
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi '.$this->job_metas['created_by'].',<br /><br />
          <span style="font-size: 28px; font-color: #636363;">Der Search Console Export <b>' . $this->job_metas['name'] . '</b> ist fertig!<br /></span>
          <br /><br /><br />
          <a href="https://enterprise.oneproseo.com/temp/searchconsole/'.$this->filename.'.csv" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Download CSV</a>
        <br /><br /></span>
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';

    $sendto   = $this->job_metas['email'];
    $subject  = 'OneProSeo | Search Console Export | ' . $this->job_metas['name'];
    $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" . 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;
    $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
    $message .= $emailcontent;
    $message .= '</body></html>';

    mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

  }


  public function auth ()
  {

    $ga = new GoogleSearchConsoleAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');

    return $ga;

  }


  private function scAuth () {


    $this->sc      = $this->auth();
    $auth          = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'No auth token created!';
    }

    $this->sc->setAccessToken($accessToken);
    $profiles = $this->sc->getProfiles();

    $this->accounts = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }
      $this->accounts[] = $item['siteUrl'];
    }

  }


  public function mySqlConnect ()
  {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      exit;
    }

  }


  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }


}

new indexcheck;

?>
