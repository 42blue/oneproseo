<?php
/**
* Main configuration file
*
* @author PowerWorx GmbH
* @copyright (C) PowerWorx GmbH 2012-2013
* @package OnePro
* @version 1.38
*/

// set error reporting to 0 per default
error_reporting(0);
ini_set('display_errors', 0);

// get system hostname
if(!defined('ONEPRO_PHP_VERSION')) define("ONEPRO_PHP_VERSION", phpversion());
if (version_compare(ONEPRO_PHP_VERSION, '5.3.0', '>=')) {
	if(!defined('ONEPRO_SYSTEM_HOSTNAME')) define('ONEPRO_SYSTEM_HOSTNAME', gethostname());
} else {
	if(!defined('ONEPRO_SYSTEM_HOSTNAME')) define('ONEPRO_SYSTEM_HOSTNAME', php_uname('n'));
}

if(isset($_SERVER['HTTP_X_FORWARDED_HOST']) && $_SERVER['HTTP_X_FORWARDED_HOST'] != "")
{
	if(!defined('ONEPRO_REQUEST_HOST')) define('ONEPRO_REQUEST_HOST', $_SERVER['HTTP_X_FORWARDED_HOST']);
}
else if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] != "")
{
	if(!defined('ONEPRO_REQUEST_HOST')) define('ONEPRO_REQUEST_HOST', $_SERVER['HTTP_HOST']);
}

if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != "")
{
	if(!defined('ONEPRO_REMOTE_ADDR')) define('ONEPRO_REMOTE_ADDR', $_SERVER['HTTP_X_FORWARDED_FOR']);
	if(!defined('ONEPRO_PROXY_ADDR')) define('ONEPRO_PROXY_ADDR', $_SERVER['REMOTE_ADDR']);
}
else if(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != "")
{
	if(!defined('ONEPRO_REMOTE_ADDR')) define('ONEPRO_REMOTE_ADDR', $_SERVER['REMOTE_ADDR']);
	if(!defined('ONEPRO_PROXY_ADDR')) define('ONEPRO_PROXY_ADDR', "");
}

if(!defined('ONEPRO_SERVER_NAME')) define('ONEPRO_SERVER_NAME', $_SERVER['SERVER_NAME']);

// define standard constants
if(!defined('ONEPRO_CHARSET')) define('ONEPRO_CHARSET', "utf-8");
if(!defined('ONEPRO_PROTOCOL')) define('ONEPRO_PROTOCOL', "http://");
if(!defined('ONEPRO_HEADER_MOVED_PERMANENTLY')) define('ONEPRO_HEADER_MOVED_PERMANENTLY', "HTTP/1.1 301 Moved Permanently");
if(!defined('ONEPRO_HEADER_MOVED_TEMPORARILY')) define('ONEPRO_HEADER_MOVED_TEMPORARILY', "HTTP/1.1 302 Moved Temporarily");
if(!defined('ONEPRO_HEADER_TEMPORARILY_REDIRECT')) define('ONEPRO_HEADER_TEMPORARILY_REDIRECT', "HTTP/1.1 307 Temporary Redirect");
if(!defined('ONEPRO_HEADER_NOT_FOUND')) define('ONEPRO_HEADER_NOT_FOUND', "HTTP/1.0 404 Not Found");
if(!defined('ONEPRO_HEADER_GONE')) define('ONEPRO_HEADER_GONE', "HTTP/1.1 410 Gone");

if(is_link("/var/www") && readlink("/var/www") == dirname(__FILE__)) {
	if(!defined('ONEPRO_BASE_PATH')) define('ONEPRO_BASE_PATH', '/var/www/');
} else {
	if(!defined('ONEPRO_BASE_PATH')) define('ONEPRO_BASE_PATH', dirname(__FILE__).'/');
}

if(!defined('ONEPRO_INCLUDES_PATH')) define('ONEPRO_INCLUDES_PATH', ONEPRO_BASE_PATH.'onepro_lib/');
if(!defined('ONEPRO_CUSTOM_INCLUDES_PATH')) define('ONEPRO_CUSTOM_INCLUDES_PATH', ONEPRO_BASE_PATH.'onepro_custom/');
if(!defined('ONEPRO_CACHE_PATH')) define('ONEPRO_CACHE_PATH', ONEPRO_INCLUDES_PATH.'cache/');

// load generic functions
require_once ONEPRO_INCLUDES_PATH . 'OnePro_Functions.php';
onepro_requireCustomFilename("OnePro_Functions_Custom.php");

if(isset($_REQUEST['debug']) && $_REQUEST['debug'] == "1" && ($_SERVER['REMOTE_ADDR'] == "217.110.209.229" || $_SERVER['HTTP_X_FORWARDED_FOR'] == "217.110.209.229"))
{
	if(!defined('ONEPRO_DEBUG_MODE'))
	{
		define('ONEPRO_DEBUG_MODE', true);
		onepro_setDebugMode();
		require_once ONEPRO_INCLUDES_PATH . 'OnePro_CustomErrorHandler.php';
	}
}
else
{
	if(!defined('ONEPRO_DEBUG_MODE')) define('ONEPRO_DEBUG_MODE', false);
}


// define class autoload function
if(!function_exists('onepro_autoload'))
{
	function onepro_autoload($class_name)
	{
		onepro_requireFilename($class_name . '.class.php');
	}
	spl_autoload_register('onepro_autoload');
}

// get config instance
$config = OnePro_Config::getInstance();

// dynamic enviroment setting
$config->setEnvironment("main");
$config->loadIni(ONEPRO_BASE_PATH . '../config.php');
$aEnvironments = onepro_getConfigParameterByName("environments");
if($aEnvironments !== false)
{
	if(is_array($aEnvironments))
	{
		foreach($aEnvironments as $sEnvironment)
		{
			$sEnvironmentMatch = onepro_getConfigParameterByName("environment_match_" . $sEnvironment);
			if($sEnvironmentMatch !== false && $sEnvironmentMatch !== "")
			{
				if(preg_match("/".$sEnvironmentMatch."/", ONEPRO_REQUEST_HOST) || preg_match("/".$sEnvironmentMatch."/", ONEPRO_SYSTEM_HOSTNAME)) {
					$config->setEnvironment($sEnvironment);
					break;
				}
			}
		}
	}
	else
	{
		$sEnvironment = $aEnvironments;
		$sEnvironmentMatch = onepro_getConfigParameterByName("environment_match_" . $sEnvironment);
		if($sEnvironmentMatch !== false && $sEnvironmentMatch !== "")
		{
			if(preg_match("/".$sEnvironmentMatch."/", ONEPRO_REQUEST_HOST) || preg_match("/".$sEnvironmentMatch."/", ONEPRO_SYSTEM_HOSTNAME)) {
				$config->setEnvironment($sEnvironment);
			}
		}
	}
}

OnePro_DB::$host = $config->get("db_host"); // set database hostname
OnePro_DB::$encoding = $config->get("db_encoding"); // set database connection encoding
OnePro_DB::$error_handler = array('OnePro_DB_Handler', 'static_error_handler');
OnePro_DB::$nonsql_error_handler = array('OnePro_DB_Handler', 'static_nonsql_error_handler');
OnePro_DB::$success_handler = array('OnePro_DB_Handler', 'static_success_handler');

?>
