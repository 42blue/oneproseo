<?php

\Slim\Slim::registerAutoloader();

session_start();

$slim = new \Slim\Slim(array(
  'templates.path' => 'oneproseo/templates/'
));

// LOAD ONEPRO CONFIG and MAP TO SLIM
$config = OnePro_Config::getInstance();
$env    = $slim->environment();

// SET DATABASE CREDENTIALS
onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");

//Disable debugging
$slim->config('debug', false);

// INCLUDES
require_once $config->get('path') . 'oneproseo/includes/db.class.php';
require_once $config->get('path') . 'oneproseo/includes/cache.class.php';
require_once $config->get('path') . 'oneproseo/includes/menu.class.php';
require_once $config->get('path') . 'oneproseo/includes/metrics_customer.class.php';

$ops_menu = new ops_menu;

// set configs for SLIM TEMPLATES
$slim->view->setData('domain', $config->get('domain'));
$slim->view->setData('path', $config->get('path'));
$slim->view->setData('csvstore', $config->get('csvstore'));
$slim->view->setData('csvurl', $config->get('csvurl'));
$slim->view->setData('googledrive', $config->get('googledrive'));
$slim->view->setData('tempcache', $config->get('tempcache'));

$slim->view->setData('ops_menu', $ops_menu->getMenu());
$slim->view->setData('path_info', $env['PATH_INFO']);

$slim->view->setData('customer_version', $config->get('customer_version'));
$slim->view->setData('customer_name', $config->get('customer_name'));
$slim->view->setData('customer_url', $config->get('customer_url'));
$slim->view->setData('customer_hostname', $config->get('customer_hostname'));
$slim->view->setData('customer_id', $config->get('customer_id'));
$slim->view->setData('customer_keywordset_id', $config->get('customer_keywordset_id'));
$slim->view->setData('customer_suffix', $config->get('customer_suffix'));
$slim->view->setData('customer_username', $config->get('customer_username'));
$slim->view->setData('customer_password', $config->get('customer_password'));
$slim->view->setData('customer_ga_account', $config->get('customer_ga_account'));
$slim->view->setData('customer_available', $config->get('customer_available'));

$slim->view->setData('modules_ranker', $config->get('modules_ranker'));
$slim->view->setData('modules_seocrawler', $config->get('modules_seocrawler'));
//$slim->view->setData('modules_crawler', $config->get('modules_crawler'));
$slim->view->setData('modules_keyworder', $config->get('modules_keyworder'));
$slim->view->setData('modules_structure', $config->get('modules_structure'));
$slim->view->setData('modules_botti', $config->get('modules_botti'));
$slim->view->setData('modules_sea', $config->get('modules_sea'));

$slim->view->setData('ruk_max_keywords', $config->get('ruk_max_keywords'));
$slim->view->setData('ruk_max_keywords_set', $config->get('ruk_max_keywords_set'));
$slim->view->setData('ruk_max_keywordsets', $config->get('ruk_max_keywordsets'));

$slim->view->setData('mysql_dbhost', $config->get('mysql_dbhost'));
$slim->view->setData('mysql_dbname', $config->get('mysql_dbname'));
$slim->view->setData('mysql_dbuser', $config->get('mysql_dbuser'));
$slim->view->setData('mysql_dbpass', $config->get('mysql_dbpass'));

$slim->view->setData('mongo_dbhost', $config->get('mongo_dbhost'));
$slim->view->setData('mongo_dbname', $config->get('mongo_dbname'));
$slim->view->setData('mongo_dbuser', $config->get('mongo_dbuser'));
$slim->view->setData('mongo_dbpass', $config->get('mongo_dbpass'));

$metrics = new metrics ($slim->view->getData());
$slim->view->setData('core_data_metrics', $metrics->getData());

// TEMP LOGIN
$authenticate = function ($login) {
  return function () use ($login) {
    // get config instance
    $config = OnePro_Config::getInstance();
    if ($login == FALSE) {
      $app = \Slim\Slim::getInstance();
      $env = $app->environment();
      $_SESSION['login_request_uri'] = $env['PATH_INFO'];
      $app->redirect($config->get('domain') . 'login', 301);
    }

    // allow multiple ajax requests (SESSION)
    session_write_close();
  };
};

// TEMP LOGIN
$authenticate = function ($login) {
  return function () use ($login) {
    // get config instance
    $config = OnePro_Config::getInstance();
    if ($login == FALSE) {
      $app = \Slim\Slim::getInstance();
      $env = $app->environment();
      $_SESSION['login_request_uri'] = $env['PATH_INFO'];
      $app->redirect($config->get('domain') . 'login', 301);
    }

    // allow multiple ajax requests (SESSION)
    session_write_close();
  };
};

// check if customerversion is allowed
function customerpermission ($id, $allowed) {
  $allowed = explode(',', $allowed);
  if (in_array($id, $allowed)) {
    return $id;
  }
  return $allowed[0];
};

// .$config->get('customer_id')
// HOME
$slim->get('/', $authenticate($_SESSION['login']), function () use ($slim, $config) {
  $slim->redirect($config->get('domain').'rankings/dashboard');
});

// LOGIN
$slim->map('/login', function () use ($slim, $config) {
  $slim->render('gen-header-login.php', array('titletag' => $config->get('customer_name') . ' OneProSeo - Enterprise', 'javascript' => 'loader-login.js'));
  $slim->render('gen-login.php');
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

$slim->map('/logout', function () use ($slim, $config) {
  $slim->render('gen-header-login.php', array('titletag' => $config->get('customer_name') . ' OneProSeo - Enterprise', 'javascript' => 'loader-login.js'));
  $slim->render('gen-logout.php');
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

$slim->map('/password', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Passwort ändern / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-gen-password.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-password.php', array('env' => $env));
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

// SEO Crawler
$slim->get('/seocrawler/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Dashboard / SEO Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
  $slim->render('seocrawler-dashboard.php');
  $slim->render('gen-footer.php');
});

// CRAWLER ROUTES
//$slim->get('/crawler/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
//  $slim->render('gen-header-ca.php', array('titletag' => 'Dashboard / Crawler / OneProSeo'));
//  $slim->render('gen-header-gen.php', array('javascript' => 'loader-seocrawler-dashboard.js'));
//  $slim->render('gen-sidebar.php');
//  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
//  $slim->render('crawler-dashboard.php');
//  $slim->render('gen-footer.php');
//});

$slim->get('/crawler/task', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Crawlauftrag / Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Crawlauftrag'));
  $slim->render('crawler-task.php');
  $slim->render('gen-footer.php');
});

$slim->get('/crawler/analyse/:crawlid', $authenticate($_SESSION['login']), function ($crawlid) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Analyse / Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-analyse.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Analyse'));
  $slim->render('crawler-analyse.php', array('crawlid' => $crawlid));
  $slim->render('gen-footer.php');
});

$slim->get('/crawler/analyse/:crawlid/site/:siteid', $authenticate($_SESSION['login']), function ($crawlid, $siteid) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Analyse / Einzelseite / Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-analyse-site.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelseite'));
  $slim->render('crawler-analyse-site.php', array('crawlid' => $crawlid, 'siteid' => $siteid));
  $slim->render('gen-footer.php');
});

$slim->get('/crawler/analyse/:crawlid/task/:taskid', $authenticate($_SESSION['login']), function ($crawlid, $taskid) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Analyse / Einzelanalyse / Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-analyse-task.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelanalyse'));
  $slim->render('crawler-analyse-task.php', array('crawlid' => $crawlid, 'taskid' => $taskid));
  $slim->render('gen-footer.php');
});

// JSON DATATABLE
$slim->get('/crawler/analyse/:crawlid/tasks/:taskid', $authenticate($_SESSION['login']), function ($crawlid, $taskid) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Analyse / Einzelanalyse / Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-analyse-json.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelanalyse'));
  $slim->render('crawler-analyse-tasks.php', array('crawlid' => $crawlid, 'taskid' => $taskid));
  $slim->render('gen-footer.php');
});

$slim->get('/crawler/analyse/:crawlid/taskcheck/:taskid/:value', $authenticate($_SESSION['login']), function ($crawlid, $taskid, $value) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Analyse / Einzelanalyse / Crawler / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-crawler-analyse-taskcheck.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelanalyse'));
  $slim->render('crawler-analyse-taskcheck.php', array('crawlid' => $crawlid, 'taskid' => $taskid, 'value' => $value));
  $slim->render('gen-footer.php');
});

// RANKING ROUTES
$slim->get('/rankings/dashboard', $authenticate($_SESSION['login']), function () use ($slim, $config) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Dashboard / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Dashboard'));
  $slim->render('rankings-dashboard.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/dashboard-rankings', $authenticate($_SESSION['login']), function () use ($slim, $config) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Visibility Daily / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-dashboard-rankings.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Visibility Daily'));
  $slim->render('rankings-dashboard-rankings.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/dashboard-visibility', $authenticate($_SESSION['login']), function () use ($slim, $config) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Visibility Weekly / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-visibility.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Visibility Weekly'));
  $slim->render('rankings-dashboard-visibility.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/dashboard-custom', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Personal Dashboard / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-dashboard-custom.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Personal Dashboard'));
  $slim->render('rankings-dashboard-custom.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/live-ranking', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Live Rankings / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-live-rankings.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Live Rankings'));
  $slim->render('rankings-live-rankings.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/projects-change/:id', $authenticate($_SESSION['login']), function ($id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Neues Projekt / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-projects-change.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Projekt bearbeiten'));
  $slim->render('rankings-projects-change.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id', $authenticate($_SESSION['login']), function ($id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Projektübersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-overview.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Projektübersicht'));
  $slim->render('rankings-overview.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/mis/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $cf_home = 'gen-home-cf'.$id.'.php';
  $slim->render('gen-header-ca.php', array('titletag' => 'OneProSeo - Dashboard'));
  $slim->render('gen-header-cf.php');
  $slim->render('gen-header-gen-cf.php', array('id' => $id));
  $slim->render($cf_home);
  $slim->render('gen-footer-cf.php');
});

$slim->get('/rankings/overview/:id/detail-url/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'URL Übersicht (Campaign) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht (Campaign)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-onedex/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'URL Übersicht OneDex (Campaign) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-url-onedex.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht OneDex (Campaign)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-complete/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'URL Übersicht (Database) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-url-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht (Database)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-onedex-complete/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'URL Übersicht OneDex (Database) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-url-onedex-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht OneDex (Database)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-onedex-analytics-complete/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'URL Übersicht Analytics / OneDex (Database) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-url-onedex-analytics-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht Analytics / OneDex (Database)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-keyword/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keyword Übersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Übersicht'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-keyword-url/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keyword / URL Übersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-keyword-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword / URL Übersicht'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-keyword-url-duplicate/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Mehrfache Rankings Keyword / URL Übersicht / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-keyword-url-duplicates.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Mehrfache Rankings Keyword / URL Übersicht'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-personal-index/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));  
  $slim->render('gen-header-ca.php', array('titletag' => 'Personal Index / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-personal-index.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Personal Index'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});


$slim->get('/rankings/overview/detail-category/:category', $authenticate($_SESSION['login']), function ($category) use ($slim, $config) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Kategorie Keyword Übersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-category.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Kategorie Keyword Übersicht'));
  $slim->render('rankings-detail-category.php', array('category' => $category));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-compare/:set_id(/:startdate)', $authenticate($_SESSION['login']), function ($id, $set_id, $startdate = '01.01.2016') use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keyword Datumsvergleich / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-compare.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Datumsvergleich'));
  $slim->render('rankings-detail-compare.php', array('id' => $id, 'set_id' => $set_id, 'startdate' => $startdate));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-competition/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keyword Wettbewerbsvergleich / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-detail-competition.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Wettbewerbsvergleich'));
  $slim->render('rankings-detail-competition.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keywords bearbeiten / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-keywords.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywords bearbeiten'));
  $slim->render('rankings-keywords.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/tags/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
	$id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Campaign Tags bearbeiten / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-keywords-tags.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Campaign Tags bearbeiten'));
  $slim->render('rankings-keywords-tags.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywordsets', $authenticate($_SESSION['login']), function ($id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keywordsets bearbeiten / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-keywordsets.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordsets bearbeiten'));
  $slim->render('rankings-keywordsets.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords/results/:keyword/:timestamp/:country', $authenticate($_SESSION['login']), function ($id, $keyword, $timestamp, $country) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Top 100 / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-results.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Top 100'));
  $slim->render('rankings-results.php', array('id' => $id, 'keyword' => $keyword, 'timestamp' => $timestamp, 'country' => $country));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/overview/:id/keywords/details/:keyword/:country', $authenticate($_SESSION['login']), function ($id, $keyword, $country) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keyword Details / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-details-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'keyword' => $keyword, 'country' => $country));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/api', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'API / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-api.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'API'));
  $slim->render('rankings-api.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

// LOCAL RANKINGS ROUTES
$slim->get('/rankings/local/:id', $authenticate($_SESSION['login']), function ($id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Rankings / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Rankings'));
  $slim->render('rankings-local.php', array('id' => $id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/keywordsets', $authenticate($_SESSION['login']), function ($id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Rankings Campaigns / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-keywordsets.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Campaigns'));
  $slim->render('rankings-local-keywordsets.php', array('id' => $id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/keywords(/:set_id)', $authenticate($_SESSION['login']), function ($id, $set_id = 0) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Keywordsets bearbeiten / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-keywords.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordsets bearbeiten'));
  $slim->render('rankings-local-keywords.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/keywords-csv', $authenticate($_SESSION['login']), function ($id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Keywordsets bearbeiten (CSV) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-keywords-csv.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordsets (CSV) bearbeiten'));
  $slim->render('rankings-local-keywords.php', array('id' => $id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-keyword/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Keyword Übersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Keyword Übersicht'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local URL Übersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local URL Übersicht (DB) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-url-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht (DB)'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-onedex/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local URL Übersicht OneDex (Campaign) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-url-onedex.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht OneDex (Campaign)'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-onedex-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local URL Übersicht OneDex (DB) / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-url-onedex-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht OneDex (DB)'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-keyword-url/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Keyword / URL Übersicht / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-keyword-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Keyword / URL Übersicht'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-onedex-analytics-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local URL Übersicht Analytics / OneDex / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-url-onedex-analytics-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht Analytics / OneDex'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
// ONE KEYWORD / ONE LOCATION
$slim->get('/rankings/local/:id/keywords/details/:keyword/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $keyword, $country, $location, $type) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keyword Details / Local Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-details-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'keyword' => $keyword, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});
// ALL KEYWORDS / ONE LOCATION
$slim->get('/rankings/local/:id/set/keywords/:setid/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $setid, $country, $location, $type) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keywordset Details / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-details-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordset Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'setid' => $setid, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});
// ONE KEYWORD / ALL LOCATIONS
$slim->get('/rankings/local/:id/set/locations/:setid/:keyword/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $setid, $keyword, $country, $location, $type) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Keywordset Details / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-details-location.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordset Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'setid' => $setid, 'keyword' => $keyword, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/local/:id/keywords/results/:keyword/:timestamp/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $keyword, $timestamp, $country, $location, $type) use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Top 100 / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-results.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Top 100'));
  $slim->render('rankings-results.php', array('id' => $id, 'keyword' => $keyword, 'timestamp' => $timestamp, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/local/:id/detail-compare/:set_id(/:startdate)', $authenticate($_SESSION['login']), function ($id, $set_id, $startdate = '01.01.2016') use ($slim, $config) {
  $id = customerpermission($id, $config->get('customer_available'));
  $slim->render('gen-header-ca.php', array('titletag' => 'Local Keyword Datumsvergleich / Rankings / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-rankings-local-detail-compare.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Keyword Datumsvergleich'));
  $slim->render('rankings-local-detail-compare.php', array('id' => $id, 'set_id' => $set_id, 'startdate' => $startdate));
  $slim->render('gen-footer.php');
});

// KEYWORDS ROUTES
$slim->get('/keywords', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->redirect($config['domain'] . 'keywords/keyword-db');
});
$slim->get('/keywords/', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->redirect('../keywords/keyword-db');
});

$slim->get('/keywords/keyword-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'One Keyword DB | OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-keywords-keyword-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-keyword-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/url-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'One URL DB | OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-keywords-url-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-url-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/suggest-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'One Suggest DB | OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-keywords-suggest-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-suggest-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/w-fragen-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'One W-Fragen DB | OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-keywords-w-fragen-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-w-fragen-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/searchvolume', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Suchvolumen / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-keywords-searchvolume.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-searchvolume.php');
  $slim->render('gen-footer.php');
});

// SEA SEO BALANCER
$slim->get('/sea-seo-balancer/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'SEA / SEO Balancer / Übersicht / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-sea-seo-balancer-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer'));
  $slim->render('sea-seo-balancer-dashboard.php');
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/overview/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'SEA / SEO Balancer / ohne Brand / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-sea-seo-balancer-overview.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: ohne Brand'));
  $slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/brand/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'SEA / SEO Balancer / inkl. Brand / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-sea-seo-balancer-brand.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: ink. Brand'));
  $slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/sea-vs-seo/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'SEA / SEO Balancer / SEO Team / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-sea-seo-balancer-seaseo.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: SEO Team'));
  $slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/seo-vs-sea/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'SEA / SEO Balancer / SEO vs. SEA / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-sea-seo-balancer-seosea.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: SEA Team'));
  $slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});

$slim->get('/sea-landingpage-finder', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Landing Page Finder / SEA / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-sea-landingpage-finder.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA Landingpage Finder'));
  $slim->render('sea-landingpage-finder.php');
  $slim->render('gen-footer.php');
});


// STRUCTURE ROUTES
$slim->get('/structure/link-silo', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Link Silo Tool / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-structure-link-silo.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('structure-link-silo.php');
  $slim->render('gen-footer.php');
});

$slim->get('/structure/indexed-urls', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Indexcheck Google / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-structure-indexed-urls.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('structure-indexed-urls.php');
  $slim->render('gen-footer.php');
});

$slim->get('/structure/:jobid', $authenticate($_SESSION['login']), function ($jobid) use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Indexcheck Google / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-structure-indexed-urls-jobs.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('structure-indexed-urls-jobs.php', array('jobid' => $jobid));
  $slim->render('gen-footer.php');
});

// BOTTI ROUTES
$slim->get('/botti/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header-ca.php', array('titletag' => 'Botti / OneProSeo'));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-botti-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('botti-dashboard.php', array('headline' => 'Dashboard'));
  $slim->render('gen-footer.php');
});

$slim->get('/botti/project/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = " . $id);

  $slim->render('gen-header-ca.php', array('titletag' => 'OneProSeo / Botti / ' . $aProject['name']));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-botti-project.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('botti-project.php', array('id' => $id, 'headline' => 'Project Details'));
  $slim->render('gen-footer.php');
});

$slim->get('/botti/project/:id/bot/:bot', $authenticate($_SESSION['login']), function ($id, $bot) use ($slim) {
  $aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = " . $id);
  $aBotData = OnePro_DB::queryFirstRow("SELECT * FROM `botti_user_agents` WHERE `id` = %i", $bot);

  $slim->render('gen-header-ca.php', array('titletag' => 'OneProSeo / Botti / ' . $aProject['name'] . " / " . $aBotData['browser'] . '/' . $aBotData['version']));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-botti-bot.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('botti-bot.php', array('id' => $id, 'bot' => $bot, 'headline' => 'Bot Details'));
  $slim->render('gen-footer.php');
});

$slim->get('/botti/project/:id/bot/:bot/url/:url', $authenticate($_SESSION['login']), function ($id, $bot, $url) use ($slim) {
  $aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = " . $id);
  $aBotData = OnePro_DB::queryFirstRow("SELECT * FROM `botti_user_agents` WHERE `id` = %i", $bot);
  $sUrl = OnePro_DB::queryOneField("url", "SELECT CONCAT(IF(`https` = '1', 'https://', 'http://'), IF(`hostname_client` IS NULL, `hostname`, `hostname_client`), `request_path`) as url FROM `botti_" . $id . "_logs` WHERE `user_agent_id` = " . $bot . " AND `url_id` = " . $url . " LIMIT 1");

  $slim->render('gen-header-ca.php', array('titletag' => 'OneProSeo / Botti / ' . $aProject['name'] . " / " . $aBotData['browser'] . '/' . $aBotData['version'] . " / " . $sUrl));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-botti-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('botti-url.php', array('id' => $id, 'bot' => $bot, 'url' => $url, 'urlfull' => 'off', 'headline' => 'Url Details'));
  $slim->render('gen-footer.php');
});

$slim->get('/botti/project/:id/bot/:bot/urlfull/:url', $authenticate($_SESSION['login']), function ($id, $bot, $url) use ($slim) {
  $aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = " . $id);
  $aBotData = OnePro_DB::queryFirstRow("SELECT * FROM `botti_user_agents` WHERE `id` = %i", $bot);
  $sUrl = OnePro_DB::queryOneField("url", "SELECT CONCAT(IF(`https` = '1', 'https://', 'http://'), IF(`hostname_client` IS NULL, `hostname`, `hostname_client`), `request_path`, IF(`query_string` IS  NULL, '', `query_string`)) as url FROM `botti_" . $id . "_logs` WHERE `user_agent_id` = " . $bot . " AND `url_id_full` = " . $url . " LIMIT 1");

  $slim->render('gen-header-ca.php', array('titletag' => 'OneProSeo / Botti / ' . $aProject['name'] . " / " . $aBotData['browser'] . '/' . $aBotData['version'] . " / " . $sUrl));
  $slim->render('gen-header-gen.php', array('javascript' => 'loader-botti-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('botti-url.php', array('id' => $id, 'bot' => $bot, 'url' => $url, 'urlfull' => 'on', 'headline' => 'Url Details'));
  $slim->render('gen-footer.php');
});

$slim->get('/botti/export', function () use ($slim) {
  $slim->render('botti-export.php');
});

// GOOGLE DRIVE
$slim->get('/googledrive', function () use ($slim) {
  $slim->render('gen-googledrive.php');
});

$slim->map('/googledrive-csv', function () use ($slim) {
  $slim->render('gen-googledrive-csv.php');
})->via('GET', 'POST');

// API
$slim->get('/api/v1/:key/:type.json', function ($key, $type) use ($slim) {
  $slim->render('gen-api.php', array('key' => $key, 'type' => $type));
});

// AJAX LOADER AND HANDLER (NOT PROTECTED CAUSE FILENAME IS STORED IN SESSION -> SHOULD BE THE SAME AS IN RANKER)
$slim->map('/ajax/crawler/analyse/csv_request', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'csv_request', 'route' => 'crawler/analyse'));
})->via('GET', 'POST');

// AJAX LOADER AND HANDLER (PROTECTED WITH)
$slim->map('/ajax/:route+/:file', $authenticate($_SESSION['login']), function ($route, $file) use ($slim) {
  $slim->render('gen-ajax.php', array('file' => $file, 'route' => $route));
})->via('GET', 'POST');

$slim->run();

?>
