<?php
/**
* Database handler class
* 
* @author PowerWorx GmbH
* @copyright (C) PowerWorx GmbH 2012-2013
* @package OnePro
* @version 2.01
*/
class OnePro_DB_Handler 
{
	public static $debug = false;
	public static $status = false;
	
	public static function static_success_handler($params) 
	{
		if(OnePro_DB_Handler::$debug || (defined('ONEPRO_DEBUG_MODE') && ONEPRO_DEBUG_MODE === true)) {
			if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
				$sBreak = "\n";
			} else {
				//$sBreak = "<br>\n";
				$sBreak = "\n";
			}
			echo "<!--" . $sBreak;
			echo "Command: " . $params['query'] . $sBreak;
			echo "Time To Run It: " . $params['runtime'] . " (milliseconds)".$sBreak;
			echo "Affected rows: " . $params['affected'] . $sBreak;
			echo "-->" . $sBreak;
		}
		OnePro_DB_Handler::$status = true;
	}
	
	public static function static_error_handler($params) 
	{
		if(OnePro_DB_Handler::$debug || (defined('ONEPRO_DEBUG_MODE') && ONEPRO_DEBUG_MODE === true)) {
			if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
				$sBreak = "\n";
			} else {
				//$sBreak = "<br>\n";
				$sBreak = "\n";
			}
			echo "<!--" . $sBreak;
			echo "Error: " . $params['error'] . $sBreak;
			echo "Query: " . $params['query'] . $sBreak;
			echo "-->" . $sBreak;
		}
		OnePro_DB_Handler::$status = false;
	}
	
	public static function static_nonsql_error_handler($params) 
	{
		if(OnePro_DB_Handler::$debug || (defined('ONEPRO_DEBUG_MODE') && ONEPRO_DEBUG_MODE === true)) {
			if (php_sapi_name() == 'cli' && empty($_SERVER['REMOTE_ADDR'])) {
				$sBreak = "\n";
			} else {
				//$sBreak = "<br>\n";
				$sBreak = "\n";
			}
			echo "<!--" . $sBreak;
			echo "Error: " . $params['error'] . $sBreak;
			echo "-->" . $sBreak;
		}
		OnePro_DB_Handler::$status = false;
	}
}
?>