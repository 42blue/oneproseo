<?php
/**
* General functions
* 
* @author PowerWorx GmbH
* @copyright (C) PowerWorx GmbH 2012-2013
* @package OnePro
* @version 1.45
*/
function onepro_parse_ini_file_extended($filename) {
	$p_ini = parse_ini_file($filename, true);
	$config = array();
	foreach($p_ini as $namespace => $properties){
		list($name, $extends) = explode(':', $namespace);
		$name = trim($name);
		$extends = trim($extends);
		// create namespace if necessary
		if(!isset($config[$name])) $config[$name] = array();
		// inherit base namespace
		if(isset($p_ini[$extends])){
			foreach($p_ini[$extends] as $prop => $val)
			{
				$config[$name][$prop] = replaceIniMarkedChars($val);
			}
		}
		// overwrite / set current namespace values
		foreach($properties as $prop => $val)
		{
			$config[$name][$prop] = replaceIniMarkedChars($val);
		}
	}
	return $config;
}

function replaceIniMarkedChars($val)
{
	$val = str_replace("###EXCLAMATION_MARK###", "!", $val);
	$val = str_replace("###EQUAL_SIGN###", "=", $val);
	
	return $val;
}

function onepro_getConfigParameterByName($sName)
{
	$config = OnePro_Config::getInstance();
	$sParameterValue = $config->get($sName);
	if($sParameterValue !== false)
	{
		if(strpos($sParameterValue, "#") !== false)
		{
			return explode("#", $sParameterValue);
		}
		return $sParameterValue;
	}
	return false;
} // function

function onepro_setDebugMode()
{
	error_reporting(E_ALL ^ E_NOTICE);
	ini_set('display_errors', 1);
}

function onepro_debugDBQueries($iDebug = 0)
{
	if($iDebug == 1)
	{
		OnePro_DB_Handler::$debug = true;
	}
	else
	{
		OnePro_DB_Handler::$debug = false;
	}
} // function

function onepro_setDbCredentials($sUsername)
{
	$config = OnePro_Config::getInstance();
	$sDbUser = $config->get("db_user_".$sUsername);
	$sDbPass = $config->get("db_pass_".$sUsername);
	
	if($sDbUser !== false && $sDbPass !== false)
	{
		OnePro_DB::$user = $sDbUser; // set database username
		OnePro_DB::$password = $sDbPass; // set database password
		$config->set("runtime_db", "");
		return true;
	}
	return false;
}

function onepro_setDatabase($sDatabaseAlias, $bDatabaseAlias = true)
{	
	$config = OnePro_Config::getInstance();
	if($bDatabaseAlias)
	{
		$sDatabase = $config->get("db_".$sDatabaseAlias);
	}
	else
	{
		$sDatabase = $sDatabaseAlias;
	}
	
	if($sDatabase !== false)
	{
		if($sDatabase != $config->get("runtime_db"))
		{
			OnePro_DB::$dbName = $sDatabase;
			OnePro_DB::useDB($sDatabase);
			$config->set("runtime_db", $sDatabase);
		}
		
		return true;
	}
	return false;
}


function onepro_getOneProBasePath()
{
	if(defined('ONEPRO_BASE_PATH')) return ONEPRO_BASE_PATH;
	else return "/var/www";
}

function onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody)
{
	$sHeader = "MIME-VERSION: 1.0" . "\r\n";
	$sHeader .= "CONTENT-TYPE: TEXT/HTML; CHARSET=UTF-8" . "\r\n";
	$sHeader .= "FROM: " . $sMailSenderName . " <" . $sMailSenderMail . ">" . "\r\n";

	mail($sMailRecipient, $sMailSubject, $sMailBody, $sHeader);
}

function onepro_encodeURL($sURL)
{
	return rawurlencode($sURL);
}

function onepro_serializeAndCompressData($sData)
{
	return gzcompress(serialize($sData));
}

function onepro_unserializeAndUncompressData($sData)
{
	return unserialize(gzuncompress($sData));
}

function onepro_encodeString($sData)
{
	return base64_encode($sData);
}

function onepro_decodeString($sData)
{
	return base64_decode($sData);
}

function onepro_getMandant()
{
	if(defined('ONEPRO_CURRENT_MANDANT')) return ONEPRO_CURRENT_MANDANT;
	else if(onepro_getConfigParameterByName("mandant_default") !== false && onepro_getConfigParameterByName("mandant_default") !== "") return onepro_getConfigParameterByName("mandant_default");
	else return false;
}

function onepro_removeHttps($sUrl)
{
	$sUrl = str_replace("https://", "http://", $sUrl);
	return $sUrl;
} // function

function onepro_timeMeasurementInit($sTag = "", $fMicrotime = "", $bCacheToFile = false)
{
	$config = OnePro_Config::getInstance();
	if($sTag == "") $sTag = "time_measurement_default";
	else $sTag = "time_measurement_" . $sTag;
	
	if($fMicrotime == "") $fMicrotime = microtime(true);
	
	if($bCacheToFile)
	{
		$sFilename = ONEPRO_CACHE_PATH . $sTag . ".txt";
		onepro_writeFile($sFilename, $fMicrotime);
	}
	else
	{
		$config->set($sTag, $fMicrotime);
	}
}

function onepro_timeMeasurementGetTime($sTag = "", $bCacheToFile = false)
{
	$config = OnePro_Config::getInstance();
	if($sTag == "") $sTag = "time_measurement_default";
	else $sTag = "time_measurement_" . $sTag;
	
	if($bCacheToFile)
	{
		$sFilename = ONEPRO_CACHE_PATH . $sTag . ".txt";
		return microtime(true) - onepro_readFile($sFilename);
	}
	else
	{
		return microtime(true) - $config->get($sTag);
	}
}

function onepro_timeMeasurementInsert($sTag, $runtime)
{
	onepro_setDbCredentials("alltours");
	onepro_setDatabase("alltours_data");
	
	OnePro_DB::insert("time_measurement", array('tag' => $sTag, 'runtime' => $runtime, 'timestamp' => date("Y-m-d H:i:s")));
}

function onepro_readWriteCache($sFilename, $sUrl, $bProxy = false, $iCacheTime = 259200, $iCacheSize = 1024, $bRewriteCache = false)
{
	$iTimeDifference = @(time() - filemtime($sFilename));
	$iFileSize = @filesize($sFilename);
	if ($iTimeDifference >= $iCacheTime || $iFileSize < $iCacheSize || $bRewriteCache) 
	{
		$sTempFilename = $sFilename . "_tmp" . mt_rand(1000,999999);
		$sData = onepro_getContentWithCurl($sUrl, $bProxy);
		if($sData === false) return onepro_readFile($sFilename);
		$sData = onepro_replaceEnvPaths($sData);
		file_put_contents($sTempFilename, $sData, LOCK_EX);
		rename($sTempFilename, $sFilename);
		if(is_file($sTempFilename)) unlink($sTempFilename);
		return $sData;
	}
	else {
		return onepro_readFile($sFilename);
	}
}

function onepro_getCacheTime($sAlias)
{
	$config = OnePro_Config::getInstance();
	$iCacheTime = $config->get("cache_time_".$sAlias);
	if($iCacheTime !== false)
	{
		return $iCacheTime;
	}
	return false;
} // function

function onepro_getBaseFQDN($bDefaultValue = false, $bAddProtocol = true, $bRemoveTrailingSlash = false)
{
	$sFQDNAlias = "base";
	if($bDefaultValue) $sFQDNAlias = "default_" . $sFQDNAlias;
	
	return onepro_getFQDN($sFQDNAlias, $bAddProtocol, $bRemoveTrailingSlash);
	
} // function

function onepro_getFQDN($sFQDNAlias, $bAddProtocol = true, $bRemoveTrailingSlash = false)
{
	$config = OnePro_Config::getInstance();
	$sFQDN = $config->get("fqdn_".$sFQDNAlias);
	if($sFQDN !== false)
	{
		if($bRemoveTrailingSlash && substr($sFQDN, -1) == "/") $sFQDN = rtrim($sFQDN, "/");
		if($bAddProtocol) return ONEPRO_PROTOCOL . $sFQDN;
		else return $sFQDN;
	}
	return false;
} // function

function onepro_getDomain($bAddProtocol = false, $bAddTrailingSlash = false)
{
	$config = OnePro_Config::getInstance();
	$sDomain = $config->get("domain");
	if($sDomain !== false)
	{
		if($bAddTrailingSlash && strstr($sDomain, -1) != "/") $sDomain = $sDomain . "/";
		if($bAddProtocol) return ONEPRO_PROTOCOL . $sDomain;
		else return $sDomain;
	}
	return false;
} // function

function onepro_getPath($sPathAlias)
{
	$config = OnePro_Config::getInstance();
	$sPath = $config->get("path_".$sPathAlias);
	if($sPath !== false)
	{
		return $sPath;
	}
	return false;
} // function

function onepro_requireFilename($sFilename)
{
	$sPath = ONEPRO_INCLUDES_PATH . $sFilename;
	if(file_exists($sPath)){
		require_once $sPath;
		return true;
	}
	return false;
} // function

function onepro_requireCustomFilename($sFilename)
{
	$sPath = ONEPRO_CUSTOM_INCLUDES_PATH . $sFilename;
	if(file_exists($sPath)){
		require_once $sPath;
		return true;
	}
	return false;
} // function

function onepro_readFile($sFilename, $bReturnArray = false)
{
	if(file_exists($sFilename))
	{
		if($bReturnArray)
		{
			$aContent = file($sFilename);
			return $aContent;
		}
		else
		{
			$sContent = file_get_contents($sFilename);
			return $sContent;
		}
	}
	return false;
} // function

function onepro_writeFile($sFilename, $sData, $bAppend = false, $bLockEx = true)
{
	if($bAppend && $bLockEx) file_put_contents($sFilename, $sData, FILE_APPEND | LOCK_EX);
	else if($bAppend && !$bLockEx) file_put_contents($sFilename, $sData, FILE_APPEND);
	else if(!$bAppend && $bLockEx) file_put_contents($sFilename, $sData, LOCK_EX);
	else file_put_contents($sFilename, $sData);
} // function

function onepro_redirect($sRequestPath, $sRedirectType = 301, $sHostname = "", $bFullUrl = false)
{
	if($sHostname == "") $sHostname = onepro_getDomain();
	switch ($sRedirectType) {
		case 301: 
			$sHTTPHeader = ONEPRO_HEADER_MOVED_PERMANENTLY;
		break;
		case 302: 
			$sHTTPHeader = ONEPRO_HEADER_MOVED_TEMPORARILY;
		break;
		case 307: 
			$sHTTPHeader = ONEPRO_HEADER_TEMPORARILY_REDIRECT;
		break;
		default:
			$sHTTPHeader = ONEPRO_HEADER_MOVED_TEMPORARILY;
		break;
	}
	header($sHTTPHeader); 
	if($bFullUrl) {
		header('Location: ' . $sRequestPath);
	}
	else
	{
		if(substr($sRequestPath, 0, 1) == '/') $sSeparator = "";
		else $sSeparator = "/";
		header('Location: ' . ONEPRO_PROTOCOL . $sHostname . $sSeparator . $sRequestPath);
	}
	header("Connection: close");
	exit(0);
} // function


// TODO: create notfound_handler.php
function onepro_pageNotFound($sStatusCode = 404)
{
	$sHandler404 = onepro_getConfigParameterByName("path_handler_404");
	$sHandler410 = onepro_getConfigParameterByName("path_handler_410");
	switch ($sStatusCode) 
	{
		case 404: 
			if($sHandler404 === false || $sHandler404 === "" || !file_exists($sHandler404)) return false;
			include $sHandler404;
		break;
		case 410: 
			if($sHandler410 === false || $sHandler410 === "" || !file_exists($sHandler410)) return false;
			include $sHandler410;
		break;
		default:
			if($sHandler404 === false || $sHandler404 === "" || !file_exists($sHandler404)) return false;
			include $sHandler404;
		break;
	}
	header("Connection: close");
	exit(0);
} // function

// Be careful when using this function! For detailed information see https://developers.google.com/webmasters/control-crawl-index/docs/robots_meta_tag?hl=de
function onepro_setXRobotsTag($bAll = false, $bNone = false, $bNoindex = false, $bNofollow = false, $bNoodp = false, $bNoarchive = false, $bNosnippet = false, $bNotranslate = false, $bNoimageindex = false, $sUnavailable_after = "")
{
	$aXRobotsTag = array();
	if($bAll) $aXRobotsTag[] = "all";
	if($bNone && !$bAll) $aXRobotsTag[] = "none";
	if(!$bAll && !$bNone)
	{
		if($bNoindex) $aXRobotsTag[] = "noindex";
		else $aXRobotsTag[] = "index";
		if($bNofollow) $aXRobotsTag[] = "nofollow";
		else $aXRobotsTag[] = "follow";
		if($bNoodp) $aXRobotsTag[] = "noodp";
		if($bNoarchive) $aXRobotsTag[] = "noarchive";
		if($bNosnippet) $aXRobotsTag[] = "nosnippet";
		if($bNotranslate) $aXRobotsTag[] = "notranslate";
		if($bNoimageindex) $aXRobotsTag[] = "noimageindex";
		if($sUnavailable_after != "") $aXRobotsTag[] = "unavailable_after: " . $sUnavailable_after;
	}
	header("X-Robots-Tag: " . implode(", ", $aXRobotsTag), true);
}

function onepro_setCacheHeaders($sType = "img")
{
	$sMaxAge = onepro_getConfigParameterByName("caching_expires_" . $sType);
	header('Cache-Control: max-age=' . $sMaxAge);
}

function onepro_setImageHeaders($sType)
{
	switch($sType)
	{
		case "jpg":
		$sHeader = "Content-Type: image/jpeg";
		break;
		case "gif":
		$sHeader = "Content-Type: image/gif";
		break;
		case "png":
		$sHeader = "Content-Type: image/png";
		break;
		default:
		$sHeader = "";
		break;
	}
	if($sHeader != "") header($sHeader);
}

function onepro_getContentWithCurl($sURI, $bProxy = false, $sUserAgent = "", $sCookieFile = "", $iTimeout = 20, $bHeader = false)
{
	$curl_handler = curl_init();
	curl_setopt($curl_handler, CURLOPT_URL, $sURI);
	curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_handler, CURLOPT_AUTOREFERER, true);
	curl_setopt($curl_handler, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl_handler, CURLOPT_MAXREDIRS, 5);
	curl_setopt($curl_handler, CURLOPT_CONNECTTIMEOUT, $iTimeout);
	if($sUserAgent != "") curl_setopt($curl_handler, CURLOPT_USERAGENT, $sUserAgent);
	if($bHeader) curl_setopt($curl_handler, CURLOPT_HEADER, true);
	if($sCookieFile != "") 
	{
		curl_setopt($curl_handler, CURLOPT_COOKIESESSION, true);
		curl_setopt($curl_handler, CURLOPT_COOKIEJAR, $sCookieFile);
		curl_setopt($curl_handler, CURLOPT_COOKIEFILE, $sCookieFile);
	}
	if($bProxy && onepro_getConfigParameterByName("http_proxy") !== false && onepro_getConfigParameterByName("http_proxy") !== "") curl_setopt($curl_handler, CURLOPT_PROXY, onepro_getConfigParameterByName("http_proxy"));
	$curl_response = curl_exec($curl_handler);
	$curl_http_code = curl_getinfo($curl_handler, CURLINFO_HTTP_CODE);
	curl_close($curl_handler);
	if(($curl_http_code == 200 || $curl_http_code == 404) && $curl_response !== false) {
		return $curl_response;
	}
	return false;
} // function

function onepro_getRequestPathFromURI($sURI)
{
	$aUriParts = parse_url($sURI);
	if($aUriParts === false) return false;
	
	return $aUriParts['path'];
} // function

function onepro_getHostnameFromURI($sURI, $bAddProtocol = true)
{
	$aUriParts = parse_url($sURI);
	if($aUriParts === false) return false;
	
	if($bAddProtocol) return $aUriParts['scheme'] . "://" . $aUriParts['host'];
	return $aUriParts['host'];
} // function

function onepro_encodeStringForUrl($sString, $sLanguage = "")
{
	$oOnePro_Encode = new OnePro_Encode();
	$oOnePro_Encode->main();
	return $oOnePro_Encode->encode($sString, $sLanguage);
}

function onepro_cutText($sText, $iLength, $sDelimiter = ' ', $sSuffix = '...')
{
	if (strlen($sText) <= $iLength ) return $sText;
	else return substr($sText, 0, strpos($sText, $sDelimiter, $iLength)) . $sSuffix;
}

function onepro_switchDatabaseTable($sTableName, $sTmpSuffix = "tmp", $sOldSuffix = "old")
{
	$bSuccess = true;
	$aCount = array();
	$sRenameOldTable = "";
	
	$sMailSenderName = "One Admin";
	$sMailSenderMail = "admin@advertising.de";
	$sMailRecipient = "serveradmins@advertising.de";
	$sMailSubject = "Warning: switch mysql tables failed";
	$sMailBody = "Date: " . date('d.m.Y H:i:s') . "\n";
	$sMailBody .= "Server: " . ONEPRO_SYSTEM_HOSTNAME . "\n";
	$sMailBody .= "Tables: " . $sTableName . ", " . $sTmpTableName  . ", " . $sOldTableName . "\n\n";
	$sMailBody .= "PLEASE CHECK!\n";
	
	$sTmpTableName = $sTableName . "_" . $sTmpSuffix;
	$sOldTableName = $sTableName . "_" . $sOldSuffix;
	$sOldBackupTableName = $sTableName . "_" . $sOldSuffix . "_backup";
	
	if(is_null($sTableName) || empty($sTableName) || $sTableName == false)
	{
		onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody);
		return false;
	}
	
	$aCount['before'][$sTableName] = OnePro_DB::queryOneField("countrows", "SELECT count(*) as countrows FROM " . $sTableName); # count table rows and exit if empty or an error occured
	if(is_null($aCount['before'][$sTableName]) || OnePro_DB_Handler::$status === false)
	{
		onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody);
		return false;
	}
	
	$aCount['before'][$sTmpTableName] = OnePro_DB::queryOneField("countrows", "SELECT count(*) as countrows FROM " . $sTmpTableName); # count tmp table rows and exit if empty or an error occured
	if(is_null($aCount['before'][$sTmpTableName]) || OnePro_DB_Handler::$status === false)
	{
		onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody);
		return false;
	}
	
	OnePro_DB::query("SHOW TABLES LIKE '" . $sOldTableName . "'"); # check if an old table exists
	if(OnePro_DB::count() == 1 && OnePro_DB_Handler::$status !== false)
	{
		$aCount['before'][$sOldTableName] = OnePro_DB::queryOneField("countrows", "SELECT count(*) as countrows FROM " . $sOldTableName); # count old table rows
		$sRenameOldTable .= $sOldTableName . " TO " . $sOldBackupTableName . ", ";
	}
	
	OnePro_DB::query("SHOW TABLES LIKE '" . $sOldBackupTableName . "'"); # check if an old backup table already exists and delete it
	if(OnePro_DB::count() == 1 && OnePro_DB_Handler::$status !== false)
	{
		OnePro_DB::query("DROP TABLE " . $sOldBackupTableName);
		if(OnePro_DB_Handler::$status === false)
		{
			onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody);
			return false;
		}
	}
	
	OnePro_DB::query("RENAME TABLE " . 
						$sRenameOldTable .
						$sTableName . " TO " . $sOldTableName . ", " .
						$sTmpTableName . " TO " . $sTableName
	);
	if(OnePro_DB_Handler::$status === false)
	{
		$bSuccess = false;
	}
	else
	{
		$aCount['after'][$sTableName] = OnePro_DB::queryOneField("countrows", "SELECT count(*) as countrows FROM " . $sTableName); # count table rows and exit if empty or an error occured
		$aCount['after'][$sOldTableName] = OnePro_DB::queryOneField("countrows", "SELECT count(*) as countrows FROM " . $sOldTableName); # count old table rows
		
		if(is_null($aCount['after'][$sTableName]) || OnePro_DB_Handler::$status === false) $bSuccess = false;
		if(is_null($aCount['after'][$sOldTableName]) || OnePro_DB_Handler::$status === false) $bSuccess = false;
		
		if($aCount['after'][$sTableName] !== $aCount['before'][$sTmpTableName] || $aCount['after'][$sOldTableName] !== $aCount['before'][$sTableName]) $bSuccess = false;
		
		if($sRenameOldTable != "")
		{
			OnePro_DB::query("DROP TABLE " . $sOldBackupTableName);
			if(OnePro_DB_Handler::$status === false) $bSuccess = false;
		}
	}
	
	if(!$bSuccess)
	{
		onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody);
	}
	
	return $bSuccess;
}

function onepro_getDirName($sPaht, $iDepth = 0)
{
	for($d = 0; $d <= $iDepth; $d++)
	{
		$sPaht = dirname($sPaht);
	}
	
	return $sPaht;
}
?>