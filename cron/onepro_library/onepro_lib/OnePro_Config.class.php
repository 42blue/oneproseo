<?php
/**
* Main configuration class
* 
* @author PowerWorx GmbH
* @copyright (C) PowerWorx GmbH 2012-2013
* @package OnePro
* @version 1.11
*/
class OnePro_Config
{
	private $CONFIG = array();
	private $environment = NULL;
	private static $instance = NULL;

	/**
	* getInstance - creates a new Singleton instance
	*/
	static public function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	public function __construct() {}
	public function __destruct() {}
	public function __clone() {}

	/**
	* adds a key->value pair to the registry
	*/
	public function set($key, $value) {
		if(is_null($this->environment)) return false;
		$this->CONFIG[$this->environment][$key] = $value;
	}

	/**
	* retrieves a key->value pair from the registry
	*/
	public function get($key) {
		if(is_null($this->environment) || !isset($this->CONFIG[$this->environment][$key])) return false;
		return $this->CONFIG[$this->environment][$key];
	}

	public function loadIni($file) {
		$this->CONFIG = onepro_parse_ini_file_extended($file);
	}

	public function setEnvironment($env) {
		$this->environment = $env;
	}
	
	public function getEnvironment() {
		return $this->environment;
	}
} 
?>