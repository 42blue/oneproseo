<?php

mb_internal_encoding('UTF-8');
date_default_timezone_set('CET');

class indexcheck {

  private $logdir = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->fetchOpenJob();
    $this->scrapeData();

    $this->db->close();

  }

  private function fetchOpenJob ()
  {

    $sql = "SELECT id FROM structure_indexed_jobs WHERE status ='working'";
    $result = $this->db->query($sql);

    // ONLY ONE JOB AT A TIME
    if ($result->num_rows > 0) {
      echo 'LOCK ' . PHP_EOL;
      exit;
    }

    // FETCH ONE JOB
    $sql = "SELECT * FROM structure_indexed_jobs WHERE status = 'new' LIMIT 1";
    $result = $this->db->query($sql);

    if ($result->num_rows < 1) {
      echo 'NO JOB ' . PHP_EOL;
      exit;
    }

    while ($row = $result->fetch_assoc()) {
      $this->jobid = $row['id'];
      $this->job_metas = $row;
    }

    // SET STATUS WORKING
    $sql = "UPDATE structure_indexed_jobs SET status = 'working' WHERE id = '$this->jobid'";
    $this->db->query($sql);

  }


  private function scrapeData ()
  {


    $this->scrapeLoop();

    $this->reconMySql();

    $sql = "UPDATE structure_indexed_jobs SET status = 'done' WHERE id = '$this->jobid'";
    $this->db->query($sql);

    $this->sendAlertMailHTML();

  }


  private function scrapeLoop () {

    // FETCH URLS to SCRAPE
    $sql    = "SELECT id, url FROM structure_indexed_urls WHERE id_job = $this->jobid AND status = 0";
    $result = $this->db->query($sql);

    $this->scrape_urls = array();

    while ($row = $result->fetch_assoc()) {
      $this->scrape_urls[$row['id']] = $row['url'];
    }

    if (count($this->scrape_urls) < 1) {
      return;
    }

    $chunks = array_chunk($this->scrape_urls, 20, true);

    foreach ($chunks as $array) {

      $payload      = array();
      foreach ($array as $id => $url) {
        $searchstring = 'site:'.$url;
        $payload[]    = array($searchstring, 'de');
      }

      $results = $this->scrapeTP($payload);

      foreach ($results as $obj) {

        $query  = $obj->keyword;
        $query  = str_replace('site:', '', $query);
        $url    = 'false';
        $status = 2;

        if (!empty($obj->results)) {

          foreach ($obj->results as $value) {
            if ($value->url == $query) {
              $url    = $value->url;
              $status = 1;              
            }
          }

        }

        $this->reconMySql();

        // SAVE TO DB
        $sql = "UPDATE structure_indexed_urls SET status = $status, url_found = '$url' WHERE url = '$query'";
        $result = $this->db->query($sql);

      }

    }

    $this->scrapeLoop();

  }


  private function scrapeTP ($payload)
  {

    $payload   = json_encode($payload);
    $payload   = base64_encode($payload);
    $postdata  = http_build_query(array('data' => $payload));
    $opts      = array('http' =>
        array(
            'timeout' => 1500, 
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );

    $context = stream_context_create($opts);
    $output  = file_get_contents('https://oneproseo.advertising.de/oneproapi/crawlinski/client.php', false, $context);

    $json = json_decode($output);

    if (!empty(json_last_error())) {
      echo json_last_error(); 
      echo PHP_EOL;
    }

    return $json;

  }

  private function sendAlertMailHTML ()
  {

    $emailcontent = '
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi '.$this->job_metas['created_by'].',<br /><br />
          <span style="font-size: 28px; font-color: #636363;">Der Google Indexcheck <b>' . $this->job_metas['name'] . '</b> ist fertig!<br /></span>
          <br /><br /><br />';

        if ($this->job_metas['source'] == 'free' || $this->job_metas['source'] == 'paid') {
            $emailcontent .= '<a href="https://www.advertising.de/oneproseo/indexcheck/show.php?id='.$this->jobid.'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Hier geht es zur Analyse!</a>';
        } else if ($this->job_metas['source'] == 'enterprise') {
            $emailcontent .= '<a href="https://enterprise.oneproseo.com/structure/'.$this->jobid.'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>';
        } else {
          $cid = $this->job_metas['customer_id'];
          $sql = "SELECT subdomain FROM oneproseo_customer_versions WHERE ruk_id = $cid";
          $result = $this->db->query($sql);
          while ($row = $result->fetch_assoc()) {
            $subdomain = $row['subdomain'];
          }
          $emailcontent .= '<a href="https://'.$subdomain.'.oneproseo.com/structure/'.$this->jobid.'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>';
        }

    $emailcontent .= '<br /><br /></span>
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';

    $sendto   = $this->job_metas['email'];
    $subject  = 'OneProSeo | Indexcheck Google | ' . $this->job_metas['name'];
    $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" . 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;
    $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
    $message .= $emailcontent;
    $message .= '</body></html>';

    mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

  }

  public function mySqlConnect ()
  {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFile($this->dateHIS() . ' MYSQLERROR SITESINDEX');
      exit;
    }

  }

  public function logToFile ($content) {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }

  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }


}

new indexcheck;

?>
