<?php

/*

In google_webmastertools_import.php importieren wir SC Daten per Tag
Hier summieren wir alle Tage aus dem aktuellen Jahr auf und packen sie in eine AGGREGATION Table
Jeden Morgen nachdem der Analytics import durch ist

*/

date_default_timezone_set('CET');

class google_webmastertools_aggregator {

  private $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_searchconsole.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->logToFile($this->dateHI() . ' GWT/SC AGGR: START');
    $this->getWebmasterToolsData();
    $this->writeResults();
    $this->logToFile($this->dateHI() . ' GWT/SC AGGR: DONE');
    $this->db->close();

  }

  // FETCH SC DATA FROM gwt_data TABLE
  public function getWebmasterToolsData ()
  {

    $sql = "SELECT
              query,
              SUM(impressions) AS sum_imp,
              SUM(clicks) AS sum_clicks,
              ctr,
              position_1,
              position_2
            FROM
              gwt_data
            GROUP BY
              query";

    $res = $this->db->query($sql);

    $this->scdata = array();
    while ($row = $res->fetch_assoc()) {
      if ($row['sum_imp'] > 100 && $row['sum_clicks'] > 20 && $row['ctr'] >= 1) {
         $this->scdata[$row['query']] = $row;
      }
    }

  }


  public function writeResults ()
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }


    foreach ($this->scdata as $query => $data) {

      $query       = $query;
      $impressions = $data['sum_imp'];
      $clicks      = $data['sum_clicks'];
      $ctr         = $data['ctr'];
      $pos1        = $data['position_1'];
      $pos2        = $data['position_2'];

      $query = "REPLACE INTO gwt_aggregation_data SET query   = '$query',
                                                       impressions = $impressions,
                                                       clicks      = $clicks,
                                                       ctr         = $ctr,
                                                       position_1  = $pos1,
                                                       position_2  = $pos2
                                                      ";
      $this->db->query($query);

      if (!empty($this->db->error)) {
        var_dump($this->db->error);
      }

    }

  }

  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }

  public function dateHI ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("h:i / d-m", $date);

  }

  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }


}

new google_webmastertools_aggregator;

?>
