<?php

/*

Hier importieren wir SC Daten per Tag / per KEYWORD
In DB ruk_project_customers muss gwt_account manuell auf 1 gestellt werden.

Der Import läuft mit einem Versatz von 3 Tagen.
Diese Emailadresse muss in der Search Console freigeschaltet werden:

352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com

muss dem SC Account hinzugefügt werden aus dem wir Daten abrufen

Das geht unter -> PROPERTY VERWALTEN -> NEUEN NUTZER HINZUFÜGEN

nohup php searchconsole_import_new.php &>/dev/null &

*/

date_default_timezone_set('CET');

include('/var/www/oneproapi/searchconsole/api/GoogleSearchConsoleAPI.class.php');

class google_searchconsole_import {

  private $logdir  = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_searchconsole.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->timestamp = $this->dateYMD3();

    $this->scAuth();
  	$this->queryAPI();      

    $this->reconMySql();

  }


  private function scAuth() {

    $customers     = $this->getActiveCustomers();

    $this->sc      = $this->auth();
    
    $auth          = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE No auth token created!');
    }

    $this->sc->setAccessToken($accessToken);

    $profiles = $this->sc->getProfiles();

    $this->accounts = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }

      $this->accounts[] = $item['siteUrl'];
      
    }

    // REDUCE 
    $this->activeaccounts = array();

    foreach ($this->accounts as $url) {

      if (stripos($url, 'sc-domain:')!== FALSE) {
        $hostname = str_ireplace('sc-domain:', '', $url);
      } else {
        $hostname = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));        
      }

      if (isset($customers[$hostname]) || stripos($url, 'bmw') > 0 || stripos($url, 'mini') > 0) {
        $this->activeaccounts[$hostname] = $url;
      }

    }

  }


  private function queryAPI() {

    $this->maxrows   = 25000; 

    $searchtypes = array('web','image','video');

    $i = 0;

    foreach ($this->activeaccounts as $hostname => $url) {

    	$amountinDB = $this->checkIfDataIsImported($hostname, $this->timestamp);

      echo PHP_EOL;
    	echo '===================================================================================';
      echo PHP_EOL;
      echo $i++;
      echo ' / ' . count($this->activeaccounts) . ' - ';
      echo $this->timestamp;
      echo ' - ';
      echo $hostname;
      echo ' - ';
      echo $amountinDB;        
      echo PHP_EOL;

      if ($amountinDB < 1) {

				echo 'RUN -> '; 
        echo $this->timestamp;
        echo ' - ';
        echo $hostname;
        echo PHP_EOL;

        foreach ($searchtypes as $type) {

          $startindex  = 0;
          $this->loops = 0;

          $this->queryLoop($url, $hostname, $type, $startindex);

        }

    	}

    }
     

  }


  private function queryLoop($url, $hostname, $type, $startindex) {

      $this->loops++;

      $dataset  = $this->sc->getDataOneProSeoBatch($url, $this->timestamp, $this->timestamp, $this->maxrows, $startindex, $type);
      
      if (!isset($dataset['rows']) || count($dataset['rows']) < 1) {
				
				echo PHP_EOL;
				echo 'NSC - NO DATA';
				echo PHP_EOL;
				var_dump($dataset);
				echo PHP_EOL;
        
        $this->logToFile($this->dateHI() . ' NSC API - ' . $type . ' NO DATA: ' . $hostname);
        return;
      }

      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE API: ' . $hostname . ' / ROWS: ' . count($dataset['rows']));

      echo $type . ' - ' . count($dataset['rows']);
      echo PHP_EOL;

      $this->saveData($dataset['rows'], $hostname, $type);

      if (count($dataset['rows']) >= $this->maxrows) {
        $startindex = $this->maxrows * $this->loops;
        $this->queryLoop($url, $hostname, $type, $startindex);
      }

  }


  private function saveData($data, $hostname, $type) {

    $this->reconMySql();

    $timestamp = $this->timestamp;

    $query = "INSERT IGNORE INTO search_console_hostnames SET hostname = '$hostname'";

    $this->db->query($query);

    $last_insert_id = $this->db->insert_id;

    // IF HOSTNAME IS ALREADY IN DB
    if ($last_insert_id == 0) {
      $query = "SELECT id FROM search_console_hostnames WHERE hostname = '$hostname'";
      $select_result = $this->db->query($query);
      $row = mysqli_fetch_assoc($select_result);
      $last_insert_id = $row['id'];
    }

    // INSERT NEW
    $sql = 'INSERT INTO search_console_data (id_hostname, timestamp, query, page, country, device, impressions, clicks, ctr, position, type) values ';

    $values = array();
    $remove = array ("'", '"', '\\');

    foreach ($data as $key => $value) {

      if ($last_insert_id == 0) {
        continue;
      }

      $query       = $value['keys'][0];
      $query       = str_replace($remove, '', $query);
      $page        = $value['keys'][1];
      $country     = $value['keys'][2];
      $device      = $value['keys'][3];            
      $impressions = (int) $value['impressions'];
      $clicks      = (int) $value['clicks'];
      $ctr         = round ($value['ctr'] * 100, 0);
      $position    = $value['position'];

      $values[] = '("'. $last_insert_id .'", "' . $timestamp . '", "' . $query . '", "' . $page . '", "' . $country . '", "' . $device . '", "' . $impressions . '", "' . $clicks . '", "' . $ctr . '", "' . $position . '", "' . $type . '")';

    }

    $sql .= implode(',', $values);

    $result = $this->db->query($sql);

    if (!empty($this->db->error)) {
      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE API - DB ERROR: ' . $hostname);
      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE API - DB ERROR: ' . $this->db->error);
    }

  }


  public function getActiveCustomers() {

    $query = "SELECT url FROM ruk_project_customers";
    $res   = $this->db->query($query);

    $customers = array();

    while ($row = $res->fetch_assoc()) {
      $hostname = str_ireplace('www.', '', parse_url($row['url'], PHP_URL_HOST));
      $customers[$hostname] = $row['url'];
    }

    return $customers;

  }


  public function checkIfDataIsImported($hostname, $timestamp) {

    $this->reconMySql();    

    $query = "SELECT id FROM search_console_hostnames WHERE hostname = '$hostname'";
    $res   = $this->db->query($query);

    $id = false;
    while ($row = $res->fetch_assoc()) {
			$id = $row['id'];
    }

    if ($id == false) {
    	return 0;
    }


    $query1 = "SELECT count(id) AS count FROM search_console_data WHERE id_hostname = '$id' AND timestamp = '$timestamp'";
    $res1   = $this->db->query($query1);

    $amount = 0;

    while ($row = $res1->fetch_assoc()) {
			$amount = $row['count'];
    }

    return $amount; 

  }


  public function dateYMD3 () {
    return date("Y-m-d", strtotime('- 3 days'));
  }


  public function dateHI ()
  {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i / d-m", $date);
  }


  public function logToFile ($content)
  {
    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);
  }


  public function auth ()
  {
    $ga = new GoogleSearchConsoleAPI('service');
    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');
    return $ga;
  }


  public function mySqlConnect ()
  {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    // set charset according to DB
    $this->db->set_charset('utf8');
    
    if (mysqli_connect_errno()) {
      printf("Connect failed: %s\n", mysqli_connect_error());
      exit();
    }

  }

  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

}

new google_searchconsole_import;

?>
