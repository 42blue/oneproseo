<?php

/*

In google_analytics_import.php importieren wir GA Daten per URL / per Tag
Hier summieren wir alle Tage aus dem aktuellen Jahr auf und packen sie in eine AGGREGATION Table
Jeden Morgen nachdem der Analytics import durch ist

*/

date_default_timezone_set('CET');

class google_analytics_aggregator {

  private $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_analytics.txt';

  public function __construct () {

    $this->mySqlConnect();

    $customers = $this->getGaCustomers();

    foreach ($customers as $key => $value) {

      $this->domain   = $value['url'];
      $this->hostname = $this->pureHostName($value['url']);

      $this->logToFile($this->dateHI() . ' GA AGGR: ' . $this->hostname);

      $al = $this->getAnalyticsDataAll();
      $fi = $this->getAnalyticsDataSeoFilter();

      if (empty($al)) {
        $this->logToFile($this->dateHI() . ' GA AGGR - NO DATA: ' . $this->hostname);
        continue;
      }

      $this->writeResults($al, $fi);

    }

    $this->db->close();

  }


  public function writeResults ($al, $fi)
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $query = "SELECT id FROM analytics_aggregation_hostnames WHERE hostname = '$this->hostname'";
    $select_result = $this->db->query($query);
    $row = mysqli_fetch_assoc($select_result);

    if (is_null($row)) {
      $query = "INSERT INTO analytics_aggregation_hostnames SET hostname = '$this->hostname'";
      $this->db->query($query);
      $last_insert_id = $this->db->insert_id;
    } else {
      $last_insert_id = $row['id'];
    }

    foreach ($al as $url => $data) {

      $url                   = $url;
      $hostname              = $data['hostname'];
      $ga_pageValue          = floatval($data['ga_pagevalue']);
      $ga_transactions       = $data['ga_transactions'];
      $ga_transactionRevenue = floatval($data['ga_transactionRevenue']);
      $ga_avgPageLoadTime    = $data['ga_avgPageLoadTime'];
      $ga_pageviews          = $data['ga_pageviews'];
      $ga_avgTimeOnPage      = $data['ga_avgTimeOnPage'];
      $ga_exits              = $data['ga_exits'];
      $ga_organicSearches    = $data['ga_organicSearches'];
      $ga_entrances          = $data['ga_entrances'];
      $ga_itemQuantity       = $data['ga_itemQuantity'];

      $ga_pageValue_filter          = floatval(0);
      $ga_transactions_filter       = 0;
      $ga_transactionRevenue_filter = floatval(0);
      $ga_avgPageLoadTime_filter    = 0;
      $ga_pageviews_filter          = 0;
      $ga_avgTimeOnPage_filter      = 0;
      $ga_exits_filter              = 0;
      $ga_organicSearches_filter    = 0;
      $ga_entrances_filter          = 0;
      $ga_itemQuantity_filter       = 0;

      if (isset($fi[$url])) {

        $ga_pageValue_filter          = floatval($fi[$url]['ga_pagevalue']);
        $ga_transactions_filter       = $fi[$url]['ga_transactions'];
        $ga_transactionRevenue_filter = floatval($fi[$url]['ga_transactionRevenue']);
        $ga_avgPageLoadTime_filter    = $fi[$url]['ga_avgPageLoadTime'];
        $ga_pageviews_filter          = $fi[$url]['ga_pageviews'];
        $ga_avgTimeOnPage_filter      = $fi[$url]['ga_avgTimeOnPage'];
        $ga_exits_filter              = $fi[$url]['ga_exits'];
        $ga_organicSearches_filter    = $fi[$url]['ga_organicSearches'];
        $ga_entrances_filter          = $fi[$url]['ga_entrances'];
        $ga_itemQuantity_filter       = $fi[$url]['ga_itemQuantity'];

      }

      // CHECK IF CONNECTION IS ALIVE
      if ($this->db->ping() === false) {
        $this->mySqlConnect();
      }

      $query = "REPLACE INTO analytics_aggregation_data SET  id_hostname           = '$last_insert_id',
                                                             url                   = '$url',
                                                             ga_pageValue          = $ga_pageValue,
                                                             ga_transactions       = '$ga_transactions',
                                                             ga_transactionRevenue = $ga_transactionRevenue,
                                                             ga_avgPageLoadTime    = '$ga_avgPageLoadTime',
                                                             ga_pageviews          = '$ga_pageviews',
                                                             ga_avgTimeOnPage      = '$ga_avgTimeOnPage',
                                                             ga_exits              = '$ga_exits',
                                                             ga_organicSearches    = '$ga_organicSearches',
                                                             ga_entrances          = '$ga_entrances',
                                                             ga_itemQuantity       = '$ga_itemQuantity',
                                                             ga_pageValue_filter          = $ga_pageValue_filter,
                                                             ga_transactions_filter       = '$ga_transactions_filter',
                                                             ga_transactionRevenue_filter = $ga_transactionRevenue_filter,
                                                             ga_avgPageLoadTime_filter    = '$ga_avgPageLoadTime_filter',
                                                             ga_pageviews_filter          = '$ga_pageviews_filter',
                                                             ga_avgTimeOnPage_filter      = '$ga_avgTimeOnPage_filter',
                                                             ga_exits_filter              = '$ga_exits_filter',
                                                             ga_organicSearches_filter    = '$ga_organicSearches_filter',
                                                             ga_entrances_filter          = '$ga_entrances_filter',
                                                             ga_itemQuantity_filter       = '$ga_itemQuantity_filter'
                                                          ";
      $this->db->query($query);

      if (!empty($this->db->error)) {
        var_dump($this->db->error);
      }

    }

  }


  // FETCH GOOGLE ANALYTICS DATA
  public function getAnalyticsDataSeoFilter ()
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }


    $first_day_year = date('Y-m-d', strtotime(date('Y-01-01')));
    $hostname       = $this->hostname;

    // get SEOFILTER google analytics data

    $sql = "SELECT
              a.id,
              a.url                        AS url,
              a.hostname                   AS hostname,
              SUM(b.ga_pageValue)          AS ga_pagevalue,
              SUM(b.ga_transactions)       AS ga_transactions,
              SUM(b.ga_transactionRevenue) AS ga_transactionRevenue,
              AVG(b.ga_avgPageLoadTime)    AS ga_avgPageLoadTime,
              SUM(b.ga_pageviews)          AS ga_pageviews,
              AVG(b.ga_avgTimeOnPage)      AS ga_avgTimeOnPage,
              SUM(b.ga_exits)              AS ga_exits,
              SUM(b.ga_organicSearches)    AS ga_organicSearches,
              SUM(b.ga_entrances)          AS ga_entrances,
              SUM(b.ga_itemQuantity)       AS ga_itemQuantity
            FROM
              analytics_urls a
            LEFT JOIN analytics_data_seofilter b
              ON b.id_url = a.id
            WHERE
              a.hostname = '$hostname'
            AND
              DATE(timestamp) >= '$first_day_year'
            AND
              DATE(timestamp) <= CURDATE()
            AND url NOT LIKE '%?%'
            GROUP BY
              url";

    $result = $this->db->query($sql);

    $scheme = parse_url($this->domain);
    $scheme = $scheme['scheme'];

    while ($row = $result->fetch_assoc()) {
      $merged = strtolower($scheme . '://' . $row['url']);
      $ga_rows[$merged] = $row;
    }

    return $ga_rows;

  }

  // FETCH GOOGLE ANALYTICS DATA
  public function getAnalyticsDataAll ()
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $first_day_year = date('Y-m-d', strtotime(date('Y-01-01')));
    $hostname       = $this->hostname;

    // get ALL google analytics data

    $sql = "SELECT
              a.id,
              a.url                        AS url,
              a.hostname                   AS hostname,
              SUM(b.ga_pageValue)          AS ga_pagevalue,
              SUM(b.ga_transactions)       AS ga_transactions,
              SUM(b.ga_transactionRevenue) AS ga_transactionRevenue,
              AVG(b.ga_avgPageLoadTime)    AS ga_avgPageLoadTime,
              SUM(b.ga_pageviews)          AS ga_pageviews,
              AVG(b.ga_avgTimeOnPage)      AS ga_avgTimeOnPage,
              SUM(b.ga_exits)              AS ga_exits,
              SUM(b.ga_organicSearches)    AS ga_organicSearches,
              SUM(b.ga_entrances)          AS ga_entrances,
              SUM(b.ga_itemQuantity)       AS ga_itemQuantity
            FROM
              analytics_urls a
            LEFT JOIN analytics_data_all b
              ON b.id_url = a.id
            WHERE
              a.hostname = '$hostname'
            AND
              DATE(timestamp) >= '$first_day_year'
            AND
              DATE(timestamp) <= CURDATE()
            AND url NOT LIKE '%?%'
            GROUP BY
              url";

    $result = $this->db->query($sql);

    $scheme = parse_url($this->domain);
    $scheme = $scheme['scheme'];

    while ($row = $result->fetch_assoc()) {
      $merged = strtolower($scheme . '://' . $row['url']);
      $ga_rows[$merged] = $row;
    }

    return $ga_rows;

  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }

  public function dateHI ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("h:i / d-m", $date);

  }

  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }


  public function getGaCustomers () {

    $ga_accounts = array();

    $sql = "SELECT
             id,
             url,
             ga_account
            FROM
             ruk_project_customers";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (!empty($row['ga_account'])) {
        $ga_accounts[$row['ga_account']] = $row;
      }
    }

    return $ga_accounts;

  }


  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


  public function testQ ()
  {

    $hostname = 'galeria-kaufhof.de';

    $sql = "SELECT
                *
            FROM
              analytics_aggregation_data
            WHERE
              id_hostname = 2131";

    $result = $this->db->query($sql);

    $ga_rows = array();

    while ($row = $result->fetch_assoc()) {
      $ga_rows[] = $row;
    }

    return $ga_rows;

  }



}

new google_analytics_aggregator;

?>
