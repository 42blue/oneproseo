<?php

date_default_timezone_set('CET');


/*

Hier importieren wir GA Daten per Tag / per URL
Die Kunden müssen in DB ruk_project_customers einen manuellen ga:XXXXXXXXXX Eintrag haben.

Der Import läuft mit einem Versatz von 3 Tagen.

Die Emailadresse:

352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com

muss dem GA Account hinzugefügt werden aus dem wir Daten abrufen!
Das geht unter -> VERWALTUNG -> NUTZERVERWALTUNG

http://ga-dev-tools.appspot.com/explorer/

GALERIA      -> ga:74172130
STYLEBOP     -> ga:11691680
THOMASCOOK   -> ga:64417690
NECKERMANN   -> ga:64424125
ADVERTISING  -> ga:4754689
FREDERICS    -> ga:736298
TERRACANIS   -> ga:55154686
ALLTOURS     -> ga:83701625
CONRAD       -> ga:92776716
LEO          -> ga:73539817
GESCHENKIDEE -> ga:8129326
IDEECADEAU   -> ga:87410611
IDEALO       -> ga:48456825

NICHT BENUTZT:

FREDERICS GUIDE -> ga:9448917
SHALOM          -> ga:51779491

*/


include('/var/www/oneproapi/analytics_v3/api/GoogleAnalyticsAPI.class.php');

class google_analytics_import {

  private $logdir = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_analytics.txt';

  public function __construct ()
  {

    $this->mySqlConnect();

    $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS START: FETCH GA CUSTOMERS ');

    $ga_accounts = $this->getGaCustomers();

//$ga_accounts = array();
//$ga_accounts['ga:11691680'] = 'ga:11691680';

    $this->gaauth($ga_accounts);

    $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS FINISH');
    $this->db->close();

  }


  public function gaauth ($ga_accounts)
  {

    $this->ga  = $this->auth();
    $auth      = $this->ga->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: NO AUTH TOKEN CREATED');
    }

    $this->ga->setAccessToken($accessToken);

    //$this->showProfiles();
    //exit;

    // LOOP OVER FILTERS
    $filters = array ('all', 'filter');

    foreach ($filters as $key => $filter) {

      $this->filter = $filter;

      // LOOP OVER CUSTOMERS
      foreach ($ga_accounts as $key => $ga_account) {

        $this->ga_account_id = $ga_account;

        $this->ga->setAccountId($this->ga_account_id);

        $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: QUERY: ' . $ga_account);

        $this->buildQuery();

      }

    }

  }


  public function buildQuery ($startindex = 1)
  {

    // $startdate = date('Y-m-d', strtotime('first day of previous month'));
    // $enddate   = date('Y-m-d', strtotime('last day of previous month'));

    $startdate = $this->dateYMD3DaysAgo();
    $enddate   = $this->dateYMD3DaysAgo();

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate,
    );

    $this->ga->setDefaultQueryParams($this->selected_date);

    $params_seofilter = array(
      'dimensions'  => 'ga:hostname,ga:landingPagePath',
      'metrics'     => 'ga:pageValue,ga:transactions,ga:transactionRevenue,ga:pageviews,ga:organicSearches,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:entrances,ga:exits,ga:itemQuantity',
      'filters'     => 'ga:pageviews!=0;ga:landingPagePath!@?',
      'segment'     => 'gaid::-5',
      'sort'        => 'ga:landingPagePath',
      'start-index' => $startindex,
      'max-results' => 10000
    );


    $params_all = array(
      'dimensions'  => 'ga:hostname,ga:landingPagePath',
      'metrics'     => 'ga:pageValue,ga:transactions,ga:transactionRevenue,ga:pageviews,ga:organicSearches,ga:avgTimeOnPage,ga:avgPageLoadTime,ga:entrances,ga:exits,ga:itemQuantity',
      'filters'     => 'ga:pageviews!=0;ga:landingPagePath!@?',
      'sort'        => 'ga:landingPagePath',
      'start-index' => $startindex,
      'max-results' => 10000
    );


    // FILTER QUERYS
    if ($this->filter == 'all') {
      $this->sql_db = 'analytics_data_all';
      $visits = $this->ga->query($params_all);
      $type = 'ALL';
    } else {
      $this->sql_db = 'analytics_data_seofilter';
      $visits = $this->ga->query($params_seofilter);
      $type = 'FILTER';
    }

    if (!isset($visits['rows'])) {
      $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: ERROR NO DATA: '. $type . ' - ' . $this->ga_account_id);
      return;
    }

    $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: ROWS '. $type .': ' . count($visits['rows']));

    if (is_null($visits['rows'])) {
      $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: ERROR NO DATA: '. $type . ' - ' . $this->ga_account_id);
    } else {
      $this->processResult($visits);
    }

    if ($visits["totalResults"] > 10000) {

      $new_start_index = $startindex + 10000;

      if ($new_start_index > $visits["totalResults"]) {
        $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: LOOP FINISHED: ' . $this->ga_account_id);
      } else {
        $this->buildQuery($new_start_index);
      }

    }

  }


  public function processResult ($visits)
  {

    $result_array = array();

    foreach ($visits['rows'] as $row) {

      foreach ($row as $cell => $val) {

        if ($cell == 0) {

          $host = $val;

        } elseif ($cell == 1) {

          if (stripos($val, 'www.') === 0 ) {
            $url = $val;
          } else {
            $url = $host . $val;
          }

          $url = rtrim($url,'/');
          $result_array[$url] = array('url' => $url);

          // create proper path
          if (stripos($val, 'not set') !== false) {
            $path = $host;
          } elseif (stripos($val, $host) === 0) {
            $path = $val;
          } else {
            $path = $host . $val;
          }

        // echo $host . '<br />' . $val . '<br />' . $path .  '<br /><br />';

          $result_array[$url]['path'] = $path;

        } elseif ($cell == 2) {

          if (isset($result_array[$url]['ga:pageValue'])) {
            $result_array[$url]['ga:pageValue'] = $result_array[$url]['ga:pageValue'] + $val;
          } else {
            $result_array[$url]['ga:pageValue'] = $val;
          }

        } elseif ($cell == 3) {

          if (isset($result_array[$url]['ga:transactions'])) {
            $result_array[$url]['ga:transactions'] = $result_array[$url]['ga:transactions'] + $val;
          } else {
            $result_array[$url]['ga:transactions'] = $val;
          }

        } elseif ($cell == 4) {

          if (isset($result_array[$url]['ga:transactionRevenue'])) {
            $result_array[$url]['ga:transactionRevenue'] = $result_array[$url]['ga:transactionRevenue'] + $val;
          } else {
            $result_array[$url]['ga:transactionRevenue'] = $val;
          }

        } elseif ($cell == 5) {

          if (isset($result_array[$url]['ga:pageviews'])) {
            $result_array[$url]['ga:pageviews'] = $result_array[$url]['ga:pageviews'] + $val;
          } else {
            $result_array[$url]['ga:pageviews'] = $val;
          }

        } elseif ($cell == 6) {

          if (isset($result_array[$url]['ga:organicSearches'])) {
            $result_array[$url]['ga:organicSearches'] = $result_array[$url]['ga:organicSearches'] + $val;
          } else {
            $result_array[$url]['ga:organicSearches'] = $val;
          }

        } elseif ($cell == 7) {

          if (isset($result_array[$url]['ga:avgTimeOnPage'])) {
            $result_array[$url]['ga:avgTimeOnPage'] = $result_array[$url]['ga:avgTimeOnPage'] + $val;
          } else {
            $result_array[$url]['ga:avgTimeOnPage'] = $val;
          }

        } elseif ($cell == 8) {

          if (isset($result_array[$url]['ga:avgPageLoadTime'])) {
            $result_array[$url]['ga:avgPageLoadTime'] = $result_array[$url]['ga:avgPageLoadTime'] + $val;
          } else {
            $result_array[$url]['ga:avgPageLoadTime'] = $val;
          }

        } elseif ($cell == 9) {

          if (isset($result_array[$url]['ga:entrances'])) {
            $result_array[$url]['ga:entrances'] = $result_array[$url]['ga:entrances'] + $val;
          } else {
            $result_array[$url]['ga:entrances'] = $val;
          }

        } elseif ($cell == 10) {

          if (isset($result_array[$url]['ga:exits'])) {
            $result_array[$url]['ga:exits'] = $result_array[$url]['ga:exits'] + $val;
          } else {
            $result_array[$url]['ga:exits'] = $val;
          }

        } elseif ($cell == 11) {

          if (isset($result_array[$url]['ga:itemQuantity'])) {
            $result_array[$url]['ga:itemQuantity'] = $result_array[$url]['ga:itemQuantity'] + $val;
          } else {
            $result_array[$url]['ga:itemQuantity'] = $val;
          }

        }

      }

    }

    $this->writeResults($result_array);

  }


  public function writeResults ($result_array)
  {


    foreach ($result_array as $url => $data) {

      $path                  = $data['path'];
      $ga_pageValue          = round($data['ga:pageValue'], 2);
      $ga_transactions       = $data['ga:transactions'];
      $ga_transactionRevenue = round($data['ga:transactionRevenue'], 2);
      $ga_avgPageLoadTime    = $data['ga:avgPageLoadTime'];
      $ga_pageviews          = $data['ga:pageviews'];
      $ga_avgTimeOnPage      = $data['ga:avgTimeOnPage'];
      $ga_exits              = $data['ga:exits'];
      $ga_organicSearches    = $data['ga:organicSearches'];
      $ga_entrances          = $data['ga:entrances'];
      $ga_itemQuantity       = $data['ga:itemQuantity'];

      $pure_host  = str_ireplace('www.', '', parse_url('http://' . $url, PHP_URL_HOST));

      // CHECK IF CONNECTION IS ALIVE
      if ($this->db->ping() === false) {
        $this->mySqlConnect();
      }

      // try to add url
      $query = "INSERT IGNORE INTO analytics_urls SET url = '$path', hostname = '$pure_host', ga_account = '$this->ga_account_id' ";

      $this->db->query($query);

      $last_insert_id = $this->db->insert_id;

      // if url is already in database
      if ($last_insert_id == 0) {
        $query = "SELECT id FROM analytics_urls WHERE url = '$path'";
        $select_result = $this->db->query($query);
        $row = mysqli_fetch_assoc($select_result);
        $last_insert_id = $row['id'];
      }

      $day = $this->selected_date['start-date'];

      // try to add url
      $query = "INSERT INTO $this->sql_db SET  id_url                = '$last_insert_id',
                                               timestamp             = '$day',
                                               ga_pageValue          = '$ga_pageValue',
                                               ga_transactions       = '$ga_transactions',
                                               ga_transactionRevenue = '$ga_transactionRevenue',
                                               ga_avgPageLoadTime    = '$ga_avgPageLoadTime',
                                               ga_pageviews          = '$ga_pageviews',
                                               ga_avgTimeOnPage      = '$ga_avgTimeOnPage',
                                               ga_exits              = '$ga_exits',
                                               ga_organicSearches    = '$ga_organicSearches',
                                               ga_entrances          = '$ga_entrances',
                                               ga_itemQuantity       = '$ga_itemQuantity'
                                            ";
      $this->db->query($query);

    }

  }


  public function getGaCustomers () {

    $ga_accounts = array();

    $sql = "SELECT
             id,
             ga_account
            FROM
             ruk_project_customers";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (!empty($row['ga_account'])) {
        $ga_accounts[$row['ga_account']] = $row['ga_account'];
      }
    }

    return $ga_accounts;

  }


  public function showProfiles ()
  {

    $profiles = $this->ga->getProfiles();

    foreach ($profiles['items'] as $item) {
      echo $id   = "ga:{$item['id']}";
      echo '<br />';
      echo $url  = $item['websiteUrl'];
      echo '<br />';
      echo $name = $item['name'];
      echo '<br />';
      echo '<br />';
    }

  }


  public function auth ()
  {

    $ga = new GoogleAnalyticsAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/analytics_v3/api/key.p12');

    return $ga;

  }


  public function mySqlConnect ()
  {
    //https://worker.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFile($this->dateHIS() . ' MYSQL CONNECT ERROR: ' . mysqli_connect_error());
      exit;
    }

  }


  public function dateYMD ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("Y-m-d", $date);

  }


  public function dateYMD3DaysAgo ()
  {

    return date('Y-m-d', strtotime( '-3 days' ) );

  }


  public function dateHI ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("h:i / d-m", $date);

  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


}

new google_analytics_import;

?>
