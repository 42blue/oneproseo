<?php

date_default_timezone_set('CET');

include('/var/www/oneproapi/analytics_v3/api/GoogleAnalyticsAPI.class.php');

class google_analytics_import {

  private $logdir = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_analytics.txt';

  public function __construct ()
  {

    $ga_accounts = array();
    $ga_accounts['ga:4754689'] = 'ga:4754689';

    $this->gaauth($ga_accounts);

  }


  public function gaauth ($ga_accounts)
  {

    $this->ga  = $this->auth();
    $auth      = $this->ga->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: NO AUTH TOKEN CREATED');
    }

    $this->ga->setAccessToken($accessToken);


    // LOOP OVER FILTERS
    $filters = array ('all');

    foreach ($filters as $key => $filter) {

      $this->filter = $filter;

      // LOOP OVER CUSTOMERS
      foreach ($ga_accounts as $key => $ga_account) {

        $this->ga_account_id = $ga_account;

        $this->ga->setAccountId($this->ga_account_id);

        $this->buildQuery();

      }

    }

  }


  public function buildQuery ($startindex = 1)
  {

    $startdate = '2010-01-01';
    $enddate   = $this->dateYMD3DaysAgo();

    $this->selected_date = array(
      'start-date' => $startdate,
      'end-date'   => $enddate,
    );

    $this->ga->setDefaultQueryParams($this->selected_date);

    $params_all = array(
      'dimensions'  => 'ga:hostname,ga:landingPagePath',
      'metrics'     => 'ga:pageviews,ga:organicSearches,ga:entrances,ga:exits',
      'filters'     => 'ga:pageviews!=0;ga:landingPagePath!@?',
      'sort'        => 'ga:pageviews',
      'start-index' => $startindex,
      'max-results' => 10000
    );

    $visits = $this->ga->query($params_all);
    $type = 'ADVERTISING';

    if (!isset($visits['rows'])) {
      $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: ERROR NO DATA: '. $type . ' - ' . $this->ga_account_id);
      return;
    }

    if (is_null($visits['rows'])) {
      $this->logToFile($this->dateHI() . ' GOOGLEANALYTICS: ERROR NO DATA: '. $type . ' - ' . $this->ga_account_id);
    } else {
      $this->processResult($visits);
    }

    if ($visits["totalResults"] > 10000) {

      $new_start_index = $startindex + 10000;

      if ($new_start_index > $visits["totalResults"]) {

      } else {
        $this->buildQuery($new_start_index);
      }

    }

  }


  public function processResult ($visits)
  {

    $result_array = array();

    foreach ($visits['rows'] as $row) {

      foreach ($row as $cell => $val) {

        if ($cell == 0) {

          $host = $val;

        } elseif ($cell == 1) {

          if (stripos($val, 'www.') === 0 ) {
            $url = $val;
          } else {
            $url = $host . $val;
          }

          $url = rtrim($url,'/');
          $result_array[$url] = array('url' => $url);

          // create proper path
          if (stripos($val, 'not set') !== false) {
            $path = $host;
          } elseif (stripos($val, $host) === 0) {
            $path = $val;
          } else {
            $path = $host . $val;
          }

        // echo $host . '<br />' . $val . '<br />' . $path .  '<br /><br />';

          $result_array[$url] = array ();

        } elseif ($cell == 2) {

          if (isset($result_array[$url]['ga:pageviews'])) {
            $result_array[$url]['ga:pageviews'] = $result_array[$url]['ga:pageviews'] + $val;
          } else {
            $result_array[$url]['ga:pageviews'] = $val;
          }

        } elseif ($cell == 3) {

          if (isset($result_array[$url]['ga:organicSearches'])) {
            $result_array[$url]['ga:organicSearches'] = $result_array[$url]['ga:organicSearches'] + $val;
          } else {
            $result_array[$url]['ga:organicSearches'] = $val;
          }

        } elseif ($cell == 4) {

          if (isset($result_array[$url]['ga:entrances'])) {
            $result_array[$url]['ga:entrances'] = $result_array[$url]['ga:entrances'] + $val;
          } else {
            $result_array[$url]['ga:entrances'] = $val;
          }

        }

      }

    }

    $file_data = json_encode($result_array);

    $file = '/var/www/enterprise.oneproseo.com/temp/cache/advertising_ga.json';

    file_put_contents($file, $file_data);

  }



  public function auth ()
  {

    $ga = new GoogleAnalyticsAPI('service');

    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/analytics_v3/api/key.p12');

    return $ga;

  }



  public function dateYMD ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("Y-m-d", $date);

  }


  public function dateYMD3DaysAgo ()
  {

    return date('Y-m-d', strtotime( '-3 days' ) );

  }


  public function dateHI ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("h:i / d-m", $date);

  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


}

new google_analytics_import;

?>
