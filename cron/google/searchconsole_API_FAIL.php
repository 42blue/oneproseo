<?php

/*

Hier importieren wir SC Daten per Tag / per KEYWORD
In DB ruk_project_customers muss gwt_account manuell auf 1 gestellt werden.

Der Import läuft mit einem Versatz von 3 Tagen.
Die Emailadresse muss in SC freigeschaltet werden:

352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com

muss dem SC Account hinzugefügt werden aus dem wir Daten abrufen!
Das geht unter -> VERWALTUNG -> NUTZERVERWALTUNG

*/

date_default_timezone_set('CET');

include('/var/www/oneproapi/searchconsole/api/GoogleSearchConsoleAPI.class.php');

class google_searchconsole_import {

  private $logdir  = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_searchconsole.txt';

  public function __construct () {

    $this->db      = $this->mySqlConnect();

    $sql = 'SELECT * FROM gwt_data ORDER BY timestamp DESC LIMIT 1';
    $result = $this->db->query($sql);
    $row = mysqli_fetch_assoc($result);

    echo  $this->dateYMD3();

    $this->logToFile($this->dateHI() . ' SEARCH CONSOLE START');
    $this->sc      = $this->auth();
    $auth          = $this->sc->auth->getAccessToken();

    if ($auth['http_code'] == 200) {
      $accessToken  = $auth['access_token'];
      $tokenExpires = $auth['expires_in'];
      $tokenCreated = time();
    } else {
      echo 'No auth token created!';
    }

    $this->sc->setAccessToken($accessToken);
    $profiles = $this->sc->getProfiles();

    $this->accounts = array();

    foreach ($profiles['siteEntry'] as $item) {
      if ($item['permissionLevel'] == 'siteUnverifiedUser') {
        continue;
      }
      $this->accounts[] = $item['siteUrl'];
    }

    $this->queryAPI();
    $this->logToFile($this->dateHI() . ' SEARCH CONSOLE END');
  }


  private function queryAPI()
  {

    foreach ($this->accounts as $url) {

      $dataset = $this->sc->getData($url, $this->dateYMD3(), $this->dateYMD3());
      $hostname = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));


var_dump($dataset);
exit;


      if (!isset($dataset['rows'])) {
        echo ' SEARCH CONSOLE API - NO DATA: ' . $hostname;
        echo PHP_EOL;
        continue;
      }

      echo $this->dateHI() . ' SEARCH CONSOLE API: ' . $hostname . ' / ROWS: ' . count($dataset['rows']);
      echo PHP_EOL;

      if (count($dataset['rows']) < 1) {
        echo $this->dateHI() . ' SEARCH CONSOLE API - ERROR: ' . $hostname . ' / ROWS: ' . count($dataset['rows']);
        echo PHP_EOL;
      }

      $this->saveData($dataset['rows'], $hostname);

    }

  }


  private function saveData($data, $hostname) {

    $ts = $this->dateYMD3();

    // TRY TO ADD HOSTNAME
    $query = "INSERT IGNORE INTO gwt_hostnames SET hostname = '$hostname'";

    $this->db->query($query);

    $last_insert_id = $this->db->insert_id;

    // IF HOSTNAME IS ALREADY IN DB
    if ($last_insert_id == 0) {
      $query = "SELECT id FROM gwt_hostnames WHERE hostname = '$hostname'";
      $select_result = $this->db->query($query);
      $row = mysqli_fetch_assoc($select_result);
      $last_insert_id = $row['id'];
    }

    // INSERT NEW
    $sql = 'INSERT INTO gwt_data (hostname_id, timestamp, query, impressions, clicks, ctr, position_1, position_2) values ';

    $values = array();
    $remove = array ("'", '"', '\\');

    foreach ($data as $key => $value) {

      if ($last_insert_id == 0) {
        continue;
      }

      $query       = $value['keys'][0];
      $query       = str_replace($remove, '', $query);
      $impressions = (int) $value['impressions'];
      $clicks      = (int) $value['clicks'];
      $ctr         = round ($value['ctr'] * 100, 0);
      $position_1  = (int) $value['position'];
      $position_2  = (int) $value['position'];

      $values[] = '("'. $last_insert_id .'", "' . $ts . '", "' . $query . '", "' . $impressions . '", "' . $clicks . '", "' . $ctr . '", "' . $position_1 . '", "' . $position_2 . '")';

    }

    $sql .= implode(',', $values);

    $result = $this->db->query($sql);

    if (empty($this->db->error)) {
      echo $hostname . ' - GOOD' . PHP_EOL;
      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE API - DB WRITE: ' . $hostname);
    } else {
      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE API - DB ERROR: ' . $hostname);
      $this->logToFile($this->dateHI() . ' SEARCH CONSOLE API - DB ERROR: ' . $this->db->error);
      //$this->logToFile($this->dateHI() . ' SEARCH CONSOLE API - DB ERROR: ' . $sql);
      //echo 'OUT';
      //exit;
    }

  }


  public function dateYMD3 ()
  {
    return date("Y-m-d", strtotime('- 4 days'));
  }


  public function dateHI ()
  {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i / d-m", $date);
  }


  public function logToFile ($content)
  {
    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);
  }


  public function auth ()
  {
    $ga = new GoogleSearchConsoleAPI('service');
    $ga->auth->setClientId('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o.apps.googleusercontent.com');
    $ga->auth->setEmail('352755655277-f93mdkia0jte33tave8n9b20lo85mo6o@developer.gserviceaccount.com');
    $ga->auth->setPrivateKey('/var/www/oneproapi/searchconsole/api/key.p12');
    return $ga;
  }


  public function mySqlConnect ()
  {
    $db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    // set charset according to DB
    $db->set_charset('utf8');
    if (mysqli_connect_errno()) {
      printf("Connect failed: %s\n", mysqli_connect_error());
      exit();
    } else {
      return $db;
    }
  }


  public function dateYMD ()
  {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);
  }

}

new google_searchconsole_import;

?>
