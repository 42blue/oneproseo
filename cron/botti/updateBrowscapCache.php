<?php
# https://github.com/browscap/browscap-php
require_once "phpbrowscap/Browscap.php";
use phpbrowscap\Browscap;
$sBrowscapCachePath = "browscap_cache";

$oBrowscap = new Browscap($sBrowscapCachePath);
$oBrowscap->updateCache();