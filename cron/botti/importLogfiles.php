<?php
require_once __DIR__ . "/../../onepro_library/onepro.conf.php";
onepro_setDebugMode(1);
#onepro_debugDBQueries(1);

$config = OnePro_Config::getInstance();

if($config->getEnvironment() == "main" && strpos(ONEPRO_BASE_PATH, "dev-")) $config->setEnvironment("dev");
else if($config->getEnvironment() == "main") $config->setEnvironment("prod");

require_once "functions.php";
require_once "phpbrowscap/Browscap.php";
use phpbrowscap\Browscap;

onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");

$sBottiPath = onepro_getPath("cron") . "botti/";
$sBrowscapCachePath = $sBottiPath . "browscap_cache";
$oBrowscap = new Browscap($sBrowscapCachePath);
$oBrowscap->doAutoUpdate = false;

$aUserAgents = array();
$aUrls = array();
$iDate = getImportDate();
$iCustomerID = getCustomer();

writeToLog("LogfileImport: starting import");
writeToLog("LogfileImport: date to import: " . $iDate);
if($iCustomerID === false) writeToLog("LogfileImport: import all customers");
else writeToLog("LogfileImport: import customer id " . $iCustomerID);

#$iCustomerID = 393;

if($iCustomerID)
{
	$aCustomers = OnePro_DB::query("SELECT `id`, `url`, `name`, `logfile`, `revproxy`, `backlink_ignore` FROM `ruk_project_customers` WHERE `logfile` IS NOT NULL AND `id` = %i", $iCustomerID);
	if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("LogfileImport: no customer id " . $iCustomerID, "error");
}
else
{
	$aCustomers = OnePro_DB::query("SELECT * FROM `ruk_project_customers` WHERE `logfile` IS NOT NULL");
	if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("LogfileImport: error fetching customers", "error");
}

$aLogFields = OnePro_DB::query("SELECT `position`, `db_field` FROM `botti_logfile_fields` ORDER BY `position` ASC");
if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("LogfileImport: could not fetch logfile fields", "error");

if(is_array($aCustomers) && is_array($aLogFields))
{
	foreach($aCustomers as $aCustomer)
	{
		if(strstr($aCustomer['logfile'], 'alltours'))
			$iDate = date('Ymd',strtotime('yesterday'));
		
		writeToLog("LogfileImport: import customer " . $aCustomer['name']);
		$aLogfilePaths = getLogfilePaths($aCustomer['logfile'], $iDate);
		foreach($aLogfilePaths as $aLogfilePathData)
		{
			$sProtocol = $aLogfilePathData['protocol'];
			$sLogfilePath = $aLogfilePathData['path'];
			if(file_exists($sLogfilePath) && is_readable($sLogfilePath))
			{
				$aLogfile = gzfile($sLogfilePath);
				if(is_array($aLogfile))
				{
					echo "import ".$sLogfilePath . "\n";
					writeToLog("lines to import: " . count($aLogfile));
					if($sProtocol == "http") $iHttps = 0;
					else $iHttps = 1;
					$iInsert = -1;
					$iLines = 0;
					$iImported = 0;
					$aInsert = array();
					$aInsertReferer = array();
					$sLogTable = "botti_" . $aCustomer['id'] . "_logs";
					$sRefererTable = "linktracker_backlinks";
					$sCustomerUrl = $aCustomer['backlink_ignore'];
					$sLogRegex = getLogRegex($aCustomer['id']);
					foreach($aLogfile as $sLine)
					{
						$iLines++;
						if(($iLines % 1000) == 0) echo $iLines . " lines processed\n";
						#$sLine = '[13/Apr/2017:15:46:30 +0200] 162.13.83.9 106.120.173.0 200 "galeria.opo-server.com" "www.galeria-kaufhof.de" "/mode/mantel-7949-page6.html" "?uid=7949&page=6" "-" "-" "Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)" 16147 6759 7048 578 - "GET /mode/mantel-7949-page6.html HTTP/1.1" 200';
						$aMatches = array();
						$iMatch = preg_match($sLogRegex, $sLine, $aMatches);
						#echo $sLogRegex;
						#echo "\n";
						#echo $sLine;
						#echo "\n";
						#print_r($aMatches);
						#die();
						if($iMatch == 0 || (count($aMatches) != 20 && count($aMatches) != 21))
						{
							continue;
						}
						
						$aTemp = explode(", ", $aMatches[3]);
						$aMatches[3] = $aTemp[0];
						$aTemp = explode(", ", $aMatches[6]);
						$aMatches[6] = $aTemp[0];
						
						unset($aMatches[0]); # remove first element which cointans the whole log line
						$sLogtime = $aMatches[1];
						if(!is_numeric($sDate)) $aMatches[1] = strtotime(formatDate($sLogtime)); # clean up date and create timestamp
						if($aMatches[1] == 0 || $aMatches[1] == "") continue;
						
						if($aMatches[2] != "-") 
						{
							$sRemoteAddr = $aMatches[2];
							$aMatches[2] = ip2long($aMatches[2]); # convert ip to long
						}
						else $aMatches[2] = 0;
						
						if($aMatches[3] != "-")
						{
							$sRemoteAddr = $aMatches[3];
							$aMatches[3] = ip2long($aMatches[3]); # convert ip to long
						}
						else $aMatches[3] = 0;
						
						$sHostname = $aMatches[5];
						if($aMatches[6] != "-" && $aMatches[6] != "") $sHostname = $aMatches[6];
						else $aMatches[6] = $aMatches[5];
						
						if($aCustomer['revproxy']) $aMatches[7] = str_replace("//", "/", $aMatches[7]); # remove double slash for revproxy configurations
						if(substr($aMatches[7], -11) == "/index.html") $aMatches[7] = str_replace("/index.html", "/", $aMatches[7]); # remove index.html at the end of request path
						if(substr($aMatches[7], -10) == "/index.php") $aMatches[7] = str_replace("/index.php", "/", $aMatches[7]); # remove index.php at the end of request path
						
						if($aMatches[8] == "-" || $aMatches[8] == "") $aMatches[8] = NULL;
						if($aMatches[9] == "-" || $aMatches[9] == "") $aMatches[9] = NULL;
						if($aMatches[10] == "-" || $aMatches[10] == "") $aMatches[10] = NULL;
						if($aMatches[12] < 30) $aMatches[12] = $aMatches[12] * 1000000; // assume the time is in seconds (nginx) if it lower than 30 (seconds) and convert it to microseconds
						
						$sRequestMethod = $aMatches[17];
						$sRequestUrl = $aMatches[18];
						$sRequestProtocol = $aMatches[19];
						if(!isset($aMatches[20]))
						{
							if(!is_null($aMatches[10]) && $aMatches[4] == '200') $sStatusLast = '301';
							else $sStatusLast = $aMatches[4];
						}
						else $sStatusLast = $aMatches[20];
						
						unset($aMatches[17]);
						unset($aMatches[18]);
						unset($aMatches[19]);
						unset($aMatches[20]);
						$aMatches[17] = $sRequestMethod . " " . $sRequestUrl . " " . $sRequestProtocol;
						$aMatches[18] = $sStatusLast;
						$aMatches[19] = $sRequestMethod;
						$aMatches[20] = $sRequestProtocol;
						
						if(!is_null($aMatches[9]))
						{
							if(strstr($aMatches[9], $sCustomerUrl) === false && strstr($aMatches[9], "suche") === false && strstr($aMatches[9], "search") === false && strstr($aMatches[9], "q=") === false && strstr($aMatches[9], "google") === false && strstr($aMatches[9], "bing") === false && strstr($aMatches[9], "yandex") === false && strstr($aMatches[9], "yahoo") === false && strstr($aMatches[9], "t-online") === false && strstr($aMatches[9], "gfsoso") === false && strstr($aMatches[9], "zapmeta") === false && strstr($aMatches[9], "adnxs.com") === false && strstr($aMatches[9], "r.yieldkit.com") === false && strstr($aMatches[9], "lcmmedia.de") === false && strstr($aMatches[9], "opo-server.com") === false && strstr($aMatches[9], "duckduckgo") === false)
							{
								if($iHttps) $sUrl = "https://";
								else $sUrl = "http://";
								$sUrl .= $sHostname;
								$sUrl .= $aMatches[7];
								$sUrl .= $aMatches[8];
								
								$aInsertReferer[] = array("customer_id" => $aCustomer['id'], "hash" => onepro_getHashId($aMatches[9]), "source" => $aMatches[9], "target" => $sUrl, "timestamp" => $aMatches[1]);
							}
						}
						
						if(stripos($aMatches[11], "bot") === false && stripos($aMatches[11], "crawler") === false && stripos($aMatches[11], "spider") === false) continue; # go further only for bots, crawlers and spiders
						if(stripos($aMatches[11], "deepcrawl.co.uk") !== false || stripos($aMatches[11], "nutch-solr") !== false || stripos($aMatches[11], "onpage.org") !== false) continue; # do not save some crawlers
						
						$aDnsInfo = getDnsInfo($sRemoteAddr, $aMatches[11]);
						if($aDnsInfo['dns_match'] == 0) continue; // do not save fake bots
						$aMatches[21] = $aDnsInfo['reverse_dns'];
						$aMatches[22] = $aDnsInfo['dns_match'];
						
						$aMatches[23] = getBrowserId($aMatches[11], $oBrowscap); # recognize user agent and save the unique identifier
						$aMatches[24] = onepro_getHashId($iHttps . $sHostname . $aMatches[7]); # create unique url identifier
						$aMatches[25] = onepro_getHashId($iHttps . $sHostname . $aMatches[7] . $aMatches[8]); # create unique url identifier
						$aMatches[26] = $iHttps;
						
						$aMatches[0] = onepro_getHashId(str_replace(" ", "", $sLogtime) . $sRemoteAddr .  $aMatches[7]); # create hash as unique request identifiert from timestamp, remote ip and request path
						
						$iInsert++;
						foreach($aLogFields as $aLogField)
						{
							$aInsert[$iInsert][$aLogField['db_field']] = $aMatches[$aLogField['position']];
						}
						
						if(count($aInsert) >= 1000)
						{
							OnePro_DB::insertIgnore($sLogTable, $aInsert);
							$aInsert = array();
							$iInsert = -1;
						}
						$iImported++;
					}
					OnePro_DB::insertIgnore($sLogTable, $aInsert);
					OnePro_DB::insertIgnore($sRefererTable, $aInsertReferer);
					writeToLog("LogfileImport: lines imported: " . $iImported);
				}
				else
				{
					writeToLog("LogfileImport: reading logfile failed (" . $sLogfilePath . ")", "error");
				}
			}
			else
			{
				writeToLog("LogfileImport: logfile doesn't exist (" . $sLogfilePath . ")", "error");
			}
		}
	}
}

writeToLog("LogfileImport: exit");

function getBrowserId($sUserAgent, $oBrowscap)
{
	$config = OnePro_Config::getInstance();
	$aUserAgents = $config->get("user_agents");
	if($aUserAgents === false) $aUserAgents = array();
	
	$iID = onepro_getHashId($sUserAgent);
	
	if(!is_array($aUserAgents[$iID]))
	{
		$aData = OnePro_DB::queryFirstRow("SELECT * FROM `botti_user_agents` WHERE `id` = " . $iID);
		if(is_null($aData) || OnePro_DB_Handler::$status === false || ((time() - strtotime($aDnsInfo['timestamp'])) > (3*24*60*60)))
		{
			$aBrowserInfo = $oBrowscap->getBrowser($sUserAgent, true); # user agent database request
			$aData = array("id" => $iID, "user_agent" => $sUserAgent, "browser" => $aBrowserInfo['Browser'], "version" => $aBrowserInfo['Version'], "data" => onepro_serializeAndCompressData($aBrowserInfo));
			OnePro_DB::insertUpdate('botti_user_agents', $aData);
		}
		else
		{
			$aBrowserInfo = onepro_unserializeAndUncompressData($aData['data']);
		}
		$aUserAgents[$iID] = $aBrowserInfo;
		$config->set("user_agents", $aUserAgents);
		
	}
	return $iID;
}

function getDnsInfo($sRemoteAddr, $sUserAgent)
{
	$aReverseDnsBotMatching = array(
		"google" => "/.*(google\.com|googlebot\.com)\.?$/",
		"http://www.bing.com/bingbot.htm" => "/.*search\.msn\.com\.?$/",
		"http://search.msn.com/msnbot.htm" => "/.*search\.msn\.com\.?$/",
		"http://yandex.com/bots" => "/.*(yandex\.com|yandex\.net|yandex.ru)\.?$/",
		"http://www.baidu.com/search/spider.html" => "/.*crawl\.baidu\.com\.?$/",
		"yahoo! slurp" => "/.*crawl\.yahoo\.net\.?$/",
	);
	
	if($sRemoteAddr !== false && $sRemoteAddr != "" && $sRemoteAddr != 0)
	{
		$iRemoteAddr = ip2long($sRemoteAddr);
		// search in the database for cached items
		$aDnsInfo = OnePro_DB::queryFirstRow("SELECT * FROM `botti_dns` WHERE `remote_host` = " . $iRemoteAddr);
		// check if found and not older than 3 days
		if(is_null($aDnsInfo) || OnePro_DB_Handler::$status === false || ((time() - strtotime($aDnsInfo['timestamp'])) > (3*24*60*60)))
		{
			$aDnsInfo = array();
			$aDnsInfo['timestamp'] = date("Y-m-d H:i:s");
			$aDnsInfo['remote_host'] = $iRemoteAddr;
			//get hostname by ip
			$aDnsInfo['reverse_dns'] = @gethostbyaddr($sRemoteAddr);
			if($aDnsInfo['reverse_dns'] !== false && $aDnsInfo['reverse_dns'] !== $sRemoteAddr) 
			{
				// get ip by hostname
				$aDnsInfo['reverse_dns_remote_host'] = @gethostbyname($aDnsInfo['reverse_dns']);
				if($aDnsInfo['reverse_dns_remote_host'] === $aDnsInfo['reverse_dns'])
				{
					$aDnsInfo['reverse_dns_remote_host'] = 0;
				}
				else
				{
					$aDnsInfo['reverse_dns_remote_host'] = ip2long($aDnsInfo['reverse_dns_remote_host']);
				}
			}
			else
			{
				$aDnsInfo['reverse_dns_remote_host'] = 0;
			}
			
			OnePro_DB::insertUpdate('botti_dns', $aDnsInfo);
		}
		
		$bReverseDnsBotMatch = true;
		// Check reverse dns hostname for most popular bots
		foreach($aReverseDnsBotMatching as $sUserAgentMatch => $sReverseDnsMatch)
		{
			// Recognize the bot by specific string in the user agent
			if(stripos($sUserAgent, $sUserAgentMatch) !== false)
			{
				// Match the hostname with defined pattern (given by the search engine)
				if(preg_match($sReverseDnsMatch, $aDnsInfo['reverse_dns']))
				{
					$bReverseDnsBotMatch = true;
				}
				else
				{
					$bReverseDnsBotMatch = false;
				}
			}
		}
		
		// check if the ip addresses and reverse dns match
		if($aDnsInfo['reverse_dns_remote_host'] === $aDnsInfo['remote_host'] && $bReverseDnsBotMatch)
		{
			$aDnsInfo['dns_match'] = 1;
		}
		else
		{
			$aDnsInfo['dns_match'] = 0;
		}
		
		return $aDnsInfo;
	}
	else
	{
		return array("reverse_dns" => NULL, "dns_match" => 0);
	}
}

function getLogRegex($iCustomerID)
{
	/*$timestamp = time() - (170 * 24 * 60 * 60);

	for($i = 0; $i < 170; $i++){
		$date = date("Ymd", $timestamp + $i * 24 * 3600);
	
	echo $date . "\n";	
	}
	exit;
	*/

	/* Description of regexp

	[24/Dec/2014:06:25:31 +0100] 
	162.13.204.244 
	54.242.21.13
	200 
	"thomascook.opo-server.com" 
	"www.thomascook.de" 
	"/hotels//6820-atlantis-paradise-island-the-cove-atlantis.html" 
	"" 
	"-" 
	"-" 
	"rogerbot/1.1 (http://moz.com/help/pro/what-is-rogerbot-, rogerbot-crawler+pr2-crawler-51@moz.com)" 
	47592 
	38645 
	38936 
	448 
	- 
	"GET /hotels//6820-atlantis-paradise-island-the-cove-atlantis.html HTTP/1.1"
	*/
	
#[2017-04-09T17:29:54+00:00]	158.69.247.29	158.69.247.29 503	"www.verypoolish.com"		"-"							"/en/index.php"					"-"					"-" "-" "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; MS-RTC LM 8" 0.023 5166 5374 594 - "-" 503
#[13/Apr/2017:15:46:30 +0200]162.13.83.9		106.120.173.0 200	"galeria.opo-server.com"	"www.galeria-kaufhof.de"	"/mode/mantel-7949-page6.html"	"?uid=7949&page=6"	"-" "-" "Sogou web spider/4.0(+http://www.sogou.com/docs/help/webmasters.htm#07)" 16147 6759 7048 578 - "GET /mode/mantel-7949-page6.html HTTP/1.1" 200
	
	$aLogRegex = array();
	$aLogRegex['default']['prefix'] = "/^";
	$aLogRegex['default'][1] = '\[([^:]+:\d+:\d+:\d+ [^\]]+)\] '; # timestamp
	$aLogRegex['default'][2] = '(\S+) '; # remote host ip
	$aLogRegex['default'][3] = '(\S+|\S+, \S+|\S+, \S+, \S+) '; # x forwarded for ip
	$aLogRegex['default'][4] = '(\S+) '; # status
	$aLogRegex['default'][5] = '\"(\S+)\" '; # host
	$aLogRegex['default'][6] = '\"(\S+|\S+, \S+|\S+, \S+, \S+)\" '; # x forwarded for host
	$aLogRegex['default'][7] = '\"(\S*)\" '; # request path
	$aLogRegex['default'][8] = '\"(\S*)\" '; # search query
	$aLogRegex['default'][9] = '\"(\S*)\" '; # referer
	$aLogRegex['default'][10] = '\"(\S*)\" '; # location
	$aLogRegex['default'][11] = '\"(.*)\" '; # user agent
	$aLogRegex['default'][12] = '(\S+) '; # response time
	$aLogRegex['default'][13] = '(\S+) '; # content lenght
	$aLogRegex['default'][14] = '(\S+) '; # bytes sent including headers
	$aLogRegex['default'][15] = '(\S+) '; # bytes received including request and headers
	$aLogRegex['default'][16] = '(\S+) '; # connection status
	$aLogRegex['default'][17] = '\"(\S+) (.*?) (\S+)\"'; # first line of request
	$aLogRegex['default'][18] = '( \S+)?'; # status of last request
	$aLogRegex['default']['suffix'] = "$/";

	$aLogRegex[229]['prefix'] = "/^";
	$aLogRegex[229][1] = '\[([^:]+T\d+:\d+:\d+[^\]]+)\] '; # timestamp
	$aLogRegex[229][2] = '(\S+) '; # remote host ip
	$aLogRegex[229][3] = '(\S+|\S+, \S+|\S+, \S+, \S+) '; # x forwarded for ip
	$aLogRegex[229][4] = '(\S+) '; # status
	$aLogRegex[229][5] = '\"(\S+)\" '; # host
	$aLogRegex[229][6] = '\"(\S+|\S+, \S+|\S+, \S+, \S+)\" '; # x forwarded for host
	$aLogRegex[229][7] = '\"(\S*)\" '; # request path
	$aLogRegex[229][8] = '\"(\S*)\" '; # search query
	$aLogRegex[229][9] = '\"(\S*)\" '; # referer
	$aLogRegex[229][10] = '\"(\S*)\" '; # location
	$aLogRegex[229][11] = '\"(.*)\" '; # user agent
	$aLogRegex[229][12] = '(\S+) '; # response time
	$aLogRegex[229][13] = '(\S+) '; # content lenght
	$aLogRegex[229][14] = '(\S+) '; # bytes sent including headers
	$aLogRegex[229][15] = '(\S+) '; # bytes received including request and headers
	$aLogRegex[229][16] = '(\S+) '; # connection status
	$aLogRegex[229][17] = '\"(\S+) (.*?) (\S+)\"'; # first line of request
	$aLogRegex[229][17] = '\"(((-)))\"'; # first line of request
	$aLogRegex[229][18] = '( \S+)?'; # status of last request
	$aLogRegex[229]['suffix'] = "$/";
	
	#$aLogRegex[1] = $aLogRegex['default'];
	#$aLogRegex[1][3] = '(\S+|\S+, \S+|\S+, \S+, \S+) '; # x forwarded for ip
	#$aLogRegex[1][6] = '\"(\S+|\S+, \S+|\S+, \S+, \S+)\" '; # x forwarded for host
	
	if(is_array($aLogRegex[$iCustomerID])) return implode("", $aLogRegex[$iCustomerID]);
	return implode("", $aLogRegex['default']);
}