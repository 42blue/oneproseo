<?php
function writeToLog($sMessage, $sType = "info")
{
	if($sType != "info" && $sType != "error") $sType = "error";
	OnePro_DB::insert("botti_system_log", array(
		"timestamp" => date('Y-m-d H:i:s'),
		"type" => $sType,
		"message" => $sMessage
		)
	);
}

function formatDate($sDate)
{
	$aMatches = array();
	$aSearch = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	$aReplace = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
	$sDate = str_replace($aSearch, $aReplace, $sDate);
	preg_match("/([^:]+):(\d+:\d+:\d+ [^\]]+)/", $sDate, $aMatches);
	if(count($aMatches) == 0) return $sDate;
	$sDate = str_replace("/", ".", $aMatches[1] . " " . $aMatches[2]);

	return $sDate;
}

function getLogfilePaths($sLogfileName, $iDate)
{
	$aLogfilePaths = array();
	$aLogfileNames = explode(",", $sLogfileName);
	foreach($aLogfileNames as $sTempLogfileName)
	{
		$aLogfilePaths[] = array("protocol" => "http", "path" => "/var/www/botti-logfiles/" . $sTempLogfileName . "/" . $sTempLogfileName . "-onepro.log-" . $iDate . ".gz");
		$aLogfilePaths[] = array("protocol" => "https", "path" => "/var/www/botti-logfiles/" . $sTempLogfileName . "/ssl-" . $sTempLogfileName . "-onepro.log-" . $iDate . ".gz");
	}

	return $aLogfilePaths;
}

function getCustomer($iCustomerID = false)
{
	$aParameterValues = onepro_getScriptParameters(array("customer"));
	if(isset($aParameterValues['customer']) && is_numeric($aParameterValues['customer']))
	{
		$iCustomerID = $aParameterValues['customer'];
	}

	return $iCustomerID;
}

function getImportDate()
{
	$aParameterValues = onepro_getScriptParameters(array("date"));
	if(isset($aParameterValues['date']) && is_numeric($aParameterValues['date']))
	{
		$iDate = $aParameterValues['date'];
	}
	else
	{
		$iDate = date("Ymd", strtotime("now"));
	}

	return $iDate;
}

function sendPageNotFoundAlertMail($sMailRecipient, $sCustomerName, $sDate, $sUrl, $iErrors, $aErrors)
{

	$sMailSenderName = "OneProSeo.com";
	$sMailSenderMail = "infomail@oneproseo.com";
	$sMailSubject = "OneProSeo / Alert / 404 Errors @ " . $sCustomerName;

	$sMailBody = '<html><head><title>OneProSeo / Alert / 404 Errors</title>';
	$sMailBody .= '<style>#btn:hover {background-color: #6ed032;}</style></head><body>';

	$sMailBody .= '
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
						<img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
					</td>
				</tr>
				<tr>
					<td style="width:10px; background-color:#444444;"></td>
					<td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
						<span style="font-size: 16px; font-color: #636363;">
						Hi ' . $sCustomerName . ' Team,<br /><br />
						Botti found ' . $iErrors . ' new 404 errors @ ' . $sCustomerName . ' from  ' . $sDate . '.<br /><br /><br />
						<a href="' . $sUrl . '" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>
						<br /><br /><br />
						URLs found<br /><br />';
	foreach($aErrors as $row)
	{
		$sMailBody .= '<a href="' . $row['url_link'] . '">' . $row['url_link'] . '</a><br />';
	}
	$sMailBody .= '
						<br /><br />
						cheers,<br />
						OneProSeo.com<br />
						S-T-A-O Strategy, Testing, Analytics, Optimization<br />
						Big Data SEO Analytics<br /><br /></span>
					</td>
					<td style="width:10px; background-color:#444444;"></td>
				</tr>
				<tr>
					<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
				</tr>
			</table>';
	$sMailBody .= '</body></html>';

	onepro_sendMail($sMailSenderName, $sMailSenderMail, $sMailRecipient, $sMailSubject, $sMailBody);

}
