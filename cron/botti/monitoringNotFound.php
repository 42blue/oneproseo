<?php
require_once __DIR__ . "/../../onepro_library/onepro.conf.php";
onepro_setDebugMode(1);
#onepro_debugDBQueries(1);

$config = OnePro_Config::getInstance();

if($config->getEnvironment() == "main" && strpos(ONEPRO_BASE_PATH, "dev-")) $config->setEnvironment("dev");
else if($config->getEnvironment() == "main") $config->setEnvironment("prod");

onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");

require_once "functions.php";

$iDateTo = getImportDate();
if(date("w", $iDateTo) == 1)
{
	$iDateFrom = date("Ymd", strtotime("-3 day"));
}
else
{
	$iDateFrom = $iDateTo;
}
$sDateFrom = substr($iDateFrom, 6, 2) . "." . substr($iDateFrom, 4, 2) . "." . substr($iDateFrom, 0, 4);
$sDateTo = substr($iDateTo, 6, 2) . "." . substr($iDateTo, 4, 2) . "." . substr($iDateTo, 0, 4);
$iTimestampFrom = strtotime($iDateFrom . " 00:00:00");
$iTimestampTo = strtotime($iDateTo . " 23:59:59");
$iCustomerID = getCustomer();

writeToLog("PageNotFound Monitoring: starting");
writeToLog("PageNotFound Monitoring: from " . $sDateFrom . " to " . $sDateTo);
if($iCustomerID === false) writeToLog("PageNotFound Monitoring: all customers");
else writeToLog("PageNotFound Monitoring: customer id " . $iCustomerID);

if($iCustomerID)
{
	$aCustomers = OnePro_DB::query("SELECT `id`, `name`, `logfile`, `revproxy` FROM `ruk_project_customers` WHERE `logfile` IS NOT NULL AND `id` = %i", $iCustomerID);
	if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("PageNotFound Monitoring: no customer id " . $iCustomerID, "error");
}
else
{
	$aCustomers = OnePro_DB::query("SELECT * FROM `ruk_project_customers` WHERE `logfile` IS NOT NULL");
	if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("PageNotFound Monitoring: error fetching customers", "error");
}

if(is_array($aCustomers))
{
	foreach($aCustomers as $aCustomer)
	{
		writeToLog("PageNotFound Monitoring: precessing customer " . $aCustomer['name']);
		$sLogTable = "botti_" . $aCustomer['id'] . "_logs";
		$aErrors = OnePro_DB::query(
			"SELECT 
			*,
			CONCAT(IF(`https` = '1', 'https://', 'http://'), IF(`hostname_client` = '-', `hostname`, `hostname_client`), `request_path`, IF(`query_string` IS  NULL, '', `query_string`)) as url_link 
			FROM `" . $sLogTable . "` WHERE `timestamp` >= %i AND `timestamp` < %i AND (`status` = 404 OR `status_last` = 404)", $iTimestampFrom, $iTimestampTo
			
		);
		if(OnePro_DB::count() == 0) 
		{
			writeToLog("PageNotFound Monitoring: no page not found entries found");
			continue;
		}
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) 
		{
			writeToLog("PageNotFound Monitoring: an error occured while fetching page not found entries", "error");
			continue;
		}
		if(OnePro_DB::count() > 0) 
		{
			$sMailRecipient = $aCustomer['team_email'];
			if($sMailRecipient == "" || is_null($sMailRecipient)) $sMailRecipient = "matthias.hotz@diva-e.com";
			$iErrors = OnePro_DB::count();
			$sUrl = "https://enterprise.oneproseo.com/botti/project/" . $aCustomer['id'] . "?datefrom=" . $sDateFrom . "&dateto=" . $sDateTo . "&status_code=404";
			$sCustomerName = $aCustomer['name'];
			//sendPageNotFoundAlertMail($sMailRecipient, $sCustomerName, $sDateTo, $sUrl, $iErrors, $aErrors);
		}
	}
}