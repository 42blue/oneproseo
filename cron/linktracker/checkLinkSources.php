<?php
require_once __DIR__ . "/../../onepro_library/onepro.conf.php";
onepro_setDebugMode(1);
#onepro_debugDBQueries(1);

$config = OnePro_Config::getInstance();

if($config->getEnvironment() == "main" && strpos(ONEPRO_BASE_PATH, "dev-")) $config->setEnvironment("dev");
else if($config->getEnvironment() == "main") $config->setEnvironment("prod");

require_once "functions.php";

onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");

$iDate = getImportDate();
$iCustomerID = getCustomer();

if($iCustomerID)
{
	$aCustomers = OnePro_DB::query("SELECT `id`, `url`, `name`, `logfile`, `revproxy`, `backlink_ignore` FROM `ruk_project_customers` WHERE `logfile` IS NOT NULL AND `id` = %i", $iCustomerID);
	if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("LogfileImport: no customer id " . $iCustomerID, "error");
}
else
{
	$aCustomers = OnePro_DB::query("SELECT * FROM `ruk_project_customers` WHERE `logfile` IS NOT NULL");
	if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("LogfileImport: error fetching customers", "error");
}

if(count($aCustomers) > 0)
{
	foreach($aCustomers as $iKey => $aCustomer)
	{
		$aBacklinks = OnePro_DB::query("SELECT * FROM `linktracker_backlinks` WHERE `customer_id` = %i AND `timestamp` > %i ORDER BY `timestamp` DESC", $aCustomer['id'], $iDate);
		if(OnePro_DB::count() == 0 || OnePro_DB_Handler::$status === false) writeToLog("Linktracker Checker: could not banklicks", "error");

		if(count($aBacklinks) > 0)
		{
			$iCount = 0;
			foreach($aBacklinks as $aBacklink)
			{
				OnePro_DB::insertIgnore('linktracker_log', array("customer_id" => $aBacklink['customer_id'], "hash" => $aBacklink['hash'], "timestamp" => time(), "source_http_code" => $aBacklink['source_http_code'], "target_http_code" => $aBacklink['target_http_code'], "link_status" => $aBacklink['link_status']));
				
				$oLinkfinder = new OnePro_Linkfinder($aBacklink['target'], array($aBacklink['source']));
				$oLinkfinderTarget = new OnePro_Linkfinder($aBacklink['target'], array($aBacklink['target']));
				$aResult = $oLinkfinder->getResult();
				$aMeta = $oLinkfinder->getMetaData();
				$aResultTarget = $oLinkfinderTarget->getResult();
				
				$sNofollow = "-";
				if($aResult[$aBacklink['source']]['rel'] == "nofollow")
				{
					$sNofollow = "nofollow [attr]";
				}
				else if(strstr($aMeta['robots'], "nofollow") !== false)
				{
					$sNofollow = "nofollow [meta]";
				}
				else if($aResult[$aBacklink['source']]['http_code'] == 200 && $aResult[$aBacklink['source']]['result'])
				{
					$sNofollow = "follow";
				}
				
				$aBacklinkStatus = array();
				$aBacklinkStatus = array("source_http_code" => $aResult[$aBacklink['source']]['http_code'], "target_http_code" => $aResultTarget[$aBacklink['target']]['http_code'], "link_nofollow" => $sNofollow, "link_status" => 1, "timestamp_checked" => time());
				if($aResult[$aBacklink['source']]['result']) $aBacklinkStatus['link_status'] = 2;
				if($aResult[$aBacklink['source']]['text']) $aBacklinkStatus['link_text'] = $aResult[$aBacklink['source']]['text'];
				
				OnePro_DB::update('linktracker_backlinks', $aBacklinkStatus, "customer_id = %i AND hash = %i", $aBacklink['customer_id'], $aBacklink['hash']);
				
				if((++$iCount) % 100 == 0) echo $iCount . " links checked\n";
			}
		}
	
	}
}
