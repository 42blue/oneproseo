<?php
function writeToLog($sMessage, $sType = "info")
{
	if($sType != "info" && $sType != "error") $sType = "error";
	OnePro_DB::insert("linktracker_system_log", array(
		"timestamp" => date('Y-m-d H:i:s'),
		"type" => $sType,
		"message" => $sMessage
		)
	);
}

function formatDate($sDate)
{
	$aMatches = array();
	$aSearch = array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	$aReplace = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
	$sDate = str_replace($aSearch, $aReplace, $sDate);
	preg_match("/([^:]+):(\d+:\d+:\d+ [^\]]+)/", $sDate, $aMatches);
	$sDate = str_replace("/", ".", $aMatches[1] . " " . $aMatches[2]);
	
	return $sDate;
}

function getCustomer($iCustomerID = false)
{
	$aParameterValues = onepro_getScriptParameters(array("customer"));
	if(isset($aParameterValues['customer']) && is_numeric($aParameterValues['customer']))
	{
		$iDate = $aParameterValues['customer'];
	}

	return $iCustomerID;
}

function getImportDate()
{
	$aParameterValues = onepro_getScriptParameters(array("date"));
	if(isset($aParameterValues['date']) && is_numeric($aParameterValues['date']))
	{
		$iDate = $aParameterValues['date'];
	}
	else
	{
		$iDate = date("Ymd", strtotime("2 years ago"));
	}
	
	return strtotime($iDate);
}
