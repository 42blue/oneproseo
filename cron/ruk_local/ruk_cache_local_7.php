<?php

date_default_timezone_set('CET');

class renderRUKOverviews {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/local/';

  public function __construct () {

    $this->mySqlConnect();

    $this->fetchSets();

    $this->db->close();

  }

  private function fetchSets ()
  {

    $sql = "SELECT id, ga_account, gwt_account, country, url, ignore_subdomain from ruk_project_customers WHERE local = 1";

    $result   = $this->db->query($sql);
    $projects = array();

    $pi = 1;

    echo $result->num_rows . PHP_EOL;

    while ($row = $result->fetch_assoc()) {

      $this->customer_id = $row['id'];

      $rows = $this->selectAllKeywordSets($row['id']);

      echo $pi . ' - DOING Project: ' . $row['id'] . ' - count:' . count($rows) . PHP_EOL;
      if (count($rows) < 1) {
        continue;
      }

      $this->gwt_account = $row['gwt_account'];
      $this->ga_account  = $row['ga_account'];
      $this->url         = $row['url'];
      $this->country     = $row['country'];
      $this->hostname_id = $row['id'];
      $this->ignore_subd = $row['ignore_subdomain'];
      $this->hostname    = $this->pureHostName($row['url']);

      $this->getRankingData($rows);
      $pi++;

    }



  }

  private function selectAllKeywordSets ($selected_project) {

    $sql = "SELECT
              b.keyword AS keyword
            FROM ruk_local_keyword_sets a
              LEFT JOIN ruk_local_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = $selected_project
            AND a.type = 'key'";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }


  private function getRankingData($rows)
  {

    $comma_separated_kws = implode('", "', $rows);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $rank_add            = 0;

    $sql = "SELECT
              id,
              keyword,
              location,
              rs_type,
              timestamp
            FROM
              ruk_scrape_keywords_local
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = '$this->country'
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 7 DAY
            AND
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings_local
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    $rows_ra  = array();

    while ($row = $result2->fetch_assoc()) {
      if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
        $row['url'] = strtolower($row['url']);
        $rows_ra[] = array('k' => $rows_kw[$row['id_kw']]['keyword'], 't' => $rows_kw[$row['id_kw']]['timestamp'], 'p' => $row['position'], 'u' => $row['url'], 'l' => $rows_kw[$row['id_kw']]['location'], 'rs' => $rows_kw[$row['id_kw']]['rs_type']);
      }
    }


// ANALYTICS DATA
    if (!empty($this->ga_account)) {

      foreach ($rows_ra as $key => $rankset) {

        $timestamp = $rankset['t'];
        $url       = preg_replace('(^https?://)', '', $rankset['u']);
        $tr        = 0;
        $pv        = 0;
        $en        = 0;

        $sql = "SELECT
                  a.id,
                  a.url,
                  b.ga_transactionRevenue AS ga_transactionRevenue,
                  b.ga_pageviews          AS ga_pageviews,
                  b.ga_entrances          AS ga_entrances,
                  b.timestamp             AS timestamp
                FROM
                  analytics_urls a
                LEFT JOIN analytics_data_seofilter b
                  ON b.id_url = a.id
                WHERE
                  a.url = '$url'
                AND
                  DATE(timestamp) = '$timestamp'";

        $result_ga = $this->db->query($sql);

        while ($row_ga = $result_ga->fetch_assoc()) {

          if (!empty($row_ga['ga_transactionRevenue'])) {
            $tr = $row_ga['ga_transactionRevenue'];
          }
          if (!empty($row_ga['ga_pageviews'])) {
            $pv = $row_ga['ga_pageviews'];
          }
          if (!empty($row_ga['ga_entrances'])) {
            $en = $row_ga['ga_entrances'];
          }

        }

        $rows_ra[$key] = array('k' => $rankset['k'], 't' => $rankset['t'], 'p' => $rankset['p'], 'u' => $rankset['u'], 'l' => $rankset['l'], 'rs' => $rankset['rs'], 'tr' => $tr, 'pv' => $pv, 'en' => $en);

      }

    }
// ANALYTICS DATA

// GWT DATA
    if ($this->gwt_account == '1') {

      $sql = "SELECT
                id
               FROM
                gwt_hostnames
               WHERE
                hostname = '$this->hostname'";

      $result_gwh = $this->db->query($sql);

      while ($row_gwh = $result_gwh->fetch_assoc()) {
        $hostname_id = $row_gwh['id'];
      }

      if (!empty($hostname_id)) {

        $sql = "SELECT
                  clicks,
                  query,
                  timestamp
                FROM
                  gwt_data
                WHERE
                  hostname_id = $hostname_id
                AND
                  DATE(timestamp) > CURDATE() - INTERVAL 7 DAY";

        $result_gw = $this->db->query($sql);

        $gwt_rows = array();

        while ($row_gw = $result_gw->fetch_assoc()) {

          if (!empty($row_gw['clicks'])) {
            $hash = crc32 ($row_gw['query'] . $row_gw['timestamp']);
            $gwt_rows[$hash] = $row_gw['clicks'];
          }

        }


        foreach ($rows_ra as $key => $rankset) {

          $timestamp = $rankset['t'];
          $keyword   = $rankset['k'];
          $cl        = 0;

          $hash2 = crc32 ($keyword . $timestamp);

          if (isset($gwt_rows[$hash2])) {

            if (!isset($rankset['tr'])) {
              $rankset['tr'] = 0;
              $rankset['pv'] = 0;
              $rankset['en'] = 0;
            }

            $rows_ra[$key] = array(
               'k'  => $rankset['k'],
               't'  => $rankset['t'],
               'p'  => $rankset['p'],
               'u'  => $rankset['u'],
               'l'  => $rankset['l'],
               'rs' => $rankset['rs'],
               'tr' => $rankset['tr'],
               'pv' => $rankset['pv'],
               'en' => $rankset['en'],
               'cl' => $gwt_rows[$hash2]
              );

          }

        }

      }

    }
// GWT DATA

    $tmpfilename = 'metrics_local_data_'.$this->customer_id.'.tmp';
    $this->writeFile($tmpfilename, $rows_ra);

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {
      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);
      return unserialize($variable);
    } else {
      return null;
    }

  }

  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;
    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  public function pureHostNameNoSubdomain ($host)
  {

    $domain = $host;

    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) && $this->ignore_subd == 1) {
      return $regs['domain'];
    } else {
      return $host;
    }

  }

  public function mySqlConnect ()
  {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }

  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }

  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


}

new renderRUKOverviews;

?>
