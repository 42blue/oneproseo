<?php

date_default_timezone_set('CET');

class ruk_delete {

  private $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->deleteRS();

    $this->db->close();

  }


  private function deleteRS ()
  {

    $date = new DateTime("today");
    // 12 month back
    date_sub($date, new DateInterval("P12M"));
    $seven_back  = $date->format("Y-m-d");

    $sql = "SELECT id, keyword FROM ruk_scrape_keywords_local WHERE timestamp < '$seven_back'";
    $result = $this->db->query($sql);

    $delete = array();

    while ($row = $result->fetch_assoc()) {
      $delete[$row['id']] = $row['keyword'];
    }

    $this->logToFile($this->dateHI() . ' RMS: ' .  $seven_back . ': ' . count($delete));

    sleep(3);

    if (count($delete) == 0) {
      exit;
    }

    $i = 0;
    foreach ($delete as $key => $keyword) {

      $i++;

      $sql1 = "DELETE FROM ruk_scrape_keywords_local WHERE id = $key";
      $result = $this->db->query($sql1);

      if (!empty($this->db->error)) {
        echo $this->db->error . '<br />' . PHP_EOL;
      }

      $sql2 = "DELETE FROM ruk_scrape_rankings_local WHERE id_kw = $key";
      $result = $this->db->query($sql2);

      if (!empty($this->db->error)) {
        echo $this->db->error . '<br />' . PHP_EOL;
      }

     echo 'RM: ' . $i . ' - ' . $keyword . PHP_EOL;

    }

  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }

  public function dateHI ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("h:i", $date);

  }

  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }

}

new ruk_delete;

?>
