<?php

date_default_timezone_set('CET');
ini_set('mysql.connect_timeout', 600);
ini_set('default_socket_timeout', 600);
ini_set("soap.wsdl_cache_enabled", "0");
/*
 Doku:
 http://www.ranking-spy.com/ranking/webserviceapi/doc/index.html?com/atenis/rankingspy/web/webservice/RankingSpyService.html
*/

class rankingspy_keywordupdate {

  private $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_rs_local.txt';
  private $logdirex  = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/exceptionlog.txt';
  private $user      = 'advertisingAdmin';
  private $pass      = 'lucile089open!';

  public function __construct ()
  {

    $this->mySqlConnect();

    $this->startRun();

    $this->db->close();

  }


  private function startRun ()
  {

    $channel_ids = $this->getChannelIds();

    if (is_null($channel_ids) || count($channel_ids) == 0) {
      $this->logToFile($this->dateHI() . ' RS LOCAL: ALL DONE ');
      exit;
    }

    $this->loopChannels($channel_ids);

  }


  private function loopChannels ($channel_ids)
  {

    foreach ($channel_ids as $channel_id => $ruk_id) {

      $this->logToFile($this->dateHI() . ' RS LOCAL: WORKING CHANNEL: ' . $channel_id . ' / RUK: ' . $ruk_id);

      $sql = "UPDATE ruk_local_keyword_sets SET working = 1 WHERE id = '$ruk_id'";
      $res = $this->db->query($sql);

      $this->setCountryLanguage($ruk_id);
      $this->setTown($channel_id);
      $this->setRsType($ruk_id);

      $this->getRankings($channel_id);

      $today = $this->dateYMD();
      $sql = "UPDATE ruk_local_keyword_sets SET working = 0, lastupdate = '$today' WHERE id = '$ruk_id'";
      $res = $this->db->query($sql);

    }

  }


  private function setCountryLanguage ($ruk_id) {

      $sql = "SELECT a.id            AS setid,
                     b.country       AS country
              FROM ruk_local_keyword_sets a
                LEFT JOIN ruk_project_customers b
                  ON b.id = a.id_customer
              WHERE a.id = '$ruk_id'";

      $res1 = $this->db->query($sql);
      $row1 = $res1->fetch_array(MYSQLI_ASSOC);

      $this->country = $row1['country'];

  }


  private function setTown ($id_rs) {

      $sql = "SELECT location FROM ruk_local_sets2rs WHERE id_rs = '$id_rs'";
      $res1 = $this->db->query($sql);
      $row1 = $res1->fetch_array(MYSQLI_ASSOC);

      $this->town = $row1['location'];

  }


  private function setRsType ($ruk_id) {

      $sql = "SELECT rs_type FROM ruk_local_keyword_sets WHERE id = '$ruk_id'";
      $res1 = $this->db->query($sql);
      $row1 = $res1->fetch_array(MYSQLI_ASSOC);

      $this->rs_type = $row1['rs_type'];

  }

  private function getChannelIds ()
  {

    $today = $this->dateYMD();
    $this->logToFile($this->dateHI() . ' RS LOCAL: GET CHANNEL IDS ');

    $sql = "SELECT
              id_kw_set,
              id_rs
            FROM
              ruk_local_sets2rs";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $channel_ids[$row['id_rs']] = $row['id_kw_set'];
    }

    return $channel_ids;

  }


  private function getRankings ($channel_id)
  {

    $client = new SoapClient('http://www.ranking-spy.com/oneadvertisingapi1611/services/RankingSpyService?wsdl', array('trace' => 1));

    try {

      echo 'FETCH CHANNEL: ' . $channel_id . "\n";

      $results = $client->getRecentReportsByChannel($this->user, $this->pass, $channel_id);

      // CHECK IF CONNECTION IS ALIVE
      if ($this->db->ping() === false) {
        $this->mySqlConnect();
      }

      foreach ($results as $result) {

        echo 'FETCH SEARCH RUN: ' . $result->searchRunId . "\n";
        $results_kw = $client->retrieveSearchRun($this->user, $this->pass, $result->searchRunId);

        // CHECK IF CONNECTION IS ALIVE
        if ($this->db->ping() === false) {
          $this->mySqlConnect();
        }

        $serp_keyword = $this->db->real_escape_string($result->searchString);

        // check if in db if yes skip
        if ( $this->checkIfAlreadyScraped($serp_keyword) == false ) {
          echo 'OLD:' . $serp_keyword . "\n";
          continue;
        } else {
          echo 'NEW: ' . $serp_keyword . "\n";
        }

        $country = $this->country;

        $sql_kw = "('".$serp_keyword."', '".$this->country."', '" . $this->town . "', '" . $this->rs_type . "', '" . $this->dateYMD() . "')";

        $query_kw = 'INSERT INTO
                        ruk_scrape_keywords_local (keyword, language, location, rs_type, timestamp)
                      VALUES
                        '. $sql_kw .' ';
        $this->db->query($query_kw);

        $last_insert_id = $this->db->insert_id;

        $sql_ra = array();
        foreach ($results_kw->details as $resultElement) {
          $pure_host  = str_ireplace('www.', '', parse_url($resultElement->matchLink, PHP_URL_HOST));
          $serp_url   = $this->db->real_escape_string($resultElement->matchLink);
          $pure_host  = $this->db->real_escape_string($pure_host);
          $sql_ra[]   = "('".$last_insert_id."', '".$resultElement->absPos."', '".$serp_url."', '".$pure_host."')";
        }

        $q_string_ra = implode(',', $sql_ra);

         // add to keyword ranking table
        $query_ra = 'INSERT INTO
                        ruk_scrape_rankings_local (id_kw, position, url, hostname)
                      VALUES
                        '. $q_string_ra .' ';

        $res = $this->db->query($query_ra);

        $res;

        if (!empty($this->db->error)) {
          $this->logToFile($this->dateHI() . ' RS LOCAL: DB ERROR 2: ' . $this->db->error);
          //$this->logToFileException($channel_id . "\n" . $serp_keyword . "\n" . $q_string_ra .  "\n\n");
        }

        echo 'WRITE DB '.  "\n";

      }

    } catch (SOAPFault $f) {

      $this->logToFile($this->dateHI() . ' RS LOCAL: SOAP ERROR: ' . $f->faultcode . ' / ' . $f->faultstring);
      $this->logToFile($this->dateHI() . ' RS LOCAL: SOAP ERROR RUN ID: ' . $result->searchRunId);

    }

  }


  public function checkIfAlreadyScraped ($keyword)
  {

    $today = $this->dateYMD();

    $sql = "SELECT
              keyword,
              language,
              location,
              rs_type,
              timestamp,
              id
            FROM
              ruk_scrape_keywords_local
            WHERE
              keyword = '$keyword'
            AND
              language = '$this->country'
            AND
              location = '$this->town'
            AND
              rs_type = '$this->rs_type'
            AND
              timestamp = '$today'";

    $result = $this->db->query($sql);

    if ($result->num_rows > 0) {
      return false;
    } else {
      return true;
    }

  }


  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFile($this->dateHI() . ' RANKING SPY - MYSQL CONNECT ERROR: ' . mysqli_connect_error());
      exit;
    }

  }


  public function dateYMD ()
  {

    $date = new DateTime();

    // START RS DATA COLLECT AT 19:00
    // SIMULATE TOMORROW
    $date->add(new DateInterval('PT6H'));

    $date = $date->getTimestamp();
    return date("Y-m-d", $date);

  }


  public function dateHI ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("h:i / d-m", $date);

  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


  public function logToFileException ($content)
  {

    $fh = fopen($this->logdirex, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


}

new rankingspy_keywordupdate;

?>
