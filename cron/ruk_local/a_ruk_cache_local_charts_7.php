<?php

date_default_timezone_set('CET');

class renderRUKOverviews {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/local/';

  public function __construct () {

    $this->mySqlConnect();

    $this->fetchSets();

    $this->db->close();

  }


  private function fetchSets ()
  {

    $sql = "SELECT id, url, ga_account, gwt_account FROM ruk_project_customers WHERE local = '1' AND id = '160'";

    $result = $this->db->query($sql);

    // GET ALL CUSTOMERS
    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];
      echo 'PRO: ' . $this->project_id . PHP_EOL;

      $this->ga_account  = $row['ga_account'];
      $this->gwt_account = $row['gwt_account'];

      // LOAD CACHE ONCE
      $this->loadCache();
      //$this->loadCache31();

      $sql = "SELECT
                id,
                type,
                rs_type
              FROM
                ruk_local_keyword_sets
              WHERE
                id_customer = '$this->project_id'";

      $result2 = $this->db->query($sql);

      $workload = array();

      // PUT ALL TOGEHTER
      $workload['all'] = 'key';
      while ($row2 = $result2->fetch_assoc()) {
        $workload[$row2['id']] = array($row2['type'], $row2['rs_type']);
      }

      // MAIN LOOP
      foreach ($workload as $id => $data) {

        $this->keyword_setid = $id;
        $this->campaign_type = $data[0];
        $this->rs_type       = $data[1];

        //echo 'SET: ' . $this->keyword_setid . PHP_EOL;
        // generate summary
        $this->doSummarySetdata();
        $this->doChart(7);
        //$this->doChart(31);

      }

    }

  }


  public function loadCache() {
    $file = 'metrics_local_data_'.$this->project_id.'.tmp';
    $this->project_cache = $this->readFile($file);
  }

  public function loadCache31() {
    $file = 'metrics_local_31_data_'.$this->project_id.'.tmp';
    $this->project_cache31 = $this->readFile($file);
  }


  private function doSummarySetdata ()
  {

    $this->cache_filename = 'charts/setdata_'  . $this->project_id . '_' . $this->keyword_setid . '.tmp';

    $this->url_campaign = '';

     // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }

    if ($this->keyword_setid == 'all' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
      $this->selectLocations();
    }

    $this->getRankingData();
    $this->renderView();

  }


  private function doChart ($days)
  {

    $this->days = $days;

    // SCALE 7 or 31 DAYS
    if ($this->days == 31) {
      $this->days   = '_31';
      $this->days_c = 31;
    } else {
      $this->days   = '';
      $this->days_c = 7;
    }

    $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . $this->days . '.tmp';

    $this->url_campaign = '';

    // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }

    if ($this->keyword_setid == 'all' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
      $this->selectLocations();
    }

    $this->getRankingDataChart($days);
    $this->renderViewChart();

  }


  private function renderView ()
  {

    if (!isset($this->rankset['_ts'])) {
      $out = '<table class="table table-normal"><thead><tr><td>Rankings / am</td></tr></thead><tbody><tr><td>Für die Keywords oder URLs in diesem Set wurden keine Rankings gefunden!<br /> Es kann bis zu 24 Stunden dauern bis erste Rankingdaten angezeigt werden.</td></tr></tbody></table>';
    } else {

      $out = '<table class="table table-normal" id="data-table-'.$this->keyword_setid.'">
        <thead>
        <tr>
          <td>Rankings / am</td>';

            $ii = 0;
            // DATES
            foreach ($this->rankset['_ts'] as $k => $ts) {
              $ii++;
              $out .= '<td>'. $this->germanTS($ts) .'</td>';
              if ($ii < count($this->rankset['_ts'])) {
                $out .= '<td>Change</td>';
              }
            }

      $out .= '</tr></thead><tbody>';

      $out .= '<tr><td>Platz 1</td>' . $this->generateCells('rank1') .'</tr>';
      $out .= '<tr><td>Platz 2</td>' . $this->generateCells('rank2') .'</tr>';
      $out .= '<tr><td>Platz 3</td>' . $this->generateCells('rank3') .'</tr>';
      $out .= '<tr><td>Platz 4</td>' . $this->generateCells('rank4') .'</tr>';
      $out .= '<tr><td>Platz 5</td>' . $this->generateCells('rank5') .'</tr>';
      $out .= '<tr><td>Platz 6-10</td>' . $this->generateCells('rank10') . '</tr>';
      $out .= '<tr><td>Summe - Top 10</td>' . $this->generateCellsSUM() . '</tr>';
      $out .= '<tr><td>&#216; - Rankings</td>' . $this->generateCellsAVG() . '</tr>';

      $out .= '
        </tbody>
      </table>';

    }

    $this->writeFileRaw($this->cache_filename, $out);

  }


  private function renderViewChart()
  {

    $chart = array();

    if (isset($this->rankset['_ts'])) {

      $chart['ctitle'] =  $this->days_c . ' Tage OneDex';
      $chart['s_2'] = false;
      $chart['s_3'] = false;
      $chart['s_4'] = false;
      $chart['s_5'] = false;

      // ONEDEX FROM CACHE (ALL KEYWORDS) or LIVE (SELECTED KEYWORSET)
      foreach ($this->dex as $ts => $val) {
        $timestamp = strtotime($ts);
        if ($this->isWeekend($ts) == true) {
          $chart['categories'][] = '<b>' . $this->germanWD($timestamp) . '., ' . date('d.m.', $timestamp) . '</b>';
        } else {
          $chart['categories'][] = $this->germanWD($timestamp) . '., ' . date('d.m.', $timestamp);
        }
        $chart['series_1'][]   = $val;
      }

      if (!empty($this->ga_account)) {
        // ONEDEX FROM CACHE (ALL KEYWORDS) or LIVE (SELECTED KEYWORSET)
        foreach ($this->dex as $ts => $val) {

          if (!isset($this->rankset['_tr'][$ts])) {
            $chart['series_2'][] = null;
          } else {
            $chart['series_2'][] = $this->rankset['_tr'][$ts];
            $chart['s_2'] = true;
          }

          if (!isset($this->rankset['_vi'][$ts])) {
            $chart['series_3'][] = null;
          } else {
            $chart['series_3'][] = $this->rankset['_vi'][$ts];
            $chart['s_3'] = true;
          }

          if (!isset($this->rankset['_en'][$ts])) {
            $chart['series_5'][] = null;
          } else {
            $chart['series_5'][] = $this->rankset['_en'][$ts];
            $chart['s_5'] = true;
          }

        }

      }

      if ($this->gwt_account == '1') {
        foreach ($this->dex as $ts => $val) {
          if (!isset($this->rankset['_cl'][$ts])) {
            $chart['series_4'][] = null;
          } else {
            $chart['series_4'][] = $this->rankset['_cl'][$ts];
            $chart['s_4'] = true;
          }
        }
      }

      // get first available timestamp
      reset($this->rankset['_ov']);
      $first_ts = key($this->rankset['_ov']);

      $q = 0;
      $rank10 = 0;
      $rank20 = 0;
      $rank30 = 0;

      $q      = round($this->rankset['_ov'][$first_ts]['avg']/$this->rankset['_ov'][$first_ts]['divider'],0);

      if (isset($this->rankset['_ov'][$first_ts]['rank10'])){
        $rank10 = 0 + $this->rankset['_ov'][$first_ts]['rank10'];
      } else {
        $rank10 = 0;
      }

      if (isset($this->rankset['_ov'][$first_ts]['rank20'])){
        $rank20 = $rank10 + $this->rankset['_ov'][$first_ts]['rank20'];
      } else {
        $rank20 = $rank10;
      }

      if (isset($this->rankset['_ov'][$first_ts]['rank30'])){
        $rank30 = $rank10 + $rank20 + $this->rankset['_ov'][$first_ts]['rank30'];
      } else {
        $rank30 = $rank10 + $rank20;
      }

      $res = array('top10' => $rank10, 'top20' => $rank20, 'top30' => $rank30, 'q' => $q, 'chart' => $chart);

    } else {

      $chart['categories'][] = $this->dateYMD();
      $chart['series'][]     = 100;
      $res = array('top10' => 0, 'top20' => 0, 'top30' => 0, 'q' => 0, 'chart' => $chart);

    }


    $this->writeFile($this->cache_filename, $res);

  }


  private function generateCellsSUM ()
  {

    $out = '';

    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {

      $ii++;
      if (!isset($this->rankset['_ov'][$ts]['sum'])) {
        $out .= '<td>0</td>';
        if ($ii < count($this->rankset['_ts'])) {
          $out .= '<td></td>';
        }
      }

      if (!isset($this->rankset['_ov'][$ts])) {
        continue;
      }

      foreach ($this->rankset['_ov'][$ts] as $key => $ranks) {

        if ($key == 'sum') {

          $yesterday = key($this->rankset['_ts']);
          next($this->rankset['_ts']);
          $ranktoday = $ranks;

          $change = '';

          if (isset($this->rankset['_ov'][$yesterday]['sum'])) {
            $rankyesterday = $this->rankset['_ov'][$yesterday]['sum'];
            $change =  $this->checkDifferenceBetweenRankingDays($ranktoday, $rankyesterday);
          }

          $out .= '<td>'. $ranktoday . '</td>';
          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change.'</td>';
          }

        }

      }

    }

    return $out;

  }


  private function generateCellsAVG ()
  {

    $out = '';

    $ii = 0;
    foreach ($this->rankset['_ts'] as $k => $ts) {
      $ii++;
      if (!isset($this->rankset['_ov'][$ts]['avg'])) {
        $out .= '<td>0</td>';
        if ($ii < count($this->rankset['_ts'])) {
          $out .= '<td></td>';
        }
      }

      if (!isset($this->rankset['_ov'][$ts])) {
        continue;
      }

      foreach ($this->rankset['_ov'][$ts] as $key => $ranks) {

        if ($key == 'avg') {

          $yesterday = key($this->rankset['_ts']);
          next($this->rankset['_ts']);
          $ranktoday = round($ranks/$this->rankset['_ov'][$ts]['divider'],0);

          $change = '';
          if (isset($this->rankset['_ov'][$yesterday]['avg']) && isset($this->rankset['_ov'][$yesterday]['divider'])) {
            $rankyesterday = round($this->rankset['_ov'][$yesterday]['avg'] / $this->rankset['_ov'][$yesterday]['divider'] ,0);
            $change =  $this->checkDifferenceBetweenRankingDaysReverse($ranktoday, $rankyesterday);
          }

          $out .= '<td>Rang '. $ranktoday . '</td>';
          if ($ii < count($this->rankset['_ts'])) {
            $out .= '<td>'.$change.'</td>';
          }

        }

      }
    }

    return $out;

  }


  private function generateCells ($rank)
  {

    $out = '';
    $i   = 0;
    $ii  = 0;

    foreach ($this->rankset['_ts'] as $k => $ts) {

      $ii++;

      $yesterday = key($this->rankset['_ts']);
      next($this->rankset['_ts']);

      if ($i < 6) {
        if (!isset($this->rankset['_ov'][$ts][$rank])) {$this->rankset['_ov'][$ts][$rank] = 0;}
        if (!isset($this->rankset['_ov'][$yesterday][$rank])) {$this->rankset['_ov'][$yesterday][$rank] = 0;}
        $change = $this->checkDifferenceBetweenRankingDays($this->rankset['_ov'][$ts][$rank], $this->rankset['_ov'][$yesterday][$rank]);
      } else {
        $change = '';
      }
      $out .= '<td>' . $this->rankset['_ov'][$ts][$rank] . '</td>';
      if ($ii < count($this->rankset['_ts'])) {
        $out .= '<td>'.$change.'</td>';
      }

      $i++;
    }

    return $out;

  }


  private function selectKeywordSet ()
  {

    $this->keywords = array();

    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $sql = "SELECT keyword, location FROM ruk_local_keywords WHERE id_kw_set = '$this->keyword_setid'";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
      $this->keywords_loc[md5($row['keyword'] . $row['location'])] = array ($row['keyword'], $row['location']);
    }

  }

  private function selectLocations ()
  {

    $this->locations = array();

    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $sql = "SELECT location FROM ruk_local_sets2rs WHERE id_kw_set = '$this->keyword_setid'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->locations[$row['location']] = $row['location'];
    }

  }


  private function selectAllKeywordSets ()
  {

    $this->keywords = array();

    $selected_project = $this->project_id;

    // get project_keyword_sets with project ID
    // get all keywords
    // get url
    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword,
                   b.location    AS location
            FROM ruk_local_keyword_sets a
              LEFT JOIN ruk_local_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords_loc[md5($row['keyword'] . $row['location'])] = array ($row['keyword'], $row['location']);
      $this->keywords[] = $row['keyword'];
    }

  }


  private function getRankingData()
  {

    $rows = $this->project_cache;
    $rows = array_reverse($rows);

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
          unset($this->url_campaign[$key]);
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }
// COMPARE OPTIONS

    $this->rankset = array();

    foreach ($rows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        if (isset($this->locations) && !empty($this->locations)) {
          if (!isset($this->locations[$row['l']])) {
            continue;
          }
        }

        // match type
        if ($this->rs_type != $row['rs']) {
          continue;
        }

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {

          $this->summarySetdataMerge($row['t'], 'avg', $row['p']);
          $this->summarySetdataMerge($row['t'], 'divider', 1);

          if ($row['p'] >= 1 && $row['p'] <= 10) {
            $this->summarySetdataMerge($row['t'], 'sum', 1);
          }
          if ($row['p'] == 1) {
            $this->summarySetdataMerge($row['t'], 'rank1', 1);
          }
          if ($row['p'] == 2) {
            $this->summarySetdataMerge($row['t'], 'rank2', 1);
          }
          if ($row['p'] == 3) {
            $this->summarySetdataMerge($row['t'], 'rank3', 1);
          }
          if ($row['p'] == 4) {
            $this->summarySetdataMerge($row['t'], 'rank4', 1);
          }
          if ($row['p'] == 5) {
            $this->summarySetdataMerge($row['t'], 'rank5', 1);
          }
          if ($row['p'] >= 6 && $row['p'] <= 10) {
            $this->summarySetdataMerge($row['t'], 'rank10', 1);
          }

        }

      }

    }

  }


  private function getRankingDataChart($days)
  {

    if ($days == 31) {
      $rows = $this->project_cache31;
    } else {
      $rows = $this->project_cache;
    }

    $this->googleadwords = $this->getAdwordsDataLocal($this->keywords_loc);

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $this->rankset = array();
    $this->dex = array();

// COMPARE OPTIONS
    $dupl = array();
    $duplga = array();
    foreach ($rows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        if (isset($this->locations) && !empty($this->locations)) {
          if (!isset($this->locations[$row['l']])) {
            continue;
          }
        }
        // match type
        if ($this->rs_type != $row['rs']) {
          continue;
        }

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

    // DEX LIVE
        if (isset($this->googleadwords[$row['k']][$row['l']][3])) {
          $opi = $this->googleadwords[$row['k']][$row['l']][3];
        } else {
          $opi = 0;
        }

        // create unique hash (ranking + day) so that only the first ranking gets recognized
        $hash = md5($row['t'] . $row['k'] . $row['l']);
        if (!isset($dupl[$hash])) {
          if (isset($this->dex[$row['t']])) {
            $add_dex = round($this->dex[$row['t']], 5);
          } else {
            $add_dex = 0;
          }
          $this->dex[$row['t']] = $this->calcOneDex($row['p'], $opi) + $add_dex;
          $dupl[$hash] = $hash;
        }
    // DEX LIVE


    // TRANSACTION REV
        if (isset($row['tr']) && $row['tr'] != '0') {
          $hashga = md5($row['t'] . $row['u']. $row['l']);
          if (!isset($duplga[$hashga])) {
            if (isset($this->rankset['_tr'][$row['t']])) {
              $this->rankset['_tr'][$row['t']] = floatval($row['tr']) + $this->rankset['_tr'][$row['t']];
            } else {
              $this->rankset['_tr'][$row['t']] = floatval($row['tr']);
            }
            $duplga[$hashga] = $hashga;
          }
        }
    // TRANSACTION REV

    // TRAFFIC
        if (isset($row['pv']) && $row['pv'] != '0') {
          $hashgax = md5($row['t'] . $row['u']. $row['l']);
          if (!isset($duplgax[$hashgax])) {
            if (isset($this->rankset['_vi'][$row['t']])) {
              $this->rankset['_vi'][$row['t']] = floatval($row['pv']) + $this->rankset['_vi'][$row['t']];
            } else {
              $this->rankset['_vi'][$row['t']] = floatval($row['pv']);
            }
            $duplgax[$hashgax] = $hashgax;
          }
        }
    // TRAFFIC


    // ENTRANCES
        if (isset($row['en']) && $row['en'] != '0') {
          $hashgaen = md5($row['t'] . $row['u']. $row['l']);
          if (!isset($duplgaen[$hashgaen])) {
            if (isset($this->rankset['_en'][$row['t']])) {
              $this->rankset['_en'][$row['t']] = floatval($row['en']) + $this->rankset['_en'][$row['t']];
            } else {
              $this->rankset['_en'][$row['t']] = floatval($row['en']);
            }
          }
        }
    // ENTRANCES

    // GWT CLICKS
        if (isset($row['cl']) && $row['cl'] != '0') {
          $hashgwt = md5($row['t'] . $row['u']. $row['l']);
          if (!isset($duplgwt[$hashgwt])) {
            if (isset($this->rankset['_cl'][$row['t']])) {
              $this->rankset['_cl'][$row['t']] = $row['cl'] + $this->rankset['_cl'][$row['t']];
            } else {
              $this->rankset['_cl'][$row['t']] = $row['cl'];
            }
            $duplgwt[$hashgwt] = $hashgwt;
          }
        }
    // GWT CLICKS

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {

          $this->summarySetdataMerge($row['t'], 'avg', $row['p']);
          $this->summarySetdataMerge($row['t'], 'divider', 1);

          if ($row['p'] >= 1 && $row['p'] <= 10) {
            $this->summarySetdataMerge($row['t'], 'rank10', 1);
          }
          if ($row['p'] > 10 && $row['p'] <= 20) {
            $this->summarySetdataMerge($row['t'], 'rank20', 1);
          }
          if ($row['p'] > 20 && $row['p'] <= 30) {
            $this->summarySetdataMerge($row['t'], 'rank30', 1);
          }

        }

      }

    }

  }


  public function summarySetdataMerge($ts, $type, $add)
  {

    if (isset($this->rankset['_ov'][$ts][$type])) {
      $this->rankset['_ov'][$ts][$type] = $this->rankset['_ov'][$ts][$type] + $add;
    } else {
      $this->rankset['_ov'][$ts][$type] = $add;
    }

  }


  // FETCH URLS in KEYWORD CAMPAIGN
  public function fetchURLSet($kw_set_id)
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }


  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  // PROPER GERMAN TS
  public function germanWD ($ts)
  {

    $day[0] = 'So';
    $day[1] = 'Mo';
    $day[2] = 'Di';
    $day[3] = 'Mi';
    $day[4] = 'Do';
    $day[5] = 'Fr';
    $day[6] = 'Sa';

    $dayn = date('w', $ts);

    return $day[$dayn];

  }

  // PROPER GERMAN TS
  public function germanTS ($ts)
  {

    $day[0] = 'Sonntag';
    $day[1] = 'Montag';
    $day[2] = 'Dienstag';
    $day[3] = 'Mittwoch';
    $day[4] = 'Donnerstag';
    $day[5] = 'Freitag';
    $day[6] = 'Samstag';

    $dayn = date('w', strtotime($ts));

    return $day[$dayn] . ', ' . date('d.m.Y', strtotime($ts));

  }


  public function checkDifferenceBetweenRankingDaysReverse ($today, $yesterday)
  {

    if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+' .  $change . '</small>';
    } else if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      return ' <i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-' .  $change . '</small>';
    }

  }


  public function checkDifferenceBetweenRankingDays ($today, $yesterday, $symbol = true)
  {

    $out = '';

    if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      if ($symbol === true) {$out = '<i class="status-success icon-circle-arrow-up"></i> ';} else {$perc = '';}
      $out .= '<small class="status-success">+' . number_format($change, 0, ',', '.') . '</small>';
    } else if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      if ($symbol === true) {$out = '<i class="status-error icon-circle-arrow-down"></i> ';} else {$perc = '';}
      $out .= '<small class="status-error">-' .  number_format($change, 0, ',', '.') . '</small>';
    }

    return $out;

  }


  // DEX
  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 3, 0);

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  public function getAdwordsDataLocal ($keywords)
  {

    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $googleadwords = array();

    foreach ($keywords as $key => $set) {

      $keyword  = $set[0];
      $location = $set[1];

      $sql = "SELECT
                keyword,
                rs_loc_id,
                searchvolume,
                cpc,
                competition,
                opi
              FROM
                gen_keywords_local
              WHERE
                keyword = '$keyword'
              AND
                rs_loc_id = '$location'";

      $res = $this->db->query($sql);

      while ($row = $res->fetch_assoc()) {
       $googleadwords[$row['keyword']][$row['rs_loc_id']] = array ($row['searchvolume'], $row['cpc'], $row['competition'], $row['opi']);
      }

    }

    return $googleadwords;

  }


  public function writeFileRaw ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }

  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }

  public function isWeekend ($date) {
    $weekDay = date('w', strtotime($date));
    if ($weekDay == 0 || $weekDay == 6) {
      return true;
    }
    return false;
  }


}

new renderRUKOverviews;

?>
