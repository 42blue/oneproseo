<?php

date_default_timezone_set('CET');

  // uaKPBAUKDfajBAzIGjpCMjbsmFzZtKIC
  // http://api.sistrix.net/domain.sichtbarkeitsindex?api_key=5rHzgQjjrpk2qBWdmdA5gp6yBwdXsZaR&date=2014-10-13&domain=advertising.de/seo
  // http://api.sistrix.net/domain.sichtbarkeitsindex?api_key=5rHzgQjjrpk2qBWdmdA5gp6yBwdXsZaR&domain=


class fetchSistrix {

  private $sendmail = false;
  private $logdir   = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  private $old_data = array();

  public function __construct () {

    $is_sunday = (date('N', strtotime($this->dateYMD())));

    if ($is_sunday != 7) {
      $this->logToFile($this->dateHIS() . ' SISTRIX DATA ERROR NOT SUNDAY ');
      exit;
    }

    $this->mySqlConnect();

    $this->fetchCustomerUrls();

    $this->getSistrixData('desktop');
    $this->getSistrixData('mobile');    

    $this->db->close();

  }



  private function fetchCustomerUrls ()
  {

  	// ONEPROSEO PROJECTS
    $sql = "SELECT country, url, competition from ruk_project_customers";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $host    = $this->pureHostName($row['url']);
      $country = substr($row['country'], 0, 2);
      $hash    = $country . $host;
      $this->all_hosts[$hash] = array($host, $country);
    }


  	// PROJECT COMPETITORS
    $sql = "SELECT country, competition from ruk_project_customers";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
			$comp    = unserialize($row['competition']);
			$country = substr($row['country'], 0, 2);
			if (!empty($comp)) {
				foreach ($comp as $url) {
					$host    = $this->pureHostName($url);
	      	$hash    = $country . $host;
	      	$this->all_hosts[$hash] = array($host, $country);
				}
			}
    }

  	// SET COMPETITORS
    $sql = "SELECT 
			    	  a.competition AS competition,
			    	  b.country  	  AS country	    	  
			    	FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_customers b
                ON a.id_customer = b.id";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
			$comp    = unserialize($row['competition']);
			$country = substr($row['country'], 0, 2);
			if (!empty($comp)) {
				foreach ($comp as $url) {
					$host    = $this->pureHostName($url);
	      	$hash    = $country . $host;
	      	$this->all_hosts[$hash] = array($host, $country);
				}
			}
    }

  }




  private function getSistrixData ($type)
  {

  	$calladd = ''; 
  	if ($type == 'mobile') {
  		$type    = 'mobile';
  		$calladd = '&mobile=true'; 
  	}

    $timestamp = $this->dateYMD();

    foreach ($this->all_hosts as $hash => $data) {

			$host    = $data[0];
    	$country = $data[1];

			echo $host . ' - ' . $country . ' - ' . $type . ' - ' . $timestamp;
			echo PHP_EOL;


      $ctx     = stream_context_create(array('http'=>array('timeout' => 3)));
      $si      = file_get_contents('https://api.sistrix.com/domain.sichtbarkeitsindex?api_key=uaKPBAUKDfajBAzIGjpCMjbsmFzZtKIC'.$calladd.'&country='.$country.'&domain=' . $host .'&date=' . $timestamp, false, $ctx);

      $xml = simplexml_load_string($si);

      if (empty($xml) or $xml == FALSE ) {
      	var_dump($data);
				$this->logToFile($this->dateHIS() . ' SISTRIX XML ERROR: ' . $host . ' - ' . $country);
      }

      foreach ($xml->answer->sichtbarkeitsindex as $item) {

        $score = $item['value'];

        $sql_ra = "('".$host."', '".$country."', '".$score."', '".$timestamp."', '".$type."')";

        $query_ra = 'INSERT INTO
                        sistrix (host, country, score, timestamp, type)
                      VALUES
                        '. $sql_ra .' ';

        $call = $this->db->query($query_ra);

        if (!empty($this->db->error)) {
          $this->logToFile($this->dateHIS() . ' SISTRIX DATA ERROR: ' . $host . ' - ' . $country . ' - ' . $this->db->error);
				}

      }

    }

  }


  public function pureHostName ($url) {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


  public function mySqlConnect () {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }


  public function dateYMD () {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);

  }


  public function dateHIS () {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i:s", $date);

  }


  public function isJson($string) {

   json_decode($string);
   return (json_last_error() == JSON_ERROR_NONE);

  }


  public function logToFile ($content) {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


  public function reconMySql () {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }



}

new fetchSistrix;

?>
