<?php
require_once __DIR__ . "/../../onepro_library/onepro.conf.php";
onepro_setDebugMode(1);
#onepro_debugDBQueries(1);

$config = OnePro_Config::getInstance();

if($config->getEnvironment() == "main" && strpos(ONEPRO_BASE_PATH, "dev-")) $config->setEnvironment("dev");
else if($config->getEnvironment() == "main") $config->setEnvironment("prod");

OnePro_DB::$connect_options = array(MYSQLI_OPT_CONNECT_TIMEOUT => 30, MYSQLI_OPT_LOCAL_INFILE => 1);
onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");

require_once 'libraries/PHPExcel/PHPExcel/IOFactory.php';

$sCurrentDirectory = onepro_getDirName(__FILE__);
$sDataDirectory = $sCurrentDirectory . "/data";
$sCsvDirectory = $sCurrentDirectory . "/csv";
$aFiles = array();

// get import files:
$rDirHandler  = opendir($sDataDirectory);
while (false !== ($filename = readdir($rDirHandler))) 
{
	$pathinfo = pathinfo($filename);
	if( (substr($pathinfo['basename'], 0, 1) == ".") || !isset($pathinfo['extension'])) {
		continue;
	}
	
	if(!isset($pathinfo['extension']) || $pathinfo['extension'] != "xlsx") {
		echo "\nWARNING: Wrong file extension for file " . $filename;
	}
	else if(!isset($pathinfo['filename']) || (!stristr($pathinfo['filename'], 'PACKAGE') && !stristr($pathinfo['filename'], 'ONLY')) ) {
		echo "\nWARNING: Wrong filename for file " . $filename;
	}
	else if(!preg_match('/^\d{6}.*$/', $pathinfo['filename'])) {
		echo "\nWARNING: Wrong or missing date format for file " . $filename;
	}
	else {
		$aFiles[] = $filename;
	}
}

if(count($aFiles) == 0) {
	echo "\nINFO: No files to import! Please put your files into subdir 'data' and make sure that the filename provides all information needed: YYMMDD_*_[PACKAGE|ONLY]_*.xlsx e.g.: 150215_30_67_TOP_HOTEL_PACKAGED.xlsx";
	exit();
}

$aImportedFiles = OnePro_DB::queryOneColumn("filename", "SELECT * FROM `hdb_tophotel_files`");

#print_r($aImportedFiles);
#exit;

// perform import:
foreach($aFiles as $sFilenameXlsx) 
{
	echo "\nPROCESSING: Importing file " . $sFilenameXlsx . " ... ";

	if(in_array($sFilenameXlsx, $aImportedFiles)) 
	{
		echo "file already imported (please check in the database)";
		continue;
	}

	$sFilenameCsv = str_replace('.xlsx', '.csv', $sFilenameXlsx);
	
	$excel = PHPExcel_IOFactory::load($sDataDirectory . "/" . $sFilenameXlsx);
	$writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');
	$writer->setDelimiter(";");
	$writer->setEnclosure("\"");
	$writer->save($sCsvDirectory . "/" . $sFilenameCsv);
	
	$sDate = "20" . substr($sFilenameXlsx, 0, 2)."-".substr($sFilenameXlsx, 2, 2)."-".substr($sFilenameXlsx, 4, 2);
	if(stristr($sFilenameXlsx, "ONLY")) {
		$sType=1;
	}
	else {
		$sType=0;
	}

	/*
	 * Performance Variante über LOAD INFILE
	 */ 
	// truncate temp table
	OnePro_DB::query("TRUNCATE TABLE hdb_tophotels_tmp;");
	if(OnePro_DB_Handler::$status === false) {
		echo "\n\nERROR: Table truncation failed.\n";
		exit();
	}
	
	// load raw data into temp table
	#$sQuery = "LOAD DATA LOCAL INFILE '{" . $sCurrentDirectory . "/" . $sCsvDirectory . "/" . $sFilenameCsv . "}'
	$sQuery = "LOAD DATA LOCAL INFILE '" . $sCsvDirectory . "/" . $sFilenameCsv . "'
				INTO TABLE `hdb_tophotels_tmp`
				FIELDS TERMINATED BY ';'
				ENCLOSED BY '\"'
				LINES TERMINATED BY '\n'
				IGNORE 1 LINES
				( @col1
				, @col2
				, @col3
				, @col4
				, @col5
				, @col6
				, @col7
				, @col8
				, @col9
				, @col10
				, @category
				, @name
				, @location
				, @province
				, @country
				, @col16
				, @iff_code
				, @giata_code
				, @col19
				, @col20
				, @col21
				, @col22
				, @col23
				, @col24
				, @col25
				, @1_pppc
				, @1_avail
				, @1_tt_ba
				, @col29
				, @2_pppc
				, @2_avail
				, @2_tt_ba
				, @col33
				, @3_pppc
				, @3_avail
				, @3_tt_ba
				, @total_pppc
				, @total_avail
				, @total_tt_ba
				, @col40
				)
				SET type = $sType
				, name = @name
				, category = @category
				, giata_code = @giata_code
				, iff_code = @iff_code
				, iff_country = @country
				, iff_province = @province
				, iff_location = @location
				, date = '$sDate'
				, pppc_1 = @1_pppc
				, avail_1 = @1_avail
				, tt_ba_1 = @1_tt_ba
				, pppc_2 = @2_pppc
				, avail_2 = @2_avail
				, tt_ba_2 = @2_tt_ba
				, pppc_3 = @3_pppc
				, avail_3 = @3_avail
				, tt_ba_3 = @3_tt_ba
				, pppc_total = @total_pppc
				, avail_total = @total_avail
				, tt_ba_total = @total_tt_ba
				, update_time = UNIX_TIMESTAMP() ";
	
	OnePro_DB::query($sQuery);
	if(OnePro_DB_Handler::$status === false) {
		echo "\n\nERROR: Loading data failed.\n";
		exit();
	}
	
	// update hotels:
	$sQuery = "INSERT INTO hdb_tophotels (name, category, giata_code, iff_code, iff_country, iff_province, iff_location) 
				SELECT name, category, giata_code, iff_code, iff_country, iff_province, iff_location 
				FROM hdb_tophotels_tmp t WHERE iff_code != 0 
					ON DUPLICATE KEY UPDATE
						name = t.name,
						category = t.category,
						giata_code = t.giata_code,
						iff_code = t.iff_code,
						iff_country = t.iff_country,
						iff_province = t.iff_province,
						iff_location = t.iff_location
					";
	
	OnePro_DB::query($sQuery);
	if(OnePro_DB_Handler::$status === false) {
		echo "\n\nERROR: Update tophotels failed.\n";
		exit();
	}
	
	// update stats:
	$sQuery = "INSERT INTO hdb_tophotel_stats (type, iff_code, date, pppc_1, avail_1, tt_ba_1, pppc_2, avail_2, tt_ba_2, pppc_3, avail_3, tt_ba_3, pppc_total, avail_total, tt_ba_total) 
				SELECT type, iff_code, date, pppc_1, avail_1, tt_ba_1, pppc_2, avail_2, tt_ba_2, pppc_3, avail_3, tt_ba_3, pppc_total, avail_total, tt_ba_total 
				FROM hdb_tophotels_tmp t WHERE iff_code != 0 
					ON DUPLICATE KEY UPDATE
						pppc_1 = t.pppc_1, 
						avail_1 = t.avail_1, 
						tt_ba_1 = t.tt_ba_1, 
						pppc_2 = t.pppc_2, 
						avail_2 = t.avail_2, 
						tt_ba_2 = t.tt_ba_2, 
						pppc_3 = t.pppc_3, 
						avail_3 = t.avail_3, 
						tt_ba_3 = t.tt_ba_3, 
						pppc_total = t.pppc_total, 
						avail_total = t.avail_total, 
						tt_ba_total = t.tt_ba_total
					";
	
	OnePro_DB::query($sQuery);
	if(OnePro_DB_Handler::$status === false) {
		echo "\n\nERROR: Update tophotels stats failed.\n";
		exit();
	}
	
	OnePro_DB::insertIgnore("hdb_tophotel_files", array("filename" => $sFilenameXlsx, "date_imported" => date("Y-m-d H:i:s")));
	
	echo "successful!";
}

echo "\n\nimported successfully!\n";
