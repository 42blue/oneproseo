<?php

/*
Alle Clicks aus Searchconsole pro Kunde mal durchschnittlicher CPC aus Adwords nur für vorhandene Keywords
Einmal im Monat und täglich aufsummiert

// 269

*/

require_once dirname(__FILE__) . '/ruk.class.php';

class ruk_media_value extends ruk {

  public  $logdir       = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct ($argv) {

    if (!isset($argv[1])) {
      exit;
    }

    $this->job = $argv[1];

    $this->mySqlConnect();

    if ($this->job == 'monthly') {
      // EINZELNE SETS
      $this->startdate = date("Y-m-d", strtotime("first day of previous month"));
      $this->enddate   = date("Y-m-d", strtotime("last day of previous month"));
      $this->selectProjects();
      $this->logToFile('MEDIAVALUE START MONTHLY');

    } else if ($this->job == 'daily') {
      // EINZELNE SETS
      $this->startdate = date("Y-m-d", strtotime("first day of this month"));
      $this->enddate   = date("Y-m-d", strtotime("today"));
      $this->selectProjects();
      $this->logToFile('MEDIAVALUE START DAILY');

    } else if ($this->job == 'monthlyall') {
      // ALLE KEYWORDS
      $this->startdate = date("Y-m-d", strtotime("first day of previous month"));
      $this->enddate   = date("Y-m-d", strtotime("last day of previous month"));
      $this->selectCustomers();
      $this->logToFile('MEDIAVALUE START MONTHLYALL');

    } else if ($this->job == 'dailyall') {
      // ALLE KEYWORDS
      $this->startdate = date("Y-m-d", strtotime("first day of this month"));
      $this->enddate   = date("Y-m-d", strtotime("today"));
      $this->selectCustomers();
      $this->logToFile('MEDIAVALUE START MONTHLYALL');

    } else {
      exit;
    }

    foreach ($this->projects_sets as $data) {

      $setid    = $data[1];
      $hostname = str_ireplace('www.', '', parse_url($data[2], PHP_URL_HOST));

      $this->gwthostid  = $this->selectHostnameId($hostname);
      $this->customerid = $data[0];

      $this->doUpdate($setid);

    }

    $this->logToFile('MEDIAVALUE DONE');

  }

  private function doUpdate ($setid) {


    if ($this->job == 'monthly') {
      $this->selectKeywordSet($setid);
    } else if ($this->job == 'daily') {
      $this->selectKeywordSet($setid);
    } else if ($this->job == 'monthlyall') {
      $this->selectAllKeywordSets();
      $setid = $this->customerid;
    } else if ($this->job == 'dailyall') {
      $this->selectAllKeywordSets();
      $setid = $this->customerid;
    }


    echo 'SETID: ' . $setid;
    echo PHP_EOL;
    echo 'AMOUNT: ' . count ($this->keywords);
    echo PHP_EOL;


    $this->fetchKeywordClicks();
    $this->fetchAdwordsCPC();

    $campaignval = 0;
    $value       = 0;
    $multiplier  = 0;

    foreach ($this->keywords_cpc as $keyword => $cpc) {

      if (isset($this->keywords_clicks[$keyword])) {
        $multiplier   = $this->clickMultiplier($this->keywords_clicks[$keyword]['pos']);
        $value       = $cpc * $this->keywords_clicks[$keyword]['clicks'] * $multiplier;
        $campaignval = $campaignval + $value;
      }

    }

    $this->logToFile('MEDIAVALUE SETID: ' . $setid . ', AMOUNT: ' . count ($this->keywords) . ', VAL: ' . $campaignval);

    $this->reconMySql();

    if ($this->job == 'monthly') {
      $sql = "UPDATE ruk_project_keyword_sets SET value = $campaignval WHERE id = '$setid'";
    } else if ($this->job == 'daily') {
      $sql = "UPDATE ruk_project_keyword_sets SET value_now = $campaignval WHERE id = '$setid'";
    } else if ($this->job == 'monthlyall') {
      $sql = "UPDATE ruk_project_customers SET value = $campaignval WHERE id = '$this->customerid'";
    } else if ($this->job == 'dailyall') {
      $sql = "UPDATE ruk_project_customers SET value_now = $campaignval WHERE id = '$this->customerid'";
    }

    $this->db->query($sql);

  }

  private function fetchAdwordsCPC () {

    $this->keywords_cpc = array();

    // QUERY RUK DB
    $result = $this->fetchAdwordsDB($this->keywords);
    foreach ($result as $k => $v) {
      $this->keywords_cpc[$k] = $v;
    }

  }

  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  private function fetchAdwordsDB ($keywords)
  {

    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $this->reconMySql();

    $sql = "SELECT
              keyword,
              cpc
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $googleadwords = array();

    while ($row = $res->fetch_assoc()) {
     $googleadwords[$row['keyword']] =  $row['cpc'];
    }

    return $googleadwords;

  }


  private function fetchKeywordClicks ()
  {

    $this->keywords_clicks  = array();
    $comma_separated_kws    = implode('", "', $this->keywords);
    $comma_separated_kws    = '"' . $comma_separated_kws . '"';

    $sql = "SELECT
              query,
              AVG(position_1) AS position,
              SUM(clicks) AS sum_clicks
            FROM
              gwt_data
            WHERE
              hostname_id = $this->gwthostid
            AND
              query IN ($comma_separated_kws)
            AND
              timestamp >= '$this->startdate'
            AND
              timestamp <= '$this->enddate'
            GROUP BY
              query";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if ($row['sum_clicks'] > 0) {
        $this->keywords_clicks[$row['query']] = array('clicks' => $row['sum_clicks'], 'pos' => $row['position']);
      }
    }

  }


  private function selectProjects ()
  {

    $this->projects_sets = array();

    $sql = "SELECT
                  a.id   AS customerid,
                  a.name AS customername,
                  a.url  AS customerurl,
                  b.id   AS kwsetid
                FROM ruk_project_customers a
                  LEFT JOIN ruk_project_keyword_sets b
                    ON b.id_customer = a.id
                    WHERE a.gwt_account = 1
                AND b.type = 'key'";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->projects_sets[] = array($row['customerid'], $row['kwsetid'], $row['customerurl']);
    }

  }


  private function selectCustomers()
  {

    $this->projects_sets = array();

    $sql = "SELECT
                  id   AS customerid,
                  name AS customername,
                  url  AS customerurl
                FROM ruk_project_customers
                WHERE gwt_account = 1";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->projects_sets[] = array($row['customerid'], false, $row['customerurl']);
    }

  }

  private function selectKeywordSet ($selected_keyword_set)
  {

    $this->keywords = array();

    $sql = "SELECT keyword FROM ruk_project_keywords WHERE id_kw_set = '".$selected_keyword_set."'";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function selectAllKeywordSets ()
  {

    $this->keywords = array();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword,
                   a.rs_type     AS rs_type,
                   c.url         AS url,
                   c.id          AS customerid,
                   c.ga_account  AS ga_account,
                   c.name        AS customername,
                   c.country     AS country
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$this->customerid."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {

      if (!empty($row['keyword'])) {
        $this->keywords[$row['keyword']] = $row['keyword'];
      }

    }

  }

  private function selectHostnameId ($host)
  {

    $hostid = '';

    $sql = "SELECT id FROM gwt_hostnames WHERE hostname = '".$host."'";
    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $hostid = $row['id'];
    }

   return $hostid;

  }

}

new ruk_media_value ($argv);

?>
