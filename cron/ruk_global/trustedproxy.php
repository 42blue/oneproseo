<?php

/*
Fallback falls RS keine Daten liefern kann
*/

require_once dirname(__FILE__) . '/ruk.class.php';

class trustedproxy_keywordupdate extends ruk {

  public  $logdir       = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_rs.txt';
  public  $logdirex     = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/exceptionlog.txt';
  public  $mailalertto  = 'matthias.hotz@gmail.com';
  public  $mailsubject  = 'TrustedProxy ALERT';
  private $service      = 'https://oneproseo.advertising.de/oneproapi/trustedproxy/tpscraper_fallback.php';
  private $timeoffset   = 'PT1H';
  private $maxrequests  = 50;

  public function __construct ()
  {

    $this->mySqlConnect();

    $this->logToFile('TP START - SYSTEM TIME: ' . $this->dateYMD());

    $scrape = $this->getOpenKeywords();
    $this->startScrape($scrape);

    $this->db->close();

  }


  private function startScrape ($scrape)
  {

    if (count($scrape) < 1) {
      $this->logToFile('TP DONE - SYSTEM TIME: ' . $this->dateYMD());
      exit;
    }

   $this->logToFile('TP START LOOP');

   $this->loopSearchRuns($scrape);

  }


  private function loopSearchRuns ($scrape)
  {

    $this->logToFile('TP START - AMOUNT: ' . count($scrape));

    $dataset = (array_chunk($scrape, $this->maxrequests));

    foreach ($dataset as $no => $payload) {
      /*
      $payload   = array();
      $payload[] = array('klositz', 'ch-fr', 'mobile') ;
      */

      $payload   = base64_encode(json_encode($payload));
      $response  = $this->scrapeCurl($payload);

      $this->resObj = json_decode($response);

      if (empty($response)) {
        $this->logToFile('TP EMPTY');
      } else if (!is_object($this->resObj)) {
        $this->logToFile('TP NO OBJECT');
      } else {
        $this->processResult();
      }

    }

  }



  private function processResult ()
  {

    if (!isset($this->resObj)) {
      return;
    }

    foreach ($this->resObj as $k => $result) {

      if (!isset($result->keyword)) {
        continue;
      }

      echo $result->keyword . PHP_EOL;

      $serp_keyword = trim($result->keyword);
      $serp_lang    = $result->lang;
      $serp_type    = $result->type;

      $lang = $serp_type . '_' . $serp_lang;

      if($serp_type == 'desktop') {
        $lang = $serp_lang;
      }

      // check if in db if yes skip
      if ($this->checkIfAlreadyScraped($serp_keyword, $lang) == false) {
        return;
      }
      $serp_keyword = $this->db->real_escape_string($result->keyword);

      $sql_kw       = "('".$serp_keyword."', '".$lang."', '" . $this->dateYMD() . "')";
      $query_kw     = 'INSERT INTO ruk_scrape_keywords (keyword, language, timestamp) VALUES '. $sql_kw .' ';
      $this->db->query($query_kw);

      $last_insert_id = $this->db->insert_id;

      $rank   = 0;
      $sql_ra = array();
      foreach ($result->results as $k => $serp) {
        $rank++;
        $pure_host  = str_ireplace('www.', '', parse_url($serp->url, PHP_URL_HOST));
        $serp_url   = $this->db->real_escape_string($serp->url);
        $pure_host  = $this->db->real_escape_string($pure_host);
        $sql_ra[]   = "('".$last_insert_id."', '".$rank."', '".$serp_url."', 0, 0, '".$pure_host."', 0)";
      }

      $q_string_ra = implode(',', $sql_ra);

       // add to keyword ranking table
      $query_ra = 'INSERT INTO ruk_scrape_rankings (id_kw, position, url, title, description, hostname, meta) VALUES '. $q_string_ra .' ';
      $res = $this->db->query($query_ra);

      if (!empty($this->db->error)) {
        $this->logToFileException('TP: DB ERROR QUERY: ' . $serp_keyword . ' - ' . $query_ra . ' -> ' . $this->db->error);
      }

    }

  }

  private function scrapeCurl ($payload)
  {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $this->service);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'data=' . $payload);
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);

    $output = curl_exec($ch);

    if (curl_exec($ch) === false) {
      $this->logToFile($this->dateHIS() . ' CurlError: ' . curl_error($ch));
      $output = 'false - empty';
    }

    curl_close($ch);

    return $output;

  }

  private function checkIfAlreadyScraped ($keyword, $lang)
  {

    $today = $this->dateYMD();
    $sql = "SELECT id FROM ruk_scrape_keywords WHERE CAST(keyword AS BINARY) = '$keyword' AND language = '$lang' AND timestamp = '$today'";
    $result = $this->db->query($sql);
    if (!empty($this->db->error)) {
      $this->logToFile('TP: checkIfAlreadyScraped ERROR: ' . $this->db->error);
    }
    if ($result->num_rows > 0) {
      return false;
    }
    return true;

  }

  private function getOpenKeywords ()
  {

    $all_keywords = array();

    $sql = "SELECT
              a.keyword   AS keyword,
              b.measuring AS measuring,
              b.rs_type   AS rs_type,
              c.country   AS country
            FROM
              ruk_project_keywords a
            LEFT JOIN
              ruk_project_keyword_sets b
            ON
              a.id_kw_set = b.id
              LEFT JOIN
                ruk_project_customers c
              ON
              b.id_customer = c.id
            WHERE
              b.measuring = 'daily'";

    $result = $this->db->query($sql);

    if (!empty($this->db->error)) {$this->logToFile('TP ERROR: ' . $this->db->error);}

    while ($row = $result->fetch_assoc()) {
      if ($row['rs_type'] == 'desktop') {
        $hash_rs_type = $row['country'];
      } else {
        $hash_rs_type = $row['rs_type'] . '_' . $row['country'];
      }

      $hash                = md5($row['keyword'] . $hash_rs_type);
      $all_keywords[$hash] = array($row['keyword'], $row['country'], $row['rs_type']);
    }


    // COMPARE VS. SCRAPED KEYWORDS
    $today = $this->dateYMD();
    $scraped_keywords = array();
    $sql = "SELECT keyword, language FROM ruk_scrape_keywords WHERE timestamp = '$today'";
    $result1 = $this->db->query($sql);

    if (!empty($this->db->error)) {$this->logToFile('TP ERROR: ' . $this->db->error);}

    while ($row1 = $result1->fetch_assoc()) {
      $hash                    = md5($row1['keyword'] . $row1['language']);
      $scraped_keywords[$hash] = array($row1['keyword'], $row1['language']);
    }

    $keywords_to_scrape = array();

    foreach ($all_keywords as $hash => $data) {
      if (!isset($scraped_keywords[$hash])) {
        $keywords_to_scrape[] = $data;
      }
    }

    return $keywords_to_scrape;

  }


  private function dateYMD ()
  {
    $date = new DateTime();
    $date->add(new DateInterval($this->timeoffset));
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);
  }

}

new trustedproxy_keywordupdate;

?>
