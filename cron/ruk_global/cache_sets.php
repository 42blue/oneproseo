<?php

/*

EMAIL WEEKLY
ALLIANZ
php cache_sets.php c 260
php cache_sets.php c 202 / 3719

*/

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKSets extends ruk  {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $emailcontent = '';


  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->logToFile('CACHE: SETS 7 DAYS START');
    $this->fetchSets();
    $this->logToFile('CACHE: SETS 7 DAYS END');

    $this->db->close();

  }


  private function fetchSets () {

    $sql = "SELECT id, name, url, country, ga_account, gwt_account, competition FROM ruk_project_customers";

    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, name, url, country, ga_account, gwt_account, competition FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    $is_monday = (date('N', strtotime($this->dateYMDReal())));

    // GET ALL CUSTOMERS
    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];

      echo 'PRO: ' . $this->project_id . PHP_EOL;

      $this->ga_account      = $row['ga_account'];
      $this->gwt_account     = $row['gwt_account'];
      $this->country         = $row['country'];
      $this->hostname        = $this->pureHostName($row['url']);
      $this->customer_name   = $row['name'];


      // LOAD CACHE ONCE
      $this->loadCache();
      $this->loadCacheWeekly();
      

      $sql = "SELECT id, type, rs_type, name, team_email, measuring FROM ruk_project_keyword_sets WHERE id_customer = '$this->project_id' AND team_email != ''";

      $result2 = $this->db->query($sql);

      if ($result2->num_rows == 0) {
        continue;
      }

      $workload = array();
      while ($row2 = $result2->fetch_assoc()) {

				if ($row2['measuring'] == 'weekly' && $is_monday != 1) {
					continue;
				}

        $workload[$row2['id']] = array($row2['type'], $row2['rs_type'], $row2['name'], $row2['team_email'], $row2['measuring']);
      }

      // MAIN LOOP
      foreach ($workload as $id => $type) {

        $this->keyword_setid   = $id;
        $this->campaign_type   = $type[0];
        $this->mo_type         = $type[1];
        $this->setname         = $type[2];
        $this->teamemail       = $type[3];
        $this->measuring       = $type[4];

        $this->doSummarySetdata();

      }

    }

  }


  private function doSummarySetdata () {

    $this->cache_filename = 'csv/setdata_'  . $this->project_id . '_' . $this->keyword_setid . '.csv';

    $this->url_campaign = '';

     // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }

    if ($this->keyword_setid == 'all' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
    }

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

    $this->getRankingData();
    $this->createCSV();
    $this->createEmail();
    $this->sendAlertMailHTML();

  }


  private function createCSV () {

		$this->csv      = array();
    $this->maildata = array();

		$searchconsole = $this->getSearchConsoleData();
		$adwords       = parent::getAdwordsDataI18N($this->keywords, $this->country, $this->country);

		$maildata = array();
    $csvdata  = array();
    $seenkws  = array();

		foreach ($this->keywords as $keyword) {

			$clicks = 0;
			$impressions = 0;
			$ctr = 0;
      $tags = '';

			$mobile_rank  = '';
			$mobile_url   = '';
			$desktop_rank = '';
			$desktop_url  = '';

			$mobile_rank_compare  = '';
			$desktop_rank_compare = '';
			$mobile_rank_y        = '';
			$desktop_rank_y       = '';

			if (isset($this->rankset[$keyword][$this->dateYMD()])) {

				$keywordAndDate  = $this->rankset[$keyword][$this->dateYMD()];

				$mobile_rank  = '';
				$mobile_url   = '';
				$desktop_rank = '';
				$desktop_url  = '';

				foreach ($keywordAndDate as $mobileOrDesktop => $ranks) {

					if (stripos($mobileOrDesktop, 'mobile') !== false) {

						$mobile_rank_compare = $ranks[0][0];
						foreach ($ranks as $multirank) {
							$mobile_rank .= $multirank[0] . ' / ' ;
							$mobile_url  .= $multirank[1] . ' / ' ;
						}

					} else {

						$desktop_rank_compare = $ranks[0][0];
						foreach ($ranks as $multirank) {
							$desktop_rank .= $multirank[0] . ' / ' ;
							$desktop_url  .= $multirank[1] . ' / ' ;
						}

					}

				}

				$mobile_rank = rtrim($mobile_rank, ' / ');
				$mobile_url  = rtrim($mobile_url, ' / ');
				$desktop_rank = rtrim($desktop_rank, ' / ');
				$desktop_url  = rtrim($desktop_url, ' / ');

			}
 
			if (isset($this->rankset[$keyword][$this->dateYMDCompare()])) {

				$keywordAndDateY  = $this->rankset[$keyword][$this->dateYMDCompare()];

				$mobile_rank_y  = '';
				$desktop_rank_y = '';

				foreach ($keywordAndDateY as $mobileOrDesktop => $ranks) {

					if (stripos($mobileOrDesktop, 'mobile') !== false) {

						$mobile_rank_y = $ranks[0][0];

					} else {

						$desktop_rank_y = $ranks[0][0];

					}

				}

			}

			$mobile_rank_y = $this->calcDelta($mobile_rank_compare, $mobile_rank_y);
			$desktop_rank_y = $this->calcDelta($desktop_rank_compare, $desktop_rank_y);

 			$search  = array('Ä', 'Ö', 'Ü');
			$replace = array('ä', 'ö', 'ü');

			$keyword = str_replace($search, $replace, $keyword);

			if (isset($searchconsole[$keyword])) {
				$impressions = $searchconsole[$keyword][0];
				$clicks      = $searchconsole[$keyword][1];
				$ctr         = round ($searchconsole[$keyword][2] / $searchconsole[$keyword][3], 2);
			}

      if (isset($this->tags[$keyword])) {
        $tags_csv_head = array();
        $tags = $this->tags[$keyword];
        $tags_csv = explode(', ', $this->tags[$keyword]);
        foreach ($tags_csv as $key) {
          $tags_csv_head[] = 'Tag';
        }
      }          

      if (isset($seenkws[$keyword])) {
        continue;
      }
      $seenkws[$keyword] = $keyword;

      $sv   = $adwords[$keyword][0];
      $cpc  = str_replace ('.', ',', $adwords[$keyword][1]);
      $ctr  = str_replace ('.', ',', $ctr);

      if ($sv < 90) {
        continue;
      }

      if (empty($searchconsole) && $this->tagscount > 0) {

        $maildata[] = array($keyword,$tags,$sv,$cpc,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url);
        $csvdata[]  = array_merge (array($keyword,$sv,$cpc,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url), $tags_csv);

      } else if (empty($searchconsole) && $this->tagscount < 1) {

        $maildata[] = array($keyword,$sv,$cpc,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url);
        $csvdata[]  = array($keyword,$sv,$cpc,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url);

      } else if (!empty($searchconsole) && $this->tagscount < 1) {

        $maildata[] = array($keyword,$sv,$cpc,$clicks,$impressions,$ctr,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url);
        $csvdata[]  = array($keyword,$sv,$cpc,$clicks,$impressions,$ctr,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url);

      } else if (!empty($searchconsole) && $this->tagscount > 0) {

        $maildata[] = array($keyword,$tags,$sv,$cpc,$clicks,$impressions,$ctr,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url);
        $csvdata[]  = array_merge (array($keyword,$sv,$cpc,$clicks,$impressions,$ctr,$mobile_rank,$mobile_rank_y,$mobile_url,$desktop_rank,$desktop_rank_y,$desktop_url), $tags_csv);

      }

		}

    if ($this->tagscount > 0) {
  		usort($maildata, function($a, $b) {
      	return $b[2] - $a[2];
  		});
    } else {
      usort($maildata, function($a, $b) {
        return $b[1] - $a[1];
      });      
    }

		$mailhead = array ();

      if (empty($searchconsole) && $this->tagscount > 0) {

        $mailhead[] = array('Keyword', 'Tags', 'SV', 'CPC (€)', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop');
        $csvhead[]  = array_merge (array('Keyword', 'SV', 'CPC (€)', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop'), $tags_csv_head);

      } else if (empty($searchconsole) && $this->tagscount < 1) {

        $mailhead[] = array('Keyword', 'SV', 'CPC (€)', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop');
        $csvhead[]  = array('Keyword', 'SV', 'CPC (€)', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop');

      } else if (!empty($searchconsole) && $this->tagscount < 1) {

        $mailhead[] = array('Keyword', 'SV', 'CPC (€)', 'Klicks', 'Impr.', 'CTR', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop');
        $csvhead[]  = array('Keyword', 'SV', 'CPC (€)', 'Klicks', 'Impressions', 'CTR', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop');

      } else if (!empty($searchconsole) && $this->tagscount > 0) {

        $mailhead[] = array('Keyword', 'Tags', 'SV', 'CPC (€)', 'Klicks', 'Impr.', 'CTR', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop');
        $csvhead[]  = array_merge (array('Keyword', 'SV', 'CPC (€)', 'Klicks', 'Impressions', 'CTR', 'Mobile', '+/-', 'URL Mobile', 'Desktop', '+/-', 'URL Desktop'), $tags_csv_head);

      }


    $this->maildata = array_merge($mailhead, $maildata);
    $this->csv      = array_merge($csvhead, $csvdata);

    $this->writeCsv($this->csv);

  }


  private function calcDelta ($today, $ago) {

		if (empty($today) && empty($ago)) { 

			return '';

		} else if (!empty($today) && !empty($ago)) {

			if ($ago > $today) {
				$calc = $ago - $today;
				return '+' . $calc;
			} else if ($ago < $today) {
				return $ago - $today;
			}

		} else if (!empty($today) && empty($ago)) {

			$calc = 101 - $today;

			return '+' . $calc;

		} else if (empty($today) && !empty($ago)) {

			return $ago - 101;

		}

  }


  private function createEmail () {

  	$content = '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 12px; border: 1px solid #fff; width: 100%;">';

  	$i = 0;

		foreach ($this->maildata as $data) {

      $i++;

      if ($i > 500) {
        continue;
      }

      $content .= '<tr>';

      foreach ($data as $value) {

        if ($i == 1) {
          $content .= '<td style="padding: 3px; border: 1px solid #9a9a9a;"><b>'.$value.'</b></td>'."\n";
        } else {
          $content .= '<td style="padding: 3px; border: 1px solid #9a9a9a;">'.$value.'</td>'."\n";  
        }
        
      }

      $content .= '</tr>';

		}

		$content .= '</table>';

    $this->emailcontent = '
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi SEO Team,<br />your update on:<br /><br />'."\n".'
          <span style="font-size: 38px; font-color: #636363;"><b>' .$this->customer_name . '</b><br /></span>
          <span style="font-size: 33px; font-color: #636363;"><b> ' . $this->setname.'</b></span>
          <br /><br /><br />
          
          <a href="https://enterprise.oneproseo.com/temp/cache/'. $this->cache_filename .'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">CSV download</a>
          <br /><br /></span>
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'
          '."\n".'<td style="vertical-align: top; width: 99%">

          					' . $content  . '

                  </td>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'
        <tr>
          '."\n".'<td style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';

  }


  private function sendAlertMailHTML ()
  {

    $sendto  = $this->teamemail;
		//$sendto  = 'm.hotz@advertising.de';

    $subject  = 'OneProSeo Reporting | ' . $this->customer_name .' | ' . $this->setname;
    $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8' . "\r\n" . 'From: OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;
    $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
    $message .= $this->emailcontent;
    $message .= '</body></html>';

    mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

  }


  private function selectKeywordSet () {

    $this->keywords  = array();
    $this->tags      = array();
    $this->tagscount = 0;

    $selected_keyword_set = $this->keyword_setid;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   a.name        AS setname,
                   b.keyword     AS keyword,
                   b.tags        AS tags
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[]            = $row['keyword'];
      $this->tags[$row['keyword']] = $row['tags'];
      if (!empty($row['tags'])) {
        $this->tagscount++;
      }
    }

  }


  private function selectAllKeywordSets () {

    $this->keywords  = array();
    $this->tags      = array();
    $this->tagscount = 0;    

    $selected_project = $this->project_id;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword,
                   b.tags        AS tags
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords[]            = $row['keyword'];
      $this->tags[$row['keyword']] = $row['tags'];
      if (!empty($row['tags'])) {
        $this->tagscount++;
      }      
    }

  }


  private function getRankingData() {

  	if ($this->measuring == 'weekly') {
  		$rows = $this->project_cache_weekly;
  	} else {
  		$rows = $this->project_cache;	
  	}

    $rows = array_reverse($rows);

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
          unset($this->url_campaign[$key]);
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $this->rankset = array();

    foreach ($rows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {
				$this->rankset[$row['k']][$row['t']][$row['l']][] = array($row['p'], $row['u']);
      }

    }

  }


  // FETCH URLS in KEYWORD CAMPAIGN
  private function fetchURLSet($kw_set_id) {

    $this->reconMySql();

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  private function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  private function getSearchConsoleData () {

    $gwt_rows = array();

		$sql = "SELECT
    					id
            FROM
              gwt_hostnames
            WHERE
              hostname = '$this->hostname'";

    $result = $this->db->query($sql);

		if ($result->num_rows < 1) {
			return $gwt_rows;
		}

    while ($row = $result->fetch_assoc()) {
    	$id = $row['id'];
    }


    $sql = "SELECT
    					query,
              impressions,
              clicks,
              ctr,
              timestamp
            FROM
              gwt_data
            WHERE
              hostname_id = $id
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 14 DAY";

    $result_gw = $this->db->query($sql);

    while ($row_gw = $result_gw->fetch_assoc()) {

      if (!empty($row_gw['clicks'])) {
        $hash = crc32 ($row_gw['query'] . $row_gw['timestamp']);
        $gwt_rows[$hash] = array($row_gw['query'], $row_gw['impressions'], $row_gw['clicks'], $row_gw['ctr']);
      }

    }


    foreach ($gwt_rows as $hash => $set) {

			$keyword = $set[0];

    	if (isset($gwt[$keyword])) {
				$gwt[$keyword][0] = $gwt[$keyword][0] + $set[1];
				$gwt[$keyword][1] = $gwt[$keyword][1] + $set[2];
				$gwt[$keyword][2] = $gwt[$keyword][2] + $set[3];
				$gwt[$keyword][3] = $gwt[$keyword][3] + 1;
    	} else {
    	  $gwt[$keyword] = array($set[1], $set[2], $set[3], 1);
    	}
    	
    }

		return $gwt;

  }


  private function dateYMD () {

  	if ($this->measuring == 'weekly') {
	    return date('Y-m-d', strtotime('Monday this week'));
  	} else {
    	return date("Y-m-d", strtotime('today'));
  	}

  }

  private function dateYMDCompare () {

  	if ($this->measuring == 'weekly') {
	    return date('Y-m-d', strtotime('Monday last week'));
  	} else {
    	return date("Y-m-d", strtotime('yesterday'));
  	}

  }

	private function dateYMDReal () {
		return date('Y-m-d', strtotime('today'));
  }

  private function dateYMDYesterday () {
    return date('Y-m-d', strtotime('-1 days'));
  }


  private function writeCsv ($data) {
    $fp = fopen($this->cache_dir . $this->cache_filename, 'w');
    foreach ($data as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }


  private function loadCache() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_x.tmp';
    $this->project_cache = $this->readFile($file);
  }

  private function loadCacheWeekly() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_weekly.tmp';
    $this->project_cache_weekly = $this->readFile($file);
  }

  private function readFile ($fileName) {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }


  private function pureHostName ($url) {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


  private function isWeekend ($date) {
    $weekDay = date('w', strtotime($date));
    if ($weekDay == 0 || $weekDay == 6) {
      return true;
    }
    return false;
  }


}

new renderRUKSets($argv);

?>