<?php

// sudo php cache_json_keyword_rankings_monthly.php c 122

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk  {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->logToFile('CACHE: JSON MONTHLY START');
    $this->fetchSets();
    $this->logToFile('CACHE: JSON MONTHLY END');

    $this->db->close();

  }

  private function fetchSets ()
  {

    $sql = "SELECT id, url, country, ga_account, gwt_account, competition FROM ruk_project_customers";

    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, url, country, ga_account, gwt_account, competition FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    // GET ALL CUSTOMERS
    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];
      echo 'PRO: ' . $this->project_id . PHP_EOL;

      $this->ga_account      = $row['ga_account'];
      $this->gwt_account     = $row['gwt_account'];
      $this->country         = $row['country'];
      $this->competition_all = $row['competition'];
      $this->hostname        = $this->pureHostName($row['url']);

      // LOAD CACHE ONCE
      $this->loadCache();
      $this->reconMySql();

      $sql     = "SELECT id, type, rs_type, competition FROM ruk_project_keyword_sets WHERE id_customer = '$this->project_id'";
      $result2 = $this->db->query($sql);

      if ($result2->num_rows == 0) {
        continue;
      }

      $workload = array();
      $workload['all'] = array('key', 'desktop');
      while ($row2 = $result2->fetch_assoc()) {
        $workload[$row2['id']] = array($row2['type'], $row2['rs_type'], $row2['competition']);
      }

      // MAIN LOOP
      foreach ($workload as $id => $type) {

        $this->competition_set = array();

        if(!isset($type[2]) || empty($type[2])) {
          $type[2] = serialize(array());
        }

        $this->keyword_setid   = $id;
        $this->campaign_type   = $type[0];
        $this->mo_type         = $type[1];
        $this->competition_set = $type[2];

        echo 'SET: ' . $this->keyword_setid . PHP_EOL;
        // generate summary
        $this->doChart();

      }

    }

  }

  public function loadCache() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_monthly.tmp';
    $this->project_cache = $this->readFile($file);
  }


  private function doChart ()
  {
    $this->cache_filename_json = 'json/rankings_'  . $this->project_id . '_' . $this->keyword_setid . '_monthly.json';

    $this->url_campaign = '';

    // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }

    if ($this->keyword_setid == 'all' || $this->keyword_setid == 'all_monthly' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
    }

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

    $this->getRankingData();

    $this->createJson();

  }


  private function createJson()
  {

    $json = array();

    if (!empty($this->rankset)) {
      foreach ($this->rankset as $keyword => $arr) {
        foreach ($arr as $date => $rank) {
        	if (!isset ($this->tags[$keyword])) {
        		$this->tags[$keyword] = '';
        	}
          $sv = 0;
          if (isset($this->googleadwords[$keyword])) {
            $sv = $this->googleadwords[$keyword][0];
          }
          $json[] = array('timestamp' => $date, 'keyword' => $keyword, 'rank' => $rank[0], 'url' => $rank[1], 'sv' => $sv,'tags' => $this->tags[$keyword]);
        }
      }
    }

    $this->writeFileRaw($this->cache_filename_json, json_encode($json, JSON_PRETTY_PRINT));    

  }


  private function selectKeywordSet ()
  {

    $this->keywords = array();
    $this->tags     = array();

    $selected_keyword_set = $this->keyword_setid;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   a.name        AS setname,
                   b.keyword     AS keyword,
                   b.tags        AS tags,                   
                   c.url         AS url,
                   c.id          AS customerid
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
      $this->tags[$row['keyword']] = $row['tags'];      
    }

  }


  private function selectAllKeywordSets ()
  {

    $this->keywords = array();
    $this->tags = array();

    $selected_project = $this->project_id;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword,
                   b.tags        AS tags
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords[] = $row['keyword'];
      $this->tags[$row['keyword']] = $row['tags'];
    }

  }


  private function getRankingData()
  {

    $rows = $this->project_cache;

    if (!is_array($this->project_cache)) {return;}

    $adwordsloc = $this->getAdwordsLoc($this->country);
    $this->googleadwords = $this->getAdwordsDataI18N($this->keywords, $adwordsloc[0], $adwordsloc[1]);

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
          unset($this->url_campaign[$key]);
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $this->rankset = array();

    foreach ($rows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {
    
        // only first ranking
        if (!isset($this->rankset[$row['k']][$row['t']])) {
          $this->rankset[$row['k']][$row['t']] = array($row['p'], $row['u']);  
        }
        
      }

    }

  }



  public function summarySetdataMerge($ts, $type, $add)
  {

    if (isset($this->rankset['_ov'][$ts][$type])) {
      $this->rankset['_ov'][$ts][$type] = $this->rankset['_ov'][$ts][$type] + $add;
    } else {
      $this->rankset['_ov'][$ts][$type] = $add;
    }

  }


  // FETCH URLS in KEYWORD CAMPAIGN
  public function fetchURLSet($kw_set_id)
  {

    $this->reconMySql();

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  public function writeFileRaw ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }


  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

}

new renderRUKOverviews ($argv);

?>
