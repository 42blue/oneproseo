<?php

/*

Erzeugt jeden Tag ein Backup von ruk_scrape_keywords in ruk_scrape_rankings_backup

*/

date_default_timezone_set('CET');

class ruk_backup {

  private $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct ()
  {
    $this->mySqlConnect();

    $this->backupRT();
    $this->deleteRT();

    $this->db->close();
  }


  private function deleteRT ()
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $date   = new DateTime("-30days");
    $days30 = $date->format("Y-m-d");

    // echo 'START DELETE'. PHP_EOL;

    $sql = "DELETE FROM ruk_scrape_rankings_backup WHERE timestamp < '$days30'";
    $result = $this->db->query($sql);

    if (!empty($this->db->error)) {
      echo $this->db->error . '<br />' . PHP_EOL;
    }
  }

  private function backupRT ()
  {
    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $today = date("Y-m-d");

    $backup = array();

    $sql = "SELECT id, timestamp FROM ruk_scrape_keywords WHERE timestamp = '$today'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $backup[$row['id']] = $row['timestamp'];
    }

    if (count($backup) == 0) {
      exit;
    }

    $i = 0;
    foreach ($backup as $key => $ts)
    {
      $i++;
      echo $i . PHP_EOL;

      $sql = "INSERT IGNORE INTO
                ruk_scrape_rankings_backup (id, id_kw, position, url, title, description, hostname, meta, timestamp)
              SELECT
                id, id_kw, position, url, title, description, hostname, meta, '$ts'
              FROM
                ruk_scrape_rankings
              WHERE id_kw = $key";
      $result = $this->db->query($sql);

      if (!empty($this->db->error)) {
        echo $this->db->error . '<br />' . PHP_EOL;
      }
    }
  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }

}

new ruk_backup;

?>
