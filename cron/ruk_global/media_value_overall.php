<?php

/*
Alle Clicks aus Searchconsole pro Kunde mal durchschnittlicher CPC aus Adwords
Einmal im Monat
*/

require_once dirname(__FILE__) . '/ruk.class.php';

class ruk_media_value_overall extends ruk {

  public  $logdir       = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->startdate = date("Y-m-d", strtotime("first day of previous month"));
    $this->enddate   = date("Y-m-d", strtotime("last day of previous month"));

    $this->selectProjects();

    $this->logToFile('MEDIAVALUE OVERALL START MONTHLY');
    $this->logToFile('MEDIAVALUE OVERALL: ' . count ($this->projects_sets));

    foreach ($this->projects_sets as $data) {

      $hostname = str_ireplace('www.', '', parse_url($data[1], PHP_URL_HOST));

      $this->gwthostid  = $this->selectHostnameId($hostname);

      if ($this->gwthostid == false) {
        echo $hostname . PHP_EOL;
        continue;
      }

      $this->customerid = $data[0];

      $this->doUpdate();

    }

    $this->logToFile('MEDIAVALUE OVERALL DONE');

  }

  private function doUpdate () {

    $this->fetchKeywordClicks();

    // ONLY THE KEYWORDS
    $this->keywords = array_keys($this->keywords_clicks);

    $this->fetchAdwordsCPC();

    $campaignval = 0;
    $value       = 0;
    $multiplier  = 0;
    foreach ($this->keywords_cpc as $keyword => $cpc) {

      if (isset($this->keywords_clicks[$keyword])) {
        $multiplier   = $this->clickMultiplier($this->keywords_clicks[$keyword]['pos']);
        $value       = $cpc * $this->keywords_clicks[$keyword]['clicks'] * $multiplier;
        $campaignval = $campaignval + $value;
      }

    }

    $this->logToFile('MEDIAVALUE OVERALL CUSTOMERID: ' . $this->customerid . ', AMOUNT: ' . count ($this->keywords) . ', VAL: ' . $campaignval);

    $this->reconMySql();
    $sql = "UPDATE ruk_project_customers SET mediavalue = $campaignval WHERE id = '$this->customerid'";
    $this->db->query($sql);

    echo ('MEDIAVALUE OVERALL: ' . $campaignval . ' - Customer: ' . $this->customerid);
    echo PHP_EOL;

  }


  private function fetchAdwordsCPC () {

    $this->keywords_cpc = array();

    // QUERY RUK DB
    $result = $this->fetchAdwordsDB($this->keywords);
    foreach ($result as $k => $v) {
      $this->keywords_cpc[$k] = $v;
    }

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  private function fetchAdwordsDB ($keywords)
  {

    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    $this->reconMySql();

    $sql = "SELECT
              keyword,
              cpc
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $googleadwords = array();

    while ($row = $res->fetch_assoc()) {
     $googleadwords[$row['keyword']] =  $row['cpc'];
    }

    return $googleadwords;

  }

  private function fetchKeywordClicks ()
  {

    $this->keywords_clicks = array();

    $this->reconMySql();

    $sql = "SELECT
              query,
              AVG(position_1) AS position,
              SUM(clicks)     AS sum_clicks
            FROM
              gwt_data
            WHERE
              hostname_id = $this->gwthostid
            AND
              timestamp >= '$this->startdate'
            AND
              timestamp <= '$this->enddate'
            GROUP BY
              query";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if ($row['sum_clicks'] > 0) {
        $this->keywords_clicks[$row['query']] = array('clicks' => $row['sum_clicks'], 'pos' => $row['position']);
      }
    }

  }


  private function selectProjects ()
  {

    $this->projects_sets = array();

    $sql = "SELECT
                  id   AS customerid,
                  name AS customername,
                  url  AS customerurl
                FROM ruk_project_customers
                WHERE gwt_account = 1";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->projects_sets[] = array($row['customerid'], $row['customerurl']);
    }

  }

  private function selectHostnameId ($host)
  {

    $this->reconMySql();

    $hostid = '';

    $sql = "SELECT id FROM gwt_hostnames WHERE hostname = '".$host."'";
    $result = $this->db->query($sql);

    if ($result->num_rows == 0) {
      return false;
    }

    while ($row = $result->fetch_assoc()) {
      $hostid = $row['id'];
    }

   return $hostid;

  }


  private function insertToAdwordsDb ($kw, $cpc, $sv, $comp) {

    $language = 'de';

    $keyword = $this->db->real_escape_string($kw);

    $row1 = floatval($sv);
    $row2 = floatval($cpc);
    $row3 = floatval($comp);

    if (($row1 == 0) && ($row2 == 0) && ($row3 == 0)) {

      $opi   = 0;
      $larry = 0;

    } else {

      if ($row1 == 0) {
        $row1 = 0.01;
      }

      if ($row2 == 0) {
        $row2 = 0.01;
      }

      if ($row3 == 0) {
        $row3 = 0.01;
      }

      $opi   = $row1 * $row2 * $row3;
      $larry = round($row2 * $row1 / $row3);

    }


    $sql = 'INSERT INTO
              gen_keywords (keyword, language, country, timestamp, searchvolume, cpc, competition, opi_larry, opi)
            VALUES
              ("'.$keyword.'", "'.$language.'", "'.$language.'",  CURDATE(), "' . $sv .'","' . $cpc .'","' . $comp .'","' . $larry .'","' . $opi .'")
            ON DUPLICATE KEY UPDATE
              keyword = "'.$keyword.'",
              language = "'.$language.'",
              country = "'.$language.'",
              timestamp = CURDATE(),
              searchvolume = "'.$sv.'",
              cpc = "'.$cpc.'",
              competition = "'.$comp.'",
              opi_larry = "'.$larry.'",
              opi = "'.$opi.'"
            ';

    $result = $this->db->query($sql);

  }

}

new ruk_media_value_overall ();

?>
