<?php

date_default_timezone_set('CET');

  // http://api.sistrix.com/domain.sichtbarkeitsindex?api_key=5rHzgQjjrpk2qBWdmdA5gp6yBwdXsZaR&date=2014-10-13&domain=advertising.de/seo
  // http://api.sistrix.com/domain.sichtbarkeitsindex?api_key=5rHzgQjjrpk2qBWdmdA5gp6yBwdXsZaR&domain=

class fetchSistrix {

  private $logdir   = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->clearTable();
    $this->fetchCustomerUrls();
    $this->getSistrixData();

    $this->db->close();

  }


  private function fetchCustomerUrls ()
  {

    $sql = "SELECT id, url from ruk_indexwatch_seo_companies";

    $result = $this->db->query($sql);

    $this->all_hosts = array();
    while ($row = $result->fetch_assoc()) {
      $host = $this->pureHostName($row['url']);
      $this->all_hosts[$row['id']] = $host;
    }

  }

  private function clearTable ()
  {

    $sql = "TRUNCATE TABLE ruk_indexwatch_seo_companies_sistrix";
    $result = $this->db->query($sql);

  }


  private function getSistrixData ()
  {

    $timestamp = $this->dateYMD();

    foreach ($this->all_hosts as $id => $host) {

      $ctx  = stream_context_create(array('http'=>array('timeout' => 3)));
      $data = file_get_contents('https://api.sistrix.com/domain.sichtbarkeitsindex?api_key=uaKPBAUKDfajBAzIGjpCMjbsmFzZtKIC&domain=' . $host, false, $ctx);

      $xml = simplexml_load_string($data);

      echo $host . PHP_EOL;

      if (empty($data)) {
        $this->logToFile($this->dateHIS() . ' SISTRIX INDEXWATCH Error - No DATA: ' . $host);
        continue;
      }

      if (empty($xml) or $xml == FALSE ) {
        $this->logToFile($this->dateHIS() . ' SISTRIX INDEXWATCH Error - Not XML: ' . $host);
        continue;
      }

      $this->reconMySql();

      foreach($xml->answer->sichtbarkeitsindex as $item) {

        $new_score = $item['value'];

        $this->logToFile($this->dateHIS() . ' SISTRIX INDEXWATCH FRESH: ' . $item['domain']);

        $sql_ra = "('".$id."', '".$new_score."', '".$timestamp."')";

        $query_ra = 'INSERT INTO
                         ruk_indexwatch_seo_companies_sistrix (id_customer, score, timestamp)
                      VALUES
                        '. $sql_ra .' ';

        $call = $this->db->query($query_ra);

        if (!empty($this->db->error)) {
          $this->logToFile($this->dateHIS() . ' SISTRIX INDEXWATCH DATA: DB ERROR: ' . $this->db->error);
        }

        $this->logToFile($this->dateHIS() . ' SISTRIX INDEXWATCH NEW DATA: ' . $item['domain']);

      }

    }

  }


  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


  public function mySqlConnect ()
  {
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }

  }


  public function dateYMD ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);

  }

  public function dateHIS ()
  {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i:s", $date);
  }

  public function logToFile ($content) {
    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);
  }

  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

}

new fetchSistrix;

?>
