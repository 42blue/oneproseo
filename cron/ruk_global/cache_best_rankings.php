<?php

require_once dirname(__FILE__) . '/ruk.class.php';

class render_best_rankings extends ruk {

  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $csv_dir   = '/var/www/enterprise.oneproseo.com/temp/export/';

  public function __construct () {

    $this->mySqlConnect();
    $this->logToFile('CACHE: BEST RANKINGS START');
    $this->fetchSets();
    $this->logToFile('CACHE: BEST RANKINGS END');
    $this->db->close();

  }


  private function fetchSets ()
  {

    $sql      = "SELECT id, country, url FROM ruk_project_customers";
    $result   = $this->db->query($sql);
    $projects = array();

    // GET PROJECTS
    while ($row = $result->fetch_assoc()) {
      $projects[$row['id']] = array($this->pureHostName($row['url']), $row['country']);
    }

    // GET KEYWORDS
    foreach ($projects as $id => $host) {
      array_push($projects[$id], $this->selectAllKeywordSets($id));
    }

    foreach ($projects as $id => $arr) {

      $this->bestrankings = array();
      $this->hostname    = $arr[0];
      $this->language    = $arr[1];

      $i = count($arr[2]);

      foreach ($arr[2] as $keyword) {
        echo $i--;
        echo ' - ';
        echo $keyword;
        echo PHP_EOL;
        $this->getBestRanking($keyword);
      }

      $this->writeFile('metrics_rankings_data_' . $id . '_best.tmp' , $this->bestrankings);

    }

  }

  public function getBestRanking($keyword) {

    $ranks = array();

    $sql = "SELECT
              a.timestamp AS ts,
              b.position  AS pos
            FROM
              ruk_scrape_keywords a
              LEFT JOIN ruk_scrape_rankings b
                ON b.id_kw = a.id
            WHERE
              a.keyword = '$keyword'
            AND
              b.hostname = '$this->hostname'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $ranks[$row['pos']] = $row ['ts'];
    }

    ksort($ranks);

    if (!empty ($ranks)) {
      $this->bestrankings[$keyword] = array (key($ranks), $ranks[key($ranks)]);
    }

  }

  public function writeFile ($fileName, $variable)
  {
    $fileName = $this->cache_dir . $fileName;
    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);
  }

  private function selectAllKeywordSets ($selected_project) {

    $this->reconMySql();

    $sql = "SELECT
              b.keyword AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = $selected_project
            AND a.type = 'key'";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

}

new render_best_rankings;

?>
