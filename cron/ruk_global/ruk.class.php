<?php

date_default_timezone_set('CET');
ini_set('mysql.connect_timeout', 1200);
ini_set('default_socket_timeout', 1200);

class ruk {

  public function mySqlConnect ()
  {

    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFileException('RANKING SPY - MYSQL CONNECT ERROR: ' . mysqli_connect_error());
      $this->alertMail('RANKING SPY - MYSQL CONNECT ERROR: ' . mysqli_connect_error());
      exit;
    }

  }

  public function TS ()
  {
    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i / d-m", $date);
  }

  public function clickMultiplier($pos)
  {

    if ($pos >= 1 && $pos < 3.5) {
      return 3;
    } else if ($pos >= 3.5 && $pos < 6.5) {
      return 2;
    } else {
      return 1;
    }

  }

  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE I18N
  public function getAdwordsDataI18N ($keywords, $country = 'de', $lang = 'de')
  {

    if (stripos($country, '-') !== FALSE) {
      $arr = explode ('-', $country);
      $country    = $arr[0];
      $lang       = $arr[1];
    }

    if ($country == 'be') {
      $lang = 'fr';
    }
    if ($country == 'uk') {
      $lang = 'en';
    }
    if ($country == 'at') {
      $lang = 'de';
    }
    if ($country == 'ch') {
      $lang = 'de';
    }

    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = stripslashes($comma_separated_kws);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $sql = "SELECT
              keyword,
              searchvolume,
              cpc,
              competition,
              opi
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              country = '$country'
            AND
              language = '$lang'
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $googleadwords = array();

    while ($row = $res->fetch_assoc()) {
     $googleadwords[$row['keyword']] = array ($row['searchvolume'], $row['cpc'], $row['competition'], $row['opi']);
    }

    return $googleadwords;

  }


  // SHITTY COUNTRY MATCHING
  public function getAdwordsLoc ($country)
  {

    $language = $country;

    if (stripos($country, '-') !== FALSE) {
      $arr = explode ('-', $country);
      $country  = $arr[0];
      $language = $arr[1];
    }
    if ($country == 'be') {
      $language = 'fr';
    }
    if ($country == 'uk') {
      $language = 'en';
    }
    if ($country == 'at') {
      $language = 'de';
    }
    if ($country == 'ch') {
      $language = 'de';
    }

    return array ($country, $language);

  }


  public function logToFile ($content)
  {
    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $this->TS() . ' - ' . $content . "\r\n");
    fclose($fh);
  }


  public function logToFileException ($content)
  {
    $fh = fopen($this->logdirex, 'a');
    fwrite($fh, $this->TS() . ' - ' . $content . "\r\n");
    fclose($fh);
  }

  public function logToFileAlert ($content)
  {
    $fh = fopen($this->logdiralert, 'a');
    fwrite($fh, $this->TS() . ' - ' . $content . "\r\n");
    fclose($fh);
  }


  public function alertMail ($content)
  {

    $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8' . "\r\n" . 'From: noreply OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;

    mail($this->mailalertto, $this->mailsubject, $content, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');
    
  }


  public function reconMySql ()
  {
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }
  }

}

?>
