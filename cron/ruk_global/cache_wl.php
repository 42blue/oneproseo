<?php

// php cache_wl.php c 226

require_once dirname(__FILE__) . '/ruk.class.php';

class render_winner_looser extends ruk {

  public $logdir        = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir     = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $csv_dir       = '/var/www/enterprise.oneproseo.com/temp/export/';
  public $ops_version   = 'enterprise';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();
    $this->logToFile('CACHE: WINNER/LOOSER START');
    $this->fetchSets();
    $this->logToFile('CACHE: WINNER/LOOSER END');
    $this->db->close();

  }


  private function fetchSets ()
  {

    $sql = "SELECT id, url, name, team_email, country FROM ruk_project_customers";

    // BASH
    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, url, name, team_email, country FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    $sets = array();

    while ($row = $result->fetch_assoc()) {
      $sets[$row['id']] = array( 'name' => $row['name'], 'email' => $row['team_email'], 'url' => $row['url'], 'country' => $row['country']);
    }

    // exclude mostly weekly
    foreach ($sets as $key => $set) {
      $sql    = "SELECT measuring FROM ruk_project_keyword_sets WHERE id_customer = '$key'";
      $result = $this->db->query($sql);

      $only_weekly = true;

      while ($row = $result->fetch_assoc()) {
        if ($row['measuring'] == 'daily') {
          $only_weekly = false;
        }
      }
       if ($only_weekly == true) {
         unset($sets[$key]);
       }
    }


    foreach ($sets as $key => $set) {

      $this->project_id    = $key;
      $this->customer_name = $set['name'];
      $this->customer_url  = $set['url'];
      $this->country       = $set['country'];
      $this->alert_to      = $set['email'];

      // CUSTOMER VERSIONS AND HOSTNAMES
      $this->reconMySql();
      $sql    = "SELECT subdomain FROM oneproseo_customer_versions WHERE ruk_id = '$key'";
      $result = $this->db->query($sql);
      $row    = $result->fetch_array(MYSQLI_ASSOC);

      // CUSTOMER SUBDOMAIN
      $this->ops_version = 'enterprise';
      if (isset($row['subdomain'])) {
        $this->ops_version = $row['subdomain'];
      }

      echo 'PRO: ' . $this->project_id . PHP_EOL;

      // LOAD CACHE ONCE
      $this->loadCache();
      $this->loadCacheBest();

      $this->reconMySql();

      if (empty($this->project_cache)) {
        continue;
      }

      $keywords = array();
      $rankset  = array();

      foreach ($this->project_cache as $key => $value) {
        // no maps rankings
        // no mobile rankings

        if (stripos($value['l'], 'mobile') !== false || stripos($value['l'], 'maps') !== false) {
          continue;
        }

        $keywords[$value['k']] = $value['k'];
        $dates[$value['t']]    = $value['t'];
        $rankset['_da'][$value['k']][$value['t']][] = array ('rank' => $value['p'], 'ts' => $value['t'], 'url' => $value['u']);
      }

      // MULTIPLE RANKINGS
      $newest_date = end($dates);
      foreach ($rankset['_da'] as $key => $value) {
        if (isset($value[$newest_date]) && count($value[$newest_date]) > 1) {
          $rankset['_dc'][$key][] = $value[$newest_date];
        }
      }

      $adwordsloc = $this->getAdwordsLoc($this->country);
      $this->adwordsData = $this->getAdwordsDataI18N($keywords, $adwordsloc[0], $adwordsloc[1]);

      $dates = array();
      $dates[1] = array($this->dateYMD(), $this->dateYMDYesterday());
      //$dates[6] = array($this->dateYMD(), $this->dateYMDsixdays());

      foreach ($dates as $key => $date) {

        $this->tempfile = '';
        $this->tempfile_json = '';
        $this->emailcontent = '';

        // MULTIPLE RANKINGS
        if (isset($rankset['_dc'])) {
          $this->duplicateRankings($rankset['_dc']);
          if ($key == 1 && !empty ($set['email'])) {
            $this->sendAlertMailHTML($this->emailcontent);
          }
        }

        $this->renderKeywords($rankset['_da'], $date);

        $filename = 'wl/wl_' . $this->project_id .'_'.$key.'.tmp';
        $this->writeFileRaw($filename, $this->tempfile);

        $filename_json_w = 'json/wl_winner_' . $this->project_id .'.json';
        $filename_json_s = 'json/wl_same_' . $this->project_id .'.json';
        $filename_json_l = 'json/wl_loser_' . $this->project_id .'.json';

        $json_w = $this->prepareJson($this->tempfile_json_w);
        $json_s = $this->prepareJson($this->tempfile_json_s);
        $json_l = $this->prepareJson($this->tempfile_json_l);

        $this->writeFileRaw($filename_json_w, json_encode($json_w, JSON_PRETTY_PRINT));
        $this->writeFileRaw($filename_json_s, json_encode($json_s, JSON_PRETTY_PRINT));
        $this->writeFileRaw($filename_json_l, json_encode($json_l, JSON_PRETTY_PRINT));                

        if ($key == 1 && !empty ($set['email'])) {
          $this->sendAlertMailHTML($this->emailcontent);
        }

      }

    }

  }

  public function loadCache() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_x.tmp';
    $this->project_cache = $this->readFile($file);
  }

  public function loadCacheBest() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_best.tmp';
    $this->project_cache_best = $this->readFile($file);
  }


  public function prepareJson($arr) {

    $new = array();
    
    $x = 0;

    foreach ($arr as $key => $value) {

      if ($x > 249) {continue;}

      $new[] = array(
        'Keyword' => $value[0],
        'Rank'    => $value[3],
        'URL'     => $value[5],        
        'Change'  => $value[4],        
        'Best'    => $value[1],
        'OneDex'  => $value[2],
        'SV'      => $this->adwordsData[$value[0]][0]
      );
    
      $x++;

    }

    return $new;

  }


  private function duplicateRankings($set) {

    $this->emailcontent = '';
    $emailcontent = array();
    $amount = 0;

    foreach ($set as $keyword => $dateset) {

      // SKIP RANK 1
      if ($dateset[0][0]['rank'] == 1) {
        continue;
      }

      $amount++;

      $emailcontent[$keyword] = '<tr><td style="padding: 2px; border-bottom: 1px solid #444444;"><a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'/keywords/results/'.urlencode($keyword).'/'.$dateset[0][0]['ts'].'/'.$this->country.'" target="_blank">'.$keyword.'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;">';

      $i = 0;
      foreach ($dateset[0] as $rank) {

        if ($i == 0) {
          $emailcontent[$keyword] .= $rank['rank'];
        } else {
          $emailcontent[$keyword] .= ' / ' . $rank['rank'];
        }
        $i++;

      }

      $emailcontent[$keyword] .= '</td>'."\n".'</tr>';

    }


    ksort($emailcontent);

    $out = '';

    foreach ($emailcontent as $key => $content) {
      $out .= $content;
    }

    $this->subject_add = 'Multiple Rankings: ' . $amount;

    $this->emailcontent = '
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi SEO Team,<br />your daily multiple rankings update on:<br /><br />'."\n".'
          <span style="font-size: 38px; font-color: #636363;"><b>' .$this->customer_name . '</b><br /></span>
          <br />
          <b>' . $amount . '</b> multiple rankings found today (place 1 rankings excluded)</span>
          <br /><br /><br />
          <a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>
          <br /><br /></span>
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          '."\n".'<td rowspan="2" style="width:10px; background-color:#444444;"></td>'."\n".'
          '."\n".'<td style="vertical-align: top; width: 99%">
                    <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;">
                    <tr>'."\n".'<td style="width:50%; padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Keyword</b></td><td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Rankings</b></td>'."\n".'</tr>
                    '.$out.'
                    </table>
                  </td>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'

        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';

  }


  private function renderKeywords($set, $dates)
  {
    $dexToday            = 0;
    $dexYesterday        = 0;
    $out_sort_highestDex = array();
    $out_sort_win        = array();
    $out_sort_loose      = array();
    $out_sort_same       = array();

    foreach ($set as $keyword => $values) {

      if (empty($values)) {
        continue;
      }

      if (!isset($values[$dates[1]][0]['rank'])) {
        $values[$dates[1]][0]['rank'] = 101;
      };

      if (!isset($values[$dates[0]][0]['rank'])) {
        $values[$dates[0]][0]['rank'] = 101;
      }

      if (!isset($values[$dates[0]][0]['url'])) {
        $values[$dates[0]][0]['url'] = '';
      }

      if (!isset($values[$dates[1]][0]['url'])) {
        $values[$dates[1]][0]['url'] = '';
      }

      if (!isset($this->project_cache_best[$keyword])) {
        $this->project_cache_best[$keyword] = array('', '');
      }

      $change = $values[$dates[1]][0]['rank'] - $values[$dates[0]][0]['rank'];
      if ($change > 0)
      {
         $change = '+' . $change;
      }

      if (isset($this->adwordsData[$keyword])) {
        $dex = $this->calcOneDex($values[$dates[0]][0]['rank'], $this->adwordsData[$keyword][3]);
        $dex_yesterday = $this->calcOneDex($values[$dates[1]][0]['rank'], $this->adwordsData[$keyword][3]);
      } else {
        $dex = 0;
        $dex_yesterday = 0;
      }

      $dexToday = $dexToday + str_replace(',', '.', $dex);;
      $dexYesterday = $dexYesterday + str_replace(',', '.', $dex_yesterday);;

      // TOP Keywords DEX
      $out_sort_highestDex[] = array (
          'dex' => $dex,
          'email' => '<td style="padding: 2px; border-bottom: 1px solid #444444;"><a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$dex.' </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><a class="white" href="https://www.google.com/search?q='.urlencode($keyword).'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$change.' </td>'."\n".'</tr> '
      );

      if ($values[$dates[0]][0]['rank'] < $values[$dates[1]][0]['rank']) {
        $out_sort_win[] = array (
            'dex' => $dex,
            'csv' => array($keyword, $this->project_cache_best[$keyword][0], $dex, $values[$dates[0]][0]['rank'], $change, $values[$dates[0]][0]['url']),
            'val' => '<td class="kw"><a href="/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td><td class="center"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td><td class="fig"><small>' . $dex . '</small></td><td class="fig"><a class="white" href="'.$values[$dates[0]][0]['url'].'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td><td><div class="change m-green metric-small right"><span>'.$change.'</span></div></td><td><div class="change m-green metric-small right"><div class="arrow-up"></div></div></td></tr>',
            'email' => '<td style="padding: 2px; border-bottom: 1px solid #444444;"><a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$dex.' </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><a class="white" href="https://www.google.com/search?q='.urlencode($keyword).'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$change.' </td>'."\n".'</tr>'
            );
      } else if ($values[$dates[0]][0]['rank'] > $values[$dates[1]][0]['rank']) {
        $out_sort_loose[] = array (
            'dex' => $dex,
            'csv' => array($keyword, $this->project_cache_best[$keyword][0], $dex, $values[$dates[0]][0]['rank'], $change, $values[$dates[0]][0]['url']),
            'val' => '<td class="kw"><a href="/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td><td class="center"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td><td class="fig"><small>' . $dex . '</small></td><td class="fig"><a class="white" href="'.$values[$dates[0]][0]['url'].'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td><td><div class="change m-red metric-small right"><span>'.$change.'</span></div></td><td><div class="change m-red metric-small right"><div class="arrow-down"></div></div></td></tr>',
            'email' => '<td style="padding: 2px; border-bottom: 1px solid #444444;"><a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$dex.' </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><a class="white" href="https://www.google.com/search?q='.urlencode($keyword).'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$change.' </td>'."\n".'</tr> '
            );
      } else if ($values[$dates[0]][0]['rank'] == $values[$dates[1]][0]['rank']) {
        $out_sort_same[] = array (
            'dex' => $dex,
            'csv' => array($keyword, $this->project_cache_best[$keyword][0], $dex, $values[$dates[0]][0]['rank'], $change, $values[$dates[0]][0]['url']),
            'val' => '<td class="kw"><a href="/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td><td class="center"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td><td class="fig"><small>' . $dex . '</small></td><td class="fig"><a class="white" href="'.$values[$dates[0]][0]['url'].'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td><td><div class="change m-green metric-small right"><span>'.$change.'</span></div></td><td><div class="change m-green metric-small right"><div class="arrow-dash"></div></div></td></tr>',
            'email' => '<td style="padding: 2px; border-bottom: 1px solid #444444;"><a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'/keywords/details/'.urlencode($keyword).'/'.$this->country.'" target="_blank">'.$keyword.'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><small title="'.$this->project_cache_best[$keyword][1].'">'.$this->project_cache_best[$keyword][0].'</small></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$dex.' </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><a class="white" href="https://www.google.com/search?q='.urlencode($keyword).'" target="_blank">'.$values[$dates[0]][0]['rank'].'</a></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> '.$change.' </td>'."\n".'</tr> '
            );
      }

    }

    $outputCSV = array();
    $outputCSV_json_w = array();
    $outputCSV_json_l = array();
    $outputCSV_json_s = array();
    $outputCSV[] = array('Keyword', 'Best', 'OneDex', 'Rank', '+/-');

    // SORT DEX TOP 20
    $top_dex = array();
    foreach ($out_sort_highestDex as $key => $row)
    {
      $top_dex[$key] = $row['dex'];
    }

    array_multisort($top_dex, SORT_DESC, SORT_NATURAL, $out_sort_highestDex);

    $out_top20_email = '';
    $i = 0;
    foreach ($out_sort_highestDex as $key => $value) {
      $i++;
      if ($i > 20) {
        continue;
      }
      $out_top20_email .= '<tr>'."\n".' <td style="padding: 2px; border-bottom: 1px solid #444444;">' .$i. '</td>' . $value['email'];
    }

    // SORT DEX WINNER$out_sort_win
    $win_dex = array();
    foreach ($out_sort_win as $key => $row)
    {
      $win_dex[$key] = $row['dex'];
    }

    array_multisort($win_dex, SORT_DESC, SORT_NATURAL, $out_sort_win);

    $out_win = '';
    $out_win_email = '';
    $i = 0;
    foreach ($out_sort_win as $key => $value) {
      $i++;
      $outputCSV[] = $value['csv'];
      if ($i > 100) {
        continue;
      }
      $outputCSV_json_w[] = $value['csv'];
      $out_win .= '<tr><td class="kw"><small>' .$i. '</small></td>' . $value['val'];
      $out_win_email .= '<tr>'."\n".' <td style="padding: 2px; border-bottom: 1px solid #444444;">' .$i. '</td>' . $value['email'];
    }

    // SORT DEX LOOSER
    $loose_dex = array();
    foreach ($out_sort_loose as $key => $row)
    {
      $loose_dex[$key] = $row['dex'];
    }

    array_multisort($loose_dex, SORT_DESC, SORT_NATURAL, $out_sort_loose);

    $out_loose = '';
    $out_loose_email = '';
    $i = 0;
    foreach ($out_sort_loose as $key => $value) {
      $i++;
      $outputCSV[] = $value['csv'];
      if ($i > 100) {
        continue;
      }
      $outputCSV_json_l[] = $value['csv'];
      $out_loose .= '<tr><td class="kw"><small>' .$i. '</small></td>' . $value['val'];
      $out_loose_email .= '<tr>'."\n".' <td style="padding: 2px; border-bottom: 1px solid #444444;">' .$i. '</td>' . $value['email'];
    }

    // SORT DEX SAME
    $same_dex = array();
    foreach ($out_sort_same as $key => $row)
    {
      $same_dex[$key] = $row['dex'];
    }

    array_multisort($same_dex, SORT_DESC, SORT_NATURAL, $out_sort_same);

    $out_same = '';
    $out_same_email = '';
    $i = 0;
    foreach ($out_sort_same as $key => $value) {
      $i++;
      $outputCSV[] = $value['csv'];
      if ($i > 100) {
        continue;
      }
      $outputCSV_json_s[] = $value['csv'];
      $out_same .= '<tr><td class="kw"><small>' .$i. '</small></td>' . $value['val'];
      $out_same_email .= '<tr>'."\n".' <td style="padding: 2px; border-bottom: 1px solid #444444;">' .$i. '</td>' . $value['email'];
    }

    $this->csv_name = 'winner_looser_' . $this->project_id;
    $this->writeCsv($outputCSV, $this->csv_name);

    $this->tempfile_json_w = $outputCSV_json_w;
    $this->tempfile_json_l = $outputCSV_json_l;
    $this->tempfile_json_s = $outputCSV_json_s;

    $this->tempfile = '
      <div class="cf-wl-float">
        <table class="cf-table">
          <tbody><tr><td class="kw"><b>#</b></td><td class="kw"><b>Keywords mit verbesserten Rankings</b></td><td class="kw"><b>Best</b></td><td class="kw"><b>OneDex</b></td><td class="kw"><b>Rang</b></td><td class="kw"><b class="right">+</b></td><td class="kw"></td></tr>' . $out_win  . '</tbody>
        </table>
      </div>
      <div class="cf-wl-float">
        <table class="cf-table">
          <tbody><tr><td class="kw"><b>#</b></td><td class="kw"><b>Keywords mit unveränderten Rankings</b></td><td class="kw"><b>Best</b></td><td class="kw"><b>OneDex</b></td><td class="kw"><b>Rang</b></td><td class="kw"><b class="right">-</b></td><td class="kw"></td></tr>' . $out_same  . '</tbody>
        </table>
      </div>
      <div class="cf-wl-float">
        <table class="cf-table">
          <tbody><tr><td class="kw"><b>#</b></td><td class="kw"><b>Keywords mit verschlechterten Rankings</b></td><td class="kw"><b>Best</b></td><td class="kw"><b>OneDex</b></td><td class="kw"><b>Rang</b></td><td class="kw"><b class="right">-</b></td><td class="kw"></td></tr>' .  $out_loose  . '</tbody>
        </table>
      </div>'
      ;


    $count_all = count ($out_sort_win) + count ($out_sort_loose) + count ($out_sort_same);

    if (count ($out_sort_win) > count ($out_sort_loose)) {

      $diff = count ($out_sort_win) - count ($out_sort_loose);
      $change_r = $diff * 100 / $count_all;
      $change_rs = '<span style="color: green;"> +'.round($change_r, 2).'%</span>';
      $change_r = '+' . round($change_r, 2);

    } else if (count ($out_sort_win) < count ($out_sort_loose)) {

      $diff = count ($out_sort_loose) - count ($out_sort_win);
      $change_r = $diff * 100 / $count_all;
      $change_rs = '<span style="color: red;"> -'.round($change_r, 2).'%</span>';
      $change_r = '-' . round($change_r, 2);

    } else {

      $change_r = '0';
      $change_rs = '<span style="color: green;">0%</span>';

    }

    $change_rankings = 'Rankings: ' . $change_rs ;

    if (!is_null($dexYesterday) && !is_null($dexToday) && $dexYesterday !== 0 && $dexToday !== 0) {
      $dexChange  = $dexToday * 100 / $dexYesterday;
      $dexpercent = $dexChange - 100;
      $dexpercent = round($dexpercent, 2);
      if ($dexpercent > 0) {
        $dexpercent = '+' . $dexpercent;
      }
    } else {
      $dexpercent = 0;
    }

    $this->subject_add = 'Rankings: ' . $change_r . '% | OneDex: ' . $dexpercent . '%';

    if ($dexpercent < 0) {
      $dexpercent = '<span style="color: red;">' . $dexpercent . '%</span>';
    } else {
      $dexpercent = '<span style="color: green;">' . $dexpercent . '%</span>';
    }

    $this->emailcontent = '
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
            <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
          </td>
        </tr>
        <tr>
          '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
         <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
          <span style="font-size: 16px; font-color: #636363;">
          Hi SEO Team,<br />your daily update on:<br /><br />'."\n".'
          <span style="font-size: 38px; font-color: #636363;"><b>' .$this->customer_name . '</b><br /></span>
          <span style="font-size: 33px; font-color: #636363;"><b>OneDex: ' . $dexpercent.'</b>
          <br />
          <b>' . $change_rankings . '</b></span>
          <br /><br /><br />
          <a href="https://'.$this->ops_version.'.oneproseo.com/rankings/overview/'.$this->project_id.'" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>
          <a href="https://enterprise.oneproseo.com/temp/export/'.$this->csv_name .'.csv" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">CSV download</a><br /><br /><br />
          <a href="https://enterprise.oneproseo.com/temp/zip/c-'. $this->project_id .'.zip" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">CSV download OneDex 12 months</a>
          <br /><br /></span>
        </td>
        '."\n".'<td style="width:10px; background-color:#444444;"></td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'
          '."\n".'<td style="vertical-align: top; width: 99%">
                    <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;">
                    <tr>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">#</b></td><td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Top 20 Keywords</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">Best</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">OneDex</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Rank</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">+/-</b> </td>'."\n".'</tr>
                    '.$out_top20_email.' </table>
                  </td>
          '."\n".'<td rowspan="7" style="width:10px; background-color:#444444;"></td>'."\n".'
        <tr>
          '."\n".'<td style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          <td>
            <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;">
            <tr>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">#</b></td><td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Winner</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">Best</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">OneDex</b> </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Rank</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">+/-</b> </td>'."\n".'</tr>
           '. $out_win_email.' </table> </td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          <td>
            <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;">
            <tr>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">#</b></td><td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Unchanged</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">Best</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">OneDex</b> </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Rank</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">+/-</b> </td>'."\n".'</tr>
           '.$out_same_email.' </table> </td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
        <tr>
          <td>
            <table border="0" cellspacing="0" cellpadding="0" style="border-collapse: collapse; font-size: 16px; border: 1px solid #fff; width: 100%;">
            <tr>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">#</b></td><td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Loser</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">Best</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">OneDex</b> </td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"><b style="font-size:20px;">Rank</b></td>'."\n".'<td style="padding: 2px; border-bottom: 1px solid #444444;"> <b style="font-size:20px;">+/-</b> </td>'."\n".'</tr>
           '.$out_loose_email.' </table> </td>'."\n".'
        </tr>
        <tr>
          '."\n".'<td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
        </tr>
      </table>';

  }



  // DEX
  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return number_format($dex, 2, ',', '.');

  }

  private function sendAlertMailHTML ($data)
  {

    // ALERT EMAIL ONLY AT 7
    $hours = date('H',time());

    if ($hours >= 5 && $hours < 6) {

      $sendto  = $this->alert_to;
      //$sendto  = 'm.hotz@advertising.de';

      $subject  = 'OneProSeo Daily | '.$this->customer_name .' | ' . $this->subject_add;
      $header   = 'MIME-Version: 1.0' . "\r\n" . 'Content-Type: text/html; charset=utf-8' . "\r\n" . 'From: OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;
      $message  = '<html><head> <style>table {border-spacing: 0;}</style></head><body>';
      $message .= $data;
      $message .= '</body></html>';

      mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

    }

  }

  public function writeFileRaw ($fileName, $variable)
  {
    $fileName = $this->cache_dir . $fileName;
    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);
  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {
      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);
      $variable = unserialize($variable);
      return $variable;
    } else {
      return null;
    }

  }

  public function writeCsv ($data, $file) {

    $fp = fopen($this->csv_dir . $file . '.csv', 'w');
    foreach ($data as $fields) {
      fputcsv($fp, $fields);
    }
    fclose($fp);
  }

  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }

  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }

  public function dateYMDsixdays ()
  {
    return date('Y-m-d', strtotime('-6 days'));
  }

}

new render_winner_looser ($argv);

?>
