<?php

require_once dirname(__FILE__) . '/ruk.class.php';

class purge extends ruk  {

  public function __construct () {

    parent::mySqlConnect();

    $this->fetchKeywordsFromProjects();

  }


  private function fetchKeywordsFromProjects ()
  {

    $data  = array();
    $rem   = array();

    $sql  = "SELECT id, keyword, searchvolume, country FROM gen_keywords";
    $res1 = $this->db->query($sql);

    while ($row = $res1->fetch_assoc()) {

      $x = $row['keyword'] . $row['country'];

      if (isset($data[$x])) {

        if ($data[$x][3] < $row['searchvolume']) {
          $rem[$data[$x][0]] = $data[$x][1] . $data[$x][3];
        } else {
          $rem[$row['id']] = $row['keyword'] . $row['searchvolume'];
        }

      } else {

        $data[$x] = array($row['id'], $row['keyword'] , $row['country'] , $row['searchvolume']);

      }

    }

    // remove duplicates with lowest search volume
    foreach ($rem as $def => $value) {
      $sql = "DELETE FROM gen_keywords WHERE id = $def";
      $res2 = $this->db->query($sql);
    }

  }

}

new purge;

?>
