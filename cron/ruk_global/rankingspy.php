<?php

/*
Hier importieren wir die Rankings pro Tag
Doku: http://www.ranking-spy.com/ranking/webserviceapi/doc/index.html?com/atenis/rankingspy/web/webservice/RankingSpyService.html
*/
ini_set("soap.wsdl_cache_enabled", "0");
require_once dirname(__FILE__) . '/ruk.class.php';

class rankingspy_keywordupdate extends ruk {

  public  $logdiralert  = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_alert.txt';

  public  $logdir       = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog_rs.txt';
  public  $logdirex     = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/exceptionlog.txt';
  public  $mailalertto  = 'matthias.hotz@gmail.com';
  public  $mailsubject  = 'RaSpy ALERT';
  private $soapClient   = 'http://www.ranking-spy.com/oneadvertisingapi1611/services/RankingSpyService?wsdl';
  private $user         = 'advertisingAdmin';
  private $pass         = 'lucile089open!';
  private $userchannel  = 19595;
  private $timeoffset   = 'PT12H';
  private $timeoffsetRS = 'PT12H';
  private $time_elapsed = 0;
  // data 2 days ago
  //private $timeoffsetRS = 'PT48H';


  public function __construct ()
  {

    $this->mySqlConnect();

    $this->rs_client = new SoapClient($this->soapClient, array('trace' => 0));

    $this->logToFile('ONEPROSEO TIME: ' . $this->dateYMD() . ' - RASPY TIME: ' . $this->dateYMDRS());

    $this->retrieveSearchRunIdsByUserId();
    $this->startSearchRuns();

    $this->db->close();

  }


  private function retrieveSearchRunIdsByUserId ()
  {

    try {

      $startDate        = $this->dateYMDRS();
      $endDate          = $this->dateYMDRS();
      // SUMMERTIME
      $result           = $this->rs_client->retrieveSearchRunIdsByUserId($this->user, $this->pass, $this->userchannel, $startDate . 'T00:00:00.000+02:00', $endDate . 'T24:00:00.000+02:00');
      // WINTERTIME
      //$result           = $this->rs_client->retrieveSearchRunIdsByUserId($this->user, $this->pass, $this->userchannel, $startDate . 'T00:00:00.000+01:00', $endDate . 'T24:00:00.000+01:00');

      $this->logToFile('RS ' . count($result) . ' SEARCH RUNS');
      if (count($result) < 85000) {
        $this->alertMail('RS ' . count($result) . ' SEARCH RUNS');
      }
      $this->searchRuns = $result;

    } catch (SOAPFault $f) {
      echo $f->faultstring;
      $this->logToFileException($f->faultstring);
      $this->alertMail($f->faultstring);
      $this->searchRuns = false;

    }

  }

  private function startSearchRuns ()
  {

    if ($this->searchRuns === false) {
      $this->logToFileException('FALSE retrieveSearchRunIdsByUserId');
      $this->alertMail('FALSE retrieveSearchRunIdsByUserId');
      exit;
    }

    if (count($this->searchRuns) == 0) {
      $this->logToFile('RS DONE');
      exit;
    }

   $this->logToFile('RS START LOOP');

   $this->loopSearchRuns();

  }


  private function loopSearchRuns ()
  {

    $freq = 1000;
    $i    = 0;

    foreach ($this->searchRuns as $id => $runid) {
      echo $i++ . "\n";
      if ($i % $freq  == ($freq - 1)) {
        $this->logToFile('RS LOOPS: ' . $i . ' - MYSQL QRY TIME (AVG): ' . $this->time_elapsed);
        $this->time_elapsed = 0;
      }

      $this->getRankings($runid);

    }

    $this->logToFile('RS DONE');

  }


  private function getRankings ($runid)
  {

    try {

      $results_kw = $this->rs_client->retrieveSearchRun($this->user, $this->pass, $runid);
      $this->reconMySql();

      if (!empty($results_kw->details)) {

        $lang         = $this->selectLanguage($results_kw->searchEngineId);
        $serp_keyword = $this->db->real_escape_string($results_kw->keyword);

        // check if in db if yes skip
        if ($this->checkIfAlreadyScraped($serp_keyword, $lang) == false) {
          return;
        }

        $start_time = microtime(true);


        $sql_kw       = "('".$serp_keyword."', '".$lang."', '" . $this->dateYMD() . "')";
        $query_kw     = 'INSERT INTO ruk_scrape_keywords (keyword, language, timestamp) VALUES '. $sql_kw .' ';
        $this->db->query($query_kw);

        $last_insert_id = $this->db->insert_id;

        $sql_ra = array();

        foreach ($results_kw->details as $resultElement) {
          $pure_host  = str_ireplace('www.', '', parse_url($resultElement->matchLink, PHP_URL_HOST));
          $serp_url   = $this->db->real_escape_string($resultElement->matchLink);
          $pure_host  = $this->db->real_escape_string($pure_host);
          $sql_ra[]   = "('".$last_insert_id."', '".$resultElement->absPos."', '".$serp_url."', 0, 0, '".$pure_host."', 0)";
        }

        $q_string_ra = implode(',', $sql_ra);

         // add to keyword ranking table
        $query_ra = 'INSERT INTO ruk_scrape_rankings (id_kw, position, url, title, description, hostname, meta) VALUES '. $q_string_ra .' ';
        $res = $this->db->query($query_ra);

        // TIMELOGGER
        $end_time = microtime(true) - $start_time;
        $div_time = $this->time_elapsed + $end_time;
        $this->time_elapsed = $div_time / 2;

        if (!empty($this->db->error)) {
          $this->logToFileException('RS: DB ERROR QUERY: ' . $serp_keyword . ' - ' . $q_string_ra . ' -> ' . $this->db->error);
        }

      } else {
        $lang         = $this->selectLanguage($results_kw->searchEngineId);
        $this->logToFileException('RS: EMPTY RANKINGS (TOP100): ' . $results_kw->keyword . ' / ' . $lang . ' / RUN ID: ' . $runid);

      }


    } catch (SOAPFault $f) {

      $this->logToFileException('RS: SOAP ERROR: ' . $f->faultcode . ' / ' . $f->faultstring);
      $this->logToFile('RS: SOAP ERROR RUN ID: ' . $runid);

    }

  }


  private function checkIfAlreadyScraped ($keyword, $lang)
  {

    $today = $this->dateYMD();

    $sql = "SELECT id FROM ruk_scrape_keywords WHERE keyword = '$keyword' AND language = '$lang' AND timestamp = '$today'";
    $result = $this->db->query($sql);

    if (!empty($this->db->error)) {
      $this->logToFile('RS: checkIfAlreadyScraped ERROR: ' . $this->db->error);
    }

    if ($result->num_rows > 0) {
      return false;
    } else {
      return true;
    }

  }


  private function dateYMDRS ()
  {
    $date = new DateTime();
    $date->sub(new DateInterval($this->timeoffsetRS));
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);
  }

  private function dateYMD ()
  {
    $date = new DateTime();
    $date->add(new DateInterval($this->timeoffset));
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);
  }


  private function selectLanguage ($searchengineid) {

    $searchid       = array();

		$searchid[9001886] = 'ad';
		$searchid[9001887] = 'maps_ad';
		$searchid[9001888] = 'mobile_ad';
		$searchid[521] = 'ae';
		$searchid[6000] = 'maps_ae';
		$searchid[2017] = 'mobile_ae';
		$searchid[9001871] = 'af';
		$searchid[9001873] = 'maps_af';
		$searchid[9001875] = 'mobile_af';
		$searchid[9001898] = 'ag';
		$searchid[9001899] = 'maps_ag';
		$searchid[9001900] = 'mobile_ag';
		$searchid[9001895] = 'ai';
		$searchid[9001896] = 'maps_ai';
		$searchid[9001897] = 'mobile_ai';
		$searchid[9002520] = 'al';
		$searchid[9002521] = 'maps_al';
		$searchid[9002522] = 'mobile_al';
		$searchid[9001901] = 'am';
		$searchid[9001902] = 'maps_am';
		$searchid[9001903] = 'mobile_am';
		$searchid[9001889] = 'ao';
		$searchid[9001891] = 'maps_ao';
		$searchid[9001893] = 'mobile_ao';
		$searchid[310] = 'ar';
		$searchid[6015] = 'maps_ar';
		$searchid[2024] = 'mobile_ar';
		$searchid[9001883] = 'as';
		$searchid[9001884] = 'maps_as';
		$searchid[9001885] = 'mobile_as';
		$searchid[10] = 'at';
		$searchid[2402] = 'maps_at';
		$searchid[2001] = 'mobile_at';
		$searchid[405] = 'au';
		$searchid[6016] = 'maps_au';
		$searchid[2025] = 'mobile_au';
		$searchid[9001904] = 'az';
		$searchid[9001906] = 'maps_az';
		$searchid[9001908] = 'mobile_az';
		$searchid[9001942] = 'ba';
		$searchid[9001944] = 'maps_ba';
		$searchid[9001946] = 'mobile_ba';
		$searchid[9001916] = 'bd';
		$searchid[9001917] = 'maps_bd';
		$searchid[9001918] = 'mobile_bd';
		$searchid[400] = 'be';
		$searchid[6001] = 'maps_be';
		$searchid[9001923] = 'mobile_be';
		$searchid[401] = 'be-nl';
		$searchid[6001] = 'maps_be-nl';
		$searchid[2018] = 'mobile_be-nl';
		$searchid[9001958] = 'bf';
		$searchid[9001959] = 'maps_bf';
		$searchid[9001960] = 'mobile_bf';
		$searchid[501] = 'bg';
		$searchid[6002] = 'maps_bg';
		$searchid[9001957] = 'mobile_bg';
		$searchid[9001913] = 'bh';
		$searchid[9001914] = 'maps_bh';
		$searchid[9001915] = 'mobile_bh';
		$searchid[9001961] = 'bi';
		$searchid[9001963] = 'maps_bi';
		$searchid[9001965] = 'mobile_bi';
		$searchid[9001930] = 'bj';
		$searchid[9001932] = 'maps_bj';
		$searchid[9001934] = 'mobile_bj';
		$searchid[9001951] = 'bn';
		$searchid[9001953] = 'maps_bn';
		$searchid[9001955] = 'mobile_bn';
		$searchid[9001936] = 'bo';
		$searchid[9001938] = 'maps_bo';
		$searchid[9001940] = 'mobile_bo';
		$searchid[150] = 'br';
		$searchid[6017] = 'maps_br';
		$searchid[2007] = 'mobile_br';
		$searchid[9001910] = 'bs';
		$searchid[9001911] = 'maps_bs';
		$searchid[9001912] = 'mobile_bs';
		$searchid[9001948] = 'bw';
		$searchid[9001949] = 'maps_bw';
		$searchid[9001950] = 'mobile_bw';
		$searchid[522] = 'by';
		$searchid[9001919] = 'maps_by';
		$searchid[9001920] = 'mobile_by';
		$searchid[9001924] = 'bz';
		$searchid[9001926] = 'maps_bz';
		$searchid[9001928] = 'mobile_bz';
		$searchid[130] = 'ca';
		$searchid[6004] = 'maps_ca';
		$searchid[2019] = 'mobile_ca';
		$searchid[131] = 'ca-fr';
		$searchid[6004] = 'maps_ca-fr';
		$searchid[2019] = 'mobile_ca-fr';
		$searchid[9002336] = 'cat';
		$searchid[9002337] = 'maps_cat';
		$searchid[9002338] = 'mobile_cat';
		$searchid[9001998] = 'cd';
		$searchid[9002000] = 'maps_cd';
		$searchid[9002002] = 'mobile_cd';
		$searchid[9001978] = 'cf';
		$searchid[9001979] = 'maps_cf';
		$searchid[9001980] = 'mobile_cf';
		$searchid[9001992] = 'cg';
		$searchid[9001994] = 'maps_cg';
		$searchid[9001996] = 'mobile_cg';
		$searchid[13] = 'ch';
		$searchid[2403] = 'maps_ch';
		$searchid[2002] = 'mobile_ch';
		$searchid[700]  = 'ch-fr';
		$searchid[1811] = 'ch-fr';
		$searchid[2403] = 'maps_ch-fr';
		$searchid[2002] = 'mobile_ch-fr';
		$searchid[701] = 'ch-it';
		$searchid[9002411] = 'maps_ch-it';
		$searchid[9002413] = 'mobile_ch-it';		
		$searchid[9002010] = 'ci';
		$searchid[9002011] = 'maps_ci';
		$searchid[9002012] = 'mobile_ci';
		$searchid[9002004] = 'ck';
		$searchid[9002005] = 'maps_ck';
		$searchid[9002006] = 'mobile_ck';
		$searchid[300] = 'cl';
		$searchid[6005] = 'maps_cl';
		$searchid[2013] = 'mobile_cl';
		$searchid[9001970] = 'cm';
		$searchid[9001971] = 'maps_cm';
		$searchid[9001972] = 'mobile_cm';
		$searchid[320] = 'co';
		$searchid[9001990] = 'maps_co';
		$searchid[9001991] = 'mobile_co';
		$searchid[26] = 'us';
		$searchid[2401] = 'maps_us';
		$searchid[2400] = 'mobile_us';
		$searchid[9002007] = 'cr';
		$searchid[9002008] = 'maps_cr';
		$searchid[9002009] = 'mobile_cr';
		$searchid[9002013] = 'cu';
		$searchid[9002014] = 'maps_cu';
		$searchid[9002015] = 'mobile_cu';
		$searchid[9001975] = 'cv';
		$searchid[9001976] = 'maps_cv';
		$searchid[9001977] = 'mobile_cv';
		$searchid[9002016] = 'cy';
		$searchid[9002018] = 'maps_cy';
		$searchid[9002020] = 'mobile_cy';
		$searchid[113] = 'cz';
		$searchid[6033] = 'maps_cz';
		$searchid[2031] = 'mobile_cz';
		$searchid[15] = 'de';
		$searchid[116] = 'maps_de';
		$searchid[2000] = 'mobile_de';
		$searchid[9002025] = 'dj';
		$searchid[9002027] = 'maps_dj';
		$searchid[9002029] = 'mobile_dj';
		$searchid[112] = 'dk';
		$searchid[6034] = 'maps_dk';
		$searchid[2008] = 'mobile_dk';
		$searchid[9002031] = 'dm';
		$searchid[9002032] = 'maps_dm';
		$searchid[9002033] = 'mobile_dm';
		$searchid[9002034] = 'do';
		$searchid[9002035] = 'maps_do';
		$searchid[9002036] = 'mobile_do';
		$searchid[9001877] = 'dz';
		$searchid[9001879] = 'maps_dz';
		$searchid[9001881] = 'mobile_dz';
		$searchid[370] = 'ec';
		$searchid[6019] = 'maps_ec';
		$searchid[2027] = 'mobile_ec';
		$searchid[9002041] = 'ee';
		$searchid[9002043] = 'maps_ee';
		$searchid[9002045] = 'mobile_ee';
		$searchid[500] = 'eg';
		$searchid[6020] = 'maps_eg';
		$searchid[9002037] = 'mobile_eg';
		$searchid[21] = 'es';
		$searchid[6035] = 'maps_es';
		$searchid[2009] = 'mobile_es';
		$searchid[9002047] = 'et';
		$searchid[9002049] = 'maps_et';
		$searchid[9002051] = 'mobile_et';
		$searchid[23] = 'fi';
		$searchid[6036] = 'maps_fi';
		$searchid[2014] = 'mobile_fi';
		$searchid[9002053] = 'fj';
		$searchid[9002054] = 'maps_fj';
		$searchid[9002055] = 'mobile_fj';
		$searchid[9002235] = 'fm';
		$searchid[9002236] = 'maps_fm';
		$searchid[9002237] = 'mobile_fm';
		$searchid[20] = 'fr';
		$searchid[6037] = 'maps_fr';
		$searchid[2006] = 'mobile_fr';
		$searchid[9002059] = 'ga';
		$searchid[9002060] = 'maps_ga';
		$searchid[9002061] = 'mobile_ga';
		$searchid[9002068] = 'ge';
		$searchid[9002069] = 'maps_ge';
		$searchid[9002070] = 'mobile_ge';
		$searchid[9002092] = 'gg';
		$searchid[9002094] = 'maps_gg';
		$searchid[9002096] = 'mobile_gg';
		$searchid[9002071] = 'gh';
		$searchid[9002073] = 'maps_gh';
		$searchid[9002075] = 'mobile_gh';
		$searchid[9002077] = 'gi';
		$searchid[9002079] = 'maps_gi';
		$searchid[9002081] = 'mobile_gi';
		$searchid[9002085] = 'gl';
		$searchid[9002086] = 'maps_gl';
		$searchid[9002087] = 'mobile_gl';
		$searchid[9002062] = 'gm';
		$searchid[9002064] = 'maps_gm';
		$searchid[9002066] = 'mobile_gm';
		$searchid[9002088] = 'gp';
		$searchid[9002089] = 'maps_gp';
		$searchid[9002090] = 'mobile_gp';
		$searchid[9000002] = 'gr';
		$searchid[9002083] = 'maps_gr';
		$searchid[9002084] = 'mobile_gr';
		$searchid[350] = 'gt';
		$searchid[6021] = 'maps_gt';
		$searchid[9002091] = 'mobile_gt';
		$searchid[9002098] = 'gy';
		$searchid[9002099] = 'maps_gy';
		$searchid[9002100] = 'mobile_gy';
		$searchid[404] = 'hk';
		$searchid[9002110] = 'maps_hk';
		$searchid[9002111] = 'mobile_hk';
		$searchid[9002107] = 'hn';
		$searchid[9002108] = 'maps_hn';
		$searchid[9002109] = 'mobile_hn';
		$searchid[504] = 'hr';
		$searchid[6038] = 'maps_hr';
		$searchid[2032] = 'mobile_hr';
		$searchid[9002101] = 'ht';
		$searchid[9002103] = 'maps_ht';
		$searchid[9002105] = 'mobile_ht';
		$searchid[125] = 'hu';
		$searchid[6039] = 'maps_hu';
		$searchid[2033] = 'mobile_hu';
		$searchid[9002117] = 'id';
		$searchid[9002119] = 'maps_id';
		$searchid[9002121] = 'mobile_id';
		$searchid[9101] = 'ie';
		$searchid[9002130] = 'maps_ie';
		$searchid[9103] = 'mobile_ie';
		$searchid[950] = 'il';
		$searchid[9002136] = 'maps_il';
		$searchid[9002138] = 'mobile_il';
		$searchid[9002133] = 'im';
		$searchid[9002134] = 'maps_im';
		$searchid[9002135] = 'mobile_im';
		$searchid[60] = 'in';
		$searchid[2405] = 'maps_in';
		$searchid[2015] = 'mobile_in';
		$searchid[9002123] = 'iq';
		$searchid[9002125] = 'maps_iq';
		$searchid[9002127] = 'mobile_iq';
		$searchid[9002112] = 'is';
		$searchid[9002113] = 'maps_is';
		$searchid[9002114] = 'mobile_is';
		$searchid[27] = 'it';
		$searchid[6040] = 'maps_it';
		$searchid[2003] = 'mobile_it';
		$searchid[9002143] = 'je';
		$searchid[9002145] = 'maps_je';
		$searchid[9002147] = 'mobile_je';
		$searchid[9002140] = 'jm';
		$searchid[9002141] = 'maps_jm';
		$searchid[9002142] = 'mobile_jm';
		$searchid[9002149] = 'jo';
		$searchid[9002150] = 'maps_jo';
		$searchid[9002151] = 'mobile_jo';
		$searchid[407] = 'jp';
		$searchid[6008] = 'maps_jp';
		$searchid[2020] = 'mobile_jp';
		$searchid[9002157] = 'ke';
		$searchid[9002158] = 'maps_ke';
		$searchid[9002159] = 'mobile_ke';
		$searchid[9002166] = 'kg';
		$searchid[9002168] = 'maps_kg';
		$searchid[9002170] = 'mobile_kg';
		$searchid[9001967] = 'kh';
		$searchid[9001968] = 'maps_kh';
		$searchid[9001969] = 'mobile_kh';
		$searchid[9002160] = 'ki';
		$searchid[9002161] = 'maps_ki';
		$searchid[9002162] = 'mobile_ki';
		$searchid[513] = 'kr';
		$searchid[6009] = 'maps_kr';
		$searchid[2021] = 'mobile_kr';
		$searchid[9002163] = 'kw';
		$searchid[9002164] = 'maps_kw';
		$searchid[9002165] = 'mobile_kw';
		$searchid[503] = 'kz';
		$searchid[9002153] = 'maps_kz';
		$searchid[9002155] = 'mobile_kz';
		$searchid[9002172] = 'la';
		$searchid[9002173] = 'maps_la';
		$searchid[9002174] = 'mobile_la';
		$searchid[9002181] = 'lb';
		$searchid[9002183] = 'maps_lb';
		$searchid[9002185] = 'mobile_lb';
		$searchid[925] = 'li';
		$searchid[6041] = 'maps_li';
		$searchid[2034] = 'mobile_li';
		$searchid[9002404] = 'lk';
		$searchid[9002406] = 'maps_lk';
		$searchid[9002408] = 'mobile_lk';
		$searchid[9002187] = 'ls';
		$searchid[9002189] = 'maps_ls';
		$searchid[9002191] = 'mobile_ls';
		$searchid[7200] = 'lt';
		$searchid[9002196] = 'maps_lt';
		$searchid[9002197] = 'mobile_lt';
		$searchid[911] = 'lu';
		$searchid[9002198] = 'maps_lu';
		$searchid[9002199] = 'mobile_lu';
		$searchid[910] = 'lu-de';
		$searchid[6042] = 'maps_lu-de';
		$searchid[2035] = 'mobile_lu-de';
		$searchid[9002175] = 'lv';
		$searchid[9002177] = 'maps_lv';
		$searchid[9002179] = 'mobile_lv';
		$searchid[2701] = 'ly';
		$searchid[6022] = 'maps_ly';
		$searchid[2703] = 'mobile_ly';
		$searchid[9002256] = 'ma';
		$searchid[9002258] = 'maps_ma';
		$searchid[9002260] = 'mobile_ma';
		$searchid[9002238] = 'md';
		$searchid[9002240] = 'maps_md';
		$searchid[9002242] = 'mobile_md';
		$searchid[9002247] = 'me';
		$searchid[9002249] = 'maps_me';
		$searchid[9002251] = 'mobile_me';
		$searchid[9002203] = 'mg';
		$searchid[9002205] = 'maps_mg';
		$searchid[9002207] = 'mobile_mg';
		$searchid[9002200] = 'mk';
		$searchid[9002201] = 'maps_mk';
		$searchid[9002202] = 'mobile_mk';
		$searchid[9002220] = 'ml';
		$searchid[9002221] = 'maps_ml';
		$searchid[9002222] = 'mobile_ml';
		$searchid[9002244] = 'mn';
		$searchid[9002245] = 'maps_mn';
		$searchid[9002246] = 'mobile_mn';
		$searchid[9002253] = 'ms';
		$searchid[9002254] = 'maps_ms';
		$searchid[9002255] = 'mobile_ms';
		$searchid[9002223] = 'mt';
		$searchid[9002225] = 'maps_mt';
		$searchid[9002227] = 'mobile_mt';
		$searchid[9002229] = 'mu';
		$searchid[9002231] = 'maps_mu';
		$searchid[9002233] = 'mobile_mu';
		$searchid[9002217] = 'mv';
		$searchid[9002218] = 'maps_mv';
		$searchid[9002219] = 'mobile_mv';
		$searchid[9002209] = 'mw';
		$searchid[9002211] = 'maps_mw';
		$searchid[9002213] = 'mobile_mw';
		$searchid[151] = 'mx';
		$searchid[6023] = 'maps_mx';
		$searchid[2016] = 'mobile_mx';
		$searchid[403] = 'my';
		$searchid[9002215] = 'maps_my';
		$searchid[9002216] = 'mobile_my';
		$searchid[9002262] = 'mz';
		$searchid[9002264] = 'maps_mz';
		$searchid[9002266] = 'mobile_mz';
		$searchid[505] = 'na';
		$searchid[6024] = 'maps_na';
		$searchid[9002269] = 'mobile_na';
		$searchid[9002285] = 'ne';
		$searchid[9002287] = 'maps_ne';
		$searchid[9002289] = 'mobile_ne';
		$searchid[9002300] = 'nf';
		$searchid[9002301] = 'maps_nf';
		$searchid[9002302] = 'mobile_nf';
		$searchid[9002291] = 'ng';
		$searchid[9002293] = 'maps_ng';
		$searchid[9002295] = 'mobile_ng';
		$searchid[9002282] = 'ni';
		$searchid[9002283] = 'maps_ni';
		$searchid[9002284] = 'mobile_ni';
		$searchid[70] = 'nl';
		$searchid[6043] = 'maps_nl';
		$searchid[2004] = 'mobile_nl';
		$searchid[124] = 'no';
		$searchid[6044] = 'maps_no';
		$searchid[2010] = 'mobile_no';
		$searchid[9002274] = 'np';
		$searchid[9002275] = 'maps_np';
		$searchid[9002276] = 'mobile_np';
		$searchid[9002271] = 'nr';
		$searchid[9002272] = 'maps_nr';
		$searchid[9002273] = 'mobile_nr';
		$searchid[9002297] = 'nu';
		$searchid[9002298] = 'maps_nu';
		$searchid[9002299] = 'mobile_nu';
		$searchid[507] = 'nz';
		$searchid[6010] = 'maps_nz';
		$searchid[2022] = 'mobile_nz';
		$searchid[9002306] = 'om';
		$searchid[9002307] = 'maps_om';
		$searchid[9002308] = 'mobile_om';
		$searchid[9002318] = 'pa';
		$searchid[9002319] = 'maps_pa';
		$searchid[9002320] = 'mobile_pa';
		$searchid[330] = 'pe';
		$searchid[6025] = 'maps_pe';
		$searchid[9002326] = 'mobile_pe';
		$searchid[510] = 'ph';
		$searchid[9002328] = 'maps_ph';
		$searchid[9002329] = 'mobile_ph';
		$searchid[9002309] = 'pk';
		$searchid[9002311] = 'maps_pk';
		$searchid[9002313] = 'mobile_pk';
		$searchid[123] = 'pl';
		$searchid[6045] = 'maps_pl';
		$searchid[2036] = 'mobile_pl';
		$searchid[9002330] = 'pn';
		$searchid[9002331] = 'maps_pn';
		$searchid[9002332] = 'mobile_pn';
		$searchid[360] = 'pr';
		$searchid[6027] = 'maps_pr';
		$searchid[2029] = 'mobile_pr';
		$searchid[9002315] = 'ps';
		$searchid[9002316] = 'maps_ps';
		$searchid[9002317] = 'mobile_ps';
		$searchid[408] = 'pt';
		$searchid[6046] = 'maps_pt';
		$searchid[2012] = 'mobile_pt';
		$searchid[9002321] = 'py';
		$searchid[9002322] = 'maps_py';
		$searchid[9002323] = 'mobile_py';
		$searchid[9002333] = 'qa';
		$searchid[9002334] = 'maps_qa';
		$searchid[9002335] = 'mobile_qa';
		$searchid[511] = 'ro';
		$searchid[6047] = 'maps_ro';
		$searchid[2037] = 'mobile_ro';
		$searchid[512] = 'rs';
		$searchid[6048] = 'maps_rs';
		$searchid[2038] = 'mobile_rs';
		$searchid[25] = 'ru';
		$searchid[6049] = 'maps_ru';
		$searchid[2039] = 'mobile_ru';
		$searchid[9002342] = 'rw';
		$searchid[9002344] = 'maps_rw';
		$searchid[9002346] = 'mobile_rw';
		$searchid[9000045] = 'sa';
		$searchid[9002363] = 'maps_sa';
		$searchid[9002364] = 'mobile_sa';
		$searchid[9002387] = 'sb';
		$searchid[9002388] = 'maps_sb';
		$searchid[9002389] = 'mobile_sb';
		$searchid[9002371] = 'sc';
		$searchid[9002373] = 'maps_sc';
		$searchid[9002375] = 'mobile_sc';
		$searchid[24] = 'se';
		$searchid[6050] = 'maps_se';
		$searchid[2011] = 'mobile_se';
		$searchid[2800] = 'sg';
		$searchid[6028] = 'maps_sg';
		$searchid[9002385] = 'mobile_sg';
		$searchid[9002348] = 'sh';
		$searchid[9002349] = 'maps_sh';
		$searchid[9002350] = 'mobile_sh';
		$searchid[117] = 'si';
		$searchid[6051] = 'maps_si';
		$searchid[2040] = 'mobile_si';
		$searchid[114] = 'sk';
		$searchid[6052] = 'maps_sk';
		$searchid[2041] = 'mobile_sk';
		$searchid[9002377] = 'sl';
		$searchid[9002379] = 'maps_sl';
		$searchid[9002381] = 'mobile_sl';
		$searchid[9002357] = 'sm';
		$searchid[9002358] = 'maps_sm';
		$searchid[9002359] = 'mobile_sm';
		$searchid[9002365] = 'sn';
		$searchid[9002367] = 'maps_sn';
		$searchid[9002369] = 'mobile_sn';
		$searchid[9002390] = 'so';
		$searchid[9002392] = 'maps_so';
		$searchid[9002394] = 'mobile_so';
		$searchid[9002360] = 'st';
		$searchid[9002361] = 'maps_st';
		$searchid[9002362] = 'mobile_st';
		$searchid[9002038] = 'sv';
		$searchid[9002039] = 'maps_sv';
		$searchid[9002040] = 'mobile_sv';
		$searchid[9001981] = 'td';
		$searchid[9001983] = 'maps_td';
		$searchid[9001985] = 'mobile_td';
		$searchid[9002431] = 'tg';
		$searchid[9002433] = 'maps_tg';
		$searchid[9002435] = 'mobile_tg';
		$searchid[515] = 'th';
		$searchid[6011] = 'maps_th';
		$searchid[9002426] = 'mobile_th';
		$searchid[9002416] = 'tj';
		$searchid[9002418] = 'maps_tj';
		$searchid[9002420] = 'mobile_tj';
		$searchid[9002437] = 'tk';
		$searchid[9002438] = 'maps_tk';
		$searchid[9002439] = 'mobile_tk';
		$searchid[9002428] = 'tl';
		$searchid[9002429] = 'maps_tl';
		$searchid[9002430] = 'mobile_tl';
		$searchid[9002458] = 'tm';
		$searchid[9002460] = 'maps_tm';
		$searchid[9002462] = 'mobile_tm';
		$searchid[9002452] = 'tn';
		$searchid[9002454] = 'maps_tn';
		$searchid[9002456] = 'mobile_tn';
		$searchid[9002440] = 'to';
		$searchid[9002442] = 'maps_to';
		$searchid[9002444] = 'mobile_to';
		$searchid[517] = 'tr';
		$searchid[6029] = 'maps_tr';
		$searchid[2030] = 'mobile_tr';
		$searchid[9002446] = 'tt';
		$searchid[9002448] = 'maps_tt';
		$searchid[9002450] = 'mobile_tt';
		$searchid[514] = 'tw';
		$searchid[9002414] = 'maps_tw';
		$searchid[9002415] = 'mobile_tw';
		$searchid[9002422] = 'tz';
		$searchid[9002423] = 'maps_tz';
		$searchid[9002424] = 'mobile_tz';
		$searchid[519] = 'ua';
		$searchid[6030] = 'maps_ua';
		$searchid[9002471] = 'mobile_ua';
		$searchid[9002464] = 'ug';
		$searchid[9002466] = 'maps_ug';
		$searchid[9002468] = 'mobile_ug';
		$searchid[22] = 'uk';
		$searchid[2404] = 'maps_uk';
		$searchid[2005] = 'mobile_uk';
		$searchid[2501] = 'uy';
		$searchid[6031] = 'maps_uy';
		$searchid[2503] = 'mobile_uy';
		$searchid[9002476] = 'uz';
		$searchid[9002478] = 'maps_uz';
		$searchid[9002480] = 'mobile_uz';
		$searchid[9002351] = 'vc';
		$searchid[9002352] = 'maps_vc';
		$searchid[9002353] = 'mobile_vc';
		$searchid[340] = 've';
		$searchid[6013] = 'maps_ve';
		$searchid[2023] = 'mobile_ve';
		$searchid[9002492] = 'vg';
		$searchid[9002493] = 'maps_vg';
		$searchid[9002494] = 'mobile_vg';
		$searchid[9002495] = 'vi';
		$searchid[9002496] = 'maps_vi';
		$searchid[9002497] = 'mobile_vi';
		$searchid[2601] = 'vn';
		$searchid[9002489] = 'maps_vn';
		$searchid[2603] = 'mobile_vn';
		$searchid[9002482] = 'vu';
		$searchid[9002484] = 'maps_vu';
		$searchid[9002486] = 'mobile_vu';
		$searchid[9002354] = 'ws';
		$searchid[9002355] = 'maps_ws';
		$searchid[9002356] = 'mobile_ws';
		$searchid[411] = 'za';
		$searchid[9002397] = 'maps_za';
		$searchid[9002399] = 'mobile_za';
		$searchid[9002498] = 'zm';
		$searchid[9002500] = 'maps_zm';
		$searchid[9002502] = 'mobile_zm';
		$searchid[9002504] = 'zw';
		$searchid[9002506] = 'maps_zw';
		$searchid[9002508] = 'mobile_zw';

    if (!isset($searchid[$searchengineid])) {
      $this->logToFileException('Unknown Search Engine ID: ' . $searchengineid);
      $this->alertMail('Unknown Search Engine ID: ' .$searchengineid);
    } else {
      return $searchid[$searchengineid];
    }

  }

}

new rankingspy_keywordupdate;

?>
