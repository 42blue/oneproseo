<?php

// php cache_competition.php c 336

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk  {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->logToFile('CACHE: COMPETITION 7 DAYS START');
    $this->fetchSets();
    $this->logToFile('CACHE: COMPETITION 7 DAYS END');

    $this->db->close();

  }

  private function fetchSets ()
  {

    $sql = "SELECT id, url, country, competition FROM ruk_project_customers";

    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, url, country, competition FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    // GET ALL CUSTOMERS
    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];

      echo 'PRO: ' . $this->project_id . PHP_EOL;

      $this->country         = $row['country'];
      $this->competition_all = $row['competition'];
      $this->hostname        = $this->pureHostName($row['url']);

      // LOAD CACHE ONCE
      $this->loadCache();


			$workload = array();
      $workload['all'] = array('key', 'desktop', $this->competition_all);
      
      $sql = "SELECT id, type, rs_type, competition FROM ruk_project_keyword_sets WHERE id_customer = '$this->project_id'";
      $result2 = $this->db->query($sql);

      if ($result2->num_rows > 0) {
	      while ($row2 = $result2->fetch_assoc()) {
	        $workload[$row2['id']] = array($row2['type'], $row2['rs_type'], $row2['competition']);
	      }
      }
      
      // MAIN LOOP
      foreach ($workload as $id => $type) {

        $this->competition_set = array();

        $this->keyword_setid   = $id;
        $this->campaign_type   = $type[0];
        $this->mo_type         = $type[1];
        $this->competition_set = $type[2];

        if ($this->campaign_type !== 'url') {
        	$this->doCompetition();        	
        }

      }

    }

  }


  public function loadCache() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_x.tmp';
    $this->project_cache = $this->readFile($file);
    $file = 'metrics_rankings_data_competition_'.$this->project_id.'_x.tmp';
    $this->comp_cache = $this->readFile($file);
  }


  private function doCompetition ()
  {

    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

    $this->url_campaign = '';

    if ($this->keyword_setid == 'all') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
    }
    
    $this->getRankingData();

    $this->getRankingDataComp();

    $this->createJson();

  }


  private function createJson() {

  	$json_data = array();

  	// do all keywords, regardless of ranking
  	foreach ($this->keywords as $keyword) {
  	  $json_data[$keyword] = array('keyword' => $keyword, $this->hostname => '');
  	}


		// do all rankings from project
  	if (isset($this->rankset['_da'][$this->dateYMD()])) {

	    foreach ($this->rankset['_da'][$this->dateYMD()] as $ts => $set) {
	      $keyword = $set[0];
	      if (isset($json_data[$keyword])) {
	  	  	$json_data[$keyword] = array('keyword' => $keyword, $this->hostname => $set[1]);
	      }
			}

  	}


		// do all competition, regardless of ranking
		if (isset($this->rankset['_hn'])) {
	
	    foreach ($this->rankset['_hn'] as $competition) {
	    	foreach ($json_data as $keyword => $set) {
	    		$json_data[$keyword] = array_merge($json_data[$keyword], array($competition => ''));	
	    	}
	    }

		}

		// do all rankings from competition
		if (isset($this->rankset['_co'])) {
    	
    	foreach ($this->rankset['_co'][$this->dateYMD()] as $competitionurl => $rankings) {
				$host = $this->pureHostName($competitionurl);
				foreach ($rankings as $keyword => $rankings) {
					$rank = $rankings[0];
					if (!isset($json_data[$keyword])) {continue;}
					$json_data[$keyword][$host] = $rank;
				}
    	}

		}
     

		$json_data = array_values($json_data);

    $this->cache_filename_json = 'json/competition_'  . $this->project_id . '_' . $this->keyword_setid . '.json';

    $this->writeFileRaw($this->cache_filename_json, json_encode($json_data, JSON_PRETTY_PRINT));

  }


  private function getRankingDataComp() {

    $competition = unserialize($this->competition_all);

 		if ($competition == FALSE) {
 			return;
 		}

    $hostnames = array();
    foreach ($competition as $url) {
    	$host = $this->pureHostName($url);
      $hostnames[$host] = $url;
      $this->rankset['_hn'][$host] = $host;
    }

    $rows = $this->comp_cache;

    if (empty($rows)) {
      return;
    }

    foreach ($rows as $hostname => $host) {
      $dupl = '';
      foreach ($host as $id => $row) {

        if (!isset($hostnames[$hostname])) {
          continue;
        }

        $searchurl = $hostnames[$hostname];

        if (strripos($row['u'], $searchurl) !== false) {

          $hash = md5($row['t'] . $row['k']);

          if (!isset($dupl[$hash])) {
            $this->rankset['_co'][$row['t']][$searchurl][$row['k']][] = $row['p'];          
            $dupl[$hash] = $hash;
          }

        }

      }

    }

  }

  private function getRankingData()
  {

    $rows = $this->project_cache;

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $this->rankset = array();
    $this->dex = array();

// COMPARE OPTIONS
    $dupl = array();
    $duplga = array();
    foreach ($rows as $row) {

      if ($this->mo_type != $row['l']) {
        continue;
      }

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

        // create unique hash (ranking + day) so that only the first ranking gets recognized
        $hash = md5($row['t'] . $row['k']);

        if (!isset($dupl[$hash])) {

          $this->rankset['_da'][$row['t']][] = array($row['k'], $row['p'], $row['u']);

          $dupl[$hash] = $hash;

        }

      }

    }

  }


  private function selectKeywordSet ()
  {

    $this->keywords = array();

    $selected_keyword_set = $this->keyword_setid;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   a.name        AS setname,
                   b.keyword     AS keyword,
                   c.url         AS url,
                   c.id          AS customerid
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function selectAllKeywordSets ()
  {

    $this->keywords = array();

    $selected_project = $this->project_id;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords[] = $row['keyword'];
    }

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }


  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  public function writeFileRaw ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


}

new renderRUKOverviews($argv);

?>
