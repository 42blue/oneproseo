<?php

// php cache_json_wl_weekly.php c 220

require_once dirname(__FILE__) . '/ruk.class.php';

class render_json_winner_looser extends ruk {

  public $logdir        = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir     = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $csv_dir       = '/var/www/enterprise.oneproseo.com/temp/export/';
  public $ops_version   = 'enterprise';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->fetchSets();

    $this->db->close();

  }


  private function fetchSets ()
  {

    $sql = "SELECT id, url, name, country FROM ruk_project_customers";

    // BASH
    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, url, name, country FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    $sets = array();

    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];
      $this->country    = $row['country'];

      echo 'PRO: ' . $this->project_id . PHP_EOL;

      // LOAD CACHE ONCE
      $this->loadCacheDaily();
      $this->loadCacheWeekly();
      $this->loadCacheBest();      
      $this->loadCacheMonthly();

      $this->reconMySql();

      $sql = "SELECT id, type, rs_type, competition FROM ruk_project_keyword_sets WHERE id_customer = '$this->project_id'";
      $result2 = $this->db->query($sql);

      if ($result2->num_rows == 0) {
        continue;
      }

      $workload = array();
      $workload['all'] = array('key', 'desktop');
      while ($row2 = $result2->fetch_assoc()) {
        $workload[$row2['id']] = array($row2['type'], $row2['rs_type'], $row2['competition']);
      }

      // MAIN LOOP
      foreach ($workload as $id => $type) {

        $this->keyword_setid   = $id;
        $this->campaign_type   = $type[0];
        $this->mo_type         = $type[1];

        $this->url_campaign = '';

        // KEYWORD CAMPAIGN OR URL CAMPAIGN
        if ($this->campaign_type == 'url') {
          $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
        }

        if ($this->keyword_setid == 'all' || $this->keyword_setid == 'all_monthly' || $this->campaign_type == 'url') {
          $this->selectAllKeywordSets();
        } else {
          $this->selectKeywordSet();
        }

        $adwordsloc = $this->getAdwordsLoc($this->country);
        $this->adwordsData = $this->getAdwordsDataI18N($this->keywords, $adwordsloc[0], $adwordsloc[1]);

        $daily   = $this->getRankingData($this->project_cache_daily);
        $weekly  = $this->getRankingData($this->project_cache_weekly);
        $monthly = $this->getRankingData($this->project_cache_monthly);

        $new_up = array();
        $new_down = array();
        $new = array();
        
        foreach ($this->keywords as $keyword) {

          $yesterday = $this->dateYesterday();
          $lastweek  = $this->dateLastMonday();
          $lastmonth = $this->dateLastMonth();
          $lastyear  = $this->dateLastYear();          

          if (!isset($daily[$keyword][$yesterday])) {
            $ranking_today[0][0] = 101;
            $ranking_today[0][1] = '-';
          } else{
            $ranking_today = $daily[$keyword][$yesterday];
          }

          if (!isset($weekly[$keyword][$lastweek])) {
            $ranking_week[0][0] = 101;
            $ranking_week[0][1] = '-';
          } else{
            $ranking_week  = $weekly[$keyword][$lastweek];                   
          }

          if (!isset($weekly[$keyword][$lastmonth])) {
            $ranking_month[0][0] = 101;
            $ranking_month[0][1] = '-';
          } else{
            $ranking_month = $weekly[$keyword][$lastmonth];          
          }

          if (!isset($monthly[$keyword][$lastyear])) {
            $ranking_year[0][0] = 101;
            $ranking_year[0][1] = '-';
          } else{
            $ranking_year  = $monthly[$keyword][$lastyear];
          }

          if (!isset($this->project_cache_best[$keyword])) {
            $this->project_cache_best[$keyword] = array('', '');
          }

          if (!isset($this->adwordsData[$keyword])) {
            $opi = 0;
            $this->adwordsData[$keyword][0] = '-';
          } else {
            $opi = $this->adwordsData[$keyword][3];
          }

          $ranking_best  = $this->project_cache_best[$keyword][0];
          $onedex        = $this->calcOneDex($ranking_today[0][0], $opi);

          if (!isset($ranking_today[0][0])) {
            $ranking_today[0][0] = 101;
          }

          $change = $ranking_week[0][0] - $ranking_today[0][0];

          if ($change > 0) {
             $change = '+' . $change;

            $new_up[] = array(
              'Keyword'           => $keyword,
              'Rank'              => $ranking_today[0][0],
              'URL'               => $ranking_today[0][1],
              'Change'            => $change,
              'Best'              => $ranking_best,
              'OneDex'            => $onedex,
              'SV'                => $this->adwordsData[$keyword][0],
              'RankPreviousWeek'  => $ranking_week[0][0],
              'RankPreviousMonth' => $ranking_month[0][0],
              'RankPreviousYear'  => $ranking_year[0][0],            
            );


          } else if ($change < 0) {

            $new_down[] = array(
              'Keyword'           => $keyword,
              'Rank'              => $ranking_today[0][0],
              'URL'               => $ranking_today[0][1],
              'Change'            => $change,
              'Best'              => $ranking_best,
              'OneDex'            => $onedex,
              'SV'                => $this->adwordsData[$keyword][0],
              'RankPreviousWeek'  => $ranking_week[0][0],
              'RankPreviousMonth' => $ranking_month[0][0],
              'RankPreviousYear'  => $ranking_year[0][0],            
            );


          } else {

            $new[] = array(
              'Keyword'           => $keyword,
              'Rank'              => $ranking_today[0][0],
              'URL'               => $ranking_today[0][1],
              'Change'            => $change,
              'Best'              => $ranking_best,
              'OneDex'            => $onedex,
              'SV'                => $this->adwordsData[$keyword][0],
              'RankPreviousWeek'  => $ranking_week[0][0],
              'RankPreviousMonth' => $ranking_month[0][0],
              'RankPreviousYear'  => $ranking_year[0][0],            
            );

          }
        
        }

        $filename_json_w = 'json/wl_winner_' . $this->project_id .'_'.$this->keyword_setid.'.json';
        $filename_json_s = 'json/wl_same_' . $this->project_id .'_'.$this->keyword_setid.'.json';
        $filename_json_l = 'json/wl_loser_' . $this->project_id .'_'.$this->keyword_setid.'.json';

        $this->writeFileRaw($filename_json_w, json_encode($new_up, JSON_PRETTY_PRINT));
        $this->writeFileRaw($filename_json_s, json_encode($new, JSON_PRETTY_PRINT));
        $this->writeFileRaw($filename_json_l, json_encode($new_down, JSON_PRETTY_PRINT));      

      }

    }

  }


  public function prepareJson($arr) {

    $new = array();
    
    $x = 0;

    foreach ($arr as $key => $value) {

      if ($x > 249) {continue;}

    
      $x++;

    }

    return $new;

  }



  private function getRankingData($rows)
  {

    if (!is_array($rows)) {return;}

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
          unset($this->url_campaign[$key]);
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $rankset = array();

    foreach ($rows as $row) {

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {
        $rankset[$row['k']][$row['t']][] = array($row['p'], $row['u']);
      }

    }

    return $rankset;

  }


  public function loadCacheDaily() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_x.tmp';
    $this->project_cache_daily = $this->readFile($file);
  }

  public function loadCacheWeekly() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_weekly.tmp';
    $this->project_cache_weekly = $this->readFile($file);
  }


  public function loadCacheMonthly() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_monthly.tmp';
    $this->project_cache_monthly = $this->readFile($file);
  }

  public function loadCacheBest() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_best.tmp';
    $this->project_cache_best = $this->readFile($file);
  }


  private function selectKeywordSet () {

    $this->keywords = array();

    $selected_keyword_set = $this->keyword_setid;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   a.name        AS setname,
                   b.keyword     AS keyword,
                   c.url         AS url,
                   c.id          AS customerid
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function selectAllKeywordSets () {

    $this->keywords = array();

    $selected_project = $this->project_id;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords[] = $row['keyword'];
    }

  }


// FETCH URLS in KEYWORD CAMPAIGN
  public function fetchURLSet($kw_set_id)
  {

    $this->reconMySql();

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  // DEX
  public function calcOneDex ($rank, $opi) {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return number_format($dex, 2, ',', '.');

  }


  public function writeFileRaw ($fileName, $variable) {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function readFile ($fileName) {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }

  public function dateYesterday ()
  {
    $d = strtotime('yesterday');
    return date('Y-m-d', $d);
  }

  public function dateLastMonday ()
  {
    $d = strtotime('Monday this week');
    return date('Y-m-d', strtotime('-1 week', $d));
  }

  public function dateLastMonth ()
  {
    $d = strtotime('Monday this week');
    return date('Y-m-d', strtotime('-4 week', $d));
  }

  public function dateLastYear ()
  {
    $d = strtotime('Monday this week');
    return date('Y-m-01', strtotime('-10 month', $d));
  }

}

new render_json_winner_looser ($argv);

?>
