<?php

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk  {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->logToFile('CACHE: INDEXWATCH START');
    //$this->fetchIndexwatchTravel();
    //$this->fetchIndexwatchSeoCompany(233, 'ibusiness');
    $this->fetchIndexwatchSeoCompany(1097, 'seocompany');
    $this->getRankingDataIndexwatchLocal();

    $this->logToFile('CACHE: INDEXWATCH END');

    $this->db->close();

  }


  private function fetchIndexwatchTravel ()
  {

    echo 'FETCH INDEXWATCH Travel' . PHP_EOL;

    $project = 40;

    $rows = $this->selectAllKeywordSets($project);

    echo 'DOING Indexwatch Travel 1 Day: ' . $project . ' - count:' . count($rows) . PHP_EOL;
    $this->getRankingDataIndexwatch($rows, 1, 'travel');
    echo 'DOING Indexwatch Travel 6 Days: ' . $project . ' - count:' . count($rows) . PHP_EOL;
    $this->getRankingDataIndexwatch($rows, 6, 'travel');

  }


  private function fetchIndexwatchSeoCompany ($setid, $name)
  {

    echo 'FETCH INDEXWATCH ' . $name . PHP_EOL;

    $rows = $this->selectKeywordSet($setid);

    echo 'DOING Indexwatch SEO 1 Day: ' . $setid . ' - count:' . count($rows) . PHP_EOL;
    $this->getRankingDataIndexwatch($rows, 1, $name);
    echo 'DOING Indexwatch SEO 6 Days: ' . $setid . ' - count:' . count($rows) . PHP_EOL;
    $this->getRankingDataIndexwatch($rows, 6, $name);

  }


  private function getRankingDataIndexwatch($rows, $days, $type)
  {

    $comma_separated_kws = implode('", "', $rows);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $rank_add            = 0;

    $sql = "SELECT
              DISTINCT keyword,
              timestamp,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              language = 'de'
            AND
              DATE(timestamp) > CURDATE() - INTERVAL '$days' DAY
            AND
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    $rows_ra = array();

    while ($row = $result2->fetch_assoc()) {
      $rows_ra[] = array('keyword' => $rows_kw[$row['id_kw']]['keyword'], 'timestamp' => $rows_kw[$row['id_kw']]['timestamp'], 'position' => $row['position'], 'url' => $row['url']);
    }

    $tmpfilename = 'indexwatch_'.$type.'_'.$days.'_x.tmp';
    $this->writeFile($tmpfilename, $rows_ra);

  }

  private function getRankingDataIndexwatchLocal ()
  {

    $sql = "SELECT
              id,
              keyword,
              location,
              rs_type,
              timestamp
            FROM
              ruk_scrape_keywords_local
            WHERE
              keyword = 'seo agentur'
            AND
              language = 'de'
            AND
              DATE(timestamp) = CURDATE()";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      $rows_kw[$row['id']] = $row;
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings_local
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    $rows_ra  = array();

    while ($row = $result2->fetch_assoc()) {

      $row['url'] = strtolower($row['url']);

      if ( isset ($rows_ra[$rows_kw[$row['id_kw']]['location']]) ) {
        array_push($rows_ra[$rows_kw[$row['id_kw']]['location']], array('p' => $row['position'], 'u' => $row['url'], 'rs' => $rows_kw[$row['id_kw']]['rs_type']));
      } else {
        $rows_ra[$rows_kw[$row['id_kw']]['location']][] = array('p' => $row['position'], 'u' => $row['url'], 'rs' => $rows_kw[$row['id_kw']]['rs_type']);
      }

    }

    $tmpfilename = 'indexwatch_seocompany_local_1_x.tmp';
    $this->writeFile($tmpfilename, $rows_ra);

  }

  private function selectAllKeywordSets ($selected_project) {

    $sql = "SELECT
              b.keyword AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = $selected_project
            AND a.type = 'key'";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }


  private function selectKeywordSet ($set_id) {

    $sql = "SELECT
              keyword
            FROM ruk_project_keywords
            WHERE id_kw_set = $set_id";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }


  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 2, 0);

  }


  public function readFile ($fileName)
  {
    $fileName = $this->cache_dir . $fileName;
    if (file_exists($fileName)) {
      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);
      return unserialize($variable);
    } else {
      return null;
    }
  }

  public function writeFile ($fileName, $variable)
  {
    $fileName = $this->cache_dir . $fileName;
    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);
  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }

  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }

}

new renderRUKOverviews;

?>
