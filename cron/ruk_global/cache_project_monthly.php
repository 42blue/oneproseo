<?php

// php cache_project_weekly.php 0 c 420

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk {

  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';

  private $cache_type = 'MONTHLY';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->logToFile('CACHE: ' . $this->cache_type . ' DAYS START');
    $this->fetchSets();
    $this->logToFile('CACHE: ' . $this->cache_type . ' DAYS END');

    $this->db->close();

  }

  private function fetchSets ()
  {

    $sql    = "SELECT id, ga_account, gwt_account, country, url, ignore_subdomain, competition FROM ruk_project_customers";

    if (isset($this->argv[2]) && isset($this->argv[3]) && $this->argv[2] == 'c') {
      $cid = $this->argv[3];
      $sql = "SELECT id, ga_account, gwt_account, country, url, ignore_subdomain, competition FROM ruk_project_customers WHERE id = '$cid'";
    }

    $result = $this->db->query($sql);
    $sets   = array();

    $i = 0;
    while ($row = $result->fetch_assoc()) {

      $sets[$i] = array(
        'id'               => $row['id'],
        'gwt_account'      => $row['gwt_account'],
        'ga_account'       => $row['ga_account'],
        'url'              => $row['url'],
        'country'          => $row['country'],
        'ignore_subdomain' => $row['ignore_subdomain'],
        'competition'      => array($row['competition'])
      );

      // COMPETITION FROM SETS
      $cid    = $row['id'];
      $sql    = "SELECT competition FROM ruk_project_keyword_sets WHERE id_customer = $cid";
      $res2   = $this->db->query($sql);
      while ($row = $res2->fetch_assoc()) {
        $sets[$i]['competition'][] = $row['competition'];
      }

      $i++;

    }

    $pi = 0;
    foreach ($sets as $key => $row) {

      $this->customer_id = $row['id'];

      $rows = $this->selectAllKeywordSets($row['id']);

      echo $pi . ' - DOING Project ' . $this->cache_type . ' : ' . $row['id'] . ' - count:' . count($rows) . PHP_EOL;
      if (count($rows) < 1) {
        continue;
      }

      $this->gwt_account = $row['gwt_account'];
      $this->ga_account  = $row['ga_account'];
      $this->url         = $row['url'];
      $this->country     = $row['country'];
      $this->hostname_id = $row['id'];
      $this->ignore_subd = $row['ignore_subdomain'];
      $this->hostname    = $this->pureHostName($row['url']);
      $this->competition = false;

      // competition
      if (!empty ($row['competition'])) {
        $data = '';
        foreach ($row['competition'] as $serialized_array) {
          if (empty($serialized_array)) {continue;}
          $this->competition = unserialize($serialized_array);
          foreach ($this->competition as $url) {
            $data[$this->pureHostName($url)] = $this->pureHostName($url);
          }
          $this->competition = $data;
        }
      }

      $this->getRankingData($rows);
      $pi++;

    }

  }


  private function selectAllKeywordSets ($selected_project) {

    $this->reconMySql();

    $sql = "SELECT
              b.keyword AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = $selected_project
            AND a.type = 'key'";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }

  private function getRankingData($rows)
  {

    $rows_ra   = array();
    $rows_comp = array();

    $d = strtotime('Monday this week');
    $w0  = date('Y-m-d', $d);
    $w1  = date('Y-m-d', strtotime('-4 week', $d));
    $w2  = date('Y-m-d', strtotime('-8 week', $d));
    $w3  = date('Y-m-d', strtotime('-12 week', $d));
    $w4  = date('Y-m-d', strtotime('-16 week', $d));
    $w5  = date('Y-m-d', strtotime('-20 week', $d));
    $w6  = date('Y-m-d', strtotime('-24 week', $d));
    $w7  = date('Y-m-d', strtotime('-28 week', $d));
    $w8  = date('Y-m-d', strtotime('-32 week', $d));
    $w9  = date('Y-m-d', strtotime('-36 week', $d));
    $w10 = date('Y-m-d', strtotime('-40 week', $d));
    $w11 = date('Y-m-d', strtotime('-44 week', $d));
    $w12 = date('Y-m-d', strtotime('-48 week', $d));

    $comma_separated_kws = implode('", "', $rows);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $rank_add            = 0;

    $sql = "SELECT
              keyword,
              timestamp,
              language,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND (timestamp = '$w0' OR timestamp = '$w1' OR timestamp = '$w2' OR timestamp = '$w3' OR timestamp = '$w4' OR timestamp = '$w5' OR timestamp = '$w6' OR timestamp = '$w7' OR timestamp = '$w8' OR timestamp = '$w9' OR timestamp = '$w10' OR timestamp = '$w11' OR timestamp = '$w12')
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      // MOBILE // MAPS // COUNTRY
      if (stripos($row['language'], '_' . $this->country) !== FALSE || $row['language'] == $this->country) {
        $rows_kw[$row['id']] = $row;
      }
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    if (empty($rows_kw)) {
      return;      
    }

    $this->reconMySql();

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    while ($row = $result2->fetch_assoc()) {

      if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
        $row['url'] = strtolower($row['url']);
        $rows_ra[] = array(
          'k' => $rows_kw[$row['id_kw']]['keyword'],
          't' => $rows_kw[$row['id_kw']]['timestamp'],
          'l' => $rows_kw[$row['id_kw']]['language'],
          'p' => $row['position'],
          'u' => $row['url']
        );
      }

      if (isset($this->competition[$row['hostname']])) {
        $rows_comp[$row['hostname']][] = array(
          'k' => $rows_kw[$row['id_kw']]['keyword'],
          't' => $rows_kw[$row['id_kw']]['timestamp'],
          'l' => $rows_kw[$row['id_kw']]['language'],
          'p' => $row['position'],
          'u' => $row['url']
        );
      }

    }

// ANALYTICS DATA
    if (!empty($this->ga_account)) {

      $this->reconMySql();

      foreach ($rows_ra as $key => $rankset) {

        $timestamp = $rankset['t'];
        $url       = preg_replace('(^https?://)', '', $rankset['u']);
        $tr        = 0;
        $pv        = 0;
        $en        = 0;

        $sql = "SELECT
                  a.id,
                  a.url,
                  b.ga_transactionRevenue AS ga_transactionRevenue,
                  b.ga_transactions       AS ga_transactions,                  
                  b.ga_pageviews          AS ga_pageviews,
                  b.ga_entrances          AS ga_entrances,
                  b.timestamp             AS timestamp
                FROM
                  analytics_urls a
                LEFT JOIN analytics_data_seofilter b
                  ON b.id_url = a.id
                WHERE
                  a.url = '$url'
            AND (timestamp = '$w0' OR timestamp = '$w1' OR timestamp = '$w2' OR timestamp = '$w3' OR timestamp = '$w4' OR timestamp = '$w5' OR timestamp = '$w6' OR timestamp = '$w7' OR timestamp = '$w8' OR timestamp = '$w9' OR timestamp = '$w10' OR timestamp = '$w11' OR timestamp = '$w12')";

        $result_ga = $this->db->query($sql);

        if ($result_ga->num_rows > 0) {
          while ($row_ga = $result_ga->fetch_assoc()) {

            if (!empty($row_ga['ga_transactionRevenue'])) {
              $tr = $row_ga['ga_transactionRevenue'];
            }
            if (!empty($row_ga['ga_pageviews'])) {
              $pv = $row_ga['ga_pageviews'];
            }
            if (!empty($row_ga['ga_entrances'])) {
              $en = $row_ga['ga_entrances'];
            }
            if (!empty($row_ga['ga_transactions'])) {
              $tx = $row_ga['ga_transactions'];
            }

          }
        }

        $rows_ra[$key] = array(
          'k' => $rankset['k'],
          't' => $rankset['t'],
          'l' => $rankset['l'],
          'p' => $rankset['p'],
          'u' => $rankset['u'],
          'tr' => $tr,
          'pv' => $pv,
          'en' => $en,
          'tx' => $tx          
        );

      }

    }
// ANALYTICS DATA

// GWT DATA
    if ($this->gwt_account == '1') {

      $this->reconMySql();

      $sql = "SELECT
                id
               FROM
                gwt_hostnames
               WHERE
                hostname = '$this->hostname'";

      $result_gwh = $this->db->query($sql);

      while ($row_gwh = $result_gwh->fetch_assoc()) {
        $hostname_id = $row_gwh['id'];
      }

      if (!empty($hostname_id)) {

        $sql = "SELECT
                  clicks,
                  query,
                  timestamp
                FROM
                  gwt_data
                WHERE
                  hostname_id = $hostname_id
            AND (timestamp = '$w0' OR timestamp = '$w1' OR timestamp = '$w2' OR timestamp = '$w3' OR timestamp = '$w4' OR timestamp = '$w5' OR timestamp = '$w6' OR timestamp = '$w7' OR timestamp = '$w8' OR timestamp = '$w9' OR timestamp = '$w10' OR timestamp = '$w11' OR timestamp = '$w12')";

        $result_gw = $this->db->query($sql);

        $gwt_rows = array();

        while ($row_gw = $result_gw->fetch_assoc()) {

          if (!empty($row_gw['clicks'])) {
            $hash = crc32 ($row_gw['query'] . $row_gw['timestamp']);
            $gwt_rows[$hash] = $row_gw['clicks'];
          }

        }


        foreach ($rows_ra as $key => $rankset) {

          $timestamp = $rankset['t'];
          $keyword   = $rankset['k'];
          $cl        = 0;

          $hash2 = crc32 ($keyword . $timestamp);

          if (isset($gwt_rows[$hash2])) {

            if (!isset($rankset['tr'])) {
              $rankset['tr'] = 0;
              $rankset['pv'] = 0;
              $rankset['en'] = 0;
            }

            $rows_ra[$key] = array(
               'k'  => $rankset['k'],
               't'  => $rankset['t'],
               'l'  => $rankset['l'],
               'p'  => $rankset['p'],
               'u'  => $rankset['u'],
               'tr' => $rankset['tr'],
               'pv' => $rankset['pv'],
               'en' => $rankset['en'],
               'tx' => $rankset['tx'],               
               'cl' => $gwt_rows[$hash2]
              );

          }

        }

      }

    }
// GWT DATA

    $tmpfilename = 'metrics_rankings_data_'.$this->customer_id.'_monthly.tmp';
    $this->writeFile($tmpfilename, $rows_ra);

    $tmpfilename = 'metrics_rankings_data_competition_'.$this->customer_id.'_monthly.tmp';
    $this->writeFile($tmpfilename, $rows_comp);

  }

  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  public function pureHostNameNoSubdomain ($host)
  {

    $domain = $host;

    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) && $this->ignore_subd == 1) {
      return $regs['domain'];
    } else {
      return $host;
    }

  }

}

new renderRUKOverviews ($argv);

?>
