<?php

require_once dirname(__FILE__) . '/ruk.class.php';

class render_best_rankings extends ruk {

  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $csv_dir   = '/var/www/enterprise.oneproseo.com/temp/export/';

  public function __construct () {

    $this->mySqlConnect();
    $this->logToFile('CACHE: BEST RANKINGS START');
    $this->fetchSets();
    $this->logToFile('CACHE: BEST RANKINGS END');
    $this->db->close();

  }


  private function fetchSets ()
  {

    $sql      = "SELECT id, country, url, name FROM ruk_project_customers";
    $result   = $this->db->query($sql);
    $projects = array();

    // GET PROJECTS
    while ($row = $result->fetch_assoc()) {
      $projects[$row['id']] = array($this->pureHostName($row['url']), $row['country'], $row['name']);
    }

    $customers = [];

    foreach ($projects as $id => $arr) {

      $hostname    = $arr[0];
      $language    = $arr[1];
      $name        = $arr[2];

      $keywords    = $this->selectAllKeywordSets($id);
    	$adwordsloc  = $this->getAdwordsLoc($language);
    	$adwordsData = $this->getAdwordsDataI18N($keywords, $adwordsloc[0], $adwordsloc[1]);

    	$keywords_relevant = [];
    	foreach ($adwordsData as $keyword => $data) {
    		if ($data[0] > 1000) {
    			$ranks = $this->getBestRanking($keyword, $hostname);
    			$ranks_old = $this->getBestRankingCompare($keyword, $hostname);
    			if (!empty($ranks)) {
    				$keywords_relevant[] = array_merge(array($keyword, $data[0], $data[3]), $ranks, $ranks_old);    				
    			}
    		}
    	}

    	echo $name;
    	echo PHP_EOL;

    	$customers[$id] = array($hostname, $name, $language, $keywords_relevant);

		}

    $this->writeFile('metrics_rankings_data_best_overall.tmp' , $customers);

  }


  public function getBestRanking($keyword, $hostname) {

    $ranks = array();

    $sql = "SELECT
              a.timestamp AS ts,
              b.position  AS pos,
              b.url       AS url
            FROM
              ruk_scrape_keywords a
              LEFT JOIN ruk_scrape_rankings b
                ON b.id_kw = a.id
            WHERE
              a.keyword = '$keyword'
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 7 DAY
            AND
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            AND 
            	b.position <= 10  
            AND
              b.hostname = '$hostname'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $ranks[$row['pos']] = array($row ['url'], $row['ts']);
    }

    ksort($ranks);

    $bestrankings = [];

    if (!empty ($ranks)) {
      $bestrankings = array (key($ranks), $ranks[key($ranks)][0], $ranks[key($ranks)][1],);
    }

    return $bestrankings;

  }



  public function getBestRankingCompare($keyword, $hostname) {

    $ranks = array();

    $sql = "SELECT
              a.timestamp AS ts,
              b.position  AS pos,
              b.url       AS url
            FROM
              ruk_scrape_keywords a
              LEFT JOIN ruk_scrape_rankings b
                ON b.id_kw = a.id
            WHERE
              a.keyword = '$keyword'
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 180 DAY
            AND
              DATE(timestamp) < CURDATE() - INTERVAL 173 DAY
            AND
              b.hostname = '$hostname'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $ranks[$row['pos']] = array($row ['url'], $row['ts']);
    }

    ksort($ranks);

    $bestrankings = [];

    if (!empty ($ranks)) {
      $bestrankings = array (key($ranks), $ranks[key($ranks)][0], $ranks[key($ranks)][1],);
    }

    return $bestrankings;

  }




  public function writeFile ($fileName, $variable)
  {
    $fileName = $this->cache_dir . $fileName;
    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);
  }

  private function selectAllKeywordSets ($selected_project) {

    $this->reconMySql();

    $sql = "SELECT
              b.keyword AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = $selected_project
            AND a.type = 'key'";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

}

new render_best_rankings;

?>
