<?php

// php cache_json_url_rankings.php c 336

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk  {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->fetchSets();

    $this->db->close();

  }

  private function fetchSets ()
  {

    $sql = "SELECT id, url, country, ga_account, gwt_account, competition FROM ruk_project_customers";

    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, url, country, ga_account, gwt_account, competition FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    // GET ALL CUSTOMERS
    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];
      echo 'PRO: ' . $this->project_id . PHP_EOL;

      $this->ga_account      = $row['ga_account'];
      $this->gwt_account     = $row['gwt_account'];
      $this->country         = $row['country'];
      $this->competition_all = $row['competition'];
      $this->hostname        = $this->pureHostName($row['url']);

      // LOAD CACHE ONCE
      $this->loadCache();
      $this->loadCacheWeekly();

      $sql = "SELECT id, type, rs_type, competition, measuring FROM ruk_project_keyword_sets WHERE id_customer = '$this->project_id'";

      $result2 = $this->db->query($sql);

      if ($result2->num_rows == 0) {
        continue;
      }

      $workload = array();
      $workload['all'] = array('key', 'desktop', '', 'daily');
      while ($row2 = $result2->fetch_assoc()) {
        $workload[$row2['id']] = array($row2['type'], $row2['rs_type'], $row2['competition'], $row2['measuring']);
      }


      // MAIN LOOP
      foreach ($workload as $id => $type) {
        $this->keyword_setid = $id;
        $this->mo_type       = $type[1];
        $this->campaign_type = $type[0];
        $this->measuring     = $type[3];
        $this->doReport();
      }

    }

  }

  public function loadCache() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_x.tmp';
    $this->project_cache = $this->readFile($file);
  }

  public function loadCacheWeekly() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_weekly.tmp';
    $this->project_cache_weekly = $this->readFile($file);
  }

  private function doReport ()
  {

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

    $this->url_campaign = '';

    // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }

    if ($this->keyword_setid == 'all' || $this->keyword_setid == 'all_monthly' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
    }

    $adwordsloc = $this->getAdwordsLoc($this->country);
    $this->googleadwords = $this->getAdwordsDataI18N($this->keywords, $adwordsloc[0], $adwordsloc[1]);
    
    $this->getRankingData();

    $this->createJson();

  }


  private function createJson() {

  	$kw_with_rankings = array();

    if (isset($this->rankset)) {

      $last_ts = end($this->rankset['_ts']);

      $new = array();

      foreach ($this->rankset['_da'][$last_ts] as $data) {

        $kw   = $data[0];
        $rank = $data[1];
        $url  = $data[2];

				$kw_with_rankings[$kw] = $kw;

        if (!isset($this->googleadwords[$kw][0])) {
          $this->googleadwords[$kw][0] = '-';
        }

        if (!isset($new[$url]) OR $rank < $new[$url]['Rank']) {
          $new[] = array('URL' => $url, 'Keyword' => $kw, 'Rank' => $rank, 'SV' => $this->googleadwords[$kw][0]);
        }

      }
      
    }
    
    $new = array_values($new);        


		// add no rankings 
		$dataadd = array();
		$allKeywords = array_flip($this->keywords);

		foreach ($allKeywords as $keyword => $x) {

			if (!isset($kw_with_rankings[$keyword])) {
// RALF SCHÖNBACH EMAIL VOM 14.05.
      	//$dataadd[] = array('URL' => '-', 'Keyword' => $keyword, 'Rank' => '-', 'SV' => $this->googleadwords[$keyword][0]);

			}

		}

    $data = array_merge($new, $dataadd);

    $this->cache_filename_json = 'json/url_rankings_'  . $this->project_id . '_' . $this->keyword_setid . '.json';

    $this->writeFileRaw($this->cache_filename_json, json_encode($data, JSON_PRETTY_PRINT));

  }


  

  private function getRankingData()
  {

    if ($this->measuring == 'weekly') {
      $rows = $this->project_cache_weekly;
    } else {
      $rows = $this->project_cache;      
    }

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $this->rankset = array();
    $this->dex = array();

// COMPARE OPTIONS
    $dupl = array();
    $duplga = array();
    foreach ($rows as $row) {

      if ($this->mo_type != $row['l']) {
        continue;
      }

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

        // create unique hash (ranking + day) so that only the first ranking gets recognized
        $hash = md5($row['t'] . $row['k']);

        if (!isset($dupl[$hash])) {

          $this->rankset['_da'][$row['t']][] = array($row['k'], $row['p'], $row['u']);

          $dupl[$hash] = $hash;

        }

      }

    }

  }


  private function selectKeywordSet ()
  {

    $this->keywords = array();

    $selected_keyword_set = $this->keyword_setid;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   a.name        AS setname,
                   b.keyword     AS keyword,
                   c.url         AS url,
                   c.id          AS customerid
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function selectAllKeywordSets ()
  {

    $this->keywords = array();

    $selected_project = $this->project_id;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords[] = $row['keyword'];
    }

  }



  // FETCH URLS in KEYWORD CAMPAIGN
  public function fetchURLSet($kw_set_id)
  {

    $this->reconMySql();

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }


  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  public function writeFileRaw ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


}

new renderRUKOverviews($argv);

?>
