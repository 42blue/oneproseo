<?php

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk {

  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';

  public function __construct () {

    $this->mySqlConnect();

    $this->logToFile('CACHE: 7 DAYS START');
    $this->fetchMetrics();
    $this->fetchSets();
    $this->fetchOneDex();
    $this->sendAlertMailHTML();
    $this->logToFile('CACHE: 7 DAYS END');

    $this->db->close();

  }

  private function fetchMetrics()
  {

    $data = array();

    // ANZAHL KEYWORDS RUK
    $sql = "SELECT keyword FROM ruk_project_keywords";
    $res = $this->db->query($sql);
    $keywords1 = array();
    $keywords2 = array();
    while ($row = $res->fetch_assoc()) {
      $kw = nl2br(stripslashes($row['keyword']));
      $keywords1[]    = $kw;
      $keywords2[$kw] = $kw;
    }
    $data[2] = count($keywords1);
    $data[0] = count($keywords2);

    // ANZAHL URLS IN DER SCRAPE DB
    $sql = "SHOW TABLE STATUS FROM oneproseo_live;";
    $res = $this->db->query($sql);
    $amount_urls = 0;
    while ($row = $res->fetch_assoc()) {
      if ($row['Name'] == 'ruk_scrape_rankings') {
        $amount_urls = $row['Rows'];
      }
    }
    $data[1] = $amount_urls;

    // ANZAHL ALLER KEYWORDS ADWORDS DB
    $sql = "SELECT id FROM gen_keywords";
    $res = $this->db->query($sql);
    $data[3] = $res->num_rows;

    // ANZAHL KUNDEN
    $sql = "SELECT id FROM ruk_project_customers";
    $res = $this->db->query($sql);
    $data[4] = $res->num_rows;

    // ANZAHL KEYWORD SETS
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE type = 'key'";
    $res = $this->db->query($sql);
    $data[5] = $res->num_rows;

    // ANZAHL URL SETS
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE type = 'url'";
    $res = $this->db->query($sql);
    $data[6] = $res->num_rows;

    $tmpfilename = 'metrics_rankings.tmp';
    $this->writeFile($tmpfilename, $data);

  }


  private function fetchSets ()
  {

    $sql = "SELECT id, ga_account, gwt_account, country, url, ignore_subdomain FROM ruk_project_customers";

    $result   = $this->db->query($sql);
    $projects = array();

    $pi = 1;

    while ($row = $result->fetch_assoc()) {

      $this->customer_id = $row['id'];

      $rows = $this->selectAllKeywordSets($row['id']);

      echo $pi . ' - DOING Project: ' . $row['id'] . ' - count:' . count($rows) . PHP_EOL;
      if (count($rows) < 1) {
        continue;
      }

      $this->gwt_account = $row['gwt_account'];
      $this->ga_account  = $row['ga_account'];
      $this->url         = $row['url'];
      $this->country     = $row['country'];
      $this->hostname_id = $row['id'];
      $this->ignore_subd = $row['ignore_subdomain'];
      $this->hostname    = $this->pureHostName($row['url']);

      $this->getRankingData($rows);
      $pi++;

    }

  }

  private function selectAllKeywordSets ($selected_project) {

    $sql = "SELECT
              b.keyword AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
            WHERE a.id_customer = $selected_project
            AND a.type = 'key'
            AND a.measuring = 'daily'";

    $result = $this->db->query($sql);

    $rows = array();

    while ($row = $result->fetch_assoc()) {
      $rows[$row['keyword']] = $row['keyword'];
    }

    return $rows;

  }


  private function getRankingData($rows)
  {

    $comma_separated_kws = implode('", "', $rows);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';
    $rank_add            = 0;

    $sql = "SELECT
              keyword,
              timestamp,
              language,
              id
            FROM
              ruk_scrape_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            AND
              DATE(timestamp) > CURDATE() - INTERVAL 7 DAY
            AND
              DATE(timestamp) < CURDATE() + INTERVAL 1 DAY
            ORDER BY timestamp DESC";

    $result = $this->db->query($sql);

    $rows_kw = array();

    while ($row = $result->fetch_assoc()) {
      // MOBILE // MAPS // COUNTRY
      if (stripos($row['language'], '_' . $this->country) !== FALSE || $row['language'] == $this->country) {
        $rows_kw[$row['id']] = $row;
      }
    }

    $rows_kw_keys = implode(',', array_keys($rows_kw));

    $sql = "SELECT
              id,
              position,
              url,
              id_kw,
              hostname
            FROM
              ruk_scrape_rankings
            WHERE
              id_kw IN ($rows_kw_keys)";

    $result2 = $this->db->query($sql);

    $rows_ra  = array();
    $rows_url = array();

    while ($row = $result2->fetch_assoc()) {
      if ($this->pureHostNameNoSubdomain($row['hostname']) == $this->hostname) {
        $row['url'] = strtolower($row['url']);
        $rows_ra[] = array(
          'k' => $rows_kw[$row['id_kw']]['keyword'],
          't' => $rows_kw[$row['id_kw']]['timestamp'],
          'l' => $rows_kw[$row['id_kw']]['language'],
          'p' => $row['position'],
          'u' => $row['url']
        );
      }
    }

// ADWORDS DATA
    if (!empty($this->ga_account)) {

      foreach ($rows_ra as $key => $rankset) {

        $timestamp = $rankset['t'];
        $url       = preg_replace('(^https?://)', '', $rankset['u']);
        $tr        = 0;
        $pv        = 0;
        $en        = 0;
        $tx        = 0;

        $sql = "SELECT
                  a.id,
                  a.url,
                  b.ga_transactionRevenue AS ga_transactionRevenue,
                  b.ga_transactions       AS ga_transactions,
                  b.ga_pageviews          AS ga_pageviews,
                  b.ga_entrances          AS ga_entrances,
                  b.timestamp             AS timestamp
                FROM
                  analytics_urls a
                LEFT JOIN analytics_data_seofilter b
                  ON b.id_url = a.id
                WHERE
                  a.url = '$url'
                AND
                  DATE(timestamp) = '$timestamp'";

        $result_ga = $this->db->query($sql);

        if ($result_ga->num_rows > 0) {
          while ($row_ga = $result_ga->fetch_assoc()) {

            if (!empty($row_ga['ga_transactionRevenue'])) {
              $tr = $row_ga['ga_transactionRevenue'];
            }
            if (!empty($row_ga['ga_pageviews'])) {
              $pv = $row_ga['ga_pageviews'];
            }
            if (!empty($row_ga['ga_entrances'])) {
              $en = $row_ga['ga_entrances'];
            }
            if (!empty($row_ga['ga_transactions'])) {
              $tx = $row_ga['ga_transactions'];
            }

          }
        }

        $rows_ra[$key] = array(
          'k' => $rankset['k'],
          't' => $rankset['t'],
          'l' => $rankset['l'],
          'p' => $rankset['p'],
          'u' => $rankset['u'],
          'tr' => $tr,
          'pv' => $pv,
          'en' => $en,
          'tx' => $tx
        );

      }

    }
// ADWORDS DATA

// GWT DATA
    if ($this->gwt_account == '1') {

      $sql = "SELECT
                id
               FROM
                gwt_hostnames
               WHERE
                hostname = '$this->hostname'";

      $result_gwh = $this->db->query($sql);

      while ($row_gwh = $result_gwh->fetch_assoc()) {
        $hostname_id = $row_gwh['id'];
      }

      if (!empty($hostname_id)) {

        $sql = "SELECT
                  clicks,
                  query,
                  timestamp
                FROM
                  gwt_data
                WHERE
                  hostname_id = $hostname_id
                AND
                  DATE(timestamp) > CURDATE() - INTERVAL 7 DAY";

        $result_gw = $this->db->query($sql);

        $gwt_rows = array();

        while ($row_gw = $result_gw->fetch_assoc()) {

          if (!empty($row_gw['clicks'])) {
            $hash = crc32 ($row_gw['query'] . $row_gw['timestamp']);
            $gwt_rows[$hash] = $row_gw['clicks'];
          }

        }


        foreach ($rows_ra as $key => $rankset) {

          $timestamp = $rankset['t'];
          $keyword   = $rankset['k'];
          $cl        = 0;

          $hash2 = crc32 ($keyword . $timestamp);

          if (isset($gwt_rows[$hash2])) {

            if (!isset($rankset['tr'])) {
              $rankset['tr'] = 0;
              $rankset['pv'] = 0;
              $rankset['en'] = 0;
            }

            $rows_ra[$key] = array(
               'k'  => $rankset['k'],
               't'  => $rankset['t'],
               'l'  => $rankset['l'],
               'p'  => $rankset['p'],
               'u'  => $rankset['u'],
               'tr' => $rankset['tr'],
               'pv' => $rankset['pv'],
               'en' => $rankset['en'],
               'tx' => $rankset['tx'],               
               'cl' => $gwt_rows[$hash2]
              );

          }

        }

      }

    }
// GWT DATA

    $tmpfilename = 'metrics_rankings_data_'.$this->customer_id.'_x.tmp';
    $this->writeFile($tmpfilename, $rows_ra);

  }


  private function fetchOneDex()
  {

    $sql = "SELECT id FROM ruk_project_customers";

    $this->result = $this->db->query($sql);

    if (!$this->result) {
      $this->mySqlQueryError();
    }

    while ($row = $this->result->fetch_assoc()) {
      $rows[$row['id']] = $row;
    }

    // get all cached ranking sets
    foreach ($rows as $key => $c_id) {

      $file = 'metrics_rankings_data_'.$c_id['id'].'_x.tmp';
      $rowx = $this->readFile($file);
      $rowx = $this->fakeMissingTimeStamp($rowx);

      if (empty($rowx)) {
        continue;
      }

      foreach ($rowx as $row) {

        $keywords[$row['k']] = $row['k'];

        $this->rankset['_ts'][$row['t']] = $row['t'];

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {

          // fix missing or empty
          if (!isset($this->rankset['_ov'][$c_id['id']][$row['t']]['avg'])) {
            $this->rankset['_ov'][$c_id['id']][$row['t']]['avg'] = 0;
          }

          if (!isset($this->rankset['_ov'][$c_id['id']][$row['t']]['divider'])) {
            $this->rankset['_ov'][$c_id['id']][$row['t']]['divider'] = 0;
          }

          // prep data
          $this->rankset['_ov'][$c_id['id']][$row['t']]['avg']                   = $this->rankset['_ov'][$c_id['id']][$row['t']]['avg'] + $row['p'];
          $this->rankset['_ov'][$c_id['id']][$row['t']]['divider']               = $this->rankset['_ov'][$c_id['id']][$row['t']]['divider'] + 1;
          $this->rankset['_ov'][$c_id['id']][$row['t']]['kw'][$row['k']][]       = array ('rank' => $row['p']);

        }

      }

    }

    // fetch adwords data
    $this->googleadwords = $this->getAdwordsData($keywords);

    // calc dex
    foreach ($this->rankset['_ov'] as $c_id => $row) {

      $save[$c_id] = array ();

        krsort($this->rankset['_ts']);

        foreach ($this->rankset['_ts'] as $k => $ts) {

          if (!isset($this->rankset['_ov'][$c_id][$ts]['avg'])) {
            $save[$c_id][$ts]['avg'] = 0;
          }

          foreach ($this->rankset['_ov'][$c_id][$ts] as $key => $ranks) {

            if ($key == 'avg') {

              if ($this->rankset['_ov'][$c_id][$ts]['divider'] == 0) {
                $this->rankset['_ov'][$c_id][$ts]['divider'] = 1;
              }
              $ranktoday = round($ranks/$this->rankset['_ov'][$c_id][$ts]['divider'],0);
              $save[$c_id][$ts]['avg'] = $ranktoday;

            }

            if ($key == 'kw') {
              $dex = 0;
              foreach ($ranks as $keyword => $rank) {
                if (isset($rank[0]['rank'])) {
                  $rank = $rank[0]['rank'];
                } else {
                  $rank = 101;
                }
                if (isset($this->googleadwords[$keyword][3])) {
                  $opi = $this->googleadwords[$keyword][3];
                } else {
                  $opi = 1;
                }

                $dex = $dex + $this->calcOneDex($rank, $opi);
              }

              $save[$c_id][$ts]['dex'] = $dex;

            }

          }
        }

    }

    // save dex
    $tmpfilename = 'metrics_one_dex_all.tmp';
    $this->writeFile($tmpfilename, $save);

  }


  public function fakeMissingTimeStamp ($rows) {

    if (count($rows) == 0) {
      return $rows;
    }

    $yesterday = $this->dateYMDYesterday();

    foreach ($rows as $row) {

      $hash      = md5($row['k'] . $row['t'] . $row['p']);
      $curDate[] = strtotime($row['t']);
      $rows_hash[$hash] = $row;

      if ($row['t'] == $yesterday) {
        $keywords[$hash]  = array($row['k'], $row['p']);
      }

    }

    arsort($curDate);

    $today       = $this->dateYMD();
    $yesterday   = date('Y-m-d', $curDate[0]);
    $new_element = array();

    foreach ($keywords as $khash => $kw) {

      $hash_today =  md5($kw[0] . $today . $kw[1]);

      if (!isset($rows_hash[$hash_today])) {

        $hash_yesterday = md5($kw[0] . $yesterday . $kw[1]);

        if (!isset($rows_hash[$hash_yesterday])){continue;}

        $id_pos = $rows_hash[$hash_yesterday]['p'];
        $id_url = $rows_hash[$hash_yesterday]['u'];

        if (is_null($id_pos)){continue;}

        $new =  array('k' => $kw[0], 't' => $today, 'p' => $id_pos, 'u' => $id_url);
        $new_element[] = $new;

      }

    }

    $result = array_merge($new_element, $rows);

    return $result;

  }


  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 2, 0);

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  public function getAdwordsData ($keywords)
  {

    $this->reconMySql();

    // compare KEYWORDS TO CRAWL vs. SCRAPED KEYWORDS
    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    // get google adwords data
    // SV, CPC, COMP
    // from stored table

    $sql = "SELECT
              keyword,
              searchvolume,
              cpc,
              competition,
              opi
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $googleadwords = array();
    while ($row = $res->fetch_assoc()) {
     $googleadwords[$row['keyword']] = array ($row['searchvolume'], $row['cpc'], $row['competition'], $row['opi']);
    }

    return $googleadwords;

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      return unserialize($variable);

    } else {

      return null;

    }

  }


  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }

  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

  public function pureHostNameNoSubdomain ($host)
  {

    $domain = $host;

    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs) && $this->ignore_subd == 1) {
      return $regs['domain'];
    } else {
      return $host;
    }

  }

  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }


  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  private function sendAlertMailHTML ()
  {

    // ALERT EMAIL ONLY AT 7
    $hours = date('H',time());
    if ($hours > 5 && $hours < 9) {

      //$sendto  = 'matthias.hotz@gmail.com';
      $sendto  = 'oneteam-seo@advertising.de';
      $subject = 'OneProSeo / Alert / Fresh Ranking Data';

      $header  = 'MIME-Version: 1.0' . "\r\n";
      $header .= 'Content-Type: text/html; charset=ISO-8859-1' . "\r\n";
      $header .= 'From: OneProSeo.com <noreply@oneproseo.com>' . "\r\n" . 'Reply-To: support@oneproseo.com' . "\r\n" ;

      $message = '<html><head><title>OneProSeo / Alert / Fresh Ranking Data</title>';
      $message .= '<style>#btn:hover {background-color: #6ed032;}</style></head><body>';

      $message .= '
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="3" style="text-align:center; background-color:#444444; padding:10px;">
                <img style="width: 600px;" src="https://enterprise.oneproseo.com/oneproseo/src/images/oneproseo/one-pro-seo-logo-mail.png"/>
              </td>
            </tr>
            <tr>
              <td style="width:10px; background-color:#444444;"></td>
              <td style="text-align: center; padding: 20px; background-color:#f8f8f8;">
                <span style="font-size: 16px; font-color: #636363;">
                Hi Advertising Team,<br /><br />
                fresh rankings and OneDex data is available in OneProSeo.<br /><br /><br />
                <a href="https://enterprise.oneproseo.com/rankings/dashboard-rankings" id="btn" style="-webkit-transition: background-color 0.2s ease-out 0s; transition: background-color 0.2s ease-out 0s; background-color: #49ab0c; border: 0 none; border-radius: 5px; color: #fff; letter-spacing: 1px; line-height: 1.2; margin-top: 1em; min-width: 160px; padding: 14px 35px; text-align: center; text-decoration: none;" target="_blank">Take a look!</a>
                <br /><br /><br />
                cheers,<br />
                OneProSeo.com<br />
                Big Data SEO Analytics<br /><br /></span>
              </td>
              <td style="width:10px; background-color:#444444;"></td>
            </tr>
            <tr>
              <td colspan="3" style="text-align:center; background-color:#444444; height:10px;"></td>
            </tr>
          </table>';
      $message .= '</body></html>';

      mail($sendto, $subject, $message, $header, '-f noreply@oneproseo.com -r noreply@oneproseo.com');

    }

  }


}

new renderRUKOverviews;

?>
