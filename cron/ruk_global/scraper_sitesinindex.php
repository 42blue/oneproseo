<?php

date_default_timezone_set('CET');

class sitesupdate {

  private $logdir     = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $this->mySqlConnect();

    $this->fetchCustomerUrls();
    $this->prepareData();
    $this->scrapeData();

    $this->db->close();

  }

  private function fetchCustomerUrls ()
  {

    $sql = "SELECT id, url from ruk_project_customers";

    $result = $this->db->query($sql);

    $this->all_hosts = array();

    while ($row = $result->fetch_assoc()) {
      $this->all_hosts[$row['id']] = $row['url'];
    }

  }

  private function prepareData ()
  {

    foreach ($this->all_hosts as $id => $url) {

      $domain = parse_url($url, PHP_URL_HOST);
      $this->to_scrape[$id] = array ('domain' => $domain);

    }

  }

  private function scrapeData ()
  {

    foreach ($this->to_scrape as $id => $data) {

      echo $data['domain'] . PHP_EOL;

      $this->logToFile($this->dateHIS() . ' SITESININDEX - SCRAPE: ' . $data['domain']);

      $keywords   = array();
      $keywords[] = array('site:' . $data['domain'], 'de');
      $query      = base64_encode(json_encode($keywords));

      $json = $this->scrapeCurl('https://oneproseo.advertising.de/oneproapi/crawlinski/client.php?data=' . $query);
      
      $res = json_decode($json, true);

      foreach ($res as $key => $value) {
        $res_domain = $value['resultStat'];
      }

      $result = array();
      $result['id']             = $id;
      $result['res_api_host']   = $res_domain;
      $result['res_api_domain'] = $res_domain;
      $result['res_host']       = $res_domain;
      $result['res_domain']     = $res_domain;

      $this->logToFile($this->dateHIS() . ' SITESININDEX - WRITE: ' . $res_domain);

      $this->saveData($result);

    }

  }


  private function saveData ($result)
  {

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $timestamp = $this->dateYMD();

    $sql_ra = "('" . $result['id'] . "', '" . $result['res_host'] . "', '" . $result['res_domain'] . "','" . $result['res_api_host'] . "','" . $result['res_api_domain'] . "','" . $timestamp . "')";

    $query_ra = 'INSERT INTO
                    ruk_project_sites_index (id_customer, gen_results_host, gen_results_url, api_results_host, api_results_url, timestamp)
                  VALUES
                    '. $sql_ra .' ';

    $this->db->query($query_ra);

  }



  private function scrapeCurl ($url)
  {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 180);

    $output = curl_exec($ch);

    if (curl_exec($ch) === false) {
      // echo 'CurlError: ' . curl_error($ch). "\n";
      $output = false;
    }

    curl_close($ch);

    return $output;

  }


  public function mySqlConnect ()
  {
    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFile($this->dateHIS() . ' MYSQLERROR SITESINDEX');
      exit;
    }

  }

  public function dateYMD ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("Y-m-d", $date);

  }

  public function dateHIS ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i:s", $date);

  }

  public function logToFile ($content) {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }

}

new sitesupdate;

?>
