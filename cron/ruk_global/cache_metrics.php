<?php

require_once dirname(__FILE__) . '/ruk.class.php';

class cache_metrics extends ruk {

  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';
  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';

  public function __construct () {

    $this->mySqlConnect();

    $this->logToFile('METRICS: START');
    $this->fetchMetrics();
    $this->fetchOneDex();
    $this->logToFile('METRICS: END');

    $this->db->close();

  }

  private function fetchMetrics()
  {

    $data = array();

    $rankings_daily = 0;
    $rankings_weekly = 0;

    $keyword_amount = $this->db->query("SELECT DISTINCT b.keyword AS keyurl, a.measuring AS measuring FROM ruk_project_keyword_sets a LEFT JOIN ruk_project_keywords b ON b.id_kw_set = a.id");
    while ($row = $keyword_amount->fetch_assoc()) {
      if ($row['measuring'] == 'daily') {
        $rankings_daily++;
      } else {
        $rankings_weekly++;
      }
    }
    // LOCAL
    $keyword_amount_local = $this->db->query("SELECT id FROM ruk_local_keywords");
    $rankings_daily = $rankings_daily + $keyword_amount_local->num_rows;

    $data[0] = $rankings_daily + $rankings_weekly / 6;

    // ANZAHL KEYWORDS RUK
    $sql = "SELECT keyword FROM ruk_project_keywords";
    $res = $this->db->query($sql);
    $keywords1 = array();
    $keywords2 = array();
    while ($row = $res->fetch_assoc()) {
      $kw = nl2br(stripslashes($row['keyword']));
      $keywords1[]    = $kw;
    }
    $data[2] = count($keywords1);

    // ANZAHL URLS IN DER SCRAPE DB
    $sql = "SHOW TABLE STATUS FROM oneproseo_live;";
    $res = $this->db->query($sql);
    $amount_urls = 0;
    while ($row = $res->fetch_assoc()) {
      if ($row['Name'] == 'ruk_scrape_rankings') {
        $amount_urls = $row['Rows'];
      }
    }
    $data[1] = $amount_urls;

    // ANZAHL ALLER KEYWORDS ADWORDS DB
    $sql = "SELECT id FROM gen_keywords";
    $res = $this->db->query($sql);
    $data[3] = $res->num_rows;

    // ANZAHL KUNDEN
    $sql = "SELECT id FROM ruk_project_customers";
    $res = $this->db->query($sql);
    $data[4] = $res->num_rows;

    // ANZAHL KEYWORD SETS
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE type = 'key'";
    $res = $this->db->query($sql);
    $data[5] = $res->num_rows;

    // ANZAHL URL SETS
    $sql = "SELECT id FROM ruk_project_keyword_sets WHERE type = 'url'";
    $res = $this->db->query($sql);
    $data[6] = $res->num_rows;

    $tmpfilename = 'metrics_rankings.tmp';
    $this->writeFile($tmpfilename, $data);

  }

  private function fetchOneDex()
  {

    $sql = "SELECT id FROM ruk_project_customers";

    $this->result = $this->db->query($sql);

    if (!$this->result) {
      $this->mySqlQueryError();
    }

    while ($row = $this->result->fetch_assoc()) {
      $rows[$row['id']] = $row;
    }

    // get all cached ranking sets
    foreach ($rows as $key => $c_id) {

      $file = 'metrics_rankings_data_'.$c_id['id'].'_x.tmp';
      $rowx = $this->readFile($file);

      if (empty($rowx)) {
        $file = 'metrics_rankings_data_'.$c_id['id'].'_weekly.tmp';
        $rowx = $this->readFile($file);
      }

      if (empty($rowx)) {
        continue;
      }

      foreach ($rowx as $row) {

        $keywords[$row['k']] = $row['k'];

        $this->rankset['_ts'][$row['t']] = $row['t'];

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {

          // fix missing or empty
          if (!isset($this->rankset['_ov'][$c_id['id']][$row['t']]['avg'])) {
            $this->rankset['_ov'][$c_id['id']][$row['t']]['avg'] = 0;
          }

          if (!isset($this->rankset['_ov'][$c_id['id']][$row['t']]['divider'])) {
            $this->rankset['_ov'][$c_id['id']][$row['t']]['divider'] = 0;
          }

          // prep data
          $this->rankset['_ov'][$c_id['id']][$row['t']]['avg']                   = $this->rankset['_ov'][$c_id['id']][$row['t']]['avg'] + $row['p'];
          $this->rankset['_ov'][$c_id['id']][$row['t']]['divider']               = $this->rankset['_ov'][$c_id['id']][$row['t']]['divider'] + 1;
          $this->rankset['_ov'][$c_id['id']][$row['t']]['kw'][$row['k']][]       = array ('rank' => $row['p']);

        }

      }

    }

    // fetch adwords data
    $this->googleadwords = $this->getAdwordsData($keywords);

    // calc dex
    foreach ($this->rankset['_ov'] as $c_id => $row) {

      $save[$c_id] = array ();

        krsort($this->rankset['_ts']);

        foreach ($this->rankset['_ts'] as $k => $ts) {

          if (!isset($this->rankset['_ov'][$c_id][$ts]['avg'])) {
            $save[$c_id][$ts]['avg'] = 0;
          }

          foreach ($this->rankset['_ov'][$c_id][$ts] as $key => $ranks) {

            if ($key == 'avg') {

              if ($this->rankset['_ov'][$c_id][$ts]['divider'] == 0) {
                $this->rankset['_ov'][$c_id][$ts]['divider'] = 1;
              }
              $ranktoday = round($ranks/$this->rankset['_ov'][$c_id][$ts]['divider'],0);
              $save[$c_id][$ts]['avg'] = $ranktoday;

            }

            if ($key == 'kw') {
              $dex = 0;
              foreach ($ranks as $keyword => $rank) {
                if (isset($rank[0]['rank'])) {
                  $rank = $rank[0]['rank'];
                } else {
                  $rank = 101;
                }
                if (isset($this->googleadwords[$keyword][3])) {
                  $opi = $this->googleadwords[$keyword][3];
                } else {
                  $opi = 1;
                }

                $dex = $dex + $this->calcOneDex($rank, $opi);
              }

              $save[$c_id][$ts]['dex'] = $dex;

            }

          }
        }

    }

    // save dex
    $tmpfilename = 'metrics_one_dex_all.tmp';
    $this->writeFile($tmpfilename, $save);

  }

  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 2, 0);

  }


  // FETCH ADWORDS DATA FROM GEN_KEYWORDS TABLE
  public function getAdwordsData ($keywords)
  {

    $this->reconMySql();

    // compare KEYWORDS TO CRAWL vs. SCRAPED KEYWORDS
    $comma_separated_kws = implode('", "', $keywords);
    $comma_separated_kws = '"' . $comma_separated_kws . '"';

    // get google adwords data
    // SV, CPC, COMP
    // from stored table

    $sql = "SELECT
              keyword,
              searchvolume,
              cpc,
              competition,
              opi
            FROM
              gen_keywords
            WHERE
              keyword IN ($comma_separated_kws)
            ORDER BY
              keyword";

    $res = $this->db->query($sql);

    $googleadwords = array();
    while ($row = $res->fetch_assoc()) {
     $googleadwords[$row['keyword']] = array ($row['searchvolume'], $row['cpc'], $row['competition'], $row['opi']);
    }

    return $googleadwords;

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      return unserialize($variable);

    } else {

      return null;

    }

  }


  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }

}

new cache_metrics;

?>
