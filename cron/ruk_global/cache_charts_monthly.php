<?php

// sudo php cache_charts_monthly.php c 336

require_once dirname(__FILE__) . '/ruk.class.php';

class renderRUKOverviews extends ruk  {

  public $cache_dir = '/var/www/enterprise.oneproseo.com/temp/cache/';
  public $logdir    = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct ($argv) {

    $this->argv = $argv;

    $this->mySqlConnect();

    $this->logToFile('CACHE: CHARTS MONTHLY START');
    $this->fetchSets();
    $this->logToFile('CACHE: CHARTS MONTHLY END');

    $this->db->close();

  }

  private function fetchSets ()
  {

    $sql = "SELECT id, url, country, ga_account, gwt_account, competition FROM ruk_project_customers";

    if (isset($this->argv[1]) && isset($this->argv[2]) && $this->argv[1] == 'c') {
      $cid = $this->argv[2];
      $sql = "SELECT id, url, country, ga_account, gwt_account, competition FROM ruk_project_customers WHERE id = $cid";
    }

    $result = $this->db->query($sql);

    // GET ALL CUSTOMERS
    while ($row = $result->fetch_assoc()) {

      $this->project_id = $row['id'];
      echo 'PRO: ' . $this->project_id . PHP_EOL;

      $this->ga_account      = $row['ga_account'];
      $this->gwt_account     = $row['gwt_account'];
      $this->country         = $row['country'];
      $this->competition_all = $row['competition'];
      $this->hostname        = $this->pureHostName($row['url']);

      // LOAD CACHE ONCE
      $this->loadCache();
      $this->reconMySql();

      $sql     = "SELECT id, type, rs_type, competition FROM ruk_project_keyword_sets WHERE id_customer = '$this->project_id'";
      $result2 = $this->db->query($sql);

      if ($result2->num_rows == 0) {
        continue;
      }

      $workload = array();
      $workload['all'] = array('key', 'desktop');
      while ($row2 = $result2->fetch_assoc()) {
        $workload[$row2['id']] = array($row2['type'], $row2['rs_type'], $row2['competition']);
      }

//$workload = array(1973 => $workload[1973]);

      // MAIN LOOP
      foreach ($workload as $id => $type) {

        $this->competition_set = array();

        if(!isset($type[2]) || empty($type[2])) {
          $type[2] = serialize(array());
        }

        $this->keyword_setid   = $id;
        $this->campaign_type   = $type[0];
        $this->mo_type         = $type[1];
        $this->competition_set = $type[2];

        echo 'SET: ' . $this->keyword_setid . PHP_EOL;
        // generate summary
        $this->doChart();

      }

    }

  }

  public function loadCache() {
    $file = 'metrics_rankings_data_'.$this->project_id.'_monthly.tmp';
    $this->project_cache = $this->readFile($file);
    $file = 'metrics_rankings_data_competition_'.$this->project_id.'_monthly.tmp';
    $this->comp_cache = $this->readFile($file);
  }


  private function doChart ()
  {

    $this->cache_filename = 'charts/chart_'  . $this->project_id . '_' . $this->keyword_setid . '_monthly.tmp';
    $this->cache_filename_json = 'json/chart_'  . $this->project_id . '_' . $this->keyword_setid . '_monthly.json';

    $this->url_campaign = '';

    // KEYWORD CAMPAIGN OR URL CAMPAIGN
    if ($this->campaign_type == 'url') {
      $this->url_campaign = $this->fetchURLSet($this->keyword_setid);
    }

    if ($this->keyword_setid == 'all' || $this->keyword_setid == 'all_monthly' || $this->campaign_type == 'url') {
      $this->selectAllKeywordSets();
    } else {
      $this->selectKeywordSet();
    }

    // INTERCEPT
    // GOOGLE VERSION & COUNTRY
    if ($this->mo_type == 'desktop') {
      $this->mo_type = $this->country;
    } else {
      $this->mo_type = $this->mo_type . '_' . $this->country;
    }

    $this->getRankingDataChart();
    $this->dex_comp = array();
    if ($this->campaign_type != 'url') {
      $this->getRankingDataComp();
    }
    $this->renderViewChart();

  }

  private function getRankingDataComp() {

    $rows = $this->comp_cache;

    if (empty($rows)) {
      return;
    }

    $competition = unserialize($this->competition_all);

    if (!empty(unserialize($this->competition_set))) {
      $competition = unserialize($this->competition_set);
      $competition = array_merge ($competition, unserialize($this->competition_all));
    }

    $hostnames = array();
    foreach ($competition as $url) {
      $hostnames[$this->pureHostName($url)] = $url;
    }

    foreach ($rows as $hostname => $host) {
      $dupl = '';
      foreach ($host as $id => $row) {

        if (!isset($hostnames[$hostname])) {
          continue;
        }

        $searchurl = $hostnames[$hostname];

        // DEX LIVE
        if (isset($this->googleadwords[$row['k']][3])) {
          $opi = $this->googleadwords[$row['k']][3];
        } else {
          $opi = 0;
        }

        if (strripos($row['u'], $searchurl) !== false) {

          $hash = md5($row['t'] . $row['k']);
          
          if (!isset($dupl[$hash])) {
          
            if (isset($this->dex_comp[$searchurl][$row['t']])) {
              $add_dex = round($this->dex_comp[$searchurl][$row['t']], 5);
            } else {
              $add_dex = 0;
            }
          
            $this->dex_comp[$searchurl][$row['t']] = $this->calcOneDex($row['p'], $opi) + $add_dex;
          
            $dupl[$hash] = $hash;
          }

        }

      }

    }

  }

  private function renderViewChart()
  {

    if (!is_array($this->project_cache)) {return;}

    $chart = array();
    $json  = array();    

    if (isset($this->rankset['_ts'])) {

      $chart['ctitle'] = '12 Monate OneDex';
      $chart['s_2']    = false;
      $chart['s_3']    = false;
      $chart['s_4']    = false;
      $chart['s_5']    = false;
      $chart['s_6']    = false;
      $chart['s_7']    = false;
      $chart['s_8']    = false;
      $chart['s_9']    = false;
      $chart['s_10']   = false;
      $chart['s_11']   = false;
      $chart['s_12']   = false;
      $chart['s_13']   = false;
      $chart['s_14']   = false;
      $chart['s_15']   = false;

      $chart['name_1']       = 'OneDex: ' . $this->hostname;

      // ONEDEX FROM CACHE (ALL KEYWORDS) or LIVE (SELECTED KEYWORSET)
      foreach ($this->dex as $ts => $val) {
        $timestamp = strtotime($ts);
        $chart['categories'][] = date('d.m.Y', $timestamp);
        $chart['series_1'][]   = $val;
        $json[] = array('ts' => $ts, 'host' => $this->hostname, 'val' => $val);         
      }

      // COMPETITION CHARTS
      $i = 5;
      foreach ($this->dex_comp as $hostname => $dataset) {
        $i++;
        $chart['s_'.$i]    = true;
        $chart['name_'.$i] = 'OneDex: ' . $hostname;
        foreach ($dataset as $ts => $val) {
          $chart['series_'.$i][] = $val;
          $json[] = array('ts' => $ts, 'host' => $hostname, 'val' => $val);
        }
      }

      if (!empty($this->ga_account)) {
        // ONEDEX FROM CACHE (ALL KEYWORDS) or LIVE (SELECTED KEYWORSET)
        foreach ($this->dex as $ts => $val) {

          if (!isset($this->rankset['_tr'][$ts])) {
            $chart['series_2'][] = null;
          } else {
            $chart['series_2'][] = $this->rankset['_tr'][$ts];
            $chart['s_2'] = true;
          }

          if (!isset($this->rankset['_vi'][$ts])) {
            $chart['series_3'][] = null;
          } else {
            $chart['series_3'][] = $this->rankset['_vi'][$ts];
            $chart['s_3'] = true;
          }

          if (!isset($this->rankset['_en'][$ts])) {
            $chart['series_5'][] = null;
          } else {
            $chart['series_5'][] = $this->rankset['_en'][$ts];
            $chart['s_5'] = true;
          }

          if (!isset($this->rankset['_tx'][$ts])) {
            $chart['series_15'][] = null;
          } else {
            $chart['series_15'][] = $this->rankset['_tx'][$ts];
            $chart['s_15'] = true;
          }

        }

      }

      if ($this->gwt_account == '1') {
        foreach ($this->dex as $ts => $val) {
          if (!isset($this->rankset['_cl'][$ts])) {
            $chart['series_4'][] = null;
          } else {
            $chart['series_4'][] = $this->rankset['_cl'][$ts];
            $chart['s_4'] = true;
          }
        }
      }

      // get first available timestamp
      reset($this->rankset['_ov']);
      $first_ts = key($this->rankset['_ov']);

      $q = 0;
      $rank10 = 0;
      $rank20 = 0;
      $rank30 = 0;

      $q = round($this->rankset['_ov'][$first_ts]['avg']/$this->rankset['_ov'][$first_ts]['divider'],0);

      if (isset($this->rankset['_ov'][$first_ts]['rank10'])){
        $rank10 = 0 + $this->rankset['_ov'][$first_ts]['rank10'];
      } else {
        $rank10 = 0;
      }

      if (isset($this->rankset['_ov'][$first_ts]['rank20'])){
        $rank20 = $rank10 + $this->rankset['_ov'][$first_ts]['rank20'];
      } else {
        $rank20 = $rank10;
      }

      if (isset($this->rankset['_ov'][$first_ts]['rank30'])){
        $rank30 = $rank10 + $rank20 + $this->rankset['_ov'][$first_ts]['rank30'];
      } else {
        $rank30 = $rank10 + $rank20;
      }

      $res = array('top10' => $rank10, 'top20' => $rank20, 'top30' => $rank30, 'q' => $q, 'chart' => $chart);

    } else {

      $chart['categories'][] = $this->dateYMD();
      $chart['series'][]     = 100;
      $res = array('top10' => 0, 'top20' => 0, 'top30' => 0, 'q' => 0, 'chart' => $chart);

    }


    $this->writeFile($this->cache_filename, $res);
    $this->writeFileRaw($this->cache_filename_json, json_encode($json, JSON_PRETTY_PRINT));    

  }


  private function selectKeywordSet ()
  {

    $this->keywords = array();

    $selected_keyword_set = $this->keyword_setid;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   a.name        AS setname,
                   b.keyword     AS keyword,
                   c.url         AS url,
                   c.id          AS customerid
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id = '".$selected_keyword_set."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      $this->keywords[] = $row['keyword'];
    }

  }


  private function selectAllKeywordSets ()
  {

    $this->keywords = array();

    $selected_project = $this->project_id;

    $this->reconMySql();

    $sql = "SELECT a.id          AS setid,
                   b.keyword     AS keyword
            FROM ruk_project_keyword_sets a
              LEFT JOIN ruk_project_keywords b
                ON b.id_kw_set = a.id
              LEFT JOIN ruk_project_customers c
                ON c.id = a.id_customer
            WHERE a.id_customer = '".$selected_project."'";

    $result = $this->db->query($sql);

    while ($row = $result->fetch_assoc()) {
      if (empty($row['keyword'])) {continue;}
      $this->keywords[] = $row['keyword'];
    }

  }


  private function getRankingDataChart()
  {

    $rows = $this->project_cache;

    if (!is_array($this->project_cache)) {return;}

    $adwordsloc = $this->getAdwordsLoc($this->country);
    $this->googleadwords = $this->getAdwordsDataI18N($this->keywords, $adwordsloc[0], $adwordsloc[1]);

// COMPARE OPTIONS
    // KEYWORD OR URL CAMPAIGN
    if (!empty($this->url_campaign)) {

      // STRAIGHT URL
      $compare_against_wildcard = array();
      $compare_against = $this->url_campaign;

      // CHECK FOR WILDCARDS
      foreach ($this->url_campaign as $key => $value) {
        if (stripos($key, '*') !== false) {
          $compare_against_wildcard[$key] = $value;
        }
      }

      // DB ROW
      $compare_with = 'u';

    } else {

      // flip for comparison
      $compare_against_wildcard = array();
      $compare_against = array_flip($this->keywords);
      $compare_with = 'k';

    }

    $this->rankset = array();
    $this->dex = array();

// COMPARE OPTIONS
    $dupl = array();
    $duplga = array();

    foreach ($rows as $row) {

      if ($this->mo_type != $row['l']) {
        continue;
      }

      if ( isset($compare_against[$row[$compare_with]]) || $this->wildcardfinder($compare_against_wildcard, $row[$compare_with]) === true ) {

        // get all measured days
        $this->rankset['_ts'][$row['t']] = $row['t'];

    // DEX LIVE
        if (isset($this->googleadwords[$row['k']][3])) {
          $opi = $this->googleadwords[$row['k']][3];
        } else {
          $opi = 0;
        }

        // create unique hash (ranking + day) so that only the first ranking gets recognized
        $hash = md5($row['t'] . $row['k']);
        if (!isset($dupl[$hash])) {
          if (isset($this->dex[$row['t']])) {
            $add_dex = round($this->dex[$row['t']], 5);
          } else {
            $add_dex = 0;
          }
          $this->dex[$row['t']] = $this->calcOneDex($row['p'], $opi) + $add_dex;
          $dupl[$hash] = $hash;
        }
    // DEX LIVE

    // TRANSACTIONS
        if (isset($row['tx']) && $row['tx'] != '0') {
          $hashga = md5($row['t'] . $row['u']);
          if (!isset($duplga[$hashga])) {
            if (isset($this->rankset['_tx'][$row['t']])) {
              $this->rankset['_tx'][$row['t']] = floatval($row['tx']) + $this->rankset['_tx'][$row['t']];
            } else {
              $this->rankset['_tx'][$row['t']] = floatval($row['tx']);
            }
            $duplga[$hashga] = $hashga;
          }
        }
    // TRANSACTIONS

    // TRANSACTION REV
        if (isset($row['tr']) && $row['tr'] != '0') {
          $hashga = md5($row['t'] . $row['u']);
          if (!isset($duplga[$hashga])) {
            if (isset($this->rankset['_tr'][$row['t']])) {
              $this->rankset['_tr'][$row['t']] = floatval($row['tr']) + $this->rankset['_tr'][$row['t']];
            } else {
              $this->rankset['_tr'][$row['t']] = floatval($row['tr']);
            }
            $duplga[$hashga] = $hashga;
          }
        }
    // TRANSACTION REV

    // TRAFFIC
        if (isset($row['pv']) && $row['pv'] != '0') {
          $hashgax = md5($row['t'] . $row['u']);
          if (!isset($duplgax[$hashgax])) {
            if (isset($this->rankset['_vi'][$row['t']])) {
              $this->rankset['_vi'][$row['t']] = floatval($row['pv']) + $this->rankset['_vi'][$row['t']];
            } else {
              $this->rankset['_vi'][$row['t']] = floatval($row['pv']);
            }
            $duplgax[$hashgax] = $hashgax;
          }
        }
    // TRAFFIC


    // ENTRANCES
        if (isset($row['en']) && $row['en'] != '0') {
          $hashgaen = md5($row['t'] . $row['u']);
          if (!isset($duplgaen[$hashgaen])) {
            if (isset($this->rankset['_en'][$row['t']])) {
              $this->rankset['_en'][$row['t']] = floatval($row['en']) + $this->rankset['_en'][$row['t']];
            } else {
              $this->rankset['_en'][$row['t']] = floatval($row['en']);
            }
          }
        }
    // ENTRANCES

    // GWT CLICKS
        if (isset($row['cl']) && $row['cl'] != '0') {
          $hashgwt = md5($row['t'] . $row['k']);
          if (!isset($duplgwt[$hashgwt])) {
            if (isset($this->rankset['_cl'][$row['t']])) {
              $this->rankset['_cl'][$row['t']] = $row['cl'] + $this->rankset['_cl'][$row['t']];
            } else {
              $this->rankset['_cl'][$row['t']] = $row['cl'];
            }
            $duplgwt[$hashgwt] = $hashgwt;
          }
        }
    // GWT CLICKS

        // todays dates
        if ($row['t'] === $this->rankset['_ts'][$row['t']]) {

          $this->summarySetdataMerge($row['t'], 'avg', $row['p']);
          $this->summarySetdataMerge($row['t'], 'divider', 1);

          if ($row['p'] >= 1 && $row['p'] <= 10) {
            $this->summarySetdataMerge($row['t'], 'rank10', 1);
          }
          if ($row['p'] > 10 && $row['p'] <= 20) {
            $this->summarySetdataMerge($row['t'], 'rank20', 1);
          }
          if ($row['p'] > 20 && $row['p'] <= 30) {
            $this->summarySetdataMerge($row['t'], 'rank30', 1);
          }

        }

      }

    }

  }


  public function summarySetdataMerge($ts, $type, $add)
  {

    if (isset($this->rankset['_ov'][$ts][$type])) {
      $this->rankset['_ov'][$ts][$type] = $this->rankset['_ov'][$ts][$type] + $add;
    } else {
      $this->rankset['_ov'][$ts][$type] = $add;
    }

  }


  // FETCH URLS in KEYWORD CAMPAIGN
  public function fetchURLSet($kw_set_id)
  {

    $this->reconMySql();

    $sql = "SELECT
              url
            FROM
              ruk_project_urls
            WHERE
              id_kw_set ='$kw_set_id'";

    $result = $this->db->query($sql);

    $url_campaign = array();

    while ($row = $result->fetch_assoc()) {
      $url_campaign[$row['url']] = $row['url'];
    }

    return $url_campaign;

  }


  // WILDCARD COMPARISON FOR URL CAMPAIGNS
  public function wildcardfinder ($wildcard_array, $haystack) {

    if (!is_array($wildcard_array)) {
      return false;
    }

    foreach ($wildcard_array as $key => $value) {

      $key = trim($key, '*');

      if (strripos($haystack, $key) !== false) {
        return true;
      }

    }

    return false;

  }


  public function dateYMD ()
  {
    return date("Y-m-d", strtotime('today'));
  }


  public function dateYMDYesterday ()
  {
    return date('Y-m-d', strtotime('-1 days'));
  }


  // PROPER GERMAN TS
  public function germanWD ($ts)
  {

    $day[0] = 'So';
    $day[1] = 'Mo';
    $day[2] = 'Di';
    $day[3] = 'Mi';
    $day[4] = 'Do';
    $day[5] = 'Fr';
    $day[6] = 'Sa';

    $dayn = date('w', $ts);

    return $day[$dayn];

  }

  // PROPER GERMAN TS
  public function germanTS ($ts)
  {

    $day[0] = 'So';
    $day[1] = 'Mo';
    $day[2] = 'Di';
    $day[3] = 'Mi';
    $day[4] = 'Do';
    $day[5] = 'Fr';
    $day[6] = 'Sa';

    $dayn = date('w', strtotime($ts));

    return $day[$dayn] . '., ' . date('d.m.Y', strtotime($ts));

  }


  public function checkDifferenceBetweenRankingDaysReverse ($today, $yesterday)
  {

    if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      return ' <i class="status-success icon-circle-arrow-up"></i> <small class="status-success">+' .  $change . '</small>';
    } else if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      return ' <i class="status-error icon-circle-arrow-down"></i> <small class="status-error">-' .  $change . '</small>';
    }

  }


  public function checkDifferenceBetweenRankingDays ($today, $yesterday, $symbol = true)
  {

    $out = '';

    if ($today > $yesterday) {
      $change = intval($today) - intval($yesterday);
      if ($symbol === true) {$out = '<i class="status-success icon-circle-arrow-up"></i> ';} else {$perc = '';}
      $out .= '<small class="status-success">+' . number_format($change, 0, ',', '.') . '</small>';
    } else if ($today < $yesterday) {
      $change = intval($yesterday) - intval($today);
      if ($symbol === true) {$out = '<i class="status-error icon-circle-arrow-down"></i> ';} else {$perc = '';}
      $out .= '<small class="status-error">-' .  number_format($change, 0, ',', '.') . '</small>';
    }

    return $out;

  }


  // DEX
  public function calcOneDex ($rank, $opi)
  {

    if ($rank > 101) {$rank = 101;}

    $rounds = 0;
    $add = 0;

    for ($i = 101; $i >= $rank; $i--) {

      $rounds++;

      if ($i > 94 && $i <= 100) {
        $add = $add + 0;
      } else if ($i >= 30 && $i <= 94) {
        $add = $add + 0.1;
      } else if ($i >= 20 && $i < 30) {
        $add = $add + 0.2;
      } else if ($i > 10 && $i < 20) {
        $add = $add + 0.5;
      } else if ($i == 10) {
        $add = $add + 2;
      } else if ($i == 9) {
        $add = $add + 3;
      } else if ($i == 8) {
        $add = $add + 4;
      } else if ($i == 7) {
        $add = $add + 5;
      } else if ($i == 6) {
        $add = $add + 6;
      } else if ($i == 5) {
        $add = $add + 7;
      } else if ($i == 4) {
        $add = $add + 10;
      } else if ($i == 3) {
        $add = $add + 12;
      } else if ($i == 2) {
        $add = $add + 18;
      } else if ($i == 1) {
        $add = $add + 20;
      }

    }

    if ($opi == 0) {
      $opi = 1;
    }

    $dex = $opi * $add / 100000;

    return round($dex, 3, 0);

  }

  public function writeFileRaw ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, $variable);
    fclose($handle);

  }


  public function writeFile ($fileName, $variable)
  {

    $fileName = $this->cache_dir . $fileName;

    $handle = fopen($fileName, 'w');
    fwrite($handle, serialize($variable));
    fclose($handle);

  }


  public function readFile ($fileName)
  {

    $fileName = $this->cache_dir . $fileName;

    if (file_exists($fileName)) {

      $handle   = fopen($fileName, 'rb');
      $variable = fread($handle, filesize($fileName));
      fclose($handle);

      $variable = unserialize($variable);

      return $variable;

    } else {

      return null;

    }

  }


  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }

}

new renderRUKOverviews ($argv);

?>
