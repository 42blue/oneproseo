<?php

date_default_timezone_set('CET');

// http://suite.searchmetrics.com/kpi_research_seosem_trend/organic-visibility?url=http://www.seorch.de&cc=DE&filter=%7B%7D&type=&link=research%2Fdomains%2Forganic&acc=0

class fetchSeodata {

  private $logdir   = '/var/www/enterprise.oneproseo.com/cron/scrapelogs/scrapelog.txt';

  public function __construct () {

    $is_monday = (date('N', strtotime($this->dateYMD())));
    if ($is_monday != 1) {
      $this->logToFile($this->dateHIS() . ' SEO DATA ERROR NOT MONDAY ');
      exit;
    }

    $this->mySqlConnect();

    $this->fetchCustomerUrls();
    $this->getSearchmetricsData();

    $this->db->close();

  }


  private function fetchCustomerUrls ()
  {

    $sql = "SELECT id, url, country from ruk_project_customers";

    $result = $this->db->query($sql);

    $this->all_hosts = array();

    while ($row = $result->fetch_assoc()) {
      $host = $this->pureHostName($row['url']);
      $this->all_hosts[$row['id']] = array($host, $row['country']);
    }

  }


  private function getSearchmetricsData ()
  {

    $timestamp = $this->dateYMD();

    foreach ($this->all_hosts as $id => $dta) {

      $ctx = stream_context_create(array('http'=>
        array( 'timeout' => 1200 )
      ));

      $url     = $dta[0];
      $country = $dta[1];

      $data = file_get_contents('http://suite.searchmetrics.com/kpi_research_seosem_trend/organic-visibility?url='.$url.'&cc='.$country.'&filter=%7B%7D&type=&link=research%2Fdomains%2Forganic&acc=0', false, $ctx);

      $score = false;

      $dom   = $this->createDomDocument($data);
      $score = $this->processResult($dom);

      $score = str_replace('.', '', $score);
      $score = str_replace(',', '', $score);
      $score = intval($score);

      $sql_ra[] = "('".$id."', '".$score."', '".$timestamp."')";

      sleep(1);

    }

    echo $q_string_ra = implode(',', $sql_ra);

    // CHECK IF CONNECTION IS ALIVE
    if ($this->db->ping() === false) {
      $this->mySqlConnect();
    }

    $query_ra = 'INSERT INTO
                    ruk_project_searchmetrics (id_customer, score, timestamp)
                  VALUES
                    '. $q_string_ra .' ';

    $call = $this->db->query($query_ra);

    if (!empty($this->db->error)) {
      $this->logToFile($this->dateHIS() . ' SEARCHMETRICS DATA: DB ERROR: ' . $this->db->error);
    }

    $this->logToFile($this->dateHIS() . ' SEARCHMETRICS DATA');

  }


  public function pureHostName ($url)
  {
    $pure_host  = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
    return $pure_host;
  }


  public function mySqlConnect ()
  {

    // https://oneproseo.advertising.de/onepma/
    $this->db = new mysqli('192.168.20.2', 'oneproseo', 'zeB3b4SLnZuNu3dT', 'oneproseo_live');

    // set charset according to DB
    $this->db->set_charset('utf8');

    if (mysqli_connect_errno()) {
      $this->logToFile($this->dateHIS() . ' SQL ERROR ' . mysqli_connect_error());
      exit();
    }

  }


  private function processResult($dom) {

    $check  = $dom->query('/html/body/div/div/div/text()');

    $value = 0;

    foreach ($check as $node) {
      $value = $dom->query('.', $node)->item(0)->nodeValue;
    }

    return $value;

  }


  public function createDomDocument ($html) {

    $dom = new DOMDocument ('1.0');
    @$dom->loadHTML($html);
    $xpath = new DOMXpath($dom);

    return $xpath;

  }


  public function dateYMD ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();

    return date("Y-m-d", $date);

  }


  public function dateHIS ()
  {

    $date = new DateTime();
    $date = $date->getTimestamp();
    return date("h:i:s", $date);

  }


  public function logToFile ($content)
  {

    $fh = fopen($this->logdir, 'a');
    fwrite($fh, $content . "\r\n");
    fclose($fh);

  }


}

new fetchSeodata;

?>
