<?php

ini_set('memory_limit', '3000M');
date_default_timezone_set('CET');

// include onepro
require_once "onepro_library/onepro.conf.php";

// $config->setEnvironment('prod');
$config->setEnvironment('dev');

// IMPORTANT: set correct domain in JS functions file
// oneproseo/src/custom/config.js
// set debug mode for bugfixing

onepro_setDebugMode();

// load Slim PHP library
onepro_requireFilename("Slim/Slim.php");
onepro_requireCustomFilename("OnePro_Slim_Routes.php");

?>
