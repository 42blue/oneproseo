<?php
/**
* Database static class
* 
* @author PowerWorx GmbH
* @copyright (C) PowerWorx GmbH 2012-2015
* @package OnePro
* @version 2.3
*/
class OnePro_DB {
  // initial connection
  public static $dbName = '';
  public static $user = '';
  public static $password = '';
  public static $host = 'localhost';
  public static $port = null;
  public static $encoding = 'latin1';
  
  // configure workings
  public static $param_char = '%';
  public static $named_param_seperator = '_';
  public static $success_handler = false;
  public static $error_handler = true;
  public static $throw_exception_on_error = false;
  public static $nonsql_error_handler = null;
  public static $throw_exception_on_nonsql_error = false;
  public static $nested_transactions = false;
  public static $usenull = true;
  public static $ssl = array('key' => '', 'cert' => '', 'ca_cert' => '', 'ca_path' => '', 'cipher' => '');
  public static $connect_options = array(MYSQLI_OPT_CONNECT_TIMEOUT => 30);
  
  // internal
  protected static $mysql_connector = null;
  protected static $mysql_connector_array = array();
  
  public static function getMySqlConnector()
  {
    #$mysql_connector = OnePro_DB::$mysql_connector;
    
    #if ($mysql_connector === null || $mysql_connector->user !== OnePro_DB::$user) {
    #  $mysql_connector = OnePro_DB::$mysql_connector = new OnePro_MySQL_Connector();
    #}
    
    $sHash = OnePro_DB::$user . '_' . OnePro_DB::$dbName;
    
    $mysql_connector = OnePro_DB::$mysql_connector_array[$sHash];
    if (!is_object($mysql_connector)) {
      $mysql_connector = OnePro_DB::$mysql_connector_array[$sHash] = new OnePro_MySQL_Connector();
    }
    
    static $variables_to_sync = array('param_char', 'named_param_seperator', 'success_handler', 'error_handler', 'throw_exception_on_error',
      'nonsql_error_handler', 'throw_exception_on_nonsql_error', 'nested_transactions', 'usenull', 'ssl', 'connect_options');

    $db_class_vars = get_class_vars('OnePro_DB'); // the OnePro_DB::$$var syntax only works in 5.3+

    foreach ($variables_to_sync as $variable) {
      if ($mysql_connector->$variable !== $db_class_vars[$variable]) {
        $mysql_connector->$variable = $db_class_vars[$variable];
      }
    }
    
    return $mysql_connector;
  }
  
  // yes, this is ugly. __callStatic() only works in 5.3+
  public static function get() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'get'), $args); }
  public static function disconnect() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'disconnect'), $args); }
  public static function query() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'query'), $args); }
  public static function queryFirstRow() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryFirstRow'), $args); }
  public static function queryOneRow() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryOneRow'), $args); }
  public static function queryAllLists() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryAllLists'), $args); }
  public static function queryFullColumns() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryFullColumns'), $args); }
  public static function queryFirstList() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryFirstList'), $args); }
  public static function queryOneList() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryOneList'), $args); }
  public static function queryFirstColumn() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryFirstColumn'), $args); }
  public static function queryOneColumn() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryOneColumn'), $args); }
  public static function queryFirstField() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryFirstField'), $args); }
  public static function queryOneField() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryOneField'), $args); }
  public static function queryRaw() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryRaw'), $args); }
  public static function queryRawUnbuf() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'queryRawUnbuf'), $args); }
  
  public static function insert() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'insert'), $args); }
  public static function insertIgnore() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'insertIgnore'), $args); }
  public static function insertUpdate() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'insertUpdate'), $args); }
  public static function replace() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'replace'), $args); }
  public static function update() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'update'), $args); }
  public static function delete() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'delete'), $args); }
  
  public static function insertId() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'insertId'), $args); }
  public static function count() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'count'), $args); }
  public static function affectedRows() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'affectedRows'), $args); }
  
  public static function useDB() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'useDB'), $args); }
  public static function startTransaction() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'startTransaction'), $args); }
  public static function commit() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'commit'), $args); }
  public static function rollback() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'rollback'), $args); }
  public static function tableList() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'tableList'), $args); }
  public static function columnList() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'columnList'), $args); }
  
  public static function sqlEval() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'sqlEval'), $args); }
  public static function nonSQLError() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'nonSQLError'), $args); }
  
  public static function serverVersion() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'serverVersion'), $args); }
  public static function transactionDepth() { $args = func_get_args(); return call_user_func_array(array(OnePro_DB::getMySqlConnector(), 'transactionDepth'), $args); }
  
  
  public static function debugMode($handler = true) { 
    OnePro_DB::$success_handler = $handler;
  }
  
}
?>