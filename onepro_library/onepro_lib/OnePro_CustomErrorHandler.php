<?php
/**
* Custom Error Handler
* 
* @author PowerWorx GmbH
* @copyright (C) PowerWorx GmbH 2012-2013
* @package OnePro
* @version 1.09
*/
function OnePro_CustomErrorHandler($error_level, $error_message, $error_file, $error_line, $error_context)
{
	$aTemp = explode("/",$error_file);
	$error_file = array_pop($aTemp); // get only the file name without path
	switch ($error_level) {
		case E_USER_ERROR:
			echo "<b>Fatal error:</b> $error_message in <b>$error_file</b> on line <b>$error_line</b><br />\n";
			exit(1);
		break;
		case E_USER_WARNING:
			echo "<b>Warning:</b> $error_message in <b>$error_file</b> on line <b>$error_line</b><br />\n";
		break;
		case E_USER_NOTICE:
			echo "<b>Notice:</b> $error_message in <b>$error_file</b> on line <b>$error_line</b><br />\n";
		break;
		default: break;
	}
	// thus, the internal PHP error handling is not executed
	return true;
}
set_error_handler("OnePro_CustomErrorHandler");
?>