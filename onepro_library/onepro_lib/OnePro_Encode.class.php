<?php
class OnePro_Encode {

  var $csconv;
  var $aCountryCharEncode;
  
	function main()	{
		$this->aLanguageCharEncode = array(
			'tr' => array('Ş'=>'S','ş'=>'s','Ö'=>'O','ö'=>'o','Ü'=>'U','ü'=>'u','Ğ'=>'G','ğ'=>'g','Ç'=>'C','ç'=>'c'),
		);
		
		$t[0] = "Égypte";
		$t[1] = "String  mit doppelten Leerzeichen";
		$t[2] = "Россия";
		$t[3] = "Ντίμελζεε ";
		$t[4] = "Schleifreisen (Thüringen)";
		$t[5] = "Фёль";
		$t[6] = "Центральная Америка";
	
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['t3lib_cs_utils'] = 'mbstring';
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['curlUse'] = 0;
		

		// made following changes to t3lib/class.t3lib_cs.php:
		// 1. added function getUrl (copied from t3lib/class.t3lib_div.php)
		// 2. changed function initUnicodeData($mode=null)
		// - adapted utf8-cache table reference
		// - changed call to getUrl
		$this->csconv = new OnePro_Encode_Worker();
		
		if($_GET['w'] AND strlen($_GET['w'])<55) $word = $_GET['w'];
		if($word != '') {
			$content = $this->encode($word);
		} else {
			foreach($t as $testName)
			{
				$content .= '<b>' . $testName . '</b> -> <b>' . $this->encode($testName) . '</b><br>';
			}
		}
	
		#return $content;
	}

    function encode($title, $language = "")
    {  	
        $aZeichen = array
        (
            "'" => "",
            "´" => "",
            "`" => "",
            "\\" => "",
        );
        
        if($language !== "" && is_array($this->aLanguageCharEncode[$language])) 
    		{
    			$aZeichen = array_merge($aZeichen, $this->aLanguageCharEncode[$language]);
    		}

        $title = strtr($title, $aZeichen);
        $charset = 'utf-8';

        // Convert to lowercase:
        //$processedTitle = $GLOBALS['TSFE']->csConvObj->conv_case($charset, $title, 'toLower');
        $processedTitle = $this->csconv->conv_case($charset, $title, 'toLower');

        // Convert some special tokens to the space character:
        $space = '-';
        $processedTitle = preg_replace('/[ \-+_]+/', $space, $processedTitle); // convert spaces

        // Convert extended letters to ascii equivalents:
        $processedTitle = $this->csconv->specCharsToASCII($charset, $processedTitle);

        // Strip the rest...:
        $processedTitle = preg_replace ('/[^a-zA-Z0-9\\' . $space . ']/', '', $processedTitle); // strip the rest
        $processedTitle = preg_replace ('/\\' . $space . '+/', $space, $processedTitle); // Convert multiple 'spaces' to a single one
        $processedTitle = trim($processedTitle, $space);

        /*
        if ($this->conf['encodeTitle_userProc']) {
            $params = array('pObj' => &$this, 'title' => $title, 'processedTitle' => $processedTitle);
            $processedTitle = t3lib_div::callUserFunction($this->conf['encodeTitle_userProc'], $params, $this);
        }
        */

        // Return encoded URL:
        return rawurlencode(strtolower($processedTitle));
    }

}
?>