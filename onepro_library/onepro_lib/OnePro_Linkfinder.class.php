<?php
class OnePro_Linkfinder
{
	public $result = array();

	public function __construct ($linktofind, $searchhere)
	{
		$this->str_Linktofind = $linktofind;
		$this->arr_Searchhere = $searchhere;
		
		// remove duplictate URLs and reset keys
		$this->arr_Searchhere = array_values(array_unique($this->arr_Searchhere));

		$this->gofind();
	}

	public function gofind()
	{
		// 1. START MULTI CURL -> ARR with HTML
		// FOREACH
		//	 2. HTML RESULT -> DOM OBJECT
		//	 3. DOM -> FIND ALL LINKS
		//	 4. CREATE ABSOLUTE LINK URLS
		// 5. COMPARE WITH linkstofind
		// 6. RETURN WHERE LINK WAS FOUND ARR

		$links_on_url = array();
		$curl_result = $this->multiCurl();
		$result = array();

		// get all links on url
		foreach ($curl_result as $baseurl => $data) 
		{
			$result[$baseurl]['result'] = 0;
			$result[$baseurl]['http_code'] = $data['http_code'];
			if($data['http_code'] == "200")
			{
				$html = $data['content'];
				$dom = $this->createDomDocument($html);
				$links_on_url[$baseurl] = $this->findlinks($baseurl, $dom);
				$metas_on_url[$baseurl] = $this->findmetas($dom);
				$result[$baseurl]['http_code'] = $data['http_code'];
			}
		}
		
		// check all links vs. arr_Linktofind
		foreach ($links_on_url as $baseurl => $urls) 
		{
			if(count($urls['link']) > 0)
			{
				foreach ($urls['link'] as $key => $url) 
				{
					// force remove get params
					$url = strtok($url, '?');
					if ($url == $this->str_Linktofind) 
					{
						$result[$baseurl]['result'] = 1;
						$result[$baseurl]['link'] = $url;
						$result[$baseurl]['text'] = $urls['text'][$key];
						$result[$baseurl]['rel'] = $urls['rel'][$key];
					}
				}
			}
		}
		
		#var_dump($links_on_url);
		#var_dump($result);
		$this->result = $result;
		$this->result_meta = $metas_on_url;
	}


	public function getResult() 
	{
		return $this->result;
	}


	public function getMetaData()
	{
		return $this->result_meta;
	}

	private function findmetas($dom) 
	{
		$param = $dom->getElementsByTagName('meta');

		$keywords = '';
		$application_name = '';

		foreach ($param as $key => $value) {

			if (strtolower($value->getAttribute('name')) == 'keywords') {
				$keywords = $param->item($key)->getAttribute('content');
			}

			if (strtolower($value->getAttribute('name')) == 'application-name') {
				$application_name = $param->item($key)->getAttribute('content');
			}
			
			if (strtolower($value->getAttribute('name')) == 'robots') {
				$robots = $param->item($key)->getAttribute('content');
			}
		}

		$res = array ('keywords' => $keywords, 'application_name' => $application_name, 'robots' => $robots);
		return $res;
	}

	private function findlinks($baseurl, $dom) 
	{
		$links = array();
		$param = $dom->getElementsByTagName('a');

		$checkforbase = $dom->getElementsByTagName('base');
		foreach ($checkforbase as $base) {
			$baseurl = $base->getAttribute('href');
		}

		foreach ($param as $key => $element) 
		{
			$foundUrlHref = $element->getAttribute('href');

			// no href e.g. anchors
			if (empty($foundUrlHref)) {
				continue;
			}

			// href starts with # -> internal anchor
			if ('#' == substr($foundUrlHref, 0, 1)) {
				continue;
			}

			$abs_url = $this->rel2abs($foundUrlHref, $baseurl);
			$linktext = $this->getTextFromNode($element);
			
			$rel = "";
			if (strtolower($element->getAttribute('rel')) == "nofollow") {
				$rel = "nofollow";
			}
			
			$links['link'][] = $abs_url;
			$links['text'][] = $linktext;
			$links['rel'][] = $rel;
		}

		return $links;
	}

	private function multiCurl() 
	{
		$requestResults = array();
		$handleArray = array();

		foreach ($this->arr_Searchhere as $name => $reqUrl) 
		{
			$handle = curl_init();
			curl_setopt($handle, CURLOPT_URL, $reqUrl);
			curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($handle, CURLOPT_REFERER, "");
			curl_setopt($handle, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
			curl_setopt($handle, CURLOPT_HEADER, false);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($handle, CURLOPT_FOLLOWLOCATION, false);
			curl_setopt($handle, CURLOPT_TIMEOUT, 35);
			curl_setopt($handle, CURLOPT_FAILONERROR, true);

			array_push($handleArray, array($reqUrl, $handle));
		}

		// create multi handle
		$mh = curl_multi_init();

		// add handles
		foreach ($handleArray as $k => $handle) 
		{
			curl_multi_add_handle($mh, $handle[1]);
		}

		$running = null;

		do {
			curl_multi_exec($mh, $running);
		} while ($running > 0);

		// get the result and save it in the result ARRAY
		foreach ($handleArray as $k => $handle)
		{
			$requestResults[$handle[0]]['content'] = curl_multi_getcontent($handle[1]);
			$info = curl_getinfo($handle[1]);
			$requestResults[$handle[0]]['http_code'] = $info['http_code'];
			
			if (curl_errno($handle[1])) {
				$requestResults[$handle[0]]['content'] = 'abort';
			}
		}
		
		// remove all handles
		foreach ($handleArray as $k => $handle) {
			curl_multi_remove_handle($mh, $handle[1]);
		}

		curl_multi_close($mh);

		return $requestResults;
	}


	function rel2abs($rel, $base)
	{
		// return if already absolute URL
		if (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;

		// queries and anchors 
		if ($rel[0]=='#' || $rel[0]=='?') return $base.$rel;

		/* parse base URL and convert to local variables:
			 $scheme, $host, $path */
		extract(parse_url($base));

		/* remove non-directory element from path */
		$path = preg_replace('#/[^/]*$#', '', $path);

		/* destroy path if relative url points to root */
		if ($rel[0] == '/') $path = '';

		/* dirty absolute URL */
		$abs = "$host$path/$rel";

		// replace '//' or '/./' or '/foo/../' with '/'
		$re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
		for($n=1; $n>0; $abs=preg_replace($re, '/', $abs, -1, $n)) {}

		$hex = explode('#', $abs);
		if (isset($hex[0])) {
			$abs = $hex[0];
		}
		return $scheme.'://'.$abs;
	}
	
	function getInnerHtml($node)
	{
		$innerHTML= '';
		$children = $node->childNodes;
		foreach ($children as $child) {
			$innerHTML .= $child->ownerDocument->saveXML( $child );
		}
		return $innerHTML;
	}
	
	function getTextFromNode($node, $text = "")
	{ 
		if ($node->tagName == null)
		{
			return $text . $node->textContent; 
		}
		
		if ($node->tagName == "img" && $node->hasAttribute("alt"))
		{
			return $text . $node->getAttribute("alt");
		}
		
		$node = $node->firstChild; 
		if ($node != null) 
			$text = $this->getTextFromNode($node, $text); 
		
		while($node->nextSibling != null) { 
			$text = $this->getTextFromNode($node->nextSibling, $text); 
			$node = $node->nextSibling; 
		}
		
		return $text; 
	} 

	function createDomDocument ($html) {
		$dom = new DOMDocument ('1.0');
		@$dom->loadHTML($html);
		return $dom;
	}
}
?>
