<?php

use OneAdvertising\ScreamingfrogCrawler;
use OneAdvertising\ScreamingfrogUtilities;

\Slim\Slim::registerAutoloader();

session_start();

$slim = new \Slim\Slim(array(
  'templates.path' => 'oneproseo/templates/'
));

// LOAD ONEPRO CONFIG and MAP TO SLIM
$config = OnePro_Config::getInstance();
$env    = $slim->environment();

// SET DATABASE CREDENTIALS
onepro_setDbCredentials("oneproseo");
onepro_setDatabase("oneproseo");

// INCLUDES
require_once $config->get('path') . 'oneproseo/includes/db.class.php';
require_once $config->get('path') . 'oneproseo/includes/cache.class.php';
require_once $config->get('path') . 'oneproseo/includes/menu.class.php';
require_once $config->get('path') . 'oneproseo/includes/userrole.class.php';
require_once $config->get('path') . 'oneproseo/includes/metrics.class.php';

$ops_menu = new ops_menu;

// set configs for SLIM TEMPLATES
$slim->view->setData('domain', $config->get('domain'));
$slim->view->setData('path', $config->get('path'));
$slim->view->setData('csvstore', $config->get('csvstore'));
$slim->view->setData('csvurl', $config->get('csvurl'));
$slim->view->setData('googledrive', $config->get('googledrive'));
$slim->view->setData('tempcache', $config->get('tempcache'));

$slim->view->setData('ops_menu', $ops_menu->getMenu());
$slim->view->setData('customer_version', $config->get('customer_version'));
$slim->view->setData('path_info', $env['PATH_INFO']);

$slim->view->setData('ruk_max_keywords', $config->get('ruk_max_keywords'));
$slim->view->setData('ruk_max_keywords_set', $config->get('ruk_max_keywords_set'));
$slim->view->setData('ruk_max_keywordsets', $config->get('ruk_max_keywordsets'));

$slim->view->setData('mysql_dbhost', $config->get('mysql_dbhost'));
$slim->view->setData('mysql_dbname', $config->get('mysql_dbname'));
$slim->view->setData('mysql_dbuser', $config->get('mysql_dbuser'));
$slim->view->setData('mysql_dbpass', $config->get('mysql_dbpass'));

$slim->view->setData('mongo_dbhost', $config->get('mongo_dbhost'));
$slim->view->setData('mongo_dbname', $config->get('mongo_dbname'));
$slim->view->setData('mongo_dbuser', $config->get('mongo_dbuser'));
$slim->view->setData('mongo_dbpass', $config->get('mongo_dbpass'));

new Userrole ($slim);

$metrics = new metrics ($slim->view->getData());
$slim->view->setData('core_data_metrics', $metrics->getData());

// TEMP LOGIN
$authenticate = function ($login) {

  return function () use ($login) {
    // get config instance
    $config = OnePro_Config::getInstance();
    $app = \Slim\Slim::getInstance();
    $env = $app->environment();

    if ($login == FALSE) {
      $_SESSION['login_request_uri'] = $env['PATH_INFO'];
      $app->redirect($config->get('domain') . 'login');
    }

    // allow multiple ajax requests (SESSION)
    session_write_close();

    // HACKABLE URLS
    if (stripos($env['PATH_INFO'], '/crawler/') === 0 && $app->view->getData('modules_crawler') == 0) {
      $app->redirect($config->get('domain'));
    }
    if (stripos($env['PATH_INFO'], '/rankings/') === 0 && $app->view->getData('modules_ranker') == 0) {
      $app->redirect($config->get('domain'));
    }
    if (stripos($env['PATH_INFO'], '/keywords/') === 0 && $app->view->getData('modules_keyworder') == 0) {
      $app->redirect($config->get('domain'));
    }
    if (stripos($env['PATH_INFO'], '/structure/') === 0 && $app->view->getData('modules_structure') == 0) {
      $app->redirect($config->get('domain'));
    }
    if (stripos($env['PATH_INFO'], '/linktracker/') === 0 && $app->view->getData('modules_linktracker') == 0) {
      $app->redirect($config->get('domain'));
    }
    if (stripos($env['PATH_INFO'], '/tools/') === 0 && $app->view->getData('modules_tools') == 0) {
      $app->redirect($config->get('domain'));
    }

  };
};

// HOME
$slim->get('/', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'OneProSeo', 'javascript' => 'loader-home.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'OneProSeo - Enterprise'));
  $slim->render('gen-home.php');
  $slim->render('gen-footer.php');
});

// LOGIN
$slim->map('/login', function () use ($slim) {
  $slim->render('gen-header-login.php', array('titletag' => 'OneProSeo - Enterprise', 'javascript' => 'loader-login.js'));
  $slim->render('gen-login.php');
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

$slim->map('/logout', function () use ($slim) {
  $slim->render('gen-header-login.php', array('titletag' => 'OneProSeo - Enterprise', 'javascript' => 'loader-login.js'));
  $slim->render('gen-logout.php');
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

$slim->map('/password', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Passwort ändern / OneProSeo', 'javascript' => 'loader-gen-password.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-password.php', array('env' => $env));
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

// Lighthouse ROUTES
// shows dashboard
$slim->get('/lighthouse/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
    require_once __DIR__ ."/../../oneproseo/application/lighthouse/GeneralUtilities.class.php";

    $slim->render('gen-header.php', array('titletag' => 'Dashboard / Lighthouse / OneProSeo', 'javascript' => 'loader-lighthouse-dashboard.js'));
    $slim->render('gen-sidebar.php');
    $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
    $slim->render('lighthouse-dashboard.php');
    $slim->render('gen-footer.php');
});
// form for page types
$slim->map('/lighthouse/forms', function () use ($slim) {
    require_once __DIR__ ."/../../oneproseo/application/lighthouse/GeneralUtilities.class.php";
    $parsedBody = $slim->request()->params();
    $indexTemplate = file_get_contents(__DIR__ . '/../../oneproseo/application/lighthouse/templates' . $parsedBody['templatefile']);

    //edit formular
    if($_POST['project'] > 0)
    {
        $aProject = \OneAdvertising\GeneralUtilities::getDataForLighthouseProject($_POST['project']);

        $urls = unserialize($aProject['urls']);
        $urls = implode("\n", $urls);

        $indexTemplate = str_replace('###projectname###', $aProject['projectname'], $indexTemplate);
        $indexTemplate = str_replace('###projecturls###', $urls, $indexTemplate);
        $indexTemplate = str_replace('###buttonvalue###', 'Update', $indexTemplate);
    }
    else
    {
        $indexTemplate = str_replace('###projectname###', '', $indexTemplate);
        $indexTemplate = str_replace('###projecturls###', '', $indexTemplate);
        $indexTemplate = str_replace('###buttonvalue###', 'Hinzufügen', $indexTemplate);
        $_POST['project'] = 0;
    }

    $indexTemplate = str_replace('###id###', $_POST['project'], $indexTemplate);

    echo $indexTemplate;
})->via('GET', 'POST');
// insert page types
$slim->map('/lighthouse/pagetypes', function () use ($slim) {
    require_once __DIR__ ."/../../oneproseo/application/lighthouse/GeneralUtilities.class.php";
    $postData = $slim->request()->params();
    //print_R($postData);
    $projectname = $postData['projectname'];
    $urls = trim($postData['urls']) ;
    $aUrls = explode("\n", $urls);
    $id = $postData['id'];
    $bValid = true;
    $j = 0;

    //validate urls
    foreach($aUrls as $sUrl)
    {
        $sUrl = filter_var($sUrl, FILTER_SANITIZE_URL);
        $sUrl = trim($sUrl);
        $aTempUrls[] = $sUrl;

        if (filter_var($sUrl, FILTER_VALIDATE_URL) === false)
        {
            $bValid = false;
            break;
        }

        $j++;
    }

    $aUrls = $aTempUrls;

    if($bValid && $j <= 15)
    {
        $urls = serialize($aUrls);

        // save data
        \OneAdvertising\GeneralUtilities::saveUrls(
            $projectname,
            $urls,
            $status = 'completed',
            $user = 1,
            $usergroup = 2,
            $id
        );

        $response = '';
    }
    else
    {
        //JS alert error
        $response = 'error';
    }

    echo $response;
})->via('GET', 'POST');
// start lighthouse test
$slim->map('/lighthouse/start', function () use ($slim) {
    require_once __DIR__ ."/../../oneproseo/application/lighthouse/GeneralUtilities.class.php";
    $postData = $slim->request()->params();
    $id = $postData['project'];

    if($id > 0)
    {
        $aProject = \OneAdvertising\GeneralUtilities::getDataForLighthouseProject($id);

        $urls = unserialize($aProject['urls']);
        $urls = implode(",", $urls);


        $sRequestID = \OneAdvertising\GeneralUtilities::getAPIRequestId($urls);
        if($sRequestID === false)
        {
            echo 'error';
            return;
        }

        //for testing
        //$sRequestID = '255bc2ba7a34d308595f16bc501dcd99';

        //update status
        \OneAdvertising\GeneralUtilities::updateStatus($id,'running');


        $lighthouseCommand = 'php ' . __DIR__ . '/../../oneproseo/application/lighthouse/getResults.php ' . $id . ' ' . $sRequestID;

        exec($lighthouseCommand, $output);

        $response = 'success: ' . $sRequestID;
    }
    else
        $response = 'error';

    echo $response;
})->via('GET', 'POST');
// charts with lighthouse tests
$slim->get('/lighthouse/summary/:projectid', $authenticate($_SESSION['login']), function ($id) use ($slim) {
    require_once __DIR__ ."/../../oneproseo/application/lighthouse/GeneralUtilities.class.php";

    $slim->render('gen-header.php', array('titletag' => 'Dashboard / Lighthouse / OneProSeo', 'javascript' => 'loader-lighthouse-dashboard.js'));
    $slim->render('gen-sidebar.php');
    $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
    $slim->render('lighthouse-dashboard.php');

    $indexTemplate = file_get_contents(__DIR__ . '/../../oneproseo/application/lighthouse/templates/lighthouse/lighthouse_summary.html');

    if($id > 0)
    {
        $aProject = \OneAdvertising\GeneralUtilities::getDataForLighthouseProject($id);
        $sCharts = \OneAdvertising\GeneralUtilities::getChartsForProject($aProject);

        $indexTemplate = str_replace('###projectname###', $aProject['projectname'], $indexTemplate);
        $indexTemplate = str_replace('###content###', $sCharts, $indexTemplate);

        $response = $indexTemplate;
    }

    echo $response;

    $slim->render('gen-footer.php');
});
// latest test
$slim->get('/lighthouse/summary/:requestid/:file', $authenticate($_SESSION['login']), function ($requestid,$file) use ($slim) {
    require_once __DIR__ ."/../../oneproseo/application/lighthouse/GeneralUtilities.class.php";

    $slim->render('gen-header.php', array('titletag' => 'Dashboard / Lighthouse / OneProSeo', 'javascript' => 'loader-lighthouse-dashboard.js'));
    $slim->render('gen-sidebar.php');
    $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
    $slim->render('lighthouse-dashboard.php');

    $indexTemplate = file_get_contents(__DIR__ . '/../../temp/LighthouseResult/' . $requestid . '/' . $file);
    echo '<div class="lighthouse-charts container"><div class="col-sm-12">' . $indexTemplate . '</div></div>';

    $slim->render('gen-footer.php');
});


// SEO CRAWLER ROUTES
// shows dashboard
$slim->get('/seocrawler/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
    $slim->render('gen-header.php', array('titletag' => 'Dashboard / SEO Crawler / OneProSeo', 'javascript' => 'loader-seocrawler-dashboard.js'));
    $slim->render('gen-sidebar.php');
    $slim->render('gen-header-sub-crawler.php', array('headline' => 'Screaming Frog SEO Spider'));
    $slim->render('seocrawler-dashboard.php');
    $slim->render('gen-footer.php');
});
// return requestet forms
$slim->map('/seocrawler/forms', function () use ($slim) {
    $parsedBody = $slim->request()->params();
    $file = file_get_contents(__DIR__ . '/../../oneproseo/application/seocrawler/templates' . $parsedBody['templatefile']);
    echo $file;
})->via('GET', 'POST');
// gets status of api data
$slim->map('/seocrawler/status', function () use ($slim) {
    require_once __DIR__ . "/../../oneproseo/application/seocrawler/ScreamingfrogCrawler.class.php";
    require_once __DIR__ ."/../../oneproseo/application/seocrawler/ScreamingfrogUtilities.class.php";
    $parsedBody = $slim->request()->params();
    $content = '';
    $counter = 0;
    // screamingfrog api
    if ($parsedBody['application'] === 'crawler') {
        $apiResponseStatusRaw = ScreamingfrogCrawler::getStatus();
        // if somethings wrong with the requestid
        if (empty($apiResponseStatusRaw)) {
            echo json_encode(['No Screaming Frog Crawler found']);
            return;
        }
        // latest request first
        $apiResponseStatus = array_reverse($apiResponseStatusRaw);
        foreach ($apiResponseStatus as $screamingfrogRequest) {
            $requestData = ScreamingfrogUtilities::getProcessDataByProcessid(
                $screamingfrogRequest['processid'],
                $_SESSION['userid'],
                'screamingfrog'
            );
            $content .= '<div class="card">
                                <div class="card-header" id="heading' . $counter . '">
                                    <div class="d-flex w-100 justify-content-between" data-toggle="collapse" data-target="#collapse' . $counter . '" aria-expanded="true" aria-controls="collapse' . $counter . '">
                                        <h5 class="mb-1">' . $requestData['request']['crawlerurl'] . ' <span class="badge badge-primary">' . $screamingfrogRequest['status'] . '</span></h5>
                                        <small>' . date("d.m.Y H:i", $requestData['crdate']) . '</small>
                                    </div>
                                </div>
                                <div id="collapse' . $counter . '" class="collapse" aria-labelledby="heading' . $counter . '" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="row">

                                        ';
            if (isset($screamingfrogRequest['files'])) {
                $content .= '<div class="col-12"><h5>Result files</h5><p class="text-muted">Download Screaming Frog Seo Spider result files.</p><hr></div>';
                foreach ($screamingfrogRequest['files'] as $file) {
                    $content .= '<div class="col-4">';
                    $content .= '<a href="' . $file['link'] . '" target="_blank"><svg class="bi bi-download" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M.5 8a.5.5 0 0 1 .5.5V12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V8.5a.5.5 0 0 1 1 0V12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8.5A.5.5 0 0 1 .5 8z"/>
  <path fill-rule="evenodd" d="M5 7.5a.5.5 0 0 1 .707 0L8 9.793 10.293 7.5a.5.5 0 1 1 .707.707l-2.646 2.647a.5.5 0 0 1-.708 0L5 8.207A.5.5 0 0 1 5 7.5z"/>
  <path fill-rule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v8a.5.5 0 0 1-1 0v-8A.5.5 0 0 1 8 1z"/>
</svg> &nbsp;&nbsp;' . $file['file'] . '</a>';
                    $content .= '</div>';
                }
            } else {
                $content .= '<div class="col-12"><pre>' . $screamingfrogRequest['log'][0] . '</pre></div>';
            }
            $content .= '</div></div></div></div>';
            $counter++;
        }
    }
    // set templates
    $indexTemplate = file_get_contents(__DIR__ . '/../../oneproseo/application/seocrawler/templates' .  $parsedBody['templatefile']);
    $viewTemplate = str_replace('###CONTENT###', $content, $indexTemplate);
    // return
    echo $viewTemplate;
})->via('GET', 'POST');
// starts crawler
$slim->map('/seocrawler/crawl', function () use ($slim) {
    require_once __DIR__ . "/../../oneproseo/application/seocrawler/ScreamingfrogCrawler.class.php";
    require_once __DIR__ ."/../../oneproseo/application/seocrawler/ScreamingfrogUtilities.class.php";
    $parsedBody = $slim->request()->params();
    $screamingFrogCrawler = new ScreamingfrogCrawler($parsedBody);


})->via('GET', 'POST');


// CRAWLER ROUTES
//$slim->get('/crawler/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
//  $slim->render('gen-header.php', array('titletag' => 'Dashboard / Crawler / OneProSeo', 'javascript' => 'loader-crawler-dashboard.js'));
//  $slim->render('gen-sidebar.php');
//  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
//  $slim->render('crawler-dashboard.php');
//  $slim->render('gen-footer.php');
//});
//$slim->get('/crawler/task', $authenticate($_SESSION['login']), function () use ($slim) {
//  $slim->render('gen-header.php', array('titletag' => 'Crawlauftrag / Crawler / OneProSeo', 'javascript' => 'loader-crawler-dashboard.js'));
//  $slim->render('gen-sidebar.php');
//  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Crawlauftrag'));
//  $slim->render('crawler-task.php');
//  $slim->render('gen-footer.php');
//});
//
//$slim->get('/crawler/analyse/:crawlid', $authenticate($_SESSION['login']), function ($crawlid) use ($slim) {
//  $slim->render('gen-header.php', array('titletag' => 'Analyse / Crawler / OneProSeo', 'javascript' => 'loader-crawler-analyse.js'));
//  $slim->render('gen-sidebar.php');
//  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Analyse'));
//  $slim->render('crawler-analyse.php', array('crawlid' => $crawlid));
//  $slim->render('gen-footer.php');
//});
//
//$slim->get('/crawler/analyse/:crawlid/site/:siteid', $authenticate($_SESSION['login']), function ($crawlid, $siteid) use ($slim) {
//  $slim->render('gen-header.php', array('titletag' => 'Analyse / Einzelseite / Crawler / OneProSeo', 'javascript' => 'loader-crawler-analyse-site.js'));
//  $slim->render('gen-sidebar.php');
//  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelseite'));
//  $slim->render('crawler-analyse-site.php', array('crawlid' => $crawlid, 'siteid' => $siteid));
//  $slim->render('gen-footer.php');
//});
//
//$slim->get('/crawler/analyse/:crawlid/task/:taskid', $authenticate($_SESSION['login']), function ($crawlid, $taskid) use ($slim) {
//  $slim->render('gen-header.php', array('titletag' => 'Analyse / Einzelanalyse / Crawler / OneProSeo', 'javascript' => 'loader-crawler-analyse-task.js'));
//  $slim->render('gen-sidebar.php');
//  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelanalyse'));
//  $slim->render('crawler-analyse-task.php', array('crawlid' => $crawlid, 'taskid' => $taskid));
//  $slim->render('gen-footer.php');
//});

// JSON DATATABLE
$slim->get('/crawler/analyse/:crawlid/tasks/:taskid', $authenticate($_SESSION['login']), function ($crawlid, $taskid) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Analyse / Einzelanalyse / Crawler / OneProSeo', 'javascript' => 'loader-crawler-analyse-json.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelanalyse'));
  $slim->render('crawler-analyse-tasks.php', array('crawlid' => $crawlid, 'taskid' => $taskid));
  $slim->render('gen-footer.php');
});

$slim->get('/crawler/analyse/:crawlid/taskcheck/:taskid/:value', $authenticate($_SESSION['login']), function ($crawlid, $taskid, $value) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Analyse / Einzelanalyse / Crawler / OneProSeo', 'javascript' => 'loader-crawler-analyse-taskcheck.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Einzelanalyse'));
  $slim->render('crawler-analyse-taskcheck.php', array('crawlid' => $crawlid, 'taskid' => $taskid, 'value' => $value));
  $slim->render('gen-footer.php');
});


// SITECLINIC
$slim->get('/crawler/siteclinic', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Dashboard / Panda Clinic / OneProSeo', 'javascript' => 'loader-siteclinic-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-crawler.php', array('headline' => 'Dashboard'));
  $slim->render('siteclinic-dashboard.php');
  $slim->render('gen-footer.php');
});
$slim->get('/crawler/siteclinic/:host', $authenticate($_SESSION['login']), function ($host) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Dashboard / Siteclinic / OneProSeo', 'javascript' => 'loader-siteclinic-host.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Siteclinic: ' . $host));
  $slim->render('siteclinic-host.php', array('host' => $host));
  $slim->render('gen-footer.php');
});
$slim->get('/crawler/siteclinic/:host/:date/result', $authenticate($_SESSION['login']), function ($host, $date) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Dashboard / Sitelinic / OneProSeo', 'javascript' => 'loader-siteclinic-detail.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Siteclinic: ' . $host));
  $slim->render('siteclinic-detail.php', array('host' => $host, 'date' => $date));
  $slim->render('gen-footer.php');
});
$slim->get('/crawler/siteclinic/:host/:date/purge', $authenticate($_SESSION['login']), function ($host, $date) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Dashboard / Sitelinic / OneProSeo', 'javascript' => 'loader-siteclinic-detail-purge.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Siteclinic: ' . $host));
  $slim->render('siteclinic-detail.php', array('host' => $host, 'date' => $date));
  $slim->render('gen-footer.php');
});


// RANKING ROUTES
$slim->get('/rankings/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Dashboard / Rankings / OneProSeo', 'javascript' => 'loader-rankings-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Dashboard'));
  $slim->render('rankings-dashboard.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/dashboard-rankings', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Visibility Daily / Rankings / OneProSeo', 'javascript' => 'loader-rankings-dashboard-rankings.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Visibility Daily'));
  $slim->render('rankings-dashboard-rankings.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/dashboard-visibility', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Visibility Weekly / Rankings / OneProSeo', 'javascript' => 'loader-rankings-visibility.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Visibility Weekly'));
  $slim->render('rankings-dashboard-visibility.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/top-rankings', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'diva-e Advertising Top Rankings / OneProSeo', 'javascript' => 'loader-rankings-top-rankings.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'diva-e Advertising Top Rankings'));
  $slim->render('rankings-top-rankings.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/dashboard-custom', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Personal Dashboard / Rankings / OneProSeo', 'javascript' => 'loader-rankings-dashboard-custom.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Personal Dashboard'));
  $slim->render('rankings-dashboard-custom.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/live-ranking', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Live Rankings / Rankings / OneProSeo', 'javascript' => 'loader-rankings-live-rankings.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Live Rankings'));
  $slim->render('rankings-live-rankings.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/projects', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Neues Projekt / Rankings / OneProSeo', 'javascript' => 'loader-rankings-projects.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Administration'));
  $slim->render('rankings-projects.php');
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/projects-change/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Neues Projekt / Rankings / OneProSeo', 'javascript' => 'loader-rankings-projects-change.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Projekt bearbeiten'));
  $slim->render('rankings-projects-change.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Projektübersicht / Rankings / OneProSeo', 'javascript' => 'loader-rankings-overview.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Projektübersicht'));
  $slim->render('rankings-overview.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/mis/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
	$aProject = OnePro_DB::queryFirstRow("SELECT * FROM `ruk_project_customers` WHERE `id` = " . $id);
  $cf_home = 'gen-home-cf'.$id.'.php';
  $slim->render('gen-header-ca.php', array('titletag' => $aProject['name'] . ' - OneProSeo Dashboard'));
  $slim->render('gen-header-cf.php');
  $slim->render('gen-header-gen-cf.php', array('id' => $id));
  $slim->render($cf_home);
  $slim->render('gen-footer-cf.php');
});

// DETAIL VIEW SETS
$slim->get('/rankings/overview/:id/detail-keyword/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword Übersicht / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Übersicht'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'URL Übersicht (Campaign) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht (Campaign)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-onedex/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'URL Übersicht OneDex (Campaign) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-url-onedex.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht OneDex (Campaign)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'URL Übersicht (DB) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-url-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht (DB)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-onedex-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'URL Übersicht OneDex (DB) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-url-onedex-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht OneDex (DB)'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-url-onedex-analytics-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'URL Übersicht Analytics / OneDex / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-url-onedex-analytics-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'URL Übersicht Analytics / OneDex'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-keyword-url/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword / URL Übersicht / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-keyword-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword / URL Übersicht'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-keyword-url-duplicate/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Mehrfache Rankings Keyword / URL Übersicht / OneProSeo', 'javascript' => 'loader-rankings-detail-keyword-url-duplicates.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Mehrfache Rankings Keyword / URL Übersicht'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-personal-index/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Personal Index / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-personal-index.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Personal Index'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-compare/:set_id(/:startdate)', $authenticate($_SESSION['login']), function ($id, $set_id, $startdate = '01.01.2018') use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword Datumsvergleich / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-compare.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Datumsvergleich'));
  $slim->render('rankings-detail-compare.php', array('id' => $id, 'set_id' => $set_id, 'startdate' => $startdate));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/detail-competition/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword Wettbewerbsvergleich / Rankings / OneProSeo', 'javascript' => 'loader-rankings-detail-competition.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Wettbewerbsvergleich'));
  $slim->render('rankings-detail-competition.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Campaign bearbeiten / Rankings / OneProSeo', 'javascript' => 'loader-rankings-keywords.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Campaign bearbeiten'));
  $slim->render('rankings-keywords.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/tags/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Campaign Tags bearbeiten / Rankings / OneProSeo', 'javascript' => 'loader-rankings-keywords-tags.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Campaign Tags bearbeiten'));
  $slim->render('rankings-keywords-tags.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords-copy/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Campaign kopieren / Rankings / OneProSeo', 'javascript' => 'loader-rankings-keywords-copy.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Campaign kopieren'));
  $slim->render('rankings-keywords-copy.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywordsets', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keywordsets bearbeiten / Rankings / OneProSeo', 'javascript' => 'loader-rankings-keywordsets.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordsets bearbeiten'));
  $slim->render('rankings-keywordsets.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords/results/:keyword/:timestamp/:country', $authenticate($_SESSION['login']), function ($id, $keyword, $timestamp, $country) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Top 100 / Rankings / OneProSeo', 'javascript' => 'loader-rankings-results.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Top 100'));
  $slim->render('rankings-results.php', array('id' => $id, 'keyword' => $keyword, 'timestamp' => $timestamp, 'country' => $country));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords/details/:keyword/:country', $authenticate($_SESSION['login']), function ($id, $keyword, $country) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword Details / Rankings / OneProSeo', 'javascript' => 'loader-rankings-details-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'keyword' => $keyword, 'country' => $country));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/keywords/details2/:keyword/:country', $authenticate($_SESSION['login']), function ($id, $keyword, $country) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword Details / Rankings / OneProSeo', 'javascript' => 'loader-rankings-details-keyword2.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'keyword' => $keyword, 'country' => $country));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/overview/:id/api', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'API / Rankings / OneProSeo', 'javascript' => 'loader-rankings-api.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'API'));
  $slim->render('rankings-api.php', array('id' => $id));
  $slim->render('gen-footer.php');
});

// LOCAL RANKINGS ROUTES
$slim->get('/rankings/local/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Rankings / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Rankings'));
  $slim->render('rankings-local.php', array('id' => $id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/keywordsets', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Rankings Campaigns / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-keywordsets.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Campaigns'));
  $slim->render('rankings-local-keywordsets.php', array('id' => $id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/keywords(/:set_id)', $authenticate($_SESSION['login']), function ($id, $set_id = 0) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Keywordsets bearbeiten / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-keywords.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordsets bearbeiten'));
  $slim->render('rankings-local-keywords.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/keywords-csv', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Keywordsets bearbeiten (CSV) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-keywords-csv.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordsets (CSV) bearbeiten'));
  $slim->render('rankings-local-keywords.php', array('id' => $id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-keyword/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Keyword Übersicht / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Keyword Übersicht'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local URL Übersicht / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local URL Übersicht (DB) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-url-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht (DB)'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-onedex/:set_id+', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local URL Übersicht OneDex (Campaign) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-url-onedex.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht OneDex (Campaign)'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-onedex-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local URL Übersicht OneDex (DB) / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-url-onedex-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht OneDex (DB)'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-keyword-url/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Keyword / URL Übersicht / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-keyword-url.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Keyword / URL Übersicht'));
  $slim->render('rankings-local-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-url-onedex-analytics-complete/:set_id', $authenticate($_SESSION['login']), function ($id, $set_id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local URL Übersicht Analytics / OneDex / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-url-onedex-analytics-complete.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local URL Übersicht Analytics / OneDex'));
  $slim->render('rankings-detail-master.php', array('id' => $id, 'set_id' => $set_id));
  $slim->render('gen-footer.php');
});
// ONE KEYWORD / ONE LOCATION
$slim->get('/rankings/local/:id/keywords/details/:keyword/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $keyword, $country, $location, $type) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keyword Details / Local Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-details-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keyword Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'keyword' => $keyword, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});
// ALL KEYWORDS / ONE LOCATION
$slim->get('/rankings/local/:id/set/keywords/:setid/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $setid, $country, $location, $type) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keywordset Details / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-details-keyword.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordset Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'setid' => $setid, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});
// ONE KEYWORD / ALL LOCATIONS
$slim->get('/rankings/local/:id/set/locations/:setid/:keyword/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $setid, $keyword, $country, $location, $type) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Keywordset Details / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-details-location.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Keywordset Details'));
  $slim->render('rankings-details-keyword.php', array('id' => $id, 'setid' => $setid, 'keyword' => $keyword, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});

$slim->get('/rankings/local/:id/keywords/results/:keyword/:timestamp/:country/:location/:type', $authenticate($_SESSION['login']), function ($id, $keyword, $timestamp, $country, $location, $type) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Top 100 / Rankings / OneProSeo', 'javascript' => 'loader-rankings-results.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Top 100'));
  $slim->render('rankings-results.php', array('id' => $id, 'keyword' => $keyword, 'timestamp' => $timestamp, 'country' => $country, 'location' => $location, 'type' => $type));
  $slim->render('gen-footer.php');
});
$slim->get('/rankings/local/:id/detail-compare/:set_id(/:startdate)', $authenticate($_SESSION['login']), function ($id, $set_id, $startdate = '01.01.2018') use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Local Keyword Datumsvergleich / Rankings / OneProSeo', 'javascript' => 'loader-rankings-local-detail-compare.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Local Keyword Datumsvergleich'));
  $slim->render('rankings-local-detail-compare.php', array('id' => $id, 'set_id' => $set_id, 'startdate' => $startdate));
  $slim->render('gen-footer.php');
});

// SEA SEO BALANCER
$slim->get('/sea-seo-balancer/dashboard', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'SEA / SEO Balancer / Übersicht / OneProSeo', 'javascript' => 'loader-sea-seo-balancer-dashboard.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer'));
  $slim->render('sea-seo-balancer-dashboard.php');
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/overview/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'SEA / SEO Balancer / ohne Brand / OneProSeo', 'javascript' => 'loader-sea-seo-balancer-overview.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: ohne Brand'));
  $slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/brand/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'SEA / SEO Balancer / inkl. Brand / OneProSeo', 'javascript' => 'loader-sea-seo-balancer-brand.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: ink. Brand'));
	$slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/sea-vs-seo/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'SEA / SEO Balancer / SEO Team / OneProSeo', 'javascript' => 'loader-sea-seo-balancer-seaseo.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: SEO Team'));
	$slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-seo-balancer/seo-vs-sea/:id(/:startdate/:enddate)', $authenticate($_SESSION['login']), function ($id, $startdate=0, $enddate=0) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'SEA / SEO Balancer / SEO vs. SEA / OneProSeo', 'javascript' => 'loader-sea-seo-balancer-seosea.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA / SEO Balancer: SEA Team'));
	$slim->render('sea-seo-balancer-details.php', array('id' => $id, 'startdate' => $startdate, 'enddate' => $enddate));
  $slim->render('gen-footer.php');
});
$slim->get('/sea-landingpage-finder', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Landing Page Finder / SEA / OneProSeo', 'javascript' => 'loader-sea-landingpage-finder.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'SEA Landingpage Finder'));
  $slim->render('sea-landingpage-finder.php');
  $slim->render('gen-footer.php');
});


// STRUCTURE ROUTES
$slim->get('/structure/link-silo', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Link Silo Tool / OneProSeo', 'javascript' => 'loader-structure-link-silo.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('structure-link-silo.php');
  $slim->render('gen-footer.php');
});

$slim->get('/structure/indexed-urls', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Indexcheck Google / OneProSeo', 'javascript' => 'loader-structure-indexed-urls.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('structure-indexed-urls.php');
  $slim->render('gen-footer.php');
});

$slim->get('/structure/:jobid', $authenticate($_SESSION['login']), function ($jobid) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Indexcheck Google / OneProSeo', 'javascript' => 'loader-structure-indexed-urls-jobs.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('structure-indexed-urls-jobs.php', array('jobid' => $jobid));
  $slim->render('gen-footer.php');
});


// KEYWORDS ROUTES
$slim->get('/keywords', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->redirect($config['domain'] . 'keywords/keyword-db');
});
$slim->get('/keywords/', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->redirect('../keywords/keyword-db');
});

$slim->get('/keywords/keyword-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'One Keyword DB | OneProSeo', 'javascript' => 'loader-keywords-keyword-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-keyword-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/url-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'One URL DB | OneProSeo', 'javascript' => 'loader-keywords-url-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-url-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/suggest-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'One Suggest DB | OneProSeo', 'javascript' => 'loader-keywords-suggest-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-suggest-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/w-fragen-db', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'One W-Fragen DB | OneProSeo', 'javascript' => 'loader-keywords-w-fragen-db.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-w-fragen-db.php');
  $slim->render('gen-footer.php');
});

$slim->get('/keywords/searchvolume', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Suchvolumen / OneProSeo', 'javascript' => 'loader-keywords-searchvolume.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('keywords-searchvolume.php');
  $slim->render('gen-footer.php');
});


// TOOLS ROUTES
$slim->get('/tools/search-console', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Search Console Export / Tools / OneProSeo', 'javascript' => 'loader-tools-search-console.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('tools-search-console.php', array('headline' => 'Search Console Export'));
  $slim->render('gen-footer.php');
});

$slim->get('/tools/search-console/:url', $authenticate($_SESSION['login']), function ($url) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Search Console Export / Tools / OneProSeo', 'javascript' => 'loader-tools-search-console-detail.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('tools-search-console-detail.php', array('url' => $url, 'headline' => 'Search Console Export'));
  $slim->render('gen-footer.php');
});

$slim->get('/tools/page-indexing', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Page Indexing / Analytics / OneProSeo', 'javascript' => 'loader-analytics-page-indexing.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('gen-header-sub-rankings.php', array('headline' => 'Page Indexing'));
  $slim->render('analytics-page-indexing.php');
  $slim->render('gen-footer.php');
});

$slim->get('/tools/rankings-searchvolume', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Rankings / Suchvolumen Export / Tools / OneProSeo', 'javascript' => 'loader-tools-rankings-searchvolume.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('tools-rankings-searchvolume.php', array('headline' => 'Rankings / Suchvolumen'));
  $slim->render('gen-footer.php');
});

$slim->get('/tools/rankings-searchvolume/:id', $authenticate($_SESSION['login']), function ($id) use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Rankings / Suchvolumen Export / Tools / OneProSeo', 'javascript' => 'loader-tools-rankings-searchvolume-detail.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('tools-rankings-searchvolume-detail.php', array('id' => $id, 'headline' => 'Rankings / Suchvolumen'));
  $slim->render('gen-footer.php');
});

$slim->get('/tools/structured-data', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Structured Data Mark-Up Generator', 'javascript' => 'loader-tools-structured-data.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('tools-structured-data.php', array('headline' => 'Structured Data Mark-Up Generator'));
  $slim->render('gen-footer.php');
});

$slim->get('/tools/serp-snippet', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Serp Snippet Mark-Up Generator', 'javascript' => 'loader-tools-serp-snippet.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('tools-serp-snippet.php', array('headline' => 'Serp Snippet Tool'));
  $slim->render('gen-footer.php');
});

// ADMIN ROUTES
$slim->map('/admin/user', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Administration / Userverwaltung', 'javascript' => 'loader-admin-user.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('admin-user.php');
  $slim->render('gen-footer.php');
})->via('GET', 'POST');

// ADMIN ROUTES
$slim->map('/admin/user-edit', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Administration / Benutzer bearbeiten', 'javascript' => 'loader-admin-user-edit.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('admin-user-edit.php');
  $slim->render('gen-footer.php');
})->via('GET', 'POST');


// DEV ROUTES
$slim->get('/dev/correlation', $authenticate($_SESSION['login']), function () use ($slim) {
  $slim->render('gen-header.php', array('titletag' => 'Korrelation / OneProSeo', 'javascript' => 'loader-analytics-correlation.js'));
  $slim->render('gen-sidebar.php');
  $slim->render('analytics-correlation.php');
  $slim->render('gen-footer.php');
});

// INDEXWATCH
$slim->get('/indexwatch/travel-hotel-benchmarking', function () use ($slim) {
  $slim->render('gen-header-indexwatch.php', array('titletag' => 'OneProSeo.com Panda 4.1 Indexwatch / Travel / Hotel', 'javascript' => 'loader-indexwatch-travel.js', 'description' => 'Unser Indexwatch zeigt die täglichen Änderungen während des Panda 4.1 Updates an. Wir haben den Bereich Travel / Hotel public verfügbar gemacht. Der grosse Index bleibt natürlich unseren Enterprise Kunden vorbehalten (-:'));
  $slim->render('gen-indexwatch-travel.php', array('headline' => 'Indexwatch / Travel / Hotel'));
  $slim->render('gen-footer.php');
});

$slim->get('/indexwatch/seo', function () use ($slim) {
  $slim->render('gen-header-indexwatch.php', array('titletag' => 'OnePro SEO Agenturen Ranking 2015 / SEO / Agenturen', 'javascript' => 'loader-indexwatch-seo.js', 'description' => 'Unser Indexwatch zeigt die täglichen Änderungen während des Panda 4.1 Updates an. '));
  $slim->render('gen-indexwatch-seo.php', array('headline' => 'TOP 100 SEO Agenturen Ranking 2015'));
  $slim->render('gen-footer.php');
});

$slim->get('/indexwatch/ibusiness', function () use ($slim) {
  $slim->render('gen-header-indexwatch.php', array('titletag' => 'iBusiness SEO Agenturen Ranking 2015 / SEO / Agenturen', 'javascript' => 'loader-indexwatch-ibusiness.js', 'description' => 'Unser Indexwatch zeigt die täglichen Änderungen während des Panda 4.1 Updates an. '));
  $slim->render('gen-indexwatch-ibusiness.php', array('headline' => 'iBusiness SEO Agenturen Ranking 2015'));
  $slim->render('gen-footer.php');
});

$slim->get('/indexwatch/url', function () use ($slim) {
  $slim->render('gen-header-indexwatch.php', array('titletag' => 'OnePro SEO Indexwatch Rankings', 'javascript' => 'loader-indexwatch-url.js', 'description' => 'Unser Indexwatch zeigt die täglichen Änderungen während des Panda 4.1 Updates an.'));
  $slim->render('gen-indexwatch-url.php', array('headline' => 'Indexwatch'));
  $slim->render('gen-footer.php');
});

$slim->get('/onie', function () use ($slim) {
  $slim->render('gen-header-onie.php');
  $slim->render('gen-onie.php');
  $slim->render('gen-footer.php');
});


// GOOGLE DRIVE
$slim->get('/googledrive', function () use ($slim) {
  $slim->render('gen-googledrive.php');
});
$slim->map('/googledrive-csv', function () use ($slim) {
  $slim->render('gen-googledrive-csv.php');
})->via('GET', 'POST');

// API
$slim->get('/api/v1/:key/:type.json', function ($key, $type) use ($slim) {
  $slim->render('gen-api.php', array('key' => $key, 'type' => $type));
});

// AJAX LOADER AND HANDLER (NOT PROTECTED CAUSE FILENAME IS STORED IN SESSION -> SHOULD BE THE SAME AS IN RANKER)
$slim->map('/ajax/crawler/analyse/csv_request', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'csv_request', 'route' => 'crawler/analyse'));
})->via('GET', 'POST');

// AJAX LOADER AND HANDLER (PROTECTED WITH)
$slim->map('/ajax/:route+/:file', $authenticate($_SESSION['login']), function ($route, $file) use ($slim) {
  $slim->render('gen-ajax.php', array('file' => $file, 'route' => $route));
})->via('GET', 'POST');

// AJAX LOADER AND HANDLER (OPEN - NO AUTH)
$slim->map('/ajaxp/rankings/indexwatch_travel', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'indexwatch_travel', 'route' => 'rankings'));
})->via('GET', 'POST');
$slim->map('/ajaxp/rankings/indexwatch_seo', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'indexwatch_seo', 'route' => 'rankings'));
})->via('GET', 'POST');
$slim->map('/ajaxp/rankings/indexwatch_seo_url', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'indexwatch_seo_url', 'route' => 'rankings'));
})->via('GET', 'POST');
$slim->map('/ajaxp/rankings/indexwatch_ibusiness', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'indexwatch_ibusiness', 'route' => 'rankings'));
})->via('GET', 'POST');
$slim->map('/ajaxp/rankings/indexwatch_url', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'indexwatch_url', 'route' => 'rankings'));
})->via('GET', 'POST');
$slim->map('/ajaxp/socialcount/socialcount', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'socialcount', 'route' => 'socialcount'));
})->via('GET', 'POST');


$slim->map('/ajaxp/tools/serp_snippet', function () use ($slim) {
  $slim->render('gen-ajax.php', array('file' => 'serp_snippet', 'route' => 'tools'));
})->via('GET', 'POST');

$slim->run();

?>
