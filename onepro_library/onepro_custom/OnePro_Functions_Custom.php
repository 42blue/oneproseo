<?php
function onepro_formatDateFromTimestamp($iTimestamp, $sFormat = "d.m.Y H:i:s")
{
	return date("d.m.Y H:i:s", $iTimestamp);
}

function onepro_getHashId($sString, $iLength = 14)
{
	$sHash = hash("sha256", $sString);
	return base_convert(substr($sHash, 0, $iLength), 16, 10);
}

function onepro_formatNumber($iNumber, $iDezimals = 0)
{
	return number_format($iNumber, $iDezimals, ",", ".");
}
?>